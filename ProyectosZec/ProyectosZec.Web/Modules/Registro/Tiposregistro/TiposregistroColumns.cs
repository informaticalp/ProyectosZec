﻿
namespace ProyectosZec.Registro.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Registro.Tiposregistro")]
    [BasedOnRow(typeof(Entities.TiposregistroRow), CheckNames = true)]
    public class TiposregistroColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 TipoRegistroId { get; set; }
        [EditLink]
        public String TipoRegistro { get; set; }
    }
}