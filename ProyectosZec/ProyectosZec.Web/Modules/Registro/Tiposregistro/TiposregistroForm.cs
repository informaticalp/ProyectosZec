﻿
namespace ProyectosZec.Registro.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Registro.Tiposregistro")]
    [BasedOnRow(typeof(Entities.TiposregistroRow), CheckNames = true)]
    public class TiposregistroForm
    {
        public String TipoRegistro { get; set; }
    }
}