﻿
namespace ProyectosZec.Registro.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Registro/Tiposregistro"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.TiposregistroRow))]
    public class TiposregistroController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Registro/Tiposregistro/TiposregistroIndex.cshtml");
        }
    }
}