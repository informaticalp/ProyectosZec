﻿
namespace ProyectosZec.Registro {

    @Serenity.Decorators.registerClass()
    export class TiposregistroGrid extends Serenity.EntityGrid<TiposregistroRow, any> {
        protected getColumnsKey() { return 'Registro.Tiposregistro'; }
        protected getDialogType() { return TiposregistroDialog; }
        protected getIdProperty() { return TiposregistroRow.idProperty; }
        protected getInsertPermission() { return TiposregistroRow.insertPermission; }
        protected getLocalTextPrefix() { return TiposregistroRow.localTextPrefix; }
        protected getService() { return TiposregistroService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}