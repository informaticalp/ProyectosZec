﻿
namespace ProyectosZec.Registro.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Registro"), TableName("tiposregistro")]
    [DisplayName("Tiposregistro"), InstanceName("Tiposregistro")]
    [ReadPermission("Registro:General")]
    [ModifyPermission("Registro:General")]
    [LookupScript("Registro.Tiposregistro")]
    public sealed class TiposregistroRow : Row, IIdRow, INameRow
    {
        [DisplayName("Tipo Registro Id"), Identity]
        public Int32? TipoRegistroId
        {
            get { return Fields.TipoRegistroId[this]; }
            set { Fields.TipoRegistroId[this] = value; }
        }

        [DisplayName("Tipo Registro"), Size(20), NotNull, QuickSearch]
        public String TipoRegistro
        {
            get { return Fields.TipoRegistro[this]; }
            set { Fields.TipoRegistro[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.TipoRegistroId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.TipoRegistro; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public TiposregistroRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field TipoRegistroId;
            public StringField TipoRegistro;
        }
    }
}
