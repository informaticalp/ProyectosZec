﻿
namespace ProyectosZec.Registro {

    @Serenity.Decorators.registerClass()
    export class TiposregistroDialog extends Serenity.EntityDialog<TiposregistroRow, any> {
        protected getFormKey() { return TiposregistroForm.formKey; }
        protected getIdProperty() { return TiposregistroRow.idProperty; }
        protected getLocalTextPrefix() { return TiposregistroRow.localTextPrefix; }
        protected getNameProperty() { return TiposregistroRow.nameProperty; }
        protected getService() { return TiposregistroService.baseUrl; }
        protected getDeletePermission() { return TiposregistroRow.deletePermission; }
        protected getInsertPermission() { return TiposregistroRow.insertPermission; }
        protected getUpdatePermission() { return TiposregistroRow.updatePermission; }

        protected form = new TiposregistroForm(this.idPrefix);

    }
}