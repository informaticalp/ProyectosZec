﻿using Serenity.Navigation;
using MyPages = ProyectosZec.Registro.Pages;

[assembly: NavigationMenu(9000, "Geiser", icon: "fa-file")]
[assembly: NavigationLink(int.MaxValue, "Geiser/Ficheros", typeof(MyPages.RegistroController), icon: "fa-file-o")]
//[assembly: NavigationLink(int.MaxValue, "Registro/Tipos de registro", typeof(MyPages.TiposregistroController), icon: null)]