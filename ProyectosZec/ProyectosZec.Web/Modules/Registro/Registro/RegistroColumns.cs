﻿
namespace ProyectosZec.Registro.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Registro.Registro")]
    [BasedOnRow(typeof(Entities.RegistroRow), CheckNames = true)]
    public class RegistroColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 RegistroId { get; set; }
        [EditLink,Width(170)]
        public String NumeroRegistro { get; set; }
        [Width(120),QuickFilter]
        public DateTime FechaRegistro { get; set; }
        [Width(100),QuickFilter]
        public String TipoRegistro { get; set; }
        [Width(300)]
        public String Titulo { get; set; }
        [Width(300)]
        public String Persona { get; set; }
        [Width(300)]
        public String Observaciones { get; set; }
        [Hidden,Width(120)]
        public String UserName { get; set; }
        [Hidden,Width(125), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}