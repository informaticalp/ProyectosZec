﻿
namespace ProyectosZec.Registro {

    @Serenity.Decorators.registerClass()
    export class RegistroDialog extends Serenity.EntityDialog<RegistroRow, any> {
        protected getFormKey() { return RegistroForm.formKey; }
        protected getIdProperty() { return RegistroRow.idProperty; }
        protected getLocalTextPrefix() { return RegistroRow.localTextPrefix; }
        protected getNameProperty() { return RegistroRow.nameProperty; }
        protected getService() { return RegistroService.baseUrl; }
        protected getDeletePermission() { return RegistroRow.deletePermission; }
        protected getInsertPermission() { return RegistroRow.insertPermission; }
        protected getUpdatePermission() { return RegistroRow.updatePermission; }

        protected form = new RegistroForm(this.idPrefix);

    }
}