﻿
namespace ProyectosZec.Registro.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Registro/Registro"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.RegistroRow))]
    public class RegistroController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Registro/Registro/RegistroIndex.cshtml");
        }
    }
}