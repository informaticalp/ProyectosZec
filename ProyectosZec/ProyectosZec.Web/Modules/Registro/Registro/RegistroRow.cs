﻿
namespace ProyectosZec.Registro.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Registro"), TableName("registro")]
    [DisplayName("Registro"), InstanceName("Registro")]
    [ReadPermission("Registro:Read")]
    [ModifyPermission("Registro:Modify")]
    public sealed class RegistroRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? RegistroId
        {
            get { return Fields.RegistroId[this]; }
            set { Fields.RegistroId[this] = value; }
        }

        [DisplayName("Nº Reg. Oficial"), Size(255), NotNull, QuickSearch,Unique]
        public String NumeroRegistro
        {
            get { return Fields.NumeroRegistro[this]; }
            set { Fields.NumeroRegistro[this] = value; }
        }

        [DisplayName("Fecha"), NotNull]
        public DateTime? FechaRegistro
        {
            get { return Fields.FechaRegistro[this]; }
            set { Fields.FechaRegistro[this] = value; }
        }

        [DisplayName("Tipo"), NotNull, ForeignKey("tiposregistro", "TipoRegistroId"), LeftJoin("jTipoRegistro"), TextualField("TipoRegistro")]
        [LookupEditor(typeof(TiposregistroRow))]
        public Int32? TipoRegistroId
        {
            get { return Fields.TipoRegistroId[this]; }
            set { Fields.TipoRegistroId[this] = value; }
        }

        [DisplayName("Titulo"), Size(100),QuickSearch]
        public String Titulo
        {
            get { return Fields.Titulo[this]; }
            set { Fields.Titulo[this] = value; }
        }

        [DisplayName("Persona/Empresa"), Size(100), QuickSearch]
        public String Persona
        {
            get { return Fields.Persona[this]; }
            set { Fields.Persona[this] = value; }
        }
        [DisplayName("Ficheros"), Size(1000),MultipleFileUploadEditor(FilenameFormat = "Registro/Files/~", ScaleWidth = 800, ScaleHeight = 600)]
        public String Ficheros
        {
            get { return Fields.Ficheros[this]; }
            set { Fields.Ficheros[this] = value; }
        }

        [DisplayName("Observaciones"), Size(1000),QuickSearch]
        public String Observaciones
        {
            get { return Fields.Observaciones[this]; }
            set { Fields.Observaciones[this] = value; }
        }

        [DisplayName("Tipo Registro"), Expression("jTipoRegistro.[TipoRegistro]")]
        public String TipoRegistro
        {
            get { return Fields.TipoRegistro[this]; }
            set { Fields.TipoRegistro[this] = value; }
        }

        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        IIdField IIdRow.IdField
        {
            get { return Fields.RegistroId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.NumeroRegistro; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public RegistroRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field RegistroId;
            public StringField NumeroRegistro;
            public DateTimeField FechaRegistro;
            public Int32Field TipoRegistroId;
            public StringField Titulo;
            public StringField Persona;
            public StringField Ficheros;
            public StringField Observaciones;

            public StringField TipoRegistro;

            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
