﻿
namespace ProyectosZec.Registro {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido
    export class RegistroGrid extends Serenity.EntityGrid<RegistroRow, any> {
        protected getColumnsKey() { return 'Registro.Registro'; }
        protected getDialogType() { return RegistroDialog; }
        protected getIdProperty() { return RegistroRow.idProperty; }
        protected getInsertPermission() { return RegistroRow.insertPermission; }
        protected getLocalTextPrefix() { return RegistroRow.localTextPrefix; }
        protected getService() { return RegistroService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        protected getDefaultSortBy() {
            return [RegistroRow.Fields.FechaRegistro]; // Este es el campo de ordenación por defecto
        }


        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Registro/Registro/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));
  

            return buttons;
            // Fin añadidos
        }
    }
}