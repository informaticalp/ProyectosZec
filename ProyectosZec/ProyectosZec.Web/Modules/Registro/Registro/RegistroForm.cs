﻿
namespace ProyectosZec.Registro.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Registro.Registro")]
    [BasedOnRow(typeof(Entities.RegistroRow), CheckNames = true)]
    public class RegistroForm
    {
        public String NumeroRegistro { get; set; }
        public DateTime FechaRegistro { get; set; }
        public Int32 TipoRegistroId { get; set; }
        public String Titulo { get; set; }
        public String Persona { get; set; }
        public String Ficheros { get; set; }
        [TextAreaEditor(Rows = 3)]
        public String Observaciones { get; set; }
    }
}