﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class AlarmasProcedimientosDialog extends Serenity.EntityDialog<AlarmasProcedimientosRow, any> {
        protected getFormKey() { return AlarmasProcedimientosForm.formKey; }
        protected getIdProperty() { return AlarmasProcedimientosRow.idProperty; }
        protected getLocalTextPrefix() { return AlarmasProcedimientosRow.localTextPrefix; }
        protected getService() { return AlarmasProcedimientosService.baseUrl; }
        protected getDeletePermission() { return AlarmasProcedimientosRow.deletePermission; }
        protected getInsertPermission() { return AlarmasProcedimientosRow.insertPermission; }
        protected getUpdatePermission() { return AlarmasProcedimientosRow.updatePermission; }

        protected form = new AlarmasProcedimientosForm(this.idPrefix);

    }
}