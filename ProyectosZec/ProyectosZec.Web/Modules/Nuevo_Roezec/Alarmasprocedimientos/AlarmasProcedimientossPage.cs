﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_RoezecAlarmasProcedimientos"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.AlarmasProcedimientosRow))]
    public class AlarmasProcedimientosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/Alarmasprocedimientos/AlarmasProcedimientosIndex.cshtml");
        }
    }
}