﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.AlarmasProcedimientos")]
    [BasedOnRow(typeof(Entities.AlarmasProcedimientosRow), CheckNames = true)]
    public class AlarmasProcedimientosColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 AlarmaProcedimientoId { get; set; }
        [EditLink,Width(260)]
        public String TipoAlarma { get; set; }
        [Width(180)]
        public String SentidoResolucion { get; set; }
        [Width(100)]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}