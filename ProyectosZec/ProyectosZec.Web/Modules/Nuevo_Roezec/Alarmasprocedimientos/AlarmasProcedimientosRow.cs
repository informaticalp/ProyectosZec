﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("AlarmasProcedimientos")]
    [DisplayName("AlarmasProcedimientos"), InstanceName("AlarmasProcedimientos")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    public sealed class AlarmasProcedimientosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? AlarmaProcedimientoId
        {
            get { return Fields.AlarmaProcedimientoId[this]; }
            set { Fields.AlarmaProcedimientoId[this] = value; }
        }

        [DisplayName("Procedimiento"), NotNull, ForeignKey("procedimientos", "ProcedimientoId"), LeftJoin("jProcedimiento"), TextualField("Procedimiento")]
        public Int32? ProcedimientoId
        {
            get { return Fields.ProcedimientoId[this]; }
            set { Fields.ProcedimientoId[this] = value; }
        }

        [DisplayName("Tipo Alarma"), Column("TipoAlarmaId"), ForeignKey("tipos_alarma", "TipoAlarmaId"), LeftJoin("jTiposAlarma"), TextualField("AlarmaFavorable"), LookupInclude]
        [LookupEditor(typeof(Entities.TiposAlarmaRow), InplaceAdd = true)]
        public Int32? TipoAlarmaId
        {
            get { return Fields.TipoAlarmaId[this]; }
            set { Fields.TipoAlarmaId[this] = value; }
        }

        [DisplayName("Alarma"), Expression("jTiposAlarma.[Texto]")]
        public String TipoAlarma
        {
            get { return Fields.TipoAlarma[this]; }
            set { Fields.TipoAlarma[this] = value; }
        }

        [DisplayName("Sentido Resolucion"), NotNull, ForeignKey("sentidosresolucion", "SentidoResolucionId"), LeftJoin("jSentidoResolucion"), TextualField("SentidoResolucion")]
        [LookupEditor(typeof(Entities.SentidosresolucionRow))]
        public Int32? SentidoResolucionId
        {
            get { return Fields.SentidoResolucionId[this]; }
            set { Fields.SentidoResolucionId[this] = value; }
        }


        [DisplayName("Procedimiento"), Expression("jProcedimiento.[Procedimiento]")]
        public String Procedimiento
        {
            get { return Fields.Procedimiento[this]; }
            set { Fields.Procedimiento[this] = value; }
        }

        [DisplayName("Procedimiento Estado Empresa Id"), Expression("jProcedimiento.[EstadoEmpresaId]")]
        public Int32? ProcedimientoEstadoEmpresaId
        {
            get { return Fields.ProcedimientoEstadoEmpresaId[this]; }
            set { Fields.ProcedimientoEstadoEmpresaId[this] = value; }
        }

        [DisplayName("Procedimiento Estado Empresa Id Desfavorable"), Expression("jProcedimiento.[EstadoEmpresaIdDesfavorable]")]
        public Int32? ProcedimientoEstadoEmpresaIdDesfavorable
        {
            get { return Fields.ProcedimientoEstadoEmpresaIdDesfavorable[this]; }
            set { Fields.ProcedimientoEstadoEmpresaIdDesfavorable[this] = value; }
        }

        [DisplayName("Procedimiento User Id"), Expression("jProcedimiento.[UserId]")]
        public Int32? ProcedimientoUserId
        {
            get { return Fields.ProcedimientoUserId[this]; }
            set { Fields.ProcedimientoUserId[this] = value; }
        }

        [DisplayName("Procedimiento Fecha Modificacion"), Expression("jProcedimiento.[FechaModificacion]")]
        public DateTime? ProcedimientoFechaModificacion
        {
            get { return Fields.ProcedimientoFechaModificacion[this]; }
            set { Fields.ProcedimientoFechaModificacion[this] = value; }
        }

        [DisplayName("Procedimiento Alarma Favorable Id"), Expression("jProcedimiento.[AlarmaFavorableId]")]
        public Int32? ProcedimientoAlarmaFavorableId
        {
            get { return Fields.ProcedimientoAlarmaFavorableId[this]; }
            set { Fields.ProcedimientoAlarmaFavorableId[this] = value; }
        }

        [DisplayName("Procedimiento Alarma Desfavorable Id"), Expression("jProcedimiento.[AlarmaDesfavorableId]")]
        public Int32? ProcedimientoAlarmaDesfavorableId
        {
            get { return Fields.ProcedimientoAlarmaDesfavorableId[this]; }
            set { Fields.ProcedimientoAlarmaDesfavorableId[this] = value; }
        }

        [DisplayName("Sentido Resolucion"), Expression("jSentidoResolucion.[SentidoResolucion]")]
        public String SentidoResolucion
        {
            get { return Fields.SentidoResolucion[this]; }
            set { Fields.SentidoResolucion[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/
 

        IIdField IIdRow.IdField
        {
            get { return Fields.AlarmaProcedimientoId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.TipoAlarma; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AlarmasProcedimientosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field AlarmaProcedimientoId;
            public Int32Field ProcedimientoId;
            public Int32Field TipoAlarmaId;

            public Int32Field SentidoResolucionId;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/

            public StringField Procedimiento;
            public StringField TipoAlarma;
            public Int32Field ProcedimientoEstadoEmpresaId;
            public Int32Field ProcedimientoEstadoEmpresaIdDesfavorable;
            public Int32Field ProcedimientoUserId;
            public DateTimeField ProcedimientoFechaModificacion;
            public Int32Field ProcedimientoAlarmaFavorableId;
            public Int32Field ProcedimientoAlarmaDesfavorableId;

            public StringField SentidoResolucion;
        }
    }
}
