﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class AlarmasProcedimientosEditDialog extends Common.GridEditorDialog<AlarmasProcedimientosRow> {
        protected getFormKey() { return AlarmasProcedimientosForm.formKey; }
        protected getNameProperty() { return AlarmasProcedimientosRow.nameProperty; }
        protected getLocalTextPrefix() { return AlarmasProcedimientosRow.localTextPrefix; }

        protected form: AlarmasProcedimientosForm;

        constructor() {
            super();
            this.form = new AlarmasProcedimientosForm(this.idPrefix);
        }
    }
}