﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.AlarmasProcedimientos")]
    [BasedOnRow(typeof(Entities.AlarmasProcedimientosRow), CheckNames = true)]
    public class AlarmasProcedimientosForm
    {
        public Int32 TipoAlarmaId { get; set; }
        public Int32 SentidoResolucionId { get; set; }

    }
}