﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class AlarmasProcedimientosGrid extends Serenity.EntityGrid<AlarmasProcedimientosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.AlarmasProcedimientos'; }
        protected getDialogType() { return AlarmasProcedimientosDialog; }
        protected getIdProperty() { return AlarmasProcedimientosRow.idProperty; }
        protected getInsertPermission() { return AlarmasProcedimientosRow.insertPermission; }
        protected getLocalTextPrefix() { return AlarmasProcedimientosRow.localTextPrefix; }
        protected getService() { return AlarmasProcedimientosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}