﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class AlarmasProcedimientosEditor extends Common.GridEditorBase<AlarmasProcedimientosRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.AlarmasProcedimientos"; }
        protected getDialogType() { return AlarmasProcedimientosEditDialog; }
        protected getLocalTextPrefix() { return AlarmasProcedimientosRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Alarma";
        }
    }
}