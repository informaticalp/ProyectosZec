﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.EstadosEnvio")]
    [BasedOnRow(typeof(Entities.EstadosEnvioRow), CheckNames = true)]
    public class EstadosEnvioForm
    {
        public String Estado { get; set; }
    }
}