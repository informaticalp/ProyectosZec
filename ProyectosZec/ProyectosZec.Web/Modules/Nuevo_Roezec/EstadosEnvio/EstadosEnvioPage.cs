﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/EstadosEnvio"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EstadosEnvioRow))]
    public class EstadosEnvioController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/EstadosEnvio/EstadosEnvioIndex.cshtml");
        }
    }
}