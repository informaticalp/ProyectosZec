﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EstadosEnvioGrid extends Serenity.EntityGrid<EstadosEnvioRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.EstadosEnvio'; }
        protected getDialogType() { return EstadosEnvioDialog; }
        protected getIdProperty() { return EstadosEnvioRow.idProperty; }
        protected getInsertPermission() { return EstadosEnvioRow.insertPermission; }
        protected getLocalTextPrefix() { return EstadosEnvioRow.localTextPrefix; }
        protected getService() { return EstadosEnvioService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}