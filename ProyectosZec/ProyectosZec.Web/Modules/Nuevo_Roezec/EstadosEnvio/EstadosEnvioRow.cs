﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("estados_envio")]
    [DisplayName("Estados Envio"), InstanceName("Estados Envio")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    [LookupScript("Nuevo_Roezec.EstadosEnvio")]
    public sealed class EstadosEnvioRow : Row, IIdRow, INameRow
    {
        [DisplayName("Estado Envio Id"), Identity]
        public Int32? EstadoEnvioId
        {
            get { return Fields.EstadoEnvioId[this]; }
            set { Fields.EstadoEnvioId[this] = value; }
        }

        [DisplayName("Estado"), Size(50), NotNull, QuickSearch]
        public String Estado
        {
            get { return Fields.Estado[this]; }
            set { Fields.Estado[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        IIdField IIdRow.IdField
        {
            get { return Fields.EstadoEnvioId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Estado; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public EstadosEnvioRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field EstadoEnvioId;
            public StringField Estado;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
