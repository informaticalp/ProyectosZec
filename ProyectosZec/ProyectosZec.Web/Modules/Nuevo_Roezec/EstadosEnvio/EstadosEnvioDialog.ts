﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EstadosEnvioDialog extends Serenity.EntityDialog<EstadosEnvioRow, any> {
        protected getFormKey() { return EstadosEnvioForm.formKey; }
        protected getIdProperty() { return EstadosEnvioRow.idProperty; }
        protected getLocalTextPrefix() { return EstadosEnvioRow.localTextPrefix; }
        protected getNameProperty() { return EstadosEnvioRow.nameProperty; }
        protected getService() { return EstadosEnvioService.baseUrl; }
        protected getDeletePermission() { return EstadosEnvioRow.deletePermission; }
        protected getInsertPermission() { return EstadosEnvioRow.insertPermission; }
        protected getUpdatePermission() { return EstadosEnvioRow.updatePermission; }

        protected form = new EstadosEnvioForm(this.idPrefix);

    }
}