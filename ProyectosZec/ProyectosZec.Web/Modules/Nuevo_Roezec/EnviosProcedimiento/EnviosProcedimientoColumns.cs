﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.EnviosProcedimiento")]
    [BasedOnRow(typeof(Entities.EnviosProcedimientoRow), CheckNames = true)]
    public class EnviosProcedimientoColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Width(50)]
        public Int32 EnvioId { get; set; }
        public String HistorialObservaciones { get; set; }
        public String TipoEnvioTipo { get; set; }
        [Width(110),DisplayFormat("d")]
        public DateTime FechaEnvio { get; set; }
        public String ContactoEnvioNombre { get; set; }
        [Width(110), DisplayFormat("d")]
        public DateTime FechaRecepcion { get; set; }
        public Int32 ContactoAcuseId { get; set; }
        [Width(100)]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}