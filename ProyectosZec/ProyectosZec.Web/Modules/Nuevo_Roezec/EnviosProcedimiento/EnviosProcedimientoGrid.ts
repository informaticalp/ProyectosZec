﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EnviosProcedimientoGrid extends Serenity.EntityGrid<EnviosProcedimientoRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.EnviosProcedimiento'; }
        protected getDialogType() { return EnviosProcedimientoDialog; }
        protected getIdProperty() { return EnviosProcedimientoRow.idProperty; }
        protected getInsertPermission() { return EnviosProcedimientoRow.insertPermission; }
        protected getLocalTextPrefix() { return EnviosProcedimientoRow.localTextPrefix; }
        protected getService() { return EnviosProcedimientoService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}