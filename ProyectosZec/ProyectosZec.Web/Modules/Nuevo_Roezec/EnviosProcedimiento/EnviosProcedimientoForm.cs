﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.EnviosProcedimiento")]
    [BasedOnRow(typeof(Entities.EnviosProcedimientoRow), CheckNames = true)]
    public class EnviosProcedimientoForm
    {
        public Int32 HistorialId { get; set; }
        public Int32 TipoEnvioId { get; set; }
        public DateTime FechaEnvio { get; set; }
        public Int32 ContactoEnvioId { get; set; }
        public DateTime FechaRecepcion { get; set; }
        public Int32 ContactoAcuseId { get; set; }
    }
}