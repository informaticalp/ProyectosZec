﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("envios")]
    [DisplayName("Envios Procedimiento"), InstanceName("Envios Procedimiento")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    public sealed class EnviosProcedimientoRow : Row, IIdRow
    {
        [DisplayName("Envio Id"), Identity]
        public Int32? EnvioId
        {
            get { return Fields.EnvioId[this]; }
            set { Fields.EnvioId[this] = value; }
        }

        [DisplayName("Historial"), NotNull, ForeignKey("historial_empresas", "HistorialId"), LeftJoin("jHistorial"), TextualField("HistorialObservaciones")]
        public Int32? HistorialId
        {
            get { return Fields.HistorialId[this]; }
            set { Fields.HistorialId[this] = value; }
        }

        [DisplayName("Tipo Envio"), NotNull, ForeignKey("tipos_envio", "TipoEnvioId"), LeftJoin("jTipoEnvio"), TextualField("TipoEnvioTipo")]
        public Int32? TipoEnvioId
        {
            get { return Fields.TipoEnvioId[this]; }
            set { Fields.TipoEnvioId[this] = value; }
        }

        [DisplayName("Fecha Envio"), NotNull]
        public DateTime? FechaEnvio
        {
            get { return Fields.FechaEnvio[this]; }
            set { Fields.FechaEnvio[this] = value; }
        }

        [DisplayName("Contacto Envio"), NotNull, ForeignKey("contactos", "ContactoId"), LeftJoin("jContactoEnvio"), TextualField("ContactoEnvioNombre")]
        public Int32? ContactoEnvioId
        {
            get { return Fields.ContactoEnvioId[this]; }
            set { Fields.ContactoEnvioId[this] = value; }
        }

        [DisplayName("Fecha Recepcion")]
        public DateTime? FechaRecepcion
        {
            get { return Fields.FechaRecepcion[this]; }
            set { Fields.FechaRecepcion[this] = value; }
        }

        [DisplayName("Contacto Acuse Id")]
        public Int32? ContactoAcuseId
        {
            get { return Fields.ContactoAcuseId[this]; }
            set { Fields.ContactoAcuseId[this] = value; }
        }

        [DisplayName("Historial Empresa Id"), Expression("jHistorial.[EmpresaId]")]
        public Int32? HistorialEmpresaId
        {
            get { return Fields.HistorialEmpresaId[this]; }
            set { Fields.HistorialEmpresaId[this] = value; }
        }

        [DisplayName("Historial Procedimiento Id"), Expression("jHistorial.[ProcedimientoId]")]
        public Int32? HistorialProcedimientoId
        {
            get { return Fields.HistorialProcedimientoId[this]; }
            set { Fields.HistorialProcedimientoId[this] = value; }
        }

        [DisplayName("Historial Fecha Inicio"), Expression("jHistorial.[Fecha_Inicio]")]
        public DateTime? HistorialFechaInicio
        {
            get { return Fields.HistorialFechaInicio[this]; }
            set { Fields.HistorialFechaInicio[this] = value; }
        }

        [DisplayName("Historial Fecha Resolucion"), Expression("jHistorial.[Fecha_Resolucion]")]
        public DateTime? HistorialFechaResolucion
        {
            get { return Fields.HistorialFechaResolucion[this]; }
            set { Fields.HistorialFechaResolucion[this] = value; }
        }

        [DisplayName("Historial Sentido Resolucion"), Expression("jHistorial.[Sentido_Resolucion]")]
        public Int16? HistorialSentidoResolucion
        {
            get { return Fields.HistorialSentidoResolucion[this]; }
            set { Fields.HistorialSentidoResolucion[this] = value; }
        }

        [DisplayName("Historial Fecha Efecto"), Expression("jHistorial.[Fecha_efecto]")]
        public DateTime? HistorialFechaEfecto
        {
            get { return Fields.HistorialFechaEfecto[this]; }
            set { Fields.HistorialFechaEfecto[this] = value; }
        }

        [DisplayName("Historial Acuse Inicio"), Expression("jHistorial.[Acuse_Inicio]")]
        public DateTime? HistorialAcuseInicio
        {
            get { return Fields.HistorialAcuseInicio[this]; }
            set { Fields.HistorialAcuseInicio[this] = value; }
        }

        [DisplayName("Historial Persona Acuse Incio Id"), Expression("jHistorial.[Persona_Acuse_IncioId]")]
        public Int32? HistorialPersonaAcuseIncioId
        {
            get { return Fields.HistorialPersonaAcuseIncioId[this]; }
            set { Fields.HistorialPersonaAcuseIncioId[this] = value; }
        }

        [DisplayName("Historial Acuse Resolucion"), Expression("jHistorial.[Acuse_Resolucion]")]
        public DateTime? HistorialAcuseResolucion
        {
            get { return Fields.HistorialAcuseResolucion[this]; }
            set { Fields.HistorialAcuseResolucion[this] = value; }
        }

        [DisplayName("Historial Persona Acuse Resolucion Id"), Expression("jHistorial.[Persona_Acuse_ResolucionId]")]
        public Int32? HistorialPersonaAcuseResolucionId
        {
            get { return Fields.HistorialPersonaAcuseResolucionId[this]; }
            set { Fields.HistorialPersonaAcuseResolucionId[this] = value; }
        }

        [DisplayName("Historial Observaciones"), Expression("jHistorial.[Observaciones]")]
        public String HistorialObservaciones
        {
            get { return Fields.HistorialObservaciones[this]; }
            set { Fields.HistorialObservaciones[this] = value; }
        }

        [DisplayName("Historial Ficheros"), Expression("jHistorial.[Ficheros]")]
        public String HistorialFicheros
        {
            get { return Fields.HistorialFicheros[this]; }
            set { Fields.HistorialFicheros[this] = value; }
        }

        [DisplayName("Tipo Envio Tipo"), Expression("jTipoEnvio.[Tipo]")]
        public String TipoEnvioTipo
        {
            get { return Fields.TipoEnvioTipo[this]; }
            set { Fields.TipoEnvioTipo[this] = value; }
        }

        [DisplayName("Contacto Envio Nombre"), Expression("jContactoEnvio.[Nombre]")]
        public String ContactoEnvioNombre
        {
            get { return Fields.ContactoEnvioNombre[this]; }
            set { Fields.ContactoEnvioNombre[this] = value; }
        }

        [DisplayName("Contacto Envio Apellidos"), Expression("jContactoEnvio.[Apellidos]")]
        public String ContactoEnvioApellidos
        {
            get { return Fields.ContactoEnvioApellidos[this]; }
            set { Fields.ContactoEnvioApellidos[this] = value; }
        }

        [DisplayName("Contacto Envio Nif"), Expression("jContactoEnvio.[Nif]")]
        public String ContactoEnvioNif
        {
            get { return Fields.ContactoEnvioNif[this]; }
            set { Fields.ContactoEnvioNif[this] = value; }
        }

        [DisplayName("Contacto Envio Telefono Fijo"), Expression("jContactoEnvio.[Telefono_fijo]")]
        public String ContactoEnvioTelefonoFijo
        {
            get { return Fields.ContactoEnvioTelefonoFijo[this]; }
            set { Fields.ContactoEnvioTelefonoFijo[this] = value; }
        }

        [DisplayName("Contacto Envio Movil"), Expression("jContactoEnvio.[Movil]")]
        public String ContactoEnvioMovil
        {
            get { return Fields.ContactoEnvioMovil[this]; }
            set { Fields.ContactoEnvioMovil[this] = value; }
        }

        [DisplayName("Contacto Envio Idioma Id"), Expression("jContactoEnvio.[IdiomaId]")]
        public Int32? ContactoEnvioIdiomaId
        {
            get { return Fields.ContactoEnvioIdiomaId[this]; }
            set { Fields.ContactoEnvioIdiomaId[this] = value; }
        }

        [DisplayName("Contacto Envio Email"), Expression("jContactoEnvio.[Email]")]
        public String ContactoEnvioEmail
        {
            get { return Fields.ContactoEnvioEmail[this]; }
            set { Fields.ContactoEnvioEmail[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        IIdField IIdRow.IdField
        {
            get { return Fields.EnvioId; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public EnviosProcedimientoRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field EnvioId;
            public Int32Field HistorialId;
            public Int32Field TipoEnvioId;
            public DateTimeField FechaEnvio;
            public Int32Field ContactoEnvioId;
            public DateTimeField FechaRecepcion;
            public Int32Field ContactoAcuseId;

            public Int32Field HistorialEmpresaId;
            public Int32Field HistorialProcedimientoId;
            public DateTimeField HistorialFechaInicio;
            public DateTimeField HistorialFechaResolucion;
            public Int16Field HistorialSentidoResolucion;
            public DateTimeField HistorialFechaEfecto;
            public DateTimeField HistorialAcuseInicio;
            public Int32Field HistorialPersonaAcuseIncioId;
            public DateTimeField HistorialAcuseResolucion;
            public Int32Field HistorialPersonaAcuseResolucionId;
            public StringField HistorialObservaciones;
            public StringField HistorialFicheros;

            public StringField TipoEnvioTipo;

            public StringField ContactoEnvioNombre;
            public StringField ContactoEnvioApellidos;
            public StringField ContactoEnvioNif;
            public StringField ContactoEnvioTelefonoFijo;
            public StringField ContactoEnvioMovil;
            public Int32Field ContactoEnvioIdiomaId;
            public StringField ContactoEnvioEmail;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
