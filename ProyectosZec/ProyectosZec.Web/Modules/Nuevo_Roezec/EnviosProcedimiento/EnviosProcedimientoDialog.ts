﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EnviosProcedimientoDialog extends Serenity.EntityDialog<EnviosProcedimientoRow, any> {
        protected getFormKey() { return EnviosProcedimientoForm.formKey; }
        protected getIdProperty() { return EnviosProcedimientoRow.idProperty; }
        protected getLocalTextPrefix() { return EnviosProcedimientoRow.localTextPrefix; }
        protected getService() { return EnviosProcedimientoService.baseUrl; }
        protected getDeletePermission() { return EnviosProcedimientoRow.deletePermission; }
        protected getInsertPermission() { return EnviosProcedimientoRow.insertPermission; }
        protected getUpdatePermission() { return EnviosProcedimientoRow.updatePermission; }

        protected form = new EnviosProcedimientoForm(this.idPrefix);

    }
}