﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/EnviosProcedimiento"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EnviosProcedimientoRow))]
    public class EnviosProcedimientoController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/EnviosProcedimiento/EnviosProcedimientoIndex.cshtml");
        }
    }
}