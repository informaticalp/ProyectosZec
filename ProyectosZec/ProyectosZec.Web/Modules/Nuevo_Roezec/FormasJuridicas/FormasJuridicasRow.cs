﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("formas_juridicas")]
    [DisplayName("Formas Juridicas"), InstanceName("Formas Juridicas")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.FormasJuridicas")]
    public sealed class FormasJuridicasRow : Row, IIdRow, INameRow
    {
        [DisplayName("Juridica Id"), Column("Forma_JuridicaId"), Identity]
        public Int32? JuridicaId
        {
            get { return Fields.JuridicaId[this]; }
            set { Fields.JuridicaId[this] = value; }
        }

        [DisplayName("Juridica"), Column("Forma_Juridica"), Size(50), NotNull, QuickSearch]
        public String Juridica
        {
            get { return Fields.Juridica[this]; }
            set { Fields.Juridica[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        IIdField IIdRow.IdField
        {
            get { return Fields.JuridicaId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Juridica; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public FormasJuridicasRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field JuridicaId;
            public StringField Juridica;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
