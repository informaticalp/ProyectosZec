﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("paises")]
    [DisplayName("Paises"), InstanceName("Paises")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.Paises")]
    public sealed class PaisesRow : Row, IIdRow, INameRow
    {
        [DisplayName("Pais Id"), Identity]
        public Int32? PaisId
        {
            get { return Fields.PaisId[this]; }
            set { Fields.PaisId[this] = value; }
        }

        [DisplayName("Pais"), Size(50), NotNull, QuickSearch]
        public String Pais
        {
            get { return Fields.Pais[this]; }
            set { Fields.Pais[this] = value; }
        }

        [DisplayName("Capital"), Size(50), NotNull]
        public String Capital
        {
            get { return Fields.Capital[this]; }
            set { Fields.Capital[this] = value; }
        }

        [DisplayName("Continente Id"), Column("ContinenteId"), NotNull, ForeignKey("Continentes", "ContinenteId"), LeftJoin("jContinentes")]
        [LookupEditor(typeof(Entities.ContinentesRow))]
        public Int32? ContinenteId
        {
            get { return Fields.ContinenteId[this]; }
            set { Fields.ContinenteId[this] = value; }
        }

        [DisplayName("Continente"), Expression("jContinentes.[Continente]")]
        public String Continente
        {
            get { return Fields.Continente[this]; }
            set { Fields.Continente[this] = value; }
        }

        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        IIdField IIdRow.IdField
        {
            get { return Fields.PaisId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Pais; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public PaisesRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field PaisId;
            public StringField Pais;
            public StringField Capital;
            public Int32Field ContinenteId;
            public StringField Continente;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
