﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Paises")]
    [BasedOnRow(typeof(Entities.PaisesRow), CheckNames = true)]
    public class PaisesColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 PaisId { get; set; }
        [EditLink]
        public String Pais { get; set; }
        public String Capital { get; set; }
        public String Continente { get; set; }
        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}