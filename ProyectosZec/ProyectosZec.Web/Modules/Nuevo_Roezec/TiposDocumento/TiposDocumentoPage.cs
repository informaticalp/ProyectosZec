﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/TiposDocumento"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.TiposDocumentoRow))]
    public class TiposDocumentoController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/TiposDocumento/TiposDocumentoIndex.cshtml");
        }
    }
}