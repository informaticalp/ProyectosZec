﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposDocumentoDialog extends Serenity.EntityDialog<TiposDocumentoRow, any> {
        protected getFormKey() { return TiposDocumentoForm.formKey; }
        protected getIdProperty() { return TiposDocumentoRow.idProperty; }
        protected getLocalTextPrefix() { return TiposDocumentoRow.localTextPrefix; }
        protected getNameProperty() { return TiposDocumentoRow.nameProperty; }
        protected getService() { return TiposDocumentoService.baseUrl; }
        protected getDeletePermission() { return TiposDocumentoRow.deletePermission; }
        protected getInsertPermission() { return TiposDocumentoRow.insertPermission; }
        protected getUpdatePermission() { return TiposDocumentoRow.updatePermission; }

        protected form = new TiposDocumentoForm(this.idPrefix);

    }
}