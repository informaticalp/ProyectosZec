﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.TiposDocumento")]
    [BasedOnRow(typeof(Entities.TiposDocumentoRow), CheckNames = true)]
    public class TiposDocumentoForm
    {
        public String Tipo { get; set; }
    }
}