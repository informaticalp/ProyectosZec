﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposDocumentoGrid extends Serenity.EntityGrid<TiposDocumentoRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.TiposDocumento'; }
        protected getDialogType() { return TiposDocumentoDialog; }
        protected getIdProperty() { return TiposDocumentoRow.idProperty; }
        protected getInsertPermission() { return TiposDocumentoRow.insertPermission; }
        protected getLocalTextPrefix() { return TiposDocumentoRow.localTextPrefix; }
        protected getService() { return TiposDocumentoService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}