﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.TiposDocumento")]
    [BasedOnRow(typeof(Entities.TiposDocumentoRow), CheckNames = true)]
    public class TiposDocumentoColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 TipoDocumentoId { get; set; }
        [EditLink]
        public String Tipo { get; set; }
    }
}