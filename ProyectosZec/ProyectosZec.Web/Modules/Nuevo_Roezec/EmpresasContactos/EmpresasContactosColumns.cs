﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.EmpresasContactos")]
    [BasedOnRow(typeof(Entities.EmpresasContactosRow), CheckNames = true)]
    public class EmpresasContactosColumns
    {
        [Width(60),EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 EmpresaContactoId { get; set; }

        [Width(120),EditLink]
        public String ContactoNombre { get; set; }
        //[Width(150), EditLink]
        //public String ContactoApellidos { get; set; }
        [Width(170)]
        public String TipoContactoContacto { get; set; }
        [Width(85), AlignCenter]
        public Boolean Residente { get; set; }
        [Width(100), AlignCenter]
        public Boolean Mancomunado { get; set; }
        [Width(85), AlignCenter]
        public Boolean Solidario { get; set; }
        [Width(85)]
        [AlignRight, DisplayFormat("#,##0.00")]
        public Decimal Porcentaje { get; set; }
        [Width(250)]
        public String ContactoEmail { get; set; }
        [Width(120), DisplayFormat("d")]
        public DateTime FechaAlta { get; set; }
        [Width(120),DisplayFormat("d")]
        public DateTime FechaBaja { get; set; }
        [Width(100)]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}