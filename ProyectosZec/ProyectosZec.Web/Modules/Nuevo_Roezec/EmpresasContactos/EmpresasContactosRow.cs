﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("empresas_contactos")]
    [DisplayName("Empresas Contactos"), InstanceName("Empresas Contactos")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    public sealed class EmpresasContactosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("Empresa_ContactoId"), Identity]
        public Int32? EmpresaContactoId
        {
            get { return Fields.EmpresaContactoId[this]; }
            set { Fields.EmpresaContactoId[this] = value; }
        }

        [DisplayName("Empresa"), NotNull, ForeignKey("empresas", "EmpresaId"), LeftJoin("jEmpresa"), TextualField("EmpresaRazon"),QuickSearch,LookupInclude]
        [LookupEditor(typeof(Entities.EmpresasRow))]
        public Int32? EmpresaId
        {
            get { return Fields.EmpresaId[this]; }
            set { Fields.EmpresaId[this] = value; }
        }

        [DisplayName("Contacto"), NotNull, ForeignKey("contactos", "ContactoId"), LeftJoin("jContacto"), TextualField("ContactoNombre"),LookupInclude]
        [LookupEditor(typeof(Entities.ContactosRow), InplaceAdd = true)]
        public Int32? ContactoId
        {
            get { return Fields.ContactoId[this]; }
            set { Fields.ContactoId[this] = value; }
        }

        [DisplayName("Tipo Contacto"), Column("Tipo_ContactoId"), NotNull, ForeignKey("tipos_contacto", "Tipo_ContactoId"), LeftJoin("jTipoContacto"), TextualField("TipoContactoContacto"),LookupInclude]
        [LookupEditor(typeof(Entities.TiposContactoRow))]
        public Int32? TipoContactoId
        {
            get { return Fields.TipoContactoId[this]; }
            set { Fields.TipoContactoId[this] = value; }
        }

        [DisplayName("Porcentaje"), Size(5), Scale(2)]
        public Decimal? Porcentaje
        {
            get { return Fields.Porcentaje[this]; }
            set { Fields.Porcentaje[this] = value; }
        }
        [DisplayName("Residente"), Column("Residente")]
        public Boolean? Residente
        {
            get { return Fields.Residente[this]; }
            set { Fields.Residente[this] = value; }
        }
        [DisplayName("Mancomunado"), Column("Mancomunado")]
        public Boolean? Mancomunado
        {
            get { return Fields.Mancomunado[this]; }
            set { Fields.Mancomunado[this] = value; }
        }
        [DisplayName("Solidario"), Column("Solidario")]
        public Boolean? Solidario
        {
            get { return Fields.Solidario[this]; }
            set { Fields.Solidario[this] = value; }
        }

        [DisplayName("Fecha Alta"), Column("Fecha_Alta")]
        public DateTime? FechaAlta
        {
            get { return Fields.FechaAlta[this]; }
            set { Fields.FechaAlta[this] = value; }
        }

        [DisplayName("Fecha Baja"), Column("Fecha_Baja")]
        public DateTime? FechaBaja
        {
            get { return Fields.FechaBaja[this]; }
            set { Fields.FechaBaja[this] = value; }
        }

        [DisplayName("Empresa"), Expression("jEmpresa.[Razon]"), QuickSearch]
        public String EmpresaRazon
        {
            get { return Fields.EmpresaRazon[this]; }
            set { Fields.EmpresaRazon[this] = value; }
        }

        [DisplayName("Forma Juridica"), Expression("jEmpresa.[FormaJuridicaId]"), ForeignKey("formas_juridicas", "FormaJuridicaId"), LeftJoin("jFormaJuridica"), LookupInclude]
        [LookupEditor(typeof(Entities.FormasJuridicasRow))]
        public Int32? EmpresaFormaJuridicaId
        {
            get { return Fields.EmpresaFormaJuridicaId[this]; }
            set { Fields.EmpresaFormaJuridicaId[this] = value; }
        }

        [DisplayName("Expediente"), Expression("jEmpresa.[Expediente]")]
        public String EmpresaNExpediente
        {
            get { return Fields.EmpresaNExpediente[this]; }
            set { Fields.EmpresaNExpediente[this] = value; }
        }

        [DisplayName("TecnicoId"), Expression("jEmpresa.[TecnicoId]"), ForeignKey("tecnicos", "TecnicoId"), LeftJoin("jTecnico"), LookupInclude, TextualField("TecnicoNombreTecnico")]
        [LookupEditor(typeof(Entities.TecnicosRow))]
        public Int32? EmpresaTecnicoId
        {
            get { return Fields.EmpresaTecnicoId[this]; }
            set { Fields.EmpresaTecnicoId[this] = value; }
        }

        [DisplayName("Tecnico"), Expression("jTecnico.[NombreTecnico]")]
        public String TecnicoNombreTecnico
        {
            get { return Fields.TecnicoNombreTecnico[this]; }
            set { Fields.TecnicoNombreTecnico[this] = value; }
        }

        [DisplayName("Cif"), Expression("jEmpresa.[Cif]")]
        public String EmpresaCif
        {
            get { return Fields.EmpresaCif[this]; }
            set { Fields.EmpresaCif[this] = value; }
        }

        [DisplayName("Direccion"), Expression("jEmpresa.[Direccion]")]
        public String EmpresaDireccion
        {
            get { return Fields.EmpresaDireccion[this]; }
            set { Fields.EmpresaDireccion[this] = value; }
        }

        [DisplayName("Poblacion"), Expression("jEmpresa.[Poblacion]")]
        public String EmpresaPoblacion
        {
            get { return Fields.EmpresaPoblacion[this]; }
            set { Fields.EmpresaPoblacion[this] = value; }
        }

        [DisplayName("IslaId"), Expression("jEmpresa.[IslaId]"), ForeignKey("islas", "IslaId"), LeftJoin("jIsla"), TextualField("IslaNombreIsla"), LookupInclude]
        [LookupEditor(typeof(Entities.IslasRow))]
        public Int32? EmpresaIslaId
        {
            get { return Fields.EmpresaIslaId[this]; }
            set { Fields.EmpresaIslaId[this] = value; }
        }
        [DisplayName("Isla"), Expression("jIsla.[NombreIsla]")]
        public String IslaNombreIsla
        {
            get { return Fields.IslaNombreIsla[this]; }
            set { Fields.IslaNombreIsla[this] = value; }
        }

        [DisplayName("Telefono Fijo"), Expression("jEmpresa.[Telefono_fijo]")]
        public String EmpresaTelefonoFijo
        {
            get { return Fields.EmpresaTelefonoFijo[this]; }
            set { Fields.EmpresaTelefonoFijo[this] = value; }
        }

        [DisplayName("Movil"), Expression("jEmpresa.[Movil]")]
        public String EmpresaMovil
        {
            get { return Fields.EmpresaMovil[this]; }
            set { Fields.EmpresaMovil[this] = value; }
        }

        [DisplayName("Email"), Expression("jEmpresa.[Email]")]
        public String EmpresaEmail
        {
            get { return Fields.EmpresaEmail[this]; }
            set { Fields.EmpresaEmail[this] = value; }
        }

        [DisplayName("Empresa Proyecto Id"), Expression("jEmpresa.[ProyectoId]")]
        public Int32? EmpresaProyectoId
        {
            get { return Fields.EmpresaProyectoId[this]; }
            set { Fields.EmpresaProyectoId[this] = value; }
        }

        [DisplayName("Expediente"), Expression("jEmpresa.[Expediente]")]
        public String EmpresaExpediente
        {
            get { return Fields.EmpresaExpediente[this]; }
            set { Fields.EmpresaExpediente[this] = value; }
        }

        [DisplayName("Motivo Exencion"), Expression("jEmpresa.[Motivo_Exencion]")]
        public String EmpresaMotivoExencion
        {
            get { return Fields.EmpresaMotivoExencion[this]; }
            set { Fields.EmpresaMotivoExencion[this] = value; }
        }

        [DisplayName("Tipologia Capital Id"), Expression("jEmpresa.[Tipologia_CapitalId]"), ForeignKey("tipologias_capital", "Tipologia_CapitalId"), LeftJoin("jTipologiaCapital"), TextualField("TipologiaCapitalCapital"), LookupInclude, QuickSearch]
        [LookupEditor(typeof(Entities.TipologiasCapitalRow))]
        public Int32? EmpresaTipologiaCapitalId
        {
            get { return Fields.EmpresaTipologiaCapitalId[this]; }
            set { Fields.EmpresaTipologiaCapitalId[this] = value; }
        }

        [DisplayName("Tipol. Capital"), Expression("jTipologiaCapital.[Tipologia_Capital]")]
        public String TipologiaCapitalCapital
        {
            get { return Fields.TipologiaCapitalCapital[this]; }
            set { Fields.TipologiaCapitalCapital[this] = value; }
        }

        [DisplayName("Tipo Garantia Id"), Expression("jEmpresa.[Tipo_Garantia_TasaId]"), ForeignKey("tipos_garantia_tasas", "Tipo_Garantia_TasaId"), LeftJoin("jTipoGarantiaTasa"), TextualField("TipoGarantiaTasaGarantiaTasa"), LookupInclude, QuickSearch]
        [LookupEditor(typeof(Entities.TiposGarantiaTasasRow))]
        public Int32? EmpresaTipoGarantiaTasaId
        {
            get { return Fields.EmpresaTipoGarantiaTasaId[this]; }
            set { Fields.EmpresaTipoGarantiaTasaId[this] = value; }
        }


        [DisplayName("Garantia Tasa"), Expression("jTipoGarantiaTasa.[Tipo_Garantia_Tasa]")]
        public String TipoGarantiaTasaGarantiaTasa
        {
            get { return Fields.TipoGarantiaTasaGarantiaTasa[this]; }
            set { Fields.TipoGarantiaTasaGarantiaTasa[this] = value; }
        }

        [DisplayName("Emp. Trasp."), Expression("jEmpresa.[Empleo_Traspasado]")]
        public Int32? EmpresaEmpleoTraspasado
        {
            get { return Fields.EmpresaEmpleoTraspasado[this]; }
            set { Fields.EmpresaEmpleoTraspasado[this] = value; }
        }

        [DisplayName("Emp. 6 Meses"), Expression("jEmpresa.[Empleo_6_meses]")]
        public Int32? EmpresaEmpleo6Meses
        {
            get { return Fields.EmpresaEmpleo6Meses[this]; }
            set { Fields.EmpresaEmpleo6Meses[this] = value; }
        }

        [DisplayName("Emp. Medio"), Expression("jEmpresa.[Empleo_promedio]")]
        public Int32? EmpresaEmpleoPromedio
        {
            get { return Fields.EmpresaEmpleoPromedio[this]; }
            set { Fields.EmpresaEmpleoPromedio[this] = value; }
        }

        [DisplayName("Emp. 2 Años"), Expression("jEmpresa.[Empleo_promedio_2_anos]")]
        public Int32? EmpresaEmpleoPromedio2Anos
        {
            get { return Fields.EmpresaEmpleoPromedio2Anos[this]; }
            set { Fields.EmpresaEmpleoPromedio2Anos[this] = value; }
        }

        [DisplayName("Inv. Traspasada"), Expression("jEmpresa.[Inversion_traspasada]")]
        public Decimal? EmpresaInversionTraspasada
        {
            get { return Fields.EmpresaInversionTraspasada[this]; }
            set { Fields.EmpresaInversionTraspasada[this] = value; }
        }

        [DisplayName("Inv. 2 Años"), Expression("jEmpresa.[Inversion_2_anos]")]
        public Decimal? EmpresaInversion2Anos
        {
            get { return Fields.EmpresaInversion2Anos[this]; }
            set { Fields.EmpresaInversion2Anos[this] = value; }
        }

        [DisplayName("Estado Emp."), Expression("jEmpresa.[EstadoEmpresaId]"), ForeignKey("estados_empresa", "EstadoEmpresaId"), LeftJoin("jEstadoEmpresa"), TextualField("EstadoEmpresaEstado"), LookupInclude]
        [LookupEditor(typeof(Entities.EstadosEmpresaRow))]
        public Int32? EmpresaEstadoEmpresaId
        {
            get { return Fields.EmpresaEstadoEmpresaId[this]; }
            set { Fields.EmpresaEstadoEmpresaId[this] = value; }
        }

        [DisplayName("Estado"), Expression("jEstadoEmpresa.[Estado]")]
        public String EstadoEmpresaEstado
        {
            get { return Fields.EstadoEmpresaEstado[this]; }
            set { Fields.EstadoEmpresaEstado[this] = value; }
        }

        [DisplayName("Empresa Num Tasa Liquidacion"), Expression("jEmpresa.[Num_Tasa_Liquidacion]")]
        public String EmpresaNumTasaLiquidacion
        {
            get { return Fields.EmpresaNumTasaLiquidacion[this]; }
            set { Fields.EmpresaNumTasaLiquidacion[this] = value; }
        }

        [DisplayName("Nombre"), Expression("jContacto.[Nombre]")]
        public String ContactoNombre
        {
            get { return Fields.ContactoNombre[this]; }
            set { Fields.ContactoNombre[this] = value; }
        }



        [DisplayName("Contacto Nif"), Expression("jContacto.[Nif]")]
        public String ContactoNif
        {
            get { return Fields.ContactoNif[this]; }
            set { Fields.ContactoNif[this] = value; }
        }

        [DisplayName("Contacto Telefono Fijo"), Expression("jContacto.[Telefono_fijo]")]
        public String ContactoTelefonoFijo
        {
            get { return Fields.ContactoTelefonoFijo[this]; }
            set { Fields.ContactoTelefonoFijo[this] = value; }
        }

        [DisplayName("Contacto Movil"), Expression("jContacto.[Movil]")]
        public String ContactoMovil
        {
            get { return Fields.ContactoMovil[this]; }
            set { Fields.ContactoMovil[this] = value; }
        }

        [DisplayName("Contacto Idioma Id"), Expression("jContacto.[IdiomaId]")]
        public Int32? ContactoIdiomaId
        {
            get { return Fields.ContactoIdiomaId[this]; }
            set { Fields.ContactoIdiomaId[this] = value; }
        }

        [DisplayName("Email"), Expression("jContacto.[Email]")]
        public String ContactoEmail
        {
            get { return Fields.ContactoEmail[this]; }
            set { Fields.ContactoEmail[this] = value; }
        }

        [DisplayName("Tipo de Contacto"), Expression("jTipoContacto.[Tipo_Contacto]")]
        public String TipoContactoContacto
        {
            get { return Fields.TipoContactoContacto[this]; }
            set { Fields.TipoContactoContacto[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/
        IIdField IIdRow.IdField
        {
            get { return Fields.EmpresaContactoId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.EmpresaRazon; }
        }
        public static readonly RowFields Fields = new RowFields().Init();

        public EmpresasContactosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field EmpresaContactoId;
            public Int32Field EmpresaId;
            public Int32Field ContactoId;
            public Int32Field TipoContactoId;
            public DecimalField Porcentaje;
            public BooleanField Residente;
            public BooleanField Mancomunado;
            public BooleanField Solidario;
            public DateTimeField FechaBaja;
            public DateTimeField FechaAlta;

            public StringField EmpresaRazon;
            public Int32Field EmpresaFormaJuridicaId;
            public StringField EmpresaNExpediente;
            public Int32Field EmpresaTecnicoId;
            public StringField EmpresaCif;
            public StringField EmpresaDireccion;
            public StringField EmpresaPoblacion;
            public Int32Field EmpresaIslaId;
            public StringField EmpresaTelefonoFijo;
            public StringField EmpresaMovil;
            public StringField EmpresaEmail;
            public Int32Field EmpresaProyectoId;
            public StringField EmpresaExpediente;
            public StringField EmpresaMotivoExencion;
            public Int32Field EmpresaTipologiaCapitalId;
            public Int32Field EmpresaTipoGarantiaTasaId;
            public Int32Field EmpresaEmpleoTraspasado;
            public Int32Field EmpresaEmpleo6Meses;
            public Int32Field EmpresaEmpleoPromedio;
            public Int32Field EmpresaEmpleoPromedio2Anos;
            public DecimalField EmpresaInversionTraspasada;
            public DecimalField EmpresaInversion2Anos;
            public Int32Field EmpresaEstadoEmpresaId;
            public StringField EmpresaNumTasaLiquidacion;
            public StringField EstadoEmpresaEstado;


            public StringField ContactoNombre;
//            public StringField ContactoApellidos;
            public StringField ContactoNif;
            public StringField ContactoTelefonoFijo;
            public StringField ContactoMovil;
            public Int32Field ContactoIdiomaId;
            public StringField ContactoEmail;

            public StringField TipoContactoContacto;
            public StringField TecnicoNombreTecnico;
            public StringField IslaNombreIsla;
            public StringField TipologiaCapitalCapital;
            public StringField TipoGarantiaTasaGarantiaTasa;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
