﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasContactosEditDialog extends Common.GridEditorDialog<EmpresasContactosRow> {
        protected getFormKey() { return EmpresasContactosForm.formKey; }
        protected getNameProperty() { return EmpresasContactosRow.nameProperty; }
        protected getLocalTextPrefix() { return EmpresasContactosRow.localTextPrefix; }

        protected form: EmpresasContactosForm;

        constructor() {
            super();
            this.form = new EmpresasContactosForm(this.idPrefix);
        }
    }
}
