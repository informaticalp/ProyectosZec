﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class EmpresasContactosEditor extends Common.GridEditorBase<EmpresasContactosRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.EmpresasContactos"; }
        protected getDialogType() { return EmpresasContactosEditDialog; }
        protected getLocalTextPrefix() { return EmpresasContactosRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Contacto";
        }
    }
}
