﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class SubsectoresGrid extends Serenity.EntityGrid<SubsectoresRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.Subsectores'; }
        protected getDialogType() { return SubsectoresDialog; }
        protected getIdProperty() { return SubsectoresRow.idProperty; }
        protected getInsertPermission() { return SubsectoresRow.insertPermission; }
        protected getLocalTextPrefix() { return SubsectoresRow.localTextPrefix; }
        protected getService() { return SubsectoresService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        // Añadidos
        // Primero campo de ordenación por defecto
        // No olvidarse Cambiar el Row y el Id
        protected getDefaultSortBy() {
            return [SubsectoresRow.Fields.SectorId];
        }

        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Nuevo_Roezec/Subsectores/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            return buttons;
            // Fin añadidos

        }
    }
}