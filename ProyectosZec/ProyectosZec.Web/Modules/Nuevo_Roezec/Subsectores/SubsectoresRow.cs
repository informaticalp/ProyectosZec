﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("subsectores")]
    [DisplayName("Subsectores"), InstanceName("Subsectores")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.Subsectores")]
    public sealed class SubsectoresRow : Row, IIdRow, INameRow
    {
        [DisplayName("Subsector Id"), Identity]
        public Int32? SubsectorId
        {
            get { return Fields.SubsectorId[this]; }
            set { Fields.SubsectorId[this] = value; }
        }

        [DisplayName("SectorId"), NotNull, ForeignKey("sectores", "SectorId"), LeftJoin("jSector"), TextualField("Sector"),LookupInclude]
        [LookupEditor("Nuevo_Roezec.Sectores")]
        public Int32? SectorId
        {
            get { return Fields.SectorId[this]; }
            set { Fields.SectorId[this] = value; }
        }

        [DisplayName("Subsector"), Size(200), NotNull, QuickSearch]
        public String Subsector
        {
            get { return Fields.Subsector[this]; }
            set { Fields.Subsector[this] = value; }
        }

        [DisplayName("Sector"), Expression("jSector.[Sector]")]
        public String Sector
        {
            get { return Fields.Sector[this]; }
            set { Fields.Sector[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/
        IIdField IIdRow.IdField
        {
            get { return Fields.SubsectorId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Subsector; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public SubsectoresRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field SubsectorId;
            public Int32Field SectorId;
            public StringField Subsector;

            public StringField Sector;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
