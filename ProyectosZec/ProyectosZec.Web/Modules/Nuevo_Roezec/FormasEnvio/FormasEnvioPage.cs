﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/FormasEnvio"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.FormasEnvioRow))]
    public class FormasEnvioController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/FormasEnvio/FormasEnvioIndex.cshtml");
        }
    }
}