﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class FormasEnvioDialog extends Serenity.EntityDialog<FormasEnvioRow, any> {
        protected getFormKey() { return FormasEnvioForm.formKey; }
        protected getIdProperty() { return FormasEnvioRow.idProperty; }
        protected getLocalTextPrefix() { return FormasEnvioRow.localTextPrefix; }
        protected getNameProperty() { return FormasEnvioRow.nameProperty; }
        protected getService() { return FormasEnvioService.baseUrl; }
        protected getDeletePermission() { return FormasEnvioRow.deletePermission; }
        protected getInsertPermission() { return FormasEnvioRow.insertPermission; }
        protected getUpdatePermission() { return FormasEnvioRow.updatePermission; }

        protected form = new FormasEnvioForm(this.idPrefix);

    }
}