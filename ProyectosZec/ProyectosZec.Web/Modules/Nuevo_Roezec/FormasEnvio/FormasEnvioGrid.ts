﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class FormasEnvioGrid extends Serenity.EntityGrid<FormasEnvioRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.FormasEnvio'; }
        protected getDialogType() { return FormasEnvioDialog; }
        protected getIdProperty() { return FormasEnvioRow.idProperty; }
        protected getInsertPermission() { return FormasEnvioRow.insertPermission; }
        protected getLocalTextPrefix() { return FormasEnvioRow.localTextPrefix; }
        protected getService() { return FormasEnvioService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}