﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.FormasEnvio")]
    [BasedOnRow(typeof(Entities.FormasEnvioRow), CheckNames = true)]
    public class FormasEnvioForm
    {
        public String Forma { get; set; }
    }
}