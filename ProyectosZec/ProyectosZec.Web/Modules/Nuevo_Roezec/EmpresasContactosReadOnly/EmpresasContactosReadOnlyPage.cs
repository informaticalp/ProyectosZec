﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/EmpresasContactosReadOnly"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EmpresasContactosRow))]
    public class EmpresasContactosReadOnlyController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/EmpresasContactosReadOnly/EmpresasContactosReadOnlyIndex.cshtml");
        }
    }
}