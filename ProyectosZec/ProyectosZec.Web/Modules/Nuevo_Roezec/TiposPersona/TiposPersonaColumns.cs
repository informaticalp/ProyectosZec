﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.TiposPersona")]
    [BasedOnRow(typeof(Entities.TiposPersonaRow), CheckNames = true)]
    public class TiposPersonaColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 TipoPersonaId { get; set; }
        [EditLink]
        public String TipoPersona { get; set; }
    }
}