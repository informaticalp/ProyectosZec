﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("tipos_persona")]
    [DisplayName("Tipos Persona"), InstanceName("Tipos Persona")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.TiposPersona")]

    public sealed class TiposPersonaRow : Row, IIdRow, INameRow
    {
        [DisplayName("Tipo Persona Id"), Identity]
        public Int32? TipoPersonaId
        {
            get { return Fields.TipoPersonaId[this]; }
            set { Fields.TipoPersonaId[this] = value; }
        }

        [DisplayName("Tipo Persona"), Size(50), QuickSearch]
        public String TipoPersona
        {
            get { return Fields.TipoPersona[this]; }
            set { Fields.TipoPersona[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.TipoPersonaId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.TipoPersona; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public TiposPersonaRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field TipoPersonaId;
            public StringField TipoPersona;
        }
    }
}
