﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposPersonaGrid extends Serenity.EntityGrid<TiposPersonaRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.TiposPersona'; }
        protected getDialogType() { return TiposPersonaDialog; }
        protected getIdProperty() { return TiposPersonaRow.idProperty; }
        protected getInsertPermission() { return TiposPersonaRow.insertPermission; }
        protected getLocalTextPrefix() { return TiposPersonaRow.localTextPrefix; }
        protected getService() { return TiposPersonaService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}