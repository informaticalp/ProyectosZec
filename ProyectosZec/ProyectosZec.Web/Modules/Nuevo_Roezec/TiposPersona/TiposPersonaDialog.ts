﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposPersonaDialog extends Serenity.EntityDialog<TiposPersonaRow, any> {
        protected getFormKey() { return TiposPersonaForm.formKey; }
        protected getIdProperty() { return TiposPersonaRow.idProperty; }
        protected getLocalTextPrefix() { return TiposPersonaRow.localTextPrefix; }
        protected getNameProperty() { return TiposPersonaRow.nameProperty; }
        protected getService() { return TiposPersonaService.baseUrl; }
        protected getDeletePermission() { return TiposPersonaRow.deletePermission; }
        protected getInsertPermission() { return TiposPersonaRow.insertPermission; }
        protected getUpdatePermission() { return TiposPersonaRow.updatePermission; }

        protected form = new TiposPersonaForm(this.idPrefix);

    }
}