﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/TiposPersona"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.TiposPersonaRow))]
    public class TiposPersonaController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/TiposPersona/TiposPersonaIndex.cshtml");
        }
    }
}