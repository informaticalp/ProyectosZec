﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("tecnicos")]
    [DisplayName("Técnicos"), InstanceName("Tecnicos")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.Tecnicos")]
    public sealed class TecnicosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Tecnico Id"), Identity]
        public Int32? TecnicoId
        {
            get { return Fields.TecnicoId[this]; }
            set { Fields.TecnicoId[this] = value; }
        }

        [DisplayName("Nombre del Técnico"), Size(30), NotNull, QuickSearch]
        public String NombreTecnico
        {
            get { return Fields.NombreTecnico[this]; }
            set { Fields.NombreTecnico[this] = value; }
        }

        [DisplayName("Abreviatura"), Size(4), NotNull]
        public String Tecnico
        {
            get { return Fields.Tecnico[this]; }
            set { Fields.Tecnico[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/
        IIdField IIdRow.IdField
        {
            get { return Fields.TecnicoId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.NombreTecnico; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public TecnicosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field TecnicoId;
            public StringField NombreTecnico;
            public StringField Tecnico;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
