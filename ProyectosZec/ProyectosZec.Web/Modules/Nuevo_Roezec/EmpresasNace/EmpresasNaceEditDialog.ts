﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasNaceEditDialog extends Common.GridEditorDialog<EmpresasNaceRow> {
        protected getFormKey() { return EmpresasNaceForm.formKey; }
        protected getNameProperty() { return EmpresasNaceRow.nameProperty; }
        protected getLocalTextPrefix() { return EmpresasNaceRow.localTextPrefix; }

        protected form: EmpresasNaceForm;

        constructor() {
            super();
            this.form = new EmpresasNaceForm(this.idPrefix);
        }
    }
}