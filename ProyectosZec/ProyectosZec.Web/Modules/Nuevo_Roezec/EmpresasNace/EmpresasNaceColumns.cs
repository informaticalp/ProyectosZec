﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.EmpresasNace")]
    [BasedOnRow(typeof(Entities.EmpresasNaceRow), CheckNames = true)]
    public class EmpresasNaceColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight, Width(60)]
        public Int32 EmpresaNaceId { get; set; }

        [Width(100)]
        public String Version { get; set; }
        [Width(70),EditLink]
        public String NaceCodigo { get; set; }

        [Width(600),EditLink]
        public String NaceDescripcion { get; set; }
        [Width(120), DisplayFormat("d")]
        public DateTime FechaValidez { get; set; }
        [Width(100),AlignCenter]
        public Boolean NacePrincipal { get; set; }
        [Width(100)]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }

    }
}