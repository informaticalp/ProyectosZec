﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("empresas_nace")]
    [DisplayName("Empresas Nace"), InstanceName("Empresas Nace")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    public sealed class EmpresasNaceRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? EmpresaNaceId
        {
            get { return Fields.EmpresaNaceId[this]; }
            set { Fields.EmpresaNaceId[this] = value; }
        }

        [DisplayName("Empresa"), NotNull, ForeignKey("empresas", "EmpresaId"), LeftJoin("jEmpresa"), TextualField("EmpresaRazon")]
        public Int32? EmpresaId
        {
            get { return Fields.EmpresaId[this]; }
            set { Fields.EmpresaId[this] = value; }
        }

        [DisplayName("Principal"), Column("NacePrincipal")]
        public Boolean? NacePrincipal
        {
            get { return Fields.NacePrincipal[this]; }
            set { Fields.NacePrincipal[this] = value; }
        }
        [DisplayName("Fecha Validez"),Column("FechaValidez")]
        public DateTime? FechaValidez
        {
            get { return Fields.FechaValidez[this]; }
            set { Fields.FechaValidez[this] = value; }
        }

        [DisplayName("Nace"), NotNull, ForeignKey("naces", "NaceId"), LeftJoin("jNace"), TextualField("NaceDescripcion"),QuickSearch]
        [LookupEditor(typeof(Entities.NacesRow))]
        public Int32? NaceId
        {
            get { return Fields.NaceId[this]; }
            set { Fields.NaceId[this] = value; }
        }

        [DisplayName("Empresa Razon"), Expression("jEmpresa.[Razon]"), QuickSearch]
        public String EmpresaRazon
        {
            get { return Fields.EmpresaRazon[this]; }
            set { Fields.EmpresaRazon[this] = value; }
        }

        [DisplayName("Empresa Forma Juridica Id"), Expression("jEmpresa.[FormaJuridicaId]")]
        public Int32? EmpresaFormaJuridicaId
        {
            get { return Fields.EmpresaFormaJuridicaId[this]; }
            set { Fields.EmpresaFormaJuridicaId[this] = value; }
        }

        [DisplayName("Empresa N Expediente"), Expression("jEmpresa.[Expediente]")]
        public String EmpresaNExpediente
        {
            get { return Fields.EmpresaNExpediente[this]; }
            set { Fields.EmpresaNExpediente[this] = value; }
        }

        [DisplayName("Empresa Tecnico Id"), Expression("jEmpresa.[TecnicoId]"), ForeignKey("tecnicos", "TecnicoId"), LeftJoin("jTecnico"), LookupInclude, TextualField("TecnicoNombreTecnico")]
        [LookupEditor(typeof(Entities.TecnicosRow))]
        public Int32? EmpresaTecnicoId
        {
            get { return Fields.EmpresaTecnicoId[this]; }
            set { Fields.EmpresaTecnicoId[this] = value; }
        }

        [DisplayName("Tecnico"), Expression("jTecnico.[NombreTecnico]")]
        public String TecnicoNombreTecnico
        {
            get { return Fields.TecnicoNombreTecnico[this]; }
            set { Fields.TecnicoNombreTecnico[this] = value; }
        }

        [DisplayName("Empresa Cif"), Expression("jEmpresa.[Cif]")]
        public String EmpresaCif
        {
            get { return Fields.EmpresaCif[this]; }
            set { Fields.EmpresaCif[this] = value; }
        }

        [DisplayName("Empresa Direccion"), Expression("jEmpresa.[Direccion]")]
        public String EmpresaDireccion
        {
            get { return Fields.EmpresaDireccion[this]; }
            set { Fields.EmpresaDireccion[this] = value; }
        }

        [DisplayName("Empresa Poblacion"), Expression("jEmpresa.[Poblacion]")]
        public String EmpresaPoblacion
        {
            get { return Fields.EmpresaPoblacion[this]; }
            set { Fields.EmpresaPoblacion[this] = value; }
        }

        [DisplayName("Empresa Isla Id"), Expression("jEmpresa.[IslaId]"), ForeignKey("islas", "IslaId"), LeftJoin("jIsla"), TextualField("IslaNombreIsla"), LookupInclude]
        [LookupEditor(typeof(Entities.IslasRow))]
        public Int32? EmpresaIslaId
        {
            get { return Fields.EmpresaIslaId[this]; }
            set { Fields.EmpresaIslaId[this] = value; }
        }
        [DisplayName("Isla"), Expression("jIsla.[NombreIsla]")]
        public String IslaNombreIsla
        {
            get { return Fields.IslaNombreIsla[this]; }
            set { Fields.IslaNombreIsla[this] = value; }
        }

        [DisplayName("Empresa Telefono Fijo"), Expression("jEmpresa.[Telefono_fijo]")]
        public String EmpresaTelefonoFijo
        {
            get { return Fields.EmpresaTelefonoFijo[this]; }
            set { Fields.EmpresaTelefonoFijo[this] = value; }
        }

        [DisplayName("Empresa Movil"), Expression("jEmpresa.[Movil]")]
        public String EmpresaMovil
        {
            get { return Fields.EmpresaMovil[this]; }
            set { Fields.EmpresaMovil[this] = value; }
        }

        [DisplayName("Empresa Email"), Expression("jEmpresa.[Email]")]
        public String EmpresaEmail
        {
            get { return Fields.EmpresaEmail[this]; }
            set { Fields.EmpresaEmail[this] = value; }
        }

        [DisplayName("Empresa Proyecto Id"), Expression("jEmpresa.[ProyectoId]")]
        public Int32? EmpresaProyectoId
        {
            get { return Fields.EmpresaProyectoId[this]; }
            set { Fields.EmpresaProyectoId[this] = value; }
        }

        [DisplayName("Empresa Expediente"), Expression("jEmpresa.[Expediente]")]
        public String EmpresaExpediente
        {
            get { return Fields.EmpresaExpediente[this]; }
            set { Fields.EmpresaExpediente[this] = value; }
        }

        [DisplayName("Empresa Motivo Exencion"), Expression("jEmpresa.[Motivo_Exencion]")]
        public String EmpresaMotivoExencion
        {
            get { return Fields.EmpresaMotivoExencion[this]; }
            set { Fields.EmpresaMotivoExencion[this] = value; }
        }

        [DisplayName("Tipologia Capital Id"), Expression("jEmpresa.[Tipologia_CapitalId]"), ForeignKey("tipologias_capital", "Tipologia_CapitalId"), LeftJoin("jTipologiaCapital"), TextualField("TipologiaCapitalCapital"), LookupInclude, QuickSearch]
        [LookupEditor(typeof(Entities.TipologiasCapitalRow))]
        public Int32? EmpresaTipologiaCapitalId
        {
            get { return Fields.EmpresaTipologiaCapitalId[this]; }
            set { Fields.EmpresaTipologiaCapitalId[this] = value; }
        }

        [DisplayName("Tipol. Capital"), Expression("jTipologiaCapital.[Tipologia_Capital]")]
        public String TipologiaCapitalCapital
        {
            get { return Fields.TipologiaCapitalCapital[this]; }
            set { Fields.TipologiaCapitalCapital[this] = value; }
        }

        [DisplayName("Tipo Garantia Id"), Expression("jEmpresa.[Tipo_Garantia_TasaId]"), ForeignKey("tipos_garantia_tasas", "Tipo_Garantia_TasaId"), LeftJoin("jTipoGarantiaTasa"), TextualField("TipoGarantiaTasaGarantiaTasa"), LookupInclude, QuickSearch]
        [LookupEditor(typeof(Entities.TiposGarantiaTasasRow))]
        public Int32? EmpresaTipoGarantiaTasaId
        {
            get { return Fields.EmpresaTipoGarantiaTasaId[this]; }
            set { Fields.EmpresaTipoGarantiaTasaId[this] = value; }
        }


        [DisplayName("Garantia Tasa"), Expression("jTipoGarantiaTasa.[Tipo_Garantia_Tasa]")]
        public String TipoGarantiaTasaGarantiaTasa
        {
            get { return Fields.TipoGarantiaTasaGarantiaTasa[this]; }
            set { Fields.TipoGarantiaTasaGarantiaTasa[this] = value; }
        }

        [DisplayName("Empresa Empleo Traspasado"), Expression("jEmpresa.[Empleo_Traspasado]")]
        public Int32? EmpresaEmpleoTraspasado
        {
            get { return Fields.EmpresaEmpleoTraspasado[this]; }
            set { Fields.EmpresaEmpleoTraspasado[this] = value; }
        }

        [DisplayName("Empresa Empleo 6 Meses"), Expression("jEmpresa.[Empleo_6_meses]")]
        public Int32? EmpresaEmpleo6Meses
        {
            get { return Fields.EmpresaEmpleo6Meses[this]; }
            set { Fields.EmpresaEmpleo6Meses[this] = value; }
        }

        [DisplayName("Empresa Empleo Promedio"), Expression("jEmpresa.[Empleo_promedio]")]
        public Int32? EmpresaEmpleoPromedio
        {
            get { return Fields.EmpresaEmpleoPromedio[this]; }
            set { Fields.EmpresaEmpleoPromedio[this] = value; }
        }

        [DisplayName("Empresa Empleo Promedio 2 Anos"), Expression("jEmpresa.[Empleo_promedio_2_anos]")]
        public Int32? EmpresaEmpleoPromedio2Anos
        {
            get { return Fields.EmpresaEmpleoPromedio2Anos[this]; }
            set { Fields.EmpresaEmpleoPromedio2Anos[this] = value; }
        }

        [DisplayName("Empresa Inversion Traspasada"), Expression("jEmpresa.[Inversion_traspasada]")]
        public Decimal? EmpresaInversionTraspasada
        {
            get { return Fields.EmpresaInversionTraspasada[this]; }
            set { Fields.EmpresaInversionTraspasada[this] = value; }
        }

        [DisplayName("Empresa Inversion 2 Anos"), Expression("jEmpresa.[Inversion_2_anos]")]
        public Decimal? EmpresaInversion2Anos
        {
            get { return Fields.EmpresaInversion2Anos[this]; }
            set { Fields.EmpresaInversion2Anos[this] = value; }
        }

        [DisplayName("EstadoId"), Expression("jEmpresa.[EstadoEmpresaId]"), ForeignKey("estados_empresa", "EstadoEmpresaId"), LeftJoin("jEstadoEmpresa"), TextualField("EstadoEmpresaEstado"), LookupInclude]
        [LookupEditor(typeof(Entities.EstadosEmpresaRow))]
        public Int32? EmpresaEstadoEmpresaId
        {
            get { return Fields.EmpresaEstadoEmpresaId[this]; }
            set { Fields.EmpresaEstadoEmpresaId[this] = value; }
        }

        [DisplayName("Estado"), Expression("jEstadoEmpresa.[Estado]")]
        public String EstadoEmpresaEstado
        {
            get { return Fields.EstadoEmpresaEstado[this]; }
            set { Fields.EstadoEmpresaEstado[this] = value; }
        }


        [DisplayName("Empresa Num Tasa Liquidacion"), Expression("jEmpresa.[Num_Tasa_Liquidacion]")]
        public String EmpresaNumTasaLiquidacion
        {
            get { return Fields.EmpresaNumTasaLiquidacion[this]; }
            set { Fields.EmpresaNumTasaLiquidacion[this] = value; }
        }

        [DisplayName("VersionId"), Expression("jNace.[VersionId"), ForeignKey("versiones_nace", "VersionId"), LeftJoin("jVersion"),TextualField("Version")]
        [LookupEditor(typeof(Entities.VersionesNaceRow))]
        public Int32? VersionId
        {
            get { return Fields.VersionId[this]; }
            set { Fields.VersionId[this] = value; }
        }

       [DisplayName("Revisión"), Expression("(select Version FROM versiones_nace WHERE VersionId=(select MAX(VersionId)  FROM naces a Where T0.[NaceId]=NaceId))")]
       //[DisplayName("Revisón"), Expression("jVersion.[Version]")]
        public String Version
        {
            get { return Fields.Version[this]; }
            set { Fields.Version[this] = value; }
        }

        [DisplayName("Codigo"), Expression("jNace.[Codigo]"),QuickSearch]
        public String NaceCodigo
        {
            get { return Fields.NaceCodigo[this]; }
            set { Fields.NaceCodigo[this] = value; }
        }

        [DisplayName("Nace"), Expression("jNace.[Descripcion]"), QuickSearch]
        public String NaceDescripcion
        {
            get { return Fields.NaceDescripcion[this]; }
            set { Fields.NaceDescripcion[this] = value; }
        }

        [DisplayName("Nace Larga")]
        //[Expression("jNace.[Descripcion]")]
        [Expression("(CONCAT(jNace.[Descripcion],' (',jNace.[Codigo],' ',jVersion.[Version],')'))")]
        //[Expression("(jNace.Descripcion || ' (' || jNace.Codigo || ' ' || jVersion.Version || ')')", Dialect = "Sqlite")]

        public String NaceLarga
        {
            get { return Fields.NaceLarga[this]; }
            set { Fields.NaceLarga[this] = value; }
        }

        [DisplayName("Subsector Id"), Expression("jNace.[SubsectorId]"), ForeignKey("Subsectores","SubsectorId"),LeftJoin("jSubsector"), TextualField("Subsector"),LookupInclude]
        [LookupEditor(typeof(Entities.SubsectoresRow), CascadeFrom = "SectorId", CascadeField = "SectorId")]
        public Int32? NaceSubsectorId
        {
            get { return Fields.NaceSubsectorId[this]; }
            set { Fields.NaceSubsectorId[this] = value; }
        }

        [DisplayName("Subsector"), Expression("jSubsector.[Subsector]")]
        public String Subsector
        {
            get { return Fields.Subsector[this]; }
            set { Fields.Subsector[this] = value; }
        }

        [DisplayName("Sector Id"), Expression("jSubsector.[SectorId]"), ForeignKey("sectores", "SectorId"), LeftJoin("jSector"), LookupInclude]
        [LookupEditor(typeof(Entities.SectoresRow))]
        public Int32? SectorId
        {
            get { return Fields.SectorId[this]; }
            set { Fields.SectorId[this] = value; }
        }

        [DisplayName("Sector"), Expression("jSector.[Sector]")]
        public String Sector
        {
            get { return Fields.Sector[this]; }
            set { Fields.Sector[this] = value; }
        }

        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/
        IIdField IIdRow.IdField
        {
            get { return Fields.EmpresaNaceId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.NaceDescripcion; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public EmpresasNaceRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field EmpresaNaceId;
            public Int32Field EmpresaId;
            public Int32Field NaceId;
            public BooleanField NacePrincipal;
            public DateTimeField FechaValidez;

            public StringField EmpresaRazon;
            public StringField NaceLarga;
            public Int32Field EmpresaFormaJuridicaId;
            public StringField EmpresaNExpediente;
            public Int32Field EmpresaTecnicoId;
            public StringField EmpresaCif;
            public StringField EmpresaDireccion;
            public StringField EmpresaPoblacion;
            public Int32Field EmpresaIslaId;
            public StringField EmpresaTelefonoFijo;
            public StringField EmpresaMovil;
            public StringField EmpresaEmail;
            public Int32Field EmpresaProyectoId;
            public StringField EmpresaExpediente;
            public StringField EmpresaMotivoExencion;
            public Int32Field EmpresaTipologiaCapitalId;
            public Int32Field EmpresaTipoGarantiaTasaId;
            public Int32Field EmpresaEmpleoTraspasado;
            public Int32Field EmpresaEmpleo6Meses;
            public Int32Field EmpresaEmpleoPromedio;
            public Int32Field EmpresaEmpleoPromedio2Anos;
            public DecimalField EmpresaInversionTraspasada;
            public DecimalField EmpresaInversion2Anos;
            public Int32Field EmpresaEstadoEmpresaId;

            public StringField TecnicoNombreTecnico;
            public StringField IslaNombreIsla;
            public StringField TipologiaCapitalCapital;
            public StringField TipoGarantiaTasaGarantiaTasa;
            public StringField EstadoEmpresaEstado;

            public StringField EmpresaNumTasaLiquidacion;
            
            public StringField NaceCodigo;
            public StringField NaceDescripcion;
            public Int32Field NaceSubsectorId;

            public Int32Field VersionId;
            public StringField Version;

            public StringField Subsector;
            public Int32Field SectorId;
            public StringField Sector;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
