﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class EmpresasNaceEditor extends Common.GridEditorBase<EmpresasNaceRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.EmpresasNace"; }
        protected getDialogType() { return EmpresasNaceEditDialog; }
        protected getLocalTextPrefix() { return EmpresasNaceRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }
        validateEntity(row, id) {
            row.EmpresaNaceId = Q.toId(row.EmpresaNaceId);
            if (row.NacePrincipal) {
                var sameProduct = Q.tryFirst(this.view.getItems(), x => x.NacePrincipal === row.NacePrincipal);
                if (sameProduct && this.id(sameProduct) !== id) {
                    Q.alert('Solo Puede haber un Nace Principal!');
                    return false;
                }
            }
            return true;
        }

        protected getAddButtonCaption() {
            return "Añadir Nace";
        }
    }
}