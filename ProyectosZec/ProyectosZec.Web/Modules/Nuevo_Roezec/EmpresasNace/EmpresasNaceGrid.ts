﻿
namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.filterable()
    @Serenity.Decorators.registerClass()
    export class EmpresasNaceGrid extends Serenity.EntityGrid<EmpresasNaceRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.EmpresasNace'; }
        protected getDialogType() { return EmpresasNaceDialog; }
        protected getIdProperty() { return EmpresasNaceRow.idProperty; }
        protected getInsertPermission() { return EmpresasNaceRow.insertPermission; }
        protected getLocalTextPrefix() { return EmpresasNaceRow.localTextPrefix; }
        protected getService() { return EmpresasNaceService.baseUrl; }


        constructor(container: JQuery) {
            super(container);
            //this.slickGrid.onSort.subscribe(function (e, args) {
            //    sortGridFunction((args.grid as Slick.Grid), args.sortCols[0], args.sortCols[0].sortCol.field);

            //    //(args.grid as Slick.Grid).init();
            //    (args.grid as Slick.Grid).invalidateAllRows();
            //    (args.grid as Slick.Grid).invalidate();
            //    (args.grid as Slick.Grid).render();
            //    (args.grid as Slick.Grid).resizeCanvas();
            //});
        }

        //protected layout() {
        //    super.layout();

        //    var sortCols = this.slickGrid.getSortColumns();

        //    sortGridFunction(this.slickGrid, sortCols[0], sortCols[0].columnId);
        //}

        //protected enableFiltering() {
        //    return true;
        //}
        //// Añadir la siguiente función
        //protected getDefaultSortBy() {
        //    return [EmpresasNaceRow.Fields.NaceCodigo]; // Este es el campo de ordenación por defecto
        //}

    }
}

//function sortGridFunction(grid: Slick.Grid, column: any, field: any) {
//    grid.getData().sort(function (a, b) {
//        var result = a[field] > b[field] ? 1 :
//            a[field] < b[field] ? -1 :
//                0;
//        return column.sortAsc ? result : -result;
//    });
//}
