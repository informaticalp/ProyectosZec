﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.MercadosReadOnly")]
    [BasedOnRow(typeof(Entities.EmpresasMercadosRow), CheckNames = true)]
    public class MercadosReadOnlyColumns
    {
    
        [Width(150)]
        public String EmpresaRazon { get; set; }
        [Width(120), QuickFilter]
        public String TecnicoNombreTecnico { get; set; }
        [Width(150), QuickFilter, QuickFilterOption("multiple", true)]
        public String EstadoEmpresaEstado { get; set; }
        [Hidden, Width(100)]
        public String EmpresaNExpediente { get; set; }
        [Hidden, Width(100)]
        public String EmpresaCif { get; set; }
        [Hidden, Width(120)]
        public String EmpresaDireccion { get; set; }
        [Hidden, Width(120)]
        public String EmpresaPoblacion { get; set; }
        [Hidden, Width(110)]
        public String TipologiaCapitalCapital { get; set; }
        [Hidden, Width(110)]
        public String TipoGarantiaTasaGarantiaTasa { get; set; }
        [Width(100), QuickFilter]
        public String IslaNombreIsla { get; set; }
        [Width(120), QuickFilter, QuickFilterOption("multiple", true)]
        public String Mercado { get; set; }
        [Hidden, Width(90), AlignRight]
        public Int32 EmpresaEmpleoTraspasado { get; set; }
        [Hidden, Width(90), AlignRight]
        public Int32 EmpresaEmpleo6Meses { get; set; }
        [Hidden, Width(90), AlignRight]
        public Int32 EmpresaEmpleoPromedio { get; set; }
        [Hidden, Width(90), AlignRight]
        public Int32 EmpresaEmpleoPromedio2Anos { get; set; }
        [Hidden, Width(90), DisplayFormat("#,##0.00"), AlignRight]
        public Decimal EmpresaInversionTraspasada { get; set; }
        [Hidden, Width(90), DisplayFormat("#,##0.00"), AlignRight]
        public Decimal EmpresaInversion2Anos { get; set; }
        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }

    }
}