﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.MercadosReadOnly")]
    [BasedOnRow(typeof(Entities.MercadosReadOnlyRow), CheckNames = true)]
    public class MercadosReadOnlyForm
    {
        public Int32 EmpresaId { get; set; }
        public Int32 MercadoId { get; set; }
    }
}