﻿/// <reference path="../EmpresasMercados/EmpresasMercadosGrid.ts" />


namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido

    export class MercadosReadOnlyGrid extends Nuevo_Roezec.EmpresasMercadosGrid {
        protected getColumnsKey() { return 'Nuevo_Roezec.MercadosReadOnly'; }

        protected getDialogType() { return MercadosReadOnlyDialog; }

        constructor(container: JQuery) {
            super(container);
        }

        /**
         * Removing add button from grid using its css class
         */
        protected getButtons(): Serenity.ToolButton[] {
            var buttons = super.getButtons();
            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Nuevo_Roezec/Mercados/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));
            return buttons;
        }

    }
}