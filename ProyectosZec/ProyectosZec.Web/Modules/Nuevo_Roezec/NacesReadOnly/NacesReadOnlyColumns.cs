﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.NacesReadOnly")]
    [BasedOnRow(typeof(Entities.EmpresasNaceRow), CheckNames = true)]
    public class NacesReadOnlyColumns
    {
        [DisplayName("Db.Shared.RecordId"), AlignRight, Width(60)]
        public Int32 EmpresaNaceId { get; set; }

        [Width(250),EditLink]
        public String EmpresaRazon { get; set; }
        [Width(120), QuickFilter]
        public String TecnicoNombreTecnico { get; set; }
        [Width(150), QuickFilter, QuickFilterOption("multiple", true)]
        public String EstadoEmpresaEstado { get; set; }
        [Hidden, Width(100)]
        public String EmpresaNExpediente { get; set; }
        [Hidden, Width(100)]
        public String EmpresaCif { get; set; }

        [Width(100), QuickFilter]
        public String IslaNombreIsla { get; set; }
        [Width(100)]
        public String Version { get; set; }
        [Width(80)]
        public String NaceCodigo { get; set; }
        [Width(250)]
        public String NaceDescripcion { get; set; }
        [Width(100), AlignCenter, QuickFilter]
        public Boolean NacePrincipal { get; set; }
        [Width(90), DisplayName("Sector"), QuickFilter]
        public String Sector { get; set; }
        [Width(400), DisplayName("SubSector"), QuickFilter, QuickFilterOption("CascadeFrom", "SectorId"), QuickFilterOption("multiple", true)]
        public String Subsector { get; set; }

        //[Hidden, Width(100)]
        //public String UserName { get; set; }
        //[Hidden, Width(130), DisplayFormat("g")]
        //public DateTime FechaModificacion { get; set; }

    }
}