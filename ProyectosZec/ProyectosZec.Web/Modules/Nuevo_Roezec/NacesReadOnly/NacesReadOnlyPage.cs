﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/NacesReadOnly"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EmpresasNaceRow))]
    public class NacesReadOnlyController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/NacesReadOnly/NacesReadOnlyIndex.cshtml");
        }
    }
}