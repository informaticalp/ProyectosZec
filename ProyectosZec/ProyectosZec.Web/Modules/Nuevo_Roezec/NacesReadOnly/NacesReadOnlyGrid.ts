﻿/// <reference path="../EmpresasNace/EmpresasNaceGrid.ts" />


namespace ProyectosZec.Nuevo_Roezec {
    import fld = EmpresasNaceRow.Fields;
    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido

    export class NacesReadOnlyGrid extends Nuevo_Roezec.EmpresasNaceGrid {
        protected getColumnsKey() { return 'Nuevo_Roezec.NacesReadOnly'; }

        protected getDialogType() { return NacesReadOnlyDialog; }

        constructor(container: JQuery) {
            super(container);
        }

        /**
         * Removing add button from grid using its css class
         */
        protected getButtons(): Serenity.ToolButton[] {
            var buttons = super.getButtons();
            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Nuevo_Roezec/EmpresasNace/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));
            return buttons;
        }
        /**
* We override getColumns() to change format functions for some columns.
* You could also write them as formatter classes, and use them at server side
*/
        protected getColumns(): Slick.Column[] {
            var columns = super.getColumns();

            Q.first(columns, x => x.field == fld.EmpresaRazon).format =
                ctx => `<a href="javascript:;" class="empresa-link">${Q.htmlEncode(ctx.value)}</a>`;

            return columns;
        }
        protected onClick(e: JQueryEventObject, row: number, cell: number): void {

            // let base grid handle clicks for its edit links
            super.onClick(e, row, cell);

            // if base grid already handled, we shouldn"t handle it again
            if (e.isDefaultPrevented()) {
                return;
            }

            // get reference to current item
            var item = this.itemAt(row);

            // get reference to clicked element
            var target = $(e.target);



            if (target.hasClass("empresa-link")) {
                e.preventDefault();
                new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                //    let message = Q.format(
                //        "<p>Has pulsado sobre la la empresa {0}.</p>" +
                //        "<p>Si pulsas sobre Si, abrimos la empresa.</p>" +
                //        "<p>Si pulsas NO, abrimos la alarma.</p>",
                //        Q.htmlEncode(item.EmpresaRazon));

                //    Q.confirm(message, () => {
                //        new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                //    },
                //        {
                //            htmlEncode: false,
                //            onNo: () => {
                //                new Nuevo_Roezec.AlarmasDialog().loadByIdAndOpenDialog(item.AlarmaId);
                //            }
                //        });
            }
        }


    }
}