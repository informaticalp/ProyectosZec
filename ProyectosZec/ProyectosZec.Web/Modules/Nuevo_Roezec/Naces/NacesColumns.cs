﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Naces")]
    [BasedOnRow(typeof(Entities.NacesRow), CheckNames = true)]
    public class NacesColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Width(50)]
        public Int32 NaceId { get; set; }
        [EditLink,Width(80),QuickFilter]
        public String Version { get; set; }
        [Width(90)]
        public String Codigo { get; set; }
        [Width(480)]
        public String Descripcion { get; set; }
        [Width(90), DisplayName("Sector"), QuickFilter]
        public String Sector { get; set; }
        [Width(480), DisplayName("SubSector"), QuickFilter, QuickFilterOption("CascadeFrom", "SectorId"), QuickFilterOption("multiple", true)]
        public String Subsector { get; set; }
        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}