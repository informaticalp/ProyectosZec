﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("naces")]
    [DisplayName("Naces"), InstanceName("Naces")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.Naces")]
    public sealed class NacesRow : Row, IIdRow, INameRow
    {
        [DisplayName("Nace Id"), Identity]
        public Int32? NaceId
        {
            get { return Fields.NaceId[this]; }
            set { Fields.NaceId[this] = value; }
        }

        [DisplayName("Version"), ForeignKey("versiones_nace", "VersionId"), LeftJoin("jVersion"), TextualField("Version"), LookupInclude]
        [LookupEditor(typeof(Entities.VersionesNaceRow), InplaceAdd = true)]
        public Int32? VersionId
        {
            get { return Fields.VersionId[this]; }
            set { Fields.VersionId[this] = value; }
        }

        [DisplayName("Version"), Expression("jVersion.[Version]")]
        public String Version
        {
            get { return Fields.Version[this]; }
            set { Fields.Version[this] = value; }
        }

        [DisplayName("Codigo"), Size(5)]
        public String Codigo
        {
            get { return Fields.Codigo[this]; }
            set { Fields.Codigo[this] = value; }
        }

        [DisplayName("Descripcion"), Size(150), NotNull]
        public String Descripcion
        {
            get { return Fields.Descripcion[this]; }
            set { Fields.Descripcion[this] = value; }
        }
        [DisplayName("Nace Larga")]
        [Expression("(CONCAT(T0.[Descripcion],' (',T0.[Codigo],' ',jversion.[version],')'))")]
        [Expression("(T0.Descripcion || ' (' || T0.Codigo || ' ' || jversion.version || ')')", Dialect = "Sqlite")]
        public String NaceLarga
        {
            get { return Fields.NaceLarga[this]; }
            set { Fields.NaceLarga[this] = value; }
        }



        [DisplayName("Subsector"), ForeignKey("subsectores", "SubsectorId"), LeftJoin("jSubsector"), TextualField("Subsector"), LookupInclude]
        [LookupEditor(typeof(Entities.SubsectoresRow), CascadeFrom = "SectorId", CascadeField = "SectorId", InplaceAdd = true)]
        public Int32? SubsectorId
        {
            get { return Fields.SubsectorId[this]; }
            set { Fields.SubsectorId[this] = value; }
        }

        [DisplayName("Sector Id"), Expression("jSubsector.[SectorId]"),ForeignKey("sectores","SectorId"),LeftJoin("jSector"),LookupInclude]
        [LookupEditor(typeof(Entities.SectoresRow))]
        public Int32? SectorId
        {
            get { return Fields.SectorId[this]; }
            set { Fields.SectorId[this] = value; }
        }

        [DisplayName("Subsector"), Expression("jSubsector.[Subsector]")]
        public String Subsector
        {
            get { return Fields.Subsector[this]; }
            set { Fields.Subsector[this] = value; }
        }

        [DisplayName("Sector"), Expression("jSector.[Sector]")]
        public String Sector
        {
            get { return Fields.Sector[this]; }
            set { Fields.Sector[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        IIdField IIdRow.IdField
        {
            get { return Fields.NaceId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.NaceLarga; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public NacesRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field NaceId;
 
            public StringField Codigo;
            public StringField Descripcion;
            public Int32Field SubsectorId;

            public StringField NaceLarga;

            public Int32Field SectorId;
            public StringField Subsector;
            public StringField Sector;

            public Int32Field VersionId;
            public StringField Version;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
