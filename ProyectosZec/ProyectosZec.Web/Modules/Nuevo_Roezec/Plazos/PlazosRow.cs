﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("plazos")]
    [DisplayName("Plazos"), InstanceName("Plazos")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.Plazos")]

    public sealed class PlazosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Plazo Id"), Identity]
        public Int32? PlazoId
        {
            get { return Fields.PlazoId[this]; }
            set { Fields.PlazoId[this] = value; }
        }

        [DisplayName("Plazo"), Size(10), NotNull, QuickSearch]
        public String Plazo
        {
            get { return Fields.Plazo[this]; }
            set { Fields.Plazo[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.PlazoId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Plazo; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public PlazosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field PlazoId;
            public StringField Plazo;
        }
    }
}
