﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/Plazos"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.PlazosRow))]
    public class PlazosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/Plazos/PlazosIndex.cshtml");
        }
    }
}