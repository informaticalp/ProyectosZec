﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class PlazosDialog extends Serenity.EntityDialog<PlazosRow, any> {
        protected getFormKey() { return PlazosForm.formKey; }
        protected getIdProperty() { return PlazosRow.idProperty; }
        protected getLocalTextPrefix() { return PlazosRow.localTextPrefix; }
        protected getNameProperty() { return PlazosRow.nameProperty; }
        protected getService() { return PlazosService.baseUrl; }
        protected getDeletePermission() { return PlazosRow.deletePermission; }
        protected getInsertPermission() { return PlazosRow.insertPermission; }
        protected getUpdatePermission() { return PlazosRow.updatePermission; }

        protected form = new PlazosForm(this.idPrefix);

    }
}