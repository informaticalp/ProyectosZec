﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class PlazosGrid extends Serenity.EntityGrid<PlazosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.Plazos'; }
        protected getDialogType() { return PlazosDialog; }
        protected getIdProperty() { return PlazosRow.idProperty; }
        protected getInsertPermission() { return PlazosRow.insertPermission; }
        protected getLocalTextPrefix() { return PlazosRow.localTextPrefix; }
        protected getService() { return PlazosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}