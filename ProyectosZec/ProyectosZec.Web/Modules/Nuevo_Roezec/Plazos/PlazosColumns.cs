﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Plazos")]
    [BasedOnRow(typeof(Entities.PlazosRow), CheckNames = true)]
    public class PlazosColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 PlazoId { get; set; }
        [EditLink]
        public String Plazo { get; set; }
    }
}