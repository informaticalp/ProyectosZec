﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.Plazos")]
    [BasedOnRow(typeof(Entities.PlazosRow), CheckNames = true)]
    public class PlazosForm
    {
        public String Plazo { get; set; }
    }
}