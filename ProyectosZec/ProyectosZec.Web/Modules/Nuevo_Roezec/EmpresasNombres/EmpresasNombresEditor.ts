﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class EmpresasNombresEditor extends Common.GridEditorBase<EmpresasNombresRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.EmpresasNombres"; }
        protected getDialogType() { return EmpresasNombresEditDialog; }
        protected getLocalTextPrefix() { return EmpresasNombresRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Nombre";
        }
    }
}