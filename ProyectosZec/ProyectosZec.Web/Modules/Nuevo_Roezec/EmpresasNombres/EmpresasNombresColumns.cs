﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.EmpresasNombres")]
    [BasedOnRow(typeof(Entities.EmpresasNombresRow), CheckNames = true)]
    public class EmpresasNombresColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Width(60)]
        public Int32 NombreId { get; set; }
        //public Int32 EmpresaId { get; set; }
        [EditLink, Width(250)]
        public String Nombre { get; set; }
        [Width(120)]
        public String Cif { get; set; }
        public DateTime Desde { get; set;}
        public DateTime Hasta { get; set; }
        [Width(100)]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}