﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.EmpresasNombres")]
    [BasedOnRow(typeof(Entities.EmpresasNombresRow), CheckNames = true)]
    public class EmpresasNombresForm
    {
        //public Int32 EmpresaId { get; set; }
        public String Nombre { get; set; }
        public String Cif { get; set; }

        public DateTime Desde { get; set; }
        public DateTime Hasta { get; set; }

        //public Int32 UserId { get; set; }
        //public DateTime FechaModificacion { get; set; }
    }
}