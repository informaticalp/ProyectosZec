﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasNombresDialog extends Serenity.EntityDialog<EmpresasNombresRow, any> {
        protected getFormKey() { return EmpresasNombresForm.formKey; }
        protected getIdProperty() { return EmpresasNombresRow.idProperty; }
        protected getLocalTextPrefix() { return EmpresasNombresRow.localTextPrefix; }
        protected getNameProperty() { return EmpresasNombresRow.nameProperty; }
        protected getService() { return EmpresasNombresService.baseUrl; }
        protected getDeletePermission() { return EmpresasNombresRow.deletePermission; }
        protected getInsertPermission() { return EmpresasNombresRow.insertPermission; }
        protected getUpdatePermission() { return EmpresasNombresRow.updatePermission; }

        protected form = new EmpresasNombresForm(this.idPrefix);

    }
}