﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasNombresEditDialog extends Common.GridEditorDialog<EmpresasNombresRow> {
        protected getFormKey() { return EmpresasNombresForm.formKey; }
        protected getNameProperty() { return EmpresasNombresRow.nameProperty; }
        protected getLocalTextPrefix() { return EmpresasNombresRow.localTextPrefix; }

        protected form: EmpresasNombresForm;

        constructor() {
            super();
            this.form = new EmpresasNombresForm(this.idPrefix);
        }
    }
}