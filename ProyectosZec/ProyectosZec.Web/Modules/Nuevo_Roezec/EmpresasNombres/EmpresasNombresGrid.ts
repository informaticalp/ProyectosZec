﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasNombresGrid extends Serenity.EntityGrid<EmpresasNombresRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.EmpresasNombres'; }
        protected getDialogType() { return EmpresasNombresDialog; }
        protected getIdProperty() { return EmpresasNombresRow.idProperty; }
        protected getInsertPermission() { return EmpresasNombresRow.insertPermission; }
        protected getLocalTextPrefix() { return EmpresasNombresRow.localTextPrefix; }
        protected getService() { return EmpresasNombresService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}