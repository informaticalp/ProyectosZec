﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/EmpresasNombres"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EmpresasNombresRow))]
    public class EmpresasNombresController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/EmpresasNombres/EmpresasNombresIndex.cshtml");
        }
    }
}