﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/HistorialReadOnly"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.HistorialEmpresasRow))]
    public class HistorialReadOnlyController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/HistorialReadOnly/HistorialReadOnlyIndex.cshtml");
        }
    }
}