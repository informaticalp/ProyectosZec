﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.HistorialReadOnly")]
    [BasedOnRow(typeof(Entities.HistorialEmpresasRow), CheckNames = true)]
    public class HistorialReadOnlyColumns
    {
        [EditLink,DisplayName("Db.Shared.RecordId"), AlignRight,Width(70)]
        public Int32 HistorialId { get; set; }
        [EditLink]
        [Width(220)]
        public String EmpresaRazon { get; set; }
        [Width(120),Hidden]
        public String TecnicoNombreTecnico { get; set; }
        [Width(150),QuickFilter, QuickFilterOption("multiple", true)]
        public String EstadoEmpresaEstado { get; set; }
        [Width(100)]
        public String EmpresaNExpediente { get; set; }
        [Hidden,Width(100)]
        public String EmpresaCif { get; set; }

        [Hidden, Width(110)]
        public String TipologiaCapitalCapital { get; set; }
        [Hidden,Width(110)] 
        public String TipoGarantiaTasaGarantiaTasa { get; set; }
 
        [EditLink]
        [Width(150), QuickFilter, QuickFilterOption("multiple", true)]
        public String Procedimiento { get; set; }

        [Width(110), QuickFilter, DisplayFormat("d")]
        public DateTime FechaInicio { get; set; }
        [Width(120), QuickFilter, DisplayFormat("d")]
        public DateTime FechaResolucion { get; set; }
        [Width(100), QuickFilter,DisplayName("Resolución")]
        public String SentidoResolucion { get; set; }
        [DisplayName("Fecha Estado"), Width(100), DisplayFormat("d"), QuickFilter]
        public DateTime FechaEfecto { get; set; }
        [DisplayName("Fec. Alarma"), Width(100), DisplayFormat("d")]
        public DateTime FechaEfectoAlarma { get; set; }
        [Hidden, Width(90),AlignRight]
        public Int32 EmpresaEmpleoTraspasado { get; set; }
        [Hidden, Width(90),AlignRight]
        public Int32 EmpresaEmpleo6Meses { get; set; }
        [Hidden, Width(90),AlignRight]
        public Int32 EmpresaEmpleoPromedio { get; set; }
        [Hidden, Width(90),AlignRight]
        public Int32 EmpresaEmpleoPromedio2Anos { get; set; }
        [Hidden, Width(90), DisplayFormat("#,##0.00"),AlignRight]
        public Decimal EmpresaInversionTraspasada { get; set; }
        [Hidden, Width(90), DisplayFormat("#,##0.00"),AlignRight]
        public Decimal EmpresaInversion2Anos { get; set; }
        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }

    }
}