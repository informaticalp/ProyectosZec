﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.Empresas")]
    [BasedOnRow(typeof(Entities.EmpresasRow), CheckNames = true)]
    public class EmpresasForm
    {
        [Tab("General")]

        public String Razon { get; set; }
        [HalfWidth(UntilNext = true)]
        public Int32 FormaJuridicaId { get; set; }
        public Int32 TecnicoId { get; set; }
        public String Cif { get; set; }
        public Int32 NumeroRoezec { get; set; }
        public Boolean Exenta { get; set; }
        public String MotivoExencion { get; set; }
        public Int32 TipologiaCapitalId { get; set; }
        public Int32 TipoGarantiaTasaId { get; set; }
        public Decimal CapitalSocial { get; set; }
        public Int32 EstadoEmpresaId { get; set; }
        public DateTime FechaCambioEstado { get; set; }
        public DateTime FechaAlta { get; set; }
        public String Expediente { get; set; }
        //public String Direccion { get; set; }
        //public String Poblacion { get; set; }

        //public String Cp { get; set; }
        public Int32 IslaId { get; set; }
        //public String TelefonoFijo { get; set; }
        //public String Movil { get; set; }
 
        //[EmailEditor]
        //public String Email { get; set; }
        [ResetFormWidth]
        [TextAreaEditor(Rows = 4)]
        public String Descripcion { get; set; }

        [Tab("Objetivos")]
        [HalfWidth(UntilNext = true)]
        public Int32 EmpleoTraspasado { get; set; }
        public Int32 Empleo6Meses { get; set; }
        //public Int32 EmpleoPromedio { get; set; }
        public Int32 EmpleoPromedio2Anos { get; set; }
        public Decimal InversionTraspasada { get; set; }
        public Decimal Inversion2Anos { get; set; }
        public String NumTasaLiquidacion { get; set; }
        [TextAreaEditor(Rows = 3)]
        public String ObservEmpleo { get; set; }
        [TextAreaEditor(Rows = 3)]
        public String ObservInversion { get; set; }

        [ResetFormWidth]

        [Tab("Historial")]
        [HistorialEmpresasEditor]
        public List<Entities.HistorialEmpresasRow> HistorialList { get; set; }
        [Tab("Contactos")]
        [EmpresasContactosEditor]
        public List<Entities.EmpresasContactosRow> ContactosList { get; set; }
        [Tab("Naces")]
        [EmpresasNaceEditor]
        public List<Entities.EmpresasNaceRow> NacesList { get; set; }
        [Tab("Capital")]
        [ProcedenciaCapitalEditor]
        public List<Entities.ProcedenciaCapitalRow> CapitalList { get; set; }
        [Tab("Mercados")]
        [EmpresasMercadosEditor]
        public List<Entities.EmpresasMercadosRow> MercadosList {get; set;}
        [Tab("Den. Social")]
        [EmpresasNombresEditor]
        public List<Entities.EmpresasNombresRow> NombresList { get; set; }
        [Tab("Direcciones")]
        [EmpresasDireccionesEditor]
        public List<Entities.EmpresasDireccionesRow> DireccionesList { get; set; }
        [Tab("Empleos")]
        [EmpresasEmpleosEditor]
        public List<Entities.EmpresasEmpleosRow> EmpleosList { get; set; }
        [Tab("Ficheros")]
        [EmpresasFicherosEditor]
        public List<Entities.EmpresasFicherosRow> FicherosList { get; set; }

    }
}