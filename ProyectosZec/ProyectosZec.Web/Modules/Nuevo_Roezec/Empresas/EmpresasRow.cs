﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("empresas")]
    [DisplayName("Empresas"), InstanceName("Empresas")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.Empresas")]
    public sealed class EmpresasRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? EmpresaId
        {
            get { return Fields.EmpresaId[this]; }
            set { Fields.EmpresaId[this] = value; }
        }

        [DisplayName("Razón"), Size(255), NotNull, QuickSearch]
        public String Razon
        {
            get { return Fields.Razon[this]; }
            set { Fields.Razon[this] = value; }
        }

        [DisplayName("Descripción"), Size(1000),QuickSearch]
        public String Descripcion
        {
            get { return Fields.Descripcion[this]; }
            set { Fields.Descripcion[this] = value; }
        }

        [DisplayName("Forma Jurídica"), ForeignKey("formas_juridicas", "Forma_JuridicaId"), LeftJoin("jFormaJuridica"), TextualField("FormaJuridicaJuridica"),LookupInclude,QuickSearch]
        [LookupEditor(typeof(Entities.FormasJuridicasRow), InplaceAdd = true)]
        public Int32? FormaJuridicaId
        {
            get { return Fields.FormaJuridicaId[this]; }
            set { Fields.FormaJuridicaId[this] = value; }
        }
        [DisplayName("Expediente"), Size(25), Expression("(CONCAT(YEAR(T0.[FechaAlta]),'/',LPAD(T0.[EmpresaId],5,0)))"), QuickSearch]
        public String ExpedienteNuevo
        {
            get { return Fields.ExpedienteNuevo[this]; }
            set { Fields.ExpedienteNuevo[this] = value; }
        }

        [DisplayName("Exp. Antiguo"), Size(25),Column("Expediente"), QuickSearch]
        public String Expediente
        {
            get { return Fields.Expediente[this]; }
            set { Fields.Expediente[this] = value; }
        }

        [DisplayName("Técnico"), ForeignKey("tecnicos", "TecnicoId"), LeftJoin("jTecnico"), TextualField("TecnicoNombreTecnico"),LookupInclude,QuickSearch,NotNull]
        [LookupEditor(typeof(Entities.TecnicosRow), InplaceAdd = true)]
        public Int32? TecnicoId
        {
            get { return Fields.TecnicoId[this]; }
            set { Fields.TecnicoId[this] = value; }
        }

        [DisplayName("Cif"), Size(20)]
        public String Cif
        {
            get { return Fields.Cif[this]; }
            set { Fields.Cif[this] = value; }
        }

        [DisplayName("Dirección"), Size(60)]
        public String Direccion
        {
            get { return Fields.Direccion[this]; }
            set { Fields.Direccion[this] = value; }
        }

        [DisplayName("Población"), Size(100)]
        public String Poblacion
        {
            get { return Fields.Poblacion[this]; }
            set { Fields.Poblacion[this] = value; }
        }

        [DisplayName("CP"), Size(7)]
        public String Cp
        {
            get { return Fields.Cp[this]; }
            set { Fields.Cp[this] = value; }
        }

        [DisplayName("Isla"), ForeignKey("islas", "IslaId"), LeftJoin("jIsla"), TextualField("IslaNombreIsla"),QuickSearch,LookupInclude,NotNull]
        [LookupEditor(typeof(Entities.IslasRow))]
        public Int32? IslaId
        {
            get { return Fields.IslaId[this]; }
            set { Fields.IslaId[this] = value; }
        }
        [DisplayName("Old_Id"), Column("Roezec_old_id"), ReadOnly(true)]
        public Int32? RoezecOldId
        {
            get { return Fields.RoezecOldId[this]; }
            set { Fields.RoezecOldId[this] = value; }
        }

        [DisplayName("Nº Roezec"),Column("NumeroRoezec"),ReadOnly(true)]
        public Int32? NumeroRoezec
        {
            get { return Fields.NumeroRoezec[this]; }
            set { Fields.NumeroRoezec[this] = value; }
        }

        [DisplayName("Teléfono Fijo"), Column("Telefono_fijo"), Size(20)]
        public String TelefonoFijo
        {
            get { return Fields.TelefonoFijo[this]; }
            set { Fields.TelefonoFijo[this] = value; }
        }

        [DisplayName("Móvil"), Size(20)]
        public String Movil
        {
            get { return Fields.Movil[this]; }
            set { Fields.Movil[this] = value; }
        }

        [DisplayName("Email"), Size(75)]
        public String Email
        {
            get { return Fields.Email[this]; }
            set { Fields.Email[this] = value; }
        }

        [DisplayName("Proyecto"), ForeignKey("proyectos", "ProyectoId"), LeftJoin("jProyecto"), TextualField("Proyecto")]
        public Int32? ProyectoId
        {
            get { return Fields.ProyectoId[this]; }
            set { Fields.ProyectoId[this] = value; }
        }

        [DisplayName("Exenta"), Column("exenta"), Size(1)]
        public Boolean? Exenta
        {
            get { return Fields.Exenta[this]; }
            set { Fields.Exenta[this] = value; }
        }
        [DisplayName("Motivo Exención"), Column("Motivo_Exencion"), Size(100)]
        public String MotivoExencion
        {
            get { return Fields.MotivoExencion[this]; }
            set { Fields.MotivoExencion[this] = value; }
        }

        [DisplayName("Tipología Capital"), Column("Tipologia_CapitalId"), ForeignKey("tipologias_capital", "Tipologia_CapitalId"), LeftJoin("jTipologiaCapital"), TextualField("TipologiaCapitalCapital"),LookupInclude,QuickSearch,NotNull]
        [LookupEditor(typeof(Entities.TipologiasCapitalRow), InplaceAdd = true)]
        public Int32? TipologiaCapitalId
        {
            get { return Fields.TipologiaCapitalId[this]; }
            set { Fields.TipologiaCapitalId[this] = value; }
        }

        [DisplayName("Tipo Garantía Tasa"), Column("Tipo_Garantia_TasaId"), ForeignKey("tipos_garantia_tasas", "Tipo_Garantia_TasaId"), LeftJoin("jTipoGarantiaTasa"), TextualField("TipoGarantiaTasaGarantiaTasa"),LookupInclude,QuickSearch,NotNull]
        [LookupEditor(typeof(Entities.TiposGarantiaTasasRow), InplaceAdd = true)]
        public Int32? TipoGarantiaTasaId
        {
            get { return Fields.TipoGarantiaTasaId[this]; }
            set { Fields.TipoGarantiaTasaId[this] = value; }
        }

        [DisplayName("Empleo Traspasado"), Column("Empleo_Traspasado"), Size(4),NotNull]
        public Int32? EmpleoTraspasado
        {
            get { return Fields.EmpleoTraspasado[this]; }
            set { Fields.EmpleoTraspasado[this] = value; }
        }

        [DisplayName("Empleo 6 Meses"), Column("Empleo_6_meses"), Size(4),NotNull]
        public Int32? Empleo6Meses
        {
            get { return Fields.Empleo6Meses[this]; }
            set { Fields.Empleo6Meses[this] = value; }
        }

        [DisplayName("Empleo Medio"), Column("Empleo_promedio"), Size(4)]
        public Int32? EmpleoPromedio
        {
            get { return Fields.EmpleoPromedio[this]; }
            set { Fields.EmpleoPromedio[this] = value; }
        }

        [DisplayName("Empleo 2 Años"), Column("Empleo_promedio_2_anos"),Size(4),NotNull]
        public Int32? EmpleoPromedio2Anos
        {
            get { return Fields.EmpleoPromedio2Anos[this]; }
            set { Fields.EmpleoPromedio2Anos[this] = value; }
        }

        [DisplayName("Capital Social"), Column("CapitalSocial"), Size(10), Scale(2)]
        public Decimal? CapitalSocial
        {
            get { return Fields.CapitalSocial[this]; }
            set { Fields.CapitalSocial[this] = value; }
        }

        [DisplayName("Inv. Traspasada"), Column("Inversion_traspasada"), Size(10), Scale(2),NotNull]
        public Decimal? InversionTraspasada
        {
            get { return Fields.InversionTraspasada[this]; }
            set { Fields.InversionTraspasada[this] = value; }
        }

        [DisplayName("Inversión 2 Años"), Column("Inversion_2_anos"), Size(10), Scale(2),NotNull]
        public Decimal? Inversion2Anos
        {
            get { return Fields.Inversion2Anos[this]; }
            set { Fields.Inversion2Anos[this] = value; }
        }

        [DisplayName("Observ. Empleo"), Column("Observ_Empleo"), Size(500), Scale(2)]
        public String ObservEmpleo
        {
            get { return Fields.ObservEmpleo[this]; }
            set { Fields.ObservEmpleo[this] = value; }
        }
        [DisplayName("Observ. Inversión"), Column("Observ_Inversion"), Size(500), Scale(2)]
        public String ObservInversion
        {
            get { return Fields.ObservInversion[this]; }
            set { Fields.ObservInversion[this] = value; }
        }
        [DisplayName("Estado Empresa"), ForeignKey("estados_empresa", "EstadoEmpresaId"), LeftJoin("jEstadoEmpresa"), TextualField("EstadoEmpresaEstado"),LookupInclude, ReadOnly(true)]
        [LookupEditor(typeof(Entities.EstadosEmpresaRow),InplaceAdd =true)]
        public Int32? EstadoEmpresaId
        {
            get { return Fields.EstadoEmpresaId[this]; }
            set { Fields.EstadoEmpresaId[this] = value; }
        }
        [DisplayName("Fecha Alta"), Column("FechaAlta"), NotNull]
        public DateTime? FechaAlta
        {
            get { return Fields.FechaAlta[this]; }
            set { Fields.FechaAlta[this] = value; }
        }

        [DisplayName("Fecha Estado"),Column("Fecha_Cambio_Estado"), NotNull, ReadOnly(true)]
        public DateTime? FechaCambioEstado
        {
            get { return Fields.FechaCambioEstado[this]; }
            set { Fields.FechaCambioEstado[this] = value; }
        }


        [DisplayName("Num Tasa Liquidación"), Column("Num_Tasa_Liquidacion"), Size(20)]
        public String NumTasaLiquidacion
        {
            get { return Fields.NumTasaLiquidacion[this]; }
            set { Fields.NumTasaLiquidacion[this] = value; }
        }

        [DisplayName("Forma Jurídica"), Expression("jFormaJuridica.[Forma_Juridica]")]
        public String FormaJuridicaJuridica
        {
            get { return Fields.FormaJuridicaJuridica[this]; }
            set { Fields.FormaJuridicaJuridica[this] = value; }
        }

        [DisplayName("Técnico"), Expression("jTecnico.[NombreTecnico]")]
        public String TecnicoNombreTecnico
        {
            get { return Fields.TecnicoNombreTecnico[this]; }
            set { Fields.TecnicoNombreTecnico[this] = value; }
        }

        [DisplayName("Técnico"), Expression("jTecnico.[Tecnico]")]
        public String Tecnico
        {
            get { return Fields.Tecnico[this]; }
            set { Fields.Tecnico[this] = value; }
        }

        [DisplayName("Isla"), Expression("jIsla.[NombreIsla]")]
        public String IslaNombreIsla
        {
            get { return Fields.IslaNombreIsla[this]; }
            set { Fields.IslaNombreIsla[this] = value; }
        }

        [DisplayName("Isla"), Expression("jIsla.[Isla]")]
        public String Isla
        {
            get { return Fields.Isla[this]; }
            set { Fields.Isla[this] = value; }
        }

        [DisplayName("Capital"), Expression("jTipologiaCapital.[Tipologia_Capital]")]
        public String TipologiaCapitalCapital
        {
            get { return Fields.TipologiaCapitalCapital[this]; }
            set { Fields.TipologiaCapitalCapital[this] = value; }
        }

        [DisplayName("Garantía Tasa"), Expression("jTipoGarantiaTasa.[Tipo_Garantia_Tasa]")]
        public String TipoGarantiaTasaGarantiaTasa
        {
            get { return Fields.TipoGarantiaTasaGarantiaTasa[this]; }
            set { Fields.TipoGarantiaTasaGarantiaTasa[this] = value; }
        }

        [DisplayName("Estado"), Expression("jEstadoEmpresa.[Estado]")]
        public String EstadoEmpresaEstado
        {
            get { return Fields.EstadoEmpresaEstado[this]; }
            set { Fields.EstadoEmpresaEstado[this] = value; }
        }

        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        [DisplayName("Contactos")]
        [MasterDetailRelation(foreignKey: "EmpresaId",IncludeColumns = "ContactoNombre,TipoContactoContacto,ContactoEmail,FechaBaja,UserName,FechaModificacion"), NotMapped]

        public List<EmpresasContactosRow> ContactosList
        {
            get { return Fields.ContactosList[this]; }
            set { Fields.ContactosList[this] = value; }
        }


        [MasterDetailRelation(foreignKey: "EmpresaId", IncludeColumns = "Version,NaceCodigo,NaceDescripcion,NacePrincipal,UserName,FechaModificacion"), NotMapped]
        [DisplayName("Naces")]
        public List<EmpresasNaceRow> NacesList
        {
            get { return Fields.NacesList[this]; }
            set { Fields.NacesList[this] = value; }
        }

        [MasterDetailRelation(foreignKey: "EmpresaId",IncludeColumns = "Procedimiento,FechaInicio,FechaResolucion, SentidoResolucion,FechaEfecto,Instrucciones,ExpedienteElectronico,UserName,FechaModificacion"), NotMapped]
        [DisplayName("Procedimientos")]
        public List<HistorialEmpresasRow> HistorialList
        {
            get { return Fields.HistorialList[this]; }
            set { Fields.HistorialList[this] = value; }
        }

        [MasterDetailRelation(foreignKey: "EmpresaId", IncludeColumns = "Pais,UserName,FechaModificacion"), NotMapped]
        [DisplayName("Procedencia")]
        public List<ProcedenciaCapitalRow> CapitalList
        {
            get { return Fields.CapitalList[this]; }
            set { Fields.CapitalList[this] = value; }
        }
        [MasterDetailRelation(foreignKey: "EmpresaId", IncludeColumns = "Mercado,UserName,FechaModificacion"), NotMapped]
        [DisplayName("Mercados")]
        public List<EmpresasMercadosRow> MercadosList
        {
            get { return Fields.MercadosList[this]; }
            set { Fields.MercadosList[this] = value; }
        }

        [MasterDetailRelation(foreignKey: "EmpresaId", IncludeColumns = "Nombre,Cif,Desde,Hasta,UserName,FechaModificacion"), NotMapped]
        [DisplayName("Den. Social")]
        public List<EmpresasNombresRow> NombresList
        {
            get { return Fields.NombresList[this]; }
            set { Fields.NombresList[this] = value; }
        }

        [MasterDetailRelation(foreignKey: "EmpresaId", IncludeColumns = "TipoDireccion,Direccion,Poblacion,CP,Isla,PaisPais,Desde,Hasta,UserName,FechaModificacion"), NotMapped]
        [DisplayName("Direcciones")]
        public List<EmpresasDireccionesRow> DireccionesList
        {
            get { return Fields.DireccionesList[this]; }
            set { Fields.DireccionesList[this] = value; }
        }

        [MasterDetailRelation(foreignKey: "EmpresaId", IncludeColumns = "Any,Empleos,UserName,FechaModificacion"), NotMapped]
        [DisplayName("Empleos")]
        public List<EmpresasEmpleosRow> EmpleosList
        {
            get { return Fields.EmpleosList[this]; }
            set { Fields.EmpleosList[this] = value; }
        }

        [MasterDetailRelation(foreignKey: "EmpresaId", IncludeColumns = "Nombre,Observaciones,UserName,FechaModificacion"), NotMapped]
        [DisplayName("Ficheros")]
        public List<EmpresasFicherosRow> FicherosList
        {
            get { return Fields.FicherosList[this]; }
            set { Fields.FicherosList[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.EmpresaId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Razon; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public EmpresasRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field EmpresaId;
            public StringField Razon;
            public StringField Descripcion;
            public Int32Field FormaJuridicaId;
            public Int32Field TecnicoId;
            public StringField Cif;
            public StringField Direccion;
            public StringField Poblacion;
            public StringField Cp;
            public Int32Field IslaId;
            public StringField TelefonoFijo;
            public StringField Movil;
            public StringField Email;
            public Int32Field ProyectoId;
            public StringField Expediente;
            public StringField ExpedienteNuevo;
            public Int32Field NumeroRoezec;
            public Int32Field RoezecOldId;
            public BooleanField Exenta;
            public StringField MotivoExencion;
            public Int32Field TipologiaCapitalId;
            public Int32Field TipoGarantiaTasaId;
            public Int32Field EmpleoTraspasado;
            public Int32Field Empleo6Meses;
            public Int32Field EmpleoPromedio;
            public Int32Field EmpleoPromedio2Anos;
            public DecimalField InversionTraspasada;
            public DecimalField Inversion2Anos;
            public StringField ObservEmpleo;
            public StringField ObservInversion;
            public Int32Field EstadoEmpresaId;
            public DateTimeField FechaCambioEstado;
            public DateTimeField FechaAlta;
            public DecimalField CapitalSocial;
 
            public StringField NumTasaLiquidacion;

            public StringField FormaJuridicaJuridica;

            public StringField TecnicoNombreTecnico;
            public StringField Tecnico;

            public StringField IslaNombreIsla;
            public StringField Isla;

            public StringField TipologiaCapitalCapital;

            public StringField TipoGarantiaTasaGarantiaTasa;

            public StringField EstadoEmpresaEstado;

            public RowListField<EmpresasContactosRow> ContactosList;
            public RowListField<EmpresasNaceRow> NacesList;
            public RowListField<HistorialEmpresasRow> HistorialList;
            public RowListField<ProcedenciaCapitalRow> CapitalList;
            public RowListField<EmpresasMercadosRow> MercadosList;
            public RowListField<EmpresasNombresRow> NombresList;
            public RowListField<EmpresasDireccionesRow> DireccionesList;
            public RowListField<EmpresasEmpleosRow> EmpleosList;
            public RowListField<EmpresasFicherosRow> FicherosList;

            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
