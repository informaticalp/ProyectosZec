﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Empresas")]
    [BasedOnRow(typeof(Entities.EmpresasRow), CheckNames = true)]
    public class EmpresasColumns
    {
        [Hidden, EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Width(60)]
        public Int32 EmpresaId { get; set; }
        [EditLink, Width(100)]
        public String ExpedienteNuevo { get; set; }
        [EditLink,Width(100)]
        public String Expediente { get; set; }
        [Width(80), AlignRight, DisplayFormat("#,###")]
        public Int32 NumeroRoezec { get; set; }
        [EditLink, Width(250)]
        public String Razon { get; set; }
        [DisplayName("Estado"), Width(140), QuickFilter, QuickFilterOption("multiple", true)]
        public String EstadoEmpresaEstado { get; set; }
        [DisplayName("Fecha Alta"), Width(120),QuickFilter, DisplayFormat("d")]
        public DateTime FechaAlta { get; set; }
        [DisplayName("Fecha Cambio"), Width(120), Hidden, DisplayFormat("d")]
        public DateTime FechaCambioEstado { get; set; }
        [Width(100), QuickFilter]
        public String FormaJuridicaJuridica { get; set; }
        [Width(150), QuickFilter]
        public String TecnicoNombreTecnico { get; set; }
        //public String Cif { get; set; }
        //[Width(150)]
        //public String Direccion { get; set; }
        //[Width(150)]
        //public String Poblacion { get; set; }
        [Width(100), QuickFilter]
        public String IslaNombreIsla { get; set; }
        //public String TelefonoFijo { get; set; }
        //public String Movil { get; set; }
        //public String Email { get; set; }

        [Hidden]
        public String MotivoExencion { get; set; }
        public String TipologiaCapitalCapital { get; set; }
        [DisplayName("Garantía Tasa"), Width(100)]
        public String TipoGarantiaTasaGarantiaTasa { get; set; }
        [DisplayName("Emp. Trasp."), Width(90)]
        public Int32 EmpleoTraspasado { get; set; }
        [DisplayName("Emp. 6 meses"), Width(90)]
        public Int32 Empleo6Meses { get; set; }
        [DisplayName("Emp. Prom."), Width(90)]
        public Int32 EmpleoPromedio { get; set; }
        [DisplayName("Emp. 2 años"), Width(90)]
        public Int32 EmpleoPromedio2Anos { get; set; }
        [DisplayName("Inv. Trasp."), Width(90)]
        public Decimal InversionTraspasada { get; set; }
        [DisplayName("Inv. 2 años"), Width(90)]
        public Decimal Inversion2Anos { get; set; }
        public String NumTasaLiquidacion { get; set; }
        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion {get; set;}
        [Hidden,AlignRight,Width(80)]
        public Int32 RoezecOldId { get; set; }
    }
}