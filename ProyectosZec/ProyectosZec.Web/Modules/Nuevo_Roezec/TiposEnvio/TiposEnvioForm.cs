﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.TiposEnvio")]
    [BasedOnRow(typeof(Entities.TiposEnvioRow), CheckNames = true)]
    public class TiposEnvioForm
    {
        public String Tipo { get; set; }
    }
}