﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/TiposEnvio"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.TiposEnvioRow))]
    public class TiposEnvioController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/TiposEnvio/TiposEnvioIndex.cshtml");
        }
    }
}