﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposEnvioGrid extends Serenity.EntityGrid<TiposEnvioRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.TiposEnvio'; }
        protected getDialogType() { return TiposEnvioDialog; }
        protected getIdProperty() { return TiposEnvioRow.idProperty; }
        protected getInsertPermission() { return TiposEnvioRow.insertPermission; }
        protected getLocalTextPrefix() { return TiposEnvioRow.localTextPrefix; }
        protected getService() { return TiposEnvioService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}