﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposEnvioDialog extends Serenity.EntityDialog<TiposEnvioRow, any> {
        protected getFormKey() { return TiposEnvioForm.formKey; }
        protected getIdProperty() { return TiposEnvioRow.idProperty; }
        protected getLocalTextPrefix() { return TiposEnvioRow.localTextPrefix; }
        protected getNameProperty() { return TiposEnvioRow.nameProperty; }
        protected getService() { return TiposEnvioService.baseUrl; }
        protected getDeletePermission() { return TiposEnvioRow.deletePermission; }
        protected getInsertPermission() { return TiposEnvioRow.insertPermission; }
        protected getUpdatePermission() { return TiposEnvioRow.updatePermission; }

        protected form = new TiposEnvioForm(this.idPrefix);

    }
}