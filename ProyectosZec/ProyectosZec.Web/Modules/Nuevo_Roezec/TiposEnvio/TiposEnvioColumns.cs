﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.TiposEnvio")]
    [BasedOnRow(typeof(Entities.TiposEnvioRow), CheckNames = true)]
    public class TiposEnvioColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 TipoEnvioId { get; set; }
        [EditLink]
        public String Tipo { get; set; }
    }
}