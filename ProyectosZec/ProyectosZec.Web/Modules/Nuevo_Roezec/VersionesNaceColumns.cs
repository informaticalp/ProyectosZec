﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.VersionesNace")]
    [BasedOnRow(typeof(Entities.VersionesNaceRow), CheckNames = true)]
    public class VersionesNaceColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 VersionId { get; set; }
        [EditLink,Width(80)]
        public String Version { get; set; }
        [Width((120)),DisplayFormat("d")]
        public DateTime Fecha { get; set; }
    }
}