﻿using Serenity.ComponentModel;
using System.ComponentModel;

namespace ProyectosZec.Modules.Nuevo_Roezec
{
    [EnumKey("Nuevo_Roezec.SentidoResolucion")]
    public enum SentidoResolucion
    {
        [Description("Positivo")]
        Positivo = 1,
        [Description("Negativo")]
        Negativo = 2,
        [Description("Repara")]
        Repara = 3
    }
}