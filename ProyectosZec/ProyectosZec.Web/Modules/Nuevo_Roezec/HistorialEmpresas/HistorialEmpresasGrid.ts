﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()


    @Serenity.Decorators.registerClass()
    export class HistorialEmpresasGrid extends Serenity.EntityGrid<HistorialEmpresasRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.HistorialEmpresas'; }
        protected getDialogType() { return <any>HistorialEmpresasDialog; }
        protected getIdProperty() { return HistorialEmpresasRow.idProperty; }
        protected getInsertPermission() { return HistorialEmpresasRow.insertPermission; }
        protected getLocalTextPrefix() { return HistorialEmpresasRow.localTextPrefix; }
        protected getService() { return HistorialEmpresasService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        //// Añadir la siguiente función
        //protected getDefaultSortBy() {
        //    return [HistorialEmpresasRow.Fields.FechaInicio]; // Este es el campo de ordenación por defecto
        //}

    }
}