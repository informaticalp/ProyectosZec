﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.HistorialEmpresas")]
    [BasedOnRow(typeof(Entities.HistorialEmpresasRow), CheckNames = true)]
    public class HistorialEmpresasForm
    {
        
        [Tab("General")]
        
        [HalfWidth(UntilNext = true)]
        public Int32 ProcedimientoId { get; set; }
        public String ExpedienteElectronico { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaResolucion { get; set; }
        public Int32 SentidoResolucionId { get; set; }
        public DateTime FechaFirma { get; set; }
        public DateTime FechaEfecto { get; set; }
        public DateTime FechaEfectoAlarma { get; set; }

        [ResetFormWidth]
        [TextAreaEditor(Rows = 3)]
        public String Observaciones { get; set; }
        [TextAreaEditor(Rows = 12)]
        public String Instrucciones { get; set; }
        [Tab("Envíos")]
        [EnviosEditor]
        public List<Entities.EnviosRow> EnviosList { get; set; }

        [Tab("Ficheros")]
        [FicherosEditor]
        public List<Entities.FicherosRow> FicherosList { get; set; }
    }
}