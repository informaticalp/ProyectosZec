﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Collections.Generic;


    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("historial_empresas")]
    [DisplayName("Historial Empresas"), InstanceName("Historial Empresas")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    public sealed class HistorialEmpresasRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? HistorialId
        {
            get { return Fields.HistorialId[this]; }
            set { Fields.HistorialId[this] = value; }
        }

        [DisplayName("Empresa"), NotNull, ForeignKey("empresas", "EmpresaId"), LeftJoin("jEmpresa"), TextualField("EmpresaRazon")]
        public Int32? EmpresaId
        {
            get { return Fields.EmpresaId[this]; }
            set { Fields.EmpresaId[this] = value; }
        }

        [DisplayName("Procedimiento"), NotNull, ForeignKey("procedimientos", "ProcedimientoId"), LeftJoin("jProcedimiento"), TextualField("Procedimiento"),LookupInclude]
        //[LookupEditor(typeof(Entities.ProcedimientosRow),FilterField ="Visible",FilterValue = 0)]  Filter seems not working
        [LookupEditor(typeof(Entities.ProcedimientosRow))]

        public Int32? ProcedimientoId
        {
            get { return Fields.ProcedimientoId[this]; }
            set { Fields.ProcedimientoId[this] = value; }
        }

        [DisplayName("Fecha Inicio"), Column("Fecha_Inicio"), NotNull]
        public DateTime? FechaInicio
        {
            get { return Fields.FechaInicio[this]; }
            set { Fields.FechaInicio[this] = value; }
        }

        [DisplayName("Fecha Firma"), Column("FechaFirma")]
        public DateTime? FechaFirma
        {
            get { return Fields.FechaFirma[this]; }
            set { Fields.FechaFirma[this] = value; }
        }

        [DisplayName("Fecha Resolucion"), Column("Fecha_Resolucion")]
        public DateTime? FechaResolucion
        {
            get { return Fields.FechaResolucion[this]; }
            set { Fields.FechaResolucion[this] = value; }
        }

        [DisplayName("Fec. Activación "), Column("EstadoActivado")]
        public DateTime? EstadoActivado
        {
            get { return Fields.EstadoActivado[this]; }
            set { Fields.EstadoActivado[this] = value; }
        }

        [DisplayName("Sentido Resolucion"), Column("SentidoResolucionId"), ForeignKey("SentidosResolucion", "SentidoResolucionId"), LeftJoin("jSentidosResolucion"), TextualField("SentidoResolucion"), LookupInclude]
        [LookupEditor(typeof(Entities.SentidosresolucionRow))]
        public Int32? SentidoResolucionId
        {
            get { return Fields.SentidoResolucionId[this]; }
            set { Fields.SentidoResolucionId[this] = value; }
        }
        [DisplayName("Sentido Resolucion"),Expression("jSentidosResolucion.[SentidoResolucion]")]
        public String SentidoResolucion
        {
            get { return Fields.SentidoResolucion[this]; }
            set { Fields.SentidoResolucion[this] = value; }
        }
        [DisplayName("Fecha Efecto Cambio Estado"), Column("Fecha_efecto")]
        public DateTime? FechaEfecto
        {
            get { return Fields.FechaEfecto[this]; } 
            set { Fields.FechaEfecto[this] = value; }
        }
        [DisplayName("Fecha Efecto Alarma"), Column("FechaEfectoAlarma")]
        public DateTime? FechaEfectoAlarma
        {
            get { return Fields.FechaEfectoAlarma[this]; }
            set { Fields.FechaEfectoAlarma[this] = value; }
        }
        [DisplayName("Nº Exp. Acceda"), Size(25)]
        public String ExpedienteElectronico
        {
            get { return Fields.ExpedienteElectronico[this]; }
            set { Fields.ExpedienteElectronico[this] = value; }
        }
        [DisplayName("Observaciones"), Size(200)]
        public String Observaciones
        {
            get { return Fields.Observaciones[this]; }
            set { Fields.Observaciones[this] = value; }
        }

        [DisplayName("Empresa"), Expression("jEmpresa.[Razon]"),QuickSearch]
        public String EmpresaRazon
        {
            get { return Fields.EmpresaRazon[this]; }
            set { Fields.EmpresaRazon[this] = value; }
        }

        [DisplayName("Forma Juridica"), Expression("jEmpresa.[FormaJuridicaId]"),ForeignKey("formas_juridicas", "FormaJuridicaId"),LeftJoin("jFormaJuridica"),LookupInclude]
        [LookupEditor(typeof(Entities.FormasJuridicasRow))]
        public Int32? EmpresaFormaJuridicaId
        {
            get { return Fields.EmpresaFormaJuridicaId[this]; }
            set { Fields.EmpresaFormaJuridicaId[this] = value; }
        }

        [DisplayName("Expediente"), Expression("jEmpresa.[Expediente]")]
        public String EmpresaNExpediente
        {
            get { return Fields.EmpresaNExpediente[this]; }
            set { Fields.EmpresaNExpediente[this] = value; }
        }

        [DisplayName("TecnicoId"), Expression("jEmpresa.[TecnicoId]"), ForeignKey("tecnicos", "TecnicoId"), LeftJoin("jTecnico"),LookupInclude,TextualField("TecnicoNombreTecnico")]
        [LookupEditor(typeof(Entities.TecnicosRow))]
        public Int32? EmpresaTecnicoId
        {
            get { return Fields.EmpresaTecnicoId[this]; }
            set { Fields.EmpresaTecnicoId[this] = value; }
        }

        [DisplayName("Tecnico"), Expression("jTecnico.[NombreTecnico]")]
        public String TecnicoNombreTecnico
        {
            get { return Fields.TecnicoNombreTecnico[this]; }
            set { Fields.TecnicoNombreTecnico[this] = value; }
        }

        [DisplayName("Cif"), Expression("jEmpresa.[Cif]")]
        public String EmpresaCif
        {
            get { return Fields.EmpresaCif[this]; }
            set { Fields.EmpresaCif[this] = value; }
        }


        [DisplayName("Telefono Fijo"), Expression("jEmpresa.[Telefono_fijo]")]
        public String EmpresaTelefonoFijo
        {
            get { return Fields.EmpresaTelefonoFijo[this]; }
            set { Fields.EmpresaTelefonoFijo[this] = value; }
        }

        [DisplayName("Movil"), Expression("jEmpresa.[Movil]")]
        public String EmpresaMovil
        {
            get { return Fields.EmpresaMovil[this]; }
            set { Fields.EmpresaMovil[this] = value; }
        }

        [DisplayName("Email"), Expression("jEmpresa.[Email]")]
        public String EmpresaEmail
        {
            get { return Fields.EmpresaEmail[this]; }
            set { Fields.EmpresaEmail[this] = value; }
        }

        [DisplayName("Empresa Proyecto Id"), Expression("jEmpresa.[ProyectoId]")]
        public Int32? EmpresaProyectoId
        {
            get { return Fields.EmpresaProyectoId[this]; }
            set { Fields.EmpresaProyectoId[this] = value; }
        }

        [DisplayName("Expediente"), Expression("jEmpresa.[Expediente]")]
        public String EmpresaExpediente
        {
            get { return Fields.EmpresaExpediente[this]; }
            set { Fields.EmpresaExpediente[this] = value; }
        }

        [DisplayName("Motivo Exencion"), Expression("jEmpresa.[Motivo_Exencion]")]
        public String EmpresaMotivoExencion
        {
            get { return Fields.EmpresaMotivoExencion[this]; }
            set { Fields.EmpresaMotivoExencion[this] = value; }
        }

        [DisplayName("Tipologia Capital Id"), Expression("jEmpresa.[Tipologia_CapitalId]"), ForeignKey("tipologias_capital", "Tipologia_CapitalId"), LeftJoin("jTipologiaCapital"), TextualField("TipologiaCapitalCapital"), LookupInclude, QuickSearch]
        [LookupEditor(typeof(Entities.TipologiasCapitalRow))]
        public Int32? EmpresaTipologiaCapitalId
        {
            get { return Fields.EmpresaTipologiaCapitalId[this]; }
            set { Fields.EmpresaTipologiaCapitalId[this] = value; }
        }

        [DisplayName("Tipol. Capital"), Expression("jTipologiaCapital.[Tipologia_Capital]")]
        public String TipologiaCapitalCapital
        {
            get { return Fields.TipologiaCapitalCapital[this]; }
            set { Fields.TipologiaCapitalCapital[this] = value; }
        }

        [DisplayName("Tipo Garantia Id"), Expression("jEmpresa.[Tipo_Garantia_TasaId]"), ForeignKey("tipos_garantia_tasas", "Tipo_Garantia_TasaId"), LeftJoin("jTipoGarantiaTasa"), TextualField("TipoGarantiaTasaGarantiaTasa"), LookupInclude, QuickSearch]
        [LookupEditor(typeof(Entities.TiposGarantiaTasasRow))]
        public Int32? EmpresaTipoGarantiaTasaId
        {
            get { return Fields.EmpresaTipoGarantiaTasaId[this]; }
            set { Fields.EmpresaTipoGarantiaTasaId[this] = value; }
        }


        [DisplayName("Garantia Tasa"), Expression("jTipoGarantiaTasa.[Tipo_Garantia_Tasa]")]
        public String TipoGarantiaTasaGarantiaTasa
        {
            get { return Fields.TipoGarantiaTasaGarantiaTasa[this]; }
            set { Fields.TipoGarantiaTasaGarantiaTasa[this] = value; }
        }

        [DisplayName("Emp. Trasp."), Expression("jEmpresa.[Empleo_Traspasado]")]
        public Int32? EmpresaEmpleoTraspasado
        {
            get { return Fields.EmpresaEmpleoTraspasado[this]; }
            set { Fields.EmpresaEmpleoTraspasado[this] = value; }
        }

        [DisplayName("Emp. 6 Meses"), Expression("jEmpresa.[Empleo_6_meses]")]
        public Int32? EmpresaEmpleo6Meses
        {
            get { return Fields.EmpresaEmpleo6Meses[this]; }
            set { Fields.EmpresaEmpleo6Meses[this] = value; }
        }

        [DisplayName("Emp. Medio"), Expression("jEmpresa.[Empleo_promedio]")]
        public Int32? EmpresaEmpleoPromedio
        {
            get { return Fields.EmpresaEmpleoPromedio[this]; }
            set { Fields.EmpresaEmpleoPromedio[this] = value; }
        }

        [DisplayName("Emp. 2 Años"), Expression("jEmpresa.[Empleo_promedio_2_anos]")]
        public Int32? EmpresaEmpleoPromedio2Anos
        {
            get { return Fields.EmpresaEmpleoPromedio2Anos[this]; }
            set { Fields.EmpresaEmpleoPromedio2Anos[this] = value; }
        }

        [DisplayName("Inv. Traspasada"), Expression("jEmpresa.[Inversion_traspasada]")]
        public Decimal? EmpresaInversionTraspasada
        {
            get { return Fields.EmpresaInversionTraspasada[this]; }
            set { Fields.EmpresaInversionTraspasada[this] = value; }
        }

        [DisplayName("Inv. 2 Años"), Expression("jEmpresa.[Inversion_2_anos]")]
        public Decimal? EmpresaInversion2Anos
        {
            get { return Fields.EmpresaInversion2Anos[this]; }
            set { Fields.EmpresaInversion2Anos[this] = value; }
        }

        [DisplayName("Estado Emp."), Expression("jEmpresa.[EstadoEmpresaId]"),ForeignKey("estados_empresa", "EstadoEmpresaId"), LeftJoin("jEstadoEmpresa"), TextualField("EstadoEmpresaEstado"), LookupInclude]
        [LookupEditor(typeof(Entities.EstadosEmpresaRow))]
        public Int32? EmpresaEstadoEmpresaId
        {
            get { return Fields.EmpresaEstadoEmpresaId[this]; }
            set { Fields.EmpresaEstadoEmpresaId[this] = value; }
        }

        [DisplayName("Estado"), Expression("jEstadoEmpresa.[Estado]")]
        public String EstadoEmpresaEstado
        {
            get { return Fields.EstadoEmpresaEstado[this]; }
            set { Fields.EstadoEmpresaEstado[this] = value; }
        }

        [DisplayName("Empresa Num Tasa Liquidacion"), Expression("jEmpresa.[Num_Tasa_Liquidacion]")]
        public String EmpresaNumTasaLiquidacion
        {
            get { return Fields.EmpresaNumTasaLiquidacion[this]; }
            set { Fields.EmpresaNumTasaLiquidacion[this] = value; }
        }

        [DisplayName("Procedimiento"), Expression("jProcedimiento.[Procedimiento]")]
        public String Procedimiento
        {
            get { return Fields.Procedimiento[this]; }
            set { Fields.Procedimiento[this] = value; }
        }
        [DisplayName("Instrucciones"), Expression("jProcedimiento.[Instrucciones]"), ReadOnly(true)]
        public String Instrucciones
        {
            get { return Fields.Instrucciones[this]; }
            set { Fields.Instrucciones[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        [MasterDetailRelation(foreignKey: "HistorialId", IncludeColumns = "FicheroId, TipoDocumento, FechaCaptura, NombreNatural,Observaciones"), NotMapped, MinSelectLevel(SelectLevel.List)]

        [DisplayName("Ficheros")]
        public List<FicherosRow> FicherosList
        {
            get { return Fields.FicherosList[this]; }
            set { Fields.FicherosList[this] = value; }
        }

        [MasterDetailRelation(foreignKey: "HistorialId", IncludeColumns = "EnvioId,TipoEnvioTipo,EstadoEnvioEstado,FormaEnvioForma,FechaEnvio,ContactoEnvioFullName,FechaRecepcion"), NotMapped, MinSelectLevel(SelectLevel.List)]

        [DisplayName("Envios")]
        public List<EnviosRow> EnviosList
        {
            get { return Fields.EnviosList[this]; }
            set { Fields.EnviosList[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.HistorialId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.EmpresaRazon; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public HistorialEmpresasRow()
            : base(Fields)
        {
        }
        public readonly Int32Field Sentido;

        public class RowFields : RowFieldsBase
        {
            public Int32Field HistorialId;
            public Int32Field EmpresaId;
            public Int32Field ProcedimientoId;
            public DateTimeField FechaInicio;
            public DateTimeField FechaResolucion;
            public Int32Field SentidoResolucionId;
            public DateTimeField FechaEfecto;
            public DateTimeField FechaFirma;
            public DateTimeField FechaEfectoAlarma;
            public StringField ExpedienteElectronico;
            public DateTimeField EstadoActivado;

            public StringField Observaciones;
            //public StringField Ficheros;

            public StringField SentidoResolucion;
            public StringField EmpresaRazon;
            public Int32Field EmpresaFormaJuridicaId;
            public StringField EmpresaNExpediente;
            public Int32Field EmpresaTecnicoId;
            public StringField EmpresaCif;
            public StringField EmpresaTelefonoFijo;
            public StringField EmpresaMovil;
            public StringField EmpresaEmail;
            public Int32Field EmpresaProyectoId;
            public StringField EmpresaExpediente;
            public StringField EmpresaMotivoExencion;
            public Int32Field EmpresaTipologiaCapitalId;
            public Int32Field EmpresaTipoGarantiaTasaId;
            public Int32Field EmpresaEmpleoTraspasado;
            public Int32Field EmpresaEmpleo6Meses;
            public Int32Field EmpresaEmpleoPromedio;
            public Int32Field EmpresaEmpleoPromedio2Anos;
            public DecimalField EmpresaInversionTraspasada;
            public DecimalField EmpresaInversion2Anos;
            public Int32Field EmpresaEstadoEmpresaId;
            public StringField EmpresaNumTasaLiquidacion;


            public StringField TecnicoNombreTecnico;

            public StringField TipologiaCapitalCapital;
            public StringField TipoGarantiaTasaGarantiaTasa;
            public StringField EstadoEmpresaEstado;

            public StringField Procedimiento;
            public StringField Instrucciones;

            public ListField<EnviosRow> EnviosList;
            public ListField<FicherosRow> FicherosList;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
