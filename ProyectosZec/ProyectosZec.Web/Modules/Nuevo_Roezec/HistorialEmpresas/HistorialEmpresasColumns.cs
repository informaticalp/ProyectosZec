﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.HistorialEmpresas")]
    [BasedOnRow(typeof(Entities.HistorialEmpresasRow), CheckNames = true)]
    public class HistorialEmpresasColumns
    {
        [Width(60),EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 HistorialId { get; set; }
        [EditLink]
        [Width(220)]
        public String Procedimiento { get; set; }
        
        [Width(110), DisplayFormat("d")]
        public DateTime FechaInicio { get; set; }
        [Width(100), DisplayName("Fec. Resol."), DisplayFormat("d")]
        public DateTime FechaResolucion { get; set; }
        [Width(100),DisplayName("Sentido")]
        public String SentidoResolucion { get; set; }
        [DisplayName("Fecha Estado"),Width(100), DisplayFormat("d")]
        public DateTime FechaEfecto { get; set; }
        [DisplayName("Fec. Alarma"), Width(100), DisplayFormat("d")]
        public DateTime FechaEfectoAlarma { get; set; }
        [Width(100)]
        public DateTime EstadoActivado { get; set; }
        [Width(250)]
        public String Instrucciones { get; set; }
        [Width(130)]
        public String ExpedienteElectronico { get; set; }
        [Width(100),Hidden]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g"),Hidden]
        public DateTime FechaModificacion { get; set; }

    }
}