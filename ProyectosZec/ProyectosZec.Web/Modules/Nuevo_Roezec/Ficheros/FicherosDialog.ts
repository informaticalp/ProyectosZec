﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class FicherosDialog extends Serenity.EntityDialog<FicherosRow, any> {
        protected getFormKey() { return FicherosForm.formKey; }
        protected getIdProperty() { return FicherosRow.idProperty; }
        protected getLocalTextPrefix() { return FicherosRow.localTextPrefix; }
        protected getNameProperty() { return FicherosRow.nameProperty; }
        protected getService() { return FicherosService.baseUrl; }
        protected getDeletePermission() { return FicherosRow.deletePermission; }
        protected getInsertPermission() { return FicherosRow.insertPermission; }
        protected getUpdatePermission() { return FicherosRow.updatePermission; }

        protected form = new FicherosForm(this.idPrefix);

    }
}