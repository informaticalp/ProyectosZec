﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class FicherosGrid extends Serenity.EntityGrid<FicherosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.Ficheros'; }
        protected getDialogType() { return FicherosDialog; }
        protected getIdProperty() { return FicherosRow.idProperty; }
        protected getInsertPermission() { return FicherosRow.insertPermission; }
        protected getLocalTextPrefix() { return FicherosRow.localTextPrefix; }
        protected getService() { return FicherosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        /* Valor por defecto al añadir un registro */
        /* Basado en Basic Samples/Dialogs/DefaultValuesInNewDialog */
        protected addButtonClick() {
            this.editItem(<Nuevo_Roezec.FicherosRow>{
                Organo: 'EA0023288'
            });
        }
    }
}