﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />


namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class FicherosEditor extends Common.GridEditorBase<FicherosRow> {
        /*protected getPersistanceStorage(): Serenity.SettingStorage { return new Common.UserPreferenceStorage(); }*/
        protected getColumnsKey() { return "Nuevo_Roezec.Ficheros"; }
        protected getDialogType() { return FicherosEditDialog; }
        protected getLocalTextPrefix() { return FicherosRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Fichero";
        }
    }
}