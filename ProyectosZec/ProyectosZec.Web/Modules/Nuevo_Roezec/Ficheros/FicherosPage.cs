﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/Ficheros"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.FicherosRow))]
    public class FicherosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/Ficheros/FicherosIndex.cshtml");
        }
    }
}