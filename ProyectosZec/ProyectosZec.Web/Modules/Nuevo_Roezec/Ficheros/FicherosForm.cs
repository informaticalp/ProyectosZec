﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.Ficheros")]
    [BasedOnRow(typeof(Entities.FicherosRow), CheckNames = true)]
    public class FicherosForm
    {
        [Tab("General")]
        [HalfWidth(UntilNext = true)]
        //public Int32 HistorialId { get; set; }
        
        public DateTime FechaCaptura { get; set; }
        public Int32 TipoDocumentoId { get; set; }
        public String CSV { get; set; }
        public String RegulacionCSV { get; set; }
        public String NombreNatural { get; set; }
        [DefaultValue("EA0023288")]
        public String Organo { get; set; }


        [ResetFormWidth]
        [TextAreaEditor(Rows = 3)]
        public String Observaciones { get; set; }
        [Tab("Fichero")]
        public String Fichero { get; set; }
        [Tab("Metadatos")]
        [MetadatosEditor]
        public List<Entities.MetadatosRow> MetadatosList { get; set; }
        
    }
}