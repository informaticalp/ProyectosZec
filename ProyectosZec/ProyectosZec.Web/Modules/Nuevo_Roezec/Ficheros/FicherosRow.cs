﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Collections.Generic;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("ficheros")]
    [DisplayName("Ficheros"), InstanceName("Ficheros")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    public sealed class FicherosRow : Row, IIdRow
    {
        [DisplayName("Fichero Id"), Identity]
        public Int32? FicheroId
        {
            get { return Fields.FicheroId[this]; }
            set { Fields.FicheroId[this] = value; }
        }

        [DisplayName("Historial"), NotNull, ForeignKey("historial_empresas", "HistorialId"), LeftJoin("jHistorial"), TextualField("HistorialObservaciones")]
        public Int32? HistorialId
        {
            get { return Fields.HistorialId[this]; }
            set { Fields.HistorialId[this] = value; }
        }

        [DisplayName("Tipo Documento"), Column("TipoDocumentoId"), NotNull, ForeignKey("tipos_documento", "TipoDocumentoId"), LeftJoin("jTipoDocumento"), TextualField("Tipo")]
        [LookupEditor(typeof(Entities.TiposDocumentoRow), InplaceAdd = true)]
        public Int32? TipoDocumentoId
        {
            get { return Fields.TipoDocumentoId[this]; }
            set { Fields.TipoDocumentoId[this] = value; }
        }
        [DisplayFormat("Tipo Documento"), Expression("jTipoDocumento.[Tipo]")]
        public String TipoDocumento
        {
            get { return Fields.TipoDocumento[this]; }
            set { Fields.TipoDocumento[this] = value; }
        }

        [DisplayName("Fichero"), Size(1000), MultipleFileUploadEditor(FilenameFormat = "Roezec/Files/~", ScaleWidth = 800, ScaleHeight = 600)]
        public String Fichero
        {
            get { return Fields.Fichero[this]; }
            set { Fields.Fichero[this] = value; }
        }
        [DisplayName("CSV"), Column("CSV"), Size(200)]
        public String CSV
        {
            get { return Fields.CSV[this]; }
            set { Fields.CSV[this] = value; }
        }
        [DisplayName("Regulación CSV"), Column("RegulacionCSV"), Size(200)]
        public String RegulacionCSV
        {
            get { return Fields.RegulacionCSV[this]; }
            set { Fields.RegulacionCSV[this] = value; }
        }
        [DisplayFormat("Organo"), Column("Organo"), Size(30)]
        public String Organo
        {
            get { return Fields.Organo[this]; }
            set { Fields.Organo[this] = value; }
        }

        [DisplayFormat("NombreNatural"), Column("NombreNatural"), Size(200)]
        public String NombreNatural
        {
            get { return Fields.NombreNatural[this]; }
            set { Fields.NombreNatural[this] = value; }
        }

        [DisplayName("Contenido"), Column("Observaciones"), Size(1000)]
        public String Observaciones
        {
            get { return Fields.Observaciones[this]; }
            set { Fields.Observaciones[this] = value; }
        }

        [DisplayName("Fecha Captura"), NotNull]
        public DateTime? FechaCaptura
        {
            get { return Fields.FechaCaptura[this]; }
            set { Fields.FechaCaptura[this] = value; }
        }

        /*** Audit Fields ****/
        [DisplayName("Usuario"),Column("UserId"),ForeignKey("default.users","UserId"),LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"),Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificacion")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        [DisplayName("Empresa Id"), Expression("jHistorial.[EmpresaId]"), ForeignKey("Empresas", "EmpresaId"), LeftJoin("jEmpresas"), TextualField("Razon")]
        public Int32? HistorialEmpresaId
        {
            get { return Fields.HistorialEmpresaId[this]; }
            set { Fields.HistorialEmpresaId[this] = value; }
        }
        [DisplayName("Razón"), Expression("jEmpresas.[Razon]"),QuickSearch]
        public String Razon
        {
            get { return Fields.Razon[this]; }
            set { Fields.Razon[this] = value; }
        }

        [DisplayName("Historial Procedimiento Id"), Expression("jHistorial.[ProcedimientoId]")]
        public Int32? HistorialProcedimientoId
        {
            get { return Fields.HistorialProcedimientoId[this]; }
            set { Fields.HistorialProcedimientoId[this] = value; }
        }

        [DisplayName("Historial Fecha Inicio"), Expression("jHistorial.[Fecha_Inicio]")]
        public DateTime? HistorialFechaInicio
        {
            get { return Fields.HistorialFechaInicio[this]; }
            set { Fields.HistorialFechaInicio[this] = value; }
        }

        [DisplayName("Historial Fecha Resolucion"), Expression("jHistorial.[Fecha_Resolucion]")]
        public DateTime? HistorialFechaResolucion
        {
            get { return Fields.HistorialFechaResolucion[this]; }
            set { Fields.HistorialFechaResolucion[this] = value; }
        }

        [DisplayName("Historial Sentido Resolucion"), Expression("jHistorial.[SentidoResolucionId]")]
        public Int16? HistorialSentidoResolucion
        {
            get { return Fields.HistorialSentidoResolucion[this]; }
            set { Fields.HistorialSentidoResolucion[this] = value; }
        }

        [DisplayName("Historial Fecha Efecto"), Expression("jHistorial.[Fecha_efecto]")]
        public DateTime? HistorialFechaEfecto
        {
            get { return Fields.HistorialFechaEfecto[this]; }
            set { Fields.HistorialFechaEfecto[this] = value; }
        }

 

        [DisplayName("Historial Observaciones"), Expression("jHistorial.[Observaciones]")]
        public String HistorialObservaciones
        {
            get { return Fields.HistorialObservaciones[this]; }
            set { Fields.HistorialObservaciones[this] = value; }
        }

        [MasterDetailRelation(foreignKey: "FicheroId", IncludeColumns = "MetadatoId,Nombre,Valor"), NotMapped, MinSelectLevel(SelectLevel.List)]

        [DisplayName("Metadatos")]
        public List<MetadatosRow> MetadatosList
        {
            get { return Fields.MetadatosList[this]; }
            set { Fields.MetadatosList[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.FicheroId; }
        }



        public static readonly RowFields Fields = new RowFields().Init();

        public FicherosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field FicheroId;
            public Int32Field HistorialId;
            public StringField Fichero;
            public StringField Organo;
            public StringField NombreNatural;
            public DateTimeField FechaCaptura;
            public Int32Field TipoDocumentoId;
            public StringField CSV;
            public StringField RegulacionCSV;
            public StringField Observaciones;

            public StringField TipoDocumento;


            public Int32Field HistorialEmpresaId;
            public StringField Razon;
            public Int32Field HistorialProcedimientoId;
            public DateTimeField HistorialFechaInicio;
            public DateTimeField HistorialFechaResolucion;
            public Int16Field HistorialSentidoResolucion;
            public DateTimeField HistorialFechaEfecto;
            public StringField HistorialObservaciones;

            public RowListField<MetadatosRow> MetadatosList;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
