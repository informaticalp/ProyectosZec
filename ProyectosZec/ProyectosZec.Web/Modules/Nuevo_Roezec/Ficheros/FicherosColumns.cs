﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Ficheros")]
    [BasedOnRow(typeof(Entities.FicherosRow), CheckNames = true)]
    public class FicherosColumns
    {
        [EditLink,DisplayName("Db.Shared.RecordId"), AlignRight]
        [Width(50)]
        public Int32 FicheroId { get; set; }
        //[Width(200)]
        //public String Razon { get; set; }
        [EditLink,Width(120),DisplayName("Tipo Doc.")]
        public String TipoDocumento { get; set; }

        [Width(110), DisplayFormat("d"),DisplayName("Fec. Captura")]
        public DateTime FechaCaptura { get; set; }
        [Width(270)]
        public String NombreNatural { get; set; }
        [Width(250)]
        public String Observaciones { get; set; }
        //public String CSV { get; set; }
        //public String RegulacionCSV { get; set; }

        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}