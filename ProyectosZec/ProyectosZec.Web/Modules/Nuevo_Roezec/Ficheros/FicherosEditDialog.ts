﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class FicherosEditDialog extends Common.GridEditorDialog<FicherosRow> {
        protected getFormKey() { return FicherosForm.formKey; }
        protected getNameProperty() { return FicherosRow.nameProperty; }
        protected getLocalTextPrefix() { return FicherosRow.localTextPrefix; }

        protected form: FicherosForm;

        constructor() {
            super();
            this.form = new FicherosForm(this.idPrefix);
        }
    }
}