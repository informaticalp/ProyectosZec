﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposAlarmaGrid extends Serenity.EntityGrid<TiposAlarmaRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.TiposAlarma'; }
        protected getDialogType() { return TiposAlarmaDialog; }
        protected getIdProperty() { return TiposAlarmaRow.idProperty; }
        protected getInsertPermission() { return TiposAlarmaRow.insertPermission; }
        protected getLocalTextPrefix() { return TiposAlarmaRow.localTextPrefix; }
        protected getService() { return TiposAlarmaService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}