﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.TiposAlarma")]
    [BasedOnRow(typeof(Entities.TiposAlarmaRow), CheckNames = true)]
    public class TiposAlarmaColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 TipoAlarmaId { get; set; }
        [EditLink,Width(350)]
        public String Texto { get; set; }
        [Width(100),AlignRight]
        public String TextoPlazo { get; set; }
        [Width(100),AlignRight]
        public Int32 DiasAviso { get; set; }
        [Width(350)]
        public String Email { get; set; }
        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}