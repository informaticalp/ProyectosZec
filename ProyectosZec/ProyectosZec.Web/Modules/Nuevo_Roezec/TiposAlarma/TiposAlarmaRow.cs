﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("tipos_alarma")]
    [DisplayName("Tipos Alarma"), InstanceName("Tipos Alarma")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    [LookupScript("Nuevo_Roezec.TiposAlarma")]
    public sealed class TiposAlarmaRow : Row, IIdRow, INameRow
    {
        [DisplayName("Tipo Alarma Id"), Identity]
        public Int32? TipoAlarmaId
        {
            get { return Fields.TipoAlarmaId[this]; }
            set { Fields.TipoAlarmaId[this] = value; }
        }

        [DisplayName("Texto"), Size(100), NotNull, QuickSearch]
        public String Texto
        {
            get { return Fields.Texto[this]; }
            set { Fields.Texto[this] = value; }
        }

        [DisplayName("Nº"), Column("dias"), NotNull]
        public Int32? Dias
        {
            get { return Fields.Dias[this]; }
            set { Fields.Dias[this] = value; }
        }

        [DisplayName("Plazo"), Column("PlazoId"),NotNull, ForeignKey("Plazos", "PlazoId"), LeftJoin("JPlazos"),TextualField("Plazo")]
        [LookupEditor(typeof(Entities.PlazosRow))]
        public Int32? PlazoId
        {
            get { return Fields.PlazoId[this]; }
            set { Fields.PlazoId[this] = value; }
        }

        [DisplayName("Plazo"), Expression("jPlazos.[Plazo]")]
        public String Plazo
        {
            get { return Fields.Plazo[this]; }
            set { Fields.Plazo[this] = value; }
        }
        [DisplayName("Caducidad"),Expression("(CONCAT(T0.[Dias],' ',jPlazos.[Plazo]))")]
        public String TextoPlazo
        {
            get { return Fields.TextoPlazo[this]; }
            set { Fields.TextoPlazo[this] = value; }
        }
        [DisplayName("Nº Dias Email"), NotNull]
        public Int32? DiasAviso
        {
            get { return Fields.DiasAviso[this]; }
            set { Fields.DiasAviso[this] = value; }
        }

        [DisplayName("Email"), Size(200)]
        public String Email
        {
            get { return Fields.Email[this]; }
            set { Fields.Email[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/
        IIdField IIdRow.IdField
        {
            get { return Fields.TipoAlarmaId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Texto; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public TiposAlarmaRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field TipoAlarmaId;
            public StringField Texto;
            public Int32Field DiasAviso;
            public Int32Field Dias;
            public Int32Field PlazoId;
            public StringField Email;
            public StringField Plazo;
            public StringField TextoPlazo;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
