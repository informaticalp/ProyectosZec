﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposAlarmaDialog extends Serenity.EntityDialog<TiposAlarmaRow, any> {
        protected getFormKey() { return TiposAlarmaForm.formKey; }
        protected getIdProperty() { return TiposAlarmaRow.idProperty; }
        protected getLocalTextPrefix() { return TiposAlarmaRow.localTextPrefix; }
        protected getNameProperty() { return TiposAlarmaRow.nameProperty; }
        protected getService() { return TiposAlarmaService.baseUrl; }
        protected getDeletePermission() { return TiposAlarmaRow.deletePermission; }
        protected getInsertPermission() { return TiposAlarmaRow.insertPermission; }
        protected getUpdatePermission() { return TiposAlarmaRow.updatePermission; }

        protected form = new TiposAlarmaForm(this.idPrefix);

    }
}