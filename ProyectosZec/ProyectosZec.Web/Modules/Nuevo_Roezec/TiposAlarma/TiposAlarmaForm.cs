﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.TiposAlarma")]
    [BasedOnRow(typeof(Entities.TiposAlarmaRow), CheckNames = true)]
    public class TiposAlarmaForm
    {
        public String Texto { get; set; }
        [HalfWidth(UntilNext = true)]
        [DisplayName("Caducidad")]
        public Int32 Dias { get; set; }
        [DisplayName("Tiempo")]
        public Int32 PlazoId { get; set; }
        [ResetFormWidth]
        public Int32 DiasAviso { get; set; }
        public String Email { get; set; }
    }
}