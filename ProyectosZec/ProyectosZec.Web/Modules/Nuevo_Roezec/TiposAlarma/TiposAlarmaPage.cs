﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/TiposAlarma"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.TiposAlarmaRow))]
    public class TiposAlarmaController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/TiposAlarma/TiposAlarmaIndex.cshtml");
        }
    }
}