﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("versiones_nace")]
    [DisplayName("Versiones Nace"), InstanceName("Versiones Nace")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.VersionesNace")]
    public sealed class VersionesNaceRow : Row, IIdRow, INameRow
    {
        [DisplayName("Version Id"), Column("VersionId"), Identity]
        public Int32? VersionId
        {
            get { return Fields.VersionId[this]; }
            set { Fields.VersionId[this] = value; }
        }

        [DisplayName("Version"), Column("Version"), Size(30), NotNull, QuickSearch]
        public String Version
        {
            get { return Fields.Version[this]; }
            set { Fields.Version[this] = value; }
        }
        [DisplayName("Fecha"), Column("Fecha")]
        public DateTime? Fecha
        {
            get { return Fields.Fecha[this]; }
            set { Fields.Fecha[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/
        IIdField IIdRow.IdField
        {
            get { return Fields.VersionId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Version; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public VersionesNaceRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field VersionId;
            public StringField Version;
            public DateTimeField Fecha;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
