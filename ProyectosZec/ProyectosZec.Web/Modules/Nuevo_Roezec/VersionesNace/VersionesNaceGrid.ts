﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class VersionesNaceGrid extends Serenity.EntityGrid<VersionesNaceRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.VersionesNace'; }
        protected getDialogType() { return VersionesNaceDialog; }
        protected getIdProperty() { return VersionesNaceRow.idProperty; }
        protected getInsertPermission() { return VersionesNaceRow.insertPermission; }
        protected getLocalTextPrefix() { return VersionesNaceRow.localTextPrefix; }
        protected getService() { return VersionesNaceService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}