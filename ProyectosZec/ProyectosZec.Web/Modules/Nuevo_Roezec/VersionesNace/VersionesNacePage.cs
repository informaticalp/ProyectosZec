﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/VersionesNace"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.VersionesNaceRow))]
    public class VersionesNaceController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/VersionesNace/VersionesNaceIndex.cshtml");
        }
    }
}