﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.VersionesNace")]
    [BasedOnRow(typeof(Entities.VersionesNaceRow), CheckNames = true)]
    public class VersionesNaceForm
    {
        public String Version { get; set; }
        public DateTime Fecha { get; set; }
    }
}