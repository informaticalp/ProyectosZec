﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class VersionesNaceDialog extends Serenity.EntityDialog<VersionesNaceRow, any> {
        protected getFormKey() { return VersionesNaceForm.formKey; }
        protected getIdProperty() { return VersionesNaceRow.idProperty; }
        protected getLocalTextPrefix() { return VersionesNaceRow.localTextPrefix; }
        protected getNameProperty() { return VersionesNaceRow.nameProperty; }
        protected getService() { return VersionesNaceService.baseUrl; }
        protected getDeletePermission() { return VersionesNaceRow.deletePermission; }
        protected getInsertPermission() { return VersionesNaceRow.insertPermission; }
        protected getUpdatePermission() { return VersionesNaceRow.updatePermission; }

        protected form = new VersionesNaceForm(this.idPrefix);

    }
}