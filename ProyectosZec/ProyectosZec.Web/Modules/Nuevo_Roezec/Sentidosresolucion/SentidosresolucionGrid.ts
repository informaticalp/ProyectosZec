﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class SentidosresolucionGrid extends Serenity.EntityGrid<SentidosresolucionRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.Sentidosresolucion'; }
        protected getDialogType() { return SentidosresolucionDialog; }
        protected getIdProperty() { return SentidosresolucionRow.idProperty; }
        protected getInsertPermission() { return SentidosresolucionRow.insertPermission; }
        protected getLocalTextPrefix() { return SentidosresolucionRow.localTextPrefix; }
        protected getService() { return SentidosresolucionService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}