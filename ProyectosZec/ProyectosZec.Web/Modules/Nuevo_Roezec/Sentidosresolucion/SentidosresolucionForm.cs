﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.Sentidosresolucion")]
    [BasedOnRow(typeof(Entities.SentidosresolucionRow), CheckNames = true)]
    public class SentidosresolucionForm
    {
        public String SentidoResolucion { get; set; }
    }
}