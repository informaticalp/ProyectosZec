﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("sentidosresolucion")]
    [DisplayName("Sentidosresolucion"), InstanceName("Sentidosresolucion")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    [LookupScript("Nuevo_Roezec.SentidosResolucion")]
    public sealed class SentidosresolucionRow : Row, IIdRow, INameRow
    {
        [DisplayName("Sentido Resolucion Id"), Identity]
        public Int32? SentidoResolucionId
        {
            get { return Fields.SentidoResolucionId[this]; }
            set { Fields.SentidoResolucionId[this] = value; }
        }

        [DisplayName("Sentido"), Size(20), NotNull, QuickSearch]
        public String SentidoResolucion
        {
            get { return Fields.SentidoResolucion[this]; }
            set { Fields.SentidoResolucion[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.SentidoResolucionId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.SentidoResolucion; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public SentidosresolucionRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field SentidoResolucionId;
            public StringField SentidoResolucion;
        }
    }
}
