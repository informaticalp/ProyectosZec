﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/Sentidosresolucion"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.SentidosresolucionRow))]
    public class SentidosresolucionController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/Sentidosresolucion/SentidosresolucionIndex.cshtml");
        }
    }
}