﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Sentidosresolucion")]
    [BasedOnRow(typeof(Entities.SentidosresolucionRow), CheckNames = true)]
    public class SentidosresolucionColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 SentidoResolucionId { get; set; }
        [EditLink]
        public String SentidoResolucion { get; set; }
    }
}