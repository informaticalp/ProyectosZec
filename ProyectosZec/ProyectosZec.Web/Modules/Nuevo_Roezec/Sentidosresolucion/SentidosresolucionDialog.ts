﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class SentidosresolucionDialog extends Serenity.EntityDialog<SentidosresolucionRow, any> {
        protected getFormKey() { return SentidosresolucionForm.formKey; }
        protected getIdProperty() { return SentidosresolucionRow.idProperty; }
        protected getLocalTextPrefix() { return SentidosresolucionRow.localTextPrefix; }
        protected getNameProperty() { return SentidosresolucionRow.nameProperty; }
        protected getService() { return SentidosresolucionService.baseUrl; }
        protected getDeletePermission() { return SentidosresolucionRow.deletePermission; }
        protected getInsertPermission() { return SentidosresolucionRow.insertPermission; }
        protected getUpdatePermission() { return SentidosresolucionRow.updatePermission; }

        protected form = new SentidosresolucionForm(this.idPrefix);

    }
}