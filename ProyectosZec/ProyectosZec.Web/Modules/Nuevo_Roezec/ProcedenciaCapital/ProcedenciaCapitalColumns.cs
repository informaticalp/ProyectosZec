﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.ProcedenciaCapital")]
    [BasedOnRow(typeof(Entities.ProcedenciaCapitalRow), CheckNames = true)]
    public class ProcedenciaCapitalColumns
    {
        [EditLink]
        public String Pais { get; set; }
        [AlignRight,DisplayFormat("#,##0.00")]
        public Decimal Porcentaje { get; set; }     
        [Width(120)]
        public DateTime Desde { get; set; }
        [Width(120)]
        public DateTime Hasta { get; set; }
        [Width(100)]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}