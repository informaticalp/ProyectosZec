﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class ProcedenciaCapitalGrid extends Serenity.EntityGrid<ProcedenciaCapitalRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.ProcedenciaCapital'; }
        protected getDialogType() { return ProcedenciaCapitalDialog; }
        protected getIdProperty() { return ProcedenciaCapitalRow.idProperty; }
        protected getInsertPermission() { return ProcedenciaCapitalRow.insertPermission; }
        protected getLocalTextPrefix() { return ProcedenciaCapitalRow.localTextPrefix; }
        protected getService() { return ProcedenciaCapitalService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}