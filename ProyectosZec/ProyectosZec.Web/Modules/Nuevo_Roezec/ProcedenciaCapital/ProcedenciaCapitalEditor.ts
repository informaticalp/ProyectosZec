﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class ProcedenciaCapitalEditor extends Common.GridEditorBase<ProcedenciaCapitalRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.ProcedenciaCapital"; }
        protected getDialogType() { return ProcedenciaCapitalEditDialog; }
        protected getLocalTextPrefix() { return ProcedenciaCapitalRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Capital";
        }
    }
}
