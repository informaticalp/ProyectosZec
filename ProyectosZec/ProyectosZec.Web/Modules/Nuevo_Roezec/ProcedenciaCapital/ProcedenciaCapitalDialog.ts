﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class ProcedenciaCapitalDialog extends Serenity.EntityDialog<ProcedenciaCapitalRow, any> {
        protected getFormKey() { return ProcedenciaCapitalForm.formKey; }
        protected getIdProperty() { return ProcedenciaCapitalRow.idProperty; }
        protected getLocalTextPrefix() { return ProcedenciaCapitalRow.localTextPrefix; }
        protected getService() { return ProcedenciaCapitalService.baseUrl; }
        protected getDeletePermission() { return ProcedenciaCapitalRow.deletePermission; }
        protected getInsertPermission() { return ProcedenciaCapitalRow.insertPermission; }
        protected getUpdatePermission() { return ProcedenciaCapitalRow.updatePermission; }

        protected form = new ProcedenciaCapitalForm(this.idPrefix);

    }
}