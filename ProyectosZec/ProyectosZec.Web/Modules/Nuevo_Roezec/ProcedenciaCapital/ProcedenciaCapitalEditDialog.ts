﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class ProcedenciaCapitalEditDialog extends Common.GridEditorDialog<ProcedenciaCapitalRow> {
        protected getFormKey() { return ProcedenciaCapitalForm.formKey; }
        protected getNameProperty() { return ProcedenciaCapitalRow.nameProperty; }
        protected getLocalTextPrefix() { return ProcedenciaCapitalRow.localTextPrefix; }

        protected form: ProcedenciaCapitalForm;

        constructor() {
            super();
            this.form = new ProcedenciaCapitalForm(this.idPrefix);
        }
    }
}
