﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("procedencia_capital")]
    [DisplayName("Procedencia Capital"), InstanceName("Procedencia Capital")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    public sealed class ProcedenciaCapitalRow : Row, IIdRow, INameRow
    {
        [DisplayName("Procedencia Id"), Identity]
        public Int32? ProcedenciaId
        {
            get { return Fields.ProcedenciaId[this]; }
            set { Fields.ProcedenciaId[this] = value; }
        }

        [DisplayName("Empresa"), NotNull, ForeignKey("empresas", "EmpresaId"), LeftJoin("jEmpresa"), TextualField("EmpresaRazon")]
        public Int32? EmpresaId
        {
            get { return Fields.EmpresaId[this]; }
            set { Fields.EmpresaId[this] = value; }
        }

        [DisplayName("Pais"), NotNull, ForeignKey("paises", "PaisId"), LeftJoin("jPais"), TextualField("Pais")]
        [LookupEditor(typeof(Entities.PaisesRow))]
        public Int32? PaisId
        {
            get { return Fields.PaisId[this]; }
            set { Fields.PaisId[this] = value; }
        }

        [DisplayName("Porcentaje"), Size(5), Scale(2), NotNull]
        public Decimal? Porcentaje
        {
            get { return Fields.Porcentaje[this]; }
            set { Fields.Porcentaje[this] = value; }
        }

        [DisplayName("Desde")]
        public DateTime? Desde
        {
            get { return Fields.Desde[this]; }
            set { Fields.Desde[this] = value; }
        }

        [DisplayName("Hasta")]
        public DateTime? Hasta
        {
            get { return Fields.Hasta[this]; }
            set { Fields.Hasta[this] = value; }
        }

        [DisplayName("Empresa Razon"), Expression("jEmpresa.[Razon]")]
        public String EmpresaRazon
        {
            get { return Fields.EmpresaRazon[this]; }
            set { Fields.EmpresaRazon[this] = value; }
        }

        [DisplayName("Empresa Forma Juridica Id"), Expression("jEmpresa.[FormaJuridicaId]")]
        public Int32? EmpresaFormaJuridicaId
        {
            get { return Fields.EmpresaFormaJuridicaId[this]; }
            set { Fields.EmpresaFormaJuridicaId[this] = value; }
        }

        [DisplayName("Empresa Tecnico Id"), Expression("jEmpresa.[TecnicoId]")]
        public Int32? EmpresaTecnicoId
        {
            get { return Fields.EmpresaTecnicoId[this]; }
            set { Fields.EmpresaTecnicoId[this] = value; }
        }

        [DisplayName("Empresa Cif"), Expression("jEmpresa.[Cif]")]
        public String EmpresaCif
        {
            get { return Fields.EmpresaCif[this]; }
            set { Fields.EmpresaCif[this] = value; }
        }

        [DisplayName("Empresa Direccion"), Expression("jEmpresa.[Direccion]")]
        public String EmpresaDireccion
        {
            get { return Fields.EmpresaDireccion[this]; }
            set { Fields.EmpresaDireccion[this] = value; }
        }

        [DisplayName("Empresa Poblacion"), Expression("jEmpresa.[Poblacion]")]
        public String EmpresaPoblacion
        {
            get { return Fields.EmpresaPoblacion[this]; }
            set { Fields.EmpresaPoblacion[this] = value; }
        }

        [DisplayName("Empresa Cp"), Expression("jEmpresa.[CP]")]
        public Int32? EmpresaCp
        {
            get { return Fields.EmpresaCp[this]; }
            set { Fields.EmpresaCp[this] = value; }
        }

        [DisplayName("Empresa Isla Id"), Expression("jEmpresa.[IslaId]")]
        public Int32? EmpresaIslaId
        {
            get { return Fields.EmpresaIslaId[this]; }
            set { Fields.EmpresaIslaId[this] = value; }
        }

        [DisplayName("Empresa Telefono Fijo"), Expression("jEmpresa.[Telefono_fijo]")]
        public String EmpresaTelefonoFijo
        {
            get { return Fields.EmpresaTelefonoFijo[this]; }
            set { Fields.EmpresaTelefonoFijo[this] = value; }
        }

        [DisplayName("Empresa Movil"), Expression("jEmpresa.[Movil]")]
        public String EmpresaMovil
        {
            get { return Fields.EmpresaMovil[this]; }
            set { Fields.EmpresaMovil[this] = value; }
        }

        [DisplayName("Empresa Email"), Expression("jEmpresa.[Email]")]
        public String EmpresaEmail
        {
            get { return Fields.EmpresaEmail[this]; }
            set { Fields.EmpresaEmail[this] = value; }
        }

        [DisplayName("Empresa Proyecto Id"), Expression("jEmpresa.[ProyectoId]")]
        public Int32? EmpresaProyectoId
        {
            get { return Fields.EmpresaProyectoId[this]; }
            set { Fields.EmpresaProyectoId[this] = value; }
        }

        [DisplayName("Empresa Expediente"), Expression("jEmpresa.[Expediente]")]
        public String EmpresaExpediente
        {
            get { return Fields.EmpresaExpediente[this]; }
            set { Fields.EmpresaExpediente[this] = value; }
        }

        [DisplayName("Empresa Motivo Exencion"), Expression("jEmpresa.[Motivo_Exencion]")]
        public String EmpresaMotivoExencion
        {
            get { return Fields.EmpresaMotivoExencion[this]; }
            set { Fields.EmpresaMotivoExencion[this] = value; }
        }

        [DisplayName("Empresa Tipologia Capital Id"), Expression("jEmpresa.[Tipologia_CapitalId]")]
        public Int32? EmpresaTipologiaCapitalId
        {
            get { return Fields.EmpresaTipologiaCapitalId[this]; }
            set { Fields.EmpresaTipologiaCapitalId[this] = value; }
        }

        [DisplayName("Empresa Tipo Garantia Tasa Id"), Expression("jEmpresa.[Tipo_Garantia_TasaId]")]
        public Int32? EmpresaTipoGarantiaTasaId
        {
            get { return Fields.EmpresaTipoGarantiaTasaId[this]; }
            set { Fields.EmpresaTipoGarantiaTasaId[this] = value; }
        }

        [DisplayName("Empresa Empleo Traspasado"), Expression("jEmpresa.[Empleo_Traspasado]")]
        public Int32? EmpresaEmpleoTraspasado
        {
            get { return Fields.EmpresaEmpleoTraspasado[this]; }
            set { Fields.EmpresaEmpleoTraspasado[this] = value; }
        }

        [DisplayName("Empresa Empleo 6 Meses"), Expression("jEmpresa.[Empleo_6_meses]")]
        public Int32? EmpresaEmpleo6Meses
        {
            get { return Fields.EmpresaEmpleo6Meses[this]; }
            set { Fields.EmpresaEmpleo6Meses[this] = value; }
        }

        [DisplayName("Empresa Empleo Promedio"), Expression("jEmpresa.[Empleo_promedio]")]
        public Int32? EmpresaEmpleoPromedio
        {
            get { return Fields.EmpresaEmpleoPromedio[this]; }
            set { Fields.EmpresaEmpleoPromedio[this] = value; }
        }

        [DisplayName("Empresa Empleo Promedio 2 Anos"), Expression("jEmpresa.[Empleo_promedio_2_anos]")]
        public Int32? EmpresaEmpleoPromedio2Anos
        {
            get { return Fields.EmpresaEmpleoPromedio2Anos[this]; }
            set { Fields.EmpresaEmpleoPromedio2Anos[this] = value; }
        }

        [DisplayName("Empresa Inversion Traspasada"), Expression("jEmpresa.[Inversion_traspasada]")]
        public Decimal? EmpresaInversionTraspasada
        {
            get { return Fields.EmpresaInversionTraspasada[this]; }
            set { Fields.EmpresaInversionTraspasada[this] = value; }
        }

        [DisplayName("Empresa Inversion 2 Anos"), Expression("jEmpresa.[Inversion_2_anos]")]
        public Decimal? EmpresaInversion2Anos
        {
            get { return Fields.EmpresaInversion2Anos[this]; }
            set { Fields.EmpresaInversion2Anos[this] = value; }
        }

        [DisplayName("Empresa Estado Empresa Id"), Expression("jEmpresa.[EstadoEmpresaId]")]
        public Int32? EmpresaEstadoEmpresaId
        {
            get { return Fields.EmpresaEstadoEmpresaId[this]; }
            set { Fields.EmpresaEstadoEmpresaId[this] = value; }
        }

        [DisplayName("Empresa Fecha Cambio Estado"), Expression("jEmpresa.[Fecha_Cambio_Estado]")]
        public DateTime? EmpresaFechaCambioEstado
        {
            get { return Fields.EmpresaFechaCambioEstado[this]; }
            set { Fields.EmpresaFechaCambioEstado[this] = value; }
        }

        [DisplayName("Empresa Num Tasa Liquidacion"), Expression("jEmpresa.[Num_Tasa_Liquidacion]")]
        public String EmpresaNumTasaLiquidacion
        {
            get { return Fields.EmpresaNumTasaLiquidacion[this]; }
            set { Fields.EmpresaNumTasaLiquidacion[this] = value; }
        }

        [DisplayName("Pais"), Expression("jPais.[Pais]")]
        public String Pais
        {
            get { return Fields.Pais[this]; }
            set { Fields.Pais[this] = value; }
        }

        [DisplayName("Pais Capital"), Expression("jPais.[Capital]")]
        public String PaisCapital
        {
            get { return Fields.PaisCapital[this]; }
            set { Fields.PaisCapital[this] = value; }
        }

        [DisplayName("Pais Continente Id"), Expression("jPais.[ContinenteId]")]
        public Int32? PaisContinenteId
        {
            get { return Fields.PaisContinenteId[this]; }
            set { Fields.PaisContinenteId[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        IIdField IIdRow.IdField
        {
            get { return Fields.ProcedenciaId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.EmpresaRazon; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public ProcedenciaCapitalRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field ProcedenciaId;
            public Int32Field EmpresaId;
            public Int32Field PaisId;
            public DecimalField Porcentaje;
            public DateTimeField Desde;
            public DateTimeField Hasta;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/

            public StringField EmpresaRazon;
            public Int32Field EmpresaFormaJuridicaId;
            public Int32Field EmpresaTecnicoId;
            public StringField EmpresaCif;
            public StringField EmpresaDireccion;
            public StringField EmpresaPoblacion;
            public Int32Field EmpresaCp;
            public Int32Field EmpresaIslaId;
            public StringField EmpresaTelefonoFijo;
            public StringField EmpresaMovil;
            public StringField EmpresaEmail;
            public Int32Field EmpresaProyectoId;
            public StringField EmpresaExpediente;
            public StringField EmpresaMotivoExencion;
            public Int32Field EmpresaTipologiaCapitalId;
            public Int32Field EmpresaTipoGarantiaTasaId;
            public Int32Field EmpresaEmpleoTraspasado;
            public Int32Field EmpresaEmpleo6Meses;
            public Int32Field EmpresaEmpleoPromedio;
            public Int32Field EmpresaEmpleoPromedio2Anos;
            public DecimalField EmpresaInversionTraspasada;
            public DecimalField EmpresaInversion2Anos;
            public Int32Field EmpresaEstadoEmpresaId;
            public DateTimeField EmpresaFechaCambioEstado;
            public StringField EmpresaNumTasaLiquidacion;

            public StringField Pais;
            public StringField PaisCapital;
            public Int32Field PaisContinenteId;
        }
    }
}
