﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/ProcedenciaCapital"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.ProcedenciaCapitalRow))]
    public class ProcedenciaCapitalController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/ProcedenciaCapital/ProcedenciaCapitalIndex.cshtml");
        }
    }
}