﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.ProcedenciaCapital")]
    [BasedOnRow(typeof(Entities.ProcedenciaCapitalRow), CheckNames = true)]
    public class ProcedenciaCapitalForm
    {
        
        public Int32 PaisId { get; set; }
        public Decimal Porcentaje { get; set; }
       
        public DateTime Desde { get; set; }
        
        public DateTime Hasta { get; set; }
    }
}