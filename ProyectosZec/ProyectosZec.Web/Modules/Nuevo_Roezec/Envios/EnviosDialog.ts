﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EnviosDialog extends Serenity.EntityDialog<EnviosRow, any> {
        protected getFormKey() { return EnviosForm.formKey; }
        protected getIdProperty() { return EnviosRow.idProperty; }
        protected getLocalTextPrefix() { return EnviosRow.localTextPrefix; }
        protected getService() { return EnviosService.baseUrl; }
        protected getDeletePermission() { return EnviosRow.deletePermission; }
        protected getInsertPermission() { return EnviosRow.insertPermission; }
        protected getUpdatePermission() { return EnviosRow.updatePermission; }

        protected form = new EnviosForm(this.idPrefix);

    }
}