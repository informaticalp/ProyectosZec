﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EnviosGrid extends Serenity.EntityGrid<EnviosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.Envios'; }
        protected getDialogType() { return EnviosDialog; }
        protected getIdProperty() { return EnviosRow.idProperty; }
        protected getInsertPermission() { return EnviosRow.insertPermission; }
        protected getLocalTextPrefix() { return EnviosRow.localTextPrefix; }
        protected getService() { return EnviosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}