﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EnviosEditDialog extends Common.GridEditorDialog<EnviosRow> {
        protected getFormKey() { return EnviosForm.formKey; }
        protected getNameProperty() { return EnviosRow.nameProperty; }
        protected getLocalTextPrefix() { return EnviosRow.localTextPrefix; }

        protected form: EnviosForm;

        constructor() {
            super();
            this.form = new EnviosForm(this.idPrefix);
        }
    }
}