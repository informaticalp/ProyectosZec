﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Envios")]
    [BasedOnRow(typeof(Entities.EnviosRow), CheckNames = true)]
    public class EnviosColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        [Width(50)]
        public Int32 EnvioId { get; set; }
        //public String HistorialId { get; set; }
        [Width(120),EditLink]
        public String TipoEnvioTipo { get; set; }
        [Width(120)]
        public String EstadoEnvioEstado { get; set; }
        [Width(120)]
        public String FormaEnvioForma { get; set; }
        [Width(120),DisplayFormat("g")]
        public DateTime FechaEnvio { get; set; }
        [Width(200)]
        public String ContactoEnvioFullName { get; set; }
        [Width(120), DisplayFormat("g")]
        public DateTime FechaRecepcion { get; set; }
        [Width(200)]
        public String ContactoAcuseFullName { get; set; }
        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }

    }
}