﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("envios")]
    [DisplayName("Envios"), InstanceName("Envios")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    public sealed class EnviosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Envio Id"), Identity]
        public Int32? EnvioId
        {
            get { return Fields.EnvioId[this]; }
            set { Fields.EnvioId[this] = value; }
        }

        [DisplayName("Historial"), NotNull, ForeignKey("historial_empresas", "HistorialId"), LeftJoin("jHistorial"), TextualField("HistorialObservaciones")]
        public Int32? HistorialId
        {
            get { return Fields.HistorialId[this]; }
            set { Fields.HistorialId[this] = value; }
        }

        [DisplayName("Tipo Envio"), NotNull, ForeignKey("tipos_envio", "TipoEnvioId"), LeftJoin("jTipoEnvio"), TextualField("TipoEnvioTipo")]
        [LookupEditor(typeof(Entities.TiposEnvioRow))]
        public Int32? TipoEnvioId
        {
            get { return Fields.TipoEnvioId[this]; }
            set { Fields.TipoEnvioId[this] = value; }
        }

        [DisplayName("Forma Envio"), NotNull, ForeignKey("formas_envio", "FormaEnvioId"), LeftJoin("jFormaEnvio"), TextualField("FormaEnvioForma")]
        [LookupEditor(typeof(Entities.FormasEnvioRow))]
        public Int32? FormaEnvioId
        {
            get { return Fields.FormaEnvioId[this]; }
            set { Fields.FormaEnvioId[this] = value; }
        }

        [DisplayName("Estado Envio"), NotNull, ForeignKey("estados_envio", "EstadoEnvioId"), LeftJoin("jEstadoEnvio"), TextualField("EstadoEnvioEstado")]
        [LookupEditor(typeof(Entities.EstadosEnvioRow))]
        public Int32? EstadoEnvioId
        {
            get { return Fields.EstadoEnvioId[this]; }
            set { Fields.EstadoEnvioId[this] = value; }
        }

        [DisplayName("Fecha Envio"), NotNull]
        public DateTime? FechaEnvio
        {
            get { return Fields.FechaEnvio[this]; }
            set { Fields.FechaEnvio[this] = value; }
        }

        [DisplayName("Contacto Envio"), NotNull, ForeignKey("contactos", "ContactoId"), LeftJoin("jContactoEnvio"), TextualField("ContactoEnvioFullName")]
        [LookupEditor(typeof(Entities.ContactosRow))]
        public Int32? ContactoEnvioId
        {
            get { return Fields.ContactoEnvioId[this]; }
            set { Fields.ContactoEnvioId[this] = value; }
        }

        [DisplayName("Fecha Recepcion")]
        public DateTime? FechaRecepcion
        {
            get { return Fields.FechaRecepcion[this]; }
            set { Fields.FechaRecepcion[this] = value; }
        }

        [DisplayName("Contacto Acuse"), ForeignKey("contactos", "ContactoId"), LeftJoin("jContactoAcuse"), TextualField("ContactoAcuseFullName")]
        [LookupEditor(typeof(Entities.ContactosRow))]
        public Int32? ContactoAcuseId
        {
            get { return Fields.ContactoAcuseId[this]; }
            set { Fields.ContactoAcuseId[this] = value; }
        }

        [DisplayName("Tipo Envio"), Expression("jTipoEnvio.[Tipo]"), MinSelectLevel(SelectLevel.List)]
        public String TipoEnvioTipo
        {
            get { return Fields.TipoEnvioTipo[this]; }
            set { Fields.TipoEnvioTipo[this] = value; }
        }

        [DisplayName("Forma Envio"), Expression("jFormaEnvio.[Forma]"), MinSelectLevel(SelectLevel.List)]
        public String FormaEnvioForma
        {
            get { return Fields.FormaEnvioForma[this]; }
            set { Fields.FormaEnvioForma[this] = value; }
        }

        [DisplayName("Estado Envio"), Expression("jEstadoEnvio.[Estado]"), MinSelectLevel(SelectLevel.List)]
        public String EstadoEnvioEstado
        {
            get { return Fields.EstadoEnvioEstado[this]; }
            set { Fields.EstadoEnvioEstado[this] = value; }
        }

        [DisplayName("Contacto Envio"), Expression("jContactoEnvio.[Nombre]")]
        public String ContactoEnvioNombre
        {
            get { return Fields.ContactoEnvioNombre[this]; }
            set { Fields.ContactoEnvioNombre[this] = value; }
        }

        [DisplayName("Contacto Envio Apellidos"), Expression("jContactoEnvio.[Apellidos]")]
        public String ContactoEnvioApellidos
        {
            get { return Fields.ContactoEnvioApellidos[this]; }
            set { Fields.ContactoEnvioApellidos[this] = value; }
        }
        [DisplayName("Contacto Envio"), MinSelectLevel(SelectLevel.List)]
        [Expression("(CONCAT(jContactoEnvio.[Nombre],' ',jContactoEnvio.[Apellidos]))")]
        [Expression("(jContactoEnvio.Nombre || ' ' || jContactoEnvio.Apellidos)", Dialect = "Sqlite")]
        public String ContactoEnvioFullName
        {
            get { return Fields.ContactoEnvioFullName[this]; }
            set { Fields.ContactoEnvioFullName[this] = value; }
        }
        [DisplayName("Contacto Acuse"), MinSelectLevel(SelectLevel.List)]
        [Expression("(CONCAT(jContactoAcuse.[Nombre],' ',jContactoAcuse.[Apellidos]))")]
        [Expression("(jContactoAcuse.Nombre || ' ' || jContactoAcuse.Apellidos)", Dialect = "Sqlite")]
        public String ContactoAcuseFullName
        {
            get { return Fields.ContactoAcuseFullName[this]; }
            set { Fields.ContactoAcuseFullName[this] = value; }
        }

        [DisplayName("Contacto Envio Nif"), Expression("jContactoEnvio.[Nif]")]
        public String ContactoEnvioNif
        {
            get { return Fields.ContactoEnvioNif[this]; }
            set { Fields.ContactoEnvioNif[this] = value; }
        }

        [DisplayName("Contacto Envio Telefono Fijo"), Expression("jContactoEnvio.[Telefono_fijo]")]
        public String ContactoEnvioTelefonoFijo
        {
            get { return Fields.ContactoEnvioTelefonoFijo[this]; }
            set { Fields.ContactoEnvioTelefonoFijo[this] = value; }
        }

        [DisplayName("Contacto Envio Movil"), Expression("jContactoEnvio.[Movil]")]
        public String ContactoEnvioMovil
        {
            get { return Fields.ContactoEnvioMovil[this]; }
            set { Fields.ContactoEnvioMovil[this] = value; }
        }

        [DisplayName("Contacto Envio Idioma"), Expression("jContactoEnvio.[IdiomaId]")]
        public Int32? ContactoEnvioIdiomaId
        {
            get { return Fields.ContactoEnvioIdiomaId[this]; }
            set { Fields.ContactoEnvioIdiomaId[this] = value; }
        }

        [DisplayName("Contacto Envio Email"), Expression("jContactoEnvio.[Email]")]
        public String ContactoEnvioEmail
        {
            get { return Fields.ContactoEnvioEmail[this]; }
            set { Fields.ContactoEnvioEmail[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        IIdField IIdRow.IdField
        {
            get { return Fields.EnvioId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.ContactoEnvioFullName; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public EnviosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field EnvioId;
            public Int32Field HistorialId;
            public Int32Field TipoEnvioId;
            public Int32Field FormaEnvioId;
            public Int32Field EstadoEnvioId;
            public DateTimeField FechaEnvio;
            public Int32Field ContactoEnvioId;
            public DateTimeField FechaRecepcion;
            public Int32Field ContactoAcuseId;

            public StringField TipoEnvioTipo;

            public StringField ContactoEnvioNombre;
            public StringField ContactoEnvioApellidos;
            public StringField ContactoEnvioNif;
            public StringField ContactoEnvioTelefonoFijo;
            public StringField ContactoEnvioMovil;
            public Int32Field ContactoEnvioIdiomaId;
            public StringField ContactoEnvioEmail;
            public StringField ContactoEnvioFullName;
            public StringField ContactoAcuseFullName;

            public StringField FormaEnvioForma;
            public StringField EstadoEnvioEstado;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
