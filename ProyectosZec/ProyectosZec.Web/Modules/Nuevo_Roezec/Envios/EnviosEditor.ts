﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />


namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EnviosEditor extends Common.GridEditorBase<EnviosRow> {
        /*protected getPersistanceStorage(): Serenity.SettingStorage { return new Common.UserPreferenceStorage(); }*/
        protected getColumnsKey() { return "Nuevo_Roezec.Envios"; }
        protected getDialogType() { return EnviosEditDialog; }
        protected getLocalTextPrefix() { return EnviosRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Envío";
        }
    }
}