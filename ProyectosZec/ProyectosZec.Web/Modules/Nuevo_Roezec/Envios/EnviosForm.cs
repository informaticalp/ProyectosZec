﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.Envios")]
    [BasedOnRow(typeof(Entities.EnviosRow), CheckNames = true)]
    public class EnviosForm
    {
        //public Int32 HistorialId { get; set; }
        public Int32 TipoEnvioId { get; set; }
        public Int32 FormaEnvioId { get; set; }
        public Int32 EstadoEnvioId { get; set; }
        public DateTime FechaEnvio { get; set; }
        public Int32 ContactoEnvioId { get; set; }
        public DateTime FechaRecepcion { get; set; }
        public Int32 ContactoAcuseId { get; set; }
    }
}