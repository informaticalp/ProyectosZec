﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/Envios"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EnviosRow))]
    public class EnviosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/Envios/EnviosIndex.cshtml");
        }
    }
}