﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Alarmas")]
    [BasedOnRow(typeof(Entities.AlarmasRow), CheckNames = true)]
    public class AlarmasColumns
    {
        [Width(60),EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 AlarmaId { get; set; }
        [Width(250),EditLink,QuickFilter]
        public String TipoAlarmaTexto { get; set; }
        [Width(300),EditLink]
        public String EmpresaRazon { get; set; }
        [Width(100),DisplayName("Exp. Antiguo")]
        public String EmpresaExpediente { get; set; }
        [Width(100)]
        public String ExpedienteNuevo { get; set; }
        [Width(130), DisplayFormat("d"),QuickFilter]
        public DateTime FechaCaducidad { get; set; }
        [Width(130), DisplayFormat("d"),QuickFilter]
        public DateTime FechaAviso { get; set; }
        [Width(80),AlignCenter,QuickFilter]
        public Boolean Activa { get; set; }
        [Width(130), DisplayFormat("g"),Hidden]
        public DateTime FechaCreacion { get; set; }

        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    } 
}