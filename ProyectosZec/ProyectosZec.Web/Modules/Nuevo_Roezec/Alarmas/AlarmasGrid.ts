﻿
namespace ProyectosZec.Nuevo_Roezec {
    import fld = AlarmasRow.Fields;
    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido

    export class AlarmasGrid extends Serenity.EntityGrid<AlarmasRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.Alarmas'; }
        protected getDialogType() { return AlarmasDialog; }
        protected getIdProperty() { return AlarmasRow.idProperty; }
        protected getInsertPermission() { return AlarmasRow.insertPermission; }
        protected getLocalTextPrefix() { return AlarmasRow.localTextPrefix; }
        protected getService() { return AlarmasService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Nuevo_Roezec/Alarmas/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            return buttons;
        }

        // Añadir la siguiente función
        protected getDefaultSortBy() {
            return [AlarmasRow.Fields.FechaAviso]; // Este es el campo de ordenación por defecto
        }

        /**
         * We override getColumns() to change format functions for some columns.
         * You could also write them as formatter classes, and use them at server side
         */
        protected getColumns(): Slick.Column[] {
            var columns = super.getColumns();

            Q.first(columns, x => x.field == fld.EmpresaRazon).format =
                ctx => `<a href="javascript:;" class="empresa-link">${Q.htmlEncode(ctx.value)}</a>`;

            return columns;
        }
        protected onClick(e: JQueryEventObject, row: number, cell: number): void {

            // let base grid handle clicks for its edit links
            super.onClick(e, row, cell);

            // if base grid already handled, we shouldn"t handle it again
            if (e.isDefaultPrevented()) {
                return;
            }

            // get reference to current item
            var item = this.itemAt(row);

            // get reference to clicked element
            var target = $(e.target);



            if (target.hasClass("empresa-link")) {
                e.preventDefault();
                new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
            //    let message = Q.format(
            //        "<p>Has pulsado sobre la la empresa {0}.</p>" +
            //        "<p>Si pulsas sobre Si, abrimos la empresa.</p>" +
            //        "<p>Si pulsas NO, abrimos la alarma.</p>",
            //        Q.htmlEncode(item.EmpresaRazon));

            //    Q.confirm(message, () => {
            //        new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
            //    },
            //        {
            //            htmlEncode: false,
            //            onNo: () => {
            //                new Nuevo_Roezec.AlarmasDialog().loadByIdAndOpenDialog(item.AlarmaId);
            //            }
            //        });
            }
    }
        /**
        * This method is called for all rows
        * @param item Data item for current row
        * @param index Index of the row in grid
        */
        protected getItemCssClass(item: Nuevo_Roezec.AlarmasRow, index: number): string {
            // hoy is Currentdate
            var hoy = new Date();
            let klass: string = ""; // Default
            
            if (item.Activa == 0)
                klass += " discontinued";
            else if (Q.formatDate(item.FechaCaducidad, 'yyyy-MM-dd') <= Q.formatDate(hoy, 'yyyy-MM-dd'))
                klass += " out-of-stock";
            else if (Q.formatDate(item.FechaAviso, 'yyyy-MM-dd') <= Q.formatDate(hoy, 'yyyy-MM-dd')) 
                klass += " critical-stock";

            return Q.trimToNull(klass);
        }

        /**
 * This method is called to get list of quick filters to be created for this grid.
 * By default, it returns quick filter objects corresponding to properties that
 * have a [QuickFilter] attribute at server side OrderColumns.cs
 */
        protected getQuickFilters(): Serenity.QuickFilter<Serenity.Widget<any>, any>[] {

            // get quick filter list from base class
            let filters = super.getQuickFilters();
            // Current month & year
            let CurYear = new Date().getFullYear();
            let CurMonth = new Date().getMonth();

            let StartMonth = CurMonth-2; // Miramos los 2 meses anteriores a hoy
            let EndMonth = CurMonth + 6; // y los 6 siguientes al mes actual
            let StartYear = CurYear;
            let EndYear = CurYear;

            if (StartMonth < 0) {        // OJO que Javascript los meses empiezan en el 0
                StartMonth = 11-StartMonth;
                StartYear -= 1;
            }

            if (EndMonth > 11) {
                EndYear += 1;
                EndMonth -= 11;
            }
            // quick filter init method is a good place to set initial
            // value for a quick filter editor, just after it is created

            Q.first(filters, x => x.field == fld.FechaAviso).init = w => {
                // w is a reference to the editor for this quick filter widget
                // here we cast it to DateEditor, and set its value as date.
                // note that in Javascript, months are 0 based, so date below
                // is actually 2016-05-01
                (w as Serenity.DateEditor).valueAsDate = new Date(StartYear, StartMonth, 1);

                // setting start date was simple. but this quick filter is actually
                // a combination of two date editors. to get reference to second one,
                // need to find its next sibling element by its class
                let endDate = w.element.nextAll(".s-DateEditor").getWidget(Serenity.DateEditor);
                endDate.valueAsDate = new Date(EndYear, EndMonth, 1);
            };



            Q.first(filters, x => x.field == fld.Activa).init = w => {
 
                (w as Serenity.IntegerEditor).value = 1;
            };


            return filters;
        }
        // Fin añadidos
    }
}