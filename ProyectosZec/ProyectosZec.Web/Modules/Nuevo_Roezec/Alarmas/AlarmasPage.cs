﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/Alarmas"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.AlarmasRow))]
    public class AlarmasController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/Alarmas/AlarmasIndex.cshtml");
        }
    }
}