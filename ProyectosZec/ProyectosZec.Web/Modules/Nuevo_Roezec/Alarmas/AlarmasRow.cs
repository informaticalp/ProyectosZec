﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("alarmas")]
    [DisplayName("Alarmas"), InstanceName("Alarmas")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    public sealed class AlarmasRow : Row, IIdRow
    {
        [DisplayName("Alarma Id"), Identity]
        public Int32? AlarmaId
        {
            get { return Fields.AlarmaId[this]; }
            set { Fields.AlarmaId[this] = value; }
        }

        [DisplayName("Tipo Alarma"), NotNull, ForeignKey("tipos_alarma", "TipoAlarmaId"), LeftJoin("jTipoAlarma"), TextualField("TipoAlarmaTexto")]
        [LookupEditor(typeof(Entities.TiposAlarmaRow), InplaceAdd = true)]
        public Int32? TipoAlarmaId
        {
            get { return Fields.TipoAlarmaId[this]; }
            set { Fields.TipoAlarmaId[this] = value; }
        }

        [DisplayName("Empresa"), ForeignKey("empresas", "EmpresaId"), LeftJoin("jEmpresa"), TextualField("EmpresaRazon")]
        [LookupEditor(typeof(Entities.EmpresasRow))]
        public Int32? EmpresaId
        {
            get { return Fields.EmpresaId[this]; }
            set { Fields.EmpresaId[this] = value; }
        }

        [DisplayName("Activa"), NotNull]
        public Boolean? Activa
        {
            get { return Fields.Activa[this]; }
            set { Fields.Activa[this] = value; }
        }

        [DisplayName("Fecha Creacion")]
        public DateTime? FechaCreacion
        {
            get { return Fields.FechaCreacion[this]; }
            set { Fields.FechaCreacion[this] = value; }
        }
        [DisplayName("Fecha Caducidad"), NotNull]
        public DateTime? FechaCaducidad
        {
            get { return Fields.FechaCaducidad[this]; }
            set { Fields.FechaCaducidad[this] = value; }
        }
        [DisplayName("Fecha Aviso"), NotNull]
        public DateTime? FechaAviso
        {
            get { return Fields.FechaAviso[this]; }
            set { Fields.FechaAviso[this] = value; }
        }

        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        [DisplayName("Tipo Alarma"), Expression("jTipoAlarma.[Texto]")]
        public String TipoAlarmaTexto
        {
            get { return Fields.TipoAlarmaTexto[this]; }
            set { Fields.TipoAlarmaTexto[this] = value; }
        }

        [DisplayName("Tipo Alarma Dias"), Expression("jTipoAlarma.[Dias]")]
        public Int32? TipoAlarmaDias
        {
            get { return Fields.TipoAlarmaDias[this]; }
            set { Fields.TipoAlarmaDias[this] = value; }
        }

        [DisplayName("Tipo Alarma Dias Aviso"), Expression("jTipoAlarma.[DiasAviso]")]
        public Int32? TipoAlarmaDiasAviso
        {
            get { return Fields.TipoAlarmaDiasAviso[this]; }
            set { Fields.TipoAlarmaDiasAviso[this] = value; }
        }

        [DisplayName("Tipo Alarma Email"), Expression("jTipoAlarma.[Email]")]
        public String TipoAlarmaEmail
        {
            get { return Fields.TipoAlarmaEmail[this]; }
            set { Fields.TipoAlarmaEmail[this] = value; }
        }

        [DisplayName("Tipo Alarma User Id"), Expression("jTipoAlarma.[UserId]")]
        public Int32? TipoAlarmaUserId
        {
            get { return Fields.TipoAlarmaUserId[this]; }
            set { Fields.TipoAlarmaUserId[this] = value; }
        }

        [DisplayName("Tipo Alarma Fecha Modificacion"), Expression("jTipoAlarma.[FechaModificacion]")]
        public DateTime? TipoAlarmaFechaModificacion
        {
            get { return Fields.TipoAlarmaFechaModificacion[this]; }
            set { Fields.TipoAlarmaFechaModificacion[this] = value; }
        }

        [DisplayName("Empresa"), Expression("jEmpresa.[Razon]"),QuickSearch]
        public String EmpresaRazon
        {
            get { return Fields.EmpresaRazon[this]; }
            set { Fields.EmpresaRazon[this] = value; }
        }

        [DisplayName("Expediente"), Size(25), Expression("(CONCAT(YEAR(jEmpresa.[FechaAlta]),'/',LPAD(jEmpresa.[EmpresaId],5,0)))"), QuickSearch]
        public String ExpedienteNuevo
        {
            get { return Fields.ExpedienteNuevo[this]; }
            set { Fields.ExpedienteNuevo[this] = value; }
        }

        [DisplayName("Empresa Forma Juridica Id"), Expression("jEmpresa.[FormaJuridicaId]")]
        public Int32? EmpresaFormaJuridicaId
        {
            get { return Fields.EmpresaFormaJuridicaId[this]; }
            set { Fields.EmpresaFormaJuridicaId[this] = value; }
        }

        [DisplayName("Empresa Tecnico Id"), Expression("jEmpresa.[TecnicoId]")]
        public Int32? EmpresaTecnicoId
        {
            get { return Fields.EmpresaTecnicoId[this]; }
            set { Fields.EmpresaTecnicoId[this] = value; }
        }

        [DisplayName("Empresa Isla Id"), Expression("jEmpresa.[IslaId]")]
        public Int32? EmpresaIslaId
        {
            get { return Fields.EmpresaIslaId[this]; }
            set { Fields.EmpresaIslaId[this] = value; }
        }


        [DisplayName("Empresa Capital Social"), Expression("jEmpresa.[CapitalSocial]")]
        public Decimal? EmpresaCapitalSocial
        {
            get { return Fields.EmpresaCapitalSocial[this]; }
            set { Fields.EmpresaCapitalSocial[this] = value; }
        }

        [DisplayName("Empresa Proyecto Id"), Expression("jEmpresa.[ProyectoId]")]
        public Int32? EmpresaProyectoId
        {
            get { return Fields.EmpresaProyectoId[this]; }
            set { Fields.EmpresaProyectoId[this] = value; }
        }

        [DisplayName("Empresa Numero Roezec"), Expression("jEmpresa.[NumeroRoezec]")]
        public Int32? EmpresaNumeroRoezec
        {
            get { return Fields.EmpresaNumeroRoezec[this]; }
            set { Fields.EmpresaNumeroRoezec[this] = value; }
        }

        [DisplayName("Exp. Antiguo"), Expression("jEmpresa.[Expediente]"),QuickSearch]
        public String EmpresaExpediente
        {
            get { return Fields.EmpresaExpediente[this]; }
            set { Fields.EmpresaExpediente[this] = value; }
        }

        [DisplayName("Empresa Motivo Exencion"), Expression("jEmpresa.[Motivo_Exencion]")]
        public String EmpresaMotivoExencion
        {
            get { return Fields.EmpresaMotivoExencion[this]; }
            set { Fields.EmpresaMotivoExencion[this] = value; }
        }

        [DisplayName("Empresa Tipologia Capital Id"), Expression("jEmpresa.[Tipologia_CapitalId]")]
        public Int32? EmpresaTipologiaCapitalId
        {
            get { return Fields.EmpresaTipologiaCapitalId[this]; }
            set { Fields.EmpresaTipologiaCapitalId[this] = value; }
        }

        [DisplayName("Empresa Tipo Garantia Tasa Id"), Expression("jEmpresa.[Tipo_Garantia_TasaId]")]
        public Int32? EmpresaTipoGarantiaTasaId
        {
            get { return Fields.EmpresaTipoGarantiaTasaId[this]; }
            set { Fields.EmpresaTipoGarantiaTasaId[this] = value; }
        }

        [DisplayName("Empresa Empleo Traspasado"), Expression("jEmpresa.[Empleo_Traspasado]")]
        public Int32? EmpresaEmpleoTraspasado
        {
            get { return Fields.EmpresaEmpleoTraspasado[this]; }
            set { Fields.EmpresaEmpleoTraspasado[this] = value; }
        }

        [DisplayName("Empresa Empleo 6 Meses"), Expression("jEmpresa.[Empleo_6_meses]")]
        public Int32? EmpresaEmpleo6Meses
        {
            get { return Fields.EmpresaEmpleo6Meses[this]; }
            set { Fields.EmpresaEmpleo6Meses[this] = value; }
        }

        [DisplayName("Empresa Empleo Promedio"), Expression("jEmpresa.[Empleo_promedio]")]
        public Int32? EmpresaEmpleoPromedio
        {
            get { return Fields.EmpresaEmpleoPromedio[this]; }
            set { Fields.EmpresaEmpleoPromedio[this] = value; }
        }

        [DisplayName("Empresa Empleo Promedio 2 Anos"), Expression("jEmpresa.[Empleo_promedio_2_anos]")]
        public Int32? EmpresaEmpleoPromedio2Anos
        {
            get { return Fields.EmpresaEmpleoPromedio2Anos[this]; }
            set { Fields.EmpresaEmpleoPromedio2Anos[this] = value; }
        }

        [DisplayName("Empresa Inversion Traspasada"), Expression("jEmpresa.[Inversion_traspasada]")]
        public Decimal? EmpresaInversionTraspasada
        {
            get { return Fields.EmpresaInversionTraspasada[this]; }
            set { Fields.EmpresaInversionTraspasada[this] = value; }
        }

        [DisplayName("Empresa Inversion 2 Anos"), Expression("jEmpresa.[Inversion_2_anos]")]
        public Decimal? EmpresaInversion2Anos
        {
            get { return Fields.EmpresaInversion2Anos[this]; }
            set { Fields.EmpresaInversion2Anos[this] = value; }
        }

        [DisplayName("Empresa Estado Empresa Id"), Expression("jEmpresa.[EstadoEmpresaId]")]
        public Int32? EmpresaEstadoEmpresaId
        {
            get { return Fields.EmpresaEstadoEmpresaId[this]; }
            set { Fields.EmpresaEstadoEmpresaId[this] = value; }
        }

        [DisplayName("Empresa Fecha Cambio Estado"), Expression("jEmpresa.[Fecha_Cambio_Estado]")]
        public DateTime? EmpresaFechaCambioEstado
        {
            get { return Fields.EmpresaFechaCambioEstado[this]; }
            set { Fields.EmpresaFechaCambioEstado[this] = value; }
        }

        [DisplayName("Empresa Num Tasa Liquidacion"), Expression("jEmpresa.[Num_Tasa_Liquidacion]")]
        public String EmpresaNumTasaLiquidacion
        {
            get { return Fields.EmpresaNumTasaLiquidacion[this]; }
            set { Fields.EmpresaNumTasaLiquidacion[this] = value; }
        }

        [DisplayName("Empresa User Id"), Expression("jEmpresa.[UserId]")]
        public Int32? EmpresaUserId
        {
            get { return Fields.EmpresaUserId[this]; }
            set { Fields.EmpresaUserId[this] = value; }
        }

        [DisplayName("Empresa Fecha Modificacion"), Expression("jEmpresa.[FechaModificacion]")]
        public DateTime? EmpresaFechaModificacion
        {
            get { return Fields.EmpresaFechaModificacion[this]; }
            set { Fields.EmpresaFechaModificacion[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.AlarmaId; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AlarmasRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field AlarmaId;
            public Int32Field TipoAlarmaId;
            public Int32Field EmpresaId;
            public BooleanField Activa;
            public DateTimeField FechaCreacion;
            public DateTimeField FechaCaducidad;
            public DateTimeField FechaAviso;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/

            public StringField TipoAlarmaTexto;
            public Int32Field TipoAlarmaDias;
            public Int32Field TipoAlarmaDiasAviso;
            public StringField TipoAlarmaEmail;
            public Int32Field TipoAlarmaUserId;
            public DateTimeField TipoAlarmaFechaModificacion;

            public StringField EmpresaRazon;
            public StringField ExpedienteNuevo;
            public Int32Field EmpresaFormaJuridicaId;
            public Int32Field EmpresaTecnicoId;
            public Int32Field EmpresaIslaId;
            public DecimalField EmpresaCapitalSocial;
            public Int32Field EmpresaProyectoId;
            public Int32Field EmpresaNumeroRoezec;
            public StringField EmpresaExpediente;
            public StringField EmpresaMotivoExencion;
            public Int32Field EmpresaTipologiaCapitalId;
            public Int32Field EmpresaTipoGarantiaTasaId;
            public Int32Field EmpresaEmpleoTraspasado;
            public Int32Field EmpresaEmpleo6Meses;
            public Int32Field EmpresaEmpleoPromedio;
            public Int32Field EmpresaEmpleoPromedio2Anos;
            public DecimalField EmpresaInversionTraspasada;
            public DecimalField EmpresaInversion2Anos;
            public Int32Field EmpresaEstadoEmpresaId;
            public DateTimeField EmpresaFechaCambioEstado;
            public StringField EmpresaNumTasaLiquidacion;
            public Int32Field EmpresaUserId;
            public DateTimeField EmpresaFechaModificacion;
        }
    }
}
