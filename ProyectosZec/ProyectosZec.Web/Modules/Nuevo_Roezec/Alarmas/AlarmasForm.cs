﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.Alarmas")]
    [BasedOnRow(typeof(Entities.AlarmasRow), CheckNames = true)]
    public class AlarmasForm
    {
        public Int32 TipoAlarmaId { get; set; }
        public Int32 EmpresaId { get; set; }
        public Boolean Activa { get; set; }
        //public DateTime FechaCreacion { get; set; }
        public DateTime FechaAviso { get; set; }
        public DateTime FechaCaducidad { get; set; }
        //public Int32 UserId { get; set; }
        //public DateTime FechaModificacion { get; set; }
    }
}