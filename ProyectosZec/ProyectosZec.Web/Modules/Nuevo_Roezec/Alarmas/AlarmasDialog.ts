﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class AlarmasDialog extends Serenity.EntityDialog<AlarmasRow, any> {
        protected getFormKey() { return AlarmasForm.formKey; }
        protected getIdProperty() { return AlarmasRow.idProperty; }
        protected getLocalTextPrefix() { return AlarmasRow.localTextPrefix; }
        protected getService() { return AlarmasService.baseUrl; }
        protected getDeletePermission() { return AlarmasRow.deletePermission; }
        protected getInsertPermission() { return AlarmasRow.insertPermission; }
        protected getUpdatePermission() { return AlarmasRow.updatePermission; }

        protected form = new AlarmasForm(this.idPrefix);

    }
}