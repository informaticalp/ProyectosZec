﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("Procedimientos")]
    [DisplayName("Procedimientos"), InstanceName("Procedimientos")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    [LookupScript("Nuevo_Roezec.Procedimientos")]

    public sealed class ProcedimientosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? ProcedimientoId
        {
            get { return Fields.ProcedimientoId[this]; }
            set { Fields.ProcedimientoId[this] = value; }
        }
        [DisplayName("Visible"), Column("Visible"),LookupInclude]
        public Boolean? Visible
        {
            get { return Fields.Visible[this]; }
            set { Fields.Visible[this] = value; }
        }
        [DisplayName("Procedimiento"), Size(100), NotNull, QuickSearch]
        public String Procedimiento
        {
            get { return Fields.Procedimiento[this]; }
            set { Fields.Procedimiento[this] = value; }
        }
        [DisplayName("Orden"), Column("Orden")]
        public Int32? Orden
        {
            get { return Fields.Orden[this]; }
            set { Fields.Orden[this] = value; }
        }
        [DisplayName("Procedimiento"),Expression("(CONCAT('(',T0.[Orden],')- ',T0.[Procedimiento]))")]
        public String NameField
        {
            get { return Fields.NameField[this]; }
            set { Fields.NameField[this] = value; }
        }

        [DisplayName("Nuevo Estado Favorable"),Column("EstadoEmpresaId"),ForeignKey("estados_empresa", "EstadoEmpresaId"), LeftJoin("jEstadoEmpresa"), TextualField("EstadoEmpresaEstado"), LookupInclude]
        [LookupEditor(typeof(Entities.EstadosEmpresaRow), InplaceAdd = true)]
        public Int32? EstadoEmpresaId
        {
            get { return Fields.EstadoEmpresaId[this]; }
            set { Fields.EstadoEmpresaId[this] = value; }
        }

        [DisplayName("Nuevo Estado Favorable"), Expression("jEstadoEmpresa.[Estado]")]
        public String EstadoEmpresaEstado
        {
            get { return Fields.EstadoEmpresaEstado[this]; }
            set { Fields.EstadoEmpresaEstado[this] = value; }
        }
        [DisplayName("Instrucciones"), Size(2000), QuickSearch]
        public String Instrucciones
        {
            get { return Fields.Instrucciones[this]; }
            set { Fields.Instrucciones[this] = value; }
        }
        
        [DisplayName("Nuevo Estado DesFavorable"), Column("EstadoEmpresaIdDesfavorable"), ForeignKey("estados_empresa", "EstadoEmpresaId"), LeftJoin("jEstadoEmpresa2"), TextualField("EstadoEmpresaEstadoDesfavorable"), LookupInclude]
        [LookupEditor(typeof(Entities.EstadosEmpresaRow), InplaceAdd = true)]
        public Int32? EstadoEmpresaIdDesfavorable
        {
            get { return Fields.EstadoEmpresaIdDesfavorable[this]; }
            set { Fields.EstadoEmpresaIdDesfavorable[this] = value; }
        }

        [DisplayName("Nuevo Estado DesFavorable"), Expression("jEstadoEmpresa2.[Estado]")]
        public String EstadoEmpresaEstadoDesfavorable
        {
            get { return Fields.EstadoEmpresaEstadoDesfavorable[this]; }
            set { Fields.EstadoEmpresaEstadoDesfavorable[this] = value; }
        }

        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        [MasterDetailRelation(foreignKey: "ProcedimientoId", IncludeColumns = "TipoAlarma,SentidoResolucion,UserName,FechaModificacion"), NotMapped]
        [DisplayName("Alarmas")]
        public List<AlarmasProcedimientosRow> AlarmasList
        {
            get { return Fields.AlarmasList[this]; }
            set { Fields.AlarmasList[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.ProcedimientoId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.NameField; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public ProcedimientosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field ProcedimientoId;
            public StringField Procedimiento;
            public Int32Field EstadoEmpresaId;
            public Int32Field EstadoEmpresaIdDesfavorable;
            public Int32Field Orden;
            public StringField EstadoEmpresaEstado;
            public StringField EstadoEmpresaEstadoDesfavorable;
            public StringField NameField;
            public StringField Instrucciones;
            public BooleanField Visible;


            public RowListField<AlarmasProcedimientosRow> AlarmasList;

            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;

            /****** End Audit Fields ********/
        }
    }
}
