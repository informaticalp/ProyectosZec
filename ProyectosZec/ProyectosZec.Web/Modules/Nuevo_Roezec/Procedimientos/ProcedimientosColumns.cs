﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Procedimientos")]
    [BasedOnRow(typeof(Entities.ProcedimientosRow), CheckNames = true)]
    public class ProcedimientosColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight, Width(60)]
        public Int32 ProcedimientoId { get; set; }
        [EditLink, Width(70),AlignRight]
        public Int32 Orden {get; set;}
        [EditLink, Width(280)]
        public String Procedimiento { get; set; }
        [Width(230)]
        public String EstadoEmpresaEstado { get; set; }
        [Width(230)]
        public String EstadoEmpresaEstadoDesfavorable { get; set; }
        [Width(300)]
        public string Instrucciones { get; set; }
        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}