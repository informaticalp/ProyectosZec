﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido
    export class ProcedimientosGrid extends Serenity.EntityGrid<ProcedimientosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.Procedimientos'; }
        protected getDialogType() { return ProcedimientosDialog; }
        protected getIdProperty() { return ProcedimientosRow.idProperty; }
        protected getInsertPermission() { return ProcedimientosRow.insertPermission; }
        protected getLocalTextPrefix() { return ProcedimientosRow.localTextPrefix; }
        protected getService() { return ProcedimientosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        // Añadidos
        // Primero campo de ordenación por defecto
        // No olvidarse Cambiar el Row y el Id
        protected getDefaultSortBy() {
            return [ProcedimientosRow.Fields.Orden];
        }

        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Nuevo_Roezec/Procedimientos/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            return buttons;
            // Fin añadidos

        }

    }
}