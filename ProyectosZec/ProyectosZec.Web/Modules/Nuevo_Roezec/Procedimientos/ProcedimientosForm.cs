﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.Procedimientos")]
    [BasedOnRow(typeof(Entities.ProcedimientosRow), CheckNames = true)]
    public class ProcedimientosForm
    {
        [Tab("General")]
        public String Procedimiento { get; set; }
        public Int32 Orden { get; set; }
        public Int32 EstadoEmpresaId { get; set; }
        public Int32 EstadoEmpresaIdDesfavorable { get; set; }
        [TextAreaEditor(Rows = 8)]
        public String Instrucciones { get; set; }
        [Tab("Alarmas")]
        [AlarmasProcedimientosEditor]
        public List<Entities.AlarmasProcedimientosRow> AlarmasList { get; set; }
 
    }
}