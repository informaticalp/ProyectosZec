﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasEmpleosEditDialog extends Common.GridEditorDialog<EmpresasEmpleosRow> {
        protected getFormKey() { return EmpresasEmpleosForm.formKey; }
        protected getNameProperty() { return EmpresasEmpleosRow.nameProperty; }
        protected getLocalTextPrefix() { return EmpresasEmpleosRow.localTextPrefix; }

        protected form: EmpresasEmpleosForm;

        constructor() {
            super();
            this.form = new EmpresasEmpleosForm(this.idPrefix);
        }
    }
}