﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.EmpresasEmpleos")]
    [BasedOnRow(typeof(Entities.EmpresasEmpleosRow), CheckNames = true)]
    public class EmpresasEmpleosColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 EmpresasEmpleosId { get; set; }
        [Width(60)]
        public Int32 Any { get; set; }
        
        [Width(90),AlignRight, DisplayFormat("#,###.0")]
        public Decimal Empleos { get; set; }
        [Width(100)]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}