﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class EmpresasEmpleosEditor extends Common.GridEditorBase<EmpresasEmpleosRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.EmpresasEmpleos"; }
        protected getDialogType() { return EmpresasEmpleosEditDialog; }
        protected getLocalTextPrefix() { return EmpresasEmpleosRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Empleo";
        }
    }
}