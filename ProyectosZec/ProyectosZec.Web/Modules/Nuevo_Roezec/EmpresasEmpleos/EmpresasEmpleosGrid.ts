﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasEmpleosGrid extends Serenity.EntityGrid<EmpresasEmpleosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.EmpresasEmpleos'; }
        protected getDialogType() { return EmpresasEmpleosDialog; }
        protected getIdProperty() { return EmpresasEmpleosRow.idProperty; }
        protected getInsertPermission() { return EmpresasEmpleosRow.insertPermission; }
        protected getLocalTextPrefix() { return EmpresasEmpleosRow.localTextPrefix; }
        protected getService() { return EmpresasEmpleosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}