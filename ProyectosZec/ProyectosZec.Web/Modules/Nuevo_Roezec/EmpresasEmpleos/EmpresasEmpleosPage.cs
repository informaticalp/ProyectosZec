﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/EmpresasEmpleos"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EmpresasEmpleosRow))]
    public class EmpresasEmpleosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/EmpresasEmpleos/EmpresasEmpleosIndex.cshtml");
        }
    }
}