﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.EmpresasEmpleos")]
    [BasedOnRow(typeof(Entities.EmpresasEmpleosRow), CheckNames = true)]
    public class EmpresasEmpleosForm
    {
        public Int32 Any { get; set; }
        public Decimal Empleos { get; set; }

    }
}