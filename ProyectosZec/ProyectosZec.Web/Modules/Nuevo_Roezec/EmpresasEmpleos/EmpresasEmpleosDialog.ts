﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasEmpleosDialog extends Serenity.EntityDialog<EmpresasEmpleosRow, any> {
        protected getFormKey() { return EmpresasEmpleosForm.formKey; }
        protected getIdProperty() { return EmpresasEmpleosRow.idProperty; }
        protected getLocalTextPrefix() { return EmpresasEmpleosRow.localTextPrefix; }
        protected getService() { return EmpresasEmpleosService.baseUrl; }
        protected getDeletePermission() { return EmpresasEmpleosRow.deletePermission; }
        protected getInsertPermission() { return EmpresasEmpleosRow.insertPermission; }
        protected getUpdatePermission() { return EmpresasEmpleosRow.updatePermission; }

        protected form = new EmpresasEmpleosForm(this.idPrefix);

    }
}