﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/Mercados"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.MercadosRow))]
    public class MercadosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/Mercados/MercadosIndex.cshtml");
        }
    }
}