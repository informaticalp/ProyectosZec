﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class MercadosGrid extends Serenity.EntityGrid<MercadosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.Mercados'; }
        protected getDialogType() { return MercadosDialog; }
        protected getIdProperty() { return MercadosRow.idProperty; }
        protected getInsertPermission() { return MercadosRow.insertPermission; }
        protected getLocalTextPrefix() { return MercadosRow.localTextPrefix; }
        protected getService() { return MercadosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}