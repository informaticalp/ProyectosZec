﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.Mercados")]
    [BasedOnRow(typeof(Entities.MercadosRow), CheckNames = true)]
    public class MercadosForm
    {
        public String Mercado { get; set; }
    }
}