﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Mercados")]
    [BasedOnRow(typeof(Entities.MercadosRow), CheckNames = true)]
    public class MercadosColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Width(50)]
        public Int32 MercadoId { get; set; }
        [EditLink]
        public String Mercado { get; set; }
        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}