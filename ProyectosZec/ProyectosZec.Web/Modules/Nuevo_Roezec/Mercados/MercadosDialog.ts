﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class MercadosDialog extends Serenity.EntityDialog<MercadosRow, any> {
        protected getFormKey() { return MercadosForm.formKey; }
        protected getIdProperty() { return MercadosRow.idProperty; }
        protected getLocalTextPrefix() { return MercadosRow.localTextPrefix; }
        protected getNameProperty() { return MercadosRow.nameProperty; }
        protected getService() { return MercadosService.baseUrl; }
        protected getDeletePermission() { return MercadosRow.deletePermission; }
        protected getInsertPermission() { return MercadosRow.insertPermission; }
        protected getUpdatePermission() { return MercadosRow.updatePermission; }

        protected form = new MercadosForm(this.idPrefix);

    }
}