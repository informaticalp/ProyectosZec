﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("mercados")]
    [DisplayName("Mercados"), InstanceName("Mercados")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.Mercados")]
    public sealed class MercadosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Mercado Id"), Identity]
        public Int32? MercadoId
        {
            get { return Fields.MercadoId[this]; }
            set { Fields.MercadoId[this] = value; }
        }

        [DisplayName("Mercado"), Size(50), NotNull, QuickSearch]
        public String Mercado
        {
            get { return Fields.Mercado[this]; }
            set { Fields.Mercado[this] = value; }
        }
        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        IIdField IIdRow.IdField
        {
            get { return Fields.MercadoId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Mercado; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public MercadosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field MercadoId;
            public StringField Mercado;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/
        }
    }
}
