﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class EmpresasFicherosEditor extends Common.GridEditorBase<EmpresasFicherosRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.EmpresasFicheros"; }
        protected getDialogType() { return EmpresasFicherosEditDialog; }
        protected getLocalTextPrefix() { return EmpresasFicherosRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Fichero";
        }
    }
}