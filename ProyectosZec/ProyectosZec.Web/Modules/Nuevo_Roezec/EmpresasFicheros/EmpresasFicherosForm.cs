﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.EmpresasFicheros")]
    [BasedOnRow(typeof(Entities.EmpresasFicherosRow), CheckNames = true)]
    public class EmpresasFicherosForm
    {
        public String Nombre { get; set; }
        public String Fichero { get; set; }
        [TextAreaEditor(Rows = 3)]
        public String Observaciones { get; set; }

    }
}