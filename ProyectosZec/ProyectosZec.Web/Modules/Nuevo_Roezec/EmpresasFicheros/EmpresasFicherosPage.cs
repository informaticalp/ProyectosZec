﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/EmpresasFicheros"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EmpresasFicherosRow))]
    public class EmpresasFicherosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/EmpresasFicheros/EmpresasFicherosIndex.cshtml");
        }
    }
}