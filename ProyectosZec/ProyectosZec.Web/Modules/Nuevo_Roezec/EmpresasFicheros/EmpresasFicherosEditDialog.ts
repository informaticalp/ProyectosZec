﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasFicherosEditDialog extends Common.GridEditorDialog<EmpresasFicherosRow> {
        protected getFormKey() { return EmpresasFicherosForm.formKey; }
        protected getNameProperty() { return EmpresasFicherosRow.nameProperty; }
        protected getLocalTextPrefix() { return EmpresasFicherosRow.localTextPrefix; }

        protected form: EmpresasFicherosForm;

        constructor() {
            super();
            this.form = new EmpresasFicherosForm(this.idPrefix);
        }
    }
}