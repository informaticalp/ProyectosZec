﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.EmpresasFicheros")]
    [BasedOnRow(typeof(Entities.EmpresasFicherosRow), CheckNames = true)]
    public class EmpresasFicherosColumns
    {
        [EditLink,DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 EmpresasFicheroId { get; set; }
        
        [EditLink,Width(200)]
        public String Nombre { get; set; }
        [Width(350)]
        public String Observaciones { get; set; }
        [Width(100)]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    }
}