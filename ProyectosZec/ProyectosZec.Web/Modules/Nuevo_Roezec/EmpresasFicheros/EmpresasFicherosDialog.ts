﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasFicherosDialog extends Serenity.EntityDialog<EmpresasFicherosRow, any> {
        protected getFormKey() { return EmpresasFicherosForm.formKey; }
        protected getIdProperty() { return EmpresasFicherosRow.idProperty; }
        protected getLocalTextPrefix() { return EmpresasFicherosRow.localTextPrefix; }
        protected getNameProperty() { return EmpresasFicherosRow.nameProperty; }
        protected getService() { return EmpresasFicherosService.baseUrl; }
        protected getDeletePermission() { return EmpresasFicherosRow.deletePermission; }
        protected getInsertPermission() { return EmpresasFicherosRow.insertPermission; }
        protected getUpdatePermission() { return EmpresasFicherosRow.updatePermission; }

        protected form = new EmpresasFicherosForm(this.idPrefix);

    }
}