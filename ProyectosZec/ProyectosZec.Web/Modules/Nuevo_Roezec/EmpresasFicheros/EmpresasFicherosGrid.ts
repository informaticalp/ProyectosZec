﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasFicherosGrid extends Serenity.EntityGrid<EmpresasFicherosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.EmpresasFicheros'; }
        protected getDialogType() { return EmpresasFicherosDialog; }
        protected getIdProperty() { return EmpresasFicherosRow.idProperty; }
        protected getInsertPermission() { return EmpresasFicherosRow.insertPermission; }
        protected getLocalTextPrefix() { return EmpresasFicherosRow.localTextPrefix; }
        protected getService() { return EmpresasFicherosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}