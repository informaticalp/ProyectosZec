﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/EmpresasDirecciones"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EmpresasDireccionesRow))]
    public class EmpresasDireccionesController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/EmpresasDirecciones/EmpresasDireccionesIndex.cshtml");
        }
    }
}