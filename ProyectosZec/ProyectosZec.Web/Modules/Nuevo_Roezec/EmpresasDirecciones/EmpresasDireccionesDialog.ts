﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasDireccionesDialog extends Serenity.EntityDialog<EmpresasDireccionesRow, any> {
        protected getFormKey() { return EmpresasDireccionesForm.formKey; }
        protected getIdProperty() { return EmpresasDireccionesRow.idProperty; }
        protected getLocalTextPrefix() { return EmpresasDireccionesRow.localTextPrefix; }
        protected getNameProperty() { return EmpresasDireccionesRow.nameProperty; }
        protected getService() { return EmpresasDireccionesService.baseUrl; }
        protected getDeletePermission() { return EmpresasDireccionesRow.deletePermission; }
        protected getInsertPermission() { return EmpresasDireccionesRow.insertPermission; }
        protected getUpdatePermission() { return EmpresasDireccionesRow.updatePermission; }

        protected form = new EmpresasDireccionesForm(this.idPrefix);

    }
}