﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasDireccionesEditDialog extends Common.GridEditorDialog<EmpresasDireccionesRow> {
        protected getFormKey() { return EmpresasDireccionesForm.formKey; }
        protected getNameProperty() { return EmpresasDireccionesRow.nameProperty; }
        protected getLocalTextPrefix() { return EmpresasDireccionesRow.localTextPrefix; }

        protected form: EmpresasDireccionesForm;

        constructor() {
            super();
            this.form = new EmpresasDireccionesForm(this.idPrefix);
        }
    }
}