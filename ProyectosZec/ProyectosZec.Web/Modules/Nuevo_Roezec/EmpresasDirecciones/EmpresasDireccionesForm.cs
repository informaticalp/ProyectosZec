﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.EmpresasDirecciones")]
    [BasedOnRow(typeof(Entities.EmpresasDireccionesRow), CheckNames = true)]
    public class EmpresasDireccionesForm
    {
        public Int32 TipoDireccionId { get; set; }
        public String Direccion { get; set; }
        public String Poblacion { get; set; }
        public String Cp { get; set; }
        public Int32 IslaId { get; set; }
        public Int32 PaisId { get; set; }
        [HalfWidth(UntilNext = true)]
        public DateTime Desde { get; set; }
        public DateTime Hasta { get; set; }

    }
}