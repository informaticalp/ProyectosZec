﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.EmpresasDirecciones")]
    [BasedOnRow(typeof(Entities.EmpresasDireccionesRow), CheckNames = true)]
    public class EmpresasDireccionesColumns
    {
        [Width(50),EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 EmpresasDireccionesId { get; set; }
        //public String EmpresaRazon { get; set; }
        [Width(150)]
        public String TipoDireccion { get; set; }
        [Width(300)]
        public String Direccion { get; set; }
        [Width(150)]
        public String Poblacion { get; set; }
        [Width(80)]
        public String Cp { get; set; }
        [Width(80)]
        public String Isla { get; set; }
        [Width(90)]
        public String PaisPais { get; set; }
        [Width(100)]
        public DateTime Desde { get; set; }
        [Width(100)]
        public DateTime Hasta { get; set; } 
        [Width(100),Hidden]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g"),Hidden]

        public DateTime FechaModificacion { get; set; }
    }
}