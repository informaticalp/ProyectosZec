﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class EmpresasDireccionesEditor extends Common.GridEditorBase<EmpresasDireccionesRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.EmpresasDirecciones"; }
        protected getDialogType() { return EmpresasDireccionesEditDialog; }
        protected getLocalTextPrefix() { return EmpresasDireccionesRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Dirección";
        }
    }
}