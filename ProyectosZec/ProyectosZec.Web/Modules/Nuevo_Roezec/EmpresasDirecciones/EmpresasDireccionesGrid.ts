﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasDireccionesGrid extends Serenity.EntityGrid<EmpresasDireccionesRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.EmpresasDirecciones'; }
        protected getDialogType() { return EmpresasDireccionesDialog; }
        protected getIdProperty() { return EmpresasDireccionesRow.idProperty; }
        protected getInsertPermission() { return EmpresasDireccionesRow.insertPermission; }
        protected getLocalTextPrefix() { return EmpresasDireccionesRow.localTextPrefix; }
        protected getService() { return EmpresasDireccionesService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        protected getDefaultSortBy() {
            return [EmpresasDireccionesRow.Fields.Desde]; // Este es el campo de ordenación por defecto
        }

    }
}