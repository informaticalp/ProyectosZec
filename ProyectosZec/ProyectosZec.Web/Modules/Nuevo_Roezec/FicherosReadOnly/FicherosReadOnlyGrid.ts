﻿/// <reference path="../Ficheros/FicherosGrid.ts" />


namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido

    export class FicherosReadOnlyGrid extends Nuevo_Roezec.FicherosGrid {
        protected getColumnsKey() { return 'Nuevo_Roezec.FicherosReadOnly'; }

        protected getDialogType() { return FicherosReadOnlyDialog; }

        constructor(container: JQuery) {
            super(container);
        }

        /**
         * Removing add button from grid using its css class
         */
        protected getButtons(): Serenity.ToolButton[] {
            var buttons = super.getButtons();
            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Nuevo_Roezec/Ficheros/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));
            return buttons;
        }

    }
}