﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.FicherosReadOnly")]
    [BasedOnRow(typeof(Entities.FicherosRow), CheckNames = true)]
    public class FicherosReadOnlyColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        [Width(30)]
        public Int32 FicheroId { get; set; }
        [EditLink,Width(200)]
        public String Razon { get; set; }
        [Width(120)]
        public String TipoDocumento { get; set; }

        [Width(130), DisplayFormat("d")]
        public DateTime FechaCaptura { get; set; }
        public String NombreNatural { get; set; }
        public String Observaciones { get; set; }
        public String CSV { get; set; }
        public String RegulacionCSV { get; set; }

        [Hidden, Width(100)]
        public String UserName { get; set; }
        [Hidden, Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }

    }
}