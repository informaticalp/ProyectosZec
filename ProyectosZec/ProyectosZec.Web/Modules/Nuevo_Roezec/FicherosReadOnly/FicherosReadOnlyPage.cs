﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/FicherosReadOnly"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.FicherosRow))]
    public class FicherosReadOnlyController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/FicherosReadOnly/FicherosReadOnlyIndex.cshtml");
        }
    }
}