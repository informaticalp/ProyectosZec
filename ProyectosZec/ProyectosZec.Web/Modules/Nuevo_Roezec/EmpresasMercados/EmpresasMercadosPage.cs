﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/EmpresasMercados"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EmpresasMercadosRow))]
    public class EmpresasMercadosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/EmpresasMercados/EmpresasMercadosIndex.cshtml");
        }
    }
}