﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasMercadosDialog extends Serenity.EntityDialog<EmpresasMercadosRow, any> {
        protected getFormKey() { return EmpresasMercadosForm.formKey; }
        protected getIdProperty() { return EmpresasMercadosRow.idProperty; }
        protected getLocalTextPrefix() { return EmpresasMercadosRow.localTextPrefix; }
        protected getService() { return EmpresasMercadosService.baseUrl; }
        protected getDeletePermission() { return EmpresasMercadosRow.deletePermission; }
        protected getInsertPermission() { return EmpresasMercadosRow.insertPermission; }
        protected getUpdatePermission() { return EmpresasMercadosRow.updatePermission; }

        protected form = new EmpresasMercadosForm(this.idPrefix);

    }
}