﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class EmpresasMercadosEditor extends Common.GridEditorBase<EmpresasMercadosRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.EmpresasMercados"; }
        protected getDialogType() { return EmpresasMercadosEditDialog; }
        protected getLocalTextPrefix() { return EmpresasMercadosRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Mercado";
        }
    }
}