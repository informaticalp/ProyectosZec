﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasMercadosGrid extends Serenity.EntityGrid<EmpresasMercadosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.EmpresasMercados'; }
        protected getDialogType() { return EmpresasMercadosDialog; }
        protected getIdProperty() { return EmpresasMercadosRow.idProperty; }
        protected getInsertPermission() { return EmpresasMercadosRow.insertPermission; }
        protected getLocalTextPrefix() { return EmpresasMercadosRow.localTextPrefix; }
        protected getService() { return EmpresasMercadosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}