﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.EmpresasMercados")]
    [BasedOnRow(typeof(Entities.EmpresasMercadosRow), CheckNames = true)]
    public class EmpresasMercadosForm
    {
        public Int32 MercadoId { get; set; }
    }
}