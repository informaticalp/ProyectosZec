﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpresasMercadosEditDialog extends Common.GridEditorDialog<EmpresasMercadosRow> {
        protected getFormKey() { return EmpresasMercadosForm.formKey; }
        protected getNameProperty() { return EmpresasMercadosRow.nameProperty; }
        protected getLocalTextPrefix() { return EmpresasMercadosRow.localTextPrefix; }

        protected form: EmpresasMercadosForm;

        constructor() {
            super();
            this.form = new EmpresasMercadosForm(this.idPrefix);
        }
    }
}