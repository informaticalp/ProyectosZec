﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("tipos_direcciones")]
    [DisplayName("Tipos Direcciones"), InstanceName("Tipos Direcciones")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Admin")]
    [DeletePermission("Roezec:Admin")]
    [InsertPermission("Roezec:Admin")]
    [LookupScript("Nuevo_Roezec.TiposDirecciones")]
    public sealed class TiposDireccionesRow : Row, IIdRow, INameRow
    {
        [DisplayName("Tipo Direccion Id"), Identity]
        public Int32? TipoDireccionId
        {
            get { return Fields.TipoDireccionId[this]; }
            set { Fields.TipoDireccionId[this] = value; }
        }

        [DisplayName("Tipo Direccion"), Size(50), NotNull, QuickSearch]
        public String TipoDireccion
        {
            get { return Fields.TipoDireccion[this]; }
            set { Fields.TipoDireccion[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.TipoDireccionId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.TipoDireccion; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public TiposDireccionesRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field TipoDireccionId;
            public StringField TipoDireccion;
        }
    }
}
