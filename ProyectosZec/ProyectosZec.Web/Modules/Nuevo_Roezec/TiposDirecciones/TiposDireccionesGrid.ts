﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposDireccionesGrid extends Serenity.EntityGrid<TiposDireccionesRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.TiposDirecciones'; }
        protected getDialogType() { return TiposDireccionesDialog; }
        protected getIdProperty() { return TiposDireccionesRow.idProperty; }
        protected getInsertPermission() { return TiposDireccionesRow.insertPermission; }
        protected getLocalTextPrefix() { return TiposDireccionesRow.localTextPrefix; }
        protected getService() { return TiposDireccionesService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}