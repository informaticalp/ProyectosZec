﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.TiposDirecciones")]
    [BasedOnRow(typeof(Entities.TiposDireccionesRow), CheckNames = true)]
    public class TiposDireccionesColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 TipoDireccionId { get; set; }
        [EditLink]
        public String TipoDireccion { get; set; }
    }
}