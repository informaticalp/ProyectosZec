﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/TiposDirecciones"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.TiposDireccionesRow))]
    public class TiposDireccionesController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/TiposDirecciones/TiposDireccionesIndex.cshtml");
        }
    }
}