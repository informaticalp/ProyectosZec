﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.TiposDirecciones")]
    [BasedOnRow(typeof(Entities.TiposDireccionesRow), CheckNames = true)]
    public class TiposDireccionesForm
    {
        public String TipoDireccion { get; set; }
    }
}