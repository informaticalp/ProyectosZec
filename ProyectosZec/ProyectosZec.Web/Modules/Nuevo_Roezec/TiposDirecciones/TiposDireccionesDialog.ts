﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class TiposDireccionesDialog extends Serenity.EntityDialog<TiposDireccionesRow, any> {
        protected getFormKey() { return TiposDireccionesForm.formKey; }
        protected getIdProperty() { return TiposDireccionesRow.idProperty; }
        protected getLocalTextPrefix() { return TiposDireccionesRow.localTextPrefix; }
        protected getNameProperty() { return TiposDireccionesRow.nameProperty; }
        protected getService() { return TiposDireccionesService.baseUrl; }
        protected getDeletePermission() { return TiposDireccionesRow.deletePermission; }
        protected getInsertPermission() { return TiposDireccionesRow.insertPermission; }
        protected getUpdatePermission() { return TiposDireccionesRow.updatePermission; }

        protected form = new TiposDireccionesForm(this.idPrefix);

    }
}