﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class MetadatosGrid extends Serenity.EntityGrid<MetadatosRow, any> {
        protected getColumnsKey() { return 'Nuevo_Roezec.Metadatos'; }
        protected getDialogType() { return MetadatosDialog; }
        protected getIdProperty() { return MetadatosRow.idProperty; }
        protected getInsertPermission() { return MetadatosRow.insertPermission; }
        protected getLocalTextPrefix() { return MetadatosRow.localTextPrefix; }
        protected getService() { return MetadatosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}