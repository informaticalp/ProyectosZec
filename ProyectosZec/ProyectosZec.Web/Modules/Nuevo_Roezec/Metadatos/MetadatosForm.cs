﻿
namespace ProyectosZec.Nuevo_Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Nuevo_Roezec.Metadatos")]
    [BasedOnRow(typeof(Entities.MetadatosRow), CheckNames = true)]
    public class MetadatosForm
    {
        public String Nombre { get; set; }
        public String Valor { get; set; }
    }
}