﻿
namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class MetadatosDialog extends Serenity.EntityDialog<MetadatosRow, any> {
        protected getFormKey() { return MetadatosForm.formKey; }
        protected getIdProperty() { return MetadatosRow.idProperty; }
        protected getLocalTextPrefix() { return MetadatosRow.localTextPrefix; }
        protected getNameProperty() { return MetadatosRow.nameProperty; }
        protected getService() { return MetadatosService.baseUrl; }
        protected getDeletePermission() { return MetadatosRow.deletePermission; }
        protected getInsertPermission() { return MetadatosRow.insertPermission; }
        protected getUpdatePermission() { return MetadatosRow.updatePermission; }

        protected form = new MetadatosForm(this.idPrefix);

    }
}