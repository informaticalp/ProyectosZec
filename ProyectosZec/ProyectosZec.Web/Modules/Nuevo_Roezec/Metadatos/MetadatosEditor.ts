﻿/// <reference path="../../Common/Helpers/GridEditorBase.ts" />

namespace ProyectosZec.Nuevo_Roezec {
    @Serenity.Decorators.registerEditor()
    @Serenity.Decorators.registerClass()
    export class MetadatosEditor extends Common.GridEditorBase<MetadatosRow> {
        protected getColumnsKey() { return "Nuevo_Roezec.Metadatos"; }
        protected getDialogType() { return MetadatosEditDialog; }
        protected getLocalTextPrefix() { return MetadatosRow.localTextPrefix; }

        constructor(container: JQuery) {
            super(container);
        }

        protected getAddButtonCaption() {
            return "Añadir Metadato";
        }
    }
}
