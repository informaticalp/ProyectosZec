﻿/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />

namespace ProyectosZec.Nuevo_Roezec {

    @Serenity.Decorators.registerClass()
    export class MetadatosEditDialog extends Common.GridEditorDialog<MetadatosRow> {
        protected getFormKey() { return MetadatosForm.formKey; }
        protected getNameProperty() { return MetadatosRow.nameProperty; }
        protected getLocalTextPrefix() { return MetadatosRow.localTextPrefix; }

        protected form: MetadatosForm;

        constructor() {
            super();
            this.form = new MetadatosForm(this.idPrefix);
        }
    }
}
