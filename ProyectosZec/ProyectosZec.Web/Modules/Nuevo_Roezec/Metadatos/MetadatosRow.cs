﻿
namespace ProyectosZec.Nuevo_Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec"), Module("Nuevo_Roezec"), TableName("metadatos")]
    [DisplayName("Metadatos"), InstanceName("Metadatos")]
    [ReadPermission("Roezec:Read")]
    [ModifyPermission("Roezec:Modify")]
    [InsertPermission("Roezec:Insert")]
    [DeletePermission("Roezec:Delete")]
    public sealed class MetadatosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Metadato Id"), Identity]
        public Int32? MetadatoId
        {
            get { return Fields.MetadatoId[this]; }
            set { Fields.MetadatoId[this] = value; }
        }

        [DisplayName("Fichero"), NotNull, ForeignKey("ficheros", "FicheroId"), LeftJoin("jFichero"), TextualField("Fichero")]
        public Int32? FicheroId
        {
            get { return Fields.FicheroId[this]; }
            set { Fields.FicheroId[this] = value; }
        }

        [DisplayName("Nombre"), Size(100), NotNull, QuickSearch]
        public String Nombre
        {
            get { return Fields.Nombre[this]; }
            set { Fields.Nombre[this] = value; }
        }

        [DisplayName("Valor"), Size(100), NotNull]
        public String Valor
        {
            get { return Fields.Valor[this]; }
            set { Fields.Valor[this] = value; }
        }

        /*** Audit Fields ****/
        [DisplayName("Usuario"), Column("UserId"), ForeignKey("default.users", "UserId"), LeftJoin("JUsers")]
        public Int32? UserId
        {
            get { return Fields.UserId[this]; }
            set { Fields.UserId[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[UserName]")]
        public String UserName
        {
            get { return Fields.UserName[this]; }
            set { Fields.UserName[this] = value; }
        }

        [DisplayName("Usuario"), Expression("jUsers.[DisplayName]")]
        public String DisplayName
        {
            get { return Fields.DisplayName[this]; }
            set { Fields.DisplayName[this] = value; }
        }

        [DisplayName("Fecha Modificación")]
        public DateTime? FechaModificacion
        {
            get { return Fields.FechaModificacion[this]; }
            set { Fields.FechaModificacion[this] = value; }
        }

        /***  End Audit Fields ***/

        [DisplayName("Fichero Historial Id"), Expression("jFichero.[HistorialId]")]
        public Int32? FicheroHistorialId
        {
            get { return Fields.FicheroHistorialId[this]; }
            set { Fields.FicheroHistorialId[this] = value; }
        }

        [DisplayName("Fichero"), Expression("jFichero.[Fichero]")]
        public String Fichero
        {
            get { return Fields.Fichero[this]; }
            set { Fields.Fichero[this] = value; }
        }

        [DisplayName("Fichero Nombre Natural"), Expression("jFichero.[NombreNatural]")]
        public String FicheroNombreNatural
        {
            get { return Fields.FicheroNombreNatural[this]; }
            set { Fields.FicheroNombreNatural[this] = value; }
        }

        [DisplayName("Fichero Organo"), Expression("jFichero.[Organo]")]
        public String FicheroOrgano
        {
            get { return Fields.FicheroOrgano[this]; }
            set { Fields.FicheroOrgano[this] = value; }
        }

        [DisplayName("Fichero Fecha Captura"), Expression("jFichero.[FechaCaptura]")]
        public DateTime? FicheroFechaCaptura
        {
            get { return Fields.FicheroFechaCaptura[this]; }
            set { Fields.FicheroFechaCaptura[this] = value; }
        }

        [DisplayName("Fichero Tipo Documento Id"), Expression("jFichero.[TipoDocumentoId]")]
        public Int32? FicheroTipoDocumentoId
        {
            get { return Fields.FicheroTipoDocumentoId[this]; }
            set { Fields.FicheroTipoDocumentoId[this] = value; }
        }

        [DisplayName("Fichero Csv"), Expression("jFichero.[CSV]")]
        public String FicheroCsv
        {
            get { return Fields.FicheroCsv[this]; }
            set { Fields.FicheroCsv[this] = value; }
        }

        [DisplayName("Fichero Regulacion Csv"), Expression("jFichero.[RegulacionCSV]")]
        public String FicheroRegulacionCsv
        {
            get { return Fields.FicheroRegulacionCsv[this]; }
            set { Fields.FicheroRegulacionCsv[this] = value; }
        }

        [DisplayName("Fichero Observaciones"), Expression("jFichero.[Observaciones]")]
        public String FicheroObservaciones
        {
            get { return Fields.FicheroObservaciones[this]; }
            set { Fields.FicheroObservaciones[this] = value; }
        }

        [DisplayName("Fichero Fecha Modificacion"), Expression("jFichero.[FechaModificacion]")]
        public DateTime? FicheroFechaModificacion
        {
            get { return Fields.FicheroFechaModificacion[this]; }
            set { Fields.FicheroFechaModificacion[this] = value; }
        }

        [DisplayName("Fichero User Id"), Expression("jFichero.[UserId]")]
        public Int32? FicheroUserId
        {
            get { return Fields.FicheroUserId[this]; }
            set { Fields.FicheroUserId[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.MetadatoId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.FicheroNombreNatural; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public MetadatosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field MetadatoId;
            public Int32Field FicheroId;
            public StringField Nombre;
            public StringField Valor;
            /****** Audit Fields ********/
            public Int32Field UserId;
            public StringField UserName;
            public StringField DisplayName;
            public DateTimeField FechaModificacion;
            /****** End Audit Fields ********/

            public Int32Field FicheroHistorialId;
            public StringField Fichero;
            public StringField FicheroNombreNatural;
            public StringField FicheroOrgano;
            public DateTimeField FicheroFechaCaptura;
            public Int32Field FicheroTipoDocumentoId;
            public StringField FicheroCsv;
            public StringField FicheroRegulacionCsv;
            public StringField FicheroObservaciones;
            public DateTimeField FicheroFechaModificacion;
            public Int32Field FicheroUserId;
        }
    }
}
