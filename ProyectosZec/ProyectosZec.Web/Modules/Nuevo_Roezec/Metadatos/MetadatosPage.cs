﻿
namespace ProyectosZec.Nuevo_Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Nuevo_Roezec/Metadatos"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.MetadatosRow))]
    public class MetadatosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Nuevo_Roezec/Metadatos/MetadatosIndex.cshtml");
        }
    }
}