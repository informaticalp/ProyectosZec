﻿
namespace ProyectosZec.Nuevo_Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Nuevo_Roezec.Metadatos")]
    [BasedOnRow(typeof(Entities.MetadatosRow), CheckNames = true)]
    public class MetadatosColumns
    {
 
        [EditLink,Width(120)]
        public String Nombre { get; set; }
        [Width(200)]
        public String Valor { get; set; }
        [Width(100)]
        public String UserName { get; set; }
        [Width(130), DisplayFormat("g")]
        public DateTime FechaModificacion { get; set; }
    
    }
}