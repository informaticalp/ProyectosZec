﻿
namespace ProyectosZec.Kairos.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Kairos.Hoy")]
    [BasedOnRow(typeof(Entities.HoyRow), CheckNames = true)]
    public class HoyForm
    {
        public Int64 SedeId { get; set; }
        public String Nombre { get; set; }

    }
}