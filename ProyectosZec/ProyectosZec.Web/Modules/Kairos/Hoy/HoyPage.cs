﻿
namespace ProyectosZec.Kairos.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Kairos/Hoy"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.HoyRow))]
    public class HoyController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Kairos/Hoy/HoyIndex.cshtml");
        }
    }
}