﻿
namespace ProyectosZec.Kairos.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Kairos.Hoy")]
    [BasedOnRow(typeof(Entities.HoyRow), CheckNames = true)]
    public class HoyColumns
    {
        [DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int64 Id { get; set; }

        [Width(100), QuickFilter]
        public String Sede { get; set; }
        [DisplayName("Empleado"), Width(250)]
        public String Nombre { get; set; }
        [DisplayName("Estado"), Width(200)]
        public String Estado { get; set; }
        public String HoraEntrada { get; set; }
        public String HoraSalida { get; set; }

    }
}