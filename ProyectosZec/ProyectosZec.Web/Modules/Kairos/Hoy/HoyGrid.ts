﻿
namespace ProyectosZec.Kairos {
    import fld = HoyRow.Fields;
    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido
    @Serenity.Decorators.registerClass()
    export class HoyGrid extends Serenity.EntityGrid<HoyRow, any> {
        protected getColumnsKey() { return 'Kairos.Hoy'; }
        protected getDialogType() { return HoyDialog; }
        protected getIdProperty() { return HoyRow.idProperty; }
        protected getInsertPermission() { return HoyRow.insertPermission; }
        protected getLocalTextPrefix() { return HoyRow.localTextPrefix; }
        protected getService() { return HoyService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }

        /**
* We override getColumns() to be able to add a custom CSS class to UnitPrice
* We could also add this class in ProductColumns.cs but didn't want to modify
* it solely for this sample.
*/
        //protected getColumns(): Slick.Column[] {
        //    var columns = super.getColumns();
        //    // adding a specific css class to UnitPrice column, 
        //    // to be able to format cell with a different background
        //    Q.first(columns, x => x.field == fld.Estado).cssClass += " col-unit-price";

        //    return columns;
        //}

        /**
 * This method is called for all rows
 * @param item Data item for current row
 * @param index Index of the row in grid
 */
        protected getItemCssClass(item: Kairos.HoyRow, index: number): string {
            let klass: string = "";

            if (item.Estado == "TELETRABAJO")
                klass += " critical-stock";
            else if (item.Estado !="PRESENCIAL")
                klass += " out-of-stock";

            return Q.trimToNull(klass);
        }
        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Kairos/Hoy/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            return buttons;
            // Fin añadidos
        }

    }
}