﻿
namespace ProyectosZec.Kairos.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Kairos"), Module("Kairos"), TableName("[dbo].[KRS_Empleados]")]
    [DisplayName("Hoy"), InstanceName("Hoy")]
    [ReadPermission("Kairos:Hoy")]
    [ModifyPermission("Kairos:Modify")]
    [InsertPermission("Kairos:Insert")]
    [DeletePermission("Kairos:Delete")]
    public sealed class HoyRow : Row, IIdRow, INameRow
    {
        [EditLink,DisplayName("Id"), Column("id"), PrimaryKey]
        public Int64? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }


        [DisplayName("Nombre"), Column("nombre"), Size(50), NotNull, QuickSearch]
        public String Nombre
        {
            get { return Fields.Nombre[this]; }
            set { Fields.Nombre[this] = value; }
        }

        [DisplayName("Id Departamento"), Column("idDepartamento"), ForeignKey("KRS_Departamentos", "id"), LeftJoin("jDepartamentos")]
        public Int64? IdDepartamento
        {
            get { return Fields.IdDepartamento[this]; }
            set { Fields.IdDepartamento[this] = value; }
        }

        [DisplayName("Sede"), Expression("jDepartamentos.[SedeId]"), ForeignKey("sedes", "SedeId"), LeftJoin("jSede"), TextualField("Sede"), LookupInclude]
        [LookupEditor("Intranet.Sedes")]
        public Int32? SedeId
        {
            get { return Fields.SedeId[this]; }
            set { Fields.SedeId[this] = value; }
        }
        [DisplayName("Sede"), Expression("jSede.[Sede]")]
        public String Sede
        {
            get { return Fields.Sede[this]; }
            set { Fields.Sede[this] = value; }
        }

        [DisplayName("Estado"), Expression("(SELECT ISNULL((SELECT DISTINCT descripcion FROM KRS_AusenciasProgramadas LEFT JOIN KRS_AusenciasProgramadasTipos ON KRS_AusenciasProgramadas.idAusenciaProgramadaTipo=KRS_AusenciasProgramadasTipos.id WHERE KRS_AusenciasProgramadas.idEmpleado=t0.[id] AND KRS_AusenciasProgramadas.fechaBorrado Is NULL AND ((SYSDATETIME() BETWEEN fechaDesde AND fechaHasta) OR CONVERT(DATE,fechaHasta) = CONVERT(DATE,SYSDATETIME()))),'PRESENCIAL'))")]
        public String Estado
        {
            get { return Fields.Estado[this]; }
            set { Fields.Estado[this] = value; }
        }

        [DisplayName("Entrada"), Size(8), Expression("RIGHT(CONVERT(DATETIME, (SELECT Entrada FROM Fichajes WHERE Fichajes.idEmpleado=T0.[id] AND CONVERT(DATE,Entrada) = CONVERT(DATE,SYSDATETIME())), 108),8)")]
        public String HoraEntrada
        {
            get { return Fields.HoraEntrada[this]; }
            set { Fields.HoraEntrada[this] = value; }
        }
        [DisplayName("Salida"), Size(8), Expression("RIGHT(CONVERT(DATETIME, (SELECT Salida FROM Fichajes WHERE Fichajes.idEmpleado=T0.[id] AND CONVERT(DATE,Salida) = CONVERT(DATE,SYSDATETIME())), 108),8)")]
        public String HoraSalida
        {
            get { return Fields.HoraEntrada[this]; }
            set { Fields.HoraEntrada[this] = value; }
        }
        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Nombre; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public HoyRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int64Field Id;
            public StringField Nombre;
            public Int64Field IdDepartamento;
            public Int32Field SedeId;
            public StringField Sede;
            public StringField Estado;
            public StringField HoraEntrada;
            public StringField HoraSalida;

        }
    }
}
