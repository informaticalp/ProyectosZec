﻿
namespace ProyectosZec.Kairos {

    @Serenity.Decorators.registerClass()
    export class HoyDialog extends Serenity.EntityDialog<HoyRow, any> {
        protected getFormKey() { return HoyForm.formKey; }
        protected getIdProperty() { return HoyRow.idProperty; }
        protected getLocalTextPrefix() { return HoyRow.localTextPrefix; }
        protected getNameProperty() { return HoyRow.nameProperty; }
        protected getService() { return HoyService.baseUrl; }
        protected getDeletePermission() { return HoyRow.deletePermission; }
        protected getInsertPermission() { return HoyRow.insertPermission; }
        protected getUpdatePermission() { return HoyRow.updatePermission; }

        protected form = new HoyForm(this.idPrefix);

    }
}