﻿/// <reference path="../HorasExtraConsumidas/HorasExtraConsumidasGrid.ts" />
namespace ProyectosZec.Kairos {

    @Serenity.Decorators.registerClass()

    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido

    export class HorasExtraConsumidasReadOnlyGrid extends Kairos.HorasExtraConsumidasGrid {
        protected getColumnsKey() { return 'Kairos.HorasExtraConsumidasReadOnly'; }
        protected getDialogType() { return HorasExtraConsumidasReadOnlyDialog; }
 

        constructor(container: JQuery) {
            super(container);
        }
        /**
     * Removing add button from grid using its css class
     */
        public getButtons(): Serenity.ToolButton[] {
            var buttons = super.getButtons();
            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);

            //buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
            //    grid: this,
            //    onViewSubmit: () => this.onViewSubmit(),
            //    service: 'Kairos/HorasExtraConsumidas/ListExcel',
            //    separator: true
            //}));

            //buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
            //    grid: this,
            //    onViewSubmit: () => this.onViewSubmit()
            //}));
            return buttons;
        }
    }
}