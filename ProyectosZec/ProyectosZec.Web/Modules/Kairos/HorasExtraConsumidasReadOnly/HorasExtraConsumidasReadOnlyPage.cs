﻿
namespace ProyectosZec.Kairos.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Kairos/HorasExtraConsumidasReadOnly"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.HorasExtraConsumidasRow))]
    public class HorasExtraConsumidasReadOnlyController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Kairos/HorasExtraConsumidasReadOnly/HorasExtraConsumidasReadOnlyIndex.cshtml");
        }
    }
}