﻿
namespace ProyectosZec.Kairos.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Kairos.HorasExtraConsumidasReadOnly")]
    [BasedOnRow(typeof(Entities.HorasExtraConsumidasRow), CheckNames = true)]
    public class HorasExtraConsumidasReadOnlyColumns
    {
        [DisplayName("Id Hora Extra"),Width(120)]
        public Int64 IdHoraExtra { get; set; }
        [Width(200)]
        public String Empleado { get; set; }
        [DisplayFormat("#,##0.00"),AlignRight,Width(100)]
        public Decimal Tiempo { get; set; }
        [DisplayName("Dia"), QuickFilter, DisplayFormat("d")]
        public DateTime Dia { get; set; }
        [DisplayName("F. Autorización"),DisplayFormat("d"),Width(120)]
        public DateTime FechaAutorizacion { get; set; }
    }
}