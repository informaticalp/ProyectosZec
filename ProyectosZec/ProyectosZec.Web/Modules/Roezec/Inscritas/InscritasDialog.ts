﻿
namespace ProyectosZec.Roezec {

    @Serenity.Decorators.registerClass()
    export class InscritasDialog extends Serenity.EntityDialog<InscritasRow, any> {
        protected getFormKey() { return InscritasForm.formKey; }
        protected getIdProperty() { return InscritasRow.idProperty; }
        protected getLocalTextPrefix() { return InscritasRow.localTextPrefix; }
        protected getNameProperty() { return InscritasRow.nameProperty; }
        protected getService() { return InscritasService.baseUrl; }
        protected getDeletePermission() { return InscritasRow.deletePermission; }
        protected getInsertPermission() { return InscritasRow.insertPermission; }
        protected getUpdatePermission() { return InscritasRow.updatePermission; }

        protected form = new InscritasForm(this.idPrefix);

    }
}