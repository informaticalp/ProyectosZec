﻿
namespace ProyectosZec.Roezec {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido
    export class InscritasGrid extends Serenity.EntityGrid<InscritasRow, any> {
        protected getColumnsKey() { return 'Roezec.Inscritas'; }
        protected getDialogType() { return InscritasDialog; }
        protected getIdProperty() { return InscritasRow.idProperty; }
        protected getInsertPermission() { return InscritasRow.insertPermission; }
        protected getLocalTextPrefix() { return InscritasRow.localTextPrefix; }
        protected getService() { return InscritasService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }

        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Roezec/RoezecEmpresas/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));
            return buttons;
        }
        // Fin añadidos
    }
}