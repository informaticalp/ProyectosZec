﻿
namespace ProyectosZec.Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Roezec/Inscritas"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.InscritasRow))]
    public class InscritasController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Roezec/Inscritas/InscritasIndex.cshtml");
        }
    }
}