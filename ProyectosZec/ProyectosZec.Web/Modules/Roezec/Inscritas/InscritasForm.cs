﻿
namespace ProyectosZec.Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Roezec.Inscritas")]
    [BasedOnRow(typeof(Entities.InscritasRow), CheckNames = true)]
    public class InscritasForm
    {
        public Int32 IdEmpresa { get; set; }
        public Int32 NumeroAsiento { get; set; }
        public String NumeroLiquidacionTasa { get; set; }
        public DateTime FechaSolicitud { get; set; }
        public DateTime FechaResolucion { get; set; }
        public DateTime FechaNotificacion { get; set; }
        public String Observaciones { get; set; }
        public DateTime FechaAlta { get; set; }
        public String UsrAlta { get; set; }
    }
}