﻿
namespace ProyectosZec.Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Roezec.Inscritas")]
    [BasedOnRow(typeof(Entities.InscritasRow), CheckNames = true)]
    public class InscritasColumns
    {
        [DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Id { get; set; }
        [Width(200)]
        public String IdEmpresaDenominacionSocial { get; set; }
        [Width(85), DisplayName("CIF")]
        public String IdEmpresaCif { get; set; }
        [Hidden,Width(200)]
        public String IdEmpresaDireccion { get; set; }
        [Hidden,Width(90)]
        public String IdEmpresaCp { get; set; }
        [Width(140)]
        public String IdEmpresaPoblacion { get; set; }
        [QuickFilter]
        [Width(90)]
        public String IdEmpresaProvincia { get; set; }
        [QuickFilter]
        [Width(90)]
        public String IdEmpresaIsla { get; set; }
        [Width(80)]
        public Int32 NumeroAsiento { get; set; }
        [Width(100)]
        public String NumeroLiquidacionTasa { get; set; }
        [Width(120)]
        public DateTime FechaSolicitud { get; set; }
        [Width(120)]
        public DateTime FechaResolucion { get; set; }
        [Width(120)]
        public DateTime FechaNotificacion { get; set; }
        public String Observaciones { get; set; }
        [Width(70), AlignRight, QuickFilter]
        public Int32 IdEmpresaAnyoExpediente { get; set; }
        [QuickFilter, AlignRight]
        [Width(80)]
        public Int32 IdEmpresaNumExpediente { get; set; }
        public DateTime FechaAlta { get; set; }
        public String UsrAlta { get; set; }
    }
}