﻿
namespace ProyectosZec.Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec_old"), Module("Roezec"), TableName("roezec_estado_inscrita")]
    [DisplayName("Inscritas"), InstanceName("Inscritas")]
    [ReadPermission("Roezec_Old:Read")]
    [ModifyPermission("Roezec_Old:Modify")]
    [InsertPermission("Roezec_Old:Insert")]
    [DeletePermission("Roezec_Old:Delete")]
    public sealed class InscritasRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Id Empresa"), Column("id_empresa"), NotNull, ForeignKey("roezec_empresas", "id"), LeftJoin("jIdEmpresa"), TextualField("IdEmpresaDenominacionSocial")]
        public Int32? IdEmpresa
        {
            get { return Fields.IdEmpresa[this]; }
            set { Fields.IdEmpresa[this] = value; }
        }

        [DisplayName("Nº Roezec"), Column("numero_asiento"), NotNull]
        public Int32? NumeroAsiento
        {
            get { return Fields.NumeroAsiento[this]; }
            set { Fields.NumeroAsiento[this] = value; }
        }

        [DisplayName("Nº Liq. Tasa"), Column("numero_liquidacion_tasa"), Size(255), NotNull, QuickSearch]
        public String NumeroLiquidacionTasa
        {
            get { return Fields.NumeroLiquidacionTasa[this]; }
            set { Fields.NumeroLiquidacionTasa[this] = value; }
        }

        [DisplayName("Fec. Solicitud"), Expression("IF(fecha_solicitud='0000-00-00 00:00:00',null,fecha_solicitud)")]
        public DateTime? FechaSolicitud
        {
            get { return Fields.FechaSolicitud[this]; }
            set { Fields.FechaSolicitud[this] = value; }
        }

        [DisplayName("Fec. Resolucion"), Expression("IF(fecha_resolucion='0000-00-00 00:00:00',null,fecha_resolucion)")]
        public DateTime? FechaResolucion
        {
            get { return Fields.FechaResolucion[this]; }
            set { Fields.FechaResolucion[this] = value; }
        }

        [DisplayName("Fec. Notificacion"),  Expression("IF(fecha_notificacion='0000-00-00 00:00:00',null,fecha_notificacion)")]
        public DateTime? FechaNotificacion
        {
            get { return Fields.FechaNotificacion[this]; }
            set { Fields.FechaNotificacion[this] = value; }
        }

        [DisplayName("Observaciones"), Column("observaciones"), Size(255), NotNull]
        public String Observaciones
        {
            get { return Fields.Observaciones[this]; }
            set { Fields.Observaciones[this] = value; }
        }

        [DisplayName("Fecha Alta"), Expression("IF(T0.fecha_alta='0000-00-00 00:00:00',null,T0.fecha_alta)")]
        public DateTime? FechaAlta
        {
            get { return Fields.FechaAlta[this]; }
            set { Fields.FechaAlta[this] = value; }
        }

        [DisplayName("Usr Alta"), Column("usr_alta"), Size(255), NotNull]
        public String UsrAlta
        {
            get { return Fields.UsrAlta[this]; }
            set { Fields.UsrAlta[this] = value; }
        }

        [DisplayName("Razon Social"), Expression("jIdEmpresa.[denominacion_social]"),QuickSearch]
        public String IdEmpresaDenominacionSocial
        {
            get { return Fields.IdEmpresaDenominacionSocial[this]; }
            set { Fields.IdEmpresaDenominacionSocial[this] = value; }
        }

        [DisplayName("Cif"), Expression("jIdEmpresa.[cif]")]
        public String IdEmpresaCif
        {
            get { return Fields.IdEmpresaCif[this]; }
            set { Fields.IdEmpresaCif[this] = value; }
        }

        [DisplayName("Direccion"), Expression("jIdEmpresa.[direccion]")]
        public String IdEmpresaDireccion
        {
            get { return Fields.IdEmpresaDireccion[this]; }
            set { Fields.IdEmpresaDireccion[this] = value; }
        }

        [DisplayName("Cp"), Expression("jIdEmpresa.[cp]")]
        public String IdEmpresaCp
        {
            get { return Fields.IdEmpresaCp[this]; }
            set { Fields.IdEmpresaCp[this] = value; }
        }

        [DisplayName("Poblacion"), Expression("jIdEmpresa.[poblacion]")]
        public String IdEmpresaPoblacion
        {
            get { return Fields.IdEmpresaPoblacion[this]; }
            set { Fields.IdEmpresaPoblacion[this] = value; }
        }

        [DisplayName("Provincia"), Expression("jIdEmpresa.[provincia]")]
        public String IdEmpresaProvincia
        {
            get { return Fields.IdEmpresaProvincia[this]; }
            set { Fields.IdEmpresaProvincia[this] = value; }
        }

        [DisplayName("Isla"), Expression("jIdEmpresa.[isla]")]
        public String IdEmpresaIsla
        {
            get { return Fields.IdEmpresaIsla[this]; }
            set { Fields.IdEmpresaIsla[this] = value; }
        }

        [DisplayName("Notas Marginales"), Expression("jIdEmpresa.[notas_marginales]")]
        public String IdEmpresaNotasMarginales
        {
            get { return Fields.IdEmpresaNotasMarginales[this]; }
            set { Fields.IdEmpresaNotasMarginales[this] = value; }
        }

        [DisplayName("Anyo Exp"), Expression("jIdEmpresa.[anyo_expediente]")]
        public Int32? IdEmpresaAnyoExpediente
        {
            get { return Fields.IdEmpresaAnyoExpediente[this]; }
            set { Fields.IdEmpresaAnyoExpediente[this] = value; }
        }

        [DisplayName("Num Expediente"), Expression("jIdEmpresa.[num_expediente]")]
        public Int32? IdEmpresaNumExpediente
        {
            get { return Fields.IdEmpresaNumExpediente[this]; }
            set { Fields.IdEmpresaNumExpediente[this] = value; }
        }

        [DisplayName("Agencia"), Expression("jIdEmpresa.[agencia]")]
        public Int32? IdEmpresaAgencia
        {
            get { return Fields.IdEmpresaAgencia[this]; }
            set { Fields.IdEmpresaAgencia[this] = value; }
        }

        [DisplayName("Tecnico"), Expression("jIdEmpresa.[tecnico]")]
        public String IdEmpresaTecnico
        {
            get { return Fields.IdEmpresaTecnico[this]; }
            set { Fields.IdEmpresaTecnico[this] = value; }
        }

        [DisplayName("Forma Juridica"), Expression("jIdEmpresa.[forma_juridica]")]
        public String IdEmpresaFormaJuridica
        {
            get { return Fields.IdEmpresaFormaJuridica[this]; }
            set { Fields.IdEmpresaFormaJuridica[this] = value; }
        }

        [DisplayName("Superficie"), Expression("jIdEmpresa.[superficie]")]
        public Double? IdEmpresaSuperficie
        {
            get { return Fields.IdEmpresaSuperficie[this]; }
            set { Fields.IdEmpresaSuperficie[this] = value; }
        }

        [DisplayName("Exenta"), Expression("jIdEmpresa.[exenta_area_acotada]")]
        public String IdEmpresaExentaAreaAcotada
        {
            get { return Fields.IdEmpresaExentaAreaAcotada[this]; }
            set { Fields.IdEmpresaExentaAreaAcotada[this] = value; }
        }

        [DisplayName("Motivos Exencion"), Expression("jIdEmpresa.[motivos_exencion]")]
        public String IdEmpresaMotivosExencion
        {
            get { return Fields.IdEmpresaMotivosExencion[this]; }
            set { Fields.IdEmpresaMotivosExencion[this] = value; }
        }

        [DisplayName("Obj. Empleo"), Expression("jIdEmpresa.[objetivo_empleo]")]
        public Double? IdEmpresaObjetivoEmpleo
        {
            get { return Fields.IdEmpresaObjetivoEmpleo[this]; }
            set { Fields.IdEmpresaObjetivoEmpleo[this] = value; }
        }

        [DisplayName("Obj Invers."), Expression("jIdEmpresa.[objetivo_inversion]")]
        public Double? IdEmpresaObjetivoInversion
        {
            get { return Fields.IdEmpresaObjetivoInversion[this]; }
            set { Fields.IdEmpresaObjetivoInversion[this] = value; }
        }

        [DisplayName("Observaciones Empleo"), Expression("jIdEmpresa.[observaciones_empleo]")]
        public String IdEmpresaObservacionesEmpleo
        {
            get { return Fields.IdEmpresaObservacionesEmpleo[this]; }
            set { Fields.IdEmpresaObservacionesEmpleo[this] = value; }
        }

        [DisplayName("Observaciones Inversion"), Expression("jIdEmpresa.[observaciones_inversion]")]
        public String IdEmpresaObservacionesInversion
        {
            get { return Fields.IdEmpresaObservacionesInversion[this]; }
            set { Fields.IdEmpresaObservacionesInversion[this] = value; }
        }

        [DisplayName("Pre Empleo"), Expression("jIdEmpresa.[pre_empleo]")]
        public Int32? IdEmpresaPreEmpleo
        {
            get { return Fields.IdEmpresaPreEmpleo[this]; }
            set { Fields.IdEmpresaPreEmpleo[this] = value; }
        }

        [DisplayName("Pre Inversion"), Expression("jIdEmpresa.[pre_inversion]")]
        public Int32? IdEmpresaPreInversion
        {
            get { return Fields.IdEmpresaPreInversion[this]; }
            set { Fields.IdEmpresaPreInversion[this] = value; }
        }

        [DisplayName("Tras Empleo"), Expression("jIdEmpresa.[tras_empleo]")]
        public Int32? IdEmpresaTrasEmpleo
        {
            get { return Fields.IdEmpresaTrasEmpleo[this]; }
            set { Fields.IdEmpresaTrasEmpleo[this] = value; }
        }

        [DisplayName("Tras Inversion"), Expression("jIdEmpresa.[tras_inversion]")]
        public Int32? IdEmpresaTrasInversion
        {
            get { return Fields.IdEmpresaTrasInversion[this]; }
            set { Fields.IdEmpresaTrasInversion[this] = value; }
        }

        [DisplayName("Fecha Alta"), Expression("jIdEmpresa.[fecha_alta]")]
        public DateTime? IdEmpresaFechaAlta
        {
            get { return Fields.IdEmpresaFechaAlta[this]; }
            set { Fields.IdEmpresaFechaAlta[this] = value; }
        }

        [DisplayName("Fecha Modificacion"), Expression("jIdEmpresa.[fecha_modificacion]")]
        public DateTime? IdEmpresaFechaModificacion
        {
            get { return Fields.IdEmpresaFechaModificacion[this]; }
            set { Fields.IdEmpresaFechaModificacion[this] = value; }
        }

        [DisplayName("Fecha Baja"), Expression("jIdEmpresa.[fecha_baja]")]
        public DateTime? IdEmpresaFechaBaja
        {
            get { return Fields.IdEmpresaFechaBaja[this]; }
            set { Fields.IdEmpresaFechaBaja[this] = value; }
        }

        [DisplayName("Situacion"), Expression("jIdEmpresa.[situacion]")]
        public String IdEmpresaSituacion
        {
            get { return Fields.IdEmpresaSituacion[this]; }
            set { Fields.IdEmpresaSituacion[this] = value; }
        }

        [DisplayName("Usr Alta"), Expression("jIdEmpresa.[usr_alta]")]
        public String IdEmpresaUsrAlta
        {
            get { return Fields.IdEmpresaUsrAlta[this]; }
            set { Fields.IdEmpresaUsrAlta[this] = value; }
        }

        [DisplayName("Usr Modificacion"), Expression("jIdEmpresa.[usr_modificacion]")]
        public String IdEmpresaUsrModificacion
        {
            get { return Fields.IdEmpresaUsrModificacion[this]; }
            set { Fields.IdEmpresaUsrModificacion[this] = value; }
        }

        [DisplayName("Usr Baja"), Expression("jIdEmpresa.[usr_baja]")]
        public String IdEmpresaUsrBaja
        {
            get { return Fields.IdEmpresaUsrBaja[this]; }
            set { Fields.IdEmpresaUsrBaja[this] = value; }
        }
        [DisplayName("Trading"), Expression("jIdEmpresa.[trading]"), Size(1)]
        public Boolean? trading
        {
            get { return Fields.trading[this]; }
            set { Fields.trading[this] = value; }
        }


        [DisplayName("Empleos2021"), Expression("jIdEmpresa.[empleos2021]")]
        public Decimal? IdEmpresaEmpleos2021
        {
            get { return Fields.IdEmpresaEmpleos2021[this]; }
            set { Fields.IdEmpresaEmpleos2021[this] = value; }
        }

        [DisplayName("Empleos2020"), Expression("jIdEmpresa.[empleos2020]")]
        public Decimal? IdEmpresaEmpleos2020
        {
            get { return Fields.IdEmpresaEmpleos2020[this]; }
            set { Fields.IdEmpresaEmpleos2020[this] = value; }
        }

        [DisplayName("Empleos2019"), Expression("jIdEmpresa.[empleos2019]")]
        public Decimal? IdEmpresaEmpleos2019
        {
            get { return Fields.IdEmpresaEmpleos2019[this]; }
            set { Fields.IdEmpresaEmpleos2019[this] = value; }
        }

        [DisplayName("Empleos2018"), Expression("jIdEmpresa.[empleos2018]")]
        public Decimal? IdEmpresaEmpleos2018
        {
            get { return Fields.IdEmpresaEmpleos2018[this]; }
            set { Fields.IdEmpresaEmpleos2018[this] = value; }
        }

        [DisplayName("Empleos2017"), Expression("jIdEmpresa.[empleos2017]")]
        public Decimal? IdEmpresaEmpleos2017
        {
            get { return Fields.IdEmpresaEmpleos2017[this]; }
            set { Fields.IdEmpresaEmpleos2017[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.NumeroLiquidacionTasa; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public InscritasRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field IdEmpresa;
            public Int32Field NumeroAsiento;
            public StringField NumeroLiquidacionTasa;
            public DateTimeField FechaSolicitud;
            public DateTimeField FechaResolucion;
            public DateTimeField FechaNotificacion;
            public StringField Observaciones;
            public DateTimeField FechaAlta;
            public StringField UsrAlta;

            public StringField IdEmpresaDenominacionSocial;
            public StringField IdEmpresaCif;
            public StringField IdEmpresaDireccion;
            public StringField IdEmpresaCp;
            public StringField IdEmpresaPoblacion;
            public StringField IdEmpresaProvincia;
            public StringField IdEmpresaIsla;
            public StringField IdEmpresaNotasMarginales;
            public Int32Field IdEmpresaAnyoExpediente;
            public Int32Field IdEmpresaNumExpediente;
            public Int32Field IdEmpresaAgencia;
            public StringField IdEmpresaTecnico;
            public StringField IdEmpresaFormaJuridica;
            public DoubleField IdEmpresaSuperficie;
            public StringField IdEmpresaExentaAreaAcotada;
            public StringField IdEmpresaMotivosExencion;
            public DoubleField IdEmpresaObjetivoEmpleo;
            public DoubleField IdEmpresaObjetivoInversion;
            public StringField IdEmpresaObservacionesEmpleo;
            public StringField IdEmpresaObservacionesInversion;
            public Int32Field IdEmpresaPreEmpleo;
            public Int32Field IdEmpresaPreInversion;
            public Int32Field IdEmpresaTrasEmpleo;
            public Int32Field IdEmpresaTrasInversion;
            public DateTimeField IdEmpresaFechaAlta;
            public DateTimeField IdEmpresaFechaModificacion;
            public DateTimeField IdEmpresaFechaBaja;
            public StringField IdEmpresaSituacion;
            public StringField IdEmpresaUsrAlta;
            public StringField IdEmpresaUsrModificacion;
            public StringField IdEmpresaUsrBaja;
            public BooleanField trading;
            public DecimalField IdEmpresaEmpleos2021;
            public DecimalField IdEmpresaEmpleos2020;
            public DecimalField IdEmpresaEmpleos2019;
            public DecimalField IdEmpresaEmpleos2018;
            public DecimalField IdEmpresaEmpleos2017;
        }
    }
}
