﻿
namespace ProyectosZec.Roezec {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido
    export class EmpleosSSGrid extends Serenity.EntityGrid<EmpleosSSRow, any> {
        protected getColumnsKey() { return 'Roezec.EmpleosSS'; }
        protected getDialogType() { return EmpleosSSDialog; }
        protected getIdProperty() { return EmpleosSSRow.idProperty; }
        protected getInsertPermission() { return EmpleosSSRow.insertPermission; }
        protected getLocalTextPrefix() { return EmpleosSSRow.localTextPrefix; }
        protected getService() { return EmpleosSSService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }

        // Agrupar y sumar 
        protected createSlickGrid() {
            var grid = super.createSlickGrid();

            // need to register this plugin for grouping or you'll have errors
            grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());

            // sumamos Objetivo Empleo y de Inversión
            this.view.setSummaryOptions({
                aggregators: [

                    new Slick.Aggregators.Sum(EmpleosSSRow.Fields.Empleos)
                ]
            });

            return grid;
        }
        // Mostramos Footer con los totales
        protected getSlickOptions() {
            var opt = super.getSlickOptions();
            opt.showFooterRow = true;

            return opt;
        }

        protected usePager() {
            return false;
        }



        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Roezec/EmpleosSS/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            buttons.push(
                {
                    title: 'Agrupar por Año Exped.',
                    cssClass: 'expand-all-button',
                    onClick: () => this.view.setGrouping(
                        [{
                            formatter: x => 'Año: ' + x.value + ' (' + x.count + ' Empresas)',
                            getter: EmpleosSSRow.Fields.AnyoExpediente
                        }])
                }
            );
            buttons.push(
                {
                    title: 'Agrupar por Año',
                    cssClass: 'expand-all-button',
                    onClick: () => this.view.setGrouping(
                        [{
                            formatter: x => 'Año: ' + x.value + ' (' + x.count + ' Empresas)',
                            getter: EmpleosSSRow.Fields.Anyo
                        }])
                }
            );
            buttons.push(
                {
                    title: 'Agrupar por Isla',
                    cssClass: 'expand-all-button',
                    onClick: () => this.view.setGrouping(
                        [{
                            formatter: x => 'Isla: ' + x.value + ' (' + x.count + ' Empresas)',
                            getter: EmpleosSSRow.Fields.Isla
                        }])
                }
            );
            buttons.push(
                {
                    title: 'Agrupar por Año e Isla',
                    cssClass: 'expand-all-button',
                    onClick: () => this.view.setGrouping(
                        [{
                            formatter: x => 'Año: ' + x.value + ' (' + x.count + ' Empresas/año)',
                            getter: EmpleosSSRow.Fields.Anyo
                        }, {
                            formatter: x => 'Isla: ' + x.value + ' (' + x.count + ' Empresas)',
                            getter: EmpleosSSRow.Fields.Isla
                        }])
                }
            );

            buttons.push(
                {
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: () => this.view.setGrouping([])
                }
            );

            return buttons;
            // Fin añadidos

        }
    }
}