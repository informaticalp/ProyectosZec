﻿
namespace ProyectosZec.Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Roezec/EmpleosSS"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EmpleosSSRow))]
    public class EmpleosSSController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Roezec/EmpleosSS/EmpleosSSIndex.cshtml");
        }
    }
}