﻿
namespace ProyectosZec.Roezec {

    @Serenity.Decorators.registerClass()
    export class EmpleosSSDialog extends Serenity.EntityDialog<EmpleosSSRow, any> {
        protected getFormKey() { return EmpleosSSForm.formKey; }
        protected getIdProperty() { return EmpleosSSRow.idProperty; }
        protected getLocalTextPrefix() { return EmpleosSSRow.localTextPrefix; }
        protected getService() { return EmpleosSSService.baseUrl; }
        protected getDeletePermission() { return EmpleosSSRow.deletePermission; }
        protected getInsertPermission() { return EmpleosSSRow.insertPermission; }
        protected getUpdatePermission() { return EmpleosSSRow.updatePermission; }

        protected form = new EmpleosSSForm(this.idPrefix);

    }
}