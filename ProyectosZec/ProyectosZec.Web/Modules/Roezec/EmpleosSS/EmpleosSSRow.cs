﻿
namespace ProyectosZec.Roezec.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Roezec_old"), Module("Roezec"), TableName("ss")]
    [DisplayName("Empleos Ss"), InstanceName("Empleos Ss")]
    [ReadPermission("Roezec_Old_SS:Read")]
    [ModifyPermission("Roezec_Old:Modify")]
    [InsertPermission("Roezec_Old:Insert")]
    [DeletePermission("Roezec_Old:Delete")]
    public sealed class EmpleosSSRow : Row, IIdRow
    {
        [DisplayName("Id"), Column("id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }
        [DisplayName("Id Empresa"), Column("id_empresa"), NotNull, ForeignKey("roezec_empresas", "id"), LeftJoin("jEmpresas")]
        public Int32? IdEmpresa
        {
            get { return Fields.IdEmpresa[this]; }
            set { Fields.IdEmpresa[this] = value; }
        }

        [DisplayName("Fecha"), Column("fecha")]
        public DateTime? Fecha
        {
            get { return Fields.Fecha[this]; }
            set { Fields.Fecha[this] = value; }
        }

        [DisplayName("Año"), Expression("Year(t0.[fecha])"),QuickSearch]
        public Int32? Anyo
        {
            get { return Fields.Anyo[this]; }
            set { Fields.Anyo[this] = value; }
        }

        [DisplayName("Empleos"), Column("empleos"), Size(8), Scale(2)]
        public Decimal? Empleos
        {
            get { return Fields.Empleos[this]; }
            set { Fields.Empleos[this] = value; }
        }

        [DisplayName("Isla"), Expression("jEmpresas.isla"), Size(255), NotNull,QuickSearch]
        public String Isla
        {
            get { return Fields.Isla[this]; }
            set { Fields.Isla[this] = value; }
        }

        [DisplayName("Año Exp."), Expression("jEmpresas.anyo_expediente")]
        public Int32? AnyoExpediente
        {
            get { return Fields.AnyoExpediente[this]; }
            set { Fields.AnyoExpediente[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public EmpleosSSRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public Int32Field IdEmpresa;
            public DateTimeField Fecha;
            public DecimalField Empleos;
            public StringField Isla;
            public Int32Field AnyoExpediente;
            public Int32Field Anyo;
        }
    }
}
