﻿
namespace ProyectosZec.Roezec.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Roezec.EmpleosSS")]
    [BasedOnRow(typeof(Entities.EmpleosSSRow), CheckNames = true)]
    public class EmpleosSSColumns
    {
        [DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Id { get; set; }
        [Width(80),AlignRight]
        public Int32 IdEmpresa { get; set; }
        [QuickFilter]
        [Width(80)]
        public Int32 Anyo { get; set; }
        [QuickFilter]
        [Width(90)]
        public Int32 AnyoExpediente { get; set; }
        [QuickFilter]
        [Width(95)]
        public String Isla { get; set; }
        [Width(85),AlignRight, DisplayFormat("#,###.00")]
        public Decimal Empleos { get; set; }
    }
}