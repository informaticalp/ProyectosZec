﻿
namespace ProyectosZec.Roezec.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Roezec.EmpleosSS")]
    [BasedOnRow(typeof(Entities.EmpleosSSRow), CheckNames = true)]
    public class EmpleosSSForm
    {
        public DateTime Fecha { get; set; }
        public Decimal Empleos { get; set; }
    }
}