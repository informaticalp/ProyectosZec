﻿
namespace ProyectosZec.Roezec {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido
    export class RoezecEmpresasSSGrid extends Serenity.EntityGrid<RoezecEmpresasSSRow, any> {
        protected getColumnsKey() { return 'Roezec.RoezecEmpresasSS'; }
        protected getDialogType() { return RoezecEmpresasSSDialog; }
        protected getIdProperty() { return RoezecEmpresasSSRow.idProperty; }
        protected getInsertPermission() { return RoezecEmpresasSSRow.insertPermission; }
        protected getLocalTextPrefix() { return RoezecEmpresasSSRow.localTextPrefix; }
        protected getService() { return RoezecEmpresasSSService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        // Agrupar y sumar 
        protected createSlickGrid() {
            var grid = super.createSlickGrid();

            // need to register this plugin for grouping or you'll have errors
            grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());

            // sumamos Objetivo Empleo y de Inversión
            this.view.setSummaryOptions({
                aggregators: [

                    new Slick.Aggregators.Sum(RoezecEmpresasSSRow.Fields.ObjetivoEmpleo),
                    new Slick.Aggregators.Sum(RoezecEmpresasSSRow.Fields.ObjetivoInversion),
                    new Slick.Aggregators.Sum(RoezecEmpresasSSRow.Fields.Empleos2021),
                    new Slick.Aggregators.Sum(RoezecEmpresasSSRow.Fields.Empleos2020),
                    new Slick.Aggregators.Sum(RoezecEmpresasSSRow.Fields.Empleos2019),
                    new Slick.Aggregators.Sum(RoezecEmpresasSSRow.Fields.Empleos2018),
                    new Slick.Aggregators.Sum(RoezecEmpresasSSRow.Fields.Empleos2017)
                ]
            });

            return grid;
        }
        // Mostramos Footer con los totales
        protected getSlickOptions() {
            var opt = super.getSlickOptions();
            opt.showFooterRow = true;
            
            return opt;
        }

        protected usePager() {
            return false;
        }


        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'Roezec/RoezecEmpresasSS/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            buttons.push(
                {
                    title: 'Agrupar por Año',
                    cssClass: 'expand-all-button',
                    onClick: () => this.view.setGrouping(
                        [{
                            formatter: x => 'Año: ' + x.value + ' (' + x.count + ' Empresas)',
                            getter: RoezecEmpresasSSRow.Fields.AnyoExpediente
                        }])
                }
            );
            buttons.push(
                {
                    title: 'Agrupar por Nace',
                    cssClass: 'expand-all-button',
                    onClick: () => this.view.setGrouping(
                        [{
                            formatter: x => 'Nace: ' + x.value + ' (' + x.count + ' Empresas)',
                            getter: RoezecEmpresasSSRow.Fields.NacePrincipal
                        }])
                }
            );
            buttons.push(
                {
                    title: 'Agrupar por Año y Técnico',
                    cssClass: 'expand-all-button',
                    onClick: () => this.view.setGrouping(
                        [{
                            formatter: x => 'Año: ' + x.value + ' (' + x.count + ' Empresas)',
                            getter: RoezecEmpresasSSRow.Fields.AnyoExpediente
                        }, {
                            formatter: x => 'Técnico: ' + x.value + ' (' + x.count + ' Empresas)',
                            getter: RoezecEmpresasSSRow.Fields.Tecnico
                        }])
                }
            );
            buttons.push(
                {
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: () => this.view.setGrouping([])
                }
            );

            return buttons;
            // Fin añadidos
        }
    }
}