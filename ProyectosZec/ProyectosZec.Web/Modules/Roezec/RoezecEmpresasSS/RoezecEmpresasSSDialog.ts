﻿
namespace ProyectosZec.Roezec {

    @Serenity.Decorators.registerClass()
    export class RoezecEmpresasSSDialog extends Serenity.EntityDialog<RoezecEmpresasSSRow, any> {
        protected getFormKey() { return RoezecEmpresasSSForm.formKey; }
        protected getIdProperty() { return RoezecEmpresasSSRow.idProperty; }
        protected getLocalTextPrefix() { return RoezecEmpresasSSRow.localTextPrefix; }
        protected getNameProperty() { return RoezecEmpresasSSRow.nameProperty; }
        protected getService() { return RoezecEmpresasSSService.baseUrl; }
        protected getDeletePermission() { return RoezecEmpresasSSRow.deletePermission; }
        protected getInsertPermission() { return RoezecEmpresasSSRow.insertPermission; }
        protected getUpdatePermission() { return RoezecEmpresasSSRow.updatePermission; }

        protected form = new RoezecEmpresasSSForm(this.idPrefix);

    }
}