﻿
namespace ProyectosZec.Roezec.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Roezec/RoezecEmpresasSS"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.RoezecEmpresasSSRow))]
    public class RoezecEmpresasSSController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Roezec/RoezecEmpresasSS/RoezecEmpresasSSIndex.cshtml");
        }
    }
}