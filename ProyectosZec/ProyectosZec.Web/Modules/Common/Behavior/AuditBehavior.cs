﻿
using Serenity;
using Serenity.Data;
using Serenity.Services;
using System;
using System.Globalization;

namespace ProyectosZec.Behaviors
{
    public class AuditBehavior : BaseSaveBehavior, IImplicitBehavior
    
    {
        private Field UserId;
        private DateTimeField LastModification;

        public bool ActivateFor(Row row)
        {
            //if (!row.IsAnyFieldChanged) return false;
           
            LastModification = (row.FindFieldByPropertyName("FechaModificacion") ??
            row.FindField("fecha_modificacion")) as DateTimeField;

            UserId = row.FindFieldByPropertyName("UserId") ??
            row.FindField("user_id");

            return !ReferenceEquals(null, LastModification) &&
            !ReferenceEquals(null, UserId);                      
        }

        public override void OnSetInternalFields(ISaveRequestHandler handler)
        {
            // *******************************************************************************************************************
            // This does not work because every save button changes last userId and lastModification unless nothing has changed
            // *******************************************************************************************************************
            if ((handler.IsUpdate && !ReferenceEquals(handler.Row,handler.Old)) || (handler.IsCreate))
            {               
                LastModification[handler.Row] = DateTime.Now;
                UserId.AsObject(handler.Row, Authorization.UserId == null ? null :
                UserId.ConvertValue(Authorization.UserId, CultureInfo.InvariantCulture));
            }            
        }

    }
}