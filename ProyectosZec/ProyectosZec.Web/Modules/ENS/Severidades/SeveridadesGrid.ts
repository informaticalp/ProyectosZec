﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    export class SeveridadesGrid extends Serenity.EntityGrid<SeveridadesRow, any> {
        protected getColumnsKey() { return 'ENS.Severidades'; }
        protected getDialogType() { return SeveridadesDialog; }
        protected getIdProperty() { return SeveridadesRow.idProperty; }
        protected getInsertPermission() { return SeveridadesRow.insertPermission; }
        protected getLocalTextPrefix() { return SeveridadesRow.localTextPrefix; }
        protected getService() { return SeveridadesService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}