﻿
namespace ProyectosZec.ENS.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("ENS"), TableName("severidades")]
    [DisplayName("Severidades"), InstanceName("Severidades")]
    [ReadPermission("Ens:General")]
    [ModifyPermission("Ens:General")]
    [LookupScript("Ens.Severidades")]
    public sealed class SeveridadesRow : Row, IIdRow, INameRow
    {
        [DisplayName("Severidad Id"), Identity]
        public Int32? SeveridadId
        {
            get { return Fields.SeveridadId[this]; }
            set { Fields.SeveridadId[this] = value; }
        }

        [DisplayName("Severidad"), Size(30), NotNull, QuickSearch]
        public String Severidad
        {
            get { return Fields.Severidad[this]; }
            set { Fields.Severidad[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.SeveridadId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Severidad; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public SeveridadesRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field SeveridadId;
            public StringField Severidad;
        }
    }
}
