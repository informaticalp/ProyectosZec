﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    export class SeveridadesDialog extends Serenity.EntityDialog<SeveridadesRow, any> {
        protected getFormKey() { return SeveridadesForm.formKey; }
        protected getIdProperty() { return SeveridadesRow.idProperty; }
        protected getLocalTextPrefix() { return SeveridadesRow.localTextPrefix; }
        protected getNameProperty() { return SeveridadesRow.nameProperty; }
        protected getService() { return SeveridadesService.baseUrl; }
        protected getDeletePermission() { return SeveridadesRow.deletePermission; }
        protected getInsertPermission() { return SeveridadesRow.insertPermission; }
        protected getUpdatePermission() { return SeveridadesRow.updatePermission; }

        protected form = new SeveridadesForm(this.idPrefix);

    }
}