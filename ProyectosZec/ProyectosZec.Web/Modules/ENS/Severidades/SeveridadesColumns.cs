﻿
namespace ProyectosZec.ENS.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("ENS.Severidades")]
    [BasedOnRow(typeof(Entities.SeveridadesRow), CheckNames = true)]
    public class SeveridadesColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 SeveridadId { get; set; }
        [EditLink]
        public String Severidad { get; set; }
    }
}