﻿
namespace ProyectosZec.ENS.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("ENS.Severidades")]
    [BasedOnRow(typeof(Entities.SeveridadesRow), CheckNames = true)]
    public class SeveridadesForm
    {
        public String Severidad { get; set; }
    }
}