﻿
namespace ProyectosZec.ENS.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("ENS/Severidades"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.SeveridadesRow))]
    public class SeveridadesController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/ENS/Severidades/SeveridadesIndex.cshtml");
        }
    }
}