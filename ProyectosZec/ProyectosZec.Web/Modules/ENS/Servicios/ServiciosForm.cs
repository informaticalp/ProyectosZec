﻿
namespace ProyectosZec.ENS.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("ENS.Servicios")]
    [BasedOnRow(typeof(Entities.ServiciosRow), CheckNames = true)]
    public class ServiciosForm
    {
        public String Servicio { get; set; }
    }
}