﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    export class ServiciosGrid extends Serenity.EntityGrid<ServiciosRow, any> {
        protected getColumnsKey() { return 'ENS.Servicios'; }
        protected getDialogType() { return ServiciosDialog; }
        protected getIdProperty() { return ServiciosRow.idProperty; }
        protected getInsertPermission() { return ServiciosRow.insertPermission; }
        protected getLocalTextPrefix() { return ServiciosRow.localTextPrefix; }
        protected getService() { return ServiciosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}