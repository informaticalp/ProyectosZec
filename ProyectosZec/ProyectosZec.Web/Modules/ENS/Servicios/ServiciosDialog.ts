﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    export class ServiciosDialog extends Serenity.EntityDialog<ServiciosRow, any> {
        protected getFormKey() { return ServiciosForm.formKey; }
        protected getIdProperty() { return ServiciosRow.idProperty; }
        protected getLocalTextPrefix() { return ServiciosRow.localTextPrefix; }
        protected getNameProperty() { return ServiciosRow.nameProperty; }
        protected getService() { return ServiciosService.baseUrl; }
        protected getDeletePermission() { return ServiciosRow.deletePermission; }
        protected getInsertPermission() { return ServiciosRow.insertPermission; }
        protected getUpdatePermission() { return ServiciosRow.updatePermission; }

        protected form = new ServiciosForm(this.idPrefix);

    }
}