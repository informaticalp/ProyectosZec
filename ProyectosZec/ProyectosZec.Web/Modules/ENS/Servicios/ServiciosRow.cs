﻿
namespace ProyectosZec.ENS.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("ENS"), TableName("servicios")]
    [DisplayName("Servicios"), InstanceName("Servicios")]
    [ReadPermission("Ens:General")]
    [ModifyPermission("Ens:General")]
    [LookupScript("Ens.Servicios")]
    public sealed class ServiciosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Servicio Id"), Identity]
        public Int32? ServicioId
        {
            get { return Fields.ServicioId[this]; }
            set { Fields.ServicioId[this] = value; }
        }

        [DisplayName("Servicio"), Size(30), NotNull, QuickSearch]
        public String Servicio
        {
            get { return Fields.Servicio[this]; }
            set { Fields.Servicio[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.ServicioId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Servicio; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public ServiciosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field ServicioId;
            public StringField Servicio;
        }
    }
}
