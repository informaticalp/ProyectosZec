﻿
namespace ProyectosZec.ENS.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("ENS.Servicios")]
    [BasedOnRow(typeof(Entities.ServiciosRow), CheckNames = true)]
    public class ServiciosColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 ServicioId { get; set; }
        [EditLink]
        public String Servicio { get; set; }
    }
}