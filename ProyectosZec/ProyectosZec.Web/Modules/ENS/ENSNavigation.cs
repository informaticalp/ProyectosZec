﻿using Serenity.Navigation;
using MyPages = ProyectosZec.ENS.Pages;

[assembly: NavigationMenu(int.MaxValue, "ENS", icon: "fa-magic")]
[assembly: NavigationLink(int.MaxValue, "ENS/Cambios", typeof(MyPages.CambiosController), icon: "fa-reorder")]
[assembly: NavigationLink(int.MaxValue, "ENS/Servicios", typeof(MyPages.ServiciosController), icon: "fa-desktop")]
//[assembly: NavigationLink(int.MaxValue, "ENS/Severidades", typeof(MyPages.SeveridadesController), icon: "fa-ban")]
[assembly: NavigationLink(int.MaxValue, "ENS/Incidencias", typeof(MyPages.IncidenciasController), icon: "fa-bell")]
[assembly: NavigationLink(int.MaxValue, "ENS/Contraseñas", typeof(MyPages.PasswordsController), icon: "fa-lock")]
//[assembly: NavigationLink(int.MaxValue, "ENS/Tiposservicio", typeof(MyPages.TiposservicioController), icon: null)]