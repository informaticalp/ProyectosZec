﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido
    export class IncidenciasGrid extends Serenity.EntityGrid<IncidenciasRow, any> {
        protected getColumnsKey() { return 'ENS.Incidencias'; }
        protected getDialogType() { return IncidenciasDialog; }
        protected getIdProperty() { return IncidenciasRow.idProperty; }
        protected getInsertPermission() { return IncidenciasRow.insertPermission; }
        protected getLocalTextPrefix() { return IncidenciasRow.localTextPrefix; }
        protected getService() { return IncidenciasService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'ENS/Incidencias/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            return buttons;
            // Fin añadidos
        }
        /**
* This method is called for all rows
* @param item Data item for current row
* @param index Index of the row in grid
*/
        protected getItemCssClass(item: ENS.IncidenciasRow, index: number): string {
            let klass: string = "";

            if (item.Cierre == null)
                klass += " rechazada";

            return Q.trimToNull(klass);
        }

    }
}