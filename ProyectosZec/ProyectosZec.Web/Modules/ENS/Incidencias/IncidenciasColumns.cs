﻿
namespace ProyectosZec.ENS.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("ENS.Incidencias")]
    [BasedOnRow(typeof(Entities.IncidenciasRow), CheckNames = true)]
    public class IncidenciasColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Width(70)]
        public Int32 IncidenciaId { get; set; }
        [EditLink,Width(100),QuickFilter]
        public String Servicio { get; set; }
        [Width(100),QuickFilter]
        public String Severidad { get; set; }
        [DisplayFormat("g"),Width(130),QuickFilter]
        public DateTime Apertura { get; set; }
        [DisplayFormat("g"), Width(130)]
        public DateTime Cierre { get; set; }
        [Width(350)]
        public String Observaciones { get; set; }
        [Width(350)]
        public String ObservacionesCierre { get; set; }
    }
}