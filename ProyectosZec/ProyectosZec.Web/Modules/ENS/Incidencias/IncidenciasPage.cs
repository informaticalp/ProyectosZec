﻿
namespace ProyectosZec.ENS.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("ENS/Incidencias"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.IncidenciasRow))]
    public class IncidenciasController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/ENS/Incidencias/IncidenciasIndex.cshtml");
        }
    }
}