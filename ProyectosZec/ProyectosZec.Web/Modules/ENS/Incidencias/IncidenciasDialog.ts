﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    export class IncidenciasDialog extends Serenity.EntityDialog<IncidenciasRow, any> {
        protected getFormKey() { return IncidenciasForm.formKey; }
        protected getIdProperty() { return IncidenciasRow.idProperty; }
        protected getLocalTextPrefix() { return IncidenciasRow.localTextPrefix; }
        protected getNameProperty() { return IncidenciasRow.nameProperty; }
        protected getService() { return IncidenciasService.baseUrl; }
        protected getDeletePermission() { return IncidenciasRow.deletePermission; }
        protected getInsertPermission() { return IncidenciasRow.insertPermission; }
        protected getUpdatePermission() { return IncidenciasRow.updatePermission; }

        protected form = new IncidenciasForm(this.idPrefix);

    }
}