﻿
namespace ProyectosZec.ENS.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("ENS.Incidencias")]
    [BasedOnRow(typeof(Entities.IncidenciasRow), CheckNames = true)]
    public class IncidenciasForm
    {
        public Int32 ServicioId { get; set; }
        public Int32 SeveridadId { get; set; }
        [DisplayFormat("g")]
        public DateTime Apertura { get; set; }
        [DisplayFormat("g")]
        public DateTime Cierre { get; set; }
        [TextAreaEditor(Rows = 5)]
        public String Observaciones { get; set; }
        [TextAreaEditor(Rows = 5)]
        public String ObservacionesCierre { get; set; }
    }
}