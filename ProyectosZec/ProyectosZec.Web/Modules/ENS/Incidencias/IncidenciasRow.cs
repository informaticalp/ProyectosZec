﻿
namespace ProyectosZec.ENS.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("ENS"), TableName("incidencias")]
    [DisplayName("Incidencias"), InstanceName("Incidencias")]
    [ReadPermission("Ens:General")]
    [ModifyPermission("Ens:General")]
    public sealed class IncidenciasRow : Row, IIdRow, INameRow
    {
        [DisplayName("Incidencia Id"), Identity]
        public Int32? IncidenciaId
        {
            get { return Fields.IncidenciaId[this]; }
            set { Fields.IncidenciaId[this] = value; }
        }

        [DisplayName("Servicio"), NotNull, ForeignKey("servicios", "ServicioId"), LeftJoin("jServicio"), TextualField("Servicio")]
        [LookupEditor(typeof(Entities.ServiciosRow))]
        public Int32? ServicioId
        {
            get { return Fields.ServicioId[this]; }
            set { Fields.ServicioId[this] = value; }
        }

        [DisplayName("Severidad"), NotNull, ForeignKey("severidades", "SeveridadId"), LeftJoin("jSeveridad"), TextualField("SeveridadServicio")]
        [LookupEditor(typeof(Entities.SeveridadesRow))]
        public Int32? SeveridadId
        {
            get { return Fields.SeveridadId[this]; }
            set { Fields.SeveridadId[this] = value; }
        }

        [DisplayName("Apertura"), NotNull]
        [DateTimeKind(DateTimeKind.Local), DateTimeEditor]
        public DateTime? Apertura
        {
            get { return Fields.Apertura[this]; }
            set { Fields.Apertura[this] = value; }
        }

        [DisplayName("Cierre")]
        [DateTimeKind(DateTimeKind.Local), DateTimeEditor]
        public DateTime? Cierre
        {
            get { return Fields.Cierre[this]; }
            set { Fields.Cierre[this] = value; }
        }

        [DisplayName("Observaciones Apertura"), Size(1500), NotNull, QuickSearch]
        public String Observaciones
        {
            get { return Fields.Observaciones[this]; }
            set { Fields.Observaciones[this] = value; }
        }
        [DisplayName("Observaciones Cierre"), Size(1500), QuickSearch]
        public String ObservacionesCierre
        {
            get { return Fields.ObservacionesCierre[this]; }
            set { Fields.ObservacionesCierre[this] = value; }
        }

        [DisplayName("Servicio"), Expression("jServicio.[Servicio]")]
        public String Servicio
        {
            get { return Fields.Servicio[this]; }
            set { Fields.Servicio[this] = value; }
        }

        [DisplayName("Severidad"), Expression("jSeveridad.[Severidad]")]
        public String Severidad
        {
            get { return Fields.Severidad[this]; }
            set { Fields.Severidad[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.IncidenciaId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Observaciones; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public IncidenciasRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field IncidenciaId;
            public Int32Field ServicioId;
            public Int32Field SeveridadId;
            public DateTimeField Apertura;
            public DateTimeField Cierre;
            public StringField Observaciones;
            public StringField ObservacionesCierre;

            public StringField Servicio;

            public StringField Severidad;
        }
    }
}
