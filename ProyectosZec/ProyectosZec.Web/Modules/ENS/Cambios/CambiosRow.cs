﻿
namespace ProyectosZec.ENS.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("ENS"), TableName("cambios")]
    [DisplayName("Cambios"), InstanceName("Cambios")]
    [ReadPermission("Ens:General")]
    [ModifyPermission("Ens:General")]
    public sealed class CambiosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Combio Id"), Identity]
        public Int32? CombioId
        {
            get { return Fields.CombioId[this]; }
            set { Fields.CombioId[this] = value; }
        }

        [DisplayName("Servicio"), NotNull, ForeignKey("servicios", "ServicioId"), LeftJoin("jServicio"), TextualField("Servicio")]
        [LookupEditor(typeof(Entities.ServiciosRow), InplaceAdd = true)]
        public Int32? ServicioId
        {
            get { return Fields.ServicioId[this]; }
            set { Fields.ServicioId[this] = value; }
        }

        [DisplayName("Observaciones"), Size(1500), NotNull, QuickSearch]
        public String Observaciones
        {
            get { return Fields.Observaciones[this]; }
            set { Fields.Observaciones[this] = value; }
        }

        [DisplayName("Fecha"), NotNull]
        public DateTime? Fecha
        {
            get { return Fields.Fecha[this]; }
            set { Fields.Fecha[this] = value; }
        }

        [DisplayName("Servicio"), Expression("jServicio.[Servicio]")]
        public String Servicio
        {
            get { return Fields.Servicio[this]; }
            set { Fields.Servicio[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.CombioId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Observaciones; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public CambiosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field CombioId;
            public Int32Field ServicioId;
            public StringField Observaciones;
            public DateTimeField Fecha;

            public StringField Servicio;
        }
    }
}
