﻿
namespace ProyectosZec.ENS.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("ENS.Cambios")]
    [BasedOnRow(typeof(Entities.CambiosRow), CheckNames = true)]
    public class CambiosColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight,Width(70)]
        public Int32 CombioId { get; set; }
        [Width(130),QuickFilter]
        public String Servicio { get; set; }
        [Width(650)]
        public String Observaciones { get; set; }
        [Width(110),QuickFilter, DisplayFormat("d")]
        public DateTime Fecha { get; set; }
    }
}