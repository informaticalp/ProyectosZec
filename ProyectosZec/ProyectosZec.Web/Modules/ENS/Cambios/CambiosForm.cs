﻿
namespace ProyectosZec.ENS.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("ENS.Cambios")]
    [BasedOnRow(typeof(Entities.CambiosRow), CheckNames = true)]
    public class CambiosForm
    {
        public Int32 ServicioId { get; set; }
        [TextAreaEditor(Rows = 8)]
        public String Observaciones { get; set; }
        public DateTime Fecha { get; set; }
    }
}