﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido
    export class CambiosGrid extends Serenity.EntityGrid<CambiosRow, any> {
        protected getColumnsKey() { return 'ENS.Cambios'; }
        protected getDialogType() { return CambiosDialog; }
        protected getIdProperty() { return CambiosRow.idProperty; }
        protected getInsertPermission() { return CambiosRow.insertPermission; }
        protected getLocalTextPrefix() { return CambiosRow.localTextPrefix; }
        protected getService() { return CambiosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        // Añadidos
        // Primero campo de ordenación por defecto
        // No olvidarse Cambiar el Row y el campo
        protected getDefaultSortBy() {
            return [CambiosRow.Fields.Fecha];
        }

        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'ENS/Cambios/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            return buttons;
            // Fin añadidos
        }
    }
}