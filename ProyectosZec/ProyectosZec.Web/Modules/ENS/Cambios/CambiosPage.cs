﻿
namespace ProyectosZec.ENS.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("ENS/Cambios"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.CambiosRow))]
    public class CambiosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/ENS/Cambios/CambiosIndex.cshtml");
        }
    }
}