﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    export class CambiosDialog extends Serenity.EntityDialog<CambiosRow, any> {
        protected getFormKey() { return CambiosForm.formKey; }
        protected getIdProperty() { return CambiosRow.idProperty; }
        protected getLocalTextPrefix() { return CambiosRow.localTextPrefix; }
        protected getNameProperty() { return CambiosRow.nameProperty; }
        protected getService() { return CambiosService.baseUrl; }
        protected getDeletePermission() { return CambiosRow.deletePermission; }
        protected getInsertPermission() { return CambiosRow.insertPermission; }
        protected getUpdatePermission() { return CambiosRow.updatePermission; }

        protected form = new CambiosForm(this.idPrefix);

    }
}