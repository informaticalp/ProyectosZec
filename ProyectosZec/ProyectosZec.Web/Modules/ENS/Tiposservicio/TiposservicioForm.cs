﻿
namespace ProyectosZec.ENS.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("ENS.Tiposservicio")]
    [BasedOnRow(typeof(Entities.TiposservicioRow), CheckNames = true)]
    public class TiposservicioForm
    {
        public String TipoServicio { get; set; }
    }
}