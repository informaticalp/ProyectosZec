﻿
namespace ProyectosZec.ENS.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("ENS"), TableName("tiposservicio")]
    [DisplayName("Tiposservicio"), InstanceName("Tiposservicio")]
    [ReadPermission("Ens:General")]
    [ModifyPermission("Ens:General")]
    [LookupScript("Ens.Tiposservicio")]
    public sealed class TiposservicioRow : Row, IIdRow, INameRow
    {
        [DisplayName("Tipo Id"), Identity]
        public Int32? TipoId
        {
            get { return Fields.TipoId[this]; }
            set { Fields.TipoId[this] = value; }
        }

        [DisplayName("Tipo Servicio"), Size(255), NotNull, QuickSearch]
        public String TipoServicio
        {
            get { return Fields.TipoServicio[this]; }
            set { Fields.TipoServicio[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.TipoId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.TipoServicio; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public TiposservicioRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field TipoId;
            public StringField TipoServicio;
        }
    }
}
