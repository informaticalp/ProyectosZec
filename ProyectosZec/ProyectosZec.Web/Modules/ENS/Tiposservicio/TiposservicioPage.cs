﻿
namespace ProyectosZec.ENS.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("ENS/Tiposservicio"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.TiposservicioRow))]
    public class TiposservicioController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/ENS/Tiposservicio/TiposservicioIndex.cshtml");
        }
    }
}