﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    export class TiposservicioDialog extends Serenity.EntityDialog<TiposservicioRow, any> {
        protected getFormKey() { return TiposservicioForm.formKey; }
        protected getIdProperty() { return TiposservicioRow.idProperty; }
        protected getLocalTextPrefix() { return TiposservicioRow.localTextPrefix; }
        protected getNameProperty() { return TiposservicioRow.nameProperty; }
        protected getService() { return TiposservicioService.baseUrl; }
        protected getDeletePermission() { return TiposservicioRow.deletePermission; }
        protected getInsertPermission() { return TiposservicioRow.insertPermission; }
        protected getUpdatePermission() { return TiposservicioRow.updatePermission; }

        protected form = new TiposservicioForm(this.idPrefix);

    }
}