﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    export class TiposservicioGrid extends Serenity.EntityGrid<TiposservicioRow, any> {
        protected getColumnsKey() { return 'ENS.Tiposservicio'; }
        protected getDialogType() { return TiposservicioDialog; }
        protected getIdProperty() { return TiposservicioRow.idProperty; }
        protected getInsertPermission() { return TiposservicioRow.insertPermission; }
        protected getLocalTextPrefix() { return TiposservicioRow.localTextPrefix; }
        protected getService() { return TiposservicioService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}