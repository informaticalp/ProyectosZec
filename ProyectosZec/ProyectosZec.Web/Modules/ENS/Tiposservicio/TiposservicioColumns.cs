﻿
namespace ProyectosZec.ENS.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("ENS.Tiposservicio")]
    [BasedOnRow(typeof(Entities.TiposservicioRow), CheckNames = true)]
    public class TiposservicioColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 TipoId { get; set; }
        [EditLink]
        public String TipoServicio { get; set; }
    }
}