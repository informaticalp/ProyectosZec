﻿
namespace ProyectosZec.ENS.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("ENS.Passwords")]
    [BasedOnRow(typeof(Entities.PasswordsRow), CheckNames = true)]
    public class PasswordsColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 PasswordId { get; set; }
        [EditLink,Width(200)]
        public String Aplicacion { get; set; }
        [Width(100)]
        public String Acceso { get; set; }
        [Width(150)]
        public String Usuario { get; set; }
        [Width(100)]
        public String Password { get; set; }
        [Width(400)]
        public String Url { get; set; }
        [Width(120),QuickFilter]
        public String TipoTipoServicio { get; set; }
        [Width(350)]
        public String Observaciones { get; set; }
    }
}