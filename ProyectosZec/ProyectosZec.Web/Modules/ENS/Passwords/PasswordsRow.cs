﻿
namespace ProyectosZec.ENS.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("ENS"), TableName("passwords")]
    [DisplayName("Passwords"), InstanceName("Passwords")]
    [ReadPermission("Ens:General")]
    [ModifyPermission("Ens:General")]
    public sealed class PasswordsRow : Row, IIdRow, INameRow
    {
        [DisplayName("Password Id"), Identity]
        public Int32? PasswordId
        {
            get { return Fields.PasswordId[this]; }
            set { Fields.PasswordId[this] = value; }
        }

        [DisplayName("Aplicacion"), Size(100), NotNull, QuickSearch]
        public String Aplicacion
        {
            get { return Fields.Aplicacion[this]; }
            set { Fields.Aplicacion[this] = value; }
        }

        [DisplayName("Acceso"), Size(80),QuickSearch]
        public String Acceso
        {
            get { return Fields.Acceso[this]; }
            set { Fields.Acceso[this] = value; }
        }

        [DisplayName("Usuario"), Size(100)]
        public String Usuario
        {
            get { return Fields.Usuario[this]; }
            set { Fields.Usuario[this] = value; }
        }

        [DisplayName("Contraseña"), Size(30)]
        public String Password
        {
            get { return Fields.Password[this]; }
            set { Fields.Password[this] = value; }
        }

        [DisplayName("Url"), Size(255)]
        public String Url
        {
            get { return Fields.Url[this]; }
            set { Fields.Url[this] = value; }
        }

        [DisplayName("Tipo"), ForeignKey("tiposservicio", "TipoId"), LeftJoin("jTipo"), TextualField("TipoTipoServicio")]
        [LookupEditor(typeof(Entities.TiposservicioRow))]
        public Int32? TipoId
        {
            get { return Fields.TipoId[this]; }
            set { Fields.TipoId[this] = value; }
        }

        [DisplayName("Tipo Servicio"), Expression("jTipo.[TipoServicio]")]
        public String TipoTipoServicio
        {
            get { return Fields.TipoTipoServicio[this]; }
            set { Fields.TipoTipoServicio[this] = value; }
        }
        [DisplayName("Observaciones"), Size(1500), QuickSearch]
        public String Observaciones
        {
            get { return Fields.Observaciones[this]; }
            set { Fields.Observaciones[this] = value; }
        }
        IIdField IIdRow.IdField
        {
            get { return Fields.PasswordId; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Aplicacion; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public PasswordsRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field PasswordId;
            public StringField Aplicacion;
            public StringField Acceso;
            public StringField Usuario;
            public StringField Password;
            public StringField Url;
            public Int32Field TipoId;

            public StringField TipoTipoServicio;
            public StringField Observaciones;
        }
    }
}
