﻿
namespace ProyectosZec.ENS.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("ENS/Passwords"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.PasswordsRow))]
    public class PasswordsController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/ENS/Passwords/PasswordsIndex.cshtml");
        }
    }
}