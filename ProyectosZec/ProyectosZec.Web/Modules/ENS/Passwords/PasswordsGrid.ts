﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    // Añadido para los filtros multiples
    @Serenity.Decorators.filterable()
    // Fin Añadido
    export class PasswordsGrid extends Serenity.EntityGrid<PasswordsRow, any> {
        protected getColumnsKey() { return 'ENS.Passwords'; }
        protected getDialogType() { return PasswordsDialog; }
        protected getIdProperty() { return PasswordsRow.idProperty; }
        protected getInsertPermission() { return PasswordsRow.insertPermission; }
        protected getLocalTextPrefix() { return PasswordsRow.localTextPrefix; }
        protected getService() { return PasswordsService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
        // Botones Excel y Pdf
        getButtons() {
            var buttons = super.getButtons();

            buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit(),
                service: 'ENS/Passwords/ListExcel',
                separator: true
            }));

            buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                grid: this,
                onViewSubmit: () => this.onViewSubmit()
            }));

            return buttons;
            // Fin añadidos
        }
    }
}