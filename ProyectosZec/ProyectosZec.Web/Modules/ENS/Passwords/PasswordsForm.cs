﻿
namespace ProyectosZec.ENS.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("ENS.Passwords")]
    [BasedOnRow(typeof(Entities.PasswordsRow), CheckNames = true)]
    public class PasswordsForm
    {
        public String Aplicacion { get; set; }
        public String Acceso { get; set; }
        public String Usuario { get; set; }
        public String Password { get; set; }
        public String Url { get; set; }
        public Int32 TipoId { get; set; }
        [TextAreaEditor(Rows = 3)]
        public String Observaciones { get; set; }
    }
}