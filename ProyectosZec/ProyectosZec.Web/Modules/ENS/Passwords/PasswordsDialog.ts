﻿
namespace ProyectosZec.ENS {

    @Serenity.Decorators.registerClass()
    export class PasswordsDialog extends Serenity.EntityDialog<PasswordsRow, any> {
        protected getFormKey() { return PasswordsForm.formKey; }
        protected getIdProperty() { return PasswordsRow.idProperty; }
        protected getLocalTextPrefix() { return PasswordsRow.localTextPrefix; }
        protected getNameProperty() { return PasswordsRow.nameProperty; }
        protected getService() { return PasswordsService.baseUrl; }
        protected getDeletePermission() { return PasswordsRow.deletePermission; }
        protected getInsertPermission() { return PasswordsRow.insertPermission; }
        protected getUpdatePermission() { return PasswordsRow.updatePermission; }

        protected form = new PasswordsForm(this.idPrefix);

    }
}