﻿var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var LanguageForm = /** @class */ (function (_super) {
            __extends(LanguageForm, _super);
            function LanguageForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!LanguageForm.init) {
                    LanguageForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(LanguageForm, [
                        'LanguageId', w0,
                        'LanguageName', w0
                    ]);
                }
                return _this;
            }
            LanguageForm.formKey = 'Administration.Language';
            return LanguageForm;
        }(Serenity.PrefixedContext));
        Administration.LanguageForm = LanguageForm;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var LanguageRow;
        (function (LanguageRow) {
            LanguageRow.idProperty = 'Id';
            LanguageRow.nameProperty = 'LanguageName';
            LanguageRow.localTextPrefix = 'Administration.Language';
            LanguageRow.lookupKey = 'Administration.Language';
            function getLookup() {
                return Q.getLookup('Administration.Language');
            }
            LanguageRow.getLookup = getLookup;
            LanguageRow.deletePermission = 'Administration:Translation';
            LanguageRow.insertPermission = 'Administration:Translation';
            LanguageRow.readPermission = 'Administration:Translation';
            LanguageRow.updatePermission = 'Administration:Translation';
        })(LanguageRow = Administration.LanguageRow || (Administration.LanguageRow = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var LanguageService;
        (function (LanguageService) {
            LanguageService.baseUrl = 'Administration/Language';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                LanguageService[x] = function (r, s, o) {
                    return Q.serviceRequest(LanguageService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(LanguageService = Administration.LanguageService || (Administration.LanguageService = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var RoleForm = /** @class */ (function (_super) {
            __extends(RoleForm, _super);
            function RoleForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!RoleForm.init) {
                    RoleForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(RoleForm, [
                        'RoleName', w0
                    ]);
                }
                return _this;
            }
            RoleForm.formKey = 'Administration.Role';
            return RoleForm;
        }(Serenity.PrefixedContext));
        Administration.RoleForm = RoleForm;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var RolePermissionRow;
        (function (RolePermissionRow) {
            RolePermissionRow.idProperty = 'RolePermissionId';
            RolePermissionRow.nameProperty = 'PermissionKey';
            RolePermissionRow.localTextPrefix = 'Administration.RolePermission';
            RolePermissionRow.deletePermission = 'Administration:Security';
            RolePermissionRow.insertPermission = 'Administration:Security';
            RolePermissionRow.readPermission = 'Administration:Security';
            RolePermissionRow.updatePermission = 'Administration:Security';
        })(RolePermissionRow = Administration.RolePermissionRow || (Administration.RolePermissionRow = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var RolePermissionService;
        (function (RolePermissionService) {
            RolePermissionService.baseUrl = 'Administration/RolePermission';
            [
                'Update',
                'List'
            ].forEach(function (x) {
                RolePermissionService[x] = function (r, s, o) {
                    return Q.serviceRequest(RolePermissionService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(RolePermissionService = Administration.RolePermissionService || (Administration.RolePermissionService = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var RoleRow;
        (function (RoleRow) {
            RoleRow.idProperty = 'RoleId';
            RoleRow.nameProperty = 'RoleName';
            RoleRow.localTextPrefix = 'Administration.Role';
            RoleRow.lookupKey = 'Administration.Role';
            function getLookup() {
                return Q.getLookup('Administration.Role');
            }
            RoleRow.getLookup = getLookup;
            RoleRow.deletePermission = 'Administration:Security';
            RoleRow.insertPermission = 'Administration:Security';
            RoleRow.readPermission = 'Administration:Security';
            RoleRow.updatePermission = 'Administration:Security';
        })(RoleRow = Administration.RoleRow || (Administration.RoleRow = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var RoleService;
        (function (RoleService) {
            RoleService.baseUrl = 'Administration/Role';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                RoleService[x] = function (r, s, o) {
                    return Q.serviceRequest(RoleService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(RoleService = Administration.RoleService || (Administration.RoleService = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var TranslationService;
        (function (TranslationService) {
            TranslationService.baseUrl = 'Administration/Translation';
            [
                'List',
                'Update'
            ].forEach(function (x) {
                TranslationService[x] = function (r, s, o) {
                    return Q.serviceRequest(TranslationService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TranslationService = Administration.TranslationService || (Administration.TranslationService = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserForm = /** @class */ (function (_super) {
            __extends(UserForm, _super);
            function UserForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!UserForm.init) {
                    UserForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.EmailEditor;
                    var w2 = s.ImageUploadEditor;
                    var w3 = s.PasswordEditor;
                    Q.initFormType(UserForm, [
                        'Username', w0,
                        'DisplayName', w0,
                        'Email', w1,
                        'UserImage', w2,
                        'Password', w3,
                        'PasswordConfirm', w3,
                        'Source', w0
                    ]);
                }
                return _this;
            }
            UserForm.formKey = 'Administration.User';
            return UserForm;
        }(Serenity.PrefixedContext));
        Administration.UserForm = UserForm;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserPermissionRow;
        (function (UserPermissionRow) {
            UserPermissionRow.idProperty = 'UserPermissionId';
            UserPermissionRow.nameProperty = 'PermissionKey';
            UserPermissionRow.localTextPrefix = 'Administration.UserPermission';
            UserPermissionRow.deletePermission = 'Administration:Security';
            UserPermissionRow.insertPermission = 'Administration:Security';
            UserPermissionRow.readPermission = 'Administration:Security';
            UserPermissionRow.updatePermission = 'Administration:Security';
        })(UserPermissionRow = Administration.UserPermissionRow || (Administration.UserPermissionRow = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserPermissionService;
        (function (UserPermissionService) {
            UserPermissionService.baseUrl = 'Administration/UserPermission';
            [
                'Update',
                'List',
                'ListRolePermissions',
                'ListPermissionKeys'
            ].forEach(function (x) {
                UserPermissionService[x] = function (r, s, o) {
                    return Q.serviceRequest(UserPermissionService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(UserPermissionService = Administration.UserPermissionService || (Administration.UserPermissionService = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserRoleRow;
        (function (UserRoleRow) {
            UserRoleRow.idProperty = 'UserRoleId';
            UserRoleRow.localTextPrefix = 'Administration.UserRole';
            UserRoleRow.deletePermission = 'Administration:Security';
            UserRoleRow.insertPermission = 'Administration:Security';
            UserRoleRow.readPermission = 'Administration:Security';
            UserRoleRow.updatePermission = 'Administration:Security';
        })(UserRoleRow = Administration.UserRoleRow || (Administration.UserRoleRow = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserRoleService;
        (function (UserRoleService) {
            UserRoleService.baseUrl = 'Administration/UserRole';
            [
                'Update',
                'List'
            ].forEach(function (x) {
                UserRoleService[x] = function (r, s, o) {
                    return Q.serviceRequest(UserRoleService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(UserRoleService = Administration.UserRoleService || (Administration.UserRoleService = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserRow;
        (function (UserRow) {
            UserRow.idProperty = 'UserId';
            UserRow.isActiveProperty = 'IsActive';
            UserRow.nameProperty = 'Username';
            UserRow.localTextPrefix = 'Administration.User';
            UserRow.lookupKey = 'Administration.User';
            function getLookup() {
                return Q.getLookup('Administration.User');
            }
            UserRow.getLookup = getLookup;
            UserRow.deletePermission = 'Administration:Security';
            UserRow.insertPermission = 'Administration:Security';
            UserRow.readPermission = 'Administration:Security';
            UserRow.updatePermission = 'Administration:Security';
        })(UserRow = Administration.UserRow || (Administration.UserRow = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserService;
        (function (UserService) {
            UserService.baseUrl = 'Administration/User';
            [
                'Create',
                'Update',
                'Delete',
                'Undelete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                UserService[x] = function (r, s, o) {
                    return Q.serviceRequest(UserService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(UserService = Administration.UserService || (Administration.UserService = {}));
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var UserPreferenceRow;
        (function (UserPreferenceRow) {
            UserPreferenceRow.idProperty = 'UserPreferenceId';
            UserPreferenceRow.nameProperty = 'Name';
            UserPreferenceRow.localTextPrefix = 'Common.UserPreference';
            UserPreferenceRow.deletePermission = '';
            UserPreferenceRow.insertPermission = '';
            UserPreferenceRow.readPermission = '';
            UserPreferenceRow.updatePermission = '';
        })(UserPreferenceRow = Common.UserPreferenceRow || (Common.UserPreferenceRow = {}));
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var UserPreferenceService;
        (function (UserPreferenceService) {
            UserPreferenceService.baseUrl = 'Common/UserPreference';
            [
                'Update',
                'Retrieve'
            ].forEach(function (x) {
                UserPreferenceService[x] = function (r, s, o) {
                    return Q.serviceRequest(UserPreferenceService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(UserPreferenceService = Common.UserPreferenceService || (Common.UserPreferenceService = {}));
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var CapitalForm = /** @class */ (function (_super) {
            __extends(CapitalForm, _super);
            function CapitalForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!CapitalForm.init) {
                    CapitalForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(CapitalForm, [
                        'Capital', w0
                    ]);
                }
                return _this;
            }
            CapitalForm.formKey = 'CuadroMandos.Capital';
            return CapitalForm;
        }(Serenity.PrefixedContext));
        CuadroMandos.CapitalForm = CapitalForm;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var CapitalRow;
        (function (CapitalRow) {
            CapitalRow.idProperty = 'CapitalId';
            CapitalRow.nameProperty = 'Capital';
            CapitalRow.localTextPrefix = 'CuadroMandos.Capital';
            CapitalRow.lookupKey = 'CuadroMandos.Capital';
            function getLookup() {
                return Q.getLookup('CuadroMandos.Capital');
            }
            CapitalRow.getLookup = getLookup;
            CapitalRow.deletePermission = 'Administration:General';
            CapitalRow.insertPermission = 'Administration:General';
            CapitalRow.readPermission = 'Administration:General';
            CapitalRow.updatePermission = 'Administration:General';
        })(CapitalRow = CuadroMandos.CapitalRow || (CuadroMandos.CapitalRow = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var CapitalService;
        (function (CapitalService) {
            CapitalService.baseUrl = 'CuadroMandos/Capital';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                CapitalService[x] = function (r, s, o) {
                    return Q.serviceRequest(CapitalService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(CapitalService = CuadroMandos.CapitalService || (CuadroMandos.CapitalService = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var EstadosForm = /** @class */ (function (_super) {
            __extends(EstadosForm, _super);
            function EstadosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EstadosForm.init) {
                    EstadosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(EstadosForm, [
                        'Estado', w0
                    ]);
                }
                return _this;
            }
            EstadosForm.formKey = 'CuadroMandos.Estados';
            return EstadosForm;
        }(Serenity.PrefixedContext));
        CuadroMandos.EstadosForm = EstadosForm;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var EstadosRow;
        (function (EstadosRow) {
            EstadosRow.idProperty = 'EstadoId';
            EstadosRow.nameProperty = 'Estado';
            EstadosRow.localTextPrefix = 'CuadroMandos.Estados';
            EstadosRow.lookupKey = 'CuadroMandos.Estados';
            function getLookup() {
                return Q.getLookup('CuadroMandos.Estados');
            }
            EstadosRow.getLookup = getLookup;
            EstadosRow.deletePermission = 'Administration:General';
            EstadosRow.insertPermission = 'Administration:General';
            EstadosRow.readPermission = 'Administration:General';
            EstadosRow.updatePermission = 'Administration:General';
        })(EstadosRow = CuadroMandos.EstadosRow || (CuadroMandos.EstadosRow = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var EstadosService;
        (function (EstadosService) {
            EstadosService.baseUrl = 'CuadroMandos/Estados';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EstadosService[x] = function (r, s, o) {
                    return Q.serviceRequest(EstadosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EstadosService = CuadroMandos.EstadosService || (CuadroMandos.EstadosService = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var IslasForm = /** @class */ (function (_super) {
            __extends(IslasForm, _super);
            function IslasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!IslasForm.init) {
                    IslasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(IslasForm, [
                        'NombreIsla', w0,
                        'Isla', w0
                    ]);
                }
                return _this;
            }
            IslasForm.formKey = 'CuadroMandos.Islas';
            return IslasForm;
        }(Serenity.PrefixedContext));
        CuadroMandos.IslasForm = IslasForm;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var IslasRow;
        (function (IslasRow) {
            IslasRow.idProperty = 'IslaId';
            IslasRow.nameProperty = 'NombreIsla';
            IslasRow.localTextPrefix = 'CuadroMandos.Islas';
            IslasRow.lookupKey = 'CuadroMandos.Islas';
            function getLookup() {
                return Q.getLookup('CuadroMandos.Islas');
            }
            IslasRow.getLookup = getLookup;
            IslasRow.deletePermission = 'Administration:General';
            IslasRow.insertPermission = 'Administration:General';
            IslasRow.readPermission = 'Administration:General';
            IslasRow.updatePermission = 'Administration:General';
        })(IslasRow = CuadroMandos.IslasRow || (CuadroMandos.IslasRow = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var IslasService;
        (function (IslasService) {
            IslasService.baseUrl = 'CuadroMandos/Islas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                IslasService[x] = function (r, s, o) {
                    return Q.serviceRequest(IslasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(IslasService = CuadroMandos.IslasService || (CuadroMandos.IslasService = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var PrescriptorinversorForm = /** @class */ (function (_super) {
            __extends(PrescriptorinversorForm, _super);
            function PrescriptorinversorForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!PrescriptorinversorForm.init) {
                    PrescriptorinversorForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(PrescriptorinversorForm, [
                        'PrescriptorInversor', w0
                    ]);
                }
                return _this;
            }
            PrescriptorinversorForm.formKey = 'CuadroMandos.Prescriptorinversor';
            return PrescriptorinversorForm;
        }(Serenity.PrefixedContext));
        CuadroMandos.PrescriptorinversorForm = PrescriptorinversorForm;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var PrescriptorinversorRow;
        (function (PrescriptorinversorRow) {
            PrescriptorinversorRow.idProperty = 'PrescriptorInversorId';
            PrescriptorinversorRow.nameProperty = 'PrescriptorInversor';
            PrescriptorinversorRow.localTextPrefix = 'CuadroMandos.Prescriptorinversor';
            PrescriptorinversorRow.lookupKey = 'CuadroMandos.Prescriptorinversor';
            function getLookup() {
                return Q.getLookup('CuadroMandos.Prescriptorinversor');
            }
            PrescriptorinversorRow.getLookup = getLookup;
            PrescriptorinversorRow.deletePermission = 'Administration:General';
            PrescriptorinversorRow.insertPermission = 'Administration:General';
            PrescriptorinversorRow.readPermission = 'Administration:General';
            PrescriptorinversorRow.updatePermission = 'Administration:General';
        })(PrescriptorinversorRow = CuadroMandos.PrescriptorinversorRow || (CuadroMandos.PrescriptorinversorRow = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var PrescriptorinversorService;
        (function (PrescriptorinversorService) {
            PrescriptorinversorService.baseUrl = 'CuadroMandos/Prescriptorinversor';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PrescriptorinversorService[x] = function (r, s, o) {
                    return Q.serviceRequest(PrescriptorinversorService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(PrescriptorinversorService = CuadroMandos.PrescriptorinversorService || (CuadroMandos.PrescriptorinversorService = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var PresentadasForm = /** @class */ (function (_super) {
            __extends(PresentadasForm, _super);
            function PresentadasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!PresentadasForm.init) {
                    PresentadasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.LookupEditor;
                    var w2 = s.DateEditor;
                    var w3 = s.IntegerEditor;
                    Q.initFormType(PresentadasForm, [
                        'Denominacion', w0,
                        'TecnicoId', w1,
                        'SubsectorId', w1,
                        'IslaId', w1,
                        'CapitalId', w1,
                        'Captacion', w0,
                        'PrescriptorInversorId', w1,
                        'Descripcion', w0,
                        'Contacto', w0,
                        'Telefono', w0,
                        'Email', w0,
                        'EstadoId', w1,
                        'FechaInicio', w2,
                        'FechaPresentacion', w2,
                        'FechaInscripcion', w2,
                        'FechaAutorizacion', w2,
                        'FechaAmpliacion', w2,
                        'FechaBaja', w2,
                        'Empleos', w3,
                        'Inversion', w3,
                        'EmpleoReal', w3,
                        'Expediente', w0,
                        'Nace', w0,
                        'InversionReal', w3
                    ]);
                }
                return _this;
            }
            PresentadasForm.formKey = 'CuadroMandos.Presentadas';
            return PresentadasForm;
        }(Serenity.PrefixedContext));
        CuadroMandos.PresentadasForm = PresentadasForm;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var PresentadasService;
        (function (PresentadasService) {
            PresentadasService.baseUrl = 'CuadroMandos/Presentadas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PresentadasService[x] = function (r, s, o) {
                    return Q.serviceRequest(PresentadasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(PresentadasService = CuadroMandos.PresentadasService || (CuadroMandos.PresentadasService = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var ProyectosForm = /** @class */ (function (_super) {
            __extends(ProyectosForm, _super);
            function ProyectosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ProyectosForm.init) {
                    ProyectosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.LookupEditor;
                    var w2 = s.IntegerEditor;
                    var w3 = s.TextAreaEditor;
                    var w4 = s.DateEditor;
                    Q.initFormType(ProyectosForm, [
                        'Denominacion', w0,
                        'TecnicoId', w1,
                        'SectorId', w1,
                        'SubsectorId', w1,
                        'IslaId', w1,
                        'CapitalId', w1,
                        'Captacion', w0,
                        'Empleos', w2,
                        'Inversion', w2,
                        'EmpleoReal', w2,
                        'InversionReal', w2,
                        'PrescriptorInversorId', w1,
                        'Expediente', w0,
                        'Nace', w0,
                        'EstadoId', w1,
                        'Descripcion', w3,
                        'Contacto', w0,
                        'Telefono', w0,
                        'Email', w0,
                        'FechaInicio', w4,
                        'FechaPresentacion', w4,
                        'FechaInscripcion', w4,
                        'FechaAutorizacion', w4,
                        'FechaAmpliacion', w4,
                        'FechaBaja', w4
                    ]);
                }
                return _this;
            }
            ProyectosForm.formKey = 'CuadroMandos.Proyectos';
            return ProyectosForm;
        }(Serenity.PrefixedContext));
        CuadroMandos.ProyectosForm = ProyectosForm;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var ProyectosRow;
        (function (ProyectosRow) {
            ProyectosRow.idProperty = 'ProyectoId';
            ProyectosRow.nameProperty = 'Denominacion';
            ProyectosRow.localTextPrefix = 'CuadroMandos.Proyectos';
            ProyectosRow.lookupKey = 'CuadroMandos.Proyectos';
            function getLookup() {
                return Q.getLookup('CuadroMandos.Proyectos');
            }
            ProyectosRow.getLookup = getLookup;
            ProyectosRow.deletePermission = 'CuadroMandos:Delete';
            ProyectosRow.insertPermission = 'CuadroMandos:Insert';
            ProyectosRow.readPermission = 'CuadroMandos:Read';
            ProyectosRow.updatePermission = 'CuadroMandos:Modify';
        })(ProyectosRow = CuadroMandos.ProyectosRow || (CuadroMandos.ProyectosRow = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var ProyectosService;
        (function (ProyectosService) {
            ProyectosService.baseUrl = 'CuadroMandos/Proyectos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ProyectosService[x] = function (r, s, o) {
                    return Q.serviceRequest(ProyectosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(ProyectosService = CuadroMandos.ProyectosService || (CuadroMandos.ProyectosService = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SectoresForm = /** @class */ (function (_super) {
            __extends(SectoresForm, _super);
            function SectoresForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!SectoresForm.init) {
                    SectoresForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(SectoresForm, [
                        'Sector', w0
                    ]);
                }
                return _this;
            }
            SectoresForm.formKey = 'CuadroMandos.Sectores';
            return SectoresForm;
        }(Serenity.PrefixedContext));
        CuadroMandos.SectoresForm = SectoresForm;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SectoresRow;
        (function (SectoresRow) {
            SectoresRow.idProperty = 'SectorId';
            SectoresRow.nameProperty = 'Sector';
            SectoresRow.localTextPrefix = 'CuadroMandos.Sectores';
            SectoresRow.lookupKey = 'CuadroMandos.Sectores';
            function getLookup() {
                return Q.getLookup('CuadroMandos.Sectores');
            }
            SectoresRow.getLookup = getLookup;
            SectoresRow.deletePermission = 'Administration:General';
            SectoresRow.insertPermission = 'Administration:General';
            SectoresRow.readPermission = 'Administration:General';
            SectoresRow.updatePermission = 'Administration:General';
        })(SectoresRow = CuadroMandos.SectoresRow || (CuadroMandos.SectoresRow = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SectoresService;
        (function (SectoresService) {
            SectoresService.baseUrl = 'CuadroMandos/Sectores';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SectoresService[x] = function (r, s, o) {
                    return Q.serviceRequest(SectoresService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(SectoresService = CuadroMandos.SectoresService || (CuadroMandos.SectoresService = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SubsectoresForm = /** @class */ (function (_super) {
            __extends(SubsectoresForm, _super);
            function SubsectoresForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!SubsectoresForm.init) {
                    SubsectoresForm.init = true;
                    var s = Serenity;
                    var w0 = s.IntegerEditor;
                    var w1 = s.StringEditor;
                    Q.initFormType(SubsectoresForm, [
                        'SectorId', w0,
                        'Subsector', w1
                    ]);
                }
                return _this;
            }
            SubsectoresForm.formKey = 'CuadroMandos.Subsectores';
            return SubsectoresForm;
        }(Serenity.PrefixedContext));
        CuadroMandos.SubsectoresForm = SubsectoresForm;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SubsectoresRow;
        (function (SubsectoresRow) {
            SubsectoresRow.idProperty = 'SubsectorId';
            SubsectoresRow.nameProperty = 'Subsector';
            SubsectoresRow.localTextPrefix = 'CuadroMandos.Subsectores';
            SubsectoresRow.lookupKey = 'CuadroMandos.Subsectores';
            function getLookup() {
                return Q.getLookup('CuadroMandos.Subsectores');
            }
            SubsectoresRow.getLookup = getLookup;
            SubsectoresRow.deletePermission = 'Administration:General';
            SubsectoresRow.insertPermission = 'Administration:General';
            SubsectoresRow.readPermission = 'Administration:General';
            SubsectoresRow.updatePermission = 'Administration:General';
        })(SubsectoresRow = CuadroMandos.SubsectoresRow || (CuadroMandos.SubsectoresRow = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SubsectoresService;
        (function (SubsectoresService) {
            SubsectoresService.baseUrl = 'CuadroMandos/Subsectores';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SubsectoresService[x] = function (r, s, o) {
                    return Q.serviceRequest(SubsectoresService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(SubsectoresService = CuadroMandos.SubsectoresService || (CuadroMandos.SubsectoresService = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var TecnicosForm = /** @class */ (function (_super) {
            __extends(TecnicosForm, _super);
            function TecnicosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!TecnicosForm.init) {
                    TecnicosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(TecnicosForm, [
                        'NombreTecnico', w0,
                        'Tecnico', w0
                    ]);
                }
                return _this;
            }
            TecnicosForm.formKey = 'CuadroMandos.Tecnicos';
            return TecnicosForm;
        }(Serenity.PrefixedContext));
        CuadroMandos.TecnicosForm = TecnicosForm;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var TecnicosRow;
        (function (TecnicosRow) {
            TecnicosRow.idProperty = 'TecnicoId';
            TecnicosRow.nameProperty = 'NombreTecnico';
            TecnicosRow.localTextPrefix = 'CuadroMandos.Tecnicos';
            TecnicosRow.lookupKey = 'CuadroMandos.Tecnicos';
            function getLookup() {
                return Q.getLookup('CuadroMandos.Tecnicos');
            }
            TecnicosRow.getLookup = getLookup;
            TecnicosRow.deletePermission = 'Administration:General';
            TecnicosRow.insertPermission = 'Administration:General';
            TecnicosRow.readPermission = 'Administration:General';
            TecnicosRow.updatePermission = 'Administration:General';
        })(TecnicosRow = CuadroMandos.TecnicosRow || (CuadroMandos.TecnicosRow = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var TecnicosService;
        (function (TecnicosService) {
            TecnicosService.baseUrl = 'CuadroMandos/Tecnicos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TecnicosService[x] = function (r, s, o) {
                    return Q.serviceRequest(TecnicosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TecnicosService = CuadroMandos.TecnicosService || (CuadroMandos.TecnicosService = {}));
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var CambiosForm = /** @class */ (function (_super) {
            __extends(CambiosForm, _super);
            function CambiosForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            CambiosForm.formKey = 'ENS.Cambios';
            return CambiosForm;
        }(Serenity.PrefixedContext));
        ENS.CambiosForm = CambiosForm;
        [, ['ServicioId', function () { return Serenity.IntegerEditor; }],
            ['Observaciones', function () { return Serenity.StringEditor; }],
            ['Fecha', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(CambiosForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var CambiosRow;
        (function (CambiosRow) {
            CambiosRow.idProperty = 'CombioId';
            CambiosRow.nameProperty = 'Observaciones';
            CambiosRow.localTextPrefix = 'ENS.Cambios';
            CambiosRow.deletePermission = 'Ens:General';
            CambiosRow.insertPermission = 'Ens:General';
            CambiosRow.readPermission = 'Ens:General';
            CambiosRow.updatePermission = 'Ens:General';
            var Fields;
            (function (Fields) {
            })(Fields = CambiosRow.Fields || (CambiosRow.Fields = {}));
            [
                'CombioId',
                'ServicioId',
                'Observaciones',
                'Fecha',
                'Servicio'
            ].forEach(function (x) { return Fields[x] = x; });
        })(CambiosRow = ENS.CambiosRow || (ENS.CambiosRow = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var CambiosService;
        (function (CambiosService) {
            CambiosService.baseUrl = 'ENS/Cambios';
            var Methods;
            (function (Methods) {
            })(Methods = CambiosService.Methods || (CambiosService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                CambiosService[x] = function (r, s, o) {
                    return Q.serviceRequest(CambiosService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = CambiosService.baseUrl + '/' + x;
            });
        })(CambiosService = ENS.CambiosService || (ENS.CambiosService = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var IncidenciasForm = /** @class */ (function (_super) {
            __extends(IncidenciasForm, _super);
            function IncidenciasForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            IncidenciasForm.formKey = 'ENS.Incidencias';
            return IncidenciasForm;
        }(Serenity.PrefixedContext));
        ENS.IncidenciasForm = IncidenciasForm;
        [, ['ServicioId', function () { return Serenity.IntegerEditor; }],
            ['SeveridadId', function () { return Serenity.IntegerEditor; }],
            ['Apertura', function () { return Serenity.DateEditor; }],
            ['Cierre', function () { return Serenity.DateEditor; }],
            ['Observaciones', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(IncidenciasForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var IncidenciasRow;
        (function (IncidenciasRow) {
            IncidenciasRow.idProperty = 'IncidenciaId';
            IncidenciasRow.nameProperty = 'Observaciones';
            IncidenciasRow.localTextPrefix = 'ENS.Incidencias';
            IncidenciasRow.deletePermission = 'Ens:General';
            IncidenciasRow.insertPermission = 'Ens:General';
            IncidenciasRow.readPermission = 'Ens:General';
            IncidenciasRow.updatePermission = 'Ens:General';
            var Fields;
            (function (Fields) {
            })(Fields = IncidenciasRow.Fields || (IncidenciasRow.Fields = {}));
            [
                'IncidenciaId',
                'ServicioId',
                'SeveridadId',
                'Apertura',
                'Cierre',
                'Observaciones',
                'Servicio',
                'SeveridadServicio'
            ].forEach(function (x) { return Fields[x] = x; });
        })(IncidenciasRow = ENS.IncidenciasRow || (ENS.IncidenciasRow = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var IncidenciasService;
        (function (IncidenciasService) {
            IncidenciasService.baseUrl = 'ENS/Incidencias';
            var Methods;
            (function (Methods) {
            })(Methods = IncidenciasService.Methods || (IncidenciasService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                IncidenciasService[x] = function (r, s, o) {
                    return Q.serviceRequest(IncidenciasService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = IncidenciasService.baseUrl + '/' + x;
            });
        })(IncidenciasService = ENS.IncidenciasService || (ENS.IncidenciasService = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var PasswordsForm = /** @class */ (function (_super) {
            __extends(PasswordsForm, _super);
            function PasswordsForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PasswordsForm.formKey = 'ENS.Passwords';
            return PasswordsForm;
        }(Serenity.PrefixedContext));
        ENS.PasswordsForm = PasswordsForm;
        [, ['Aplicacion', function () { return Serenity.StringEditor; }],
            ['Acceso', function () { return Serenity.StringEditor; }],
            ['Usuario', function () { return Serenity.StringEditor; }],
            ['Password', function () { return Serenity.StringEditor; }],
            ['Url', function () { return Serenity.StringEditor; }],
            ['TipoId', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(PasswordsForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var PasswordsRow;
        (function (PasswordsRow) {
            PasswordsRow.idProperty = 'PasswordId';
            PasswordsRow.nameProperty = 'Aplicacion';
            PasswordsRow.localTextPrefix = 'ENS.Passwords';
            PasswordsRow.deletePermission = 'Ens:General';
            PasswordsRow.insertPermission = 'Ens:General';
            PasswordsRow.readPermission = 'Ens:General';
            PasswordsRow.updatePermission = 'Ens:General';
            var Fields;
            (function (Fields) {
            })(Fields = PasswordsRow.Fields || (PasswordsRow.Fields = {}));
            [
                'PasswordId',
                'Aplicacion',
                'Acceso',
                'Usuario',
                'Password',
                'Url',
                'TipoId',
                'TipoTipoServicio',
                'Observaciones'
            ].forEach(function (x) { return Fields[x] = x; });
        })(PasswordsRow = ENS.PasswordsRow || (ENS.PasswordsRow = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var PasswordsService;
        (function (PasswordsService) {
            PasswordsService.baseUrl = 'ENS/Passwords';
            var Methods;
            (function (Methods) {
            })(Methods = PasswordsService.Methods || (PasswordsService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PasswordsService[x] = function (r, s, o) {
                    return Q.serviceRequest(PasswordsService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = PasswordsService.baseUrl + '/' + x;
            });
        })(PasswordsService = ENS.PasswordsService || (ENS.PasswordsService = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var ServiciosForm = /** @class */ (function (_super) {
            __extends(ServiciosForm, _super);
            function ServiciosForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ServiciosForm.formKey = 'ENS.Servicios';
            return ServiciosForm;
        }(Serenity.PrefixedContext));
        ENS.ServiciosForm = ServiciosForm;
        [, ['Servicio', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(ServiciosForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var ServiciosRow;
        (function (ServiciosRow) {
            ServiciosRow.idProperty = 'ServicioId';
            ServiciosRow.nameProperty = 'Servicio';
            ServiciosRow.localTextPrefix = 'ENS.Servicios';
            ServiciosRow.deletePermission = 'Ens:General';
            ServiciosRow.insertPermission = 'Ens:General';
            ServiciosRow.readPermission = 'Ens:General';
            ServiciosRow.updatePermission = 'Ens:General';
            var Fields;
            (function (Fields) {
            })(Fields = ServiciosRow.Fields || (ServiciosRow.Fields = {}));
            [
                'ServicioId',
                'Servicio'
            ].forEach(function (x) { return Fields[x] = x; });
        })(ServiciosRow = ENS.ServiciosRow || (ENS.ServiciosRow = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var ServiciosService;
        (function (ServiciosService) {
            ServiciosService.baseUrl = 'ENS/Servicios';
            var Methods;
            (function (Methods) {
            })(Methods = ServiciosService.Methods || (ServiciosService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ServiciosService[x] = function (r, s, o) {
                    return Q.serviceRequest(ServiciosService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = ServiciosService.baseUrl + '/' + x;
            });
        })(ServiciosService = ENS.ServiciosService || (ENS.ServiciosService = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var SeveridadesForm = /** @class */ (function (_super) {
            __extends(SeveridadesForm, _super);
            function SeveridadesForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            SeveridadesForm.formKey = 'ENS.Severidades';
            return SeveridadesForm;
        }(Serenity.PrefixedContext));
        ENS.SeveridadesForm = SeveridadesForm;
        [, ['Severidad', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(SeveridadesForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var SeveridadesRow;
        (function (SeveridadesRow) {
            SeveridadesRow.idProperty = 'SeveridadId';
            SeveridadesRow.nameProperty = 'Severidad';
            SeveridadesRow.localTextPrefix = 'ENS.Severidades';
            SeveridadesRow.deletePermission = 'Ens:General';
            SeveridadesRow.insertPermission = 'Ens:General';
            SeveridadesRow.readPermission = 'Ens:General';
            SeveridadesRow.updatePermission = 'Ens:General';
            var Fields;
            (function (Fields) {
            })(Fields = SeveridadesRow.Fields || (SeveridadesRow.Fields = {}));
            [
                'SeveridadId',
                'Severidad'
            ].forEach(function (x) { return Fields[x] = x; });
        })(SeveridadesRow = ENS.SeveridadesRow || (ENS.SeveridadesRow = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var SeveridadesService;
        (function (SeveridadesService) {
            SeveridadesService.baseUrl = 'ENS/Severidades';
            var Methods;
            (function (Methods) {
            })(Methods = SeveridadesService.Methods || (SeveridadesService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SeveridadesService[x] = function (r, s, o) {
                    return Q.serviceRequest(SeveridadesService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = SeveridadesService.baseUrl + '/' + x;
            });
        })(SeveridadesService = ENS.SeveridadesService || (ENS.SeveridadesService = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var TiposservicioForm = /** @class */ (function (_super) {
            __extends(TiposservicioForm, _super);
            function TiposservicioForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TiposservicioForm.formKey = 'ENS.Tiposservicio';
            return TiposservicioForm;
        }(Serenity.PrefixedContext));
        ENS.TiposservicioForm = TiposservicioForm;
        [, ['TipoServicio', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TiposservicioForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var TiposservicioRow;
        (function (TiposservicioRow) {
            TiposservicioRow.idProperty = 'TipoId';
            TiposservicioRow.nameProperty = 'TipoServicio';
            TiposservicioRow.localTextPrefix = 'ENS.Tiposservicio';
            TiposservicioRow.deletePermission = 'Ens:General';
            TiposservicioRow.insertPermission = 'Ens:General';
            TiposservicioRow.readPermission = 'Ens:General';
            TiposservicioRow.updatePermission = 'Ens:General';
            var Fields;
            (function (Fields) {
            })(Fields = TiposservicioRow.Fields || (TiposservicioRow.Fields = {}));
            [
                'TipoId',
                'TipoServicio'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TiposservicioRow = ENS.TiposservicioRow || (ENS.TiposservicioRow = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var TiposservicioService;
        (function (TiposservicioService) {
            TiposservicioService.baseUrl = 'ENS/Tiposservicio';
            var Methods;
            (function (Methods) {
            })(Methods = TiposservicioService.Methods || (TiposservicioService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposservicioService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposservicioService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TiposservicioService.baseUrl + '/' + x;
            });
        })(TiposservicioService = ENS.TiposservicioService || (ENS.TiposservicioService = {}));
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var InmovilizadosForm = /** @class */ (function (_super) {
            __extends(InmovilizadosForm, _super);
            function InmovilizadosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!InmovilizadosForm.init) {
                    InmovilizadosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.LookupEditor;
                    var w2 = s.DateEditor;
                    var w3 = s.DecimalEditor;
                    var w4 = s.IntegerEditor;
                    var w5 = s.MultipleImageUploadEditor;
                    Q.initFormType(InmovilizadosForm, [
                        'Codigo', w0,
                        'Descripcion', w0,
                        'Ubicacion', w0,
                        'NumeroSerie', w0,
                        'TipoInmovilizadoId', w1,
                        'SubTipoInmovilizadoId', w1,
                        'Pg', w0,
                        'SedeId', w1,
                        'ProveedorId', w1,
                        'FechaCompra', w2,
                        'FechaBaja', w2,
                        'Valor', w3,
                        'Amortizacion', w4,
                        'Garantia', w4,
                        'Factura', w0,
                        'GalleryImages', w5,
                        'Files', w5
                    ]);
                }
                return _this;
            }
            InmovilizadosForm.formKey = 'Inmovilizado.Inmovilizados';
            return InmovilizadosForm;
        }(Serenity.PrefixedContext));
        Inmovilizado.InmovilizadosForm = InmovilizadosForm;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var InmovilizadosRow;
        (function (InmovilizadosRow) {
            InmovilizadosRow.idProperty = 'InmovilizadoId';
            InmovilizadosRow.nameProperty = 'Descripcion';
            InmovilizadosRow.localTextPrefix = 'Inmovilizado.Inmovilizados';
            InmovilizadosRow.deletePermission = 'Inmovilizado:Modify';
            InmovilizadosRow.insertPermission = 'Inmovilizado:Modify';
            InmovilizadosRow.readPermission = 'Inmovilizado:Read';
            InmovilizadosRow.updatePermission = 'Inmovilizado:Modify';
        })(InmovilizadosRow = Inmovilizado.InmovilizadosRow || (Inmovilizado.InmovilizadosRow = {}));
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var InmovilizadosService;
        (function (InmovilizadosService) {
            InmovilizadosService.baseUrl = 'Inmovilizado/Inmovilizados';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                InmovilizadosService[x] = function (r, s, o) {
                    return Q.serviceRequest(InmovilizadosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(InmovilizadosService = Inmovilizado.InmovilizadosService || (Inmovilizado.InmovilizadosService = {}));
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var ProveedoresForm = /** @class */ (function (_super) {
            __extends(ProveedoresForm, _super);
            function ProveedoresForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ProveedoresForm.init) {
                    ProveedoresForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(ProveedoresForm, [
                        'Proveedor', w0,
                        'Contacto', w0,
                        'Telefono', w0,
                        'Email', w0
                    ]);
                }
                return _this;
            }
            ProveedoresForm.formKey = 'Inmovilizado.Proveedores';
            return ProveedoresForm;
        }(Serenity.PrefixedContext));
        Inmovilizado.ProveedoresForm = ProveedoresForm;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var ProveedoresRow;
        (function (ProveedoresRow) {
            ProveedoresRow.idProperty = 'ProveedorId';
            ProveedoresRow.nameProperty = 'Proveedor';
            ProveedoresRow.localTextPrefix = 'Inmovilizado.Proveedores';
            ProveedoresRow.lookupKey = 'Inmovilizado.Proveedores';
            function getLookup() {
                return Q.getLookup('Inmovilizado.Proveedores');
            }
            ProveedoresRow.getLookup = getLookup;
            ProveedoresRow.deletePermission = 'Inmovilizado:Modify';
            ProveedoresRow.insertPermission = 'Inmovilizado:Modify';
            ProveedoresRow.readPermission = 'Inmovilizado:Read';
            ProveedoresRow.updatePermission = 'Inmovilizado:Modify';
        })(ProveedoresRow = Inmovilizado.ProveedoresRow || (Inmovilizado.ProveedoresRow = {}));
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var ProveedoresService;
        (function (ProveedoresService) {
            ProveedoresService.baseUrl = 'Inmovilizado/Proveedores';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ProveedoresService[x] = function (r, s, o) {
                    return Q.serviceRequest(ProveedoresService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(ProveedoresService = Inmovilizado.ProveedoresService || (Inmovilizado.ProveedoresService = {}));
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var SubtiposinmovilizadoForm = /** @class */ (function (_super) {
            __extends(SubtiposinmovilizadoForm, _super);
            function SubtiposinmovilizadoForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!SubtiposinmovilizadoForm.init) {
                    SubtiposinmovilizadoForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    var w1 = s.StringEditor;
                    Q.initFormType(SubtiposinmovilizadoForm, [
                        'TipoInmovilizadoId', w0,
                        'SubTipo', w1
                    ]);
                }
                return _this;
            }
            SubtiposinmovilizadoForm.formKey = 'Inmovilizado.Subtiposinmovilizado';
            return SubtiposinmovilizadoForm;
        }(Serenity.PrefixedContext));
        Inmovilizado.SubtiposinmovilizadoForm = SubtiposinmovilizadoForm;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var SubtiposinmovilizadoRow;
        (function (SubtiposinmovilizadoRow) {
            SubtiposinmovilizadoRow.idProperty = 'SubTipoInmovilizadoId';
            SubtiposinmovilizadoRow.nameProperty = 'SubTipo';
            SubtiposinmovilizadoRow.localTextPrefix = 'Inmovilizado.Subtiposinmovilizado';
            SubtiposinmovilizadoRow.lookupKey = 'Inmovilizado.Subtiposinmovilizado';
            function getLookup() {
                return Q.getLookup('Inmovilizado.Subtiposinmovilizado');
            }
            SubtiposinmovilizadoRow.getLookup = getLookup;
            SubtiposinmovilizadoRow.deletePermission = 'Inmovilizado:Modify';
            SubtiposinmovilizadoRow.insertPermission = 'Inmovilizado:Modify';
            SubtiposinmovilizadoRow.readPermission = 'Inmovilizado:Read';
            SubtiposinmovilizadoRow.updatePermission = 'Inmovilizado:Modify';
        })(SubtiposinmovilizadoRow = Inmovilizado.SubtiposinmovilizadoRow || (Inmovilizado.SubtiposinmovilizadoRow = {}));
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var SubtiposinmovilizadoService;
        (function (SubtiposinmovilizadoService) {
            SubtiposinmovilizadoService.baseUrl = 'Inmovilizado/Subtiposinmovilizado';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SubtiposinmovilizadoService[x] = function (r, s, o) {
                    return Q.serviceRequest(SubtiposinmovilizadoService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(SubtiposinmovilizadoService = Inmovilizado.SubtiposinmovilizadoService || (Inmovilizado.SubtiposinmovilizadoService = {}));
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var TiposinmovilizadoForm = /** @class */ (function (_super) {
            __extends(TiposinmovilizadoForm, _super);
            function TiposinmovilizadoForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!TiposinmovilizadoForm.init) {
                    TiposinmovilizadoForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(TiposinmovilizadoForm, [
                        'Tipo', w0
                    ]);
                }
                return _this;
            }
            TiposinmovilizadoForm.formKey = 'Inmovilizado.Tiposinmovilizado';
            return TiposinmovilizadoForm;
        }(Serenity.PrefixedContext));
        Inmovilizado.TiposinmovilizadoForm = TiposinmovilizadoForm;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var TiposinmovilizadoRow;
        (function (TiposinmovilizadoRow) {
            TiposinmovilizadoRow.idProperty = 'TipoInmovilizadoId';
            TiposinmovilizadoRow.nameProperty = 'Tipo';
            TiposinmovilizadoRow.localTextPrefix = 'Inmovilizado.Tiposinmovilizado';
            TiposinmovilizadoRow.lookupKey = 'Inmovilizado.Tiposinmovilizado';
            function getLookup() {
                return Q.getLookup('Inmovilizado.Tiposinmovilizado');
            }
            TiposinmovilizadoRow.getLookup = getLookup;
            TiposinmovilizadoRow.deletePermission = 'Inmovilizado:General';
            TiposinmovilizadoRow.insertPermission = 'Inmovilizado:General';
            TiposinmovilizadoRow.readPermission = 'Inmovilizado:General';
            TiposinmovilizadoRow.updatePermission = 'Inmovilizado:General';
        })(TiposinmovilizadoRow = Inmovilizado.TiposinmovilizadoRow || (Inmovilizado.TiposinmovilizadoRow = {}));
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var TiposinmovilizadoService;
        (function (TiposinmovilizadoService) {
            TiposinmovilizadoService.baseUrl = 'Inmovilizado/Tiposinmovilizado';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposinmovilizadoService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposinmovilizadoService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TiposinmovilizadoService = Inmovilizado.TiposinmovilizadoService || (Inmovilizado.TiposinmovilizadoService = {}));
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var DepartamentosForm = /** @class */ (function (_super) {
            __extends(DepartamentosForm, _super);
            function DepartamentosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!DepartamentosForm.init) {
                    DepartamentosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(DepartamentosForm, [
                        'Departamento', w0
                    ]);
                }
                return _this;
            }
            DepartamentosForm.formKey = 'Intranet.Departamentos';
            return DepartamentosForm;
        }(Serenity.PrefixedContext));
        Intranet.DepartamentosForm = DepartamentosForm;
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var DepartamentosRow;
        (function (DepartamentosRow) {
            DepartamentosRow.idProperty = 'DepartamentoId';
            DepartamentosRow.nameProperty = 'Departamento';
            DepartamentosRow.localTextPrefix = 'Intranet.Departamentos';
            DepartamentosRow.lookupKey = 'Intranet.Departamentos';
            function getLookup() {
                return Q.getLookup('Intranet.Departamentos');
            }
            DepartamentosRow.getLookup = getLookup;
            DepartamentosRow.deletePermission = 'Telefonos:Modify';
            DepartamentosRow.insertPermission = 'Telefonos:Modify';
            DepartamentosRow.readPermission = 'Telefonos:Read';
            DepartamentosRow.updatePermission = 'Telefonos:Modify';
        })(DepartamentosRow = Intranet.DepartamentosRow || (Intranet.DepartamentosRow = {}));
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var DepartamentosService;
        (function (DepartamentosService) {
            DepartamentosService.baseUrl = 'Intranet/Departamentos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                DepartamentosService[x] = function (r, s, o) {
                    return Q.serviceRequest(DepartamentosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(DepartamentosService = Intranet.DepartamentosService || (Intranet.DepartamentosService = {}));
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var SedesForm = /** @class */ (function (_super) {
            __extends(SedesForm, _super);
            function SedesForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!SedesForm.init) {
                    SedesForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(SedesForm, [
                        'Sede', w0
                    ]);
                }
                return _this;
            }
            SedesForm.formKey = 'Intranet.Sedes';
            return SedesForm;
        }(Serenity.PrefixedContext));
        Intranet.SedesForm = SedesForm;
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var SedesRow;
        (function (SedesRow) {
            SedesRow.idProperty = 'SedeId';
            SedesRow.nameProperty = 'Sede';
            SedesRow.localTextPrefix = 'Intranet.Sedes';
            SedesRow.lookupKey = 'Intranet.Sedes';
            function getLookup() {
                return Q.getLookup('Intranet.Sedes');
            }
            SedesRow.getLookup = getLookup;
            SedesRow.deletePermission = 'Telefonos:Modify';
            SedesRow.insertPermission = 'Telefonos:Modify';
            SedesRow.readPermission = 'Telefonos:Read';
            SedesRow.updatePermission = 'Telefonos:Modify';
        })(SedesRow = Intranet.SedesRow || (Intranet.SedesRow = {}));
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var SedesService;
        (function (SedesService) {
            SedesService.baseUrl = 'Intranet/Sedes';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SedesService[x] = function (r, s, o) {
                    return Q.serviceRequest(SedesService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(SedesService = Intranet.SedesService || (Intranet.SedesService = {}));
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var TelefonosForm = /** @class */ (function (_super) {
            __extends(TelefonosForm, _super);
            function TelefonosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!TelefonosForm.init) {
                    TelefonosForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    var w1 = s.StringEditor;
                    Q.initFormType(TelefonosForm, [
                        'SedeId', w0,
                        'DepartamentoId', w0,
                        'Nombre', w1,
                        'ExtCorta', w1,
                        'Fijo', w1,
                        'Movil', w1,
                        'CortoMovil', w1,
                        'Contrato', w1,
                        'PUK', w1,
                        'Multisim', w1
                    ]);
                }
                return _this;
            }
            TelefonosForm.formKey = 'Intranet.Telefonos';
            return TelefonosForm;
        }(Serenity.PrefixedContext));
        Intranet.TelefonosForm = TelefonosForm;
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var TelefonosRow;
        (function (TelefonosRow) {
            TelefonosRow.idProperty = 'TelefonoId';
            TelefonosRow.nameProperty = 'Nombre';
            TelefonosRow.localTextPrefix = 'Intranet.Telefonos';
            TelefonosRow.lookupKey = 'Telefonos.Telefonos';
            function getLookup() {
                return Q.getLookup('Telefonos.Telefonos');
            }
            TelefonosRow.getLookup = getLookup;
            TelefonosRow.deletePermission = 'Telefonos:Delete';
            TelefonosRow.insertPermission = 'Telefonos:Insert';
            TelefonosRow.readPermission = 'Telefonos:Read';
            TelefonosRow.updatePermission = 'Telefonos:Modify';
        })(TelefonosRow = Intranet.TelefonosRow || (Intranet.TelefonosRow = {}));
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var TelefonosService;
        (function (TelefonosService) {
            TelefonosService.baseUrl = 'Intranet/Telefonos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TelefonosService[x] = function (r, s, o) {
                    return Q.serviceRequest(TelefonosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TelefonosService = Intranet.TelefonosService || (Intranet.TelefonosService = {}));
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var AusenciasProgramadasForm = /** @class */ (function (_super) {
            __extends(AusenciasProgramadasForm, _super);
            function AusenciasProgramadasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!AusenciasProgramadasForm.init) {
                    AusenciasProgramadasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.LookupEditor;
                    var w2 = s.DateTimeEditor;
                    var w3 = s.DateEditor;
                    var w4 = s.DecimalEditor;
                    var w5 = s.IntegerEditor;
                    Q.initFormType(AusenciasProgramadasForm, [
                        'CodigoCliente', w0,
                        'IdEmpleado', w1,
                        'IdAusenciaProgramadaTipo', w1,
                        'FechaDesde', w2,
                        'FechaHasta', w2,
                        'FechaBorrado', w3,
                        'TotalHoras', w4,
                        'TotalDias', w5
                    ]);
                }
                return _this;
            }
            AusenciasProgramadasForm.formKey = 'Kairos.AusenciasProgramadas';
            return AusenciasProgramadasForm;
        }(Serenity.PrefixedContext));
        Kairos.AusenciasProgramadasForm = AusenciasProgramadasForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var AusenciasProgramadasRow;
        (function (AusenciasProgramadasRow) {
            AusenciasProgramadasRow.idProperty = 'Id';
            AusenciasProgramadasRow.localTextPrefix = 'Kairos.AusenciasProgramadas';
            AusenciasProgramadasRow.deletePermission = 'Kairos:Delete';
            AusenciasProgramadasRow.insertPermission = 'Kairos:Insert';
            AusenciasProgramadasRow.readPermission = 'Kairos:Read';
            AusenciasProgramadasRow.updatePermission = 'Kairos:Admin';
        })(AusenciasProgramadasRow = Kairos.AusenciasProgramadasRow || (Kairos.AusenciasProgramadasRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var AusenciasProgramadasService;
        (function (AusenciasProgramadasService) {
            AusenciasProgramadasService.baseUrl = 'Kairos/AusenciasProgramadas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AusenciasProgramadasService[x] = function (r, s, o) {
                    return Q.serviceRequest(AusenciasProgramadasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(AusenciasProgramadasService = Kairos.AusenciasProgramadasService || (Kairos.AusenciasProgramadasService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DepartamentosForm = /** @class */ (function (_super) {
            __extends(DepartamentosForm, _super);
            function DepartamentosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!DepartamentosForm.init) {
                    DepartamentosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.DateEditor;
                    var w2 = s.IntegerEditor;
                    Q.initFormType(DepartamentosForm, [
                        'CodigoCliente', w0,
                        'Codigo', w0,
                        'Descripcion', w0,
                        'FechaBorrado', w1,
                        'SedeId', w2
                    ]);
                }
                return _this;
            }
            DepartamentosForm.formKey = 'Kairos.Departamentos';
            return DepartamentosForm;
        }(Serenity.PrefixedContext));
        Kairos.DepartamentosForm = DepartamentosForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DepartamentosRow;
        (function (DepartamentosRow) {
            DepartamentosRow.idProperty = 'Id';
            DepartamentosRow.nameProperty = 'Codigo';
            DepartamentosRow.localTextPrefix = 'Kairos.Departamentos';
            DepartamentosRow.deletePermission = 'Kairos:Delete';
            DepartamentosRow.insertPermission = 'Kairos:Insert';
            DepartamentosRow.readPermission = 'Kairos:Read';
            DepartamentosRow.updatePermission = 'Kairos:Modify';
        })(DepartamentosRow = Kairos.DepartamentosRow || (Kairos.DepartamentosRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DepartamentosService;
        (function (DepartamentosService) {
            DepartamentosService.baseUrl = 'Kairos/Departamentos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                DepartamentosService[x] = function (r, s, o) {
                    return Q.serviceRequest(DepartamentosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(DepartamentosService = Kairos.DepartamentosService || (Kairos.DepartamentosService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DiarioForm = /** @class */ (function (_super) {
            __extends(DiarioForm, _super);
            function DiarioForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!DiarioForm.init) {
                    DiarioForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.DateEditor;
                    Q.initFormType(DiarioForm, [
                        'IdDepartamento', w0,
                        'Empleado', w0,
                        'Fecha', w1,
                        'Entrada', w1,
                        'HoraEntrada', w0,
                        'Salida', w1,
                        'HoraSalida', w0
                    ]);
                }
                return _this;
            }
            DiarioForm.formKey = 'Kairos.Diario';
            return DiarioForm;
        }(Serenity.PrefixedContext));
        Kairos.DiarioForm = DiarioForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DiarioRow;
        (function (DiarioRow) {
            DiarioRow.idProperty = 'Id';
            DiarioRow.nameProperty = 'Empleado';
            DiarioRow.localTextPrefix = 'Kairos.Diario';
            DiarioRow.deletePermission = 'Kairos:Delete';
            DiarioRow.insertPermission = 'Kairos:Insert';
            DiarioRow.readPermission = 'Kairos:Read';
            DiarioRow.updatePermission = 'Kairos:Modify';
        })(DiarioRow = Kairos.DiarioRow || (Kairos.DiarioRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DiarioService;
        (function (DiarioService) {
            DiarioService.baseUrl = 'Kairos/Diario';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                DiarioService[x] = function (r, s, o) {
                    return Q.serviceRequest(DiarioService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(DiarioService = Kairos.DiarioService || (Kairos.DiarioService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var EstadosExtrasForm = /** @class */ (function (_super) {
            __extends(EstadosExtrasForm, _super);
            function EstadosExtrasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EstadosExtrasForm.init) {
                    EstadosExtrasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(EstadosExtrasForm, [
                        'Descripcion', w0
                    ]);
                }
                return _this;
            }
            EstadosExtrasForm.formKey = 'Kairos.EstadosExtras';
            return EstadosExtrasForm;
        }(Serenity.PrefixedContext));
        Kairos.EstadosExtrasForm = EstadosExtrasForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var EstadosExtrasRow;
        (function (EstadosExtrasRow) {
            EstadosExtrasRow.idProperty = 'EstadoId';
            EstadosExtrasRow.nameProperty = 'Descripcion';
            EstadosExtrasRow.localTextPrefix = 'Kairos.EstadosExtras';
            EstadosExtrasRow.lookupKey = 'Kairos.EstadosExtras';
            function getLookup() {
                return Q.getLookup('Kairos.EstadosExtras');
            }
            EstadosExtrasRow.getLookup = getLookup;
            EstadosExtrasRow.deletePermission = 'Kairos:Delete';
            EstadosExtrasRow.insertPermission = 'Kairos:Insert';
            EstadosExtrasRow.readPermission = 'Kairos:Read';
            EstadosExtrasRow.updatePermission = 'Kairos:Modify';
        })(EstadosExtrasRow = Kairos.EstadosExtrasRow || (Kairos.EstadosExtrasRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var EstadosExtrasService;
        (function (EstadosExtrasService) {
            EstadosExtrasService.baseUrl = 'Kairos/EstadosExtras';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EstadosExtrasService[x] = function (r, s, o) {
                    return Q.serviceRequest(EstadosExtrasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EstadosExtrasService = Kairos.EstadosExtrasService || (Kairos.EstadosExtrasService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var ExtrasForm = /** @class */ (function (_super) {
            __extends(ExtrasForm, _super);
            function ExtrasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ExtrasForm.init) {
                    ExtrasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.DateEditor;
                    var w2 = s.IntegerEditor;
                    var w3 = s.DecimalEditor;
                    var w4 = s.LookupEditor;
                    var w5 = Kairos.HorasExtraConsumidasEditor;
                    Q.initFormType(ExtrasForm, [
                        'Id', w0,
                        'CodigoCliente', w0,
                        'IdEmpleado', w0,
                        'Fecha', w1,
                        'IdHoraExtraCabecera', w0,
                        'Tipo', w2,
                        'TotalHorasExtrasReales', w3,
                        'TotalHorasExtrasConvertidas', w3,
                        'IdAusenciaProgramadaTipo', w0,
                        'Dia', w0,
                        'Estado', w4,
                        'MotivoCancelacion', w0,
                        'FechaAceptacionCancelacion', w1,
                        'Consumidas', w5
                    ]);
                }
                return _this;
            }
            ExtrasForm.formKey = 'Kairos.Extras';
            return ExtrasForm;
        }(Serenity.PrefixedContext));
        Kairos.ExtrasForm = ExtrasForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var ExtrasRow;
        (function (ExtrasRow) {
            ExtrasRow.idProperty = 'Id';
            ExtrasRow.nameProperty = 'Dia';
            ExtrasRow.localTextPrefix = 'Kairos.Extras';
            ExtrasRow.deletePermission = 'Kairos:Admin';
            ExtrasRow.insertPermission = 'Kairos:Admin';
            ExtrasRow.readPermission = 'Kairos:Read';
            ExtrasRow.updatePermission = 'Kairos:Admin';
        })(ExtrasRow = Kairos.ExtrasRow || (Kairos.ExtrasRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var ExtrasService;
        (function (ExtrasService) {
            ExtrasService.baseUrl = 'Kairos/Extras';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ExtrasService[x] = function (r, s, o) {
                    return Q.serviceRequest(ExtrasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(ExtrasService = Kairos.ExtrasService || (Kairos.ExtrasService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var FichajesForm = /** @class */ (function (_super) {
            __extends(FichajesForm, _super);
            function FichajesForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!FichajesForm.init) {
                    FichajesForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    var w1 = s.StringEditor;
                    var w2 = s.DateTimeEditor;
                    var w3 = s.DateEditor;
                    var w4 = s.IntegerEditor;
                    var w5 = s.MultipleImageUploadEditor;
                    Q.initFormType(FichajesForm, [
                        'IdEmpleado', w0,
                        'CodigoCliente', w1,
                        'FechaHora', w2,
                        'Observaciones', w1,
                        'GpsPosicionLatitud', w1,
                        'GpsPosicionLongitud', w1,
                        'GpsFechaHora', w2,
                        'GpsProveedor', w1,
                        'GpsAltitud', w1,
                        'IdTerminal', w1,
                        'IdDispositivoModelo', w1,
                        'Modificado', w3,
                        'Anulado', w3,
                        'Validado', w4,
                        'TipoDispositivo', w4,
                        'EntradaSalida', w0,
                        'IdEmpresa', w1,
                        'Files', w5
                    ]);
                }
                return _this;
            }
            FichajesForm.formKey = 'Kairos.Fichajes';
            return FichajesForm;
        }(Serenity.PrefixedContext));
        Kairos.FichajesForm = FichajesForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var FichajesRow;
        (function (FichajesRow) {
            FichajesRow.idProperty = 'Id';
            FichajesRow.nameProperty = 'Observaciones';
            FichajesRow.localTextPrefix = 'Kairos.Fichajes';
            FichajesRow.deletePermission = 'Kairos:Delete';
            FichajesRow.insertPermission = 'Kairos:Insert';
            FichajesRow.readPermission = 'Kairos:Read';
            FichajesRow.updatePermission = 'Kairos:Admin';
        })(FichajesRow = Kairos.FichajesRow || (Kairos.FichajesRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var FichajesService;
        (function (FichajesService) {
            FichajesService.baseUrl = 'Kairos/Fichajes';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                FichajesService[x] = function (r, s, o) {
                    return Q.serviceRequest(FichajesService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(FichajesService = Kairos.FichajesService || (Kairos.FichajesService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HorasExtraConsumidasForm = /** @class */ (function (_super) {
            __extends(HorasExtraConsumidasForm, _super);
            function HorasExtraConsumidasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!HorasExtraConsumidasForm.init) {
                    HorasExtraConsumidasForm.init = true;
                    var s = Serenity;
                    var w0 = s.DecimalEditor;
                    var w1 = s.DateEditor;
                    Q.initFormType(HorasExtraConsumidasForm, [
                        'Tiempo', w0,
                        'Dia', w1
                    ]);
                }
                return _this;
            }
            HorasExtraConsumidasForm.formKey = 'Kairos.HorasExtraConsumidas';
            return HorasExtraConsumidasForm;
        }(Serenity.PrefixedContext));
        Kairos.HorasExtraConsumidasForm = HorasExtraConsumidasForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HorasExtraConsumidasRow;
        (function (HorasExtraConsumidasRow) {
            HorasExtraConsumidasRow.idProperty = 'Id';
            HorasExtraConsumidasRow.nameProperty = 'Empleado';
            HorasExtraConsumidasRow.localTextPrefix = 'Kairos.HorasExtraConsumidas';
            HorasExtraConsumidasRow.deletePermission = 'Kairos:Admin';
            HorasExtraConsumidasRow.insertPermission = 'Kairos:Admin';
            HorasExtraConsumidasRow.readPermission = 'Kairos:Read';
            HorasExtraConsumidasRow.updatePermission = 'Kairos:Admin';
        })(HorasExtraConsumidasRow = Kairos.HorasExtraConsumidasRow || (Kairos.HorasExtraConsumidasRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HorasExtraConsumidasService;
        (function (HorasExtraConsumidasService) {
            HorasExtraConsumidasService.baseUrl = 'Kairos/HorasExtraConsumidas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                HorasExtraConsumidasService[x] = function (r, s, o) {
                    return Q.serviceRequest(HorasExtraConsumidasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(HorasExtraConsumidasService = Kairos.HorasExtraConsumidasService || (Kairos.HorasExtraConsumidasService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HoyForm = /** @class */ (function (_super) {
            __extends(HoyForm, _super);
            function HoyForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            HoyForm.formKey = 'Kairos.Hoy';
            return HoyForm;
        }(Serenity.PrefixedContext));
        Kairos.HoyForm = HoyForm;
        [, ['CodigoCliente', function () { return Serenity.IntegerEditor; }],
            ['Nombre', function () { return Serenity.StringEditor; }],
            ['CodigoValidacion', function () { return Serenity.StringEditor; }],
            ['FechaBaja', function () { return Serenity.DateEditor; }],
            ['Pin', function () { return Serenity.IntegerEditor; }],
            ['Tecnico', function () { return Serenity.BooleanEditor; }],
            ['UsoHorario', function () { return Serenity.StringEditor; }],
            ['SacarFotoFichaje', function () { return Serenity.BooleanEditor; }],
            ['FechaActualizacion', function () { return Serenity.DateEditor; }],
            ['FechaBorrado', function () { return Serenity.DateEditor; }],
            ['NumeroTarjetaFichaje', function () { return Serenity.StringEditor; }],
            ['IdDepartamento', function () { return Serenity.IntegerEditor; }],
            ['IdEmpresa', function () { return Serenity.IntegerEditor; }],
            ['Email', function () { return Serenity.StringEditor; }],
            ['PermiteRecordatorio', function () { return Serenity.BooleanEditor; }],
            ['PermiteFichajeAutomatico', function () { return Serenity.BooleanEditor; }],
            ['IdEmpresaFichajeAutomatico', function () { return Serenity.IntegerEditor; }],
            ['ProgramaExternoIdEmpleado', function () { return Serenity.StringEditor; }],
            ['ProgramaExternoDescripcion', function () { return Serenity.StringEditor; }],
            ['IdHoraExtraCabecera', function () { return Serenity.IntegerEditor; }],
            ['ClaveAccesoWeb', function () { return Serenity.StringEditor; }],
            ['PermiteFichajeWeb', function () { return Serenity.BooleanEditor; }]
        ].forEach(function (x) { return Object.defineProperty(HoyForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HoyRow;
        (function (HoyRow) {
            HoyRow.idProperty = 'Id';
            HoyRow.nameProperty = 'Nombre';
            HoyRow.localTextPrefix = 'Kairos.Hoy';
            HoyRow.deletePermission = 'Kairos:Delete';
            HoyRow.insertPermission = 'Kairos:Insert';
            HoyRow.readPermission = 'Kairos:Hoy';
            HoyRow.updatePermission = 'Kairos:Modify';
            var Fields;
            (function (Fields) {
            })(Fields = HoyRow.Fields || (HoyRow.Fields = {}));
            [
                'Id',
                'Nombre',
                'IdDepartamento',
                'SedId',
                'Sede',
                'Estado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(HoyRow = Kairos.HoyRow || (Kairos.HoyRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HoyService;
        (function (HoyService) {
            HoyService.baseUrl = 'Kairos/Hoy';
            var Methods;
            (function (Methods) {
            })(Methods = HoyService.Methods || (HoyService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                HoyService[x] = function (r, s, o) {
                    return Q.serviceRequest(HoyService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = HoyService.baseUrl + '/' + x;
            });
        })(HoyService = Kairos.HoyService || (Kairos.HoyService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsAusenciasProgramadasTiposForm = /** @class */ (function (_super) {
            __extends(KrsAusenciasProgramadasTiposForm, _super);
            function KrsAusenciasProgramadasTiposForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!KrsAusenciasProgramadasTiposForm.init) {
                    KrsAusenciasProgramadasTiposForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.DateEditor;
                    var w2 = s.BooleanEditor;
                    Q.initFormType(KrsAusenciasProgramadasTiposForm, [
                        'CodigoCliente', w0,
                        'Codigo', w0,
                        'Descripcion', w0,
                        'ColorFondo', w0,
                        'ColorLetra', w0,
                        'FechaBorrado', w1,
                        'PermitirSolicitud', w2,
                        'FechaActualizacion', w1,
                        'ContabilizarTiempo', w2
                    ]);
                }
                return _this;
            }
            KrsAusenciasProgramadasTiposForm.formKey = 'Kairos.KrsAusenciasProgramadasTipos';
            return KrsAusenciasProgramadasTiposForm;
        }(Serenity.PrefixedContext));
        Kairos.KrsAusenciasProgramadasTiposForm = KrsAusenciasProgramadasTiposForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsAusenciasProgramadasTiposRow;
        (function (KrsAusenciasProgramadasTiposRow) {
            KrsAusenciasProgramadasTiposRow.idProperty = 'Id';
            KrsAusenciasProgramadasTiposRow.nameProperty = 'Descripcion';
            KrsAusenciasProgramadasTiposRow.localTextPrefix = 'Kairos.KrsAusenciasProgramadasTipos';
            KrsAusenciasProgramadasTiposRow.lookupKey = 'Kairos.Krs_AusenciasProgramadasTipos';
            function getLookup() {
                return Q.getLookup('Kairos.Krs_AusenciasProgramadasTipos');
            }
            KrsAusenciasProgramadasTiposRow.getLookup = getLookup;
            KrsAusenciasProgramadasTiposRow.deletePermission = 'Kairos:Delete';
            KrsAusenciasProgramadasTiposRow.insertPermission = 'Kairos:Insert';
            KrsAusenciasProgramadasTiposRow.readPermission = 'Kairos:Read';
            KrsAusenciasProgramadasTiposRow.updatePermission = 'Kairos:Modify';
        })(KrsAusenciasProgramadasTiposRow = Kairos.KrsAusenciasProgramadasTiposRow || (Kairos.KrsAusenciasProgramadasTiposRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsAusenciasProgramadasTiposService;
        (function (KrsAusenciasProgramadasTiposService) {
            KrsAusenciasProgramadasTiposService.baseUrl = 'Kairos/KrsAusenciasProgramadasTipos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                KrsAusenciasProgramadasTiposService[x] = function (r, s, o) {
                    return Q.serviceRequest(KrsAusenciasProgramadasTiposService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(KrsAusenciasProgramadasTiposService = Kairos.KrsAusenciasProgramadasTiposService || (Kairos.KrsAusenciasProgramadasTiposService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsEmpleadosForm = /** @class */ (function (_super) {
            __extends(KrsEmpleadosForm, _super);
            function KrsEmpleadosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!KrsEmpleadosForm.init) {
                    KrsEmpleadosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.DateEditor;
                    var w2 = s.IntegerEditor;
                    var w3 = s.BooleanEditor;
                    Q.initFormType(KrsEmpleadosForm, [
                        'CodigoCliente', w0,
                        'Nombre', w0,
                        'CodigoValidacion', w0,
                        'FechaBaja', w1,
                        'Pin', w2,
                        'Tecnico', w3,
                        'UsoHorario', w0,
                        'SacarFotoFichaje', w3,
                        'FechaActualizacion', w1,
                        'FechaBorrado', w1,
                        'NumeroTarjetaFichaje', w0,
                        'IdDepartamento', w0,
                        'IdEmpresa', w0,
                        'Email', w0,
                        'PermiteRecordatorio', w3,
                        'PermiteFichajeAutomatico', w3,
                        'IdEmpresaFichajeAutomatico', w0,
                        'ProgramaExternoIdEmpleado', w0,
                        'ProgramaExternoDescripcion', w0,
                        'IdHoraExtraCabecera', w0,
                        'ClaveAccesoWeb', w0,
                        'PermiteFichajeWeb', w3
                    ]);
                }
                return _this;
            }
            KrsEmpleadosForm.formKey = 'Kairos.KrsEmpleados';
            return KrsEmpleadosForm;
        }(Serenity.PrefixedContext));
        Kairos.KrsEmpleadosForm = KrsEmpleadosForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsEmpleadosRow;
        (function (KrsEmpleadosRow) {
            KrsEmpleadosRow.idProperty = 'Id';
            KrsEmpleadosRow.nameProperty = 'Nombre';
            KrsEmpleadosRow.localTextPrefix = 'Kairos.KrsEmpleados';
            KrsEmpleadosRow.lookupKey = 'Kairos.KrsEmpleados';
            function getLookup() {
                return Q.getLookup('Kairos.KrsEmpleados');
            }
            KrsEmpleadosRow.getLookup = getLookup;
            KrsEmpleadosRow.deletePermission = 'Kairos:Delete';
            KrsEmpleadosRow.insertPermission = 'Kairos:Insert';
            KrsEmpleadosRow.readPermission = 'Kairos:Read';
            KrsEmpleadosRow.updatePermission = 'Kairos:Modify';
        })(KrsEmpleadosRow = Kairos.KrsEmpleadosRow || (Kairos.KrsEmpleadosRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsEmpleadosService;
        (function (KrsEmpleadosService) {
            KrsEmpleadosService.baseUrl = 'Kairos/KrsEmpleados';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                KrsEmpleadosService[x] = function (r, s, o) {
                    return Q.serviceRequest(KrsEmpleadosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(KrsEmpleadosService = Kairos.KrsEmpleadosService || (Kairos.KrsEmpleadosService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var TiposFichajeForm = /** @class */ (function (_super) {
            __extends(TiposFichajeForm, _super);
            function TiposFichajeForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!TiposFichajeForm.init) {
                    TiposFichajeForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(TiposFichajeForm, [
                        'Tipo', w0
                    ]);
                }
                return _this;
            }
            TiposFichajeForm.formKey = 'Kairos.TiposFichaje';
            return TiposFichajeForm;
        }(Serenity.PrefixedContext));
        Kairos.TiposFichajeForm = TiposFichajeForm;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var TiposFichajeRow;
        (function (TiposFichajeRow) {
            TiposFichajeRow.idProperty = 'Id';
            TiposFichajeRow.nameProperty = 'Tipo';
            TiposFichajeRow.localTextPrefix = 'Kairos.TiposFichaje';
            TiposFichajeRow.lookupKey = 'Kairos.TiposFichaje';
            function getLookup() {
                return Q.getLookup('Kairos.TiposFichaje');
            }
            TiposFichajeRow.getLookup = getLookup;
            TiposFichajeRow.deletePermission = 'Kairos:Delete';
            TiposFichajeRow.insertPermission = 'Kairos:Insert';
            TiposFichajeRow.readPermission = 'Kairos:Read';
            TiposFichajeRow.updatePermission = 'Kairos:Modify';
        })(TiposFichajeRow = Kairos.TiposFichajeRow || (Kairos.TiposFichajeRow = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var TiposFichajeService;
        (function (TiposFichajeService) {
            TiposFichajeService.baseUrl = 'Kairos/TiposFichaje';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposFichajeService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposFichajeService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TiposFichajeService = Kairos.TiposFichajeService || (Kairos.TiposFichajeService = {}));
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Membership;
    (function (Membership) {
        var ChangePasswordForm = /** @class */ (function (_super) {
            __extends(ChangePasswordForm, _super);
            function ChangePasswordForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ChangePasswordForm.init) {
                    ChangePasswordForm.init = true;
                    var s = Serenity;
                    var w0 = s.PasswordEditor;
                    Q.initFormType(ChangePasswordForm, [
                        'OldPassword', w0,
                        'NewPassword', w0,
                        'ConfirmPassword', w0
                    ]);
                }
                return _this;
            }
            ChangePasswordForm.formKey = 'Membership.ChangePassword';
            return ChangePasswordForm;
        }(Serenity.PrefixedContext));
        Membership.ChangePasswordForm = ChangePasswordForm;
    })(Membership = ProyectosZec.Membership || (ProyectosZec.Membership = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Membership;
    (function (Membership) {
        var ForgotPasswordForm = /** @class */ (function (_super) {
            __extends(ForgotPasswordForm, _super);
            function ForgotPasswordForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ForgotPasswordForm.init) {
                    ForgotPasswordForm.init = true;
                    var s = Serenity;
                    var w0 = s.EmailEditor;
                    Q.initFormType(ForgotPasswordForm, [
                        'Email', w0
                    ]);
                }
                return _this;
            }
            ForgotPasswordForm.formKey = 'Membership.ForgotPassword';
            return ForgotPasswordForm;
        }(Serenity.PrefixedContext));
        Membership.ForgotPasswordForm = ForgotPasswordForm;
    })(Membership = ProyectosZec.Membership || (ProyectosZec.Membership = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Membership;
    (function (Membership) {
        var LoginForm = /** @class */ (function (_super) {
            __extends(LoginForm, _super);
            function LoginForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!LoginForm.init) {
                    LoginForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.PasswordEditor;
                    Q.initFormType(LoginForm, [
                        'Username', w0,
                        'Password', w1
                    ]);
                }
                return _this;
            }
            LoginForm.formKey = 'Membership.Login';
            return LoginForm;
        }(Serenity.PrefixedContext));
        Membership.LoginForm = LoginForm;
    })(Membership = ProyectosZec.Membership || (ProyectosZec.Membership = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Membership;
    (function (Membership) {
        var ResetPasswordForm = /** @class */ (function (_super) {
            __extends(ResetPasswordForm, _super);
            function ResetPasswordForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ResetPasswordForm.init) {
                    ResetPasswordForm.init = true;
                    var s = Serenity;
                    var w0 = s.PasswordEditor;
                    Q.initFormType(ResetPasswordForm, [
                        'NewPassword', w0,
                        'ConfirmPassword', w0
                    ]);
                }
                return _this;
            }
            ResetPasswordForm.formKey = 'Membership.ResetPassword';
            return ResetPasswordForm;
        }(Serenity.PrefixedContext));
        Membership.ResetPasswordForm = ResetPasswordForm;
    })(Membership = ProyectosZec.Membership || (ProyectosZec.Membership = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Membership;
    (function (Membership) {
        var SignUpForm = /** @class */ (function (_super) {
            __extends(SignUpForm, _super);
            function SignUpForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!SignUpForm.init) {
                    SignUpForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.EmailEditor;
                    var w2 = s.PasswordEditor;
                    Q.initFormType(SignUpForm, [
                        'DisplayName', w0,
                        'Email', w1,
                        'ConfirmEmail', w1,
                        'Password', w2,
                        'ConfirmPassword', w2
                    ]);
                }
                return _this;
            }
            SignUpForm.formKey = 'Membership.SignUp';
            return SignUpForm;
        }(Serenity.PrefixedContext));
        Membership.SignUpForm = SignUpForm;
    })(Membership = ProyectosZec.Membership || (ProyectosZec.Membership = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasForm = /** @class */ (function (_super) {
            __extends(AlarmasForm, _super);
            function AlarmasForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AlarmasForm.formKey = 'Nuevo_Roezec.Alarmas';
            return AlarmasForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.AlarmasForm = AlarmasForm;
        [, ['TipoAlarmaId', function () { return Serenity.IntegerEditor; }],
            ['EmpresaId', function () { return Serenity.IntegerEditor; }],
            ['Activa', function () { return Serenity.IntegerEditor; }],
            ['FechaCreacion', function () { return Serenity.DateEditor; }],
            ['UserId', function () { return Serenity.IntegerEditor; }],
            ['FechaModificacion', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(AlarmasForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasProcedimientosForm = /** @class */ (function (_super) {
            __extends(AlarmasProcedimientosForm, _super);
            function AlarmasProcedimientosForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AlarmasProcedimientosForm.formKey = 'Nuevo_Roezec.AlarmasProcedimientos';
            return AlarmasProcedimientosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.AlarmasProcedimientosForm = AlarmasProcedimientosForm;
        [, ['ProcedimientoId', function () { return Serenity.IntegerEditor; }],
            ['SentidoResolucionId', function () { return Serenity.IntegerEditor; }],
            ['UserId', function () { return Serenity.IntegerEditor; }],
            ['FechaModificacion', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(AlarmasProcedimientosForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasProcedimientosRow;
        (function (AlarmasProcedimientosRow) {
            AlarmasProcedimientosRow.idProperty = 'AlarmaProcedimientoId';
            AlarmasProcedimientosRow.nameProperty = 'TipoAlarma';
            AlarmasProcedimientosRow.localTextPrefix = 'Nuevo_Roezec.AlarmasProcedimientos';
            AlarmasProcedimientosRow.deletePermission = 'Roezec:Delete';
            AlarmasProcedimientosRow.insertPermission = 'Roezec:Insert';
            AlarmasProcedimientosRow.readPermission = 'Roezec:Read';
            AlarmasProcedimientosRow.updatePermission = 'Roezec:Update';
            var Fields;
            (function (Fields) {
            })(Fields = AlarmasProcedimientosRow.Fields || (AlarmasProcedimientosRow.Fields = {}));
            [
                'AlarmaProcedimientoId',
                'ProcedimientoId',
                'SentidoResolucionId',
                'UserId',
                'UserName',
                'DisplayName',
                'FechaModificacion',
                'Procedimiento',
                'TipoAlarma',
                'ProcedimientoEstadoEmpresaId',
                'ProcedimientoEstadoEmpresaIdDesfavorable',
                'ProcedimientoUserId',
                'ProcedimientoFechaModificacion',
                'ProcedimientoAlarmaFavorableId',
                'ProcedimientoAlarmaDesfavorableId',
                'SentidoResolucion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(AlarmasProcedimientosRow = Nuevo_Roezec.AlarmasProcedimientosRow || (Nuevo_Roezec.AlarmasProcedimientosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasProcedimientosService;
        (function (AlarmasProcedimientosService) {
            AlarmasProcedimientosService.baseUrl = 'Nuevo_Roezec/AlarmasProcedimientos';
            var Methods;
            (function (Methods) {
            })(Methods = AlarmasProcedimientosService.Methods || (AlarmasProcedimientosService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AlarmasProcedimientosService[x] = function (r, s, o) {
                    return Q.serviceRequest(AlarmasProcedimientosService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = AlarmasProcedimientosService.baseUrl + '/' + x;
            });
        })(AlarmasProcedimientosService = Nuevo_Roezec.AlarmasProcedimientosService || (Nuevo_Roezec.AlarmasProcedimientosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasRow;
        (function (AlarmasRow) {
            AlarmasRow.idProperty = 'AlarmaId';
            AlarmasRow.localTextPrefix = 'Nuevo_Roezec.Alarmas';
            AlarmasRow.deletePermission = 'Roezec:Read';
            AlarmasRow.insertPermission = 'Roezec:Read';
            AlarmasRow.readPermission = 'Roezec:Read';
            AlarmasRow.updatePermission = 'Roezec:Read';
            var Fields;
            (function (Fields) {
            })(Fields = AlarmasRow.Fields || (AlarmasRow.Fields = {}));
            [
                'AlarmaId',
                'TipoAlarmaId',
                'EmpresaId',
                'Activa',
                'FechaCreacion',
                'FechaAviso',
                'FechaCaducidad',
                'UserId',
                'FechaModificacion',
                'TipoAlarmaTexto',
                'TipoAlarmaDias',
                'TipoAlarmaDiasAviso',
                'TipoAlarmaEmail',
                'TipoAlarmaUserId',
                'TipoAlarmaFechaModificacion',
                'EmpresaRazon',
                'EmpresaFormaJuridicaId',
                'EmpresaTecnicoId',
                'EmpresaCif',
                'EmpresaDireccion',
                'EmpresaPoblacion',
                'EmpresaCp',
                'EmpresaIslaId',
                'EmpresaTelefonoFijo',
                'EmpresaMovil',
                'EmpresaCapitalSocial',
                'EmpresaEmail',
                'EmpresaProyectoId',
                'EmpresaNumeroRoezec',
                'EmpresaExpediente',
                'EmpresaMotivoExencion',
                'EmpresaTipologiaCapitalId',
                'EmpresaTipoGarantiaTasaId',
                'EmpresaEmpleoTraspasado',
                'EmpresaEmpleo6Meses',
                'EmpresaEmpleoPromedio',
                'EmpresaEmpleoPromedio2Anos',
                'EmpresaInversionTraspasada',
                'EmpresaInversion2Anos',
                'EmpresaEstadoEmpresaId',
                'EmpresaFechaCambioEstado',
                'EmpresaNumTasaLiquidacion',
                'EmpresaUserId',
                'EmpresaFechaModificacion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(AlarmasRow = Nuevo_Roezec.AlarmasRow || (Nuevo_Roezec.AlarmasRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasService;
        (function (AlarmasService) {
            AlarmasService.baseUrl = 'Nuevo_Roezec/Alarmas';
            var Methods;
            (function (Methods) {
            })(Methods = AlarmasService.Methods || (AlarmasService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AlarmasService[x] = function (r, s, o) {
                    return Q.serviceRequest(AlarmasService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = AlarmasService.baseUrl + '/' + x;
            });
        })(AlarmasService = Nuevo_Roezec.AlarmasService || (Nuevo_Roezec.AlarmasService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var CapitalForm = /** @class */ (function (_super) {
            __extends(CapitalForm, _super);
            function CapitalForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!CapitalForm.init) {
                    CapitalForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(CapitalForm, [
                        'Capital', w0
                    ]);
                }
                return _this;
            }
            CapitalForm.formKey = 'Nuevo_Roezec.Capital';
            return CapitalForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.CapitalForm = CapitalForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var CapitalRow;
        (function (CapitalRow) {
            CapitalRow.idProperty = 'CapitalId';
            CapitalRow.nameProperty = 'Capital';
            CapitalRow.localTextPrefix = 'Nuevo_Roezec.Capital';
            CapitalRow.lookupKey = 'Nuevo_Roezec.Capital';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Capital');
            }
            CapitalRow.getLookup = getLookup;
            CapitalRow.deletePermission = 'Roezec:Modify';
            CapitalRow.insertPermission = 'Roezec:Modify';
            CapitalRow.readPermission = 'Roezec:Read';
            CapitalRow.updatePermission = 'Roezec:Modify';
        })(CapitalRow = Nuevo_Roezec.CapitalRow || (Nuevo_Roezec.CapitalRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var CapitalService;
        (function (CapitalService) {
            CapitalService.baseUrl = 'Nuevo_Roezec/Capital';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                CapitalService[x] = function (r, s, o) {
                    return Q.serviceRequest(CapitalService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(CapitalService = Nuevo_Roezec.CapitalService || (Nuevo_Roezec.CapitalService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContactosForm = /** @class */ (function (_super) {
            __extends(ContactosForm, _super);
            function ContactosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ContactosForm.init) {
                    ContactosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.LookupEditor;
                    Q.initFormType(ContactosForm, [
                        'Nombre', w0,
                        'Apellidos', w0,
                        'Nif', w0,
                        'TelefonoFijo', w0,
                        'Movil', w0,
                        'IdiomaId', w1,
                        'Email', w0
                    ]);
                }
                return _this;
            }
            ContactosForm.formKey = 'Nuevo_Roezec.Contactos';
            return ContactosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.ContactosForm = ContactosForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContactosRow;
        (function (ContactosRow) {
            ContactosRow.idProperty = 'ContactoId';
            ContactosRow.nameProperty = 'Nombre';
            ContactosRow.localTextPrefix = 'Nuevo_Roezec.Contactos';
            ContactosRow.lookupKey = 'Nuevo_Roezec.Contactos';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Contactos');
            }
            ContactosRow.getLookup = getLookup;
            ContactosRow.deletePermission = 'Roezec:Modify';
            ContactosRow.insertPermission = 'Roezec:Modify';
            ContactosRow.readPermission = 'Roezec:Read';
            ContactosRow.updatePermission = 'Roezec:Modify';
        })(ContactosRow = Nuevo_Roezec.ContactosRow || (Nuevo_Roezec.ContactosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContactosService;
        (function (ContactosService) {
            ContactosService.baseUrl = 'Nuevo_Roezec/Contactos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ContactosService[x] = function (r, s, o) {
                    return Q.serviceRequest(ContactosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(ContactosService = Nuevo_Roezec.ContactosService || (Nuevo_Roezec.ContactosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContinentesForm = /** @class */ (function (_super) {
            __extends(ContinentesForm, _super);
            function ContinentesForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ContinentesForm.init) {
                    ContinentesForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(ContinentesForm, [
                        'Continente', w0
                    ]);
                }
                return _this;
            }
            ContinentesForm.formKey = 'Nuevo_Roezec.Continentes';
            return ContinentesForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.ContinentesForm = ContinentesForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContinentesRow;
        (function (ContinentesRow) {
            ContinentesRow.idProperty = 'ContinenteId';
            ContinentesRow.nameProperty = 'Continente';
            ContinentesRow.localTextPrefix = 'Nuevo_Roezec.Continentes';
            ContinentesRow.lookupKey = 'Nuevo_Roezec.Continentes';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Continentes');
            }
            ContinentesRow.getLookup = getLookup;
            ContinentesRow.deletePermission = 'Roezec:Admin';
            ContinentesRow.insertPermission = 'Roezec:Admin';
            ContinentesRow.readPermission = 'Roezec:Read';
            ContinentesRow.updatePermission = 'Roezec:Admin';
        })(ContinentesRow = Nuevo_Roezec.ContinentesRow || (Nuevo_Roezec.ContinentesRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContinentesService;
        (function (ContinentesService) {
            ContinentesService.baseUrl = 'Nuevo_Roezec/Continentes';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ContinentesService[x] = function (r, s, o) {
                    return Q.serviceRequest(ContinentesService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(ContinentesService = Nuevo_Roezec.ContinentesService || (Nuevo_Roezec.ContinentesService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosForm = /** @class */ (function (_super) {
            __extends(EmpresasContactosForm, _super);
            function EmpresasContactosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EmpresasContactosForm.init) {
                    EmpresasContactosForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    var w1 = s.DateEditor;
                    Q.initFormType(EmpresasContactosForm, [
                        'ContactoId', w0,
                        'TipoContactoId', w0,
                        'FechaAlta', w1,
                        'FechaBaja', w1
                    ]);
                }
                return _this;
            }
            EmpresasContactosForm.formKey = 'Nuevo_Roezec.EmpresasContactos';
            return EmpresasContactosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EmpresasContactosForm = EmpresasContactosForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosReadOnlyForm = /** @class */ (function (_super) {
            __extends(EmpresasContactosReadOnlyForm, _super);
            function EmpresasContactosReadOnlyForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            EmpresasContactosReadOnlyForm.formKey = 'Nuevo_Roezec.EmpresasContactosReadOnly';
            return EmpresasContactosReadOnlyForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EmpresasContactosReadOnlyForm = EmpresasContactosReadOnlyForm;
        [, ['EmpresaId', function () { return Serenity.IntegerEditor; }],
            ['ContactoId', function () { return Serenity.IntegerEditor; }],
            ['TipoContactoId', function () { return Serenity.IntegerEditor; }],
            ['FechaAlta', function () { return Serenity.DateEditor; }],
            ['FechaBaja', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(EmpresasContactosReadOnlyForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosReadOnlyRow;
        (function (EmpresasContactosReadOnlyRow) {
            EmpresasContactosReadOnlyRow.idProperty = 'EmpresaContactoId';
            EmpresasContactosReadOnlyRow.localTextPrefix = 'Nuevo_Roezec.EmpresasContactosReadOnly';
            EmpresasContactosReadOnlyRow.deletePermission = 'Roezec:General';
            EmpresasContactosReadOnlyRow.insertPermission = 'Roezec:General';
            EmpresasContactosReadOnlyRow.readPermission = 'Roezec:General';
            EmpresasContactosReadOnlyRow.updatePermission = 'Roezec:General';
            var Fields;
            (function (Fields) {
            })(Fields = EmpresasContactosReadOnlyRow.Fields || (EmpresasContactosReadOnlyRow.Fields = {}));
            [
                'EmpresaContactoId',
                'EmpresaId',
                'ContactoId',
                'TipoContactoId',
                'FechaAlta',
                'FechaBaja',
                'EmpresaRazon',
                'EmpresaFormaJuridicaId',
                'EmpresaTecnicoId',
                'EmpresaCif',
                'EmpresaDireccion',
                'EmpresaPoblacion',
                'EmpresaCp',
                'EmpresaIslaId',
                'EmpresaTelefonoFijo',
                'EmpresaMovil',
                'EmpresaEmail',
                'EmpresaProyectoId',
                'EmpresaExpediente',
                'EmpresaMotivoExencion',
                'EmpresaTipologiaCapitalId',
                'EmpresaTipoGarantiaTasaId',
                'EmpresaEmpleoTraspasado',
                'EmpresaEmpleo6Meses',
                'EmpresaEmpleoPromedio',
                'EmpresaEmpleoPromedio2Anos',
                'EmpresaInversionTraspasada',
                'EmpresaInversion2Anos',
                'EmpresaEstadoEmpresaId',
                'EmpresaFechaCambioEstado',
                'EmpresaNumTasaLiquidacion',
                'ContactoNombre',
                'ContactoApellidos',
                'ContactoNif',
                'ContactoTelefonoFijo',
                'ContactoMovil',
                'ContactoIdiomaId',
                'ContactoEmail',
                'TipoContactoContacto'
            ].forEach(function (x) { return Fields[x] = x; });
        })(EmpresasContactosReadOnlyRow = Nuevo_Roezec.EmpresasContactosReadOnlyRow || (Nuevo_Roezec.EmpresasContactosReadOnlyRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosReadOnlyService;
        (function (EmpresasContactosReadOnlyService) {
            EmpresasContactosReadOnlyService.baseUrl = 'Nuevo_Roezec/EmpresasContactosReadOnly';
            var Methods;
            (function (Methods) {
            })(Methods = EmpresasContactosReadOnlyService.Methods || (EmpresasContactosReadOnlyService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpresasContactosReadOnlyService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpresasContactosReadOnlyService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = EmpresasContactosReadOnlyService.baseUrl + '/' + x;
            });
        })(EmpresasContactosReadOnlyService = Nuevo_Roezec.EmpresasContactosReadOnlyService || (Nuevo_Roezec.EmpresasContactosReadOnlyService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosRow;
        (function (EmpresasContactosRow) {
            EmpresasContactosRow.idProperty = 'EmpresaContactoId';
            EmpresasContactosRow.nameProperty = 'Fullname';
            EmpresasContactosRow.localTextPrefix = 'Nuevo_Roezec.EmpresasContactos';
            EmpresasContactosRow.deletePermission = 'Roezec:Modify';
            EmpresasContactosRow.insertPermission = 'Roezec:Modify';
            EmpresasContactosRow.readPermission = 'Roezec:Read';
            EmpresasContactosRow.updatePermission = 'Roezec:Modify';
        })(EmpresasContactosRow = Nuevo_Roezec.EmpresasContactosRow || (Nuevo_Roezec.EmpresasContactosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosService;
        (function (EmpresasContactosService) {
            EmpresasContactosService.baseUrl = 'Nuevo_Roezec/EmpresasContactos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpresasContactosService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpresasContactosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EmpresasContactosService = Nuevo_Roezec.EmpresasContactosService || (Nuevo_Roezec.EmpresasContactosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasDireccionesForm = /** @class */ (function (_super) {
            __extends(EmpresasDireccionesForm, _super);
            function EmpresasDireccionesForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            EmpresasDireccionesForm.formKey = 'Nuevo_Roezec.EmpresasDirecciones';
            return EmpresasDireccionesForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EmpresasDireccionesForm = EmpresasDireccionesForm;
        [, ['EmpresaId', function () { return Serenity.IntegerEditor; }],
            ['Direccion', function () { return Serenity.StringEditor; }],
            ['Poblacion', function () { return Serenity.StringEditor; }],
            ['Cp', function () { return Serenity.StringEditor; }],
            ['IslaId', function () { return Serenity.IntegerEditor; }],
            ['Desde', function () { return Serenity.DateEditor; }],
            ['Hasta', function () { return Serenity.DateEditor; }],
            ['UserId', function () { return Serenity.IntegerEditor; }],
            ['FechaModificacion', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(EmpresasDireccionesForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasDireccionesRow;
        (function (EmpresasDireccionesRow) {
            EmpresasDireccionesRow.idProperty = 'EmpresasDireccionesId';
            EmpresasDireccionesRow.nameProperty = 'Direccion';
            EmpresasDireccionesRow.localTextPrefix = 'Nuevo_Roezec.EmpresasDirecciones';
            EmpresasDireccionesRow.deletePermission = 'Roezec:Delete';
            EmpresasDireccionesRow.insertPermission = 'Roezec:Modify';
            EmpresasDireccionesRow.readPermission = 'Roezec:Read';
            EmpresasDireccionesRow.updatePermission = 'Roezec:Modify';
            var Fields;
            (function (Fields) {
            })(Fields = EmpresasDireccionesRow.Fields || (EmpresasDireccionesRow.Fields = {}));
            [
                'EmpresasDireccionesId',
                'EmpresaId',
                'Direccion',
                'Poblacion',
                'Cp',
                'IslaId',
                'Desde',
                'Hasta',
                'UserId',
                'UserName',
                'DisplayName',
                'FechaModificacion',
                'EmpresaRazon',
                'EmpresaDescripcion',
                'EmpresaFormaJuridicaId',
                'EmpresaTecnicoId',
                'EmpresaCif',
                'EmpresaDireccion',
                'EmpresaPoblacion',
                'EmpresaCp',
                'EmpresaIslaId',
                'EmpresaTelefonoFijo',
                'EmpresaMovil',
                'EmpresaCapitalSocial',
                'EmpresaEmail',
                'EmpresaProyectoId',
                'EmpresaNumeroRoezec',
                'EmpresaExpediente',
                'EmpresaMotivoExencion',
                'EmpresaTipologiaCapitalId',
                'EmpresaTipoGarantiaTasaId',
                'EmpresaEmpleoTraspasado',
                'EmpresaEmpleo6Meses',
                'EmpresaEmpleoPromedio',
                'EmpresaEmpleoPromedio2Anos',
                'EmpresaInversionTraspasada',
                'EmpresaInversion2Anos',
                'EmpresaEstadoEmpresaId',
                'EmpresaFechaAlta',
                'EmpresaFechaCambioEstado',
                'EmpresaNumTasaLiquidacion',
                'EmpresaUserId',
                'EmpresaFechaModificacion',
                'IslaNombreIsla',
                'IslaCodigo',
                'Isla',
                'IslaUserId',
                'IslaFechaModificacion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(EmpresasDireccionesRow = Nuevo_Roezec.EmpresasDireccionesRow || (Nuevo_Roezec.EmpresasDireccionesRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasDireccionesService;
        (function (EmpresasDireccionesService) {
            EmpresasDireccionesService.baseUrl = 'Nuevo_Roezec/EmpresasDirecciones';
            var Methods;
            (function (Methods) {
            })(Methods = EmpresasDireccionesService.Methods || (EmpresasDireccionesService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpresasDireccionesService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpresasDireccionesService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = EmpresasDireccionesService.baseUrl + '/' + x;
            });
        })(EmpresasDireccionesService = Nuevo_Roezec.EmpresasDireccionesService || (Nuevo_Roezec.EmpresasDireccionesService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasEmpleosForm = /** @class */ (function (_super) {
            __extends(EmpresasEmpleosForm, _super);
            function EmpresasEmpleosForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            EmpresasEmpleosForm.formKey = 'Nuevo_Roezec.EmpresasEmpleos';
            return EmpresasEmpleosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EmpresasEmpleosForm = EmpresasEmpleosForm;
        [, ['EmpresaId', function () { return Serenity.IntegerEditor; }],
            ['Any', function () { return Serenity.IntegerEditor; }],
            ['Empleos', function () { return Serenity.IntegerEditor; }],
            ['UserId', function () { return Serenity.IntegerEditor; }],
            ['FechaModificacion', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(EmpresasEmpleosForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasEmpleosRow;
        (function (EmpresasEmpleosRow) {
            EmpresasEmpleosRow.idProperty = 'EmpresasEmpleosId';
            EmpresasEmpleosRow.nameProperty = 'EmpresaRazon';
            EmpresasEmpleosRow.localTextPrefix = 'Nuevo_Roezec.EmpresasEmpleos';
            EmpresasEmpleosRow.deletePermission = 'Roezec:Delete';
            EmpresasEmpleosRow.insertPermission = 'Roezec:Modify';
            EmpresasEmpleosRow.readPermission = 'Roezec:Read';
            EmpresasEmpleosRow.updatePermission = 'Roezec:Modify';
            var Fields;
            (function (Fields) {
            })(Fields = EmpresasEmpleosRow.Fields || (EmpresasEmpleosRow.Fields = {}));
            [
                'EmpresasEmpleosId',
                'EmpresaId',
                'Any',
                'Empleos',
                'UserId',
                'FechaModificacion',
                'EmpresaRazon',
                'EmpresaDescripcion',
                'EmpresaFormaJuridicaId',
                'EmpresaTecnicoId',
                'EmpresaCif',
                'EmpresaDireccion',
                'EmpresaPoblacion',
                'EmpresaCp',
                'EmpresaIslaId',
                'EmpresaTelefonoFijo',
                'EmpresaMovil',
                'EmpresaCapitalSocial',
                'EmpresaEmail',
                'EmpresaProyectoId',
                'EmpresaNumeroRoezec',
                'EmpresaExpediente',
                'EmpresaMotivoExencion',
                'EmpresaTipologiaCapitalId',
                'EmpresaTipoGarantiaTasaId',
                'EmpresaEmpleoTraspasado',
                'EmpresaEmpleo6Meses',
                'EmpresaEmpleoPromedio',
                'EmpresaEmpleoPromedio2Anos',
                'EmpresaInversionTraspasada',
                'EmpresaInversion2Anos',
                'EmpresaEstadoEmpresaId',
                'EmpresaFechaAlta',
                'EmpresaFechaCambioEstado',
                'EmpresaNumTasaLiquidacion',
                'EmpresaUserId',
                'EmpresaFechaModificacion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(EmpresasEmpleosRow = Nuevo_Roezec.EmpresasEmpleosRow || (Nuevo_Roezec.EmpresasEmpleosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasEmpleosService;
        (function (EmpresasEmpleosService) {
            EmpresasEmpleosService.baseUrl = 'Nuevo_Roezec/EmpresasEmpleos';
            var Methods;
            (function (Methods) {
            })(Methods = EmpresasEmpleosService.Methods || (EmpresasEmpleosService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpresasEmpleosService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpresasEmpleosService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = EmpresasEmpleosService.baseUrl + '/' + x;
            });
        })(EmpresasEmpleosService = Nuevo_Roezec.EmpresasEmpleosService || (Nuevo_Roezec.EmpresasEmpleosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasFicherosForm = /** @class */ (function (_super) {
            __extends(EmpresasFicherosForm, _super);
            function EmpresasFicherosForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            EmpresasFicherosForm.formKey = 'Nuevo_Roezec.EmpresasFicheros';
            return EmpresasFicherosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EmpresasFicherosForm = EmpresasFicherosForm;
        [, ['EmpresaId', function () { return Serenity.IntegerEditor; }],
            ['Fichero', function () { return Serenity.StringEditor; }],
            ['Nombre', function () { return Serenity.StringEditor; }],
            ['Observaciones', function () { return Serenity.StringEditor; }],
            ['UserId', function () { return Serenity.IntegerEditor; }],
            ['FechaModificacion', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(EmpresasFicherosForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasFicherosRow;
        (function (EmpresasFicherosRow) {
            EmpresasFicherosRow.idProperty = 'EmpresasFicheroId';
            EmpresasFicherosRow.nameProperty = 'Nombre';
            EmpresasFicherosRow.localTextPrefix = 'Nuevo_Roezec.EmpresasFicheros';
            EmpresasFicherosRow.deletePermission = 'Roezec:Modify';
            EmpresasFicherosRow.insertPermission = 'Roezec:Insert';
            EmpresasFicherosRow.readPermission = 'Roezec:Read';
            EmpresasFicherosRow.updatePermission = 'Roezec:Modify';
            var Fields;
            (function (Fields) {
            })(Fields = EmpresasFicherosRow.Fields || (EmpresasFicherosRow.Fields = {}));
            [
                'EmpresasFicheroId',
                'EmpresaId',
                'Fichero',
                'Nombre',
                'Observaciones',
                'UserId',
                'FechaModificacion',
                'EmpresaRazon',
                'EmpresaDescripcion',
                'EmpresaFormaJuridicaId',
                'EmpresaTecnicoId',
                'EmpresaCif',
                'EmpresaDireccion',
                'EmpresaPoblacion',
                'EmpresaCp',
                'EmpresaIslaId',
                'EmpresaTelefonoFijo',
                'EmpresaMovil',
                'EmpresaCapitalSocial',
                'EmpresaEmail',
                'EmpresaProyectoId',
                'EmpresaNumeroRoezec',
                'EmpresaExpediente',
                'EmpresaMotivoExencion',
                'EmpresaTipologiaCapitalId',
                'EmpresaTipoGarantiaTasaId',
                'EmpresaEmpleoTraspasado',
                'EmpresaEmpleo6Meses',
                'EmpresaEmpleoPromedio',
                'EmpresaEmpleoPromedio2Anos',
                'EmpresaInversionTraspasada',
                'EmpresaInversion2Anos',
                'EmpresaEstadoEmpresaId',
                'EmpresaFechaAlta',
                'EmpresaFechaCambioEstado',
                'EmpresaNumTasaLiquidacion',
                'EmpresaUserId',
                'EmpresaFechaModificacion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(EmpresasFicherosRow = Nuevo_Roezec.EmpresasFicherosRow || (Nuevo_Roezec.EmpresasFicherosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasFicherosService;
        (function (EmpresasFicherosService) {
            EmpresasFicherosService.baseUrl = 'Nuevo_Roezec/EmpresasFicheros';
            var Methods;
            (function (Methods) {
            })(Methods = EmpresasFicherosService.Methods || (EmpresasFicherosService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpresasFicherosService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpresasFicherosService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = EmpresasFicherosService.baseUrl + '/' + x;
            });
        })(EmpresasFicherosService = Nuevo_Roezec.EmpresasFicherosService || (Nuevo_Roezec.EmpresasFicherosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasForm = /** @class */ (function (_super) {
            __extends(EmpresasForm, _super);
            function EmpresasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EmpresasForm.init) {
                    EmpresasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.LookupEditor;
                    var w2 = s.DateEditor;
                    var w3 = s.EmailEditor;
                    var w4 = s.IntegerEditor;
                    var w5 = s.DecimalEditor;
                    var w6 = Nuevo_Roezec.EmpresasNaceEditor;
                    var w7 = Nuevo_Roezec.HistorialEmpresasEditor;
                    var w8 = Nuevo_Roezec.EmpresasContactosEditor;
                    var w9 = Nuevo_Roezec.ProcedenciaCapitalEditor;
                    var w10 = Nuevo_Roezec.EmpresasMercadosEditor;
                    Q.initFormType(EmpresasForm, [
                        'Razon', w0,
                        'FormaJuridicaId', w1,
                        'TecnicoId', w1,
                        'Cif', w0,
                        'Expediente', w0,
                        'MotivoExencion', w0,
                        'TipologiaCapitalId', w1,
                        'TipoGarantiaTasaId', w1,
                        'EstadoEmpresaId', w1,
                        'FechaCambioEstado', w2,
                        'Direccion', w0,
                        'Poblacion', w0,
                        'Cp', w0,
                        'IslaId', w1,
                        'TelefonoFijo', w0,
                        'Movil', w0,
                        'Email', w3,
                        'EmpleoTraspasado', w4,
                        'Empleo6Meses', w4,
                        'EmpleoPromedio', w4,
                        'EmpleoPromedio2Anos', w4,
                        'InversionTraspasada', w5,
                        'Inversion2Anos', w5,
                        'NumTasaLiquidacion', w0,
                        'NacesList', w6,
                        'HistorialList', w7,
                        'ContactosList', w8,
                        'CapitalList', w9,
                        'MercadosList', w10
                    ]);
                }
                return _this;
            }
            EmpresasForm.formKey = 'Nuevo_Roezec.Empresas';
            return EmpresasForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EmpresasForm = EmpresasForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasMercadosForm = /** @class */ (function (_super) {
            __extends(EmpresasMercadosForm, _super);
            function EmpresasMercadosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EmpresasMercadosForm.init) {
                    EmpresasMercadosForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    Q.initFormType(EmpresasMercadosForm, [
                        'MercadoId', w0
                    ]);
                }
                return _this;
            }
            EmpresasMercadosForm.formKey = 'Nuevo_Roezec.EmpresasMercados';
            return EmpresasMercadosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EmpresasMercadosForm = EmpresasMercadosForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasMercadosRow;
        (function (EmpresasMercadosRow) {
            EmpresasMercadosRow.idProperty = 'EmpresaMercadoId';
            EmpresasMercadosRow.nameProperty = 'EmpresaRazon';
            EmpresasMercadosRow.localTextPrefix = 'Nuevo_Roezec.EmpresasMercados';
            EmpresasMercadosRow.deletePermission = 'Roezec:Delete';
            EmpresasMercadosRow.insertPermission = 'Roezec:Modify';
            EmpresasMercadosRow.readPermission = 'Roezec:Read';
            EmpresasMercadosRow.updatePermission = 'Roezec:Modify';
        })(EmpresasMercadosRow = Nuevo_Roezec.EmpresasMercadosRow || (Nuevo_Roezec.EmpresasMercadosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasMercadosService;
        (function (EmpresasMercadosService) {
            EmpresasMercadosService.baseUrl = 'Nuevo_Roezec/EmpresasMercados';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpresasMercadosService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpresasMercadosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EmpresasMercadosService = Nuevo_Roezec.EmpresasMercadosService || (Nuevo_Roezec.EmpresasMercadosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNaceForm = /** @class */ (function (_super) {
            __extends(EmpresasNaceForm, _super);
            function EmpresasNaceForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EmpresasNaceForm.init) {
                    EmpresasNaceForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    Q.initFormType(EmpresasNaceForm, [
                        'NaceId', w0
                    ]);
                }
                return _this;
            }
            EmpresasNaceForm.formKey = 'Nuevo_Roezec.EmpresasNace';
            return EmpresasNaceForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EmpresasNaceForm = EmpresasNaceForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNaceRow;
        (function (EmpresasNaceRow) {
            EmpresasNaceRow.idProperty = 'EmpresaNaceId';
            EmpresasNaceRow.nameProperty = 'NaceDescripcion';
            EmpresasNaceRow.localTextPrefix = 'Nuevo_Roezec.EmpresasNace';
            EmpresasNaceRow.deletePermission = 'Roezec:Modify';
            EmpresasNaceRow.insertPermission = 'Roezec:Modify';
            EmpresasNaceRow.readPermission = 'Roezec:Read';
            EmpresasNaceRow.updatePermission = 'Roezec:Modify';
        })(EmpresasNaceRow = Nuevo_Roezec.EmpresasNaceRow || (Nuevo_Roezec.EmpresasNaceRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNaceService;
        (function (EmpresasNaceService) {
            EmpresasNaceService.baseUrl = 'Nuevo_Roezec/EmpresasNace';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpresasNaceService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpresasNaceService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EmpresasNaceService = Nuevo_Roezec.EmpresasNaceService || (Nuevo_Roezec.EmpresasNaceService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNombresForm = /** @class */ (function (_super) {
            __extends(EmpresasNombresForm, _super);
            function EmpresasNombresForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            EmpresasNombresForm.formKey = 'Nuevo_Roezec.EmpresasNombres';
            return EmpresasNombresForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EmpresasNombresForm = EmpresasNombresForm;
        [, ['EmpresaId', function () { return Serenity.IntegerEditor; }],
            ['Nombre', function () { return Serenity.StringEditor; }],
            ['UserId', function () { return Serenity.IntegerEditor; }],
            ['FechaModificacion', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(EmpresasNombresForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNombresRow;
        (function (EmpresasNombresRow) {
            EmpresasNombresRow.idProperty = 'NombreId';
            EmpresasNombresRow.nameProperty = 'Nombre';
            EmpresasNombresRow.localTextPrefix = 'Nuevo_Roezec.EmpresasNombres';
            EmpresasNombresRow.deletePermission = 'Roezec_Old:Delete';
            EmpresasNombresRow.insertPermission = 'Roezec_Old:Insert';
            EmpresasNombresRow.readPermission = 'Roezec_Old:Read';
            EmpresasNombresRow.updatePermission = 'Roezec_Old:Modify';
            var Fields;
            (function (Fields) {
            })(Fields = EmpresasNombresRow.Fields || (EmpresasNombresRow.Fields = {}));
            [
                'NombreId',
                'EmpresaId',
                'Nombre',
                'UserId',
                'FechaModificacion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(EmpresasNombresRow = Nuevo_Roezec.EmpresasNombresRow || (Nuevo_Roezec.EmpresasNombresRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNombresService;
        (function (EmpresasNombresService) {
            EmpresasNombresService.baseUrl = 'Nuevo_Roezec/EmpresasNombres';
            var Methods;
            (function (Methods) {
            })(Methods = EmpresasNombresService.Methods || (EmpresasNombresService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpresasNombresService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpresasNombresService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = EmpresasNombresService.baseUrl + '/' + x;
            });
        })(EmpresasNombresService = Nuevo_Roezec.EmpresasNombresService || (Nuevo_Roezec.EmpresasNombresService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasRow;
        (function (EmpresasRow) {
            EmpresasRow.idProperty = 'EmpresaId';
            EmpresasRow.nameProperty = 'Razon';
            EmpresasRow.localTextPrefix = 'Nuevo_Roezec.Empresas';
            EmpresasRow.lookupKey = 'Nuevo_Roezec.Empresas';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Empresas');
            }
            EmpresasRow.getLookup = getLookup;
            EmpresasRow.deletePermission = 'Roezec:Admin';
            EmpresasRow.insertPermission = 'Roezec:Insert';
            EmpresasRow.readPermission = 'Roezec:Read';
            EmpresasRow.updatePermission = 'Roezec:Modify';
        })(EmpresasRow = Nuevo_Roezec.EmpresasRow || (Nuevo_Roezec.EmpresasRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasService;
        (function (EmpresasService) {
            EmpresasService.baseUrl = 'Nuevo_Roezec/Empresas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpresasService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpresasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EmpresasService = Nuevo_Roezec.EmpresasService || (Nuevo_Roezec.EmpresasService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosForm = /** @class */ (function (_super) {
            __extends(EnviosForm, _super);
            function EnviosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EnviosForm.init) {
                    EnviosForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    var w1 = s.DateEditor;
                    Q.initFormType(EnviosForm, [
                        'TipoEnvioId', w0,
                        'FormaEnvioId', w0,
                        'EstadoEnvioId', w0,
                        'FechaEnvio', w1,
                        'ContactoEnvioId', w0,
                        'FechaRecepcion', w1,
                        'ContactoAcuseId', w0
                    ]);
                }
                return _this;
            }
            EnviosForm.formKey = 'Nuevo_Roezec.Envios';
            return EnviosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EnviosForm = EnviosForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosProcedimientoForm = /** @class */ (function (_super) {
            __extends(EnviosProcedimientoForm, _super);
            function EnviosProcedimientoForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EnviosProcedimientoForm.init) {
                    EnviosProcedimientoForm.init = true;
                    var s = Serenity;
                    var w0 = s.IntegerEditor;
                    var w1 = s.DateEditor;
                    Q.initFormType(EnviosProcedimientoForm, [
                        'HistorialId', w0,
                        'TipoEnvioId', w0,
                        'FechaEnvio', w1,
                        'ContactoEnvioId', w0,
                        'FechaRecepcion', w1,
                        'ContactoAcuseId', w0
                    ]);
                }
                return _this;
            }
            EnviosProcedimientoForm.formKey = 'Nuevo_Roezec.EnviosProcedimiento';
            return EnviosProcedimientoForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EnviosProcedimientoForm = EnviosProcedimientoForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosProcedimientoRow;
        (function (EnviosProcedimientoRow) {
            EnviosProcedimientoRow.idProperty = 'EnvioId';
            EnviosProcedimientoRow.localTextPrefix = 'Nuevo_Roezec.EnviosProcedimiento';
            EnviosProcedimientoRow.deletePermission = 'Roezec:Modify';
            EnviosProcedimientoRow.insertPermission = 'Roezec:Modify';
            EnviosProcedimientoRow.readPermission = 'Roezec:Read';
            EnviosProcedimientoRow.updatePermission = 'Roezec:Modify';
        })(EnviosProcedimientoRow = Nuevo_Roezec.EnviosProcedimientoRow || (Nuevo_Roezec.EnviosProcedimientoRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosProcedimientoService;
        (function (EnviosProcedimientoService) {
            EnviosProcedimientoService.baseUrl = 'Nuevo_Roezec/EnviosProcedimiento';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EnviosProcedimientoService[x] = function (r, s, o) {
                    return Q.serviceRequest(EnviosProcedimientoService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EnviosProcedimientoService = Nuevo_Roezec.EnviosProcedimientoService || (Nuevo_Roezec.EnviosProcedimientoService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosRow;
        (function (EnviosRow) {
            EnviosRow.idProperty = 'EnvioId';
            EnviosRow.nameProperty = 'ContactoEnvioFullName';
            EnviosRow.localTextPrefix = 'Nuevo_Roezec.Envios';
            EnviosRow.deletePermission = 'Roezec:Delete';
            EnviosRow.insertPermission = 'Roezec:Insert';
            EnviosRow.readPermission = 'Roezec:Read';
            EnviosRow.updatePermission = 'Roezec:Modify';
        })(EnviosRow = Nuevo_Roezec.EnviosRow || (Nuevo_Roezec.EnviosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosService;
        (function (EnviosService) {
            EnviosService.baseUrl = 'Nuevo_Roezec/Envios';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EnviosService[x] = function (r, s, o) {
                    return Q.serviceRequest(EnviosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EnviosService = Nuevo_Roezec.EnviosService || (Nuevo_Roezec.EnviosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEmpresaForm = /** @class */ (function (_super) {
            __extends(EstadosEmpresaForm, _super);
            function EstadosEmpresaForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EstadosEmpresaForm.init) {
                    EstadosEmpresaForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(EstadosEmpresaForm, [
                        'Estado', w0
                    ]);
                }
                return _this;
            }
            EstadosEmpresaForm.formKey = 'Nuevo_Roezec.EstadosEmpresa';
            return EstadosEmpresaForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EstadosEmpresaForm = EstadosEmpresaForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEmpresaRow;
        (function (EstadosEmpresaRow) {
            EstadosEmpresaRow.idProperty = 'EstadoEmpresaId';
            EstadosEmpresaRow.nameProperty = 'Estado';
            EstadosEmpresaRow.localTextPrefix = 'Nuevo_Roezec.EstadosEmpresa';
            EstadosEmpresaRow.lookupKey = 'Nuevo_Roezec.EstadosEmpresa';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.EstadosEmpresa');
            }
            EstadosEmpresaRow.getLookup = getLookup;
            EstadosEmpresaRow.deletePermission = 'Roezec:Admin';
            EstadosEmpresaRow.insertPermission = 'Roezec:Admin';
            EstadosEmpresaRow.readPermission = 'Roezec:Read';
            EstadosEmpresaRow.updatePermission = 'Roezec:Admin';
        })(EstadosEmpresaRow = Nuevo_Roezec.EstadosEmpresaRow || (Nuevo_Roezec.EstadosEmpresaRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEmpresaService;
        (function (EstadosEmpresaService) {
            EstadosEmpresaService.baseUrl = 'Nuevo_Roezec/EstadosEmpresa';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EstadosEmpresaService[x] = function (r, s, o) {
                    return Q.serviceRequest(EstadosEmpresaService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EstadosEmpresaService = Nuevo_Roezec.EstadosEmpresaService || (Nuevo_Roezec.EstadosEmpresaService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEnvioForm = /** @class */ (function (_super) {
            __extends(EstadosEnvioForm, _super);
            function EstadosEnvioForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!EstadosEnvioForm.init) {
                    EstadosEnvioForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(EstadosEnvioForm, [
                        'Estado', w0
                    ]);
                }
                return _this;
            }
            EstadosEnvioForm.formKey = 'Nuevo_Roezec.EstadosEnvio';
            return EstadosEnvioForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.EstadosEnvioForm = EstadosEnvioForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEnvioRow;
        (function (EstadosEnvioRow) {
            EstadosEnvioRow.idProperty = 'EstadoEnvioId';
            EstadosEnvioRow.nameProperty = 'Estado';
            EstadosEnvioRow.localTextPrefix = 'Nuevo_Roezec.EstadosEnvio';
            EstadosEnvioRow.lookupKey = 'Nuevo_Roezec.EstadosEnvio';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.EstadosEnvio');
            }
            EstadosEnvioRow.getLookup = getLookup;
            EstadosEnvioRow.deletePermission = 'Roezec:Admin';
            EstadosEnvioRow.insertPermission = 'Roezec:Admin';
            EstadosEnvioRow.readPermission = 'Roezec:Read';
            EstadosEnvioRow.updatePermission = 'Roezec:Admin';
        })(EstadosEnvioRow = Nuevo_Roezec.EstadosEnvioRow || (Nuevo_Roezec.EstadosEnvioRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEnvioService;
        (function (EstadosEnvioService) {
            EstadosEnvioService.baseUrl = 'Nuevo_Roezec/EstadosEnvio';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EstadosEnvioService[x] = function (r, s, o) {
                    return Q.serviceRequest(EstadosEnvioService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(EstadosEnvioService = Nuevo_Roezec.EstadosEnvioService || (Nuevo_Roezec.EstadosEnvioService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FicherosForm = /** @class */ (function (_super) {
            __extends(FicherosForm, _super);
            function FicherosForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            FicherosForm.formKey = 'Nuevo_Roezec.Ficheros';
            return FicherosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.FicherosForm = FicherosForm;
        [, ['HistorialId', function () { return Serenity.IntegerEditor; }],
            ['Fichero', function () { return Serenity.StringEditor; }],
            ['FechaModificacion', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(FicherosForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FicherosRow;
        (function (FicherosRow) {
            FicherosRow.idProperty = 'FicheroId';
            FicherosRow.nameProperty = 'Fichero';
            FicherosRow.localTextPrefix = 'Nuevo_Roezec.Ficheros';
            FicherosRow.deletePermission = 'Roezec:Delete';
            FicherosRow.insertPermission = 'Roezec:Insert';
            FicherosRow.readPermission = 'Roezec:Read';
            FicherosRow.updatePermission = 'Roezec:Modify';
            var Fields;
            (function (Fields) {
            })(Fields = FicherosRow.Fields || (FicherosRow.Fields = {}));
            [
                'FicheroId',
                'HistorialId',
                'Fichero',
                'FechaModificacion',
                'HistorialEmpresaId',
                'HistorialProcedimientoId',
                'HistorialFechaInicio',
                'HistorialFechaResolucion',
                'HistorialSentidoResolucion',
                'HistorialFechaEfecto',
                'HistorialAcuseInicio',
                'HistorialPersonaAcuseIncioId',
                'HistorialAcuseResolucion',
                'HistorialPersonaAcuseResolucionId',
                'HistorialObservaciones',
                'HistorialFicheros'
            ].forEach(function (x) { return Fields[x] = x; });
        })(FicherosRow = Nuevo_Roezec.FicherosRow || (Nuevo_Roezec.FicherosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FicherosService;
        (function (FicherosService) {
            FicherosService.baseUrl = 'Nuevo_Roezec/Ficheros';
            var Methods;
            (function (Methods) {
            })(Methods = FicherosService.Methods || (FicherosService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                FicherosService[x] = function (r, s, o) {
                    return Q.serviceRequest(FicherosService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = FicherosService.baseUrl + '/' + x;
            });
        })(FicherosService = Nuevo_Roezec.FicherosService || (Nuevo_Roezec.FicherosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasEnvioForm = /** @class */ (function (_super) {
            __extends(FormasEnvioForm, _super);
            function FormasEnvioForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!FormasEnvioForm.init) {
                    FormasEnvioForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(FormasEnvioForm, [
                        'Forma', w0
                    ]);
                }
                return _this;
            }
            FormasEnvioForm.formKey = 'Nuevo_Roezec.FormasEnvio';
            return FormasEnvioForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.FormasEnvioForm = FormasEnvioForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasEnvioRow;
        (function (FormasEnvioRow) {
            FormasEnvioRow.idProperty = 'FormaEnvioId';
            FormasEnvioRow.nameProperty = 'Forma';
            FormasEnvioRow.localTextPrefix = 'Nuevo_Roezec.FormasEnvio';
            FormasEnvioRow.lookupKey = 'Nuevo_Roezec.FormasEnvio';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.FormasEnvio');
            }
            FormasEnvioRow.getLookup = getLookup;
            FormasEnvioRow.deletePermission = 'Roezec:Admin';
            FormasEnvioRow.insertPermission = 'Roezec:Admin';
            FormasEnvioRow.readPermission = 'Roezec:Read';
            FormasEnvioRow.updatePermission = 'Roezec:Admin';
        })(FormasEnvioRow = Nuevo_Roezec.FormasEnvioRow || (Nuevo_Roezec.FormasEnvioRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasEnvioService;
        (function (FormasEnvioService) {
            FormasEnvioService.baseUrl = 'Nuevo_Roezec/FormasEnvio';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                FormasEnvioService[x] = function (r, s, o) {
                    return Q.serviceRequest(FormasEnvioService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(FormasEnvioService = Nuevo_Roezec.FormasEnvioService || (Nuevo_Roezec.FormasEnvioService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasJuridicasForm = /** @class */ (function (_super) {
            __extends(FormasJuridicasForm, _super);
            function FormasJuridicasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!FormasJuridicasForm.init) {
                    FormasJuridicasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(FormasJuridicasForm, [
                        'Juridica', w0
                    ]);
                }
                return _this;
            }
            FormasJuridicasForm.formKey = 'Nuevo_Roezec.FormasJuridicas';
            return FormasJuridicasForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.FormasJuridicasForm = FormasJuridicasForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasJuridicasRow;
        (function (FormasJuridicasRow) {
            FormasJuridicasRow.idProperty = 'JuridicaId';
            FormasJuridicasRow.nameProperty = 'Juridica';
            FormasJuridicasRow.localTextPrefix = 'Nuevo_Roezec.FormasJuridicas';
            FormasJuridicasRow.lookupKey = 'Nuevo_Roezec.FormasJuridicas';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.FormasJuridicas');
            }
            FormasJuridicasRow.getLookup = getLookup;
            FormasJuridicasRow.deletePermission = 'Roezec:Admin';
            FormasJuridicasRow.insertPermission = 'Roezec:Admin';
            FormasJuridicasRow.readPermission = 'Roezec:Read';
            FormasJuridicasRow.updatePermission = 'Roezec:Admin';
        })(FormasJuridicasRow = Nuevo_Roezec.FormasJuridicasRow || (Nuevo_Roezec.FormasJuridicasRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasJuridicasService;
        (function (FormasJuridicasService) {
            FormasJuridicasService.baseUrl = 'Nuevo_Roezec/FormasJuridicas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                FormasJuridicasService[x] = function (r, s, o) {
                    return Q.serviceRequest(FormasJuridicasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(FormasJuridicasService = Nuevo_Roezec.FormasJuridicasService || (Nuevo_Roezec.FormasJuridicasService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var HistorialEmpresasForm = /** @class */ (function (_super) {
            __extends(HistorialEmpresasForm, _super);
            function HistorialEmpresasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!HistorialEmpresasForm.init) {
                    HistorialEmpresasForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    var w1 = s.DateEditor;
                    var w2 = s.BooleanEditor;
                    var w3 = Nuevo_Roezec.EnviosEditor;
                    var w4 = s.TextAreaEditor;
                    var w5 = s.MultipleImageUploadEditor;
                    Q.initFormType(HistorialEmpresasForm, [
                        'ProcedimientoId', w0,
                        'FechaInicio', w1,
                        'FechaResolucion', w1,
                        'SentidoResolucion', w2,
                        'FechaEfecto', w1,
                        'EnviosList', w3,
                        'Observaciones', w4,
                        'Ficheros', w5
                    ]);
                }
                return _this;
            }
            HistorialEmpresasForm.formKey = 'Nuevo_Roezec.HistorialEmpresas';
            return HistorialEmpresasForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.HistorialEmpresasForm = HistorialEmpresasForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var HistorialEmpresasRow;
        (function (HistorialEmpresasRow) {
            HistorialEmpresasRow.idProperty = 'HistorialId';
            HistorialEmpresasRow.nameProperty = 'EmpresaRazon';
            HistorialEmpresasRow.localTextPrefix = 'Nuevo_Roezec.HistorialEmpresas';
            HistorialEmpresasRow.deletePermission = 'Roezec:Modify';
            HistorialEmpresasRow.insertPermission = 'Roezec:Modify';
            HistorialEmpresasRow.readPermission = 'Roezec:Read';
            HistorialEmpresasRow.updatePermission = 'Roezec:Modify';
        })(HistorialEmpresasRow = Nuevo_Roezec.HistorialEmpresasRow || (Nuevo_Roezec.HistorialEmpresasRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var HistorialEmpresasService;
        (function (HistorialEmpresasService) {
            HistorialEmpresasService.baseUrl = 'Nuevo_Roezec/HistorialEmpresas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                HistorialEmpresasService[x] = function (r, s, o) {
                    return Q.serviceRequest(HistorialEmpresasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(HistorialEmpresasService = Nuevo_Roezec.HistorialEmpresasService || (Nuevo_Roezec.HistorialEmpresasService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IdiomasForm = /** @class */ (function (_super) {
            __extends(IdiomasForm, _super);
            function IdiomasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!IdiomasForm.init) {
                    IdiomasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(IdiomasForm, [
                        'Idioma', w0,
                        'NombreIdioma', w0
                    ]);
                }
                return _this;
            }
            IdiomasForm.formKey = 'Nuevo_Roezec.Idiomas';
            return IdiomasForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.IdiomasForm = IdiomasForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IdiomasRow;
        (function (IdiomasRow) {
            IdiomasRow.idProperty = 'IdiomaId';
            IdiomasRow.nameProperty = 'NombreIdioma';
            IdiomasRow.localTextPrefix = 'Nuevo_Roezec.Idiomas';
            IdiomasRow.lookupKey = 'Nuevo_Roezec.Idiomas';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Idiomas');
            }
            IdiomasRow.getLookup = getLookup;
            IdiomasRow.deletePermission = 'Roezec:Modify';
            IdiomasRow.insertPermission = 'Roezec:Modify';
            IdiomasRow.readPermission = 'Roezec:Read';
            IdiomasRow.updatePermission = 'Roezec:Modify';
        })(IdiomasRow = Nuevo_Roezec.IdiomasRow || (Nuevo_Roezec.IdiomasRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IdiomasService;
        (function (IdiomasService) {
            IdiomasService.baseUrl = 'Nuevo_Roezec/Idiomas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                IdiomasService[x] = function (r, s, o) {
                    return Q.serviceRequest(IdiomasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(IdiomasService = Nuevo_Roezec.IdiomasService || (Nuevo_Roezec.IdiomasService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IslasForm = /** @class */ (function (_super) {
            __extends(IslasForm, _super);
            function IslasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!IslasForm.init) {
                    IslasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(IslasForm, [
                        'NombreIsla', w0,
                        'Isla', w0
                    ]);
                }
                return _this;
            }
            IslasForm.formKey = 'Nuevo_Roezec.Islas';
            return IslasForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.IslasForm = IslasForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IslasRow;
        (function (IslasRow) {
            IslasRow.idProperty = 'IslaId';
            IslasRow.nameProperty = 'NombreIsla';
            IslasRow.localTextPrefix = 'Nuevo_Roezec.Islas';
            IslasRow.lookupKey = 'Nuevo_Roezec.Islas';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Islas');
            }
            IslasRow.getLookup = getLookup;
            IslasRow.deletePermission = 'Roezec:Delete';
            IslasRow.insertPermission = 'Roezec:Insert';
            IslasRow.readPermission = 'Roezec:Read';
            IslasRow.updatePermission = 'Roezec:Modify';
        })(IslasRow = Nuevo_Roezec.IslasRow || (Nuevo_Roezec.IslasRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IslasService;
        (function (IslasService) {
            IslasService.baseUrl = 'Nuevo_Roezec/Islas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                IslasService[x] = function (r, s, o) {
                    return Q.serviceRequest(IslasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(IslasService = Nuevo_Roezec.IslasService || (Nuevo_Roezec.IslasService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosForm = /** @class */ (function (_super) {
            __extends(MercadosForm, _super);
            function MercadosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!MercadosForm.init) {
                    MercadosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(MercadosForm, [
                        'Mercado', w0
                    ]);
                }
                return _this;
            }
            MercadosForm.formKey = 'Nuevo_Roezec.Mercados';
            return MercadosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.MercadosForm = MercadosForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosReadOnlyForm = /** @class */ (function (_super) {
            __extends(MercadosReadOnlyForm, _super);
            function MercadosReadOnlyForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            MercadosReadOnlyForm.formKey = 'Nuevo_Roezec.MercadosReadOnly';
            return MercadosReadOnlyForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.MercadosReadOnlyForm = MercadosReadOnlyForm;
        [, ['EmpresaId', function () { return Serenity.IntegerEditor; }],
            ['MercadoId', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(MercadosReadOnlyForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosReadOnlyRow;
        (function (MercadosReadOnlyRow) {
            MercadosReadOnlyRow.idProperty = 'EmpresaMercadoId';
            MercadosReadOnlyRow.localTextPrefix = 'Nuevo_Roezec.MercadosReadOnly';
            MercadosReadOnlyRow.deletePermission = 'Roezec::General';
            MercadosReadOnlyRow.insertPermission = 'Roezec::General';
            MercadosReadOnlyRow.readPermission = 'Roezec::General';
            MercadosReadOnlyRow.updatePermission = 'Roezec::General';
            var Fields;
            (function (Fields) {
            })(Fields = MercadosReadOnlyRow.Fields || (MercadosReadOnlyRow.Fields = {}));
            [
                'EmpresaMercadoId',
                'EmpresaId',
                'MercadoId',
                'EmpresaRazon',
                'EmpresaFormaJuridicaId',
                'EmpresaTecnicoId',
                'EmpresaCif',
                'EmpresaDireccion',
                'EmpresaPoblacion',
                'EmpresaCp',
                'EmpresaIslaId',
                'EmpresaTelefonoFijo',
                'EmpresaMovil',
                'EmpresaEmail',
                'EmpresaProyectoId',
                'EmpresaExpediente',
                'EmpresaMotivoExencion',
                'EmpresaTipologiaCapitalId',
                'EmpresaTipoGarantiaTasaId',
                'EmpresaEmpleoTraspasado',
                'EmpresaEmpleo6Meses',
                'EmpresaEmpleoPromedio',
                'EmpresaEmpleoPromedio2Anos',
                'EmpresaInversionTraspasada',
                'EmpresaInversion2Anos',
                'EmpresaEstadoEmpresaId',
                'EmpresaFechaCambioEstado',
                'EmpresaNumTasaLiquidacion',
                'Mercado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(MercadosReadOnlyRow = Nuevo_Roezec.MercadosReadOnlyRow || (Nuevo_Roezec.MercadosReadOnlyRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosReadOnlyService;
        (function (MercadosReadOnlyService) {
            MercadosReadOnlyService.baseUrl = 'Nuevo_Roezec/MercadosReadOnly';
            var Methods;
            (function (Methods) {
            })(Methods = MercadosReadOnlyService.Methods || (MercadosReadOnlyService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                MercadosReadOnlyService[x] = function (r, s, o) {
                    return Q.serviceRequest(MercadosReadOnlyService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = MercadosReadOnlyService.baseUrl + '/' + x;
            });
        })(MercadosReadOnlyService = Nuevo_Roezec.MercadosReadOnlyService || (Nuevo_Roezec.MercadosReadOnlyService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosRow;
        (function (MercadosRow) {
            MercadosRow.idProperty = 'MercadoId';
            MercadosRow.nameProperty = 'Mercado';
            MercadosRow.localTextPrefix = 'Nuevo_Roezec.Mercados';
            MercadosRow.lookupKey = 'Nuevo_Roezec.Mercados';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Mercados');
            }
            MercadosRow.getLookup = getLookup;
            MercadosRow.deletePermission = 'Roezec:Delete';
            MercadosRow.insertPermission = 'Roezec:Modify';
            MercadosRow.readPermission = 'Roezec:Read';
            MercadosRow.updatePermission = 'Roezec:Modify';
        })(MercadosRow = Nuevo_Roezec.MercadosRow || (Nuevo_Roezec.MercadosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosService;
        (function (MercadosService) {
            MercadosService.baseUrl = 'Nuevo_Roezec/Mercados';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                MercadosService[x] = function (r, s, o) {
                    return Q.serviceRequest(MercadosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(MercadosService = Nuevo_Roezec.MercadosService || (Nuevo_Roezec.MercadosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MetadatosForm = /** @class */ (function (_super) {
            __extends(MetadatosForm, _super);
            function MetadatosForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            MetadatosForm.formKey = 'Nuevo_Roezec.Metadatos';
            return MetadatosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.MetadatosForm = MetadatosForm;
        [, ['FicheroId', function () { return Serenity.IntegerEditor; }],
            ['Nombre', function () { return Serenity.StringEditor; }],
            ['Valor', function () { return Serenity.StringEditor; }],
            ['FechaModificacion', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(MetadatosForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MetadatosRow;
        (function (MetadatosRow) {
            MetadatosRow.idProperty = 'MetadatoId';
            MetadatosRow.nameProperty = 'Nombre';
            MetadatosRow.localTextPrefix = 'Nuevo_Roezec.Metadatos';
            MetadatosRow.deletePermission = 'Roezec:General';
            MetadatosRow.insertPermission = 'Roezec:General';
            MetadatosRow.readPermission = 'Roezec:General';
            MetadatosRow.updatePermission = 'Roezec:General';
            var Fields;
            (function (Fields) {
            })(Fields = MetadatosRow.Fields || (MetadatosRow.Fields = {}));
            [
                'MetadatoId',
                'FicheroId',
                'Nombre',
                'Valor',
                'FechaModificacion',
                'FicheroHistorialId',
                'Fichero',
                'FicheroNombreNatural',
                'FicheroOrgano',
                'FicheroFechaCaptura',
                'FicheroTipoDocumentoId',
                'FicheroCsv',
                'FicheroRegulacionCsv',
                'FicheroObservaciones',
                'FicheroFechaModificacion',
                'FicheroUserId'
            ].forEach(function (x) { return Fields[x] = x; });
        })(MetadatosRow = Nuevo_Roezec.MetadatosRow || (Nuevo_Roezec.MetadatosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MetadatosService;
        (function (MetadatosService) {
            MetadatosService.baseUrl = 'Nuevo_Roezec/Metadatos';
            var Methods;
            (function (Methods) {
            })(Methods = MetadatosService.Methods || (MetadatosService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                MetadatosService[x] = function (r, s, o) {
                    return Q.serviceRequest(MetadatosService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = MetadatosService.baseUrl + '/' + x;
            });
        })(MetadatosService = Nuevo_Roezec.MetadatosService || (Nuevo_Roezec.MetadatosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var NacesForm = /** @class */ (function (_super) {
            __extends(NacesForm, _super);
            function NacesForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!NacesForm.init) {
                    NacesForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    var w1 = s.StringEditor;
                    Q.initFormType(NacesForm, [
                        'VersionId', w0,
                        'Codigo', w1,
                        'Descripcion', w1,
                        'SectorId', w0,
                        'SubsectorId', w0
                    ]);
                }
                return _this;
            }
            NacesForm.formKey = 'Nuevo_Roezec.Naces';
            return NacesForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.NacesForm = NacesForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var NacesRow;
        (function (NacesRow) {
            NacesRow.idProperty = 'NaceId';
            NacesRow.nameProperty = 'NaceLarga';
            NacesRow.localTextPrefix = 'Nuevo_Roezec.Naces';
            NacesRow.lookupKey = 'Nuevo_Roezec.Naces';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Naces');
            }
            NacesRow.getLookup = getLookup;
            NacesRow.deletePermission = 'Roezec:Modify';
            NacesRow.insertPermission = 'Roezec:Modify';
            NacesRow.readPermission = 'Roezec:Read';
            NacesRow.updatePermission = 'Roezec:Modify';
        })(NacesRow = Nuevo_Roezec.NacesRow || (Nuevo_Roezec.NacesRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var NacesService;
        (function (NacesService) {
            NacesService.baseUrl = 'Nuevo_Roezec/Naces';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                NacesService[x] = function (r, s, o) {
                    return Q.serviceRequest(NacesService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(NacesService = Nuevo_Roezec.NacesService || (Nuevo_Roezec.NacesService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PaisesForm = /** @class */ (function (_super) {
            __extends(PaisesForm, _super);
            function PaisesForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!PaisesForm.init) {
                    PaisesForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.IntegerEditor;
                    Q.initFormType(PaisesForm, [
                        'Pais', w0,
                        'Capital', w0,
                        'ContinenteId', w1
                    ]);
                }
                return _this;
            }
            PaisesForm.formKey = 'Nuevo_Roezec.Paises';
            return PaisesForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.PaisesForm = PaisesForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PaisesRow;
        (function (PaisesRow) {
            PaisesRow.idProperty = 'PaisId';
            PaisesRow.nameProperty = 'Pais';
            PaisesRow.localTextPrefix = 'Nuevo_Roezec.Paises';
            PaisesRow.lookupKey = 'Nuevo_Roezec.Paises';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Paises');
            }
            PaisesRow.getLookup = getLookup;
            PaisesRow.deletePermission = 'Roezec:Modify';
            PaisesRow.insertPermission = 'Roezec:Modify';
            PaisesRow.readPermission = 'Roezec:Read';
            PaisesRow.updatePermission = 'Roezec:Modify';
        })(PaisesRow = Nuevo_Roezec.PaisesRow || (Nuevo_Roezec.PaisesRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PaisesService;
        (function (PaisesService) {
            PaisesService.baseUrl = 'Nuevo_Roezec/Paises';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PaisesService[x] = function (r, s, o) {
                    return Q.serviceRequest(PaisesService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(PaisesService = Nuevo_Roezec.PaisesService || (Nuevo_Roezec.PaisesService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PlazosForm = /** @class */ (function (_super) {
            __extends(PlazosForm, _super);
            function PlazosForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PlazosForm.formKey = 'Nuevo_Roezec.Plazos';
            return PlazosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.PlazosForm = PlazosForm;
        [, ['Plazo', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(PlazosForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PlazosRow;
        (function (PlazosRow) {
            PlazosRow.idProperty = 'PlazoId';
            PlazosRow.nameProperty = 'Plazo';
            PlazosRow.localTextPrefix = 'Nuevo_Roezec.Plazos';
            PlazosRow.deletePermission = 'Roezec:Read';
            PlazosRow.insertPermission = 'Roezec:Read';
            PlazosRow.readPermission = 'Roezec:Read';
            PlazosRow.updatePermission = 'Roezec:Read';
            var Fields;
            (function (Fields) {
            })(Fields = PlazosRow.Fields || (PlazosRow.Fields = {}));
            [
                'PlazoId',
                'Plazo'
            ].forEach(function (x) { return Fields[x] = x; });
        })(PlazosRow = Nuevo_Roezec.PlazosRow || (Nuevo_Roezec.PlazosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PlazosService;
        (function (PlazosService) {
            PlazosService.baseUrl = 'Nuevo_Roezec/Plazos';
            var Methods;
            (function (Methods) {
            })(Methods = PlazosService.Methods || (PlazosService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PlazosService[x] = function (r, s, o) {
                    return Q.serviceRequest(PlazosService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = PlazosService.baseUrl + '/' + x;
            });
        })(PlazosService = Nuevo_Roezec.PlazosService || (Nuevo_Roezec.PlazosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedenciaCapitalForm = /** @class */ (function (_super) {
            __extends(ProcedenciaCapitalForm, _super);
            function ProcedenciaCapitalForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ProcedenciaCapitalForm.init) {
                    ProcedenciaCapitalForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    var w1 = s.DecimalEditor;
                    Q.initFormType(ProcedenciaCapitalForm, [
                        'PaisId', w0,
                        'Porcentaje', w1
                    ]);
                }
                return _this;
            }
            ProcedenciaCapitalForm.formKey = 'Nuevo_Roezec.ProcedenciaCapital';
            return ProcedenciaCapitalForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.ProcedenciaCapitalForm = ProcedenciaCapitalForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedenciaCapitalRow;
        (function (ProcedenciaCapitalRow) {
            ProcedenciaCapitalRow.idProperty = 'ProcedenciaId';
            ProcedenciaCapitalRow.nameProperty = 'EmpresaRazon';
            ProcedenciaCapitalRow.localTextPrefix = 'Nuevo_Roezec.ProcedenciaCapital';
            ProcedenciaCapitalRow.deletePermission = 'Roezec:Delete';
            ProcedenciaCapitalRow.insertPermission = 'Roezec:Insert';
            ProcedenciaCapitalRow.readPermission = 'Roezec:Read';
            ProcedenciaCapitalRow.updatePermission = 'Roezec:Modify';
        })(ProcedenciaCapitalRow = Nuevo_Roezec.ProcedenciaCapitalRow || (Nuevo_Roezec.ProcedenciaCapitalRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedenciaCapitalService;
        (function (ProcedenciaCapitalService) {
            ProcedenciaCapitalService.baseUrl = 'Nuevo_Roezec/ProcedenciaCapital';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ProcedenciaCapitalService[x] = function (r, s, o) {
                    return Q.serviceRequest(ProcedenciaCapitalService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(ProcedenciaCapitalService = Nuevo_Roezec.ProcedenciaCapitalService || (Nuevo_Roezec.ProcedenciaCapitalService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedimientosForm = /** @class */ (function (_super) {
            __extends(ProcedimientosForm, _super);
            function ProcedimientosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ProcedimientosForm.init) {
                    ProcedimientosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.IntegerEditor;
                    var w2 = s.LookupEditor;
                    Q.initFormType(ProcedimientosForm, [
                        'Procedimiento', w0,
                        'Caducidad_dias', w1,
                        'EstadoEmpresaId', w2
                    ]);
                }
                return _this;
            }
            ProcedimientosForm.formKey = 'Nuevo_Roezec.Procedimientos';
            return ProcedimientosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.ProcedimientosForm = ProcedimientosForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedimientosRow;
        (function (ProcedimientosRow) {
            ProcedimientosRow.idProperty = 'ProcedimientoId';
            ProcedimientosRow.nameProperty = 'NameField';
            ProcedimientosRow.localTextPrefix = 'Nuevo_Roezec.Procedimientos';
            ProcedimientosRow.lookupKey = 'Nuevo_Roezec.Procedimientos';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Procedimientos');
            }
            ProcedimientosRow.getLookup = getLookup;
            ProcedimientosRow.deletePermission = 'Roezec:Admin';
            ProcedimientosRow.insertPermission = 'Roezec:Admin';
            ProcedimientosRow.readPermission = 'Roezec:Read';
            ProcedimientosRow.updatePermission = 'Roezec:Admin';
        })(ProcedimientosRow = Nuevo_Roezec.ProcedimientosRow || (Nuevo_Roezec.ProcedimientosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedimientosService;
        (function (ProcedimientosService) {
            ProcedimientosService.baseUrl = 'Nuevo_Roezec/Procedimientos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ProcedimientosService[x] = function (r, s, o) {
                    return Q.serviceRequest(ProcedimientosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(ProcedimientosService = Nuevo_Roezec.ProcedimientosService || (Nuevo_Roezec.ProcedimientosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SectoresForm = /** @class */ (function (_super) {
            __extends(SectoresForm, _super);
            function SectoresForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!SectoresForm.init) {
                    SectoresForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(SectoresForm, [
                        'Sector', w0
                    ]);
                }
                return _this;
            }
            SectoresForm.formKey = 'Nuevo_Roezec.Sectores';
            return SectoresForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.SectoresForm = SectoresForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SectoresRow;
        (function (SectoresRow) {
            SectoresRow.idProperty = 'SectorId';
            SectoresRow.nameProperty = 'Sector';
            SectoresRow.localTextPrefix = 'Nuevo_Roezec.Sectores';
            SectoresRow.lookupKey = 'Nuevo_Roezec.Sectores';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Sectores');
            }
            SectoresRow.getLookup = getLookup;
            SectoresRow.deletePermission = 'Roezec:Modify';
            SectoresRow.insertPermission = 'Roezec:Modify';
            SectoresRow.readPermission = 'Roezec:Read';
            SectoresRow.updatePermission = 'Roezec:Modify';
        })(SectoresRow = Nuevo_Roezec.SectoresRow || (Nuevo_Roezec.SectoresRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SectoresService;
        (function (SectoresService) {
            SectoresService.baseUrl = 'Nuevo_Roezec/Sectores';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SectoresService[x] = function (r, s, o) {
                    return Q.serviceRequest(SectoresService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(SectoresService = Nuevo_Roezec.SectoresService || (Nuevo_Roezec.SectoresService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SentidosresolucionForm = /** @class */ (function (_super) {
            __extends(SentidosresolucionForm, _super);
            function SentidosresolucionForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            SentidosresolucionForm.formKey = 'Nuevo_Roezec.Sentidosresolucion';
            return SentidosresolucionForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.SentidosresolucionForm = SentidosresolucionForm;
        [, ['Sentido', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(SentidosresolucionForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SentidosresolucionRow;
        (function (SentidosresolucionRow) {
            SentidosresolucionRow.idProperty = 'SentidoResolucionId';
            SentidosresolucionRow.nameProperty = 'Sentido';
            SentidosresolucionRow.localTextPrefix = 'Nuevo_Roezec.Sentidosresolucion';
            SentidosresolucionRow.deletePermission = 'Roezec:Admin';
            SentidosresolucionRow.insertPermission = 'Roezec:Admin';
            SentidosresolucionRow.readPermission = 'Roezec:Read';
            SentidosresolucionRow.updatePermission = 'Roezec:Admin';
            var Fields;
            (function (Fields) {
            })(Fields = SentidosresolucionRow.Fields || (SentidosresolucionRow.Fields = {}));
            [
                'SentidoResolucionId',
                'Sentido'
            ].forEach(function (x) { return Fields[x] = x; });
        })(SentidosresolucionRow = Nuevo_Roezec.SentidosresolucionRow || (Nuevo_Roezec.SentidosresolucionRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SentidosresolucionService;
        (function (SentidosresolucionService) {
            SentidosresolucionService.baseUrl = 'Nuevo_Roezec/Sentidosresolucion';
            var Methods;
            (function (Methods) {
            })(Methods = SentidosresolucionService.Methods || (SentidosresolucionService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SentidosresolucionService[x] = function (r, s, o) {
                    return Q.serviceRequest(SentidosresolucionService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = SentidosresolucionService.baseUrl + '/' + x;
            });
        })(SentidosresolucionService = Nuevo_Roezec.SentidosresolucionService || (Nuevo_Roezec.SentidosresolucionService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SubsectoresForm = /** @class */ (function (_super) {
            __extends(SubsectoresForm, _super);
            function SubsectoresForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!SubsectoresForm.init) {
                    SubsectoresForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    var w1 = s.StringEditor;
                    Q.initFormType(SubsectoresForm, [
                        'SectorId', w0,
                        'Subsector', w1
                    ]);
                }
                return _this;
            }
            SubsectoresForm.formKey = 'Nuevo_Roezec.Subsectores';
            return SubsectoresForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.SubsectoresForm = SubsectoresForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SubsectoresRow;
        (function (SubsectoresRow) {
            SubsectoresRow.idProperty = 'SubsectorId';
            SubsectoresRow.nameProperty = 'Subsector';
            SubsectoresRow.localTextPrefix = 'Nuevo_Roezec.Subsectores';
            SubsectoresRow.lookupKey = 'Nuevo_Roezec.Subsectores';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Subsectores');
            }
            SubsectoresRow.getLookup = getLookup;
            SubsectoresRow.deletePermission = 'Roezec:Admin';
            SubsectoresRow.insertPermission = 'Roezec:Admin';
            SubsectoresRow.readPermission = 'Roezec:Read';
            SubsectoresRow.updatePermission = 'Roezec:Admin';
        })(SubsectoresRow = Nuevo_Roezec.SubsectoresRow || (Nuevo_Roezec.SubsectoresRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SubsectoresService;
        (function (SubsectoresService) {
            SubsectoresService.baseUrl = 'Nuevo_Roezec/Subsectores';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SubsectoresService[x] = function (r, s, o) {
                    return Q.serviceRequest(SubsectoresService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(SubsectoresService = Nuevo_Roezec.SubsectoresService || (Nuevo_Roezec.SubsectoresService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TecnicosForm = /** @class */ (function (_super) {
            __extends(TecnicosForm, _super);
            function TecnicosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!TecnicosForm.init) {
                    TecnicosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(TecnicosForm, [
                        'NombreTecnico', w0,
                        'Tecnico', w0
                    ]);
                }
                return _this;
            }
            TecnicosForm.formKey = 'Nuevo_Roezec.Tecnicos';
            return TecnicosForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.TecnicosForm = TecnicosForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TecnicosRow;
        (function (TecnicosRow) {
            TecnicosRow.idProperty = 'TecnicoId';
            TecnicosRow.nameProperty = 'NombreTecnico';
            TecnicosRow.localTextPrefix = 'Nuevo_Roezec.Tecnicos';
            TecnicosRow.lookupKey = 'Nuevo_Roezec.Tecnicos';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.Tecnicos');
            }
            TecnicosRow.getLookup = getLookup;
            TecnicosRow.deletePermission = 'Roezec:Admin';
            TecnicosRow.insertPermission = 'Roezec:Admin';
            TecnicosRow.readPermission = 'Roezec:Read';
            TecnicosRow.updatePermission = 'Roezec:Admin';
        })(TecnicosRow = Nuevo_Roezec.TecnicosRow || (Nuevo_Roezec.TecnicosRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TecnicosService;
        (function (TecnicosService) {
            TecnicosService.baseUrl = 'Nuevo_Roezec/Tecnicos';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TecnicosService[x] = function (r, s, o) {
                    return Q.serviceRequest(TecnicosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TecnicosService = Nuevo_Roezec.TecnicosService || (Nuevo_Roezec.TecnicosService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TipologiasCapitalForm = /** @class */ (function (_super) {
            __extends(TipologiasCapitalForm, _super);
            function TipologiasCapitalForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!TipologiasCapitalForm.init) {
                    TipologiasCapitalForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(TipologiasCapitalForm, [
                        'Capital', w0
                    ]);
                }
                return _this;
            }
            TipologiasCapitalForm.formKey = 'Nuevo_Roezec.TipologiasCapital';
            return TipologiasCapitalForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.TipologiasCapitalForm = TipologiasCapitalForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TipologiasCapitalRow;
        (function (TipologiasCapitalRow) {
            TipologiasCapitalRow.idProperty = 'CapitalId';
            TipologiasCapitalRow.nameProperty = 'Capital';
            TipologiasCapitalRow.localTextPrefix = 'Nuevo_Roezec.TipologiasCapital';
            TipologiasCapitalRow.lookupKey = 'Nuevo_Roezec.TipologiasCapital';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.TipologiasCapital');
            }
            TipologiasCapitalRow.getLookup = getLookup;
            TipologiasCapitalRow.deletePermission = 'Roezec:Admin';
            TipologiasCapitalRow.insertPermission = 'Roezec:Admin';
            TipologiasCapitalRow.readPermission = 'Roezec:Read';
            TipologiasCapitalRow.updatePermission = 'Roezec:Admin';
        })(TipologiasCapitalRow = Nuevo_Roezec.TipologiasCapitalRow || (Nuevo_Roezec.TipologiasCapitalRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TipologiasCapitalService;
        (function (TipologiasCapitalService) {
            TipologiasCapitalService.baseUrl = 'Nuevo_Roezec/TipologiasCapital';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TipologiasCapitalService[x] = function (r, s, o) {
                    return Q.serviceRequest(TipologiasCapitalService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TipologiasCapitalService = Nuevo_Roezec.TipologiasCapitalService || (Nuevo_Roezec.TipologiasCapitalService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposAlarmaForm = /** @class */ (function (_super) {
            __extends(TiposAlarmaForm, _super);
            function TiposAlarmaForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TiposAlarmaForm.formKey = 'Nuevo_Roezec.TiposAlarma';
            return TiposAlarmaForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.TiposAlarmaForm = TiposAlarmaForm;
        [, ['Texto', function () { return Serenity.StringEditor; }],
            ['DiasAviso', function () { return Serenity.IntegerEditor; }],
            ['Email', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TiposAlarmaForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposAlarmaRow;
        (function (TiposAlarmaRow) {
            TiposAlarmaRow.idProperty = 'TipoAlarmaId';
            TiposAlarmaRow.nameProperty = 'Texto';
            TiposAlarmaRow.localTextPrefix = 'Nuevo_Roezec.TiposAlarma';
            TiposAlarmaRow.deletePermission = 'Roezec:Admin';
            TiposAlarmaRow.insertPermission = 'Roezec:Admin';
            TiposAlarmaRow.readPermission = 'Roezec:Read';
            TiposAlarmaRow.updatePermission = 'Roezec:Admin';
            var Fields;
            (function (Fields) {
            })(Fields = TiposAlarmaRow.Fields || (TiposAlarmaRow.Fields = {}));
            [
                'TipoAlarmaId',
                'Texto',
                'DiasAviso',
                'Email'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TiposAlarmaRow = Nuevo_Roezec.TiposAlarmaRow || (Nuevo_Roezec.TiposAlarmaRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposAlarmaService;
        (function (TiposAlarmaService) {
            TiposAlarmaService.baseUrl = 'Nuevo_Roezec/TiposAlarma';
            var Methods;
            (function (Methods) {
            })(Methods = TiposAlarmaService.Methods || (TiposAlarmaService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposAlarmaService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposAlarmaService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TiposAlarmaService.baseUrl + '/' + x;
            });
        })(TiposAlarmaService = Nuevo_Roezec.TiposAlarmaService || (Nuevo_Roezec.TiposAlarmaService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposContactoForm = /** @class */ (function (_super) {
            __extends(TiposContactoForm, _super);
            function TiposContactoForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!TiposContactoForm.init) {
                    TiposContactoForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(TiposContactoForm, [
                        'Contacto', w0
                    ]);
                }
                return _this;
            }
            TiposContactoForm.formKey = 'Nuevo_Roezec.TiposContacto';
            return TiposContactoForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.TiposContactoForm = TiposContactoForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposContactoRow;
        (function (TiposContactoRow) {
            TiposContactoRow.idProperty = 'ContactoId';
            TiposContactoRow.nameProperty = 'Contacto';
            TiposContactoRow.localTextPrefix = 'Nuevo_Roezec.TiposContacto';
            TiposContactoRow.lookupKey = 'Nuevo_Roezec.TiposContacto';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.TiposContacto');
            }
            TiposContactoRow.getLookup = getLookup;
            TiposContactoRow.deletePermission = 'Roezec:Admin';
            TiposContactoRow.insertPermission = 'Roezec:Admin';
            TiposContactoRow.readPermission = 'Roezec:Read';
            TiposContactoRow.updatePermission = 'Roezec:Admin';
        })(TiposContactoRow = Nuevo_Roezec.TiposContactoRow || (Nuevo_Roezec.TiposContactoRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposContactoService;
        (function (TiposContactoService) {
            TiposContactoService.baseUrl = 'Nuevo_Roezec/TiposContacto';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposContactoService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposContactoService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TiposContactoService = Nuevo_Roezec.TiposContactoService || (Nuevo_Roezec.TiposContactoService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDireccionesForm = /** @class */ (function (_super) {
            __extends(TiposDireccionesForm, _super);
            function TiposDireccionesForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TiposDireccionesForm.formKey = 'Nuevo_Roezec.TiposDirecciones';
            return TiposDireccionesForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.TiposDireccionesForm = TiposDireccionesForm;
        [, ['TipoDireccion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TiposDireccionesForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDireccionesRow;
        (function (TiposDireccionesRow) {
            TiposDireccionesRow.idProperty = 'TipoDireccionId';
            TiposDireccionesRow.nameProperty = 'TipoDireccion';
            TiposDireccionesRow.localTextPrefix = 'Nuevo_Roezec.TiposDirecciones';
            TiposDireccionesRow.deletePermission = 'Roezec:Admin';
            TiposDireccionesRow.insertPermission = 'Roezec:Admin';
            TiposDireccionesRow.readPermission = 'Roezec:Read';
            TiposDireccionesRow.updatePermission = 'Roezec:Admin';
            var Fields;
            (function (Fields) {
            })(Fields = TiposDireccionesRow.Fields || (TiposDireccionesRow.Fields = {}));
            [
                'TipoDireccionId',
                'TipoDireccion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TiposDireccionesRow = Nuevo_Roezec.TiposDireccionesRow || (Nuevo_Roezec.TiposDireccionesRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDireccionesService;
        (function (TiposDireccionesService) {
            TiposDireccionesService.baseUrl = 'Nuevo_Roezec/TiposDirecciones';
            var Methods;
            (function (Methods) {
            })(Methods = TiposDireccionesService.Methods || (TiposDireccionesService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposDireccionesService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposDireccionesService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TiposDireccionesService.baseUrl + '/' + x;
            });
        })(TiposDireccionesService = Nuevo_Roezec.TiposDireccionesService || (Nuevo_Roezec.TiposDireccionesService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDocumentoForm = /** @class */ (function (_super) {
            __extends(TiposDocumentoForm, _super);
            function TiposDocumentoForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TiposDocumentoForm.formKey = 'Nuevo_Roezec.TiposDocumento';
            return TiposDocumentoForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.TiposDocumentoForm = TiposDocumentoForm;
        [, ['Tipo', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TiposDocumentoForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDocumentoRow;
        (function (TiposDocumentoRow) {
            TiposDocumentoRow.idProperty = 'TipoDocumentoId';
            TiposDocumentoRow.nameProperty = 'Tipo';
            TiposDocumentoRow.localTextPrefix = 'Nuevo_Roezec.TiposDocumento';
            TiposDocumentoRow.deletePermission = 'Roezec:Admin';
            TiposDocumentoRow.insertPermission = 'Roezec:Admin';
            TiposDocumentoRow.readPermission = 'Roezec:Read';
            TiposDocumentoRow.updatePermission = 'Roezec:Admin';
            var Fields;
            (function (Fields) {
            })(Fields = TiposDocumentoRow.Fields || (TiposDocumentoRow.Fields = {}));
            [
                'TipoDocumentoId',
                'Tipo'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TiposDocumentoRow = Nuevo_Roezec.TiposDocumentoRow || (Nuevo_Roezec.TiposDocumentoRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDocumentoService;
        (function (TiposDocumentoService) {
            TiposDocumentoService.baseUrl = 'Nuevo_Roezec/TiposDocumento';
            var Methods;
            (function (Methods) {
            })(Methods = TiposDocumentoService.Methods || (TiposDocumentoService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposDocumentoService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposDocumentoService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TiposDocumentoService.baseUrl + '/' + x;
            });
        })(TiposDocumentoService = Nuevo_Roezec.TiposDocumentoService || (Nuevo_Roezec.TiposDocumentoService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposEnvioForm = /** @class */ (function (_super) {
            __extends(TiposEnvioForm, _super);
            function TiposEnvioForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!TiposEnvioForm.init) {
                    TiposEnvioForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(TiposEnvioForm, [
                        'Tipo', w0
                    ]);
                }
                return _this;
            }
            TiposEnvioForm.formKey = 'Nuevo_Roezec.TiposEnvio';
            return TiposEnvioForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.TiposEnvioForm = TiposEnvioForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposEnvioRow;
        (function (TiposEnvioRow) {
            TiposEnvioRow.idProperty = 'TipoEnvioId';
            TiposEnvioRow.nameProperty = 'Tipo';
            TiposEnvioRow.localTextPrefix = 'Nuevo_Roezec.TiposEnvio';
            TiposEnvioRow.lookupKey = 'Nuevo_Roezec.TiposEnvio';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.TiposEnvio');
            }
            TiposEnvioRow.getLookup = getLookup;
            TiposEnvioRow.deletePermission = 'Roezec:Admin';
            TiposEnvioRow.insertPermission = 'Roezec:Admin';
            TiposEnvioRow.readPermission = 'Roezec:Read';
            TiposEnvioRow.updatePermission = 'Roezec:Admin';
        })(TiposEnvioRow = Nuevo_Roezec.TiposEnvioRow || (Nuevo_Roezec.TiposEnvioRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposEnvioService;
        (function (TiposEnvioService) {
            TiposEnvioService.baseUrl = 'Nuevo_Roezec/TiposEnvio';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposEnvioService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposEnvioService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TiposEnvioService = Nuevo_Roezec.TiposEnvioService || (Nuevo_Roezec.TiposEnvioService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposGarantiaTasasForm = /** @class */ (function (_super) {
            __extends(TiposGarantiaTasasForm, _super);
            function TiposGarantiaTasasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!TiposGarantiaTasasForm.init) {
                    TiposGarantiaTasasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(TiposGarantiaTasasForm, [
                        'GarantiaTasa', w0
                    ]);
                }
                return _this;
            }
            TiposGarantiaTasasForm.formKey = 'Nuevo_Roezec.TiposGarantiaTasas';
            return TiposGarantiaTasasForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.TiposGarantiaTasasForm = TiposGarantiaTasasForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposGarantiaTasasRow;
        (function (TiposGarantiaTasasRow) {
            TiposGarantiaTasasRow.idProperty = 'GarantiaTasaId';
            TiposGarantiaTasasRow.nameProperty = 'GarantiaTasa';
            TiposGarantiaTasasRow.localTextPrefix = 'Nuevo_Roezec.TiposGarantiaTasas';
            TiposGarantiaTasasRow.lookupKey = 'Nuevo_Roezec.TiposGarantiaTasas';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.TiposGarantiaTasas');
            }
            TiposGarantiaTasasRow.getLookup = getLookup;
            TiposGarantiaTasasRow.deletePermission = 'Roezec:Admin';
            TiposGarantiaTasasRow.insertPermission = 'Roezec:Admin';
            TiposGarantiaTasasRow.readPermission = 'Roezec:Read';
            TiposGarantiaTasasRow.updatePermission = 'Roezec:Admin';
        })(TiposGarantiaTasasRow = Nuevo_Roezec.TiposGarantiaTasasRow || (Nuevo_Roezec.TiposGarantiaTasasRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposGarantiaTasasService;
        (function (TiposGarantiaTasasService) {
            TiposGarantiaTasasService.baseUrl = 'Nuevo_Roezec/TiposGarantiaTasas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposGarantiaTasasService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposGarantiaTasasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(TiposGarantiaTasasService = Nuevo_Roezec.TiposGarantiaTasasService || (Nuevo_Roezec.TiposGarantiaTasasService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposPersonaForm = /** @class */ (function (_super) {
            __extends(TiposPersonaForm, _super);
            function TiposPersonaForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TiposPersonaForm.formKey = 'Nuevo_Roezec.TiposPersona';
            return TiposPersonaForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.TiposPersonaForm = TiposPersonaForm;
        [, ['TipoPersona', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TiposPersonaForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposPersonaRow;
        (function (TiposPersonaRow) {
            TiposPersonaRow.idProperty = 'TipoPersonaId';
            TiposPersonaRow.nameProperty = 'TipoPersona';
            TiposPersonaRow.localTextPrefix = 'Nuevo_Roezec.TiposPersona';
            TiposPersonaRow.deletePermission = 'Roezec:Admin';
            TiposPersonaRow.insertPermission = 'Roezec:Admin';
            TiposPersonaRow.readPermission = 'Roezec:Read';
            TiposPersonaRow.updatePermission = 'Roezec:Admin';
            var Fields;
            (function (Fields) {
            })(Fields = TiposPersonaRow.Fields || (TiposPersonaRow.Fields = {}));
            [
                'TipoPersonaId',
                'TipoPersona'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TiposPersonaRow = Nuevo_Roezec.TiposPersonaRow || (Nuevo_Roezec.TiposPersonaRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposPersonaService;
        (function (TiposPersonaService) {
            TiposPersonaService.baseUrl = 'Nuevo_Roezec/TiposPersona';
            var Methods;
            (function (Methods) {
            })(Methods = TiposPersonaService.Methods || (TiposPersonaService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposPersonaService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposPersonaService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TiposPersonaService.baseUrl + '/' + x;
            });
        })(TiposPersonaService = Nuevo_Roezec.TiposPersonaService || (Nuevo_Roezec.TiposPersonaService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var VersionesNaceForm = /** @class */ (function (_super) {
            __extends(VersionesNaceForm, _super);
            function VersionesNaceForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!VersionesNaceForm.init) {
                    VersionesNaceForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.DateEditor;
                    Q.initFormType(VersionesNaceForm, [
                        'Version', w0,
                        'Fecha', w1
                    ]);
                }
                return _this;
            }
            VersionesNaceForm.formKey = 'Nuevo_Roezec.VersionesNace';
            return VersionesNaceForm;
        }(Serenity.PrefixedContext));
        Nuevo_Roezec.VersionesNaceForm = VersionesNaceForm;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var VersionesNaceRow;
        (function (VersionesNaceRow) {
            VersionesNaceRow.idProperty = 'VersionId';
            VersionesNaceRow.nameProperty = 'Version';
            VersionesNaceRow.localTextPrefix = 'Nuevo_Roezec.VersionesNace';
            VersionesNaceRow.lookupKey = 'Nuevo_Roezec.VersionesNace';
            function getLookup() {
                return Q.getLookup('Nuevo_Roezec.VersionesNace');
            }
            VersionesNaceRow.getLookup = getLookup;
            VersionesNaceRow.deletePermission = 'Roezec:Admin';
            VersionesNaceRow.insertPermission = 'Roezec:Admin';
            VersionesNaceRow.readPermission = 'Roezec:Read';
            VersionesNaceRow.updatePermission = 'Roezec:Admin';
        })(VersionesNaceRow = Nuevo_Roezec.VersionesNaceRow || (Nuevo_Roezec.VersionesNaceRow = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var VersionesNaceService;
        (function (VersionesNaceService) {
            VersionesNaceService.baseUrl = 'Nuevo_Roezec/VersionesNace';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                VersionesNaceService[x] = function (r, s, o) {
                    return Q.serviceRequest(VersionesNaceService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(VersionesNaceService = Nuevo_Roezec.VersionesNaceService || (Nuevo_Roezec.VersionesNaceService = {}));
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var RegistroForm = /** @class */ (function (_super) {
            __extends(RegistroForm, _super);
            function RegistroForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            RegistroForm.formKey = 'Registro.Registro';
            return RegistroForm;
        }(Serenity.PrefixedContext));
        Registro.RegistroForm = RegistroForm;
        [, ['NumeroRegistro', function () { return Serenity.StringEditor; }],
            ['FechaRegistro', function () { return Serenity.DateEditor; }],
            ['TipoRegistroId', function () { return Serenity.IntegerEditor; }],
            ['Titulo', function () { return Serenity.StringEditor; }],
            ['Ficheros', function () { return Serenity.StringEditor; }],
            ['Observaciones', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(RegistroForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var RegistroRow;
        (function (RegistroRow) {
            RegistroRow.idProperty = 'RegistroId';
            RegistroRow.nameProperty = 'NumeroRegistro';
            RegistroRow.localTextPrefix = 'Registro.Registro';
            RegistroRow.deletePermission = 'Registro:Modify';
            RegistroRow.insertPermission = 'Registro:Modify';
            RegistroRow.readPermission = 'Registro:Read';
            RegistroRow.updatePermission = 'Registro:Modify';
            var Fields;
            (function (Fields) {
            })(Fields = RegistroRow.Fields || (RegistroRow.Fields = {}));
            [
                'RegistroId',
                'NumeroRegistro',
                'FechaRegistro',
                'TipoRegistroId',
                'Titulo',
                'Ficheros',
                'Observaciones',
                'TipoRegistro',
                'UserId',
                'FechaModificacion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(RegistroRow = Registro.RegistroRow || (Registro.RegistroRow = {}));
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var RegistroService;
        (function (RegistroService) {
            RegistroService.baseUrl = 'Registro/Registro';
            var Methods;
            (function (Methods) {
            })(Methods = RegistroService.Methods || (RegistroService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                RegistroService[x] = function (r, s, o) {
                    return Q.serviceRequest(RegistroService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = RegistroService.baseUrl + '/' + x;
            });
        })(RegistroService = Registro.RegistroService || (Registro.RegistroService = {}));
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var TiposregistroForm = /** @class */ (function (_super) {
            __extends(TiposregistroForm, _super);
            function TiposregistroForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TiposregistroForm.formKey = 'Registro.Tiposregistro';
            return TiposregistroForm;
        }(Serenity.PrefixedContext));
        Registro.TiposregistroForm = TiposregistroForm;
        [, ['TipoRegistro', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TiposregistroForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var TiposregistroRow;
        (function (TiposregistroRow) {
            TiposregistroRow.idProperty = 'TipoRegistroId';
            TiposregistroRow.nameProperty = 'TipoRegistro';
            TiposregistroRow.localTextPrefix = 'Registro.Tiposregistro';
            TiposregistroRow.deletePermission = 'Registro:General';
            TiposregistroRow.insertPermission = 'Registro:General';
            TiposregistroRow.readPermission = 'Registro:General';
            TiposregistroRow.updatePermission = 'Registro:General';
            var Fields;
            (function (Fields) {
            })(Fields = TiposregistroRow.Fields || (TiposregistroRow.Fields = {}));
            [
                'TipoRegistroId',
                'TipoRegistro'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TiposregistroRow = Registro.TiposregistroRow || (Registro.TiposregistroRow = {}));
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var TiposregistroService;
        (function (TiposregistroService) {
            TiposregistroService.baseUrl = 'Registro/Tiposregistro';
            var Methods;
            (function (Methods) {
            })(Methods = TiposregistroService.Methods || (TiposregistroService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TiposregistroService[x] = function (r, s, o) {
                    return Q.serviceRequest(TiposregistroService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TiposregistroService.baseUrl + '/' + x;
            });
        })(TiposregistroService = Registro.TiposregistroService || (Registro.TiposregistroService = {}));
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var ActividadesForm = /** @class */ (function (_super) {
            __extends(ActividadesForm, _super);
            function ActividadesForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!ActividadesForm.init) {
                    ActividadesForm.init = true;
                    var s = Serenity;
                    var w0 = s.LookupEditor;
                    Q.initFormType(ActividadesForm, [
                        'IdNace', w0,
                        'IdEmpresa', w0
                    ]);
                }
                return _this;
            }
            ActividadesForm.formKey = 'Roezec.Actividades';
            return ActividadesForm;
        }(Serenity.PrefixedContext));
        Roezec.ActividadesForm = ActividadesForm;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var ActividadesRow;
        (function (ActividadesRow) {
            ActividadesRow.idProperty = 'Id';
            ActividadesRow.nameProperty = 'Actividad';
            ActividadesRow.localTextPrefix = 'Roezec.Actividades';
            ActividadesRow.deletePermission = 'Roezec_Old:Delete';
            ActividadesRow.insertPermission = 'Roezec_Old:Insert';
            ActividadesRow.readPermission = 'Roezec_Old:Read';
            ActividadesRow.updatePermission = 'Roezec_Old:Modify';
        })(ActividadesRow = Roezec.ActividadesRow || (Roezec.ActividadesRow = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var ActividadesService;
        (function (ActividadesService) {
            ActividadesService.baseUrl = 'Roezec/Actividades';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ActividadesService[x] = function (r, s, o) {
                    return Q.serviceRequest(ActividadesService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(ActividadesService = Roezec.ActividadesService || (Roezec.ActividadesService = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var EmpleosSSForm = /** @class */ (function (_super) {
            __extends(EmpleosSSForm, _super);
            function EmpleosSSForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            EmpleosSSForm.formKey = 'Roezec.EmpleosSS';
            return EmpleosSSForm;
        }(Serenity.PrefixedContext));
        Roezec.EmpleosSSForm = EmpleosSSForm;
        [, ['Fecha', function () { return Serenity.DateEditor; }],
            ['Empleos', function () { return Serenity.DecimalEditor; }]
        ].forEach(function (x) { return Object.defineProperty(EmpleosSSForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var EmpleosSSRow;
        (function (EmpleosSSRow) {
            EmpleosSSRow.idProperty = 'Id';
            EmpleosSSRow.localTextPrefix = 'Roezec.EmpleosSS';
            EmpleosSSRow.deletePermission = 'Roezec_Old_SS:Read';
            EmpleosSSRow.insertPermission = 'Roezec_Old_SS:Read';
            EmpleosSSRow.readPermission = 'Roezec_Old_SS:Read';
            EmpleosSSRow.updatePermission = 'Roezec_Old_SS:Read';
            var Fields;
            (function (Fields) {
            })(Fields = EmpleosSSRow.Fields || (EmpleosSSRow.Fields = {}));
            [
                'Id',
                'IdEmpresa',
                'Fecha',
                'Empleos',
                'Anyo',
                'Isla',
                'AnyoExpediente'
            ].forEach(function (x) { return Fields[x] = x; });
        })(EmpleosSSRow = Roezec.EmpleosSSRow || (Roezec.EmpleosSSRow = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var EmpleosSSService;
        (function (EmpleosSSService) {
            EmpleosSSService.baseUrl = 'Roezec/EmpleosSS';
            var Methods;
            (function (Methods) {
            })(Methods = EmpleosSSService.Methods || (EmpleosSSService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EmpleosSSService[x] = function (r, s, o) {
                    return Q.serviceRequest(EmpleosSSService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = EmpleosSSService.baseUrl + '/' + x;
            });
        })(EmpleosSSService = Roezec.EmpleosSSService || (Roezec.EmpleosSSService = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var InscritasForm = /** @class */ (function (_super) {
            __extends(InscritasForm, _super);
            function InscritasForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            InscritasForm.formKey = 'Roezec.Inscritas';
            return InscritasForm;
        }(Serenity.PrefixedContext));
        Roezec.InscritasForm = InscritasForm;
        [, ['IdEmpresa', function () { return Serenity.IntegerEditor; }],
            ['NumeroAsiento', function () { return Serenity.IntegerEditor; }],
            ['NumeroLiquidacionTasa', function () { return Serenity.StringEditor; }],
            ['FechaSolicitud', function () { return Serenity.DateEditor; }],
            ['FechaResolucion', function () { return Serenity.DateEditor; }],
            ['FechaNotificacion', function () { return Serenity.DateEditor; }],
            ['Observaciones', function () { return Serenity.StringEditor; }],
            ['FechaAlta', function () { return Serenity.DateEditor; }],
            ['UsrAlta', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(InscritasForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var InscritasRow;
        (function (InscritasRow) {
            InscritasRow.idProperty = 'Id';
            InscritasRow.nameProperty = 'NumeroLiquidacionTasa';
            InscritasRow.localTextPrefix = 'Roezec.Inscritas';
            InscritasRow.deletePermission = 'Roezec:Delete';
            InscritasRow.insertPermission = 'Roezec:Insert';
            InscritasRow.readPermission = 'Roezec:Read';
            InscritasRow.updatePermission = 'Roezec:Modify';
            var Fields;
            (function (Fields) {
            })(Fields = InscritasRow.Fields || (InscritasRow.Fields = {}));
            [
                'Id',
                'IdEmpresa',
                'NumeroAsiento',
                'NumeroLiquidacionTasa',
                'FechaSolicitud',
                'FechaResolucion',
                'FechaNotificacion',
                'Observaciones',
                'FechaAlta',
                'UsrAlta',
                'IdEmpresaDenominacionSocial',
                'IdEmpresaCif',
                'IdEmpresaDireccion',
                'IdEmpresaCp',
                'IdEmpresaPoblacion',
                'IdEmpresaProvincia',
                'IdEmpresaIsla',
                'IdEmpresaNotasMarginales',
                'IdEmpresaAnyoExpediente',
                'IdEmpresaNumExpediente',
                'IdEmpresaAgencia',
                'IdEmpresaTecnico',
                'IdEmpresaFormaJuridica',
                'IdEmpresaSuperficie',
                'IdEmpresaExentaAreaAcotada',
                'IdEmpresaMotivosExencion',
                'IdEmpresaObjetivoEmpleo',
                'IdEmpresaObjetivoInversion',
                'IdEmpresaObservacionesEmpleo',
                'IdEmpresaObservacionesInversion',
                'IdEmpresaPreEmpleo',
                'IdEmpresaPreInversion',
                'IdEmpresaTrasEmpleo',
                'IdEmpresaTrasInversion',
                'IdEmpresaFechaAlta',
                'IdEmpresaFechaModificacion',
                'IdEmpresaFechaBaja',
                'IdEmpresaSituacion',
                'IdEmpresaUsrAlta',
                'IdEmpresaUsrModificacion',
                'IdEmpresaUsrBaja',
                'IdEmpresaTrading',
                'IdEmpresaEmpleos2021',
                'IdEmpresaEmpleos2020',
                'IdEmpresaEmpleos2019',
                'IdEmpresaEmpleos2018',
                'IdEmpresaEmpleos2017'
            ].forEach(function (x) { return Fields[x] = x; });
        })(InscritasRow = Roezec.InscritasRow || (Roezec.InscritasRow = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var InscritasService;
        (function (InscritasService) {
            InscritasService.baseUrl = 'Roezec/Inscritas';
            var Methods;
            (function (Methods) {
            })(Methods = InscritasService.Methods || (InscritasService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                InscritasService[x] = function (r, s, o) {
                    return Q.serviceRequest(InscritasService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = InscritasService.baseUrl + '/' + x;
            });
        })(InscritasService = Roezec.InscritasService || (Roezec.InscritasService = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var NacesForm = /** @class */ (function (_super) {
            __extends(NacesForm, _super);
            function NacesForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!NacesForm.init) {
                    NacesForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.DateEditor;
                    Q.initFormType(NacesForm, [
                        'Nace', w0,
                        'Codigo', w0,
                        'Actividad', w0,
                        'FechaAlta', w1,
                        'UsrAlta', w0
                    ]);
                }
                return _this;
            }
            NacesForm.formKey = 'Roezec.Naces';
            return NacesForm;
        }(Serenity.PrefixedContext));
        Roezec.NacesForm = NacesForm;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var NacesRow;
        (function (NacesRow) {
            NacesRow.idProperty = 'Id';
            NacesRow.nameProperty = 'Actividad';
            NacesRow.localTextPrefix = 'Roezec.Naces';
            NacesRow.lookupKey = 'Roezec.Naces';
            function getLookup() {
                return Q.getLookup('Roezec.Naces');
            }
            NacesRow.getLookup = getLookup;
            NacesRow.deletePermission = 'Roezec_Old:Delete';
            NacesRow.insertPermission = 'Roezec_Old:Insert';
            NacesRow.readPermission = 'Roezec_Old:Read';
            NacesRow.updatePermission = 'Roezec_Old:Modify';
        })(NacesRow = Roezec.NacesRow || (Roezec.NacesRow = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var NacesService;
        (function (NacesService) {
            NacesService.baseUrl = 'Roezec/Naces';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                NacesService[x] = function (r, s, o) {
                    return Q.serviceRequest(NacesService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(NacesService = Roezec.NacesService || (Roezec.NacesService = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RepresentantesForm = /** @class */ (function (_super) {
            __extends(RepresentantesForm, _super);
            function RepresentantesForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!RepresentantesForm.init) {
                    RepresentantesForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.IntegerEditor;
                    var w2 = s.LookupEditor;
                    var w3 = s.DateEditor;
                    Q.initFormType(RepresentantesForm, [
                        'TipoDoc', w0,
                        'Doc', w0,
                        'IdRepresentanteFisico', w1,
                        'Nombre', w0,
                        'Direccion', w0,
                        'Cp', w0,
                        'Poblacion', w0,
                        'Provincia', w0,
                        'Pais', w0,
                        'Email', w0,
                        'Telefono', w0,
                        'Descripcion', w0,
                        'IdEmpresa', w2,
                        'FechaAlta', w3,
                        'FechaModificacion', w3,
                        'FechaBaja', w3,
                        'UsrAlta', w0,
                        'UsrModificacion', w0,
                        'UsrBaja', w0,
                        'MotivoBaja', w0
                    ]);
                }
                return _this;
            }
            RepresentantesForm.formKey = 'Roezec.Representantes';
            return RepresentantesForm;
        }(Serenity.PrefixedContext));
        Roezec.RepresentantesForm = RepresentantesForm;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RepresentantesRow;
        (function (RepresentantesRow) {
            RepresentantesRow.idProperty = 'Id';
            RepresentantesRow.nameProperty = 'TipoDoc';
            RepresentantesRow.localTextPrefix = 'Roezec.Representantes';
            RepresentantesRow.deletePermission = 'Roezec_Old:Delete';
            RepresentantesRow.insertPermission = 'Roezec_Old:Insert';
            RepresentantesRow.readPermission = 'Roezec_Old:Read';
            RepresentantesRow.updatePermission = 'Roezec_Old:Modify';
        })(RepresentantesRow = Roezec.RepresentantesRow || (Roezec.RepresentantesRow = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RepresentantesService;
        (function (RepresentantesService) {
            RepresentantesService.baseUrl = 'Roezec/Representantes';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                RepresentantesService[x] = function (r, s, o) {
                    return Q.serviceRequest(RepresentantesService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(RepresentantesService = Roezec.RepresentantesService || (Roezec.RepresentantesService = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasForm = /** @class */ (function (_super) {
            __extends(RoezecEmpresasForm, _super);
            function RoezecEmpresasForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!RoezecEmpresasForm.init) {
                    RoezecEmpresasForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.IntegerEditor;
                    var w2 = s.DecimalEditor;
                    var w3 = s.DateEditor;
                    var w4 = s.LookupEditor;
                    Q.initFormType(RoezecEmpresasForm, [
                        'DenominacionSocial', w0,
                        'Cif', w0,
                        'Direccion', w0,
                        'Cp', w0,
                        'Poblacion', w0,
                        'Provincia', w0,
                        'Isla', w0,
                        'NotasMarginales', w0,
                        'AnyoExpediente', w1,
                        'NumExpediente', w1,
                        'Agencia', w1,
                        'Tecnico', w0,
                        'FormaJuridica', w0,
                        'Superficie', w2,
                        'ExentaAreaAcotada', w0,
                        'MotivosExencion', w0,
                        'ObjetivoEmpleo', w2,
                        'ObjetivoInversion', w2,
                        'ObservacionesEmpleo', w0,
                        'ObservacionesInversion', w0,
                        'PreEmpleo', w1,
                        'PreInversion', w1,
                        'TrasEmpleo', w1,
                        'TrasInversion', w1,
                        'FechaAlta', w3,
                        'FechaModificacion', w3,
                        'FechaBaja', w3,
                        'SituacionId', w4,
                        'UsrAlta', w0,
                        'UsrModificacion', w0,
                        'UsrBaja', w0
                    ]);
                }
                return _this;
            }
            RoezecEmpresasForm.formKey = 'Roezec.RoezecEmpresas';
            return RoezecEmpresasForm;
        }(Serenity.PrefixedContext));
        Roezec.RoezecEmpresasForm = RoezecEmpresasForm;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasRow;
        (function (RoezecEmpresasRow) {
            RoezecEmpresasRow.idProperty = 'Id';
            RoezecEmpresasRow.nameProperty = 'DenominacionSocial';
            RoezecEmpresasRow.localTextPrefix = 'Roezec.RoezecEmpresas';
            RoezecEmpresasRow.lookupKey = 'Roezec.RoezecEmpresas';
            function getLookup() {
                return Q.getLookup('Roezec.RoezecEmpresas');
            }
            RoezecEmpresasRow.getLookup = getLookup;
            RoezecEmpresasRow.deletePermission = 'Roezec_Old:Delete';
            RoezecEmpresasRow.insertPermission = 'Roezec_Old:Insert';
            RoezecEmpresasRow.readPermission = 'Roezec_Old:Read';
            RoezecEmpresasRow.updatePermission = 'Roezec_Old:Modify';
        })(RoezecEmpresasRow = Roezec.RoezecEmpresasRow || (Roezec.RoezecEmpresasRow = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasSSForm = /** @class */ (function (_super) {
            __extends(RoezecEmpresasSSForm, _super);
            function RoezecEmpresasSSForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            RoezecEmpresasSSForm.formKey = 'Roezec.RoezecEmpresasSS';
            return RoezecEmpresasSSForm;
        }(Serenity.PrefixedContext));
        Roezec.RoezecEmpresasSSForm = RoezecEmpresasSSForm;
        [, ['DenominacionSocial', function () { return Serenity.StringEditor; }],
            ['Cif', function () { return Serenity.StringEditor; }],
            ['Direccion', function () { return Serenity.StringEditor; }],
            ['Cp', function () { return Serenity.StringEditor; }],
            ['Poblacion', function () { return Serenity.StringEditor; }],
            ['Provincia', function () { return Serenity.StringEditor; }],
            ['Isla', function () { return Serenity.StringEditor; }],
            ['NotasMarginales', function () { return Serenity.StringEditor; }],
            ['AnyoExpediente', function () { return Serenity.IntegerEditor; }],
            ['NumExpediente', function () { return Serenity.IntegerEditor; }],
            ['Agencia', function () { return Serenity.IntegerEditor; }],
            ['Tecnico', function () { return Serenity.StringEditor; }],
            ['FormaJuridica', function () { return Serenity.StringEditor; }],
            ['Superficie', function () { return Serenity.DecimalEditor; }],
            ['ExentaAreaAcotada', function () { return Serenity.StringEditor; }],
            ['MotivosExencion', function () { return Serenity.StringEditor; }],
            ['ObjetivoEmpleo', function () { return Serenity.DecimalEditor; }],
            ['ObjetivoInversion', function () { return Serenity.DecimalEditor; }],
            ['ObservacionesEmpleo', function () { return Serenity.StringEditor; }],
            ['ObservacionesInversion', function () { return Serenity.StringEditor; }],
            ['PreEmpleo', function () { return Serenity.IntegerEditor; }],
            ['PreInversion', function () { return Serenity.IntegerEditor; }],
            ['TrasEmpleo', function () { return Serenity.IntegerEditor; }],
            ['TrasInversion', function () { return Serenity.IntegerEditor; }],
            ['FechaAlta', function () { return Serenity.DateEditor; }],
            ['FechaModificacion', function () { return Serenity.DateEditor; }],
            ['FechaBaja', function () { return Serenity.DateEditor; }],
            ['Situacion', function () { return Serenity.StringEditor; }],
            ['UsrAlta', function () { return Serenity.StringEditor; }],
            ['UsrModificacion', function () { return Serenity.StringEditor; }],
            ['UsrBaja', function () { return Serenity.StringEditor; }],
            ['Trading', function () { return Serenity.IntegerEditor; }],
            ['Empleos2021', function () { return Serenity.DecimalEditor; }],
            ['Empleos2020', function () { return Serenity.DecimalEditor; }],
            ['Empleos2019', function () { return Serenity.DecimalEditor; }],
            ['Empleos2018', function () { return Serenity.DecimalEditor; }],
            ['Empleos2017', function () { return Serenity.DecimalEditor; }]
        ].forEach(function (x) { return Object.defineProperty(RoezecEmpresasSSForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasSSRow;
        (function (RoezecEmpresasSSRow) {
            RoezecEmpresasSSRow.idProperty = 'Id';
            RoezecEmpresasSSRow.nameProperty = 'DenominacionSocial';
            RoezecEmpresasSSRow.localTextPrefix = 'Roezec.RoezecEmpresasSS';
            RoezecEmpresasSSRow.deletePermission = 'RoezecSS:General';
            RoezecEmpresasSSRow.insertPermission = 'RoezecSS:General';
            RoezecEmpresasSSRow.readPermission = 'RoezecSS:General';
            RoezecEmpresasSSRow.updatePermission = 'RoezecSS:General';
            var Fields;
            (function (Fields) {
            })(Fields = RoezecEmpresasSSRow.Fields || (RoezecEmpresasSSRow.Fields = {}));
            [
                'Id',
                'DenominacionSocial',
                'Cif',
                'Direccion',
                'Cp',
                'Poblacion',
                'Provincia',
                'Isla',
                'NotasMarginales',
                'AnyoExpediente',
                'NumExpediente',
                'Agencia',
                'Tecnico',
                'FormaJuridica',
                'Superficie',
                'ExentaAreaAcotada',
                'MotivosExencion',
                'ObjetivoEmpleo',
                'ObjetivoInversion',
                'ObservacionesEmpleo',
                'ObservacionesInversion',
                'PreEmpleo',
                'PreInversion',
                'TrasEmpleo',
                'TrasInversion',
                'FechaAlta',
                'FechaModificacion',
                'FechaBaja',
                'Situacion',
                'UsrAlta',
                'UsrModificacion',
                'UsrBaja',
                'Trading',
                'Empleos2021',
                'Empleos2020',
                'Empleos2019',
                'Empleos2018',
                'Empleos2017',
                'NaceId',
                'NacePrincipal'
            ].forEach(function (x) { return Fields[x] = x; });
        })(RoezecEmpresasSSRow = Roezec.RoezecEmpresasSSRow || (Roezec.RoezecEmpresasSSRow = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasSSService;
        (function (RoezecEmpresasSSService) {
            RoezecEmpresasSSService.baseUrl = 'Roezec/RoezecEmpresasSS';
            var Methods;
            (function (Methods) {
            })(Methods = RoezecEmpresasSSService.Methods || (RoezecEmpresasSSService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                RoezecEmpresasSSService[x] = function (r, s, o) {
                    return Q.serviceRequest(RoezecEmpresasSSService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = RoezecEmpresasSSService.baseUrl + '/' + x;
            });
        })(RoezecEmpresasSSService = Roezec.RoezecEmpresasSSService || (Roezec.RoezecEmpresasSSService = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasService;
        (function (RoezecEmpresasService) {
            RoezecEmpresasService.baseUrl = 'Roezec/RoezecEmpresas';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                RoezecEmpresasService[x] = function (r, s, o) {
                    return Q.serviceRequest(RoezecEmpresasService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(RoezecEmpresasService = Roezec.RoezecEmpresasService || (Roezec.RoezecEmpresasService = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEstadosForm = /** @class */ (function (_super) {
            __extends(RoezecEstadosForm, _super);
            function RoezecEstadosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!RoezecEstadosForm.init) {
                    RoezecEstadosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    Q.initFormType(RoezecEstadosForm, [
                        'Descripcion', w0
                    ]);
                }
                return _this;
            }
            RoezecEstadosForm.formKey = 'Roezec.RoezecEstados';
            return RoezecEstadosForm;
        }(Serenity.PrefixedContext));
        Roezec.RoezecEstadosForm = RoezecEstadosForm;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEstadosRow;
        (function (RoezecEstadosRow) {
            RoezecEstadosRow.idProperty = 'Codigo';
            RoezecEstadosRow.nameProperty = 'Descripcion';
            RoezecEstadosRow.localTextPrefix = 'Roezec.RoezecEstados';
            RoezecEstadosRow.lookupKey = 'Roezec.RoezecEstados';
            function getLookup() {
                return Q.getLookup('Roezec.RoezecEstados');
            }
            RoezecEstadosRow.getLookup = getLookup;
            RoezecEstadosRow.deletePermission = 'Roezec_Old:Delete';
            RoezecEstadosRow.insertPermission = 'Roezec_Old:Insert';
            RoezecEstadosRow.readPermission = 'Roezec_Old:Read';
            RoezecEstadosRow.updatePermission = 'Roezec_Old:Modify';
        })(RoezecEstadosRow = Roezec.RoezecEstadosRow || (Roezec.RoezecEstadosRow = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEstadosService;
        (function (RoezecEstadosService) {
            RoezecEstadosService.baseUrl = 'Roezec/RoezecEstados';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                RoezecEstadosService[x] = function (r, s, o) {
                    return Q.serviceRequest(RoezecEstadosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(RoezecEstadosService = Roezec.RoezecEstadosService || (Roezec.RoezecEstadosService = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var SociosForm = /** @class */ (function (_super) {
            __extends(SociosForm, _super);
            function SociosForm(prefix) {
                var _this = _super.call(this, prefix) || this;
                if (!SociosForm.init) {
                    SociosForm.init = true;
                    var s = Serenity;
                    var w0 = s.StringEditor;
                    var w1 = s.LookupEditor;
                    var w2 = s.IntegerEditor;
                    var w3 = s.DateEditor;
                    Q.initFormType(SociosForm, [
                        'TipoDoc', w0,
                        'Doc', w0,
                        'TipoPersona', w0,
                        'Nombre', w0,
                        'Direccion', w0,
                        'Cp', w0,
                        'Poblacion', w0,
                        'Provincia', w0,
                        'Pais', w0,
                        'Email', w0,
                        'Telefono', w0,
                        'Descripcion', w0,
                        'IdEmpresa', w1,
                        'Participacion', w2,
                        'FechaAlta', w3,
                        'FechaModificacion', w3,
                        'FechaBaja', w3,
                        'UsrAlta', w0,
                        'UsrModificacion', w0,
                        'UsrBaja', w0,
                        'MotivoBaja', w0
                    ]);
                }
                return _this;
            }
            SociosForm.formKey = 'Roezec.Socios';
            return SociosForm;
        }(Serenity.PrefixedContext));
        Roezec.SociosForm = SociosForm;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var SociosRow;
        (function (SociosRow) {
            SociosRow.idProperty = 'Id';
            SociosRow.nameProperty = 'TipoDoc';
            SociosRow.localTextPrefix = 'Roezec.Socios';
            SociosRow.deletePermission = 'Roezec_Old:Delete';
            SociosRow.insertPermission = 'Roezec_Old:Insert';
            SociosRow.readPermission = 'Roezec_Old:Read';
            SociosRow.updatePermission = 'Roezec_Old:Modify';
        })(SociosRow = Roezec.SociosRow || (Roezec.SociosRow = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var SociosService;
        (function (SociosService) {
            SociosService.baseUrl = 'Roezec/Socios';
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SociosService[x] = function (r, s, o) {
                    return Q.serviceRequest(SociosService.baseUrl + '/' + x, r, s, o);
                };
            });
        })(SociosService = Roezec.SociosService || (Roezec.SociosService = {}));
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Texts;
    (function (Texts) {
        ProyectosZec['Texts'] = Q.proxyTexts(Texts, '', { Db: { Administration: { Language: { Id: 1, LanguageId: 1, LanguageName: 1 }, Role: { RoleId: 1, RoleName: 1 }, RolePermission: { PermissionKey: 1, RoleId: 1, RolePermissionId: 1, RoleRoleName: 1 }, Translation: { CustomText: 1, EntityPlural: 1, Key: 1, OverrideConfirmation: 1, SaveChangesButton: 1, SourceLanguage: 1, SourceText: 1, TargetLanguage: 1, TargetText: 1 }, User: { DisplayName: 1, Email: 1, InsertDate: 1, InsertUserId: 1, IsActive: 1, LastDirectoryUpdate: 1, Password: 1, PasswordConfirm: 1, PasswordHash: 1, PasswordSalt: 1, Source: 1, UpdateDate: 1, UpdateUserId: 1, UserId: 1, UserImage: 1, Username: 1 }, UserPermission: { Granted: 1, PermissionKey: 1, User: 1, UserId: 1, UserPermissionId: 1, Username: 1 }, UserRole: { RoleId: 1, User: 1, UserId: 1, UserRoleId: 1, Username: 1 } }, Common: { UserPreference: { Name: 1, PreferenceType: 1, UserId: 1, UserPreferenceId: 1, Value: 1 } }, CuadroMandos: { Capital: { Capital: 1, CapitalId: 1 }, Estados: { Estado: 1, EstadoId: 1 }, Islas: { Isla: 1, IslaId: 1, NombreIsla: 1 }, Prescriptorinversor: { PrescriptorInversor: 1, PrescriptorInversorId: 1 }, Proyectos: { Capital: 1, CapitalId: 1, Captacion: 1, Contacto: 1, Denominacion: 1, Descripcion: 1, Email: 1, EmpleoReal: 1, Empleos: 1, Estado: 1, EstadoId: 1, Expediente: 1, FechaAmpliacion: 1, FechaAutorizacion: 1, FechaBaja: 1, FechaInicio: 1, FechaInscripcion: 1, FechaPresentacion: 1, Inversion: 1, InversionReal: 1, Isla: 1, IslaId: 1, Nace: 1, NombreTecnico: 1, PrescriptorInversor: 1, PrescriptorInversorId: 1, ProyectoId: 1, Sector: 1, SectorId: 1, Subsector: 1, SubsectorId: 1, SubsectorSectorId: 1, Tecnico: 1, TecnicoId: 1, Telefono: 1 }, Sectores: { Sector: 1, SectorId: 1 }, Subsectores: { Sector: 1, SectorId: 1, Subsector: 1, SubsectorId: 1 }, Tecnicos: { NombreTecnico: 1, Tecnico: 1, TecnicoId: 1 } }, Inmovilizado: { Inmovilizados: { Amortizacion: 1, Codigo: 1, Descripcion: 1, Factura: 1, FechaBaja: 1, FechaCompra: 1, Files: 1, GalleryImages: 1, Garantia: 1, InmovilizadoId: 1, NumeroSerie: 1, Pg: 1, Proveedor: 1, ProveedorId: 1, Sede: 1, SedeId: 1, SubTipo: 1, SubTipoInmovilizadoId: 1, Tipo: 1, TipoInmovilizadoId: 1, Ubicacion: 1, Valor: 1, ValorResidual: 1 }, Proveedores: { Contacto: 1, Email: 1, Proveedor: 1, ProveedorId: 1, Telefono: 1 }, Subtiposinmovilizado: { SubTipo: 1, SubTipoInmovilizadoId: 1, TipoInmovilizadoId: 1, TipoInmovilizadoTipo: 1 }, Tiposinmovilizado: { Tipo: 1, TipoInmovilizadoId: 1 } }, Intranet: { Departamentos: { Departamento: 1, DepartamentoId: 1 }, Sedes: { Sede: 1, SedeId: 1 }, Telefonos: { Contrato: 1, CortoMovil: 1, Departamento: 1, DepartamentoId: 1, ExtCorta: 1, Fijo: 1, Movil: 1, Multisim: 1, Nombre: 1, PUK: 1, Sede: 1, SedeId: 1, TelefonoId: 1 } }, Kairos: { AusenciasProgramadas: { CodigoCliente: 1, Descripcion: 1, Empleado: 1, FechaBorrado: 1, FechaDesde: 1, FechaHasta: 1, Id: 1, IdAusenciaProgramadaTipo: 1, IdAusenciaProgramadaTipoCodigo: 1, IdAusenciaProgramadaTipoCodigoCliente: 1, IdAusenciaProgramadaTipoColorFondo: 1, IdAusenciaProgramadaTipoColorLetra: 1, IdAusenciaProgramadaTipoContabilizarTiempo: 1, IdAusenciaProgramadaTipoFechaActualizacion: 1, IdAusenciaProgramadaTipoFechaBorrado: 1, IdAusenciaProgramadaTipoPermitirSolicitud: 1, IdEmpleado: 1, TotalDias: 1, TotalHoras: 1 }, Departamentos: { Codigo: 1, CodigoCliente: 1, Descripcion: 1, FechaBorrado: 1, Id: 1, SedeId: 1 }, Diario: { Empleado: 1, Entrada: 1, Fecha: 1, HoraEntrada: 1, HoraSalida: 1, Id: 1, IdDepartamento: 1, IdEmpleado: 1, Salida: 1, Sede: 1, SedeId: 1 }, EstadosExtras: { Descripcion: 1, EstadoId: 1 }, Extras: { CodigoCliente: 1, Consumidas: 1, Consumidashhmm: 1, Convertidas: 1, Dia: 1, DptoId: 1, Empleado: 1, Estado: 1, EstadoDesc: 1, Fecha: 1, FechaAceptacionCancelacion: 1, Id: 1, IdAusenciaProgramadaTipo: 1, IdEmpleado: 1, IdHoraExtraCabecera: 1, IdHoraExtraCabeceraCodigo: 1, IdHoraExtraCabeceraCodigoCliente: 1, IdHoraExtraCabeceraDescripcion: 1, IdHoraExtraCabeceraFechaBorrado: 1, MotivoCancelacion: 1, Pendientes: 1, Pendienteshhmm: 1, Sede: 1, SedeId: 1, Tipo: 1, TotalConsumidas: 1, TotalHorasExtrasConvertidas: 1, TotalHorasExtrasReales: 1 }, Fichajes: { Anulado: 1, CodigoCliente: 1, Dispositivo: 1, DptoId: 1, Empleado: 1, EntradaSalida: 1, Fecha: 1, FechaHora: 1, Fichaje: 1, Files: 1, GpsAltitud: 1, GpsFechaHora: 1, GpsPosicionLatitud: 1, GpsPosicionLongitud: 1, GpsProveedor: 1, Hora: 1, Id: 1, IdDispositivoModelo: 1, IdDispositivoModeloCodigoCliente: 1, IdDispositivoModeloFabricante: 1, IdDispositivoModeloFechaBorrado: 1, IdDispositivoModeloMac: 1, IdDispositivoModeloMarca: 1, IdDispositivoModeloModelo: 1, IdEmpleado: 1, IdEmpresa: 1, IdTerminal: 1, IdTerminalCodigo: 1, IdTerminalCodigoCliente: 1, IdTerminalDescripcion: 1, IdTerminalDispositivoHuellaDactilar: 1, IdTerminalDispositivoHuellaDactilarDns: 1, IdTerminalDispositivoHuellaDactilarFirmware: 1, IdTerminalDispositivoHuellaDactilarIp: 1, IdTerminalDispositivoHuellaDactilarMarca: 1, IdTerminalDispositivoHuellaDactilarModelo: 1, IdTerminalDispositivoHuellaDactilarNumero: 1, IdTerminalDispositivoHuellaDactilarPuerto: 1, IdTerminalFechaActualizacion: 1, IdTerminalFechaBorrado: 1, IdTerminalIdDepartamento: 1, IdTerminalTipoDispositivo: 1, Modificado: 1, Observaciones: 1, Sede: 1, SedeId: 1, TipoDispositivo: 1, Validado: 1 }, HorasExtraConsumidas: { Dia: 1, Empleado: 1, FechaAutorizacion: 1, Id: 1, IdEmpleado: 1, IdHoraExtra: 1, IdHoraExtraCodigoCliente: 1, IdHoraExtraDia: 1, IdHoraExtraEstado: 1, IdHoraExtraFecha: 1, IdHoraExtraFechaAceptacionCancelacion: 1, IdHoraExtraIdAusenciaProgramadaTipo: 1, IdHoraExtraIdEmpleado: 1, IdHoraExtraIdHoraExtraCabecera: 1, IdHoraExtraMotivoCancelacion: 1, IdHoraExtraTipo: 1, IdHoraExtraTotalHorasExtrasConvertidas: 1, IdHoraExtraTotalHorasExtrasReales: 1, Tiempo: 1 }, KrsAusenciasProgramadasTipos: { Codigo: 1, CodigoCliente: 1, ColorFondo: 1, ColorLetra: 1, ContabilizarTiempo: 1, Descripcion: 1, FechaActualizacion: 1, FechaBorrado: 1, Id: 1, PermitirSolicitud: 1 }, KrsEmpleados: { ClaveAccesoWeb: 1, CodigoCliente: 1, CodigoValidacion: 1, Email: 1, FechaActualizacion: 1, FechaBaja: 1, FechaBorrado: 1, Id: 1, IdDepartamento: 1, IdEmpresa: 1, IdEmpresaFichajeAutomatico: 1, IdHoraExtraCabecera: 1, IdHoraExtraCabeceraCodigo: 1, IdHoraExtraCabeceraCodigoCliente: 1, IdHoraExtraCabeceraDescripcion: 1, IdHoraExtraCabeceraFechaBorrado: 1, Nombre: 1, NumeroTarjetaFichaje: 1, PermiteFichajeAutomatico: 1, PermiteFichajeWeb: 1, PermiteRecordatorio: 1, Pin: 1, ProgramaExternoDescripcion: 1, ProgramaExternoIdEmpleado: 1, SacarFotoFichaje: 1, Tecnico: 1, UsoHorario: 1 }, TiposFichaje: { Id: 1, Tipo: 1 } }, Nuevo_Roezec: { Capital: { Capital: 1, CapitalId: 1 }, Contactos: { Apellidos: 1, ContactoId: 1, Email: 1, Fullname: 1, Idioma: 1, IdiomaId: 1, IdiomaNombreIdioma: 1, Movil: 1, Nif: 1, Nombre: 1, TelefonoFijo: 1 }, Continentes: { Continente: 1, ContinenteId: 1 }, Empresas: { CapitalList: 1, Cif: 1, ContactosList: 1, Cp: 1, Direccion: 1, Email: 1, Empleo6Meses: 1, EmpleoPromedio: 1, EmpleoPromedio2Anos: 1, EmpleoTraspasado: 1, EmpresaId: 1, EstadoEmpresaEstado: 1, EstadoEmpresaId: 1, Expediente: 1, FechaCambioEstado: 1, FormaJuridicaId: 1, FormaJuridicaJuridica: 1, HistorialList: 1, Inversion2Anos: 1, InversionTraspasada: 1, Isla: 1, IslaId: 1, IslaNombreIsla: 1, MercadosList: 1, MotivoExencion: 1, Movil: 1, NacesList: 1, NumTasaLiquidacion: 1, Poblacion: 1, ProyectoId: 1, Razon: 1, Tecnico: 1, TecnicoId: 1, TecnicoNombreTecnico: 1, TelefonoFijo: 1, TipoGarantiaTasaGarantiaTasa: 1, TipoGarantiaTasaId: 1, TipologiaCapitalCapital: 1, TipologiaCapitalId: 1 }, EmpresasContactos: { ContactoApellidos: 1, ContactoEmail: 1, ContactoId: 1, ContactoIdiomaId: 1, ContactoMovil: 1, ContactoNif: 1, ContactoNombre: 1, ContactoTelefonoFijo: 1, EmpresaCif: 1, EmpresaContactoId: 1, EmpresaDireccion: 1, EmpresaEmail: 1, EmpresaEmpleo6Meses: 1, EmpresaEmpleoPromedio: 1, EmpresaEmpleoPromedio2Anos: 1, EmpresaEmpleoTraspasado: 1, EmpresaEstadoEmpresaId: 1, EmpresaExpediente: 1, EmpresaFormaJuridicaId: 1, EmpresaId: 1, EmpresaInversion2Anos: 1, EmpresaInversionTraspasada: 1, EmpresaIslaId: 1, EmpresaMotivoExencion: 1, EmpresaMovil: 1, EmpresaNExpediente: 1, EmpresaNumTasaLiquidacion: 1, EmpresaPoblacion: 1, EmpresaProyectoId: 1, EmpresaRazon: 1, EmpresaTecnicoId: 1, EmpresaTelefonoFijo: 1, EmpresaTipoGarantiaTasaId: 1, EmpresaTipologiaCapitalId: 1, EstadoEmpresaEstado: 1, FechaAlta: 1, FechaBaja: 1, Fullname: 1, IslaNombreIsla: 1, TecnicoNombreTecnico: 1, TipoContactoContacto: 1, TipoContactoId: 1, TipoGarantiaTasaGarantiaTasa: 1, TipologiaCapitalCapital: 1 }, EmpresasMercados: { EmpresaCif: 1, EmpresaCp: 1, EmpresaDireccion: 1, EmpresaEmail: 1, EmpresaEmpleo6Meses: 1, EmpresaEmpleoPromedio: 1, EmpresaEmpleoPromedio2Anos: 1, EmpresaEmpleoTraspasado: 1, EmpresaEstadoEmpresaId: 1, EmpresaExpediente: 1, EmpresaFechaCambioEstado: 1, EmpresaFormaJuridicaId: 1, EmpresaId: 1, EmpresaInversion2Anos: 1, EmpresaInversionTraspasada: 1, EmpresaIslaId: 1, EmpresaMercadoId: 1, EmpresaMotivoExencion: 1, EmpresaMovil: 1, EmpresaNumTasaLiquidacion: 1, EmpresaPoblacion: 1, EmpresaProyectoId: 1, EmpresaRazon: 1, EmpresaTecnicoId: 1, EmpresaTelefonoFijo: 1, EmpresaTipoGarantiaTasaId: 1, EmpresaTipologiaCapitalId: 1, Mercado: 1, MercadoId: 1 }, EmpresasNace: { EmpresaCif: 1, EmpresaDireccion: 1, EmpresaEmail: 1, EmpresaEmpleo6Meses: 1, EmpresaEmpleoPromedio: 1, EmpresaEmpleoPromedio2Anos: 1, EmpresaEmpleoTraspasado: 1, EmpresaEstadoEmpresaId: 1, EmpresaExpediente: 1, EmpresaFormaJuridicaId: 1, EmpresaId: 1, EmpresaInversion2Anos: 1, EmpresaInversionTraspasada: 1, EmpresaIslaId: 1, EmpresaMotivoExencion: 1, EmpresaMovil: 1, EmpresaNExpediente: 1, EmpresaNaceId: 1, EmpresaNumTasaLiquidacion: 1, EmpresaPoblacion: 1, EmpresaProyectoId: 1, EmpresaRazon: 1, EmpresaTecnicoId: 1, EmpresaTelefonoFijo: 1, EmpresaTipoGarantiaTasaId: 1, EmpresaTipologiaCapitalId: 1, EstadoEmpresaEstado: 1, IslaNombreIsla: 1, NaceCodigo: 1, NaceDescripcion: 1, NaceId: 1, NaceLarga: 1, NaceSubsectorId: 1, Sector: 1, SectorId: 1, Subsector: 1, TecnicoNombreTecnico: 1, TipoGarantiaTasaGarantiaTasa: 1, TipologiaCapitalCapital: 1, Version: 1, VersionId: 1 }, Envios: { ContactoAcuseFullName: 1, ContactoAcuseId: 1, ContactoEnvioApellidos: 1, ContactoEnvioEmail: 1, ContactoEnvioFullName: 1, ContactoEnvioId: 1, ContactoEnvioIdiomaId: 1, ContactoEnvioMovil: 1, ContactoEnvioNif: 1, ContactoEnvioNombre: 1, ContactoEnvioTelefonoFijo: 1, EnvioId: 1, EstadoEnvioEstado: 1, EstadoEnvioId: 1, FechaEnvio: 1, FechaRecepcion: 1, FormaEnvioForma: 1, FormaEnvioId: 1, HistorialId: 1, TipoEnvioId: 1, TipoEnvioTipo: 1 }, EnviosProcedimiento: { ContactoAcuseId: 1, ContactoEnvioApellidos: 1, ContactoEnvioEmail: 1, ContactoEnvioId: 1, ContactoEnvioIdiomaId: 1, ContactoEnvioMovil: 1, ContactoEnvioNif: 1, ContactoEnvioNombre: 1, ContactoEnvioTelefonoFijo: 1, EnvioId: 1, FechaEnvio: 1, FechaRecepcion: 1, HistorialAcuseInicio: 1, HistorialAcuseResolucion: 1, HistorialEmpresaId: 1, HistorialFechaEfecto: 1, HistorialFechaInicio: 1, HistorialFechaResolucion: 1, HistorialFicheros: 1, HistorialId: 1, HistorialObservaciones: 1, HistorialPersonaAcuseIncioId: 1, HistorialPersonaAcuseResolucionId: 1, HistorialProcedimientoId: 1, HistorialSentidoResolucion: 1, TipoEnvioId: 1, TipoEnvioTipo: 1 }, EstadosEmpresa: { Estado: 1, EstadoEmpresaId: 1 }, EstadosEnvio: { Estado: 1, EstadoEnvioId: 1 }, FormasEnvio: { Forma: 1, FormaEnvioId: 1 }, FormasJuridicas: { Juridica: 1, JuridicaId: 1 }, HistorialEmpresas: { EmpresaCif: 1, EmpresaDireccion: 1, EmpresaEmail: 1, EmpresaEmpleo6Meses: 1, EmpresaEmpleoPromedio: 1, EmpresaEmpleoPromedio2Anos: 1, EmpresaEmpleoTraspasado: 1, EmpresaEstadoEmpresaId: 1, EmpresaExpediente: 1, EmpresaFormaJuridicaId: 1, EmpresaId: 1, EmpresaInversion2Anos: 1, EmpresaInversionTraspasada: 1, EmpresaIslaId: 1, EmpresaMotivoExencion: 1, EmpresaMovil: 1, EmpresaNExpediente: 1, EmpresaNumTasaLiquidacion: 1, EmpresaPoblacion: 1, EmpresaProyectoId: 1, EmpresaRazon: 1, EmpresaTecnicoId: 1, EmpresaTelefonoFijo: 1, EmpresaTipoGarantiaTasaId: 1, EmpresaTipologiaCapitalId: 1, EnviosList: 1, EstadoEmpresaEstado: 1, FechaEfecto: 1, FechaInicio: 1, FechaResolucion: 1, Ficheros: 1, HistorialId: 1, IslaNombreIsla: 1, Observaciones: 1, Procedimiento: 1, ProcedimientoId: 1, SentidoResolucion: 1, TecnicoNombreTecnico: 1, TipoGarantiaTasaGarantiaTasa: 1, TipologiaCapitalCapital: 1 }, Idiomas: { Idioma: 1, IdiomaId: 1, NombreIdioma: 1 }, Islas: { Isla: 1, IslaId: 1, NombreIsla: 1 }, Mercados: { Mercado: 1, MercadoId: 1 }, Naces: { Codigo: 1, Descripcion: 1, NaceId: 1, NaceLarga: 1, Sector: 1, SectorId: 1, Subsector: 1, SubsectorId: 1, Version: 1, VersionId: 1 }, Paises: { Capital: 1, ContinenteId: 1, Pais: 1, PaisId: 1 }, ProcedenciaCapital: { EmpresaCif: 1, EmpresaCp: 1, EmpresaDireccion: 1, EmpresaEmail: 1, EmpresaEmpleo6Meses: 1, EmpresaEmpleoPromedio: 1, EmpresaEmpleoPromedio2Anos: 1, EmpresaEmpleoTraspasado: 1, EmpresaEstadoEmpresaId: 1, EmpresaExpediente: 1, EmpresaFechaCambioEstado: 1, EmpresaFormaJuridicaId: 1, EmpresaId: 1, EmpresaInversion2Anos: 1, EmpresaInversionTraspasada: 1, EmpresaIslaId: 1, EmpresaMotivoExencion: 1, EmpresaMovil: 1, EmpresaNumTasaLiquidacion: 1, EmpresaPoblacion: 1, EmpresaProyectoId: 1, EmpresaRazon: 1, EmpresaTecnicoId: 1, EmpresaTelefonoFijo: 1, EmpresaTipoGarantiaTasaId: 1, EmpresaTipologiaCapitalId: 1, Pais: 1, PaisCapital: 1, PaisContinenteId: 1, PaisId: 1, Porcentaje: 1, ProcedenciaId: 1 }, Procedimientos: { Caducidad_dias: 1, EstadoEmpresaEstado: 1, EstadoEmpresaId: 1, Procedimiento: 1, ProcedimientoId: 1 }, Sectores: { Sector: 1, SectorId: 1 }, Subsectores: { Sector: 1, SectorId: 1, Subsector: 1, SubsectorId: 1 }, Tecnicos: { NombreTecnico: 1, Tecnico: 1, TecnicoId: 1 }, TipologiasCapital: { Capital: 1, CapitalId: 1 }, TiposContacto: { Contacto: 1, ContactoId: 1 }, TiposEnvio: { Tipo: 1, TipoEnvioId: 1 }, TiposGarantiaTasas: { GarantiaTasa: 1, GarantiaTasaId: 1 }, VersionesNace: { Fecha: 1, Version: 1, VersionId: 1 } }, Roezec: { Actividades: { Actividad: 1, Agencia: 1, AnyoExpediente: 1, Cif: 1, Codigo: 1, Cp: 1, Direccion: 1, Empresa: 1, Estado: 1, ExentaAreaAcotada: 1, FechaAlta: 1, FechaBaja: 1, FechaModificacion: 1, FormaJuridica: 1, Id: 1, IdEmpresa: 1, IdNace: 1, Isla: 1, MotivosExencion: 1, NotasMarginales: 1, NumExpediente: 1, ObjetivoEmpleo: 1, ObjetivoInversion: 1, ObservacionesEmpleo: 1, ObservacionesInversion: 1, Poblacion: 1, PreEmpleo: 1, PreInversion: 1, Provincia: 1, SituacionId: 1, Superficie: 1, Tecnico: 1, TrasEmpleo: 1, TrasInversion: 1, UsrAlta: 1, UsrBaja: 1, UsrModificacion: 1 }, Naces: { Actividad: 1, Codigo: 1, FechaAlta: 1, Id: 1, Nace: 1, UsrAlta: 1 }, Representantes: { Agencia: 1, AnyoExpediente: 1, Cif: 1, Cp: 1, Descripcion: 1, Direccion: 1, Doc: 1, Email: 1, Empresa: 1, Estado: 1, ExentaAreaAcotada: 1, FechaAlta: 1, FechaBaja: 1, FechaModificacion: 1, FormaJuridica: 1, Id: 1, IdEmpresa: 1, IdRepresentanteFisico: 1, Isla: 1, MotivoBaja: 1, MotivosExencion: 1, Nombre: 1, NotasMarginales: 1, NumExpediente: 1, ObjetivoEmpleo: 1, ObjetivoInversion: 1, ObservacionesEmpleo: 1, ObservacionesInversion: 1, Pais: 1, Poblacion: 1, PreEmpleo: 1, PreInversion: 1, Provincia: 1, SituacionId: 1, Superficie: 1, Tecnico: 1, Telefono: 1, TipoDoc: 1, TrasEmpleo: 1, TrasInversion: 1, UsrAlta: 1, UsrBaja: 1, UsrModificacion: 1 }, RoezecEmpresas: { Agencia: 1, AnyoExpediente: 1, Cif: 1, Cp: 1, DenominacionSocial: 1, Direccion: 1, Estado: 1, ExentaAreaAcotada: 1, FechaAlta: 1, FechaBaja: 1, FechaModificacion: 1, FormaJuridica: 1, Id: 1, Isla: 1, MotivosExencion: 1, NotasMarginales: 1, NumExpediente: 1, ObjetivoEmpleo: 1, ObjetivoInversion: 1, ObservacionesEmpleo: 1, ObservacionesInversion: 1, Poblacion: 1, PreEmpleo: 1, PreInversion: 1, Provincia: 1, SituacionId: 1, Superficie: 1, Tecnico: 1, TrasEmpleo: 1, TrasInversion: 1, UsrAlta: 1, UsrBaja: 1, UsrModificacion: 1 }, RoezecEstados: { Codigo: 1, Descripcion: 1 }, Socios: { Agencia: 1, AnyoExpediente: 1, Cif: 1, Cp: 1, Descripcion: 1, Direccion: 1, Doc: 1, Email: 1, Empresa: 1, Estado: 1, ExentaAreaAcotada: 1, FechaAlta: 1, FechaBaja: 1, FechaModificacion: 1, FormaJuridica: 1, Id: 1, IdEmpresa: 1, Isla: 1, MotivoBaja: 1, MotivosExencion: 1, Nombre: 1, NotasMarginales: 1, NumExpediente: 1, ObjetivoEmpleo: 1, ObjetivoInversion: 1, ObservacionesEmpleo: 1, ObservacionesInversion: 1, Pais: 1, Participacion: 1, Poblacion: 1, PreEmpleo: 1, PreInversion: 1, Provincia: 1, SituacionId: 1, Superficie: 1, Tecnico: 1, Telefono: 1, TipoDoc: 1, TipoPersona: 1, TrasEmpleo: 1, TrasInversion: 1, UsrAlta: 1, UsrBaja: 1, UsrModificacion: 1 } } }, Forms: { Membership: { ChangePassword: { FormTitle: 1, SubmitButton: 1, Success: 1 }, ForgotPassword: { BackToLogin: 1, FormInfo: 1, FormTitle: 1, SubmitButton: 1, Success: 1 }, Login: { FacebookButton: 1, ForgotPassword: 1, FormTitle: 1, GoogleButton: 1, OR: 1, RememberMe: 1, SignInButton: 1, SignUpButton: 1 }, ResetPassword: { BackToLogin: 1, EmailSubject: 1, FormTitle: 1, SubmitButton: 1, Success: 1 }, SignUp: { AcceptTerms: 1, ActivateEmailSubject: 1, ActivationCompleteMessage: 1, BackToLogin: 1, ConfirmEmail: 1, ConfirmPassword: 1, DisplayName: 1, Email: 1, FormInfo: 1, FormTitle: 1, Password: 1, SubmitButton: 1, Success: 1 } } }, Site: { AccessDenied: { ClickToChangeUser: 1, ClickToLogin: 1, LackPermissions: 1, NotLoggedIn: 1, PageTitle: 1 }, BasicProgressDialog: { CancelTitle: 1, PleaseWait: 1 }, BulkServiceAction: { AllHadErrorsFormat: 1, AllSuccessFormat: 1, ConfirmationFormat: 1, ErrorCount: 1, NothingToProcess: 1, SomeHadErrorsFormat: 1, SuccessCount: 1 }, Dashboard: { ContentDescription: 1 }, Layout: { FooterCopyright: 1, FooterInfo: 1, FooterRights: 1, GeneralSettings: 1, Language: 1, Theme: 1, ThemeBlack: 1, ThemeBlackLight: 1, ThemeBlue: 1, ThemeBlueLight: 1, ThemeGreen: 1, ThemeGreenLight: 1, ThemePurple: 1, ThemePurpleLight: 1, ThemeRed: 1, ThemeRedLight: 1, ThemeYellow: 1, ThemeYellowLight: 1 }, RolePermissionDialog: { DialogTitle: 1, EditButton: 1, SaveSuccess: 1 }, UserDialog: { EditPermissionsButton: 1, EditRolesButton: 1 }, UserPermissionDialog: { DialogTitle: 1, Grant: 1, Permission: 1, Revoke: 1, SaveSuccess: 1 }, UserRoleDialog: { DialogTitle: 1, SaveSuccess: 1 }, ValidationError: { Title: 1 } }, Validation: { AuthenticationError: 1, CantFindUserWithEmail: 1, CurrentPasswordMismatch: 1, DeleteForeignKeyError: 1, EmailConfirm: 1, EmailInUse: 1, InvalidActivateToken: 1, InvalidResetToken: 1, MinRequiredPasswordLength: 1, SavePrimaryKeyError: 1 } });
    })(Texts = ProyectosZec.Texts || (ProyectosZec.Texts = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var LanguageDialog = /** @class */ (function (_super) {
            __extends(LanguageDialog, _super);
            function LanguageDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Administration.LanguageForm(_this.idPrefix);
                return _this;
            }
            LanguageDialog.prototype.getFormKey = function () { return Administration.LanguageForm.formKey; };
            LanguageDialog.prototype.getIdProperty = function () { return Administration.LanguageRow.idProperty; };
            LanguageDialog.prototype.getLocalTextPrefix = function () { return Administration.LanguageRow.localTextPrefix; };
            LanguageDialog.prototype.getNameProperty = function () { return Administration.LanguageRow.nameProperty; };
            LanguageDialog.prototype.getService = function () { return Administration.LanguageService.baseUrl; };
            LanguageDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], LanguageDialog);
            return LanguageDialog;
        }(Serenity.EntityDialog));
        Administration.LanguageDialog = LanguageDialog;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var LanguageGrid = /** @class */ (function (_super) {
            __extends(LanguageGrid, _super);
            function LanguageGrid(container) {
                return _super.call(this, container) || this;
            }
            LanguageGrid.prototype.getColumnsKey = function () { return "Administration.Language"; };
            LanguageGrid.prototype.getDialogType = function () { return Administration.LanguageDialog; };
            LanguageGrid.prototype.getIdProperty = function () { return Administration.LanguageRow.idProperty; };
            LanguageGrid.prototype.getLocalTextPrefix = function () { return Administration.LanguageRow.localTextPrefix; };
            LanguageGrid.prototype.getService = function () { return Administration.LanguageService.baseUrl; };
            LanguageGrid.prototype.getDefaultSortBy = function () {
                return ["LanguageName" /* LanguageName */];
            };
            LanguageGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], LanguageGrid);
            return LanguageGrid;
        }(Serenity.EntityGrid));
        Administration.LanguageGrid = LanguageGrid;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var RoleDialog = /** @class */ (function (_super) {
            __extends(RoleDialog, _super);
            function RoleDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Administration.RoleForm(_this.idPrefix);
                return _this;
            }
            RoleDialog.prototype.getFormKey = function () { return Administration.RoleForm.formKey; };
            RoleDialog.prototype.getIdProperty = function () { return Administration.RoleRow.idProperty; };
            RoleDialog.prototype.getLocalTextPrefix = function () { return Administration.RoleRow.localTextPrefix; };
            RoleDialog.prototype.getNameProperty = function () { return Administration.RoleRow.nameProperty; };
            RoleDialog.prototype.getService = function () { return Administration.RoleService.baseUrl; };
            RoleDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.push({
                    title: Q.text('Site.RolePermissionDialog.EditButton'),
                    cssClass: 'edit-permissions-button',
                    icon: 'fa-lock text-green',
                    onClick: function () {
                        new Administration.RolePermissionDialog({
                            roleID: _this.entity.RoleId,
                            title: _this.entity.RoleName
                        }).dialogOpen();
                    }
                });
                return buttons;
            };
            RoleDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                this.toolbar.findButton("edit-permissions-button").toggleClass("disabled", this.isNewOrDeleted());
            };
            RoleDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RoleDialog);
            return RoleDialog;
        }(Serenity.EntityDialog));
        Administration.RoleDialog = RoleDialog;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var RoleGrid = /** @class */ (function (_super) {
            __extends(RoleGrid, _super);
            function RoleGrid(container) {
                return _super.call(this, container) || this;
            }
            RoleGrid.prototype.getColumnsKey = function () { return "Administration.Role"; };
            RoleGrid.prototype.getDialogType = function () { return Administration.RoleDialog; };
            RoleGrid.prototype.getIdProperty = function () { return Administration.RoleRow.idProperty; };
            RoleGrid.prototype.getLocalTextPrefix = function () { return Administration.RoleRow.localTextPrefix; };
            RoleGrid.prototype.getService = function () { return Administration.RoleService.baseUrl; };
            RoleGrid.prototype.getDefaultSortBy = function () {
                return ["RoleName" /* RoleName */];
            };
            RoleGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], RoleGrid);
            return RoleGrid;
        }(Serenity.EntityGrid));
        Administration.RoleGrid = RoleGrid;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var RolePermissionDialog = /** @class */ (function (_super) {
            __extends(RolePermissionDialog, _super);
            function RolePermissionDialog(opt) {
                var _this = _super.call(this, opt) || this;
                _this.permissions = new Administration.PermissionCheckEditor(_this.byId('Permissions'), {
                    showRevoke: false
                });
                Administration.RolePermissionService.List({
                    RoleID: _this.options.roleID,
                    Module: null,
                    Submodule: null
                }, function (response) {
                    _this.permissions.value = response.Entities.map(function (x) { return ({ PermissionKey: x }); });
                });
                _this.permissions.implicitPermissions = Q.getRemoteData('Administration.ImplicitPermissions');
                return _this;
            }
            RolePermissionDialog.prototype.getDialogOptions = function () {
                var _this = this;
                var opt = _super.prototype.getDialogOptions.call(this);
                opt.buttons = [
                    {
                        text: Q.text('Dialogs.OkButton'),
                        click: function (e) {
                            Administration.RolePermissionService.Update({
                                RoleID: _this.options.roleID,
                                Permissions: _this.permissions.value.map(function (x) { return x.PermissionKey; }),
                                Module: null,
                                Submodule: null
                            }, function (response) {
                                _this.dialogClose();
                                window.setTimeout(function () { return Q.notifySuccess(Q.text('Site.RolePermissionDialog.SaveSuccess')); }, 0);
                            });
                        }
                    }, {
                        text: Q.text('Dialogs.CancelButton'),
                        click: function () { return _this.dialogClose(); }
                    }
                ];
                opt.title = Q.format(Q.text('Site.RolePermissionDialog.DialogTitle'), this.options.title);
                return opt;
            };
            RolePermissionDialog.prototype.getTemplate = function () {
                return '<div id="~_Permissions"></div>';
            };
            RolePermissionDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RolePermissionDialog);
            return RolePermissionDialog;
        }(Serenity.TemplatedDialog));
        Administration.RolePermissionDialog = RolePermissionDialog;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var TranslationGrid = /** @class */ (function (_super) {
            __extends(TranslationGrid, _super);
            function TranslationGrid(container) {
                var _this = _super.call(this, container) || this;
                _this.element.on('keyup.' + _this.uniqueName + ' change.' + _this.uniqueName, 'input.custom-text', function (e) {
                    var value = Q.trimToNull($(e.target).val());
                    if (value === '') {
                        value = null;
                    }
                    _this.view.getItemById($(e.target).data('key')).CustomText = value;
                    _this.hasChanges = true;
                });
                return _this;
            }
            TranslationGrid.prototype.getIdProperty = function () { return "Key"; };
            TranslationGrid.prototype.getLocalTextPrefix = function () { return "Administration.Translation"; };
            TranslationGrid.prototype.getService = function () { return Administration.TranslationService.baseUrl; };
            TranslationGrid.prototype.onClick = function (e, row, cell) {
                var _this = this;
                _super.prototype.onClick.call(this, e, row, cell);
                if (e.isDefaultPrevented()) {
                    return;
                }
                var item = this.itemAt(row);
                var done;
                if ($(e.target).hasClass('source-text')) {
                    e.preventDefault();
                    done = function () {
                        item.CustomText = item.SourceText;
                        _this.view.updateItem(item.Key, item);
                        _this.hasChanges = true;
                    };
                    if (Q.isTrimmedEmpty(item.CustomText) ||
                        (Q.trimToEmpty(item.CustomText) === Q.trimToEmpty(item.SourceText))) {
                        done();
                        return;
                    }
                    Q.confirm(Q.text('Db.Administration.Translation.OverrideConfirmation'), done);
                    return;
                }
                if ($(e.target).hasClass('target-text')) {
                    e.preventDefault();
                    done = function () {
                        item.CustomText = item.TargetText;
                        _this.view.updateItem(item.Key, item);
                        _this.hasChanges = true;
                    };
                    if (Q.isTrimmedEmpty(item.CustomText) ||
                        (Q.trimToEmpty(item.CustomText) === Q.trimToEmpty(item.TargetText))) {
                        done();
                        return;
                    }
                    Q.confirm(Q.text('Db.Administration.Translation.OverrideConfirmation'), done);
                    return;
                }
            };
            TranslationGrid.prototype.getColumns = function () {
                var columns = [];
                columns.push({ field: 'Key', width: 300, sortable: false });
                columns.push({
                    field: 'SourceText',
                    width: 300,
                    sortable: false,
                    format: function (ctx) {
                        return Q.outerHtml($('<a/>')
                            .addClass('source-text')
                            .text(ctx.value || ''));
                    }
                });
                columns.push({
                    field: 'CustomText',
                    width: 300,
                    sortable: false,
                    format: function (ctx) { return Q.outerHtml($('<input/>')
                        .addClass('custom-text')
                        .attr('value', ctx.value)
                        .attr('type', 'text')
                        .attr('data-key', ctx.item.Key)); }
                });
                columns.push({
                    field: 'TargetText',
                    width: 300,
                    sortable: false,
                    format: function (ctx) { return Q.outerHtml($('<a/>')
                        .addClass('target-text')
                        .text(ctx.value || '')); }
                });
                return columns;
            };
            TranslationGrid.prototype.createToolbarExtensions = function () {
                var _this = this;
                _super.prototype.createToolbarExtensions.call(this);
                var opt = {
                    lookupKey: 'Administration.Language'
                };
                this.sourceLanguage = Serenity.Widget.create({
                    type: Serenity.LookupEditor,
                    element: function (el) { return el.appendTo(_this.toolbar.element).attr('placeholder', '--- ' +
                        Q.text('Db.Administration.Translation.SourceLanguage') + ' ---'); },
                    options: opt
                });
                this.sourceLanguage.changeSelect2(function (e) {
                    if (_this.hasChanges) {
                        _this.saveChanges(_this.targetLanguageKey).then(function () { return _this.refresh(); });
                    }
                    else {
                        _this.refresh();
                    }
                });
                this.targetLanguage = Serenity.Widget.create({
                    type: Serenity.LookupEditor,
                    element: function (el) { return el.appendTo(_this.toolbar.element).attr('placeholder', '--- ' +
                        Q.text('Db.Administration.Translation.TargetLanguage') + ' ---'); },
                    options: opt
                });
                this.targetLanguage.changeSelect2(function (e) {
                    if (_this.hasChanges) {
                        _this.saveChanges(_this.targetLanguageKey).then(function () { return _this.refresh(); });
                    }
                    else {
                        _this.refresh();
                    }
                });
            };
            TranslationGrid.prototype.saveChanges = function (language) {
                var _this = this;
                var translations = {};
                for (var _i = 0, _a = this.getItems(); _i < _a.length; _i++) {
                    var item = _a[_i];
                    translations[item.Key] = item.CustomText;
                }
                return Promise.resolve(Administration.TranslationService.Update({
                    TargetLanguageID: language,
                    Translations: translations
                })).then(function () {
                    _this.hasChanges = false;
                    language = Q.trimToNull(language) || 'invariant';
                    Q.notifySuccess('User translations in "' + language +
                        '" language are saved to "user.texts.' +
                        language + '.json" ' + 'file under "~/App_Data/texts/"', '');
                });
            };
            TranslationGrid.prototype.onViewSubmit = function () {
                var request = this.view.params;
                request.SourceLanguageID = this.sourceLanguage.value;
                this.targetLanguageKey = this.targetLanguage.value || '';
                request.TargetLanguageID = this.targetLanguageKey;
                this.hasChanges = false;
                return _super.prototype.onViewSubmit.call(this);
            };
            TranslationGrid.prototype.getButtons = function () {
                var _this = this;
                return [{
                        title: Q.text('Db.Administration.Translation.SaveChangesButton'),
                        onClick: function (e) { return _this.saveChanges(_this.targetLanguageKey).then(function () { return _this.refresh(); }); },
                        cssClass: 'apply-changes-button'
                    }];
            };
            TranslationGrid.prototype.createQuickSearchInput = function () {
                var _this = this;
                Serenity.GridUtils.addQuickSearchInputCustom(this.toolbar.element, function (field, searchText) {
                    _this.searchText = searchText;
                    _this.view.setItems(_this.view.getItems(), true);
                });
            };
            TranslationGrid.prototype.onViewFilter = function (item) {
                if (!_super.prototype.onViewFilter.call(this, item)) {
                    return false;
                }
                if (!this.searchText) {
                    return true;
                }
                var sd = Select2.util.stripDiacritics;
                var searching = sd(this.searchText).toLowerCase();
                function match(str) {
                    if (!str)
                        return false;
                    return str.toLowerCase().indexOf(searching) >= 0;
                }
                return Q.isEmptyOrNull(searching) || match(item.Key) || match(item.SourceText) ||
                    match(item.TargetText) || match(item.CustomText);
            };
            TranslationGrid.prototype.usePager = function () {
                return false;
            };
            TranslationGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TranslationGrid);
            return TranslationGrid;
        }(Serenity.EntityGrid));
        Administration.TranslationGrid = TranslationGrid;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserDialog = /** @class */ (function (_super) {
            __extends(UserDialog, _super);
            function UserDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Administration.UserForm(_this.idPrefix);
                _this.form.Password.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.Password.value.length < 7)
                        return "Password must be at least 7 characters!";
                });
                _this.form.PasswordConfirm.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.Password.value != _this.form.PasswordConfirm.value)
                        return "The passwords entered doesn't match!";
                });
                return _this;
            }
            UserDialog.prototype.getFormKey = function () { return Administration.UserForm.formKey; };
            UserDialog.prototype.getIdProperty = function () { return Administration.UserRow.idProperty; };
            UserDialog.prototype.getIsActiveProperty = function () { return Administration.UserRow.isActiveProperty; };
            UserDialog.prototype.getLocalTextPrefix = function () { return Administration.UserRow.localTextPrefix; };
            UserDialog.prototype.getNameProperty = function () { return Administration.UserRow.nameProperty; };
            UserDialog.prototype.getService = function () { return Administration.UserService.baseUrl; };
            UserDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.push({
                    title: Q.text('Site.UserDialog.EditRolesButton'),
                    cssClass: 'edit-roles-button',
                    icon: 'fa-users text-blue',
                    onClick: function () {
                        new Administration.UserRoleDialog({
                            userID: _this.entity.UserId,
                            username: _this.entity.Username
                        }).dialogOpen();
                    }
                });
                buttons.push({
                    title: Q.text('Site.UserDialog.EditPermissionsButton'),
                    cssClass: 'edit-permissions-button',
                    icon: 'fa-lock text-green',
                    onClick: function () {
                        new Administration.UserPermissionDialog({
                            userID: _this.entity.UserId,
                            username: _this.entity.Username
                        }).dialogOpen();
                    }
                });
                return buttons;
            };
            UserDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                this.toolbar.findButton('edit-roles-button').toggleClass('disabled', this.isNewOrDeleted());
                this.toolbar.findButton("edit-permissions-button").toggleClass("disabled", this.isNewOrDeleted());
            };
            UserDialog.prototype.afterLoadEntity = function () {
                _super.prototype.afterLoadEntity.call(this);
                // these fields are only required in new record mode
                this.form.Password.element.toggleClass('required', this.isNew())
                    .closest('.field').find('sup').toggle(this.isNew());
                this.form.PasswordConfirm.element.toggleClass('required', this.isNew())
                    .closest('.field').find('sup').toggle(this.isNew());
            };
            UserDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], UserDialog);
            return UserDialog;
        }(Serenity.EntityDialog));
        Administration.UserDialog = UserDialog;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserGrid = /** @class */ (function (_super) {
            __extends(UserGrid, _super);
            function UserGrid(container) {
                return _super.call(this, container) || this;
            }
            UserGrid.prototype.getColumnsKey = function () { return "Administration.User"; };
            UserGrid.prototype.getDialogType = function () { return Administration.UserDialog; };
            UserGrid.prototype.getIdProperty = function () { return Administration.UserRow.idProperty; };
            UserGrid.prototype.getIsActiveProperty = function () { return Administration.UserRow.isActiveProperty; };
            UserGrid.prototype.getLocalTextPrefix = function () { return Administration.UserRow.localTextPrefix; };
            UserGrid.prototype.getService = function () { return Administration.UserService.baseUrl; };
            UserGrid.prototype.getDefaultSortBy = function () {
                return ["Username" /* Username */];
            };
            UserGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], UserGrid);
            return UserGrid;
        }(Serenity.EntityGrid));
        Administration.UserGrid = UserGrid;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Authorization;
    (function (Authorization) {
        Object.defineProperty(Authorization, 'userDefinition', {
            get: function () {
                return Q.getRemoteData('UserData');
            }
        });
        function hasPermission(permissionKey) {
            var ud = Authorization.userDefinition;
            return ud.Username === 'admin' || !!ud.Permissions[permissionKey];
        }
        Authorization.hasPermission = hasPermission;
    })(Authorization = ProyectosZec.Authorization || (ProyectosZec.Authorization = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var PermissionCheckEditor = /** @class */ (function (_super) {
            __extends(PermissionCheckEditor, _super);
            function PermissionCheckEditor(container, opt) {
                var _this = _super.call(this, container, opt) || this;
                _this._rolePermissions = {};
                _this._implicitPermissions = {};
                var titleByKey = {};
                var permissionKeys = _this.getSortedGroupAndPermissionKeys(titleByKey);
                var items = permissionKeys.map(function (key) { return ({
                    Key: key,
                    ParentKey: _this.getParentKey(key),
                    Title: titleByKey[key],
                    GrantRevoke: null,
                    IsGroup: key.charAt(key.length - 1) === ':'
                }); });
                _this.byParentKey = Q.toGrouping(items, function (x) { return x.ParentKey; });
                _this.setItems(items);
                return _this;
            }
            PermissionCheckEditor.prototype.getIdProperty = function () { return "Key"; };
            PermissionCheckEditor.prototype.getItemGrantRevokeClass = function (item, grant) {
                if (!item.IsGroup) {
                    return ((item.GrantRevoke === grant) ? ' checked' : '');
                }
                var desc = this.getDescendants(item, true);
                var granted = desc.filter(function (x) { return x.GrantRevoke === grant; });
                if (!granted.length) {
                    return '';
                }
                if (desc.length === granted.length) {
                    return 'checked';
                }
                return 'checked partial';
            };
            PermissionCheckEditor.prototype.roleOrImplicit = function (key) {
                if (this._rolePermissions[key])
                    return true;
                for (var _i = 0, _a = Object.keys(this._rolePermissions); _i < _a.length; _i++) {
                    var k = _a[_i];
                    var d = this._implicitPermissions[k];
                    if (d && d[key])
                        return true;
                }
                for (var _b = 0, _c = Object.keys(this._implicitPermissions); _b < _c.length; _b++) {
                    var i = _c[_b];
                    var item = this.view.getItemById(i);
                    if (item && item.GrantRevoke == true) {
                        var d = this._implicitPermissions[i];
                        if (d && d[key])
                            return true;
                    }
                }
            };
            PermissionCheckEditor.prototype.getItemEffectiveClass = function (item) {
                var _this = this;
                if (item.IsGroup) {
                    var desc = this.getDescendants(item, true);
                    var grantCount = Q.count(desc, function (x) { return x.GrantRevoke === true ||
                        (x.GrantRevoke == null && _this.roleOrImplicit(x.Key)); });
                    if (grantCount === desc.length || desc.length === 0) {
                        return 'allow';
                    }
                    if (grantCount === 0) {
                        return 'deny';
                    }
                    return 'partial';
                }
                var granted = item.GrantRevoke === true ||
                    (item.GrantRevoke == null && this.roleOrImplicit(item.Key));
                return (granted ? ' allow' : ' deny');
            };
            PermissionCheckEditor.prototype.getColumns = function () {
                var _this = this;
                var columns = [{
                        name: Q.text('Site.UserPermissionDialog.Permission'),
                        field: 'Title',
                        format: Serenity.SlickFormatting.treeToggle(function () { return _this.view; }, function (x) { return x.Key; }, function (ctx) {
                            var item = ctx.item;
                            var klass = _this.getItemEffectiveClass(item);
                            return '<span class="effective-permission ' + klass + '">' + Q.htmlEncode(ctx.value) + '</span>';
                        }),
                        width: 495,
                        sortable: false
                    }, {
                        name: Q.text('Site.UserPermissionDialog.Grant'), field: 'Grant',
                        format: function (ctx) {
                            var item1 = ctx.item;
                            var klass1 = _this.getItemGrantRevokeClass(item1, true);
                            return "<span class='check-box grant no-float " + klass1 + "'></span>";
                        },
                        width: 65,
                        sortable: false,
                        headerCssClass: 'align-center',
                        cssClass: 'align-center'
                    }];
                if (this.options.showRevoke) {
                    columns.push({
                        name: Q.text('Site.UserPermissionDialog.Revoke'), field: 'Revoke',
                        format: function (ctx) {
                            var item2 = ctx.item;
                            var klass2 = _this.getItemGrantRevokeClass(item2, false);
                            return '<span class="check-box revoke no-float ' + klass2 + '"></span>';
                        },
                        width: 65,
                        sortable: false,
                        headerCssClass: 'align-center',
                        cssClass: 'align-center'
                    });
                }
                return columns;
            };
            PermissionCheckEditor.prototype.setItems = function (items) {
                Serenity.SlickTreeHelper.setIndents(items, function (x) { return x.Key; }, function (x) { return x.ParentKey; }, false);
                this.view.setItems(items, true);
            };
            PermissionCheckEditor.prototype.onViewSubmit = function () {
                return false;
            };
            PermissionCheckEditor.prototype.onViewFilter = function (item) {
                var _this = this;
                if (!_super.prototype.onViewFilter.call(this, item)) {
                    return false;
                }
                if (!Serenity.SlickTreeHelper.filterById(item, this.view, function (x) { return x.ParentKey; }))
                    return false;
                if (this.searchText) {
                    return this.matchContains(item) || item.IsGroup && Q.any(this.getDescendants(item, false), function (x) { return _this.matchContains(x); });
                }
                return true;
            };
            PermissionCheckEditor.prototype.matchContains = function (item) {
                return Select2.util.stripDiacritics(item.Title || '').toLowerCase().indexOf(this.searchText) >= 0;
            };
            PermissionCheckEditor.prototype.getDescendants = function (item, excludeGroups) {
                var result = [];
                var stack = [item];
                while (stack.length > 0) {
                    var i = stack.pop();
                    var children = this.byParentKey[i.Key];
                    if (!children)
                        continue;
                    for (var _i = 0, children_1 = children; _i < children_1.length; _i++) {
                        var child = children_1[_i];
                        if (!excludeGroups || !child.IsGroup) {
                            result.push(child);
                        }
                        stack.push(child);
                    }
                }
                return result;
            };
            PermissionCheckEditor.prototype.onClick = function (e, row, cell) {
                _super.prototype.onClick.call(this, e, row, cell);
                if (!e.isDefaultPrevented()) {
                    Serenity.SlickTreeHelper.toggleClick(e, row, cell, this.view, function (x) { return x.Key; });
                }
                if (e.isDefaultPrevented()) {
                    return;
                }
                var target = $(e.target);
                var grant = target.hasClass('grant');
                if (grant || target.hasClass('revoke')) {
                    e.preventDefault();
                    var item = this.itemAt(row);
                    var checkedOrPartial = target.hasClass('checked') || target.hasClass('partial');
                    if (checkedOrPartial) {
                        grant = null;
                    }
                    else {
                        grant = grant !== checkedOrPartial;
                    }
                    if (item.IsGroup) {
                        for (var _i = 0, _a = this.getDescendants(item, true); _i < _a.length; _i++) {
                            var d = _a[_i];
                            d.GrantRevoke = grant;
                        }
                    }
                    else
                        item.GrantRevoke = grant;
                    this.slickGrid.invalidate();
                }
            };
            PermissionCheckEditor.prototype.getParentKey = function (key) {
                if (key.charAt(key.length - 1) === ':') {
                    key = key.substr(0, key.length - 1);
                }
                var idx = key.lastIndexOf(':');
                if (idx >= 0) {
                    return key.substr(0, idx + 1);
                }
                return null;
            };
            PermissionCheckEditor.prototype.getButtons = function () {
                return [];
            };
            PermissionCheckEditor.prototype.createToolbarExtensions = function () {
                var _this = this;
                _super.prototype.createToolbarExtensions.call(this);
                Serenity.GridUtils.addQuickSearchInputCustom(this.toolbar.element, function (field, text) {
                    _this.searchText = Select2.util.stripDiacritics(Q.trimToNull(text) || '').toLowerCase();
                    _this.view.setItems(_this.view.getItems(), true);
                });
            };
            PermissionCheckEditor.prototype.getSortedGroupAndPermissionKeys = function (titleByKey) {
                var keys = Q.getRemoteData('Administration.PermissionKeys').Entities;
                var titleWithGroup = {};
                for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                    var k = keys_1[_i];
                    var s = k;
                    if (!s) {
                        continue;
                    }
                    if (s.charAt(s.length - 1) == ':') {
                        s = s.substr(0, s.length - 1);
                        if (s.length === 0) {
                            continue;
                        }
                    }
                    if (titleByKey[s]) {
                        continue;
                    }
                    titleByKey[s] = Q.coalesce(Q.tryGetText('Permission.' + s), s);
                    var parts = s.split(':');
                    var group = '';
                    var groupTitle = '';
                    for (var i = 0; i < parts.length - 1; i++) {
                        group = group + parts[i] + ':';
                        var txt = Q.tryGetText('Permission.' + group);
                        if (txt == null) {
                            txt = parts[i];
                        }
                        titleByKey[group] = txt;
                        groupTitle = groupTitle + titleByKey[group] + ':';
                        titleWithGroup[group] = groupTitle;
                    }
                    titleWithGroup[s] = groupTitle + titleByKey[s];
                }
                keys = Object.keys(titleByKey);
                keys = keys.sort(function (x, y) { return Q.turkishLocaleCompare(titleWithGroup[x], titleWithGroup[y]); });
                return keys;
            };
            Object.defineProperty(PermissionCheckEditor.prototype, "value", {
                get: function () {
                    var result = [];
                    for (var _i = 0, _a = this.view.getItems(); _i < _a.length; _i++) {
                        var item = _a[_i];
                        if (item.GrantRevoke != null && item.Key.charAt(item.Key.length - 1) != ':') {
                            result.push({ PermissionKey: item.Key, Granted: item.GrantRevoke });
                        }
                    }
                    return result;
                },
                set: function (value) {
                    for (var _i = 0, _a = this.view.getItems(); _i < _a.length; _i++) {
                        var item = _a[_i];
                        item.GrantRevoke = null;
                    }
                    if (value != null) {
                        for (var _b = 0, value_1 = value; _b < value_1.length; _b++) {
                            var row = value_1[_b];
                            var r = this.view.getItemById(row.PermissionKey);
                            if (r) {
                                r.GrantRevoke = Q.coalesce(row.Granted, true);
                            }
                        }
                    }
                    this.setItems(this.getItems());
                },
                enumerable: false,
                configurable: true
            });
            Object.defineProperty(PermissionCheckEditor.prototype, "rolePermissions", {
                get: function () {
                    return Object.keys(this._rolePermissions);
                },
                set: function (value) {
                    this._rolePermissions = {};
                    if (value) {
                        for (var _i = 0, value_2 = value; _i < value_2.length; _i++) {
                            var k = value_2[_i];
                            this._rolePermissions[k] = true;
                        }
                    }
                    this.setItems(this.getItems());
                },
                enumerable: false,
                configurable: true
            });
            Object.defineProperty(PermissionCheckEditor.prototype, "implicitPermissions", {
                set: function (value) {
                    this._implicitPermissions = {};
                    if (value) {
                        for (var _i = 0, _a = Object.keys(value); _i < _a.length; _i++) {
                            var k = _a[_i];
                            this._implicitPermissions[k] = this._implicitPermissions[k] || {};
                            var l = value[k];
                            if (l) {
                                for (var _b = 0, l_1 = l; _b < l_1.length; _b++) {
                                    var s = l_1[_b];
                                    this._implicitPermissions[k][s] = true;
                                }
                            }
                        }
                    }
                },
                enumerable: false,
                configurable: true
            });
            PermissionCheckEditor = __decorate([
                Serenity.Decorators.registerEditor([Serenity.IGetEditValue, Serenity.ISetEditValue])
            ], PermissionCheckEditor);
            return PermissionCheckEditor;
        }(Serenity.DataGrid));
        Administration.PermissionCheckEditor = PermissionCheckEditor;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserPermissionDialog = /** @class */ (function (_super) {
            __extends(UserPermissionDialog, _super);
            function UserPermissionDialog(opt) {
                var _this = _super.call(this, opt) || this;
                _this.permissions = new Administration.PermissionCheckEditor(_this.byId('Permissions'), {
                    showRevoke: true
                });
                Administration.UserPermissionService.List({
                    UserID: _this.options.userID,
                    Module: null,
                    Submodule: null
                }, function (response) {
                    _this.permissions.value = response.Entities;
                });
                Administration.UserPermissionService.ListRolePermissions({
                    UserID: _this.options.userID,
                    Module: null,
                    Submodule: null,
                }, function (response) {
                    _this.permissions.rolePermissions = response.Entities;
                });
                _this.permissions.implicitPermissions = Q.getRemoteData('Administration.ImplicitPermissions');
                return _this;
            }
            UserPermissionDialog.prototype.getDialogOptions = function () {
                var _this = this;
                var opt = _super.prototype.getDialogOptions.call(this);
                opt.buttons = [
                    {
                        text: Q.text('Dialogs.OkButton'),
                        click: function (e) {
                            Administration.UserPermissionService.Update({
                                UserID: _this.options.userID,
                                Permissions: _this.permissions.value,
                                Module: null,
                                Submodule: null
                            }, function (response) {
                                _this.dialogClose();
                                window.setTimeout(function () { return Q.notifySuccess(Q.text('Site.UserPermissionDialog.SaveSuccess')); }, 0);
                            });
                        }
                    }, {
                        text: Q.text('Dialogs.CancelButton'),
                        click: function () { return _this.dialogClose(); }
                    }
                ];
                opt.title = Q.format(Q.text('Site.UserPermissionDialog.DialogTitle'), this.options.username);
                return opt;
            };
            UserPermissionDialog.prototype.getTemplate = function () {
                return '<div id="~_Permissions"></div>';
            };
            UserPermissionDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], UserPermissionDialog);
            return UserPermissionDialog;
        }(Serenity.TemplatedDialog));
        Administration.UserPermissionDialog = UserPermissionDialog;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var RoleCheckEditor = /** @class */ (function (_super) {
            __extends(RoleCheckEditor, _super);
            function RoleCheckEditor(div) {
                return _super.call(this, div) || this;
            }
            RoleCheckEditor.prototype.createToolbarExtensions = function () {
                var _this = this;
                _super.prototype.createToolbarExtensions.call(this);
                Serenity.GridUtils.addQuickSearchInputCustom(this.toolbar.element, function (field, text) {
                    _this.searchText = Select2.util.stripDiacritics(text || '').toUpperCase();
                    _this.view.setItems(_this.view.getItems(), true);
                });
            };
            RoleCheckEditor.prototype.getButtons = function () {
                return [];
            };
            RoleCheckEditor.prototype.getTreeItems = function () {
                return Administration.RoleRow.getLookup().items.map(function (role) { return ({
                    id: role.RoleId.toString(),
                    text: role.RoleName
                }); });
            };
            RoleCheckEditor.prototype.onViewFilter = function (item) {
                return _super.prototype.onViewFilter.call(this, item) &&
                    (Q.isEmptyOrNull(this.searchText) ||
                        Select2.util.stripDiacritics(item.text || '')
                            .toUpperCase().indexOf(this.searchText) >= 0);
            };
            RoleCheckEditor = __decorate([
                Serenity.Decorators.registerEditor()
            ], RoleCheckEditor);
            return RoleCheckEditor;
        }(Serenity.CheckTreeEditor));
        Administration.RoleCheckEditor = RoleCheckEditor;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Administration;
    (function (Administration) {
        var UserRoleDialog = /** @class */ (function (_super) {
            __extends(UserRoleDialog, _super);
            function UserRoleDialog(opt) {
                var _this = _super.call(this, opt) || this;
                _this.permissions = new Administration.RoleCheckEditor(_this.byId('Roles'));
                Administration.UserRoleService.List({
                    UserID: _this.options.userID
                }, function (response) {
                    _this.permissions.value = response.Entities.map(function (x) { return x.toString(); });
                });
                return _this;
            }
            UserRoleDialog.prototype.getDialogOptions = function () {
                var _this = this;
                var opt = _super.prototype.getDialogOptions.call(this);
                opt.buttons = [{
                        text: Q.text('Dialogs.OkButton'),
                        click: function () {
                            Q.serviceRequest('Administration/UserRole/Update', {
                                UserID: _this.options.userID,
                                Roles: _this.permissions.value.map(function (x) { return parseInt(x, 10); })
                            }, function (response) {
                                _this.dialogClose();
                                Q.notifySuccess(Q.text('Site.UserRoleDialog.SaveSuccess'));
                            });
                        }
                    }, {
                        text: Q.text('Dialogs.CancelButton'),
                        click: function () { return _this.dialogClose(); }
                    }];
                opt.title = Q.format(Q.text('Site.UserRoleDialog.DialogTitle'), this.options.username);
                return opt;
            };
            UserRoleDialog.prototype.getTemplate = function () {
                return "<div id='~_Roles'></div>";
            };
            UserRoleDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], UserRoleDialog);
            return UserRoleDialog;
        }(Serenity.TemplatedDialog));
        Administration.UserRoleDialog = UserRoleDialog;
    })(Administration = ProyectosZec.Administration || (ProyectosZec.Administration = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var LanguageList;
    (function (LanguageList) {
        function getValue() {
            var result = [];
            for (var _i = 0, _a = ProyectosZec.Administration.LanguageRow.getLookup().items; _i < _a.length; _i++) {
                var k = _a[_i];
                if (k.LanguageId !== 'en') {
                    result.push([k.Id.toString(), k.LanguageName]);
                }
            }
            return result;
        }
        LanguageList.getValue = getValue;
    })(LanguageList = ProyectosZec.LanguageList || (ProyectosZec.LanguageList = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../Common/Helpers/LanguageList.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var ScriptInitialization;
    (function (ScriptInitialization) {
        Q.Config.responsiveDialogs = true;
        Q.Config.rootNamespaces.push('ProyectosZec');
        Serenity.EntityDialog.defaultLanguageList = ProyectosZec.LanguageList.getValue;
        if ($.fn['colorbox']) {
            $.fn['colorbox'].settings.maxWidth = "95%";
            $.fn['colorbox'].settings.maxHeight = "95%";
        }
        window.onerror = Q.ErrorHandling.runtimeErrorHandler;
    })(ScriptInitialization = ProyectosZec.ScriptInitialization || (ProyectosZec.ScriptInitialization = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var BasicProgressDialog = /** @class */ (function (_super) {
        __extends(BasicProgressDialog, _super);
        function BasicProgressDialog() {
            var _this = _super.call(this) || this;
            _this.byId('ProgressBar').progressbar({
                max: 100,
                value: 0,
                change: function (e, v) {
                    _this.byId('ProgressLabel').text(_this.value + ' / ' + _this.max);
                }
            });
            return _this;
        }
        Object.defineProperty(BasicProgressDialog.prototype, "max", {
            get: function () {
                return this.byId('ProgressBar').progressbar().progressbar('option', 'max');
            },
            set: function (value) {
                this.byId('ProgressBar').progressbar().progressbar('option', 'max', value);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(BasicProgressDialog.prototype, "value", {
            get: function () {
                return this.byId('ProgressBar').progressbar('value');
            },
            set: function (value) {
                this.byId('ProgressBar').progressbar().progressbar('value', value);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(BasicProgressDialog.prototype, "title", {
            get: function () {
                return this.element.dialog().dialog('option', 'title');
            },
            set: function (value) {
                this.element.dialog().dialog('option', 'title', value);
            },
            enumerable: false,
            configurable: true
        });
        BasicProgressDialog.prototype.getDialogOptions = function () {
            var _this = this;
            var opt = _super.prototype.getDialogOptions.call(this);
            opt.title = Q.text('Site.BasicProgressDialog.PleaseWait');
            opt.width = 600;
            opt.buttons = [{
                    text: Q.text('Dialogs.CancelButton'),
                    click: function () {
                        _this.cancelled = true;
                        _this.element.closest('.ui-dialog')
                            .find('.ui-dialog-buttonpane .ui-button')
                            .attr('disabled', 'disabled')
                            .css('opacity', '0.5');
                        _this.element.dialog('option', 'title', Q.trimToNull(_this.cancelTitle) ||
                            Q.text('Site.BasicProgressDialog.CancelTitle'));
                    }
                }];
            return opt;
        };
        BasicProgressDialog.prototype.initDialog = function () {
            _super.prototype.initDialog.call(this);
            this.element.closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
        };
        BasicProgressDialog.prototype.getTemplate = function () {
            return ("<div class='s-DialogContent s-BasicProgressDialogContent'>" +
                "<div id='~_StatusText' class='status-text' ></div>" +
                "<div id='~_ProgressBar' class='progress-bar'>" +
                "<div id='~_ProgressLabel' class='progress-label' ></div>" +
                "</div>" +
                "</div>");
        };
        return BasicProgressDialog;
    }(Serenity.TemplatedDialog));
    ProyectosZec.BasicProgressDialog = BasicProgressDialog;
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var BulkServiceAction = /** @class */ (function () {
            function BulkServiceAction() {
            }
            BulkServiceAction.prototype.createProgressDialog = function () {
                this.progressDialog = new ProyectosZec.BasicProgressDialog();
                this.progressDialog.dialogOpen();
                this.progressDialog.max = this.keys.length;
                this.progressDialog.value = 0;
            };
            BulkServiceAction.prototype.getConfirmationFormat = function () {
                return Q.text('Site.BulkServiceAction.ConfirmationFormat');
            };
            BulkServiceAction.prototype.getConfirmationMessage = function (targetCount) {
                return Q.format(this.getConfirmationFormat(), targetCount);
            };
            BulkServiceAction.prototype.confirm = function (targetCount, action) {
                Q.confirm(this.getConfirmationMessage(targetCount), action);
            };
            BulkServiceAction.prototype.getNothingToProcessMessage = function () {
                return Q.text('Site.BulkServiceAction.NothingToProcess');
            };
            BulkServiceAction.prototype.nothingToProcess = function () {
                Q.notifyError(this.getNothingToProcessMessage());
            };
            BulkServiceAction.prototype.getParallelRequests = function () {
                return 1;
            };
            BulkServiceAction.prototype.getBatchSize = function () {
                return 1;
            };
            BulkServiceAction.prototype.startParallelExecution = function () {
                this.createProgressDialog();
                this.successCount = 0;
                this.errorCount = 0;
                this.pendingRequests = 0;
                this.completedRequests = 0;
                this.errorCount = 0;
                this.errorByKey = {};
                this.queue = this.keys.slice();
                this.queueIndex = 0;
                var parallelRequests = this.getParallelRequests();
                while (parallelRequests-- > 0) {
                    this.executeNextBatch();
                }
            };
            BulkServiceAction.prototype.serviceCallCleanup = function () {
                this.pendingRequests--;
                this.completedRequests++;
                var title = Q.text((this.progressDialog.cancelled ?
                    'Site.BasicProgressDialog.CancelTitle' : 'Site.BasicProgressDialog.PleaseWait'));
                title += ' (';
                if (this.successCount > 0) {
                    title += Q.format(Q.text('Site.BulkServiceAction.SuccessCount'), this.successCount);
                }
                if (this.errorCount > 0) {
                    if (this.successCount > 0) {
                        title += ', ';
                    }
                    title += Q.format(Q.text('Site.BulkServiceAction.ErrorCount'), this.errorCount);
                }
                this.progressDialog.title = title + ')';
                this.progressDialog.value = this.successCount + this.errorCount;
                if (!this.progressDialog.cancelled && this.progressDialog.value < this.keys.length) {
                    this.executeNextBatch();
                }
                else if (this.pendingRequests === 0) {
                    this.progressDialog.dialogClose();
                    this.showResults();
                    if (this.done) {
                        this.done();
                        this.done = null;
                    }
                }
            };
            BulkServiceAction.prototype.executeForBatch = function (batch) {
            };
            BulkServiceAction.prototype.executeNextBatch = function () {
                var batchSize = this.getBatchSize();
                var batch = [];
                while (true) {
                    if (batch.length >= batchSize) {
                        break;
                    }
                    if (this.queueIndex >= this.queue.length) {
                        break;
                    }
                    batch.push(this.queue[this.queueIndex++]);
                }
                if (batch.length > 0) {
                    this.pendingRequests++;
                    this.executeForBatch(batch);
                }
            };
            BulkServiceAction.prototype.getAllHadErrorsFormat = function () {
                return Q.text('Site.BulkServiceAction.AllHadErrorsFormat');
            };
            BulkServiceAction.prototype.showAllHadErrors = function () {
                Q.notifyError(Q.format(this.getAllHadErrorsFormat(), this.errorCount));
            };
            BulkServiceAction.prototype.getSomeHadErrorsFormat = function () {
                return Q.text('Site.BulkServiceAction.SomeHadErrorsFormat');
            };
            BulkServiceAction.prototype.showSomeHadErrors = function () {
                Q.notifyWarning(Q.format(this.getSomeHadErrorsFormat(), this.successCount, this.errorCount));
            };
            BulkServiceAction.prototype.getAllSuccessFormat = function () {
                return Q.text('Site.BulkServiceAction.AllSuccessFormat');
            };
            BulkServiceAction.prototype.showAllSuccess = function () {
                Q.notifySuccess(Q.format(this.getAllSuccessFormat(), this.successCount));
            };
            BulkServiceAction.prototype.showResults = function () {
                if (this.errorCount === 0 && this.successCount === 0) {
                    this.nothingToProcess();
                    return;
                }
                if (this.errorCount > 0 && this.successCount === 0) {
                    this.showAllHadErrors();
                    return;
                }
                if (this.errorCount > 0) {
                    this.showSomeHadErrors();
                    return;
                }
                this.showAllSuccess();
            };
            BulkServiceAction.prototype.execute = function (keys) {
                var _this = this;
                this.keys = keys;
                if (this.keys.length === 0) {
                    this.nothingToProcess();
                    return;
                }
                this.confirm(this.keys.length, function () { return _this.startParallelExecution(); });
            };
            BulkServiceAction.prototype.get_successCount = function () {
                return this.successCount;
            };
            BulkServiceAction.prototype.set_successCount = function (value) {
                this.successCount = value;
            };
            BulkServiceAction.prototype.get_errorCount = function () {
                return this.errorCount;
            };
            BulkServiceAction.prototype.set_errorCount = function (value) {
                this.errorCount = value;
            };
            return BulkServiceAction;
        }());
        Common.BulkServiceAction = BulkServiceAction;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var DialogUtils;
    (function (DialogUtils) {
        function pendingChangesConfirmation(element, hasPendingChanges) {
            element.on('dialogbeforeclose panelbeforeclose', function (e) {
                if (!Serenity.WX.hasOriginalEvent(e) || !hasPendingChanges()) {
                    return;
                }
                e.preventDefault();
                Q.confirm('You have pending changes. Save them?', function () { return element.find('div.save-and-close-button').click(); }, {
                    onNo: function () {
                        if (element.hasClass('ui-dialog-content'))
                            element.dialog('close');
                        else if (element.hasClass('s-Panel'))
                            Serenity.TemplatedDialog.closePanel(element);
                    }
                });
            });
        }
        DialogUtils.pendingChangesConfirmation = pendingChangesConfirmation;
    })(DialogUtils = ProyectosZec.DialogUtils || (ProyectosZec.DialogUtils = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var EnumSelectFormatter = /** @class */ (function () {
            function EnumSelectFormatter() {
                this.allowClear = true;
            }
            EnumSelectFormatter.prototype.format = function (ctx) {
                var enumType = Serenity.EnumTypeRegistry.get(this.enumKey);
                var sb = "<select>";
                if (this.allowClear) {
                    sb += '<option value="">';
                    sb += Q.htmlEncode(this.emptyItemText || Q.text("Controls.SelectEditor.EmptyItemText"));
                    sb += '</option>';
                }
                for (var _i = 0, _a = Object.keys(enumType).filter(function (v) { return !isNaN(parseInt(v, 10)); }); _i < _a.length; _i++) {
                    var x = _a[_i];
                    sb += '<option value="' + Q.attrEncode(x) + '"';
                    if (x == ctx.value)
                        sb += " selected";
                    var name = enumType[x];
                    sb += ">";
                    sb += Q.htmlEncode(Q.tryGetText("Enums." + this.enumKey + "." + name) || name);
                    sb += "</option>";
                }
                sb += "</select>";
                return sb;
            };
            __decorate([
                Serenity.Decorators.option()
            ], EnumSelectFormatter.prototype, "enumKey", void 0);
            __decorate([
                Serenity.Decorators.option()
            ], EnumSelectFormatter.prototype, "allowClear", void 0);
            __decorate([
                Serenity.Decorators.option()
            ], EnumSelectFormatter.prototype, "emptyItemText", void 0);
            EnumSelectFormatter = __decorate([
                Serenity.Decorators.registerFormatter()
            ], EnumSelectFormatter);
            return EnumSelectFormatter;
        }());
        Common.EnumSelectFormatter = EnumSelectFormatter;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var ExcelExportHelper;
        (function (ExcelExportHelper) {
            function createToolButton(options) {
                return {
                    hint: Q.coalesce(options.hint, 'Excel'),
                    title: Q.coalesce(options.title, ''),
                    cssClass: 'export-xlsx-button',
                    onClick: function () {
                        if (!options.onViewSubmit()) {
                            return;
                        }
                        var grid = options.grid;
                        var request = Q.deepClone(grid.getView().params);
                        request.Take = 0;
                        request.Skip = 0;
                        var sortBy = grid.getView().sortBy;
                        if (sortBy) {
                            request.Sort = sortBy;
                        }
                        request.IncludeColumns = [];
                        var columns = grid.getGrid().getColumns();
                        for (var _i = 0, columns_1 = columns; _i < columns_1.length; _i++) {
                            var column = columns_1[_i];
                            request.IncludeColumns.push(column.id || column.field);
                        }
                        Q.postToService({ service: options.service, request: request, target: '_blank' });
                    },
                    separator: options.separator
                };
            }
            ExcelExportHelper.createToolButton = createToolButton;
        })(ExcelExportHelper = Common.ExcelExportHelper || (Common.ExcelExportHelper = {}));
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var GridEditorBase = /** @class */ (function (_super) {
            __extends(GridEditorBase, _super);
            function GridEditorBase(container) {
                var _this = _super.call(this, container) || this;
                _this.nextId = 1;
                return _this;
            }
            GridEditorBase.prototype.getIdProperty = function () { return "__id"; };
            GridEditorBase.prototype.id = function (entity) {
                return entity[this.getIdProperty()];
            };
            GridEditorBase.prototype.getNextId = function () {
                return "`" + this.nextId++;
            };
            GridEditorBase.prototype.setNewId = function (entity) {
                entity[this.getIdProperty()] = this.getNextId();
            };
            GridEditorBase.prototype.save = function (opt, callback) {
                var _this = this;
                var request = opt.request;
                var row = Q.deepClone(request.Entity);
                var id = this.id(row);
                if (id == null) {
                    row[this.getIdProperty()] = this.getNextId();
                }
                if (!this.validateEntity(row, id)) {
                    return;
                }
                var items = this.view.getItems().slice();
                if (id == null) {
                    items.push(row);
                }
                else {
                    var index = Q.indexOf(items, function (x) { return _this.id(x) === id; });
                    items[index] = Q.deepClone({}, items[index], row);
                }
                this.setEntities(items);
                callback({});
            };
            GridEditorBase.prototype.deleteEntity = function (id) {
                this.view.deleteItem(id);
                return true;
            };
            GridEditorBase.prototype.validateEntity = function (row, id) {
                return true;
            };
            GridEditorBase.prototype.setEntities = function (items) {
                this.view.setItems(items, true);
            };
            GridEditorBase.prototype.getNewEntity = function () {
                return {};
            };
            GridEditorBase.prototype.getButtons = function () {
                var _this = this;
                return [{
                        title: this.getAddButtonCaption(),
                        cssClass: 'add-button',
                        onClick: function () {
                            _this.createEntityDialog(_this.getItemType(), function (dlg) {
                                var dialog = dlg;
                                dialog.onSave = function (opt, callback) { return _this.save(opt, callback); };
                                dialog.loadEntityAndOpenDialog(_this.getNewEntity());
                            });
                        }
                    }];
            };
            GridEditorBase.prototype.editItem = function (entityOrId) {
                var _this = this;
                var id = entityOrId;
                var item = this.view.getItemById(id);
                this.createEntityDialog(this.getItemType(), function (dlg) {
                    var dialog = dlg;
                    dialog.onDelete = function (opt, callback) {
                        if (!_this.deleteEntity(id)) {
                            return;
                        }
                        callback({});
                    };
                    dialog.onSave = function (opt, callback) { return _this.save(opt, callback); };
                    dialog.loadEntityAndOpenDialog(item);
                });
                ;
            };
            GridEditorBase.prototype.getEditValue = function (property, target) {
                target[property.name] = this.value;
            };
            GridEditorBase.prototype.setEditValue = function (source, property) {
                this.value = source[property.name];
            };
            Object.defineProperty(GridEditorBase.prototype, "value", {
                get: function () {
                    var p = this.getIdProperty();
                    return this.view.getItems().map(function (x) {
                        var y = Q.deepClone(x);
                        var id = y[p];
                        if (id && id.toString().charAt(0) == '`')
                            delete y[p];
                        return y;
                    });
                },
                set: function (value) {
                    var _this = this;
                    var p = this.getIdProperty();
                    this.view.setItems((value || []).map(function (x) {
                        var y = Q.deepClone(x);
                        if (y[p] == null)
                            y[p] = "`" + _this.getNextId();
                        return y;
                    }), true);
                },
                enumerable: false,
                configurable: true
            });
            GridEditorBase.prototype.getGridCanLoad = function () {
                return false;
            };
            GridEditorBase.prototype.usePager = function () {
                return false;
            };
            GridEditorBase.prototype.getInitialTitle = function () {
                return null;
            };
            GridEditorBase.prototype.createQuickSearchInput = function () {
            };
            GridEditorBase = __decorate([
                Serenity.Decorators.registerClass([Serenity.IGetEditValue, Serenity.ISetEditValue]),
                Serenity.Decorators.editor(),
                Serenity.Decorators.element("<div/>")
            ], GridEditorBase);
            return GridEditorBase;
        }(Serenity.EntityGrid));
        Common.GridEditorBase = GridEditorBase;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var GridEditorDialog = /** @class */ (function (_super) {
            __extends(GridEditorDialog, _super);
            function GridEditorDialog() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GridEditorDialog.prototype.getIdProperty = function () { return "__id"; };
            GridEditorDialog.prototype.destroy = function () {
                this.onSave = null;
                this.onDelete = null;
                _super.prototype.destroy.call(this);
            };
            GridEditorDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                // apply changes button doesn't work properly with in-memory grids yet
                if (this.applyChangesButton) {
                    this.applyChangesButton.hide();
                }
            };
            GridEditorDialog.prototype.saveHandler = function (options, callback) {
                this.onSave && this.onSave(options, callback);
            };
            GridEditorDialog.prototype.deleteHandler = function (options, callback) {
                this.onDelete && this.onDelete(options, callback);
            };
            GridEditorDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], GridEditorDialog);
            return GridEditorDialog;
        }(Serenity.EntityDialog));
        Common.GridEditorDialog = GridEditorDialog;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    /**
     * This is an editor widget but it only displays a text, not edits it.
     *
     */
    var StaticTextBlock = /** @class */ (function (_super) {
        __extends(StaticTextBlock, _super);
        function StaticTextBlock(container, options) {
            var _this = _super.call(this, container, options) || this;
            // hide the caption label for this editor if in a form. ugly hack
            if (_this.options.hideLabel)
                _this.element.closest('.field').find('.caption').hide();
            _this.updateElementContent();
            return _this;
        }
        StaticTextBlock.prototype.updateElementContent = function () {
            var text = Q.coalesce(this.options.text, this.value);
            // if isLocalText is set, text is actually a local text key
            if (this.options.isLocalText)
                text = Q.text(text);
            // don't html encode if isHtml option is true
            if (this.options.isHtml)
                this.element.html(text);
            else
                this.element.text(text);
        };
        /**
         * By implementing ISetEditValue interface, we allow this editor to display its field value.
         * But only do this when our text content is not explicitly set in options
         */
        StaticTextBlock.prototype.setEditValue = function (source, property) {
            if (this.options.text == null) {
                this.value = Q.coalesce(this.options.text, source[property.name]);
                this.updateElementContent();
            }
        };
        StaticTextBlock = __decorate([
            Serenity.Decorators.element("<div/>"),
            Serenity.Decorators.registerEditor([Serenity.ISetEditValue])
        ], StaticTextBlock);
        return StaticTextBlock;
    }(Serenity.Widget));
    ProyectosZec.StaticTextBlock = StaticTextBlock;
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var LanguageSelection = /** @class */ (function (_super) {
            __extends(LanguageSelection, _super);
            function LanguageSelection(select, currentLanguage) {
                var _this = _super.call(this, select) || this;
                currentLanguage = Q.coalesce(currentLanguage, 'en');
                _this.change(function (e) {
                    $.cookie('LanguagePreference', select.val(), {
                        path: Q.Config.applicationPath,
                        expires: 365
                    });
                    window.location.reload(true);
                });
                Q.getLookupAsync('Administration.Language').then(function (x) {
                    if (!Q.any(x.items, function (z) { return z.LanguageId === currentLanguage; })) {
                        var idx = currentLanguage.lastIndexOf('-');
                        if (idx >= 0) {
                            currentLanguage = currentLanguage.substr(0, idx);
                            if (!Q.any(x.items, function (y) { return y.LanguageId === currentLanguage; })) {
                                currentLanguage = 'en';
                            }
                        }
                        else {
                            currentLanguage = 'en';
                        }
                    }
                    for (var _i = 0, _a = x.items; _i < _a.length; _i++) {
                        var l = _a[_i];
                        Q.addOption(select, l.LanguageId, l.LanguageName);
                    }
                    select.val(currentLanguage);
                });
                return _this;
            }
            return LanguageSelection;
        }(Serenity.Widget));
        Common.LanguageSelection = LanguageSelection;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var SidebarSearch = /** @class */ (function (_super) {
            __extends(SidebarSearch, _super);
            function SidebarSearch(input, menuUL) {
                var _this = _super.call(this, input) || this;
                new Serenity.QuickSearchInput(input, {
                    onSearch: function (field, text, success) {
                        _this.updateMatchFlags(text);
                        success(true);
                    }
                });
                _this.menuUL = menuUL;
                return _this;
            }
            SidebarSearch.prototype.updateMatchFlags = function (text) {
                var liList = this.menuUL.find('li').removeClass('non-match');
                text = Q.trimToNull(text);
                if (text == null) {
                    liList.show();
                    liList.removeClass('expanded');
                    return;
                }
                var parts = text.replace(',', ' ').split(' ').filter(function (x) { return !Q.isTrimmedEmpty(x); });
                for (var i = 0; i < parts.length; i++) {
                    parts[i] = Q.trimToNull(Select2.util.stripDiacritics(parts[i]).toUpperCase());
                }
                var items = liList;
                items.each(function (idx, e) {
                    var x = $(e);
                    var title = Select2.util.stripDiacritics(Q.coalesce(x.text(), '').toUpperCase());
                    for (var _i = 0, parts_1 = parts; _i < parts_1.length; _i++) {
                        var p = parts_1[_i];
                        if (p != null && !(title.indexOf(p) !== -1)) {
                            x.addClass('non-match');
                            break;
                        }
                    }
                });
                var matchingItems = items.not('.non-match');
                var visibles = matchingItems.parents('li').add(matchingItems);
                var nonVisibles = liList.not(visibles);
                nonVisibles.hide().addClass('non-match');
                visibles.show();
                liList.addClass('expanded');
            };
            return SidebarSearch;
        }(Serenity.Widget));
        Common.SidebarSearch = SidebarSearch;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var ThemeSelection = /** @class */ (function (_super) {
            __extends(ThemeSelection, _super);
            function ThemeSelection(select) {
                var _this = _super.call(this, select) || this;
                _this.change(function (e) {
                    var path = Q.Config.applicationPath;
                    if (path && path != '/' && Q.endsWith(path, '/'))
                        path = path.substr(0, path.length - 1);
                    $.cookie('ThemePreference', select.val(), {
                        path: path,
                        expires: 365
                    });
                    var theme = select.val() || '';
                    var darkSidebar = theme.indexOf('light') < 0;
                    $('body').removeClass('skin-' + _this.getCurrentTheme());
                    $('body').addClass('skin-' + theme)
                        .toggleClass('dark-sidebar', darkSidebar)
                        .toggleClass('light-sidebar', !darkSidebar);
                });
                Q.addOption(select, 'blue', Q.text('Site.Layout.ThemeBlue'));
                Q.addOption(select, 'blue-light', Q.text('Site.Layout.ThemeBlueLight'));
                Q.addOption(select, 'purple', Q.text('Site.Layout.ThemePurple'));
                Q.addOption(select, 'purple-light', Q.text('Site.Layout.ThemePurpleLight'));
                Q.addOption(select, 'red', Q.text('Site.Layout.ThemeRed'));
                Q.addOption(select, 'red-light', Q.text('Site.Layout.ThemeRedLight'));
                Q.addOption(select, 'green', Q.text('Site.Layout.ThemeGreen'));
                Q.addOption(select, 'green-light', Q.text('Site.Layout.ThemeGreenLight'));
                Q.addOption(select, 'yellow', Q.text('Site.Layout.ThemeYellow'));
                Q.addOption(select, 'yellow-light', Q.text('Site.Layout.ThemeYellowLight'));
                Q.addOption(select, 'black', Q.text('Site.Layout.ThemeBlack'));
                Q.addOption(select, 'black-light', Q.text('Site.Layout.ThemeBlackLight'));
                select.val(_this.getCurrentTheme());
                return _this;
            }
            ThemeSelection.prototype.getCurrentTheme = function () {
                var skinClass = Q.first(($('body').attr('class') || '').split(' '), function (x) { return Q.startsWith(x, 'skin-'); });
                if (skinClass) {
                    return skinClass.substr(5);
                }
                return 'blue';
            };
            return ThemeSelection;
        }(Serenity.Widget));
        Common.ThemeSelection = ThemeSelection;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var PdfExportHelper;
        (function (PdfExportHelper) {
            function toAutoTableColumns(srcColumns, columnStyles, columnTitles) {
                return srcColumns.map(function (src) {
                    var col = {
                        dataKey: src.id || src.field,
                        title: src.name || ''
                    };
                    if (columnTitles && columnTitles[col.dataKey] != null)
                        col.title = columnTitles[col.dataKey];
                    var style = {};
                    if ((src.cssClass || '').indexOf("align-right") >= 0)
                        style.halign = 'right';
                    else if ((src.cssClass || '').indexOf("align-center") >= 0)
                        style.halign = 'center';
                    columnStyles[col.dataKey] = style;
                    return col;
                });
            }
            function toAutoTableData(entities, keys, srcColumns) {
                var el = document.createElement('span');
                var row = 0;
                return entities.map(function (item) {
                    var dst = {};
                    for (var cell = 0; cell < srcColumns.length; cell++) {
                        var src = srcColumns[cell];
                        var fld = src.field || '';
                        var key = keys[cell];
                        var txt = void 0;
                        var html = void 0;
                        if (src.formatter) {
                            html = src.formatter(row, cell, item[fld], src, item);
                        }
                        else if (src.format) {
                            html = src.format({ row: row, cell: cell, item: item, value: item[fld] });
                        }
                        else {
                            dst[key] = item[fld];
                            continue;
                        }
                        if (!html || (html.indexOf('<') < 0 && html.indexOf('&') < 0))
                            dst[key] = html;
                        else {
                            el.innerHTML = html;
                            if (el.children.length == 1 &&
                                $(el.children[0]).is(":input")) {
                                dst[key] = $(el.children[0]).val();
                            }
                            else if (el.children.length == 1 &&
                                $(el.children).is('.check-box')) {
                                dst[key] = $(el.children).hasClass("checked") ? "X" : "";
                            }
                            else
                                dst[key] = el.textContent || '';
                        }
                    }
                    row++;
                    return dst;
                });
            }
            function exportToPdf(options) {
                var g = options.grid;
                if (!options.onViewSubmit())
                    return;
                includeAutoTable();
                var request = Q.deepClone(g.view.params);
                request.Take = 0;
                request.Skip = 0;
                var sortBy = g.view.sortBy;
                if (sortBy != null)
                    request.Sort = sortBy;
                var gridColumns = g.slickGrid.getColumns();
                gridColumns = gridColumns.filter(function (x) { return x.id !== "__select__"; });
                request.IncludeColumns = [];
                for (var _i = 0, gridColumns_1 = gridColumns; _i < gridColumns_1.length; _i++) {
                    var column = gridColumns_1[_i];
                    request.IncludeColumns.push(column.id || column.field);
                }
                Q.serviceCall({
                    url: g.view.url,
                    request: request,
                    onSuccess: function (response) {
                        var doc = new jsPDF('l', 'pt');
                        var srcColumns = gridColumns;
                        var columnStyles = {};
                        var columns = toAutoTableColumns(srcColumns, columnStyles, options.columnTitles);
                        var keys = columns.map(function (x) { return x.dataKey; });
                        var entities = response.Entities || [];
                        var data = toAutoTableData(entities, keys, srcColumns);
                        doc.setFontSize(options.titleFontSize || 10);
                        doc.setFontStyle('bold');
                        var reportTitle = options.reportTitle || g.getTitle() || "Report";
                        doc.autoTableText(reportTitle, doc.internal.pageSize.width / 2, options.titleTop || 25, { halign: 'center' });
                        var totalPagesExp = "{{T}}";
                        var pageNumbers = options.pageNumbers == null || options.pageNumbers;
                        var autoOptions = $.extend({
                            margin: { top: 25, left: 25, right: 25, bottom: pageNumbers ? 25 : 30 },
                            startY: 60,
                            styles: {
                                fontSize: 8,
                                overflow: 'linebreak',
                                cellPadding: 2,
                                valign: 'middle'
                            },
                            columnStyles: columnStyles
                        }, options.tableOptions);
                        if (pageNumbers) {
                            var footer = function (data) {
                                var str = data.pageCount;
                                // Total page number plugin only available in jspdf v1.0+
                                if (typeof doc.putTotalPages === 'function') {
                                    str = str + " / " + totalPagesExp;
                                }
                                doc.autoTableText(str, doc.internal.pageSize.width / 2, doc.internal.pageSize.height - autoOptions.margin.bottom, {
                                    halign: 'center'
                                });
                            };
                            autoOptions.afterPageContent = footer;
                        }
                        // Print header of page
                        if (options.printDateTimeHeader == null || options.printDateTimeHeader) {
                            var beforePage = function (data) {
                                doc.setFontStyle('normal');
                                doc.setFontSize(8);
                                // Date and time of the report
                                doc.autoTableText(Q.formatDate(new Date(), "dd-MM-yyyy HH:mm"), doc.internal.pageSize.width - autoOptions.margin.right, 13, {
                                    halign: 'right'
                                });
                            };
                            autoOptions.beforePageContent = beforePage;
                        }
                        doc.autoTable(columns, data, autoOptions);
                        if (typeof doc.putTotalPages === 'function') {
                            doc.putTotalPages(totalPagesExp);
                        }
                        if (!options.output || options.output == "file") {
                            var fileName = options.fileName || options.reportTitle || "{0}_{1}.pdf";
                            fileName = Q.format(fileName, g.getTitle() || "report", Q.formatDate(new Date(), "yyyyMMdd_HHmm"));
                            doc.save(fileName);
                            return;
                        }
                        if (options.autoPrint)
                            doc.autoPrint();
                        var output = options.output;
                        if (output == 'newwindow' || '_blank')
                            output = 'dataurlnewwindow';
                        else if (output == 'window')
                            output = 'datauri';
                        if (output == 'datauri')
                            doc.output(output);
                        else {
                            var tmpOut = doc.output('datauristring');
                            if (output == 'dataurlnewwindow') {
                                var fileTmpName = options.reportTitle || g.getTitle();
                                var url_with_name = tmpOut.replace("data:application/pdf;", "data:application/pdf;name=" + fileTmpName + ".pdf;");
                                var html = '<html>' +
                                    '<style>html, body { padding: 0; margin: 0; } iframe { width: 100%; height: 100%; border: 0;}  </style>' +
                                    '<body>' +
                                    '<p></p>' +
                                    '<iframe type="application/pdf" src="' + url_with_name + '"></iframe>' +
                                    '</body></html>';
                                var a = window.open("about:blank", "_blank");
                                a.document.write(html);
                                a.document.close();
                            }
                        }
                    }
                });
            }
            PdfExportHelper.exportToPdf = exportToPdf;
            function createToolButton(options) {
                return {
                    title: options.title || '',
                    hint: options.hint || 'PDF',
                    cssClass: 'export-pdf-button',
                    onClick: function () { return exportToPdf(options); },
                    separator: options.separator
                };
            }
            PdfExportHelper.createToolButton = createToolButton;
            function includeJsPDF() {
                if (typeof jsPDF !== "undefined")
                    return;
                var script = $("jsPDFScript");
                if (script.length > 0)
                    return;
                $("<script/>")
                    .attr("type", "text/javascript")
                    .attr("id", "jsPDFScript")
                    .attr("src", Q.resolveUrl("~/Scripts/jspdf.min.js"))
                    .appendTo(document.head);
            }
            function includeAutoTable() {
                includeJsPDF();
                if (typeof jsPDF === "undefined" ||
                    typeof jsPDF.API == "undefined" ||
                    typeof jsPDF.API.autoTable !== "undefined")
                    return;
                var script = $("jsPDFAutoTableScript");
                if (script.length > 0)
                    return;
                $("<script/>")
                    .attr("type", "text/javascript")
                    .attr("id", "jsPDFAutoTableScript")
                    .attr("src", Q.resolveUrl("~/Scripts/jspdf.plugin.autotable.min.js"))
                    .appendTo(document.head);
            }
        })(PdfExportHelper = Common.PdfExportHelper || (Common.PdfExportHelper = {}));
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var ReportDialog = /** @class */ (function (_super) {
            __extends(ReportDialog, _super);
            function ReportDialog(options) {
                var _this = _super.call(this, options) || this;
                _this.updateInterface();
                _this.loadReport(_this.options.reportKey);
                return _this;
            }
            ReportDialog.prototype.getDialogButtons = function () {
                return null;
            };
            ReportDialog.prototype.createPropertyGrid = function () {
                this.propertyGrid && this.byId('PropertyGrid').html('').attr('class', '');
                this.propertyGrid = new Serenity.PropertyGrid(this.byId('PropertyGrid'), {
                    idPrefix: this.idPrefix,
                    useCategories: true,
                    items: this.report.Properties
                }).init(null);
            };
            ReportDialog.prototype.loadReport = function (reportKey) {
                var _this = this;
                Q.serviceCall({
                    url: Q.resolveUrl('~/Report/Retrieve'),
                    request: {
                        ReportKey: reportKey
                    },
                    onSuccess: function (response) {
                        _this.report = response;
                        _this.element.dialog().dialog('option', 'title', _this.report.Title);
                        _this.createPropertyGrid();
                        _this.propertyGrid.load(_this.report.InitialSettings || {});
                        _this.updateInterface();
                        _this.dialogOpen();
                    }
                });
            };
            ReportDialog.prototype.updateInterface = function () {
                this.toolbar.findButton('print-preview-button')
                    .toggle(this.report && !this.report.IsDataOnlyReport);
                this.toolbar.findButton('export-pdf-button')
                    .toggle(this.report && !this.report.IsDataOnlyReport);
                this.toolbar.findButton('export-xlsx-button')
                    .toggle(this.report && this.report.IsDataOnlyReport);
            };
            ReportDialog.prototype.executeReport = function (target, ext, download) {
                if (!this.validateForm()) {
                    return;
                }
                var opt = {};
                this.propertyGrid.save(opt);
                Common.ReportHelper.execute({
                    download: download,
                    reportKey: this.report.ReportKey,
                    extension: ext,
                    target: target,
                    params: opt
                });
            };
            ReportDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                return [
                    {
                        title: 'Preview',
                        cssClass: 'print-preview-button',
                        onClick: function () { return _this.executeReport('_blank', null, false); }
                    },
                    {
                        title: 'PDF',
                        cssClass: 'export-pdf-button',
                        onClick: function () { return _this.executeReport('_blank', 'pdf', true); }
                    },
                    {
                        title: 'Excel',
                        cssClass: 'export-xlsx-button',
                        onClick: function () { return _this.executeReport('_blank', 'xlsx', true); }
                    }
                ];
            };
            return ReportDialog;
        }(Serenity.TemplatedDialog));
        Common.ReportDialog = ReportDialog;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var ReportHelper;
        (function (ReportHelper) {
            function createToolButton(options) {
                return {
                    title: Q.coalesce(options.title, 'Report'),
                    cssClass: Q.coalesce(options.cssClass, 'print-button'),
                    icon: options.icon,
                    onClick: function () {
                        ReportHelper.execute(options);
                    }
                };
            }
            ReportHelper.createToolButton = createToolButton;
            function execute(options) {
                var opt = options.getParams ? options.getParams() : options.params;
                Q.postToUrl({
                    url: '~/Report/' + (options.download ? 'Download' : 'Render'),
                    params: {
                        key: options.reportKey,
                        ext: Q.coalesce(options.extension, 'pdf'),
                        opt: opt ? $.toJSON(opt) : ''
                    },
                    target: Q.coalesce(options.target, '_blank')
                });
            }
            ReportHelper.execute = execute;
        })(ReportHelper = Common.ReportHelper || (Common.ReportHelper = {}));
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var ReportPage = /** @class */ (function (_super) {
            __extends(ReportPage, _super);
            function ReportPage(element) {
                var _this = _super.call(this, element) || this;
                $('.report-link', element).click(function (e) { return _this.reportLinkClick(e); });
                $('div.line', element).click(function (e) { return _this.categoryClick(e); });
                new Serenity.QuickSearchInput($('.s-QuickSearchBar input', element), {
                    onSearch: function (field, text, done) {
                        _this.updateMatchFlags(text);
                        done(true);
                    }
                });
                return _this;
            }
            ReportPage.prototype.updateMatchFlags = function (text) {
                var liList = $('.report-list', this.element).find('li').removeClass('non-match');
                text = Q.trimToNull(text);
                if (!text) {
                    liList.children('ul').hide();
                    liList.show().removeClass('expanded');
                    return;
                }
                text = Select2.util.stripDiacritics(text).toUpperCase();
                var reportItems = liList.filter('.report-item');
                reportItems.each(function (ix, e) {
                    var x = $(e);
                    var title = Select2.util.stripDiacritics(Q.coalesce(x.text(), '').toUpperCase());
                    if (title.indexOf(text) < 0) {
                        x.addClass('non-match');
                    }
                });
                var matchingItems = reportItems.not('.non-match');
                var visibles = matchingItems.parents('li').add(matchingItems);
                var nonVisibles = liList.not(visibles);
                nonVisibles.hide().addClass('non-match');
                visibles.show();
                if (visibles.length <= 100) {
                    liList.children('ul').show();
                    liList.addClass('expanded');
                }
            };
            ReportPage.prototype.categoryClick = function (e) {
                var li = $(e.target).closest('li');
                if (li.hasClass('expanded')) {
                    li.find('ul').hide('fast');
                    li.removeClass('expanded');
                    li.find('li').removeClass('expanded');
                }
                else {
                    li.addClass('expanded');
                    li.children('ul').show('fast');
                    if (li.children('ul').children('li').length === 1 && !li.children('ul').children('li').hasClass('expanded')) {
                        li.children('ul').children('li').children('.line').click();
                    }
                }
            };
            ReportPage.prototype.reportLinkClick = function (e) {
                e.preventDefault();
                new Common.ReportDialog({
                    reportKey: $(e.target).data('key')
                }).dialogOpen();
            };
            return ReportPage;
        }(Serenity.Widget));
        Common.ReportPage = ReportPage;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Common;
    (function (Common) {
        var UserPreferenceStorage = /** @class */ (function () {
            function UserPreferenceStorage() {
            }
            UserPreferenceStorage.prototype.getItem = function (key) {
                var value;
                Common.UserPreferenceService.Retrieve({
                    PreferenceType: "UserPreferenceStorage",
                    Name: key
                }, function (response) { return value = response.Value; }, {
                    async: false
                });
                return value;
            };
            UserPreferenceStorage.prototype.setItem = function (key, data) {
                Common.UserPreferenceService.Update({
                    PreferenceType: "UserPreferenceStorage",
                    Name: key,
                    Value: data
                });
            };
            return UserPreferenceStorage;
        }());
        Common.UserPreferenceStorage = UserPreferenceStorage;
    })(Common = ProyectosZec.Common || (ProyectosZec.Common = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var CapitalDialog = /** @class */ (function (_super) {
            __extends(CapitalDialog, _super);
            function CapitalDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new CuadroMandos.CapitalForm(_this.idPrefix);
                return _this;
            }
            CapitalDialog.prototype.getFormKey = function () { return CuadroMandos.CapitalForm.formKey; };
            CapitalDialog.prototype.getIdProperty = function () { return CuadroMandos.CapitalRow.idProperty; };
            CapitalDialog.prototype.getLocalTextPrefix = function () { return CuadroMandos.CapitalRow.localTextPrefix; };
            CapitalDialog.prototype.getNameProperty = function () { return CuadroMandos.CapitalRow.nameProperty; };
            CapitalDialog.prototype.getService = function () { return CuadroMandos.CapitalService.baseUrl; };
            CapitalDialog.prototype.getDeletePermission = function () { return CuadroMandos.CapitalRow.deletePermission; };
            CapitalDialog.prototype.getInsertPermission = function () { return CuadroMandos.CapitalRow.insertPermission; };
            CapitalDialog.prototype.getUpdatePermission = function () { return CuadroMandos.CapitalRow.updatePermission; };
            CapitalDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], CapitalDialog);
            return CapitalDialog;
        }(Serenity.EntityDialog));
        CuadroMandos.CapitalDialog = CapitalDialog;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var CapitalGrid = /** @class */ (function (_super) {
            __extends(CapitalGrid, _super);
            function CapitalGrid(container) {
                return _super.call(this, container) || this;
            }
            CapitalGrid.prototype.getColumnsKey = function () { return 'CuadroMandos.Capital'; };
            CapitalGrid.prototype.getDialogType = function () { return CuadroMandos.CapitalDialog; };
            CapitalGrid.prototype.getIdProperty = function () { return CuadroMandos.CapitalRow.idProperty; };
            CapitalGrid.prototype.getInsertPermission = function () { return CuadroMandos.CapitalRow.insertPermission; };
            CapitalGrid.prototype.getLocalTextPrefix = function () { return CuadroMandos.CapitalRow.localTextPrefix; };
            CapitalGrid.prototype.getService = function () { return CuadroMandos.CapitalService.baseUrl; };
            CapitalGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], CapitalGrid);
            return CapitalGrid;
        }(Serenity.EntityGrid));
        CuadroMandos.CapitalGrid = CapitalGrid;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var EstadosDialog = /** @class */ (function (_super) {
            __extends(EstadosDialog, _super);
            function EstadosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new CuadroMandos.EstadosForm(_this.idPrefix);
                return _this;
            }
            EstadosDialog.prototype.getFormKey = function () { return CuadroMandos.EstadosForm.formKey; };
            EstadosDialog.prototype.getIdProperty = function () { return CuadroMandos.EstadosRow.idProperty; };
            EstadosDialog.prototype.getLocalTextPrefix = function () { return CuadroMandos.EstadosRow.localTextPrefix; };
            EstadosDialog.prototype.getNameProperty = function () { return CuadroMandos.EstadosRow.nameProperty; };
            EstadosDialog.prototype.getService = function () { return CuadroMandos.EstadosService.baseUrl; };
            EstadosDialog.prototype.getDeletePermission = function () { return CuadroMandos.EstadosRow.deletePermission; };
            EstadosDialog.prototype.getInsertPermission = function () { return CuadroMandos.EstadosRow.insertPermission; };
            EstadosDialog.prototype.getUpdatePermission = function () { return CuadroMandos.EstadosRow.updatePermission; };
            EstadosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EstadosDialog);
            return EstadosDialog;
        }(Serenity.EntityDialog));
        CuadroMandos.EstadosDialog = EstadosDialog;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var EstadosGrid = /** @class */ (function (_super) {
            __extends(EstadosGrid, _super);
            function EstadosGrid(container) {
                return _super.call(this, container) || this;
            }
            EstadosGrid.prototype.getColumnsKey = function () { return 'CuadroMandos.Estados'; };
            EstadosGrid.prototype.getDialogType = function () { return CuadroMandos.EstadosDialog; };
            EstadosGrid.prototype.getIdProperty = function () { return CuadroMandos.EstadosRow.idProperty; };
            EstadosGrid.prototype.getInsertPermission = function () { return CuadroMandos.EstadosRow.insertPermission; };
            EstadosGrid.prototype.getLocalTextPrefix = function () { return CuadroMandos.EstadosRow.localTextPrefix; };
            EstadosGrid.prototype.getService = function () { return CuadroMandos.EstadosService.baseUrl; };
            EstadosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EstadosGrid);
            return EstadosGrid;
        }(Serenity.EntityGrid));
        CuadroMandos.EstadosGrid = EstadosGrid;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var IslasDialog = /** @class */ (function (_super) {
            __extends(IslasDialog, _super);
            function IslasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new CuadroMandos.IslasForm(_this.idPrefix);
                return _this;
            }
            IslasDialog.prototype.getFormKey = function () { return CuadroMandos.IslasForm.formKey; };
            IslasDialog.prototype.getIdProperty = function () { return CuadroMandos.IslasRow.idProperty; };
            IslasDialog.prototype.getLocalTextPrefix = function () { return CuadroMandos.IslasRow.localTextPrefix; };
            IslasDialog.prototype.getNameProperty = function () { return CuadroMandos.IslasRow.nameProperty; };
            IslasDialog.prototype.getService = function () { return CuadroMandos.IslasService.baseUrl; };
            IslasDialog.prototype.getDeletePermission = function () { return CuadroMandos.IslasRow.deletePermission; };
            IslasDialog.prototype.getInsertPermission = function () { return CuadroMandos.IslasRow.insertPermission; };
            IslasDialog.prototype.getUpdatePermission = function () { return CuadroMandos.IslasRow.updatePermission; };
            IslasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], IslasDialog);
            return IslasDialog;
        }(Serenity.EntityDialog));
        CuadroMandos.IslasDialog = IslasDialog;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var IslasGrid = /** @class */ (function (_super) {
            __extends(IslasGrid, _super);
            function IslasGrid(container) {
                return _super.call(this, container) || this;
            }
            IslasGrid.prototype.getColumnsKey = function () { return 'CuadroMandos.Islas'; };
            IslasGrid.prototype.getDialogType = function () { return CuadroMandos.IslasDialog; };
            IslasGrid.prototype.getIdProperty = function () { return CuadroMandos.IslasRow.idProperty; };
            IslasGrid.prototype.getInsertPermission = function () { return CuadroMandos.IslasRow.insertPermission; };
            IslasGrid.prototype.getLocalTextPrefix = function () { return CuadroMandos.IslasRow.localTextPrefix; };
            IslasGrid.prototype.getService = function () { return CuadroMandos.IslasService.baseUrl; };
            IslasGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], IslasGrid);
            return IslasGrid;
        }(Serenity.EntityGrid));
        CuadroMandos.IslasGrid = IslasGrid;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var PrescriptorinversorDialog = /** @class */ (function (_super) {
            __extends(PrescriptorinversorDialog, _super);
            function PrescriptorinversorDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new CuadroMandos.PrescriptorinversorForm(_this.idPrefix);
                return _this;
            }
            PrescriptorinversorDialog.prototype.getFormKey = function () { return CuadroMandos.PrescriptorinversorForm.formKey; };
            PrescriptorinversorDialog.prototype.getIdProperty = function () { return CuadroMandos.PrescriptorinversorRow.idProperty; };
            PrescriptorinversorDialog.prototype.getLocalTextPrefix = function () { return CuadroMandos.PrescriptorinversorRow.localTextPrefix; };
            PrescriptorinversorDialog.prototype.getNameProperty = function () { return CuadroMandos.PrescriptorinversorRow.nameProperty; };
            PrescriptorinversorDialog.prototype.getService = function () { return CuadroMandos.PrescriptorinversorService.baseUrl; };
            PrescriptorinversorDialog.prototype.getDeletePermission = function () { return CuadroMandos.PrescriptorinversorRow.deletePermission; };
            PrescriptorinversorDialog.prototype.getInsertPermission = function () { return CuadroMandos.PrescriptorinversorRow.insertPermission; };
            PrescriptorinversorDialog.prototype.getUpdatePermission = function () { return CuadroMandos.PrescriptorinversorRow.updatePermission; };
            PrescriptorinversorDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], PrescriptorinversorDialog);
            return PrescriptorinversorDialog;
        }(Serenity.EntityDialog));
        CuadroMandos.PrescriptorinversorDialog = PrescriptorinversorDialog;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var PrescriptorinversorGrid = /** @class */ (function (_super) {
            __extends(PrescriptorinversorGrid, _super);
            function PrescriptorinversorGrid(container) {
                return _super.call(this, container) || this;
            }
            PrescriptorinversorGrid.prototype.getColumnsKey = function () { return 'CuadroMandos.Prescriptorinversor'; };
            PrescriptorinversorGrid.prototype.getDialogType = function () { return CuadroMandos.PrescriptorinversorDialog; };
            PrescriptorinversorGrid.prototype.getIdProperty = function () { return CuadroMandos.PrescriptorinversorRow.idProperty; };
            PrescriptorinversorGrid.prototype.getInsertPermission = function () { return CuadroMandos.PrescriptorinversorRow.insertPermission; };
            PrescriptorinversorGrid.prototype.getLocalTextPrefix = function () { return CuadroMandos.PrescriptorinversorRow.localTextPrefix; };
            PrescriptorinversorGrid.prototype.getService = function () { return CuadroMandos.PrescriptorinversorService.baseUrl; };
            PrescriptorinversorGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], PrescriptorinversorGrid);
            return PrescriptorinversorGrid;
        }(Serenity.EntityGrid));
        CuadroMandos.PrescriptorinversorGrid = PrescriptorinversorGrid;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var PresentadasDialog = /** @class */ (function (_super) {
            __extends(PresentadasDialog, _super);
            function PresentadasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new CuadroMandos.ProyectosForm(_this.idPrefix);
                return _this;
            }
            PresentadasDialog.prototype.getFormKey = function () { return CuadroMandos.ProyectosForm.formKey; };
            PresentadasDialog.prototype.getIdProperty = function () { return CuadroMandos.ProyectosRow.idProperty; };
            PresentadasDialog.prototype.getLocalTextPrefix = function () { return CuadroMandos.ProyectosRow.localTextPrefix; };
            PresentadasDialog.prototype.getNameProperty = function () { return CuadroMandos.ProyectosRow.nameProperty; };
            PresentadasDialog.prototype.getService = function () { return CuadroMandos.PresentadasService.baseUrl; };
            PresentadasDialog.prototype.getDeletePermission = function () { return CuadroMandos.ProyectosRow.deletePermission; };
            PresentadasDialog.prototype.getInsertPermission = function () { return CuadroMandos.ProyectosRow.insertPermission; };
            PresentadasDialog.prototype.getUpdatePermission = function () { return CuadroMandos.ProyectosRow.updatePermission; };
            PresentadasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], PresentadasDialog);
            return PresentadasDialog;
        }(Serenity.EntityDialog));
        CuadroMandos.PresentadasDialog = PresentadasDialog;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var PresentadasGrid = /** @class */ (function (_super) {
            __extends(PresentadasGrid, _super);
            function PresentadasGrid(container) {
                return _super.call(this, container) || this;
            }
            PresentadasGrid.prototype.getColumnsKey = function () { return 'CuadroMandos.Presentadas'; };
            PresentadasGrid.prototype.getDialogType = function () { return CuadroMandos.PresentadasDialog; };
            PresentadasGrid.prototype.getIdProperty = function () { return CuadroMandos.ProyectosRow.idProperty; };
            PresentadasGrid.prototype.getInsertPermission = function () { return CuadroMandos.ProyectosRow.insertPermission; };
            PresentadasGrid.prototype.getLocalTextPrefix = function () { return CuadroMandos.ProyectosRow.localTextPrefix; };
            PresentadasGrid.prototype.getService = function () { return CuadroMandos.PresentadasService.baseUrl; };
            // Añadidos
            // Primero campo de ordenación por defecto en este caso, fecha de presentación
            PresentadasGrid.prototype.getDefaultSortBy = function () {
                return ["FechaPresentacion" /* FechaPresentacion */];
            };
            // Agrupar y sumar 
            PresentadasGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                this.view.setSummaryOptions({
                    aggregators: [
                        new Slick.Aggregators.Sum("Empleos" /* Empleos */),
                        new Slick.Aggregators.Sum("EmpleoReal" /* EmpleoReal */),
                        new Slick.Aggregators.Sum("Inversion" /* Inversion */),
                        new Slick.Aggregators.Sum("InversionReal" /* InversionReal */)
                    ]
                });
                return grid;
            };
            // Añadimos la fila de pie para los totales
            PresentadasGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                opt.showFooterRow = true;
                return opt;
            };
            PresentadasGrid.prototype.usePager = function () {
                return false;
            };
            // Ahora los botones. Quitamos el botón de añadir y ponemos los de Excel, Pdf y Agrupar por técnico
            PresentadasGrid.prototype.getButtons = function () {
                var _this = this;
                // call base method to get list of buttons
                // by default, base entity grid adds a few buttons, 
                // add, refresh, column selection in order.
                var buttons = _super.prototype.getButtons.call(this);
                // METHOD 3 - recommended
                // remove by splicing, but this time find button index
                // by its css class. it is the best and safer method
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                // Ahora añadimos el resto de botones
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'CuadroMandos/Proyectos/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.push({
                    title: 'Técnico',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Técnico: ' + x.value + ' (' + x.count + ' Proyectos)'; },
                            getter: "Tecnico" /* Tecnico */
                        }]); }
                });
                buttons.push({
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: function () { return _this.view.setGrouping([]); }
                });
                return buttons;
            };
            PresentadasGrid.prototype.onViewSubmit = function () {
                // only continue if base class returns true (didn't cancel request)
                if (!_super.prototype.onViewSubmit.call(this)) {
                    return false;
                }
                // view object is the data source for grid (SlickRemoteView)
                // this is an EntityGrid so its Params object is a ListRequest
                var request = this.view.params;
                // list request has a Criteria parameter
                // we AND criteria here to existing one because 
                // otherwise we might clear filter set by 
                // an edit filter dialog if any.
                request.Criteria = Serenity.Criteria.and(request.Criteria, [['FechaPresentacion'], '>', '1900-01-01']);
                // TypeScript doesn't support operator overloading
                // so we had to use array syntax above to build criteria.
                // Make sure you write
                // [['Field'], '>', 10] (which means field A is greater than 10)
                // not 
                // ['A', '>', 10] (which means string 'A' is greater than 10
                return true;
            };
            PresentadasGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], PresentadasGrid);
            return PresentadasGrid;
        }(Serenity.EntityGrid));
        CuadroMandos.PresentadasGrid = PresentadasGrid;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var ProyectosDialog = /** @class */ (function (_super) {
            __extends(ProyectosDialog, _super);
            function ProyectosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new CuadroMandos.ProyectosForm(_this.idPrefix);
                return _this;
            }
            ProyectosDialog.prototype.getFormKey = function () { return CuadroMandos.ProyectosForm.formKey; };
            ProyectosDialog.prototype.getIdProperty = function () { return CuadroMandos.ProyectosRow.idProperty; };
            ProyectosDialog.prototype.getLocalTextPrefix = function () { return CuadroMandos.ProyectosRow.localTextPrefix; };
            ProyectosDialog.prototype.getNameProperty = function () { return CuadroMandos.ProyectosRow.nameProperty; };
            ProyectosDialog.prototype.getService = function () { return CuadroMandos.ProyectosService.baseUrl; };
            ProyectosDialog.prototype.getDeletePermission = function () { return CuadroMandos.ProyectosRow.deletePermission; };
            ProyectosDialog.prototype.getInsertPermission = function () { return CuadroMandos.ProyectosRow.insertPermission; };
            ProyectosDialog.prototype.getUpdatePermission = function () { return CuadroMandos.ProyectosRow.updatePermission; };
            ProyectosDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], ProyectosDialog);
            return ProyectosDialog;
        }(Serenity.EntityDialog));
        CuadroMandos.ProyectosDialog = ProyectosDialog;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var ProyectosGrid = /** @class */ (function (_super) {
            __extends(ProyectosGrid, _super);
            function ProyectosGrid(container) {
                return _super.call(this, container) || this;
            }
            ProyectosGrid.prototype.getColumnsKey = function () { return 'CuadroMandos.Proyectos'; };
            ProyectosGrid.prototype.getDialogType = function () { return CuadroMandos.ProyectosDialog; };
            ProyectosGrid.prototype.getIdProperty = function () { return CuadroMandos.ProyectosRow.idProperty; };
            ProyectosGrid.prototype.getInsertPermission = function () { return CuadroMandos.ProyectosRow.insertPermission; };
            ProyectosGrid.prototype.getLocalTextPrefix = function () { return CuadroMandos.ProyectosRow.localTextPrefix; };
            ProyectosGrid.prototype.getService = function () { return CuadroMandos.ProyectosService.baseUrl; };
            // Añadidos
            // Primero campo de ordenación por defecto
            ProyectosGrid.prototype.getDefaultSortBy = function () {
                return ["ProyectoId" /* ProyectoId */];
            };
            // Agrupar y sumar 
            ProyectosGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                this.view.setSummaryOptions({
                    aggregators: [
                        new Slick.Aggregators.Sum("Empleos" /* Empleos */),
                        new Slick.Aggregators.Sum("EmpleoReal" /* EmpleoReal */),
                        new Slick.Aggregators.Sum("Inversion" /* Inversion */),
                        new Slick.Aggregators.Sum("InversionReal" /* InversionReal */)
                    ]
                });
                return grid;
            };
            ProyectosGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                opt.showFooterRow = true;
                return opt;
            };
            ProyectosGrid.prototype.usePager = function () {
                return false;
            };
            // Botones Excel y Pdf
            ProyectosGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'CuadroMandos/Proyectos/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.push({
                    title: 'Técnico',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Técnico: ' + x.value + ' (' + x.count + ' Proyectos)'; },
                            getter: "Tecnico" /* Tecnico */
                        }]); }
                });
                buttons.push({
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: function () { return _this.view.setGrouping([]); }
                });
                return buttons;
                // Fin añadidos
            };
            ProyectosGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], ProyectosGrid);
            return ProyectosGrid;
        }(Serenity.EntityGrid));
        CuadroMandos.ProyectosGrid = ProyectosGrid;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SectoresDialog = /** @class */ (function (_super) {
            __extends(SectoresDialog, _super);
            function SectoresDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new CuadroMandos.SectoresForm(_this.idPrefix);
                return _this;
            }
            SectoresDialog.prototype.getFormKey = function () { return CuadroMandos.SectoresForm.formKey; };
            SectoresDialog.prototype.getIdProperty = function () { return CuadroMandos.SectoresRow.idProperty; };
            SectoresDialog.prototype.getLocalTextPrefix = function () { return CuadroMandos.SectoresRow.localTextPrefix; };
            SectoresDialog.prototype.getNameProperty = function () { return CuadroMandos.SectoresRow.nameProperty; };
            SectoresDialog.prototype.getService = function () { return CuadroMandos.SectoresService.baseUrl; };
            SectoresDialog.prototype.getDeletePermission = function () { return CuadroMandos.SectoresRow.deletePermission; };
            SectoresDialog.prototype.getInsertPermission = function () { return CuadroMandos.SectoresRow.insertPermission; };
            SectoresDialog.prototype.getUpdatePermission = function () { return CuadroMandos.SectoresRow.updatePermission; };
            SectoresDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], SectoresDialog);
            return SectoresDialog;
        }(Serenity.EntityDialog));
        CuadroMandos.SectoresDialog = SectoresDialog;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SectoresGrid = /** @class */ (function (_super) {
            __extends(SectoresGrid, _super);
            function SectoresGrid(container) {
                return _super.call(this, container) || this;
            }
            SectoresGrid.prototype.getColumnsKey = function () { return 'CuadroMandos.Sectores'; };
            SectoresGrid.prototype.getDialogType = function () { return CuadroMandos.SectoresDialog; };
            SectoresGrid.prototype.getIdProperty = function () { return CuadroMandos.SectoresRow.idProperty; };
            SectoresGrid.prototype.getInsertPermission = function () { return CuadroMandos.SectoresRow.insertPermission; };
            SectoresGrid.prototype.getLocalTextPrefix = function () { return CuadroMandos.SectoresRow.localTextPrefix; };
            SectoresGrid.prototype.getService = function () { return CuadroMandos.SectoresService.baseUrl; };
            // Añadidos
            // Primero campo de ordenación por defecto
            SectoresGrid.prototype.getDefaultSortBy = function () {
                return ["SectorId" /* SectorId */];
            };
            // Botones Excel y Pdf
            SectoresGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'CuadroMandos/Sectores/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            SectoresGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], SectoresGrid);
            return SectoresGrid;
        }(Serenity.EntityGrid));
        CuadroMandos.SectoresGrid = SectoresGrid;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SubsectoresDialog = /** @class */ (function (_super) {
            __extends(SubsectoresDialog, _super);
            function SubsectoresDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new CuadroMandos.SubsectoresForm(_this.idPrefix);
                return _this;
            }
            SubsectoresDialog.prototype.getFormKey = function () { return CuadroMandos.SubsectoresForm.formKey; };
            SubsectoresDialog.prototype.getIdProperty = function () { return CuadroMandos.SubsectoresRow.idProperty; };
            SubsectoresDialog.prototype.getLocalTextPrefix = function () { return CuadroMandos.SubsectoresRow.localTextPrefix; };
            SubsectoresDialog.prototype.getNameProperty = function () { return CuadroMandos.SubsectoresRow.nameProperty; };
            SubsectoresDialog.prototype.getService = function () { return CuadroMandos.SubsectoresService.baseUrl; };
            SubsectoresDialog.prototype.getDeletePermission = function () { return CuadroMandos.SubsectoresRow.deletePermission; };
            SubsectoresDialog.prototype.getInsertPermission = function () { return CuadroMandos.SubsectoresRow.insertPermission; };
            SubsectoresDialog.prototype.getUpdatePermission = function () { return CuadroMandos.SubsectoresRow.updatePermission; };
            SubsectoresDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], SubsectoresDialog);
            return SubsectoresDialog;
        }(Serenity.EntityDialog));
        CuadroMandos.SubsectoresDialog = SubsectoresDialog;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var SubsectoresGrid = /** @class */ (function (_super) {
            __extends(SubsectoresGrid, _super);
            function SubsectoresGrid(container) {
                return _super.call(this, container) || this;
            }
            SubsectoresGrid.prototype.getColumnsKey = function () { return 'CuadroMandos.Subsectores'; };
            SubsectoresGrid.prototype.getDialogType = function () { return CuadroMandos.SubsectoresDialog; };
            SubsectoresGrid.prototype.getIdProperty = function () { return CuadroMandos.SubsectoresRow.idProperty; };
            SubsectoresGrid.prototype.getInsertPermission = function () { return CuadroMandos.SubsectoresRow.insertPermission; };
            SubsectoresGrid.prototype.getLocalTextPrefix = function () { return CuadroMandos.SubsectoresRow.localTextPrefix; };
            SubsectoresGrid.prototype.getService = function () { return CuadroMandos.SubsectoresService.baseUrl; };
            // Añadidos
            // Primero campo de ordenación por defecto
            SubsectoresGrid.prototype.getDefaultSortBy = function () {
                return ["SubsectorId" /* SubsectorId */];
            };
            // Botones Excel y Pdf
            SubsectoresGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'CuadroMandos/Subsectores/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            SubsectoresGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido 
            ], SubsectoresGrid);
            return SubsectoresGrid;
        }(Serenity.EntityGrid));
        CuadroMandos.SubsectoresGrid = SubsectoresGrid;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var TecnicosDialog = /** @class */ (function (_super) {
            __extends(TecnicosDialog, _super);
            function TecnicosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new CuadroMandos.TecnicosForm(_this.idPrefix);
                return _this;
            }
            TecnicosDialog.prototype.getFormKey = function () { return CuadroMandos.TecnicosForm.formKey; };
            TecnicosDialog.prototype.getIdProperty = function () { return CuadroMandos.TecnicosRow.idProperty; };
            TecnicosDialog.prototype.getLocalTextPrefix = function () { return CuadroMandos.TecnicosRow.localTextPrefix; };
            TecnicosDialog.prototype.getNameProperty = function () { return CuadroMandos.TecnicosRow.nameProperty; };
            TecnicosDialog.prototype.getService = function () { return CuadroMandos.TecnicosService.baseUrl; };
            TecnicosDialog.prototype.getDeletePermission = function () { return CuadroMandos.TecnicosRow.deletePermission; };
            TecnicosDialog.prototype.getInsertPermission = function () { return CuadroMandos.TecnicosRow.insertPermission; };
            TecnicosDialog.prototype.getUpdatePermission = function () { return CuadroMandos.TecnicosRow.updatePermission; };
            TecnicosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TecnicosDialog);
            return TecnicosDialog;
        }(Serenity.EntityDialog));
        CuadroMandos.TecnicosDialog = TecnicosDialog;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var CuadroMandos;
    (function (CuadroMandos) {
        var TecnicosGrid = /** @class */ (function (_super) {
            __extends(TecnicosGrid, _super);
            function TecnicosGrid(container) {
                return _super.call(this, container) || this;
            }
            TecnicosGrid.prototype.getColumnsKey = function () { return 'CuadroMandos.Tecnicos'; };
            TecnicosGrid.prototype.getDialogType = function () { return CuadroMandos.TecnicosDialog; };
            TecnicosGrid.prototype.getIdProperty = function () { return CuadroMandos.TecnicosRow.idProperty; };
            TecnicosGrid.prototype.getInsertPermission = function () { return CuadroMandos.TecnicosRow.insertPermission; };
            TecnicosGrid.prototype.getLocalTextPrefix = function () { return CuadroMandos.TecnicosRow.localTextPrefix; };
            TecnicosGrid.prototype.getService = function () { return CuadroMandos.TecnicosService.baseUrl; };
            TecnicosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TecnicosGrid);
            return TecnicosGrid;
        }(Serenity.EntityGrid));
        CuadroMandos.TecnicosGrid = TecnicosGrid;
    })(CuadroMandos = ProyectosZec.CuadroMandos || (ProyectosZec.CuadroMandos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var CambiosDialog = /** @class */ (function (_super) {
            __extends(CambiosDialog, _super);
            function CambiosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new ENS.CambiosForm(_this.idPrefix);
                return _this;
            }
            CambiosDialog.prototype.getFormKey = function () { return ENS.CambiosForm.formKey; };
            CambiosDialog.prototype.getIdProperty = function () { return ENS.CambiosRow.idProperty; };
            CambiosDialog.prototype.getLocalTextPrefix = function () { return ENS.CambiosRow.localTextPrefix; };
            CambiosDialog.prototype.getNameProperty = function () { return ENS.CambiosRow.nameProperty; };
            CambiosDialog.prototype.getService = function () { return ENS.CambiosService.baseUrl; };
            CambiosDialog.prototype.getDeletePermission = function () { return ENS.CambiosRow.deletePermission; };
            CambiosDialog.prototype.getInsertPermission = function () { return ENS.CambiosRow.insertPermission; };
            CambiosDialog.prototype.getUpdatePermission = function () { return ENS.CambiosRow.updatePermission; };
            CambiosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], CambiosDialog);
            return CambiosDialog;
        }(Serenity.EntityDialog));
        ENS.CambiosDialog = CambiosDialog;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var CambiosGrid = /** @class */ (function (_super) {
            __extends(CambiosGrid, _super);
            function CambiosGrid(container) {
                return _super.call(this, container) || this;
            }
            CambiosGrid.prototype.getColumnsKey = function () { return 'ENS.Cambios'; };
            CambiosGrid.prototype.getDialogType = function () { return ENS.CambiosDialog; };
            CambiosGrid.prototype.getIdProperty = function () { return ENS.CambiosRow.idProperty; };
            CambiosGrid.prototype.getInsertPermission = function () { return ENS.CambiosRow.insertPermission; };
            CambiosGrid.prototype.getLocalTextPrefix = function () { return ENS.CambiosRow.localTextPrefix; };
            CambiosGrid.prototype.getService = function () { return ENS.CambiosService.baseUrl; };
            // Añadidos
            // Primero campo de ordenación por defecto
            // No olvidarse Cambiar el Row y el campo
            CambiosGrid.prototype.getDefaultSortBy = function () {
                return [ENS.CambiosRow.Fields.Fecha];
            };
            // Botones Excel y Pdf
            CambiosGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'ENS/Cambios/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            CambiosGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], CambiosGrid);
            return CambiosGrid;
        }(Serenity.EntityGrid));
        ENS.CambiosGrid = CambiosGrid;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var IncidenciasDialog = /** @class */ (function (_super) {
            __extends(IncidenciasDialog, _super);
            function IncidenciasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new ENS.IncidenciasForm(_this.idPrefix);
                return _this;
            }
            IncidenciasDialog.prototype.getFormKey = function () { return ENS.IncidenciasForm.formKey; };
            IncidenciasDialog.prototype.getIdProperty = function () { return ENS.IncidenciasRow.idProperty; };
            IncidenciasDialog.prototype.getLocalTextPrefix = function () { return ENS.IncidenciasRow.localTextPrefix; };
            IncidenciasDialog.prototype.getNameProperty = function () { return ENS.IncidenciasRow.nameProperty; };
            IncidenciasDialog.prototype.getService = function () { return ENS.IncidenciasService.baseUrl; };
            IncidenciasDialog.prototype.getDeletePermission = function () { return ENS.IncidenciasRow.deletePermission; };
            IncidenciasDialog.prototype.getInsertPermission = function () { return ENS.IncidenciasRow.insertPermission; };
            IncidenciasDialog.prototype.getUpdatePermission = function () { return ENS.IncidenciasRow.updatePermission; };
            IncidenciasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], IncidenciasDialog);
            return IncidenciasDialog;
        }(Serenity.EntityDialog));
        ENS.IncidenciasDialog = IncidenciasDialog;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var IncidenciasGrid = /** @class */ (function (_super) {
            __extends(IncidenciasGrid, _super);
            function IncidenciasGrid(container) {
                return _super.call(this, container) || this;
            }
            IncidenciasGrid.prototype.getColumnsKey = function () { return 'ENS.Incidencias'; };
            IncidenciasGrid.prototype.getDialogType = function () { return ENS.IncidenciasDialog; };
            IncidenciasGrid.prototype.getIdProperty = function () { return ENS.IncidenciasRow.idProperty; };
            IncidenciasGrid.prototype.getInsertPermission = function () { return ENS.IncidenciasRow.insertPermission; };
            IncidenciasGrid.prototype.getLocalTextPrefix = function () { return ENS.IncidenciasRow.localTextPrefix; };
            IncidenciasGrid.prototype.getService = function () { return ENS.IncidenciasService.baseUrl; };
            // Botones Excel y Pdf
            IncidenciasGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'ENS/Incidencias/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            /**
    * This method is called for all rows
    * @param item Data item for current row
    * @param index Index of the row in grid
    */
            IncidenciasGrid.prototype.getItemCssClass = function (item, index) {
                var klass = "";
                if (item.Cierre == null)
                    klass += " rechazada";
                return Q.trimToNull(klass);
            };
            IncidenciasGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], IncidenciasGrid);
            return IncidenciasGrid;
        }(Serenity.EntityGrid));
        ENS.IncidenciasGrid = IncidenciasGrid;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var PasswordsDialog = /** @class */ (function (_super) {
            __extends(PasswordsDialog, _super);
            function PasswordsDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new ENS.PasswordsForm(_this.idPrefix);
                return _this;
            }
            PasswordsDialog.prototype.getFormKey = function () { return ENS.PasswordsForm.formKey; };
            PasswordsDialog.prototype.getIdProperty = function () { return ENS.PasswordsRow.idProperty; };
            PasswordsDialog.prototype.getLocalTextPrefix = function () { return ENS.PasswordsRow.localTextPrefix; };
            PasswordsDialog.prototype.getNameProperty = function () { return ENS.PasswordsRow.nameProperty; };
            PasswordsDialog.prototype.getService = function () { return ENS.PasswordsService.baseUrl; };
            PasswordsDialog.prototype.getDeletePermission = function () { return ENS.PasswordsRow.deletePermission; };
            PasswordsDialog.prototype.getInsertPermission = function () { return ENS.PasswordsRow.insertPermission; };
            PasswordsDialog.prototype.getUpdatePermission = function () { return ENS.PasswordsRow.updatePermission; };
            PasswordsDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], PasswordsDialog);
            return PasswordsDialog;
        }(Serenity.EntityDialog));
        ENS.PasswordsDialog = PasswordsDialog;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var PasswordsGrid = /** @class */ (function (_super) {
            __extends(PasswordsGrid, _super);
            function PasswordsGrid(container) {
                return _super.call(this, container) || this;
            }
            PasswordsGrid.prototype.getColumnsKey = function () { return 'ENS.Passwords'; };
            PasswordsGrid.prototype.getDialogType = function () { return ENS.PasswordsDialog; };
            PasswordsGrid.prototype.getIdProperty = function () { return ENS.PasswordsRow.idProperty; };
            PasswordsGrid.prototype.getInsertPermission = function () { return ENS.PasswordsRow.insertPermission; };
            PasswordsGrid.prototype.getLocalTextPrefix = function () { return ENS.PasswordsRow.localTextPrefix; };
            PasswordsGrid.prototype.getService = function () { return ENS.PasswordsService.baseUrl; };
            // Botones Excel y Pdf
            PasswordsGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'ENS/Passwords/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            PasswordsGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], PasswordsGrid);
            return PasswordsGrid;
        }(Serenity.EntityGrid));
        ENS.PasswordsGrid = PasswordsGrid;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var ServiciosDialog = /** @class */ (function (_super) {
            __extends(ServiciosDialog, _super);
            function ServiciosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new ENS.ServiciosForm(_this.idPrefix);
                return _this;
            }
            ServiciosDialog.prototype.getFormKey = function () { return ENS.ServiciosForm.formKey; };
            ServiciosDialog.prototype.getIdProperty = function () { return ENS.ServiciosRow.idProperty; };
            ServiciosDialog.prototype.getLocalTextPrefix = function () { return ENS.ServiciosRow.localTextPrefix; };
            ServiciosDialog.prototype.getNameProperty = function () { return ENS.ServiciosRow.nameProperty; };
            ServiciosDialog.prototype.getService = function () { return ENS.ServiciosService.baseUrl; };
            ServiciosDialog.prototype.getDeletePermission = function () { return ENS.ServiciosRow.deletePermission; };
            ServiciosDialog.prototype.getInsertPermission = function () { return ENS.ServiciosRow.insertPermission; };
            ServiciosDialog.prototype.getUpdatePermission = function () { return ENS.ServiciosRow.updatePermission; };
            ServiciosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], ServiciosDialog);
            return ServiciosDialog;
        }(Serenity.EntityDialog));
        ENS.ServiciosDialog = ServiciosDialog;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var ServiciosGrid = /** @class */ (function (_super) {
            __extends(ServiciosGrid, _super);
            function ServiciosGrid(container) {
                return _super.call(this, container) || this;
            }
            ServiciosGrid.prototype.getColumnsKey = function () { return 'ENS.Servicios'; };
            ServiciosGrid.prototype.getDialogType = function () { return ENS.ServiciosDialog; };
            ServiciosGrid.prototype.getIdProperty = function () { return ENS.ServiciosRow.idProperty; };
            ServiciosGrid.prototype.getInsertPermission = function () { return ENS.ServiciosRow.insertPermission; };
            ServiciosGrid.prototype.getLocalTextPrefix = function () { return ENS.ServiciosRow.localTextPrefix; };
            ServiciosGrid.prototype.getService = function () { return ENS.ServiciosService.baseUrl; };
            ServiciosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], ServiciosGrid);
            return ServiciosGrid;
        }(Serenity.EntityGrid));
        ENS.ServiciosGrid = ServiciosGrid;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var SeveridadesDialog = /** @class */ (function (_super) {
            __extends(SeveridadesDialog, _super);
            function SeveridadesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new ENS.SeveridadesForm(_this.idPrefix);
                return _this;
            }
            SeveridadesDialog.prototype.getFormKey = function () { return ENS.SeveridadesForm.formKey; };
            SeveridadesDialog.prototype.getIdProperty = function () { return ENS.SeveridadesRow.idProperty; };
            SeveridadesDialog.prototype.getLocalTextPrefix = function () { return ENS.SeveridadesRow.localTextPrefix; };
            SeveridadesDialog.prototype.getNameProperty = function () { return ENS.SeveridadesRow.nameProperty; };
            SeveridadesDialog.prototype.getService = function () { return ENS.SeveridadesService.baseUrl; };
            SeveridadesDialog.prototype.getDeletePermission = function () { return ENS.SeveridadesRow.deletePermission; };
            SeveridadesDialog.prototype.getInsertPermission = function () { return ENS.SeveridadesRow.insertPermission; };
            SeveridadesDialog.prototype.getUpdatePermission = function () { return ENS.SeveridadesRow.updatePermission; };
            SeveridadesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], SeveridadesDialog);
            return SeveridadesDialog;
        }(Serenity.EntityDialog));
        ENS.SeveridadesDialog = SeveridadesDialog;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var SeveridadesGrid = /** @class */ (function (_super) {
            __extends(SeveridadesGrid, _super);
            function SeveridadesGrid(container) {
                return _super.call(this, container) || this;
            }
            SeveridadesGrid.prototype.getColumnsKey = function () { return 'ENS.Severidades'; };
            SeveridadesGrid.prototype.getDialogType = function () { return ENS.SeveridadesDialog; };
            SeveridadesGrid.prototype.getIdProperty = function () { return ENS.SeveridadesRow.idProperty; };
            SeveridadesGrid.prototype.getInsertPermission = function () { return ENS.SeveridadesRow.insertPermission; };
            SeveridadesGrid.prototype.getLocalTextPrefix = function () { return ENS.SeveridadesRow.localTextPrefix; };
            SeveridadesGrid.prototype.getService = function () { return ENS.SeveridadesService.baseUrl; };
            SeveridadesGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], SeveridadesGrid);
            return SeveridadesGrid;
        }(Serenity.EntityGrid));
        ENS.SeveridadesGrid = SeveridadesGrid;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var TiposservicioDialog = /** @class */ (function (_super) {
            __extends(TiposservicioDialog, _super);
            function TiposservicioDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new ENS.TiposservicioForm(_this.idPrefix);
                return _this;
            }
            TiposservicioDialog.prototype.getFormKey = function () { return ENS.TiposservicioForm.formKey; };
            TiposservicioDialog.prototype.getIdProperty = function () { return ENS.TiposservicioRow.idProperty; };
            TiposservicioDialog.prototype.getLocalTextPrefix = function () { return ENS.TiposservicioRow.localTextPrefix; };
            TiposservicioDialog.prototype.getNameProperty = function () { return ENS.TiposservicioRow.nameProperty; };
            TiposservicioDialog.prototype.getService = function () { return ENS.TiposservicioService.baseUrl; };
            TiposservicioDialog.prototype.getDeletePermission = function () { return ENS.TiposservicioRow.deletePermission; };
            TiposservicioDialog.prototype.getInsertPermission = function () { return ENS.TiposservicioRow.insertPermission; };
            TiposservicioDialog.prototype.getUpdatePermission = function () { return ENS.TiposservicioRow.updatePermission; };
            TiposservicioDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposservicioDialog);
            return TiposservicioDialog;
        }(Serenity.EntityDialog));
        ENS.TiposservicioDialog = TiposservicioDialog;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var ENS;
    (function (ENS) {
        var TiposservicioGrid = /** @class */ (function (_super) {
            __extends(TiposservicioGrid, _super);
            function TiposservicioGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposservicioGrid.prototype.getColumnsKey = function () { return 'ENS.Tiposservicio'; };
            TiposservicioGrid.prototype.getDialogType = function () { return ENS.TiposservicioDialog; };
            TiposservicioGrid.prototype.getIdProperty = function () { return ENS.TiposservicioRow.idProperty; };
            TiposservicioGrid.prototype.getInsertPermission = function () { return ENS.TiposservicioRow.insertPermission; };
            TiposservicioGrid.prototype.getLocalTextPrefix = function () { return ENS.TiposservicioRow.localTextPrefix; };
            TiposservicioGrid.prototype.getService = function () { return ENS.TiposservicioService.baseUrl; };
            TiposservicioGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposservicioGrid);
            return TiposservicioGrid;
        }(Serenity.EntityGrid));
        ENS.TiposservicioGrid = TiposservicioGrid;
    })(ENS = ProyectosZec.ENS || (ProyectosZec.ENS = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var InmovilizadosDialog = /** @class */ (function (_super) {
            __extends(InmovilizadosDialog, _super);
            function InmovilizadosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Inmovilizado.InmovilizadosForm(_this.idPrefix);
                return _this;
            }
            InmovilizadosDialog.prototype.getFormKey = function () { return Inmovilizado.InmovilizadosForm.formKey; };
            InmovilizadosDialog.prototype.getIdProperty = function () { return Inmovilizado.InmovilizadosRow.idProperty; };
            InmovilizadosDialog.prototype.getLocalTextPrefix = function () { return Inmovilizado.InmovilizadosRow.localTextPrefix; };
            InmovilizadosDialog.prototype.getNameProperty = function () { return Inmovilizado.InmovilizadosRow.nameProperty; };
            InmovilizadosDialog.prototype.getService = function () { return Inmovilizado.InmovilizadosService.baseUrl; };
            InmovilizadosDialog.prototype.getDeletePermission = function () { return Inmovilizado.InmovilizadosRow.deletePermission; };
            InmovilizadosDialog.prototype.getInsertPermission = function () { return Inmovilizado.InmovilizadosRow.insertPermission; };
            InmovilizadosDialog.prototype.getUpdatePermission = function () { return Inmovilizado.InmovilizadosRow.updatePermission; };
            InmovilizadosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], InmovilizadosDialog);
            return InmovilizadosDialog;
        }(Serenity.EntityDialog));
        Inmovilizado.InmovilizadosDialog = InmovilizadosDialog;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var InmovilizadosGrid = /** @class */ (function (_super) {
            __extends(InmovilizadosGrid, _super);
            function InmovilizadosGrid(container) {
                return _super.call(this, container) || this;
            }
            InmovilizadosGrid.prototype.getColumnsKey = function () { return 'Inmovilizado.Inmovilizados'; };
            InmovilizadosGrid.prototype.getDialogType = function () { return Inmovilizado.InmovilizadosDialog; };
            InmovilizadosGrid.prototype.getIdProperty = function () { return Inmovilizado.InmovilizadosRow.idProperty; };
            InmovilizadosGrid.prototype.getInsertPermission = function () { return Inmovilizado.InmovilizadosRow.insertPermission; };
            InmovilizadosGrid.prototype.getLocalTextPrefix = function () { return Inmovilizado.InmovilizadosRow.localTextPrefix; };
            InmovilizadosGrid.prototype.getService = function () { return Inmovilizado.InmovilizadosService.baseUrl; };
            // Agrupar y sumar 
            InmovilizadosGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                this.view.setSummaryOptions({
                    aggregators: [
                        new Slick.Aggregators.Sum("Valor" /* Valor */),
                        new Slick.Aggregators.Sum("ValorResidual" /* ValorResidual */)
                    ]
                });
                // Declaramos que el Grid puedes seleccionar fila
                grid.setSelectionModel(new Slick.RowSelectionModel());
                return grid;
            };
            InmovilizadosGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                // Mostrar pie de página
                opt.showFooterRow = true;
                // Fin pie de página
                // Fila Seleccionable
                opt.enableTextSelectionOnCells = true;
                opt.selectedCellCssClass = "slick-row-selected";
                opt.enableCellNavigation = true;
                // Fin Fila Seleccionable
                return opt;
            };
            InmovilizadosGrid.prototype.usePager = function () {
                return false;
            };
            InmovilizadosGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                // Botones Excel y Pdf
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Inmovilizado/Inmovilizados/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.push({
                    title: 'SubTipo',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Subtipo: ' + x.value + ' (' + x.count + ' Inmovilizados)'; },
                            getter: "SubTipo" /* SubTipo */
                        }]); }
                });
                buttons.push({
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: function () { return _this.view.setGrouping([]); }
                });
                return buttons;
                // Fin añadidos
            };
            /**
            * We override getColumns() to be able to add a custom CSS class to UnitPrice
            * We could also add this class in ProductColumns.cs but didn't want to modify
            * it solely for this sample.
            */
            InmovilizadosGrid.prototype.getColumns = function () {
                var columns = _super.prototype.getColumns.call(this);
                // adding a specific css class to UnitPrice column, 
                // to be able to format cell with a different background
                Q.first(columns, function (x) { return x.field == "ValorResidual" /* ValorResidual */; }).cssClass += " col-unit-price";
                return columns;
            };
            /**
            * This method is called for all rows
            * @param item Data item for current row
            * @param index Index of the row in grid
            */
            InmovilizadosGrid.prototype.getItemCssClass = function (item, index) {
                var klass = "";
                if (item.FechaBaja != null)
                    klass += " discontinued";
                else if (item.ValorResidual <= 0)
                    klass += " out-of-stock";
                return Q.trimToNull(klass);
            };
            InmovilizadosGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], InmovilizadosGrid);
            return InmovilizadosGrid;
        }(Serenity.EntityGrid));
        Inmovilizado.InmovilizadosGrid = InmovilizadosGrid;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var ProveedoresDialog = /** @class */ (function (_super) {
            __extends(ProveedoresDialog, _super);
            function ProveedoresDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Inmovilizado.ProveedoresForm(_this.idPrefix);
                return _this;
            }
            ProveedoresDialog.prototype.getFormKey = function () { return Inmovilizado.ProveedoresForm.formKey; };
            ProveedoresDialog.prototype.getIdProperty = function () { return Inmovilizado.ProveedoresRow.idProperty; };
            ProveedoresDialog.prototype.getLocalTextPrefix = function () { return Inmovilizado.ProveedoresRow.localTextPrefix; };
            ProveedoresDialog.prototype.getNameProperty = function () { return Inmovilizado.ProveedoresRow.nameProperty; };
            ProveedoresDialog.prototype.getService = function () { return Inmovilizado.ProveedoresService.baseUrl; };
            ProveedoresDialog.prototype.getDeletePermission = function () { return Inmovilizado.ProveedoresRow.deletePermission; };
            ProveedoresDialog.prototype.getInsertPermission = function () { return Inmovilizado.ProveedoresRow.insertPermission; };
            ProveedoresDialog.prototype.getUpdatePermission = function () { return Inmovilizado.ProveedoresRow.updatePermission; };
            ProveedoresDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], ProveedoresDialog);
            return ProveedoresDialog;
        }(Serenity.EntityDialog));
        Inmovilizado.ProveedoresDialog = ProveedoresDialog;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var ProveedoresGrid = /** @class */ (function (_super) {
            __extends(ProveedoresGrid, _super);
            function ProveedoresGrid(container) {
                return _super.call(this, container) || this;
            }
            ProveedoresGrid.prototype.getColumnsKey = function () { return 'Inmovilizado.Proveedores'; };
            ProveedoresGrid.prototype.getDialogType = function () { return Inmovilizado.ProveedoresDialog; };
            ProveedoresGrid.prototype.getIdProperty = function () { return Inmovilizado.ProveedoresRow.idProperty; };
            ProveedoresGrid.prototype.getInsertPermission = function () { return Inmovilizado.ProveedoresRow.insertPermission; };
            ProveedoresGrid.prototype.getLocalTextPrefix = function () { return Inmovilizado.ProveedoresRow.localTextPrefix; };
            ProveedoresGrid.prototype.getService = function () { return Inmovilizado.ProveedoresService.baseUrl; };
            ProveedoresGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                // Botones Excel y Pdf
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Inmovilizado/Proveedores/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            ProveedoresGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], ProveedoresGrid);
            return ProveedoresGrid;
        }(Serenity.EntityGrid));
        Inmovilizado.ProveedoresGrid = ProveedoresGrid;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var SubtiposinmovilizadoDialog = /** @class */ (function (_super) {
            __extends(SubtiposinmovilizadoDialog, _super);
            function SubtiposinmovilizadoDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Inmovilizado.SubtiposinmovilizadoForm(_this.idPrefix);
                return _this;
            }
            SubtiposinmovilizadoDialog.prototype.getFormKey = function () { return Inmovilizado.SubtiposinmovilizadoForm.formKey; };
            SubtiposinmovilizadoDialog.prototype.getIdProperty = function () { return Inmovilizado.SubtiposinmovilizadoRow.idProperty; };
            SubtiposinmovilizadoDialog.prototype.getLocalTextPrefix = function () { return Inmovilizado.SubtiposinmovilizadoRow.localTextPrefix; };
            SubtiposinmovilizadoDialog.prototype.getNameProperty = function () { return Inmovilizado.SubtiposinmovilizadoRow.nameProperty; };
            SubtiposinmovilizadoDialog.prototype.getService = function () { return Inmovilizado.SubtiposinmovilizadoService.baseUrl; };
            SubtiposinmovilizadoDialog.prototype.getDeletePermission = function () { return Inmovilizado.SubtiposinmovilizadoRow.deletePermission; };
            SubtiposinmovilizadoDialog.prototype.getInsertPermission = function () { return Inmovilizado.SubtiposinmovilizadoRow.insertPermission; };
            SubtiposinmovilizadoDialog.prototype.getUpdatePermission = function () { return Inmovilizado.SubtiposinmovilizadoRow.updatePermission; };
            SubtiposinmovilizadoDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], SubtiposinmovilizadoDialog);
            return SubtiposinmovilizadoDialog;
        }(Serenity.EntityDialog));
        Inmovilizado.SubtiposinmovilizadoDialog = SubtiposinmovilizadoDialog;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var SubtiposinmovilizadoGrid = /** @class */ (function (_super) {
            __extends(SubtiposinmovilizadoGrid, _super);
            function SubtiposinmovilizadoGrid(container) {
                return _super.call(this, container) || this;
            }
            SubtiposinmovilizadoGrid.prototype.getColumnsKey = function () { return 'Inmovilizado.Subtiposinmovilizado'; };
            SubtiposinmovilizadoGrid.prototype.getDialogType = function () { return Inmovilizado.SubtiposinmovilizadoDialog; };
            SubtiposinmovilizadoGrid.prototype.getIdProperty = function () { return Inmovilizado.SubtiposinmovilizadoRow.idProperty; };
            SubtiposinmovilizadoGrid.prototype.getInsertPermission = function () { return Inmovilizado.SubtiposinmovilizadoRow.insertPermission; };
            SubtiposinmovilizadoGrid.prototype.getLocalTextPrefix = function () { return Inmovilizado.SubtiposinmovilizadoRow.localTextPrefix; };
            SubtiposinmovilizadoGrid.prototype.getService = function () { return Inmovilizado.SubtiposinmovilizadoService.baseUrl; };
            SubtiposinmovilizadoGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Inmovilizado/Subtiposinmovilizados/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            SubtiposinmovilizadoGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], SubtiposinmovilizadoGrid);
            return SubtiposinmovilizadoGrid;
        }(Serenity.EntityGrid));
        Inmovilizado.SubtiposinmovilizadoGrid = SubtiposinmovilizadoGrid;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var TiposinmovilizadoDialog = /** @class */ (function (_super) {
            __extends(TiposinmovilizadoDialog, _super);
            function TiposinmovilizadoDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Inmovilizado.TiposinmovilizadoForm(_this.idPrefix);
                return _this;
            }
            TiposinmovilizadoDialog.prototype.getFormKey = function () { return Inmovilizado.TiposinmovilizadoForm.formKey; };
            TiposinmovilizadoDialog.prototype.getIdProperty = function () { return Inmovilizado.TiposinmovilizadoRow.idProperty; };
            TiposinmovilizadoDialog.prototype.getLocalTextPrefix = function () { return Inmovilizado.TiposinmovilizadoRow.localTextPrefix; };
            TiposinmovilizadoDialog.prototype.getNameProperty = function () { return Inmovilizado.TiposinmovilizadoRow.nameProperty; };
            TiposinmovilizadoDialog.prototype.getService = function () { return Inmovilizado.TiposinmovilizadoService.baseUrl; };
            TiposinmovilizadoDialog.prototype.getDeletePermission = function () { return Inmovilizado.TiposinmovilizadoRow.deletePermission; };
            TiposinmovilizadoDialog.prototype.getInsertPermission = function () { return Inmovilizado.TiposinmovilizadoRow.insertPermission; };
            TiposinmovilizadoDialog.prototype.getUpdatePermission = function () { return Inmovilizado.TiposinmovilizadoRow.updatePermission; };
            TiposinmovilizadoDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposinmovilizadoDialog);
            return TiposinmovilizadoDialog;
        }(Serenity.EntityDialog));
        Inmovilizado.TiposinmovilizadoDialog = TiposinmovilizadoDialog;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Inmovilizado;
    (function (Inmovilizado) {
        var TiposinmovilizadoGrid = /** @class */ (function (_super) {
            __extends(TiposinmovilizadoGrid, _super);
            function TiposinmovilizadoGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposinmovilizadoGrid.prototype.getColumnsKey = function () { return 'Inmovilizado.Tiposinmovilizado'; };
            TiposinmovilizadoGrid.prototype.getDialogType = function () { return Inmovilizado.TiposinmovilizadoDialog; };
            TiposinmovilizadoGrid.prototype.getIdProperty = function () { return Inmovilizado.TiposinmovilizadoRow.idProperty; };
            TiposinmovilizadoGrid.prototype.getInsertPermission = function () { return Inmovilizado.TiposinmovilizadoRow.insertPermission; };
            TiposinmovilizadoGrid.prototype.getLocalTextPrefix = function () { return Inmovilizado.TiposinmovilizadoRow.localTextPrefix; };
            TiposinmovilizadoGrid.prototype.getService = function () { return Inmovilizado.TiposinmovilizadoService.baseUrl; };
            TiposinmovilizadoGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposinmovilizadoGrid);
            return TiposinmovilizadoGrid;
        }(Serenity.EntityGrid));
        Inmovilizado.TiposinmovilizadoGrid = TiposinmovilizadoGrid;
    })(Inmovilizado = ProyectosZec.Inmovilizado || (ProyectosZec.Inmovilizado = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var DepartamentosDialog = /** @class */ (function (_super) {
            __extends(DepartamentosDialog, _super);
            function DepartamentosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Intranet.DepartamentosForm(_this.idPrefix);
                return _this;
            }
            DepartamentosDialog.prototype.getFormKey = function () { return Intranet.DepartamentosForm.formKey; };
            DepartamentosDialog.prototype.getIdProperty = function () { return Intranet.DepartamentosRow.idProperty; };
            DepartamentosDialog.prototype.getLocalTextPrefix = function () { return Intranet.DepartamentosRow.localTextPrefix; };
            DepartamentosDialog.prototype.getNameProperty = function () { return Intranet.DepartamentosRow.nameProperty; };
            DepartamentosDialog.prototype.getService = function () { return Intranet.DepartamentosService.baseUrl; };
            DepartamentosDialog.prototype.getDeletePermission = function () { return Intranet.DepartamentosRow.deletePermission; };
            DepartamentosDialog.prototype.getInsertPermission = function () { return Intranet.DepartamentosRow.insertPermission; };
            DepartamentosDialog.prototype.getUpdatePermission = function () { return Intranet.DepartamentosRow.updatePermission; };
            DepartamentosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], DepartamentosDialog);
            return DepartamentosDialog;
        }(Serenity.EntityDialog));
        Intranet.DepartamentosDialog = DepartamentosDialog;
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var DepartamentosGrid = /** @class */ (function (_super) {
            __extends(DepartamentosGrid, _super);
            function DepartamentosGrid(container) {
                return _super.call(this, container) || this;
            }
            DepartamentosGrid.prototype.getColumnsKey = function () { return 'Intranet.Departamentos'; };
            DepartamentosGrid.prototype.getDialogType = function () { return Intranet.DepartamentosDialog; };
            DepartamentosGrid.prototype.getIdProperty = function () { return Intranet.DepartamentosRow.idProperty; };
            DepartamentosGrid.prototype.getInsertPermission = function () { return Intranet.DepartamentosRow.insertPermission; };
            DepartamentosGrid.prototype.getLocalTextPrefix = function () { return Intranet.DepartamentosRow.localTextPrefix; };
            DepartamentosGrid.prototype.getService = function () { return Intranet.DepartamentosService.baseUrl; };
            DepartamentosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], DepartamentosGrid);
            return DepartamentosGrid;
        }(Serenity.EntityGrid));
        Intranet.DepartamentosGrid = DepartamentosGrid;
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var SedesDialog = /** @class */ (function (_super) {
            __extends(SedesDialog, _super);
            function SedesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Intranet.SedesForm(_this.idPrefix);
                return _this;
            }
            SedesDialog.prototype.getFormKey = function () { return Intranet.SedesForm.formKey; };
            SedesDialog.prototype.getIdProperty = function () { return Intranet.SedesRow.idProperty; };
            SedesDialog.prototype.getLocalTextPrefix = function () { return Intranet.SedesRow.localTextPrefix; };
            SedesDialog.prototype.getNameProperty = function () { return Intranet.SedesRow.nameProperty; };
            SedesDialog.prototype.getService = function () { return Intranet.SedesService.baseUrl; };
            SedesDialog.prototype.getDeletePermission = function () { return Intranet.SedesRow.deletePermission; };
            SedesDialog.prototype.getInsertPermission = function () { return Intranet.SedesRow.insertPermission; };
            SedesDialog.prototype.getUpdatePermission = function () { return Intranet.SedesRow.updatePermission; };
            SedesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], SedesDialog);
            return SedesDialog;
        }(Serenity.EntityDialog));
        Intranet.SedesDialog = SedesDialog;
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var SedesGrid = /** @class */ (function (_super) {
            __extends(SedesGrid, _super);
            function SedesGrid(container) {
                return _super.call(this, container) || this;
            }
            SedesGrid.prototype.getColumnsKey = function () { return 'Intranet.Sedes'; };
            SedesGrid.prototype.getDialogType = function () { return Intranet.SedesDialog; };
            SedesGrid.prototype.getIdProperty = function () { return Intranet.SedesRow.idProperty; };
            SedesGrid.prototype.getInsertPermission = function () { return Intranet.SedesRow.insertPermission; };
            SedesGrid.prototype.getLocalTextPrefix = function () { return Intranet.SedesRow.localTextPrefix; };
            SedesGrid.prototype.getService = function () { return Intranet.SedesService.baseUrl; };
            SedesGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], SedesGrid);
            return SedesGrid;
        }(Serenity.EntityGrid));
        Intranet.SedesGrid = SedesGrid;
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var TelefonosDialog = /** @class */ (function (_super) {
            __extends(TelefonosDialog, _super);
            function TelefonosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Intranet.TelefonosForm(_this.idPrefix);
                return _this;
            }
            TelefonosDialog.prototype.getFormKey = function () { return Intranet.TelefonosForm.formKey; };
            TelefonosDialog.prototype.getIdProperty = function () { return Intranet.TelefonosRow.idProperty; };
            TelefonosDialog.prototype.getLocalTextPrefix = function () { return Intranet.TelefonosRow.localTextPrefix; };
            TelefonosDialog.prototype.getNameProperty = function () { return Intranet.TelefonosRow.nameProperty; };
            TelefonosDialog.prototype.getService = function () { return Intranet.TelefonosService.baseUrl; };
            TelefonosDialog.prototype.getDeletePermission = function () { return Intranet.TelefonosRow.deletePermission; };
            TelefonosDialog.prototype.getInsertPermission = function () { return Intranet.TelefonosRow.insertPermission; };
            TelefonosDialog.prototype.getUpdatePermission = function () { return Intranet.TelefonosRow.updatePermission; };
            TelefonosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TelefonosDialog);
            return TelefonosDialog;
        }(Serenity.EntityDialog));
        Intranet.TelefonosDialog = TelefonosDialog;
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Intranet;
    (function (Intranet) {
        var TelefonosGrid = /** @class */ (function (_super) {
            __extends(TelefonosGrid, _super);
            function TelefonosGrid(container) {
                return _super.call(this, container) || this;
            }
            TelefonosGrid.prototype.getColumnsKey = function () { return 'Intranet.Telefonos'; };
            TelefonosGrid.prototype.getDialogType = function () { return Intranet.TelefonosDialog; };
            TelefonosGrid.prototype.getIdProperty = function () { return Intranet.TelefonosRow.idProperty; };
            TelefonosGrid.prototype.getInsertPermission = function () { return Intranet.TelefonosRow.insertPermission; };
            TelefonosGrid.prototype.getLocalTextPrefix = function () { return Intranet.TelefonosRow.localTextPrefix; };
            TelefonosGrid.prototype.getService = function () { return Intranet.TelefonosService.baseUrl; };
            // Añadidos
            // Primero campo de ordenación por defecto
            // No olvidarse Cambiar el Row y el Id
            TelefonosGrid.prototype.getDefaultSortBy = function () {
                return ["TelefonoId" /* TelefonoId */];
            };
            // Botones Excel y Pdf
            TelefonosGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Intranet/Telefonos/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            TelefonosGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], TelefonosGrid);
            return TelefonosGrid;
        }(Serenity.EntityGrid));
        Intranet.TelefonosGrid = TelefonosGrid;
    })(Intranet = ProyectosZec.Intranet || (ProyectosZec.Intranet = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var AusenciasProgramadasDialog = /** @class */ (function (_super) {
            __extends(AusenciasProgramadasDialog, _super);
            function AusenciasProgramadasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.AusenciasProgramadasForm(_this.idPrefix);
                return _this;
            }
            AusenciasProgramadasDialog.prototype.getFormKey = function () { return Kairos.AusenciasProgramadasForm.formKey; };
            AusenciasProgramadasDialog.prototype.getIdProperty = function () { return Kairos.AusenciasProgramadasRow.idProperty; };
            AusenciasProgramadasDialog.prototype.getLocalTextPrefix = function () { return Kairos.AusenciasProgramadasRow.localTextPrefix; };
            AusenciasProgramadasDialog.prototype.getService = function () { return Kairos.AusenciasProgramadasService.baseUrl; };
            AusenciasProgramadasDialog.prototype.getDeletePermission = function () { return Kairos.AusenciasProgramadasRow.deletePermission; };
            AusenciasProgramadasDialog.prototype.getInsertPermission = function () { return Kairos.AusenciasProgramadasRow.insertPermission; };
            AusenciasProgramadasDialog.prototype.getUpdatePermission = function () { return Kairos.AusenciasProgramadasRow.updatePermission; };
            AusenciasProgramadasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], AusenciasProgramadasDialog);
            return AusenciasProgramadasDialog;
        }(Serenity.EntityDialog));
        Kairos.AusenciasProgramadasDialog = AusenciasProgramadasDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var AusenciasProgramadasGrid = /** @class */ (function (_super) {
            __extends(AusenciasProgramadasGrid, _super);
            function AusenciasProgramadasGrid(container) {
                return _super.call(this, container) || this;
            }
            AusenciasProgramadasGrid.prototype.getColumnsKey = function () { return 'Kairos.AusenciasProgramadas'; };
            AusenciasProgramadasGrid.prototype.getDialogType = function () { return Kairos.AusenciasProgramadasDialog; };
            AusenciasProgramadasGrid.prototype.getIdProperty = function () { return Kairos.AusenciasProgramadasRow.idProperty; };
            AusenciasProgramadasGrid.prototype.getInsertPermission = function () { return Kairos.AusenciasProgramadasRow.insertPermission; };
            AusenciasProgramadasGrid.prototype.getLocalTextPrefix = function () { return Kairos.AusenciasProgramadasRow.localTextPrefix; };
            AusenciasProgramadasGrid.prototype.getService = function () { return Kairos.AusenciasProgramadasService.baseUrl; };
            // Agrupar y sumar 
            AusenciasProgramadasGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                this.view.setSummaryOptions({
                    aggregators: [
                        new Slick.Aggregators.Sum("TotalDias" /* TotalDias */),
                        new Slick.Aggregators.Sum("TotalHoras" /* TotalHoras */)
                    ]
                });
                // Declaramos que el Grid puedes seleccionar fila
                grid.setSelectionModel(new Slick.RowSelectionModel());
                return grid;
            };
            AusenciasProgramadasGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                // Mostrar pie de página
                opt.showFooterRow = true;
                // Fin pie de página
                // Fila Seleccionable
                opt.enableTextSelectionOnCells = true;
                opt.selectedCellCssClass = "slick-row-selected";
                opt.enableCellNavigation = true;
                // Fin Fila Seleccionable
                return opt;
            };
            AusenciasProgramadasGrid.prototype.usePager = function () {
                return false;
            };
            // Botones Excel y Pdf
            AusenciasProgramadasGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Kairos/AusenciasProgramadas/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.push({
                    title: 'Empleado',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Empleado: ' + x.value + ' (' + x.count + ' Solicitudes)'; },
                            getter: "Empleado" /* Empleado */
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Empleado y Tipo Solicitud',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Empleado: ' + x.value + ' (' + x.count + ' Solicitudes)'; },
                            getter: "Empleado" /* Empleado */
                        }, {
                            formatter: function (x) { return 'Tipo: ' + x.value + ' (' + x.count + ' Solicitudes)'; },
                            getter: "Descripcion" /* Descripcion */
                        }]); }
                });
                buttons.push({
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: function () { return _this.view.setGrouping([]); }
                });
                // Quitamos boton de añadir para evitar que se añadan nuevas extras
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                return buttons;
                // Fin añadidos
            };
            AusenciasProgramadasGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], AusenciasProgramadasGrid);
            return AusenciasProgramadasGrid;
        }(Serenity.EntityGrid));
        Kairos.AusenciasProgramadasGrid = AusenciasProgramadasGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DepartamentosDialog = /** @class */ (function (_super) {
            __extends(DepartamentosDialog, _super);
            function DepartamentosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.DepartamentosForm(_this.idPrefix);
                return _this;
            }
            DepartamentosDialog.prototype.getFormKey = function () { return Kairos.DepartamentosForm.formKey; };
            DepartamentosDialog.prototype.getIdProperty = function () { return Kairos.DepartamentosRow.idProperty; };
            DepartamentosDialog.prototype.getLocalTextPrefix = function () { return Kairos.DepartamentosRow.localTextPrefix; };
            DepartamentosDialog.prototype.getNameProperty = function () { return Kairos.DepartamentosRow.nameProperty; };
            DepartamentosDialog.prototype.getService = function () { return Kairos.DepartamentosService.baseUrl; };
            DepartamentosDialog.prototype.getDeletePermission = function () { return Kairos.DepartamentosRow.deletePermission; };
            DepartamentosDialog.prototype.getInsertPermission = function () { return Kairos.DepartamentosRow.insertPermission; };
            DepartamentosDialog.prototype.getUpdatePermission = function () { return Kairos.DepartamentosRow.updatePermission; };
            DepartamentosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], DepartamentosDialog);
            return DepartamentosDialog;
        }(Serenity.EntityDialog));
        Kairos.DepartamentosDialog = DepartamentosDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DepartamentosGrid = /** @class */ (function (_super) {
            __extends(DepartamentosGrid, _super);
            function DepartamentosGrid(container) {
                return _super.call(this, container) || this;
            }
            DepartamentosGrid.prototype.getColumnsKey = function () { return 'Kairos.Departamentos'; };
            DepartamentosGrid.prototype.getDialogType = function () { return Kairos.DepartamentosDialog; };
            DepartamentosGrid.prototype.getIdProperty = function () { return Kairos.DepartamentosRow.idProperty; };
            DepartamentosGrid.prototype.getInsertPermission = function () { return Kairos.DepartamentosRow.insertPermission; };
            DepartamentosGrid.prototype.getLocalTextPrefix = function () { return Kairos.DepartamentosRow.localTextPrefix; };
            DepartamentosGrid.prototype.getService = function () { return Kairos.DepartamentosService.baseUrl; };
            DepartamentosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], DepartamentosGrid);
            return DepartamentosGrid;
        }(Serenity.EntityGrid));
        Kairos.DepartamentosGrid = DepartamentosGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DiarioDialog = /** @class */ (function (_super) {
            __extends(DiarioDialog, _super);
            function DiarioDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.DiarioForm(_this.idPrefix);
                return _this;
            }
            DiarioDialog.prototype.getFormKey = function () { return Kairos.DiarioForm.formKey; };
            DiarioDialog.prototype.getIdProperty = function () { return Kairos.DiarioRow.idProperty; };
            DiarioDialog.prototype.getLocalTextPrefix = function () { return Kairos.DiarioRow.localTextPrefix; };
            DiarioDialog.prototype.getNameProperty = function () { return Kairos.DiarioRow.nameProperty; };
            DiarioDialog.prototype.getService = function () { return Kairos.DiarioService.baseUrl; };
            DiarioDialog.prototype.getDeletePermission = function () { return Kairos.DiarioRow.deletePermission; };
            DiarioDialog.prototype.getInsertPermission = function () { return Kairos.DiarioRow.insertPermission; };
            DiarioDialog.prototype.getUpdatePermission = function () { return Kairos.DiarioRow.updatePermission; };
            DiarioDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], DiarioDialog);
            return DiarioDialog;
        }(Serenity.EntityDialog));
        Kairos.DiarioDialog = DiarioDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var DiarioGrid = /** @class */ (function (_super) {
            __extends(DiarioGrid, _super);
            function DiarioGrid(container) {
                return _super.call(this, container) || this;
            }
            DiarioGrid.prototype.getColumnsKey = function () { return 'Kairos.Diario'; };
            DiarioGrid.prototype.getDialogType = function () { return Kairos.DiarioDialog; };
            DiarioGrid.prototype.getIdProperty = function () { return Kairos.DiarioRow.idProperty; };
            DiarioGrid.prototype.getInsertPermission = function () { return Kairos.DiarioRow.insertPermission; };
            DiarioGrid.prototype.getLocalTextPrefix = function () { return Kairos.DiarioRow.localTextPrefix; };
            DiarioGrid.prototype.getService = function () { return Kairos.DiarioService.baseUrl; };
            DiarioGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // Declaramos que el Grid puedes seleccionar fila
                grid.setSelectionModel(new Slick.RowSelectionModel());
                return grid;
            };
            /**
    * This method is called for all rows
    * @param item Data item for current row
    * @param index Index of the row in grid
    */
            DiarioGrid.prototype.getItemCssClass = function (item, index) {
                var klass = "";
                if (item.Salida == null)
                    klass += " out-of-stock";
                else if (item.Entrada == null)
                    klass += " critical-stock";
                return Q.trimToNull(klass);
            };
            // Botones Excel y Pdf
            DiarioGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Kairos/Diario/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            DiarioGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], DiarioGrid);
            return DiarioGrid;
        }(Serenity.EntityGrid));
        Kairos.DiarioGrid = DiarioGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var EstadosExtrasDialog = /** @class */ (function (_super) {
            __extends(EstadosExtrasDialog, _super);
            function EstadosExtrasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.EstadosExtrasForm(_this.idPrefix);
                return _this;
            }
            EstadosExtrasDialog.prototype.getFormKey = function () { return Kairos.EstadosExtrasForm.formKey; };
            EstadosExtrasDialog.prototype.getIdProperty = function () { return Kairos.EstadosExtrasRow.idProperty; };
            EstadosExtrasDialog.prototype.getLocalTextPrefix = function () { return Kairos.EstadosExtrasRow.localTextPrefix; };
            EstadosExtrasDialog.prototype.getNameProperty = function () { return Kairos.EstadosExtrasRow.nameProperty; };
            EstadosExtrasDialog.prototype.getService = function () { return Kairos.EstadosExtrasService.baseUrl; };
            EstadosExtrasDialog.prototype.getDeletePermission = function () { return Kairos.EstadosExtrasRow.deletePermission; };
            EstadosExtrasDialog.prototype.getInsertPermission = function () { return Kairos.EstadosExtrasRow.insertPermission; };
            EstadosExtrasDialog.prototype.getUpdatePermission = function () { return Kairos.EstadosExtrasRow.updatePermission; };
            EstadosExtrasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EstadosExtrasDialog);
            return EstadosExtrasDialog;
        }(Serenity.EntityDialog));
        Kairos.EstadosExtrasDialog = EstadosExtrasDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var EstadosExtrasGrid = /** @class */ (function (_super) {
            __extends(EstadosExtrasGrid, _super);
            function EstadosExtrasGrid(container) {
                return _super.call(this, container) || this;
            }
            EstadosExtrasGrid.prototype.getColumnsKey = function () { return 'Kairos.EstadosExtras'; };
            EstadosExtrasGrid.prototype.getDialogType = function () { return Kairos.EstadosExtrasDialog; };
            EstadosExtrasGrid.prototype.getIdProperty = function () { return Kairos.EstadosExtrasRow.idProperty; };
            EstadosExtrasGrid.prototype.getInsertPermission = function () { return Kairos.EstadosExtrasRow.insertPermission; };
            EstadosExtrasGrid.prototype.getLocalTextPrefix = function () { return Kairos.EstadosExtrasRow.localTextPrefix; };
            EstadosExtrasGrid.prototype.getService = function () { return Kairos.EstadosExtrasService.baseUrl; };
            EstadosExtrasGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EstadosExtrasGrid);
            return EstadosExtrasGrid;
        }(Serenity.EntityGrid));
        Kairos.EstadosExtrasGrid = EstadosExtrasGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var ExtrasDialog = /** @class */ (function (_super) {
            __extends(ExtrasDialog, _super);
            function ExtrasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.ExtrasForm(_this.idPrefix);
                return _this;
            }
            ExtrasDialog.prototype.getFormKey = function () { return Kairos.ExtrasForm.formKey; };
            ExtrasDialog.prototype.getIdProperty = function () { return Kairos.ExtrasRow.idProperty; };
            ExtrasDialog.prototype.getLocalTextPrefix = function () { return Kairos.ExtrasRow.localTextPrefix; };
            ExtrasDialog.prototype.getNameProperty = function () { return Kairos.ExtrasRow.nameProperty; };
            ExtrasDialog.prototype.getService = function () { return Kairos.ExtrasService.baseUrl; };
            ExtrasDialog.prototype.getDeletePermission = function () { return Kairos.ExtrasRow.deletePermission; };
            ExtrasDialog.prototype.getInsertPermission = function () { return Kairos.ExtrasRow.insertPermission; };
            ExtrasDialog.prototype.getUpdatePermission = function () { return Kairos.ExtrasRow.updatePermission; };
            ExtrasDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.panel()
            ], ExtrasDialog);
            return ExtrasDialog;
        }(Serenity.EntityDialog));
        Kairos.ExtrasDialog = ExtrasDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var ExtrasGrid = /** @class */ (function (_super) {
            __extends(ExtrasGrid, _super);
            function ExtrasGrid(container) {
                return _super.call(this, container) || this;
            }
            ExtrasGrid.prototype.getColumnsKey = function () { return 'Kairos.Extras'; };
            ExtrasGrid.prototype.getDialogType = function () { return Kairos.ExtrasDialog; };
            ExtrasGrid.prototype.getIdProperty = function () { return Kairos.ExtrasRow.idProperty; };
            ExtrasGrid.prototype.getInsertPermission = function () { return Kairos.ExtrasRow.insertPermission; };
            ExtrasGrid.prototype.getLocalTextPrefix = function () { return Kairos.ExtrasRow.localTextPrefix; };
            ExtrasGrid.prototype.getService = function () { return Kairos.ExtrasService.baseUrl; };
            // Agrupar y sumar 
            ExtrasGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                this.view.setSummaryOptions({
                    aggregators: [
                        new Slick.Aggregators.Sum("TotalHorasExtrasReales" /* TotalHorasExtrasReales */),
                        new Slick.Aggregators.Sum("TotalHorasExtrasConvertidas" /* TotalHorasExtrasConvertidas */),
                        new Slick.Aggregators.Sum("Pendientes" /* Pendientes */),
                        new Slick.Aggregators.Sum("TotalConsumidas" /* TotalConsumidas */)
                    ]
                });
                // Declaramos que el Grid puedes seleccionar fila
                grid.setSelectionModel(new Slick.RowSelectionModel());
                return grid;
            };
            ExtrasGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                // Mostrar pie de página
                opt.showFooterRow = true;
                // Fin pie de página
                // Fila Seleccionable
                opt.enableTextSelectionOnCells = true;
                opt.selectedCellCssClass = "slick-row-selected";
                opt.enableCellNavigation = true;
                // Fin Fila Seleccionable
                return opt;
            };
            ExtrasGrid.prototype.usePager = function () {
                return false;
            };
            // Botones Excel y Pdf
            ExtrasGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Kairos/Extras/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                // Quitamos boton de añadir para evitar que se añadan nuevas extras
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                return buttons;
                // Fin añadidos
            };
            /**
     * This method is called for all rows
     * @param item Data item for current row
     * @param index Index of the row in grid
     */
            ExtrasGrid.prototype.getItemCssClass = function (item, index) {
                var klass = "";
                if (item.Estado == "A")
                    klass += " aceptada";
                else if (item.Estado == "C")
                    klass += " rechazada";
                return Q.trimToNull(klass);
            };
            ExtrasGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], ExtrasGrid);
            return ExtrasGrid;
        }(Serenity.EntityGrid));
        Kairos.ExtrasGrid = ExtrasGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var FichajesDialog = /** @class */ (function (_super) {
            __extends(FichajesDialog, _super);
            function FichajesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.FichajesForm(_this.idPrefix);
                return _this;
            }
            FichajesDialog.prototype.getFormKey = function () { return Kairos.FichajesForm.formKey; };
            FichajesDialog.prototype.getIdProperty = function () { return Kairos.FichajesRow.idProperty; };
            FichajesDialog.prototype.getLocalTextPrefix = function () { return Kairos.FichajesRow.localTextPrefix; };
            FichajesDialog.prototype.getNameProperty = function () { return Kairos.FichajesRow.nameProperty; };
            FichajesDialog.prototype.getService = function () { return Kairos.FichajesService.baseUrl; };
            FichajesDialog.prototype.getDeletePermission = function () { return Kairos.FichajesRow.deletePermission; };
            FichajesDialog.prototype.getInsertPermission = function () { return Kairos.FichajesRow.insertPermission; };
            FichajesDialog.prototype.getUpdatePermission = function () { return Kairos.FichajesRow.updatePermission; };
            FichajesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], FichajesDialog);
            return FichajesDialog;
        }(Serenity.EntityDialog));
        Kairos.FichajesDialog = FichajesDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var FichajesGrid = /** @class */ (function (_super) {
            __extends(FichajesGrid, _super);
            function FichajesGrid(container) {
                return _super.call(this, container) || this;
            }
            FichajesGrid.prototype.getColumnsKey = function () { return 'Kairos.Fichajes'; };
            FichajesGrid.prototype.getDialogType = function () { return Kairos.FichajesDialog; };
            FichajesGrid.prototype.getIdProperty = function () { return Kairos.FichajesRow.idProperty; };
            FichajesGrid.prototype.getInsertPermission = function () { return Kairos.FichajesRow.insertPermission; };
            FichajesGrid.prototype.getLocalTextPrefix = function () { return Kairos.FichajesRow.localTextPrefix; };
            FichajesGrid.prototype.getService = function () { return Kairos.FichajesService.baseUrl; };
            // Botones Excel y Pdf
            FichajesGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Kairos/Fichajes/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            /**
    * This method is called for all rows
    * @param item Data item for current row
    * @param index Index of the row in grid
    */
            FichajesGrid.prototype.getItemCssClass = function (item, index) {
                var klass = "";
                if (item.Files != null)
                    klass += " aceptada";
                return Q.trimToNull(klass);
            };
            FichajesGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], FichajesGrid);
            return FichajesGrid;
        }(Serenity.EntityGrid));
        Kairos.FichajesGrid = FichajesGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HorasExtraConsumidasDialog = /** @class */ (function (_super) {
            __extends(HorasExtraConsumidasDialog, _super);
            function HorasExtraConsumidasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.HorasExtraConsumidasForm(_this.idPrefix);
                return _this;
            }
            HorasExtraConsumidasDialog.prototype.getFormKey = function () { return Kairos.HorasExtraConsumidasForm.formKey; };
            HorasExtraConsumidasDialog.prototype.getIdProperty = function () { return Kairos.HorasExtraConsumidasRow.idProperty; };
            HorasExtraConsumidasDialog.prototype.getLocalTextPrefix = function () { return Kairos.HorasExtraConsumidasRow.localTextPrefix; };
            HorasExtraConsumidasDialog.prototype.getService = function () { return Kairos.HorasExtraConsumidasService.baseUrl; };
            HorasExtraConsumidasDialog.prototype.getDeletePermission = function () { return Kairos.HorasExtraConsumidasRow.deletePermission; };
            HorasExtraConsumidasDialog.prototype.getInsertPermission = function () { return Kairos.HorasExtraConsumidasRow.insertPermission; };
            HorasExtraConsumidasDialog.prototype.getUpdatePermission = function () { return Kairos.HorasExtraConsumidasRow.updatePermission; };
            HorasExtraConsumidasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], HorasExtraConsumidasDialog);
            return HorasExtraConsumidasDialog;
        }(Serenity.EntityDialog));
        Kairos.HorasExtraConsumidasDialog = HorasExtraConsumidasDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HorasExtraConsumidasEditDialog = /** @class */ (function (_super) {
            __extends(HorasExtraConsumidasEditDialog, _super);
            function HorasExtraConsumidasEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Kairos.HorasExtraConsumidasForm(_this.idPrefix);
                return _this;
            }
            HorasExtraConsumidasEditDialog.prototype.getFormKey = function () { return Kairos.HorasExtraConsumidasForm.formKey; };
            HorasExtraConsumidasEditDialog.prototype.getNameProperty = function () { return Kairos.HorasExtraConsumidasRow.nameProperty; };
            HorasExtraConsumidasEditDialog.prototype.getLocalTextPrefix = function () { return Kairos.HorasExtraConsumidasRow.localTextPrefix; };
            HorasExtraConsumidasEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], HorasExtraConsumidasEditDialog);
            return HorasExtraConsumidasEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Kairos.HorasExtraConsumidasEditDialog = HorasExtraConsumidasEditDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HorasExtraConsumidasEditor = /** @class */ (function (_super) {
            __extends(HorasExtraConsumidasEditor, _super);
            function HorasExtraConsumidasEditor(container) {
                return _super.call(this, container) || this;
            }
            HorasExtraConsumidasEditor.prototype.getColumnsKey = function () { return "Kairos.HorasExtraConsumidas"; };
            HorasExtraConsumidasEditor.prototype.getDialogType = function () { return Kairos.HorasExtraConsumidasEditDialog; };
            HorasExtraConsumidasEditor.prototype.getLocalTextPrefix = function () { return Kairos.HorasExtraConsumidasRow.localTextPrefix; };
            HorasExtraConsumidasEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Hora Extra Consumida";
            };
            HorasExtraConsumidasEditor = __decorate([
                Serenity.Decorators.registerEditor()
            ], HorasExtraConsumidasEditor);
            return HorasExtraConsumidasEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Kairos.HorasExtraConsumidasEditor = HorasExtraConsumidasEditor;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HorasExtraConsumidasGrid = /** @class */ (function (_super) {
            __extends(HorasExtraConsumidasGrid, _super);
            function HorasExtraConsumidasGrid(container) {
                return _super.call(this, container) || this;
            }
            HorasExtraConsumidasGrid.prototype.getColumnsKey = function () { return 'Kairos.HorasExtraConsumidas'; };
            HorasExtraConsumidasGrid.prototype.getDialogType = function () { return Kairos.HorasExtraConsumidasDialog; };
            HorasExtraConsumidasGrid.prototype.getIdProperty = function () { return Kairos.HorasExtraConsumidasRow.idProperty; };
            HorasExtraConsumidasGrid.prototype.getInsertPermission = function () { return Kairos.HorasExtraConsumidasRow.insertPermission; };
            HorasExtraConsumidasGrid.prototype.getLocalTextPrefix = function () { return Kairos.HorasExtraConsumidasRow.localTextPrefix; };
            HorasExtraConsumidasGrid.prototype.getService = function () { return Kairos.HorasExtraConsumidasService.baseUrl; };
            // Botones Excel y Pdf
            HorasExtraConsumidasGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Kairos/HorasExtraConsumidas/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                // Quitamos boton de añadir para evitar que se añadan nuevas extras
                //            buttons.splice(Q.indexOf(buttons, x => x.cssClass == "add-button"), 1);
                return buttons;
                // Fin añadidos
            };
            HorasExtraConsumidasGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], HorasExtraConsumidasGrid);
            return HorasExtraConsumidasGrid;
        }(Serenity.EntityGrid));
        Kairos.HorasExtraConsumidasGrid = HorasExtraConsumidasGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../HorasExtraConsumidas/HorasExtraConsumidasDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HorasExtraConsumidasReadOnlyDialog = /** @class */ (function (_super) {
            __extends(HorasExtraConsumidasReadOnlyDialog, _super);
            function HorasExtraConsumidasReadOnlyDialog() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             * This is the method that gets list of tool
             * buttons to be created in a dialog.
             *
             * Here we'll remove save and close button, and
             * apply changes buttons.
             */
            HorasExtraConsumidasReadOnlyDialog.prototype.getToolbarButtons = function () {
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "save-and-close-button"; }), 1);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "apply-changes-button"; }), 1);
                // We could also remove delete button here, but for demonstration 
                // purposes we'll hide it in another method (updateInterface)
                // buttons.splice(Q.indexOf(buttons, x => x.cssClass == "delete-button"), 1);
                return buttons;
            };
            /**
             * This method is a good place to update states of
             * interface elements. It is called after dialog
             * is initialized and an entity is loaded into dialog.
             * This is also called in new item mode.
             */
            HorasExtraConsumidasReadOnlyDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                // finding all editor elements and setting their readonly attribute
                // note that this helper method only works with basic inputs, 
                // some editors require widget based set readonly overload (setReadOnly)
                Serenity.EditorUtils.setReadonly(this.element.find('.editor'), true);
                // remove required asterisk (*)
                this.element.find('sup').hide();
                // here is a way to locate a button by its css class
                // note that this method is not available in 
                // getToolbarButtons() because buttons are not 
                // created there yet!
                // 
                // this.toolbar.findButton('delete-button').hide();
                // entity dialog also has reference variables to
                // its default buttons, lets use them to hide delete button
                this.deleteButton.hide();
                // we could also hide save buttons just like delete button,
                // but they are null now as we removed them in getToolbarButtons()
                // if we didn't we could write like this:
                // 
                // this.applyChangesButton.hide();
                // this.saveAndCloseButton.hide();
                // instead of hiding, we could disable a button
                // 
                // this.deleteButton.toggleClass('disabled', true);
            };
            /**
             * This method is called when dialog title needs to be updated.
             * Base class returns something like 'Edit xyz' for edit mode,
             * and 'New xyz' for new record mode.
             *
             * But our dialog is readonly, so we should change it to 'View xyz'
             */
            HorasExtraConsumidasReadOnlyDialog.prototype.getEntityTitle = function () {
                if (!this.isEditMode()) {
                    // we shouldn't hit here, but anyway for demo...
                    return "Como has conseguido entrar aquí?. Avisa a Informática";
                }
                else {
                    // entitySingular is type of record this dialog edits. something like 'Supplier'.
                    // you could hardcode it, but this is for demonstration
                    var entityType = _super.prototype.getEntitySingular.call(this);
                    // get name field value of record this dialog edits
                    var name_1 = this.getEntityNameFieldValue() || "";
                    // you could use Q.format with a local text, but again demo...
                    return 'Vista ' + entityType + " (" + name_1 + ")";
                }
            };
            /**
             * This method is actually the one that calls getEntityTitle()
             * and updates the dialog title. We could do it here too...
             */
            HorasExtraConsumidasReadOnlyDialog.prototype.updateTitle = function () {
                _super.prototype.updateTitle.call(this);
                // remove super.updateTitle() call above and uncomment 
                // below line if you'd like to use this version
                // 
                // this.dialogTitle = 'View Supplier (' + this.getEntityNameFieldValue() + ')';
            };
            HorasExtraConsumidasReadOnlyDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], HorasExtraConsumidasReadOnlyDialog);
            return HorasExtraConsumidasReadOnlyDialog;
        }(Kairos.HorasExtraConsumidasDialog));
        Kairos.HorasExtraConsumidasReadOnlyDialog = HorasExtraConsumidasReadOnlyDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../HorasExtraConsumidas/HorasExtraConsumidasGrid.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HorasExtraConsumidasReadOnlyGrid = /** @class */ (function (_super) {
            __extends(HorasExtraConsumidasReadOnlyGrid, _super);
            function HorasExtraConsumidasReadOnlyGrid(container) {
                return _super.call(this, container) || this;
            }
            HorasExtraConsumidasReadOnlyGrid.prototype.getColumnsKey = function () { return 'Kairos.HorasExtraConsumidasReadOnly'; };
            HorasExtraConsumidasReadOnlyGrid.prototype.getDialogType = function () { return Kairos.HorasExtraConsumidasReadOnlyDialog; };
            /**
         * Removing add button from grid using its css class
         */
            HorasExtraConsumidasReadOnlyGrid.prototype.getButtons = function () {
                var buttons = _super.prototype.getButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                //buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                //    grid: this,
                //    onViewSubmit: () => this.onViewSubmit(),
                //    service: 'Kairos/HorasExtraConsumidas/ListExcel',
                //    separator: true
                //}));
                //buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                //    grid: this,
                //    onViewSubmit: () => this.onViewSubmit()
                //}));
                return buttons;
            };
            HorasExtraConsumidasReadOnlyGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], HorasExtraConsumidasReadOnlyGrid);
            return HorasExtraConsumidasReadOnlyGrid;
        }(Kairos.HorasExtraConsumidasGrid));
        Kairos.HorasExtraConsumidasReadOnlyGrid = HorasExtraConsumidasReadOnlyGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HoyDialog = /** @class */ (function (_super) {
            __extends(HoyDialog, _super);
            function HoyDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.HoyForm(_this.idPrefix);
                return _this;
            }
            HoyDialog.prototype.getFormKey = function () { return Kairos.HoyForm.formKey; };
            HoyDialog.prototype.getIdProperty = function () { return Kairos.HoyRow.idProperty; };
            HoyDialog.prototype.getLocalTextPrefix = function () { return Kairos.HoyRow.localTextPrefix; };
            HoyDialog.prototype.getNameProperty = function () { return Kairos.HoyRow.nameProperty; };
            HoyDialog.prototype.getService = function () { return Kairos.HoyService.baseUrl; };
            HoyDialog.prototype.getDeletePermission = function () { return Kairos.HoyRow.deletePermission; };
            HoyDialog.prototype.getInsertPermission = function () { return Kairos.HoyRow.insertPermission; };
            HoyDialog.prototype.getUpdatePermission = function () { return Kairos.HoyRow.updatePermission; };
            HoyDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], HoyDialog);
            return HoyDialog;
        }(Serenity.EntityDialog));
        Kairos.HoyDialog = HoyDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var HoyGrid = /** @class */ (function (_super) {
            __extends(HoyGrid, _super);
            function HoyGrid(container) {
                return _super.call(this, container) || this;
            }
            HoyGrid.prototype.getColumnsKey = function () { return 'Kairos.Hoy'; };
            HoyGrid.prototype.getDialogType = function () { return Kairos.HoyDialog; };
            HoyGrid.prototype.getIdProperty = function () { return Kairos.HoyRow.idProperty; };
            HoyGrid.prototype.getInsertPermission = function () { return Kairos.HoyRow.insertPermission; };
            HoyGrid.prototype.getLocalTextPrefix = function () { return Kairos.HoyRow.localTextPrefix; };
            HoyGrid.prototype.getService = function () { return Kairos.HoyService.baseUrl; };
            /**
    * We override getColumns() to be able to add a custom CSS class to UnitPrice
    * We could also add this class in ProductColumns.cs but didn't want to modify
    * it solely for this sample.
    */
            //protected getColumns(): Slick.Column[] {
            //    var columns = super.getColumns();
            //    // adding a specific css class to UnitPrice column, 
            //    // to be able to format cell with a different background
            //    Q.first(columns, x => x.field == fld.Estado).cssClass += " col-unit-price";
            //    return columns;
            //}
            /**
     * This method is called for all rows
     * @param item Data item for current row
     * @param index Index of the row in grid
     */
            HoyGrid.prototype.getItemCssClass = function (item, index) {
                var klass = "";
                if (item.Estado == "TELETRABAJO")
                    klass += " critical-stock";
                else if (item.Estado != "PRESENCIAL")
                    klass += " out-of-stock";
                return Q.trimToNull(klass);
            };
            // Botones Excel y Pdf
            HoyGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Kairos/Hoy/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            HoyGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
                ,
                Serenity.Decorators.registerClass()
            ], HoyGrid);
            return HoyGrid;
        }(Serenity.EntityGrid));
        Kairos.HoyGrid = HoyGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsAusenciasProgramadasTiposDialog = /** @class */ (function (_super) {
            __extends(KrsAusenciasProgramadasTiposDialog, _super);
            function KrsAusenciasProgramadasTiposDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.KrsAusenciasProgramadasTiposForm(_this.idPrefix);
                return _this;
            }
            KrsAusenciasProgramadasTiposDialog.prototype.getFormKey = function () { return Kairos.KrsAusenciasProgramadasTiposForm.formKey; };
            KrsAusenciasProgramadasTiposDialog.prototype.getIdProperty = function () { return Kairos.KrsAusenciasProgramadasTiposRow.idProperty; };
            KrsAusenciasProgramadasTiposDialog.prototype.getLocalTextPrefix = function () { return Kairos.KrsAusenciasProgramadasTiposRow.localTextPrefix; };
            KrsAusenciasProgramadasTiposDialog.prototype.getNameProperty = function () { return Kairos.KrsAusenciasProgramadasTiposRow.nameProperty; };
            KrsAusenciasProgramadasTiposDialog.prototype.getService = function () { return Kairos.KrsAusenciasProgramadasTiposService.baseUrl; };
            KrsAusenciasProgramadasTiposDialog.prototype.getDeletePermission = function () { return Kairos.KrsAusenciasProgramadasTiposRow.deletePermission; };
            KrsAusenciasProgramadasTiposDialog.prototype.getInsertPermission = function () { return Kairos.KrsAusenciasProgramadasTiposRow.insertPermission; };
            KrsAusenciasProgramadasTiposDialog.prototype.getUpdatePermission = function () { return Kairos.KrsAusenciasProgramadasTiposRow.updatePermission; };
            KrsAusenciasProgramadasTiposDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], KrsAusenciasProgramadasTiposDialog);
            return KrsAusenciasProgramadasTiposDialog;
        }(Serenity.EntityDialog));
        Kairos.KrsAusenciasProgramadasTiposDialog = KrsAusenciasProgramadasTiposDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsAusenciasProgramadasTiposGrid = /** @class */ (function (_super) {
            __extends(KrsAusenciasProgramadasTiposGrid, _super);
            function KrsAusenciasProgramadasTiposGrid(container) {
                return _super.call(this, container) || this;
            }
            KrsAusenciasProgramadasTiposGrid.prototype.getColumnsKey = function () { return 'Kairos.KrsAusenciasProgramadasTipos'; };
            KrsAusenciasProgramadasTiposGrid.prototype.getDialogType = function () { return Kairos.KrsAusenciasProgramadasTiposDialog; };
            KrsAusenciasProgramadasTiposGrid.prototype.getIdProperty = function () { return Kairos.KrsAusenciasProgramadasTiposRow.idProperty; };
            KrsAusenciasProgramadasTiposGrid.prototype.getInsertPermission = function () { return Kairos.KrsAusenciasProgramadasTiposRow.insertPermission; };
            KrsAusenciasProgramadasTiposGrid.prototype.getLocalTextPrefix = function () { return Kairos.KrsAusenciasProgramadasTiposRow.localTextPrefix; };
            KrsAusenciasProgramadasTiposGrid.prototype.getService = function () { return Kairos.KrsAusenciasProgramadasTiposService.baseUrl; };
            KrsAusenciasProgramadasTiposGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], KrsAusenciasProgramadasTiposGrid);
            return KrsAusenciasProgramadasTiposGrid;
        }(Serenity.EntityGrid));
        Kairos.KrsAusenciasProgramadasTiposGrid = KrsAusenciasProgramadasTiposGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsEmpleadosDialog = /** @class */ (function (_super) {
            __extends(KrsEmpleadosDialog, _super);
            function KrsEmpleadosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.KrsEmpleadosForm(_this.idPrefix);
                return _this;
            }
            KrsEmpleadosDialog.prototype.getFormKey = function () { return Kairos.KrsEmpleadosForm.formKey; };
            KrsEmpleadosDialog.prototype.getIdProperty = function () { return Kairos.KrsEmpleadosRow.idProperty; };
            KrsEmpleadosDialog.prototype.getLocalTextPrefix = function () { return Kairos.KrsEmpleadosRow.localTextPrefix; };
            KrsEmpleadosDialog.prototype.getNameProperty = function () { return Kairos.KrsEmpleadosRow.nameProperty; };
            KrsEmpleadosDialog.prototype.getService = function () { return Kairos.KrsEmpleadosService.baseUrl; };
            KrsEmpleadosDialog.prototype.getDeletePermission = function () { return Kairos.KrsEmpleadosRow.deletePermission; };
            KrsEmpleadosDialog.prototype.getInsertPermission = function () { return Kairos.KrsEmpleadosRow.insertPermission; };
            KrsEmpleadosDialog.prototype.getUpdatePermission = function () { return Kairos.KrsEmpleadosRow.updatePermission; };
            KrsEmpleadosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], KrsEmpleadosDialog);
            return KrsEmpleadosDialog;
        }(Serenity.EntityDialog));
        Kairos.KrsEmpleadosDialog = KrsEmpleadosDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var KrsEmpleadosGrid = /** @class */ (function (_super) {
            __extends(KrsEmpleadosGrid, _super);
            function KrsEmpleadosGrid(container) {
                return _super.call(this, container) || this;
            }
            KrsEmpleadosGrid.prototype.getColumnsKey = function () { return 'Kairos.KrsEmpleados'; };
            KrsEmpleadosGrid.prototype.getDialogType = function () { return Kairos.KrsEmpleadosDialog; };
            KrsEmpleadosGrid.prototype.getIdProperty = function () { return Kairos.KrsEmpleadosRow.idProperty; };
            KrsEmpleadosGrid.prototype.getInsertPermission = function () { return Kairos.KrsEmpleadosRow.insertPermission; };
            KrsEmpleadosGrid.prototype.getLocalTextPrefix = function () { return Kairos.KrsEmpleadosRow.localTextPrefix; };
            KrsEmpleadosGrid.prototype.getService = function () { return Kairos.KrsEmpleadosService.baseUrl; };
            KrsEmpleadosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], KrsEmpleadosGrid);
            return KrsEmpleadosGrid;
        }(Serenity.EntityGrid));
        Kairos.KrsEmpleadosGrid = KrsEmpleadosGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var TiposFichajeDialog = /** @class */ (function (_super) {
            __extends(TiposFichajeDialog, _super);
            function TiposFichajeDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Kairos.TiposFichajeForm(_this.idPrefix);
                return _this;
            }
            TiposFichajeDialog.prototype.getFormKey = function () { return Kairos.TiposFichajeForm.formKey; };
            TiposFichajeDialog.prototype.getIdProperty = function () { return Kairos.TiposFichajeRow.idProperty; };
            TiposFichajeDialog.prototype.getLocalTextPrefix = function () { return Kairos.TiposFichajeRow.localTextPrefix; };
            TiposFichajeDialog.prototype.getNameProperty = function () { return Kairos.TiposFichajeRow.nameProperty; };
            TiposFichajeDialog.prototype.getService = function () { return Kairos.TiposFichajeService.baseUrl; };
            TiposFichajeDialog.prototype.getDeletePermission = function () { return Kairos.TiposFichajeRow.deletePermission; };
            TiposFichajeDialog.prototype.getInsertPermission = function () { return Kairos.TiposFichajeRow.insertPermission; };
            TiposFichajeDialog.prototype.getUpdatePermission = function () { return Kairos.TiposFichajeRow.updatePermission; };
            TiposFichajeDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposFichajeDialog);
            return TiposFichajeDialog;
        }(Serenity.EntityDialog));
        Kairos.TiposFichajeDialog = TiposFichajeDialog;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Kairos;
    (function (Kairos) {
        var TiposFichajeGrid = /** @class */ (function (_super) {
            __extends(TiposFichajeGrid, _super);
            function TiposFichajeGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposFichajeGrid.prototype.getColumnsKey = function () { return 'Kairos.TiposFichaje'; };
            TiposFichajeGrid.prototype.getDialogType = function () { return Kairos.TiposFichajeDialog; };
            TiposFichajeGrid.prototype.getIdProperty = function () { return Kairos.TiposFichajeRow.idProperty; };
            TiposFichajeGrid.prototype.getInsertPermission = function () { return Kairos.TiposFichajeRow.insertPermission; };
            TiposFichajeGrid.prototype.getLocalTextPrefix = function () { return Kairos.TiposFichajeRow.localTextPrefix; };
            TiposFichajeGrid.prototype.getService = function () { return Kairos.TiposFichajeService.baseUrl; };
            TiposFichajeGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposFichajeGrid);
            return TiposFichajeGrid;
        }(Serenity.EntityGrid));
        Kairos.TiposFichajeGrid = TiposFichajeGrid;
    })(Kairos = ProyectosZec.Kairos || (ProyectosZec.Kairos = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Membership;
    (function (Membership) {
        var ChangePasswordPanel = /** @class */ (function (_super) {
            __extends(ChangePasswordPanel, _super);
            function ChangePasswordPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.ChangePasswordForm(_this.idPrefix);
                _this.form.NewPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.w('ConfirmPassword', Serenity.PasswordEditor).value.length < 7) {
                        return Q.format(Q.text('Validation.MinRequiredPasswordLength'), 7);
                    }
                });
                _this.form.ConfirmPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value !== _this.form.NewPassword.value) {
                        return Q.text('Validation.PasswordConfirm');
                    }
                });
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/ChangePassword'),
                        request: request,
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.ChangePassword.Success'), function () {
                                window.location.href = Q.resolveUrl('~/');
                            });
                        }
                    });
                });
                return _this;
            }
            ChangePasswordPanel.prototype.getFormKey = function () { return Membership.ChangePasswordForm.formKey; };
            ChangePasswordPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], ChangePasswordPanel);
            return ChangePasswordPanel;
        }(Serenity.PropertyPanel));
        Membership.ChangePasswordPanel = ChangePasswordPanel;
    })(Membership = ProyectosZec.Membership || (ProyectosZec.Membership = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Membership;
    (function (Membership) {
        var ForgotPasswordPanel = /** @class */ (function (_super) {
            __extends(ForgotPasswordPanel, _super);
            function ForgotPasswordPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.ForgotPasswordForm(_this.idPrefix);
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/ForgotPassword'),
                        request: request,
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.ForgotPassword.Success'), function () {
                                window.location.href = Q.resolveUrl('~/');
                            });
                        }
                    });
                });
                return _this;
            }
            ForgotPasswordPanel.prototype.getFormKey = function () { return Membership.ForgotPasswordForm.formKey; };
            ForgotPasswordPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], ForgotPasswordPanel);
            return ForgotPasswordPanel;
        }(Serenity.PropertyPanel));
        Membership.ForgotPasswordPanel = ForgotPasswordPanel;
    })(Membership = ProyectosZec.Membership || (ProyectosZec.Membership = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Membership;
    (function (Membership) {
        var ResetPasswordPanel = /** @class */ (function (_super) {
            __extends(ResetPasswordPanel, _super);
            function ResetPasswordPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.ResetPasswordForm(_this.idPrefix);
                _this.form.NewPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value.length < 7) {
                        return Q.format(Q.text('Validation.MinRequiredPasswordLength'), 7);
                    }
                });
                _this.form.ConfirmPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value !== _this.form.NewPassword.value) {
                        return Q.text('Validation.PasswordConfirm');
                    }
                });
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    request.Token = _this.byId('Token').val();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/ResetPassword'),
                        request: request,
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.ResetPassword.Success'), function () {
                                window.location.href = Q.resolveUrl('~/Account/Login');
                            });
                        }
                    });
                });
                return _this;
            }
            ResetPasswordPanel.prototype.getFormKey = function () { return Membership.ResetPasswordForm.formKey; };
            ResetPasswordPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], ResetPasswordPanel);
            return ResetPasswordPanel;
        }(Serenity.PropertyPanel));
        Membership.ResetPasswordPanel = ResetPasswordPanel;
    })(Membership = ProyectosZec.Membership || (ProyectosZec.Membership = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Membership;
    (function (Membership) {
        var SignUpPanel = /** @class */ (function (_super) {
            __extends(SignUpPanel, _super);
            function SignUpPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.SignUpForm(_this.idPrefix);
                _this.form.ConfirmEmail.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmEmail.value !== _this.form.Email.value) {
                        return Q.text('Validation.EmailConfirm');
                    }
                });
                _this.form.ConfirmPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value !== _this.form.Password.value) {
                        return Q.text('Validation.PasswordConfirm');
                    }
                });
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/SignUp'),
                        request: {
                            DisplayName: _this.form.DisplayName.value,
                            Email: _this.form.Email.value,
                            Password: _this.form.Password.value
                        },
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.SignUp.Success'), function () {
                                window.location.href = Q.resolveUrl('~/');
                            });
                        }
                    });
                });
                return _this;
            }
            SignUpPanel.prototype.getFormKey = function () { return Membership.SignUpForm.formKey; };
            SignUpPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], SignUpPanel);
            return SignUpPanel;
        }(Serenity.PropertyPanel));
        Membership.SignUpPanel = SignUpPanel;
    })(Membership = ProyectosZec.Membership || (ProyectosZec.Membership = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasDialog = /** @class */ (function (_super) {
            __extends(AlarmasDialog, _super);
            function AlarmasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.AlarmasForm(_this.idPrefix);
                return _this;
            }
            AlarmasDialog.prototype.getFormKey = function () { return Nuevo_Roezec.AlarmasForm.formKey; };
            AlarmasDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.AlarmasRow.idProperty; };
            AlarmasDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.AlarmasRow.localTextPrefix; };
            AlarmasDialog.prototype.getService = function () { return Nuevo_Roezec.AlarmasService.baseUrl; };
            AlarmasDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.AlarmasRow.deletePermission; };
            AlarmasDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.AlarmasRow.insertPermission; };
            AlarmasDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.AlarmasRow.updatePermission; };
            AlarmasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], AlarmasDialog);
            return AlarmasDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.AlarmasDialog = AlarmasDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var fld = Nuevo_Roezec.AlarmasRow.Fields;
        var AlarmasGrid = /** @class */ (function (_super) {
            __extends(AlarmasGrid, _super);
            function AlarmasGrid(container) {
                return _super.call(this, container) || this;
            }
            AlarmasGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Alarmas'; };
            AlarmasGrid.prototype.getDialogType = function () { return Nuevo_Roezec.AlarmasDialog; };
            AlarmasGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.AlarmasRow.idProperty; };
            AlarmasGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.AlarmasRow.insertPermission; };
            AlarmasGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.AlarmasRow.localTextPrefix; };
            AlarmasGrid.prototype.getService = function () { return Nuevo_Roezec.AlarmasService.baseUrl; };
            // Botones Excel y Pdf
            AlarmasGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/Alarmas/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            // Añadir la siguiente función
            AlarmasGrid.prototype.getDefaultSortBy = function () {
                return [Nuevo_Roezec.AlarmasRow.Fields.FechaAviso]; // Este es el campo de ordenación por defecto
            };
            /**
             * We override getColumns() to change format functions for some columns.
             * You could also write them as formatter classes, and use them at server side
             */
            AlarmasGrid.prototype.getColumns = function () {
                var columns = _super.prototype.getColumns.call(this);
                Q.first(columns, function (x) { return x.field == fld.EmpresaRazon; }).format =
                    function (ctx) { return "<a href=\"javascript:;\" class=\"empresa-link\">" + Q.htmlEncode(ctx.value) + "</a>"; };
                return columns;
            };
            AlarmasGrid.prototype.onClick = function (e, row, cell) {
                // let base grid handle clicks for its edit links
                _super.prototype.onClick.call(this, e, row, cell);
                // if base grid already handled, we shouldn"t handle it again
                if (e.isDefaultPrevented()) {
                    return;
                }
                // get reference to current item
                var item = this.itemAt(row);
                // get reference to clicked element
                var target = $(e.target);
                if (target.hasClass("empresa-link")) {
                    e.preventDefault();
                    new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                    //    let message = Q.format(
                    //        "<p>Has pulsado sobre la la empresa {0}.</p>" +
                    //        "<p>Si pulsas sobre Si, abrimos la empresa.</p>" +
                    //        "<p>Si pulsas NO, abrimos la alarma.</p>",
                    //        Q.htmlEncode(item.EmpresaRazon));
                    //    Q.confirm(message, () => {
                    //        new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                    //    },
                    //        {
                    //            htmlEncode: false,
                    //            onNo: () => {
                    //                new Nuevo_Roezec.AlarmasDialog().loadByIdAndOpenDialog(item.AlarmaId);
                    //            }
                    //        });
                }
            };
            /**
            * This method is called for all rows
            * @param item Data item for current row
            * @param index Index of the row in grid
            */
            AlarmasGrid.prototype.getItemCssClass = function (item, index) {
                // hoy is Currentdate
                var hoy = new Date();
                var klass = ""; // Default
                if (item.Activa == 0)
                    klass += " discontinued";
                else if (Q.formatDate(item.FechaCaducidad, 'yyyy-MM-dd') <= Q.formatDate(hoy, 'yyyy-MM-dd'))
                    klass += " out-of-stock";
                else if (Q.formatDate(item.FechaAviso, 'yyyy-MM-dd') <= Q.formatDate(hoy, 'yyyy-MM-dd'))
                    klass += " critical-stock";
                return Q.trimToNull(klass);
            };
            /**
     * This method is called to get list of quick filters to be created for this grid.
     * By default, it returns quick filter objects corresponding to properties that
     * have a [QuickFilter] attribute at server side OrderColumns.cs
     */
            AlarmasGrid.prototype.getQuickFilters = function () {
                // get quick filter list from base class
                var filters = _super.prototype.getQuickFilters.call(this);
                // Current month & year
                var CurYear = new Date().getFullYear();
                var CurMonth = new Date().getMonth();
                var StartMonth = CurMonth - 2; // Miramos los 2 meses anteriores a hoy
                var EndMonth = CurMonth + 6; // y los 6 siguientes al mes actual
                var StartYear = CurYear;
                var EndYear = CurYear;
                if (StartMonth < 0) { // OJO que Javascript los meses empiezan en el 0
                    StartMonth = 11 - StartMonth;
                    StartYear -= 1;
                }
                if (EndMonth > 11) {
                    EndYear += 1;
                    EndMonth -= 11;
                }
                // quick filter init method is a good place to set initial
                // value for a quick filter editor, just after it is created
                Q.first(filters, function (x) { return x.field == fld.FechaAviso; }).init = function (w) {
                    // w is a reference to the editor for this quick filter widget
                    // here we cast it to DateEditor, and set its value as date.
                    // note that in Javascript, months are 0 based, so date below
                    // is actually 2016-05-01
                    w.valueAsDate = new Date(StartYear, StartMonth, 1);
                    // setting start date was simple. but this quick filter is actually
                    // a combination of two date editors. to get reference to second one,
                    // need to find its next sibling element by its class
                    var endDate = w.element.nextAll(".s-DateEditor").getWidget(Serenity.DateEditor);
                    endDate.valueAsDate = new Date(EndYear, EndMonth, 1);
                };
                Q.first(filters, function (x) { return x.field == fld.Activa; }).init = function (w) {
                    w.value = 1;
                };
                return filters;
            };
            AlarmasGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], AlarmasGrid);
            return AlarmasGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.AlarmasGrid = AlarmasGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasProcedimientosDialog = /** @class */ (function (_super) {
            __extends(AlarmasProcedimientosDialog, _super);
            function AlarmasProcedimientosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.AlarmasProcedimientosForm(_this.idPrefix);
                return _this;
            }
            AlarmasProcedimientosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.AlarmasProcedimientosForm.formKey; };
            AlarmasProcedimientosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.idProperty; };
            AlarmasProcedimientosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.localTextPrefix; };
            AlarmasProcedimientosDialog.prototype.getService = function () { return Nuevo_Roezec.AlarmasProcedimientosService.baseUrl; };
            AlarmasProcedimientosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.deletePermission; };
            AlarmasProcedimientosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.insertPermission; };
            AlarmasProcedimientosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.updatePermission; };
            AlarmasProcedimientosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], AlarmasProcedimientosDialog);
            return AlarmasProcedimientosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.AlarmasProcedimientosDialog = AlarmasProcedimientosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasProcedimientosEditDialog = /** @class */ (function (_super) {
            __extends(AlarmasProcedimientosEditDialog, _super);
            function AlarmasProcedimientosEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.AlarmasProcedimientosForm(_this.idPrefix);
                return _this;
            }
            AlarmasProcedimientosEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.AlarmasProcedimientosForm.formKey; };
            AlarmasProcedimientosEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.nameProperty; };
            AlarmasProcedimientosEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.localTextPrefix; };
            AlarmasProcedimientosEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], AlarmasProcedimientosEditDialog);
            return AlarmasProcedimientosEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.AlarmasProcedimientosEditDialog = AlarmasProcedimientosEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasProcedimientosEditor = /** @class */ (function (_super) {
            __extends(AlarmasProcedimientosEditor, _super);
            function AlarmasProcedimientosEditor(container) {
                return _super.call(this, container) || this;
            }
            AlarmasProcedimientosEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.AlarmasProcedimientos"; };
            AlarmasProcedimientosEditor.prototype.getDialogType = function () { return Nuevo_Roezec.AlarmasProcedimientosEditDialog; };
            AlarmasProcedimientosEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.localTextPrefix; };
            AlarmasProcedimientosEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Alarma";
            };
            AlarmasProcedimientosEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], AlarmasProcedimientosEditor);
            return AlarmasProcedimientosEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.AlarmasProcedimientosEditor = AlarmasProcedimientosEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var AlarmasProcedimientosGrid = /** @class */ (function (_super) {
            __extends(AlarmasProcedimientosGrid, _super);
            function AlarmasProcedimientosGrid(container) {
                return _super.call(this, container) || this;
            }
            AlarmasProcedimientosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.AlarmasProcedimientos'; };
            AlarmasProcedimientosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.AlarmasProcedimientosDialog; };
            AlarmasProcedimientosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.idProperty; };
            AlarmasProcedimientosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.insertPermission; };
            AlarmasProcedimientosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.AlarmasProcedimientosRow.localTextPrefix; };
            AlarmasProcedimientosGrid.prototype.getService = function () { return Nuevo_Roezec.AlarmasProcedimientosService.baseUrl; };
            AlarmasProcedimientosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AlarmasProcedimientosGrid);
            return AlarmasProcedimientosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.AlarmasProcedimientosGrid = AlarmasProcedimientosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var CapitalDialog = /** @class */ (function (_super) {
            __extends(CapitalDialog, _super);
            function CapitalDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.CapitalForm(_this.idPrefix);
                return _this;
            }
            CapitalDialog.prototype.getFormKey = function () { return Nuevo_Roezec.CapitalForm.formKey; };
            CapitalDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.CapitalRow.idProperty; };
            CapitalDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.CapitalRow.localTextPrefix; };
            CapitalDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.CapitalRow.nameProperty; };
            CapitalDialog.prototype.getService = function () { return Nuevo_Roezec.CapitalService.baseUrl; };
            CapitalDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.CapitalRow.deletePermission; };
            CapitalDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.CapitalRow.insertPermission; };
            CapitalDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.CapitalRow.updatePermission; };
            CapitalDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], CapitalDialog);
            return CapitalDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.CapitalDialog = CapitalDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var CapitalGrid = /** @class */ (function (_super) {
            __extends(CapitalGrid, _super);
            function CapitalGrid(container) {
                return _super.call(this, container) || this;
            }
            CapitalGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Capital'; };
            CapitalGrid.prototype.getDialogType = function () { return Nuevo_Roezec.CapitalDialog; };
            CapitalGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.CapitalRow.idProperty; };
            CapitalGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.CapitalRow.insertPermission; };
            CapitalGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.CapitalRow.localTextPrefix; };
            CapitalGrid.prototype.getService = function () { return Nuevo_Roezec.CapitalService.baseUrl; };
            CapitalGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], CapitalGrid);
            return CapitalGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.CapitalGrid = CapitalGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContactosDialog = /** @class */ (function (_super) {
            __extends(ContactosDialog, _super);
            function ContactosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.ContactosForm(_this.idPrefix);
                return _this;
            }
            ContactosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.ContactosForm.formKey; };
            ContactosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.ContactosRow.idProperty; };
            ContactosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ContactosRow.localTextPrefix; };
            ContactosDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.ContactosRow.nameProperty; };
            ContactosDialog.prototype.getService = function () { return Nuevo_Roezec.ContactosService.baseUrl; };
            ContactosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.ContactosRow.deletePermission; };
            ContactosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.ContactosRow.insertPermission; };
            ContactosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.ContactosRow.updatePermission; };
            ContactosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], ContactosDialog);
            return ContactosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.ContactosDialog = ContactosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContactosGrid = /** @class */ (function (_super) {
            __extends(ContactosGrid, _super);
            function ContactosGrid(container) {
                return _super.call(this, container) || this;
            }
            ContactosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Contactos'; };
            ContactosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.ContactosDialog; };
            ContactosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.ContactosRow.idProperty; };
            ContactosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.ContactosRow.insertPermission; };
            ContactosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ContactosRow.localTextPrefix; };
            ContactosGrid.prototype.getService = function () { return Nuevo_Roezec.ContactosService.baseUrl; };
            ContactosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], ContactosGrid);
            return ContactosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.ContactosGrid = ContactosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContinentesDialog = /** @class */ (function (_super) {
            __extends(ContinentesDialog, _super);
            function ContinentesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.ContinentesForm(_this.idPrefix);
                return _this;
            }
            ContinentesDialog.prototype.getFormKey = function () { return Nuevo_Roezec.ContinentesForm.formKey; };
            ContinentesDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.ContinentesRow.idProperty; };
            ContinentesDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ContinentesRow.localTextPrefix; };
            ContinentesDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.ContinentesRow.nameProperty; };
            ContinentesDialog.prototype.getService = function () { return Nuevo_Roezec.ContinentesService.baseUrl; };
            ContinentesDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.ContinentesRow.deletePermission; };
            ContinentesDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.ContinentesRow.insertPermission; };
            ContinentesDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.ContinentesRow.updatePermission; };
            ContinentesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], ContinentesDialog);
            return ContinentesDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.ContinentesDialog = ContinentesDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ContinentesGrid = /** @class */ (function (_super) {
            __extends(ContinentesGrid, _super);
            function ContinentesGrid(container) {
                return _super.call(this, container) || this;
            }
            ContinentesGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Continentes'; };
            ContinentesGrid.prototype.getDialogType = function () { return Nuevo_Roezec.ContinentesDialog; };
            ContinentesGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.ContinentesRow.idProperty; };
            ContinentesGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.ContinentesRow.insertPermission; };
            ContinentesGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ContinentesRow.localTextPrefix; };
            ContinentesGrid.prototype.getService = function () { return Nuevo_Roezec.ContinentesService.baseUrl; };
            ContinentesGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], ContinentesGrid);
            return ContinentesGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.ContinentesGrid = ContinentesGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasDialog = /** @class */ (function (_super) {
            __extends(EmpresasDialog, _super);
            // *****************************************************************************
            function EmpresasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EmpresasForm(_this.idPrefix);
                return _this;
            }
            EmpresasDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasForm.formKey; };
            EmpresasDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasRow.idProperty; };
            EmpresasDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasRow.localTextPrefix; };
            EmpresasDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasRow.nameProperty; };
            EmpresasDialog.prototype.getService = function () { return Nuevo_Roezec.EmpresasService.baseUrl; };
            EmpresasDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EmpresasRow.deletePermission; };
            EmpresasDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasRow.insertPermission; };
            EmpresasDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EmpresasRow.updatePermission; };
            EmpresasDialog = __decorate([
                Serenity.Decorators.registerClass()
                // *****************************************************************************
                // Importante. Al añadir la linea siguiente, el form paramodificar registro
                // se abre en un panel a ventana completa en vez de un pop-up
                // Util para mantenimientos con muchos campos
                // Javier Núñez Julio 2021
                ,
                Serenity.Decorators.panel()
                // *****************************************************************************
            ], EmpresasDialog);
            return EmpresasDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EmpresasDialog = EmpresasDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasGrid = /** @class */ (function (_super) {
            __extends(EmpresasGrid, _super);
            function EmpresasGrid(container) {
                return _super.call(this, container) || this;
            }
            EmpresasGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Empresas'; };
            EmpresasGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasDialog; };
            EmpresasGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasRow.idProperty; };
            EmpresasGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasRow.insertPermission; };
            EmpresasGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasRow.localTextPrefix; };
            EmpresasGrid.prototype.getService = function () { return Nuevo_Roezec.EmpresasService.baseUrl; };
            /* Valor por defecto al añadir un registro */
            /* Basado en Basic Samples/Dialogs/DefaultValuesInNewDialog */
            EmpresasGrid.prototype.addButtonClick = function () {
                this.editItem({
                    FechaCambioEstado: Q.formatDate(new Date(), 'yyyy-MM-dd'),
                    EstadoEmpresaId: Nuevo_Roezec.EstadosEmpresaRow.getLookup().items
                        .filter(function (x) { return x.Estado === 'Presentada'; })[0].EstadoEmpresaId
                });
            };
            //protected getQuickFilters() {
            //    var filters = super.getQuickFilters();
            //    filters.push({
            //        type: Serenity.LookupEditor,
            //        options: {
            //            lookupKey: ProcedimientosRow.lookupKey
            //        },
            //        field: 'ProcedimientoId',
            //        title: 'Tiene Procedimiento en Historial',
            //        handler: w => {
            //            (this.view.params as ProyectosZec.Nuevo_Roezec.HistorialListRequest).ProcedimientoId = Q.toId(w.value);
            //        },
            //    });
            //    return filters;
            //}
            // Botones Excel y Pdf
            EmpresasGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/Empresas/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.push({
                    title: 'Técnico',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Técnico: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: "Tecnico" /* Tecnico */
                        }]); }
                });
                buttons.push({
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: function () { return _this.view.setGrouping([]); }
                });
                return buttons;
                // Fin añadidos
            };
            EmpresasGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], EmpresasGrid);
            return EmpresasGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EmpresasGrid = EmpresasGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosEditDialog = /** @class */ (function (_super) {
            __extends(EmpresasContactosEditDialog, _super);
            function EmpresasContactosEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.EmpresasContactosForm(_this.idPrefix);
                return _this;
            }
            EmpresasContactosEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasContactosForm.formKey; };
            EmpresasContactosEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasContactosRow.nameProperty; };
            EmpresasContactosEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasContactosRow.localTextPrefix; };
            EmpresasContactosEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasContactosEditDialog);
            return EmpresasContactosEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.EmpresasContactosEditDialog = EmpresasContactosEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosDialog = /** @class */ (function (_super) {
            __extends(EmpresasContactosDialog, _super);
            function EmpresasContactosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EmpresasContactosForm(_this.idPrefix);
                return _this;
            }
            EmpresasContactosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasContactosForm.formKey; };
            EmpresasContactosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasContactosRow.idProperty; };
            EmpresasContactosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasContactosRow.localTextPrefix; };
            EmpresasContactosDialog.prototype.getService = function () { return Nuevo_Roezec.EmpresasContactosService.baseUrl; };
            EmpresasContactosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EmpresasContactosRow.deletePermission; };
            EmpresasContactosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasContactosRow.insertPermission; };
            EmpresasContactosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EmpresasContactosRow.updatePermission; };
            EmpresasContactosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasContactosDialog);
            return EmpresasContactosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EmpresasContactosDialog = EmpresasContactosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosEditor = /** @class */ (function (_super) {
            __extends(EmpresasContactosEditor, _super);
            function EmpresasContactosEditor(container) {
                return _super.call(this, container) || this;
            }
            EmpresasContactosEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.EmpresasContactos"; };
            EmpresasContactosEditor.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasContactosEditDialog; };
            EmpresasContactosEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasContactosRow.localTextPrefix; };
            EmpresasContactosEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Contacto";
            };
            EmpresasContactosEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], EmpresasContactosEditor);
            return EmpresasContactosEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.EmpresasContactosEditor = EmpresasContactosEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosGrid = /** @class */ (function (_super) {
            __extends(EmpresasContactosGrid, _super);
            function EmpresasContactosGrid(container) {
                return _super.call(this, container) || this;
            }
            EmpresasContactosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EmpresasContactos'; };
            EmpresasContactosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasContactosDialog; };
            EmpresasContactosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasContactosRow.idProperty; };
            EmpresasContactosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasContactosRow.insertPermission; };
            EmpresasContactosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasContactosRow.localTextPrefix; };
            EmpresasContactosGrid.prototype.getService = function () { return Nuevo_Roezec.EmpresasContactosService.baseUrl; };
            EmpresasContactosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasContactosGrid);
            return EmpresasContactosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EmpresasContactosGrid = EmpresasContactosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../EmpresasContactos/EmpresasContactosDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosReadOnlyDialog = /** @class */ (function (_super) {
            __extends(EmpresasContactosReadOnlyDialog, _super);
            // Fin Añadido
            function EmpresasContactosReadOnlyDialog() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             * This is the method that gets list of tool
             * buttons to be created in a dialog.
             *
             * Here we'll remove save and close button, and
             * apply changes buttons.
             */
            EmpresasContactosReadOnlyDialog.prototype.getToolbarButtons = function () {
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "save-and-close-button"; }), 1);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "apply-changes-button"; }), 1);
                // We could also remove delete button here, but for demonstration 
                // purposes we'll hide it in another method (updateInterface)
                // buttons.splice(Q.indexOf(buttons, x => x.cssClass == "delete-button"), 1);
                return buttons;
            };
            /**
             * This method is a good place to update states of
             * interface elements. It is called after dialog
             * is initialized and an entity is loaded into dialog.
             * This is also called in new item mode.
             */
            EmpresasContactosReadOnlyDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                // finding all editor elements and setting their readonly attribute
                // note that this helper method only works with basic inputs, 
                // some editors require widget based set readonly overload (setReadOnly)
                Serenity.EditorUtils.setReadonly(this.element.find('.editor'), true);
                // remove required asterisk (*)
                this.element.find('sup').hide();
                // here is a way to locate a button by its css class
                // note that this method is not available in 
                // getToolbarButtons() because buttons are not 
                // created there yet!
                // 
                // this.toolbar.findButton('delete-button').hide();
                // entity dialog also has reference variables to
                // its default buttons, lets use them to hide delete button
                this.deleteButton.hide();
                // we could also hide save buttons just like delete button,
                // but they are null now as we removed them in getToolbarButtons()
                // if we didn't we could write like this:
                // 
                // this.applyChangesButton.hide();
                // this.saveAndCloseButton.hide();
                // instead of hiding, we could disable a button
                // 
                // this.deleteButton.toggleClass('disabled', true);
            };
            /**
             * This method is called when dialog title needs to be updated.
             * Base class returns something like 'Edit xyz' for edit mode,
             * and 'New xyz' for new record mode.
             *
             * But our dialog is readonly, so we should change it to 'View xyz'
             */
            EmpresasContactosReadOnlyDialog.prototype.getEntityTitle = function () {
                if (!this.isEditMode()) {
                    // we shouldn't hit here, but anyway for demo...
                    return "Como has conseguido entrar aquí?. Avisa a Informática";
                }
                else {
                    // entitySingular is type of record this dialog edits. something like 'Supplier'.
                    // you could hardcode it, but this is for demonstration
                    var entityType = _super.prototype.getEntitySingular.call(this);
                    // get name field value of record this dialog edits
                    var name_2 = this.getEntityNameFieldValue() || "";
                    // you could use Q.format with a local text, but again demo...
                    return 'Vista ' + entityType + " (" + name_2 + ")";
                }
            };
            /**
             * This method is actually the one that calls getEntityTitle()
             * and updates the dialog title. We could do it here too...
             */
            EmpresasContactosReadOnlyDialog.prototype.updateTitle = function () {
                _super.prototype.updateTitle.call(this);
                // remove super.updateTitle() call above and uncomment 
                // below line if you'd like to use this version
                // 
                // this.dialogTitle = 'View Supplier (' + this.getEntityNameFieldValue() + ')';
            };
            EmpresasContactosReadOnlyDialog = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], EmpresasContactosReadOnlyDialog);
            return EmpresasContactosReadOnlyDialog;
        }(Nuevo_Roezec.EmpresasContactosDialog));
        Nuevo_Roezec.EmpresasContactosReadOnlyDialog = EmpresasContactosReadOnlyDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../EmpresasContactos/EmpresasContactosGrid.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasContactosReadOnlyGrid = /** @class */ (function (_super) {
            __extends(EmpresasContactosReadOnlyGrid, _super);
            function EmpresasContactosReadOnlyGrid(container) {
                return _super.call(this, container) || this;
            }
            EmpresasContactosReadOnlyGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EmpresasContactosReadOnly'; };
            EmpresasContactosReadOnlyGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasContactosReadOnlyDialog; };
            /**
             * Removing add button from grid using its css class
             */
            EmpresasContactosReadOnlyGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/EmpresasContactos/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            /**
             * We override getColumns() to change format functions for some columns.
             * You could also write them as formatter classes, and use them at server side
             */
            EmpresasContactosReadOnlyGrid.prototype.getColumns = function () {
                var columns = _super.prototype.getColumns.call(this);
                Q.first(columns, function (x) { return x.field == "EmpresaRazon" /* EmpresaRazon */; }).format =
                    function (ctx) { return "<a href=\"javascript:;\" class=\"empresa-link\">" + Q.htmlEncode(ctx.value) + "</a>"; };
                return columns;
            };
            EmpresasContactosReadOnlyGrid.prototype.onClick = function (e, row, cell) {
                // let base grid handle clicks for its edit links
                _super.prototype.onClick.call(this, e, row, cell);
                // if base grid already handled, we shouldn"t handle it again
                if (e.isDefaultPrevented()) {
                    return;
                }
                // get reference to current item
                var item = this.itemAt(row);
                // get reference to clicked element
                var target = $(e.target);
                if (target.hasClass("empresa-link")) {
                    e.preventDefault();
                    new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                    //    let message = Q.format(
                    //        "<p>Has pulsado sobre la la empresa {0}.</p>" +
                    //        "<p>Si pulsas sobre Si, abrimos la empresa.</p>" +
                    //        "<p>Si pulsas NO, abrimos la alarma.</p>",
                    //        Q.htmlEncode(item.EmpresaRazon));
                    //    Q.confirm(message, () => {
                    //        new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                    //    },
                    //        {
                    //            htmlEncode: false,
                    //            onNo: () => {
                    //                new Nuevo_Roezec.AlarmasDialog().loadByIdAndOpenDialog(item.AlarmaId);
                    //            }
                    //        });
                }
            };
            EmpresasContactosReadOnlyGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], EmpresasContactosReadOnlyGrid);
            return EmpresasContactosReadOnlyGrid;
        }(Nuevo_Roezec.EmpresasContactosGrid));
        Nuevo_Roezec.EmpresasContactosReadOnlyGrid = EmpresasContactosReadOnlyGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasDireccionesDialog = /** @class */ (function (_super) {
            __extends(EmpresasDireccionesDialog, _super);
            function EmpresasDireccionesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EmpresasDireccionesForm(_this.idPrefix);
                return _this;
            }
            EmpresasDireccionesDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasDireccionesForm.formKey; };
            EmpresasDireccionesDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasDireccionesRow.idProperty; };
            EmpresasDireccionesDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasDireccionesRow.localTextPrefix; };
            EmpresasDireccionesDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasDireccionesRow.nameProperty; };
            EmpresasDireccionesDialog.prototype.getService = function () { return Nuevo_Roezec.EmpresasDireccionesService.baseUrl; };
            EmpresasDireccionesDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EmpresasDireccionesRow.deletePermission; };
            EmpresasDireccionesDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasDireccionesRow.insertPermission; };
            EmpresasDireccionesDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EmpresasDireccionesRow.updatePermission; };
            EmpresasDireccionesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasDireccionesDialog);
            return EmpresasDireccionesDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EmpresasDireccionesDialog = EmpresasDireccionesDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasDireccionesEditDialog = /** @class */ (function (_super) {
            __extends(EmpresasDireccionesEditDialog, _super);
            function EmpresasDireccionesEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.EmpresasDireccionesForm(_this.idPrefix);
                return _this;
            }
            EmpresasDireccionesEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasDireccionesForm.formKey; };
            EmpresasDireccionesEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasDireccionesRow.nameProperty; };
            EmpresasDireccionesEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasDireccionesRow.localTextPrefix; };
            EmpresasDireccionesEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasDireccionesEditDialog);
            return EmpresasDireccionesEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.EmpresasDireccionesEditDialog = EmpresasDireccionesEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasDireccionesEditor = /** @class */ (function (_super) {
            __extends(EmpresasDireccionesEditor, _super);
            function EmpresasDireccionesEditor(container) {
                return _super.call(this, container) || this;
            }
            EmpresasDireccionesEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.EmpresasDirecciones"; };
            EmpresasDireccionesEditor.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasDireccionesEditDialog; };
            EmpresasDireccionesEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasDireccionesRow.localTextPrefix; };
            EmpresasDireccionesEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Dirección";
            };
            EmpresasDireccionesEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], EmpresasDireccionesEditor);
            return EmpresasDireccionesEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.EmpresasDireccionesEditor = EmpresasDireccionesEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasDireccionesGrid = /** @class */ (function (_super) {
            __extends(EmpresasDireccionesGrid, _super);
            function EmpresasDireccionesGrid(container) {
                return _super.call(this, container) || this;
            }
            EmpresasDireccionesGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EmpresasDirecciones'; };
            EmpresasDireccionesGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasDireccionesDialog; };
            EmpresasDireccionesGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasDireccionesRow.idProperty; };
            EmpresasDireccionesGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasDireccionesRow.insertPermission; };
            EmpresasDireccionesGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasDireccionesRow.localTextPrefix; };
            EmpresasDireccionesGrid.prototype.getService = function () { return Nuevo_Roezec.EmpresasDireccionesService.baseUrl; };
            EmpresasDireccionesGrid.prototype.getDefaultSortBy = function () {
                return [Nuevo_Roezec.EmpresasDireccionesRow.Fields.Desde]; // Este es el campo de ordenación por defecto
            };
            EmpresasDireccionesGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasDireccionesGrid);
            return EmpresasDireccionesGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EmpresasDireccionesGrid = EmpresasDireccionesGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasEmpleosDialog = /** @class */ (function (_super) {
            __extends(EmpresasEmpleosDialog, _super);
            function EmpresasEmpleosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EmpresasEmpleosForm(_this.idPrefix);
                return _this;
            }
            EmpresasEmpleosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasEmpleosForm.formKey; };
            EmpresasEmpleosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasEmpleosRow.idProperty; };
            EmpresasEmpleosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasEmpleosRow.localTextPrefix; };
            EmpresasEmpleosDialog.prototype.getService = function () { return Nuevo_Roezec.EmpresasEmpleosService.baseUrl; };
            EmpresasEmpleosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EmpresasEmpleosRow.deletePermission; };
            EmpresasEmpleosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasEmpleosRow.insertPermission; };
            EmpresasEmpleosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EmpresasEmpleosRow.updatePermission; };
            EmpresasEmpleosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasEmpleosDialog);
            return EmpresasEmpleosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EmpresasEmpleosDialog = EmpresasEmpleosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasEmpleosEditDialog = /** @class */ (function (_super) {
            __extends(EmpresasEmpleosEditDialog, _super);
            function EmpresasEmpleosEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.EmpresasEmpleosForm(_this.idPrefix);
                return _this;
            }
            EmpresasEmpleosEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasEmpleosForm.formKey; };
            EmpresasEmpleosEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasEmpleosRow.nameProperty; };
            EmpresasEmpleosEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasEmpleosRow.localTextPrefix; };
            EmpresasEmpleosEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasEmpleosEditDialog);
            return EmpresasEmpleosEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.EmpresasEmpleosEditDialog = EmpresasEmpleosEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasEmpleosEditor = /** @class */ (function (_super) {
            __extends(EmpresasEmpleosEditor, _super);
            function EmpresasEmpleosEditor(container) {
                return _super.call(this, container) || this;
            }
            EmpresasEmpleosEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.EmpresasEmpleos"; };
            EmpresasEmpleosEditor.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasEmpleosEditDialog; };
            EmpresasEmpleosEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasEmpleosRow.localTextPrefix; };
            EmpresasEmpleosEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Empleo";
            };
            EmpresasEmpleosEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], EmpresasEmpleosEditor);
            return EmpresasEmpleosEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.EmpresasEmpleosEditor = EmpresasEmpleosEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasEmpleosGrid = /** @class */ (function (_super) {
            __extends(EmpresasEmpleosGrid, _super);
            function EmpresasEmpleosGrid(container) {
                return _super.call(this, container) || this;
            }
            EmpresasEmpleosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EmpresasEmpleos'; };
            EmpresasEmpleosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasEmpleosDialog; };
            EmpresasEmpleosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasEmpleosRow.idProperty; };
            EmpresasEmpleosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasEmpleosRow.insertPermission; };
            EmpresasEmpleosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasEmpleosRow.localTextPrefix; };
            EmpresasEmpleosGrid.prototype.getService = function () { return Nuevo_Roezec.EmpresasEmpleosService.baseUrl; };
            EmpresasEmpleosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasEmpleosGrid);
            return EmpresasEmpleosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EmpresasEmpleosGrid = EmpresasEmpleosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasFicherosDialog = /** @class */ (function (_super) {
            __extends(EmpresasFicherosDialog, _super);
            function EmpresasFicherosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EmpresasFicherosForm(_this.idPrefix);
                return _this;
            }
            EmpresasFicherosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasFicherosForm.formKey; };
            EmpresasFicherosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasFicherosRow.idProperty; };
            EmpresasFicherosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasFicherosRow.localTextPrefix; };
            EmpresasFicherosDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasFicherosRow.nameProperty; };
            EmpresasFicherosDialog.prototype.getService = function () { return Nuevo_Roezec.EmpresasFicherosService.baseUrl; };
            EmpresasFicherosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EmpresasFicherosRow.deletePermission; };
            EmpresasFicherosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasFicherosRow.insertPermission; };
            EmpresasFicherosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EmpresasFicherosRow.updatePermission; };
            EmpresasFicherosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasFicherosDialog);
            return EmpresasFicherosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EmpresasFicherosDialog = EmpresasFicherosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasFicherosEditDialog = /** @class */ (function (_super) {
            __extends(EmpresasFicherosEditDialog, _super);
            function EmpresasFicherosEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.EmpresasFicherosForm(_this.idPrefix);
                return _this;
            }
            EmpresasFicherosEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasFicherosForm.formKey; };
            EmpresasFicherosEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasFicherosRow.nameProperty; };
            EmpresasFicherosEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasFicherosRow.localTextPrefix; };
            EmpresasFicherosEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasFicherosEditDialog);
            return EmpresasFicherosEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.EmpresasFicherosEditDialog = EmpresasFicherosEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasFicherosEditor = /** @class */ (function (_super) {
            __extends(EmpresasFicherosEditor, _super);
            function EmpresasFicherosEditor(container) {
                return _super.call(this, container) || this;
            }
            EmpresasFicherosEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.EmpresasFicheros"; };
            EmpresasFicherosEditor.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasFicherosEditDialog; };
            EmpresasFicherosEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasFicherosRow.localTextPrefix; };
            EmpresasFicherosEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Fichero";
            };
            EmpresasFicherosEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], EmpresasFicherosEditor);
            return EmpresasFicherosEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.EmpresasFicherosEditor = EmpresasFicherosEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasFicherosGrid = /** @class */ (function (_super) {
            __extends(EmpresasFicherosGrid, _super);
            function EmpresasFicherosGrid(container) {
                return _super.call(this, container) || this;
            }
            EmpresasFicherosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EmpresasFicheros'; };
            EmpresasFicherosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasFicherosDialog; };
            EmpresasFicherosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasFicherosRow.idProperty; };
            EmpresasFicherosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasFicherosRow.insertPermission; };
            EmpresasFicherosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasFicherosRow.localTextPrefix; };
            EmpresasFicherosGrid.prototype.getService = function () { return Nuevo_Roezec.EmpresasFicherosService.baseUrl; };
            EmpresasFicherosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasFicherosGrid);
            return EmpresasFicherosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EmpresasFicherosGrid = EmpresasFicherosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasMercadosDialog = /** @class */ (function (_super) {
            __extends(EmpresasMercadosDialog, _super);
            function EmpresasMercadosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EmpresasMercadosForm(_this.idPrefix);
                return _this;
            }
            EmpresasMercadosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasMercadosForm.formKey; };
            EmpresasMercadosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasMercadosRow.idProperty; };
            EmpresasMercadosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasMercadosRow.localTextPrefix; };
            EmpresasMercadosDialog.prototype.getService = function () { return Nuevo_Roezec.EmpresasMercadosService.baseUrl; };
            EmpresasMercadosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EmpresasMercadosRow.deletePermission; };
            EmpresasMercadosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasMercadosRow.insertPermission; };
            EmpresasMercadosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EmpresasMercadosRow.updatePermission; };
            EmpresasMercadosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasMercadosDialog);
            return EmpresasMercadosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EmpresasMercadosDialog = EmpresasMercadosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasMercadosEditDialog = /** @class */ (function (_super) {
            __extends(EmpresasMercadosEditDialog, _super);
            function EmpresasMercadosEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.EmpresasMercadosForm(_this.idPrefix);
                return _this;
            }
            EmpresasMercadosEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasMercadosForm.formKey; };
            EmpresasMercadosEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasMercadosRow.nameProperty; };
            EmpresasMercadosEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasMercadosRow.localTextPrefix; };
            EmpresasMercadosEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasMercadosEditDialog);
            return EmpresasMercadosEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.EmpresasMercadosEditDialog = EmpresasMercadosEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasMercadosEditor = /** @class */ (function (_super) {
            __extends(EmpresasMercadosEditor, _super);
            function EmpresasMercadosEditor(container) {
                return _super.call(this, container) || this;
            }
            EmpresasMercadosEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.EmpresasMercados"; };
            EmpresasMercadosEditor.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasMercadosEditDialog; };
            EmpresasMercadosEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasMercadosRow.localTextPrefix; };
            EmpresasMercadosEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Mercado";
            };
            EmpresasMercadosEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], EmpresasMercadosEditor);
            return EmpresasMercadosEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.EmpresasMercadosEditor = EmpresasMercadosEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasMercadosGrid = /** @class */ (function (_super) {
            __extends(EmpresasMercadosGrid, _super);
            function EmpresasMercadosGrid(container) {
                return _super.call(this, container) || this;
            }
            EmpresasMercadosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EmpresasMercados'; };
            EmpresasMercadosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasMercadosDialog; };
            EmpresasMercadosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasMercadosRow.idProperty; };
            EmpresasMercadosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasMercadosRow.insertPermission; };
            EmpresasMercadosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasMercadosRow.localTextPrefix; };
            EmpresasMercadosGrid.prototype.getService = function () { return Nuevo_Roezec.EmpresasMercadosService.baseUrl; };
            EmpresasMercadosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasMercadosGrid);
            return EmpresasMercadosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EmpresasMercadosGrid = EmpresasMercadosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNaceDialog = /** @class */ (function (_super) {
            __extends(EmpresasNaceDialog, _super);
            function EmpresasNaceDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EmpresasNaceForm(_this.idPrefix);
                return _this;
            }
            EmpresasNaceDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasNaceForm.formKey; };
            EmpresasNaceDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasNaceRow.idProperty; };
            EmpresasNaceDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasNaceRow.localTextPrefix; };
            EmpresasNaceDialog.prototype.getService = function () { return Nuevo_Roezec.EmpresasNaceService.baseUrl; };
            EmpresasNaceDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EmpresasNaceRow.deletePermission; };
            EmpresasNaceDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasNaceRow.insertPermission; };
            EmpresasNaceDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EmpresasNaceRow.updatePermission; };
            EmpresasNaceDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasNaceDialog);
            return EmpresasNaceDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EmpresasNaceDialog = EmpresasNaceDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNaceEditDialog = /** @class */ (function (_super) {
            __extends(EmpresasNaceEditDialog, _super);
            function EmpresasNaceEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.EmpresasNaceForm(_this.idPrefix);
                return _this;
            }
            EmpresasNaceEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasNaceForm.formKey; };
            EmpresasNaceEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasNaceRow.nameProperty; };
            EmpresasNaceEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasNaceRow.localTextPrefix; };
            EmpresasNaceEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasNaceEditDialog);
            return EmpresasNaceEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.EmpresasNaceEditDialog = EmpresasNaceEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNaceEditor = /** @class */ (function (_super) {
            __extends(EmpresasNaceEditor, _super);
            function EmpresasNaceEditor(container) {
                return _super.call(this, container) || this;
            }
            EmpresasNaceEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.EmpresasNace"; };
            EmpresasNaceEditor.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasNaceEditDialog; };
            EmpresasNaceEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasNaceRow.localTextPrefix; };
            EmpresasNaceEditor.prototype.validateEntity = function (row, id) {
                row.EmpresaNaceId = Q.toId(row.EmpresaNaceId);
                if (row.NacePrincipal) {
                    var sameProduct = Q.tryFirst(this.view.getItems(), function (x) { return x.NacePrincipal === row.NacePrincipal; });
                    if (sameProduct && this.id(sameProduct) !== id) {
                        Q.alert('Solo Puede haber un Nace Principal!');
                        return false;
                    }
                }
                return true;
            };
            EmpresasNaceEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Nace";
            };
            EmpresasNaceEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], EmpresasNaceEditor);
            return EmpresasNaceEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.EmpresasNaceEditor = EmpresasNaceEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNaceGrid = /** @class */ (function (_super) {
            __extends(EmpresasNaceGrid, _super);
            function EmpresasNaceGrid(container) {
                return _super.call(this, container) || this;
                //this.slickGrid.onSort.subscribe(function (e, args) {
                //    sortGridFunction((args.grid as Slick.Grid), args.sortCols[0], args.sortCols[0].sortCol.field);
                //    //(args.grid as Slick.Grid).init();
                //    (args.grid as Slick.Grid).invalidateAllRows();
                //    (args.grid as Slick.Grid).invalidate();
                //    (args.grid as Slick.Grid).render();
                //    (args.grid as Slick.Grid).resizeCanvas();
                //});
            }
            EmpresasNaceGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EmpresasNace'; };
            EmpresasNaceGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasNaceDialog; };
            EmpresasNaceGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasNaceRow.idProperty; };
            EmpresasNaceGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasNaceRow.insertPermission; };
            EmpresasNaceGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasNaceRow.localTextPrefix; };
            EmpresasNaceGrid.prototype.getService = function () { return Nuevo_Roezec.EmpresasNaceService.baseUrl; };
            EmpresasNaceGrid = __decorate([
                Serenity.Decorators.filterable(),
                Serenity.Decorators.registerClass()
            ], EmpresasNaceGrid);
            return EmpresasNaceGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EmpresasNaceGrid = EmpresasNaceGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
//function sortGridFunction(grid: Slick.Grid, column: any, field: any) {
//    grid.getData().sort(function (a, b) {
//        var result = a[field] > b[field] ? 1 :
//            a[field] < b[field] ? -1 :
//                0;
//        return column.sortAsc ? result : -result;
//    });
//}
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNombresDialog = /** @class */ (function (_super) {
            __extends(EmpresasNombresDialog, _super);
            function EmpresasNombresDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EmpresasNombresForm(_this.idPrefix);
                return _this;
            }
            EmpresasNombresDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasNombresForm.formKey; };
            EmpresasNombresDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasNombresRow.idProperty; };
            EmpresasNombresDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasNombresRow.localTextPrefix; };
            EmpresasNombresDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasNombresRow.nameProperty; };
            EmpresasNombresDialog.prototype.getService = function () { return Nuevo_Roezec.EmpresasNombresService.baseUrl; };
            EmpresasNombresDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EmpresasNombresRow.deletePermission; };
            EmpresasNombresDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasNombresRow.insertPermission; };
            EmpresasNombresDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EmpresasNombresRow.updatePermission; };
            EmpresasNombresDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasNombresDialog);
            return EmpresasNombresDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EmpresasNombresDialog = EmpresasNombresDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNombresEditDialog = /** @class */ (function (_super) {
            __extends(EmpresasNombresEditDialog, _super);
            function EmpresasNombresEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.EmpresasNombresForm(_this.idPrefix);
                return _this;
            }
            EmpresasNombresEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EmpresasNombresForm.formKey; };
            EmpresasNombresEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EmpresasNombresRow.nameProperty; };
            EmpresasNombresEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasNombresRow.localTextPrefix; };
            EmpresasNombresEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasNombresEditDialog);
            return EmpresasNombresEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.EmpresasNombresEditDialog = EmpresasNombresEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNombresEditor = /** @class */ (function (_super) {
            __extends(EmpresasNombresEditor, _super);
            function EmpresasNombresEditor(container) {
                return _super.call(this, container) || this;
            }
            EmpresasNombresEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.EmpresasNombres"; };
            EmpresasNombresEditor.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasNombresEditDialog; };
            EmpresasNombresEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasNombresRow.localTextPrefix; };
            EmpresasNombresEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Nombre";
            };
            EmpresasNombresEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], EmpresasNombresEditor);
            return EmpresasNombresEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.EmpresasNombresEditor = EmpresasNombresEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EmpresasNombresGrid = /** @class */ (function (_super) {
            __extends(EmpresasNombresGrid, _super);
            function EmpresasNombresGrid(container) {
                return _super.call(this, container) || this;
            }
            EmpresasNombresGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EmpresasNombres'; };
            EmpresasNombresGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EmpresasNombresDialog; };
            EmpresasNombresGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EmpresasNombresRow.idProperty; };
            EmpresasNombresGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EmpresasNombresRow.insertPermission; };
            EmpresasNombresGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EmpresasNombresRow.localTextPrefix; };
            EmpresasNombresGrid.prototype.getService = function () { return Nuevo_Roezec.EmpresasNombresService.baseUrl; };
            EmpresasNombresGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpresasNombresGrid);
            return EmpresasNombresGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EmpresasNombresGrid = EmpresasNombresGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosDialog = /** @class */ (function (_super) {
            __extends(EnviosDialog, _super);
            function EnviosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EnviosForm(_this.idPrefix);
                return _this;
            }
            EnviosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EnviosForm.formKey; };
            EnviosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EnviosRow.idProperty; };
            EnviosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EnviosRow.localTextPrefix; };
            EnviosDialog.prototype.getService = function () { return Nuevo_Roezec.EnviosService.baseUrl; };
            EnviosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EnviosRow.deletePermission; };
            EnviosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EnviosRow.insertPermission; };
            EnviosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EnviosRow.updatePermission; };
            EnviosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EnviosDialog);
            return EnviosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EnviosDialog = EnviosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosEditDialog = /** @class */ (function (_super) {
            __extends(EnviosEditDialog, _super);
            function EnviosEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.EnviosForm(_this.idPrefix);
                return _this;
            }
            EnviosEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EnviosForm.formKey; };
            EnviosEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EnviosRow.nameProperty; };
            EnviosEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EnviosRow.localTextPrefix; };
            EnviosEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EnviosEditDialog);
            return EnviosEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.EnviosEditDialog = EnviosEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosEditor = /** @class */ (function (_super) {
            __extends(EnviosEditor, _super);
            function EnviosEditor(container) {
                return _super.call(this, container) || this;
            }
            /*protected getPersistanceStorage(): Serenity.SettingStorage { return new Common.UserPreferenceStorage(); }*/
            EnviosEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.Envios"; };
            EnviosEditor.prototype.getDialogType = function () { return Nuevo_Roezec.EnviosEditDialog; };
            EnviosEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EnviosRow.localTextPrefix; };
            EnviosEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Envío";
            };
            EnviosEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], EnviosEditor);
            return EnviosEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.EnviosEditor = EnviosEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosGrid = /** @class */ (function (_super) {
            __extends(EnviosGrid, _super);
            function EnviosGrid(container) {
                return _super.call(this, container) || this;
            }
            EnviosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Envios'; };
            EnviosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EnviosDialog; };
            EnviosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EnviosRow.idProperty; };
            EnviosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EnviosRow.insertPermission; };
            EnviosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EnviosRow.localTextPrefix; };
            EnviosGrid.prototype.getService = function () { return Nuevo_Roezec.EnviosService.baseUrl; };
            EnviosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EnviosGrid);
            return EnviosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EnviosGrid = EnviosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosProcedimientoDialog = /** @class */ (function (_super) {
            __extends(EnviosProcedimientoDialog, _super);
            function EnviosProcedimientoDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EnviosProcedimientoForm(_this.idPrefix);
                return _this;
            }
            EnviosProcedimientoDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EnviosProcedimientoForm.formKey; };
            EnviosProcedimientoDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EnviosProcedimientoRow.idProperty; };
            EnviosProcedimientoDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EnviosProcedimientoRow.localTextPrefix; };
            EnviosProcedimientoDialog.prototype.getService = function () { return Nuevo_Roezec.EnviosProcedimientoService.baseUrl; };
            EnviosProcedimientoDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EnviosProcedimientoRow.deletePermission; };
            EnviosProcedimientoDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EnviosProcedimientoRow.insertPermission; };
            EnviosProcedimientoDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EnviosProcedimientoRow.updatePermission; };
            EnviosProcedimientoDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EnviosProcedimientoDialog);
            return EnviosProcedimientoDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EnviosProcedimientoDialog = EnviosProcedimientoDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EnviosProcedimientoGrid = /** @class */ (function (_super) {
            __extends(EnviosProcedimientoGrid, _super);
            function EnviosProcedimientoGrid(container) {
                return _super.call(this, container) || this;
            }
            EnviosProcedimientoGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EnviosProcedimiento'; };
            EnviosProcedimientoGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EnviosProcedimientoDialog; };
            EnviosProcedimientoGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EnviosProcedimientoRow.idProperty; };
            EnviosProcedimientoGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EnviosProcedimientoRow.insertPermission; };
            EnviosProcedimientoGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EnviosProcedimientoRow.localTextPrefix; };
            EnviosProcedimientoGrid.prototype.getService = function () { return Nuevo_Roezec.EnviosProcedimientoService.baseUrl; };
            EnviosProcedimientoGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EnviosProcedimientoGrid);
            return EnviosProcedimientoGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EnviosProcedimientoGrid = EnviosProcedimientoGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEmpresaDialog = /** @class */ (function (_super) {
            __extends(EstadosEmpresaDialog, _super);
            function EstadosEmpresaDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EstadosEmpresaForm(_this.idPrefix);
                return _this;
            }
            EstadosEmpresaDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EstadosEmpresaForm.formKey; };
            EstadosEmpresaDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EstadosEmpresaRow.idProperty; };
            EstadosEmpresaDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EstadosEmpresaRow.localTextPrefix; };
            EstadosEmpresaDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EstadosEmpresaRow.nameProperty; };
            EstadosEmpresaDialog.prototype.getService = function () { return Nuevo_Roezec.EstadosEmpresaService.baseUrl; };
            EstadosEmpresaDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EstadosEmpresaRow.deletePermission; };
            EstadosEmpresaDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EstadosEmpresaRow.insertPermission; };
            EstadosEmpresaDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EstadosEmpresaRow.updatePermission; };
            EstadosEmpresaDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EstadosEmpresaDialog);
            return EstadosEmpresaDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EstadosEmpresaDialog = EstadosEmpresaDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEmpresaGrid = /** @class */ (function (_super) {
            __extends(EstadosEmpresaGrid, _super);
            function EstadosEmpresaGrid(container) {
                return _super.call(this, container) || this;
            }
            EstadosEmpresaGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EstadosEmpresa'; };
            EstadosEmpresaGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EstadosEmpresaDialog; };
            EstadosEmpresaGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EstadosEmpresaRow.idProperty; };
            EstadosEmpresaGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EstadosEmpresaRow.insertPermission; };
            EstadosEmpresaGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EstadosEmpresaRow.localTextPrefix; };
            EstadosEmpresaGrid.prototype.getService = function () { return Nuevo_Roezec.EstadosEmpresaService.baseUrl; };
            EstadosEmpresaGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EstadosEmpresaGrid);
            return EstadosEmpresaGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EstadosEmpresaGrid = EstadosEmpresaGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEnvioDialog = /** @class */ (function (_super) {
            __extends(EstadosEnvioDialog, _super);
            function EstadosEnvioDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.EstadosEnvioForm(_this.idPrefix);
                return _this;
            }
            EstadosEnvioDialog.prototype.getFormKey = function () { return Nuevo_Roezec.EstadosEnvioForm.formKey; };
            EstadosEnvioDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.EstadosEnvioRow.idProperty; };
            EstadosEnvioDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EstadosEnvioRow.localTextPrefix; };
            EstadosEnvioDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.EstadosEnvioRow.nameProperty; };
            EstadosEnvioDialog.prototype.getService = function () { return Nuevo_Roezec.EstadosEnvioService.baseUrl; };
            EstadosEnvioDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.EstadosEnvioRow.deletePermission; };
            EstadosEnvioDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.EstadosEnvioRow.insertPermission; };
            EstadosEnvioDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.EstadosEnvioRow.updatePermission; };
            EstadosEnvioDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EstadosEnvioDialog);
            return EstadosEnvioDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.EstadosEnvioDialog = EstadosEnvioDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var EstadosEnvioGrid = /** @class */ (function (_super) {
            __extends(EstadosEnvioGrid, _super);
            function EstadosEnvioGrid(container) {
                return _super.call(this, container) || this;
            }
            EstadosEnvioGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.EstadosEnvio'; };
            EstadosEnvioGrid.prototype.getDialogType = function () { return Nuevo_Roezec.EstadosEnvioDialog; };
            EstadosEnvioGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.EstadosEnvioRow.idProperty; };
            EstadosEnvioGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.EstadosEnvioRow.insertPermission; };
            EstadosEnvioGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.EstadosEnvioRow.localTextPrefix; };
            EstadosEnvioGrid.prototype.getService = function () { return Nuevo_Roezec.EstadosEnvioService.baseUrl; };
            EstadosEnvioGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EstadosEnvioGrid);
            return EstadosEnvioGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.EstadosEnvioGrid = EstadosEnvioGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FicherosDialog = /** @class */ (function (_super) {
            __extends(FicherosDialog, _super);
            function FicherosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.FicherosForm(_this.idPrefix);
                return _this;
            }
            FicherosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.FicherosForm.formKey; };
            FicherosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.FicherosRow.idProperty; };
            FicherosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.FicherosRow.localTextPrefix; };
            FicherosDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.FicherosRow.nameProperty; };
            FicherosDialog.prototype.getService = function () { return Nuevo_Roezec.FicherosService.baseUrl; };
            FicherosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.FicherosRow.deletePermission; };
            FicherosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.FicherosRow.insertPermission; };
            FicherosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.FicherosRow.updatePermission; };
            FicherosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], FicherosDialog);
            return FicherosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.FicherosDialog = FicherosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FicherosEditDialog = /** @class */ (function (_super) {
            __extends(FicherosEditDialog, _super);
            function FicherosEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.FicherosForm(_this.idPrefix);
                return _this;
            }
            FicherosEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.FicherosForm.formKey; };
            FicherosEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.FicherosRow.nameProperty; };
            FicherosEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.FicherosRow.localTextPrefix; };
            FicherosEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], FicherosEditDialog);
            return FicherosEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.FicherosEditDialog = FicherosEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FicherosEditor = /** @class */ (function (_super) {
            __extends(FicherosEditor, _super);
            function FicherosEditor(container) {
                return _super.call(this, container) || this;
            }
            /*protected getPersistanceStorage(): Serenity.SettingStorage { return new Common.UserPreferenceStorage(); }*/
            FicherosEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.Ficheros"; };
            FicherosEditor.prototype.getDialogType = function () { return Nuevo_Roezec.FicherosEditDialog; };
            FicherosEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.FicherosRow.localTextPrefix; };
            FicherosEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Fichero";
            };
            FicherosEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], FicherosEditor);
            return FicherosEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.FicherosEditor = FicherosEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FicherosGrid = /** @class */ (function (_super) {
            __extends(FicherosGrid, _super);
            function FicherosGrid(container) {
                return _super.call(this, container) || this;
            }
            FicherosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Ficheros'; };
            FicherosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.FicherosDialog; };
            FicherosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.FicherosRow.idProperty; };
            FicherosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.FicherosRow.insertPermission; };
            FicherosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.FicherosRow.localTextPrefix; };
            FicherosGrid.prototype.getService = function () { return Nuevo_Roezec.FicherosService.baseUrl; };
            /* Valor por defecto al añadir un registro */
            /* Basado en Basic Samples/Dialogs/DefaultValuesInNewDialog */
            FicherosGrid.prototype.addButtonClick = function () {
                this.editItem({
                    Organo: 'EA0023288'
                });
            };
            FicherosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], FicherosGrid);
            return FicherosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.FicherosGrid = FicherosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../Ficheros/FicherosDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FicherosReadOnlyDialog = /** @class */ (function (_super) {
            __extends(FicherosReadOnlyDialog, _super);
            function FicherosReadOnlyDialog() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             * This is the method that gets list of tool
             * buttons to be created in a dialog.
             *
             * Here we'll remove save and close button, and
             * apply changes buttons.
             */
            FicherosReadOnlyDialog.prototype.getToolbarButtons = function () {
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "save-and-close-button"; }), 1);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "apply-changes-button"; }), 1);
                // We could also remove delete button here, but for demonstration 
                // purposes we'll hide it in another method (updateInterface)
                // buttons.splice(Q.indexOf(buttons, x => x.cssClass == "delete-button"), 1);
                return buttons;
            };
            /**
             * This method is a good place to update states of
             * interface elements. It is called after dialog
             * is initialized and an entity is loaded into dialog.
             * This is also called in new item mode.
             */
            FicherosReadOnlyDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                // finding all editor elements and setting their readonly attribute
                // note that this helper method only works with basic inputs, 
                // some editors require widget based set readonly overload (setReadOnly)
                Serenity.EditorUtils.setReadonly(this.element.find('.editor'), true);
                // remove required asterisk (*)
                this.element.find('sup').hide();
                // here is a way to locate a button by its css class
                // note that this method is not available in 
                // getToolbarButtons() because buttons are not 
                // created there yet!
                // 
                // this.toolbar.findButton('delete-button').hide();
                // entity dialog also has reference variables to
                // its default buttons, lets use them to hide delete button
                this.deleteButton.hide();
                // we could also hide save buttons just like delete button,
                // but they are null now as we removed them in getToolbarButtons()
                // if we didn't we could write like this:
                // 
                // this.applyChangesButton.hide();
                // this.saveAndCloseButton.hide();
                // instead of hiding, we could disable a button
                // 
                // this.deleteButton.toggleClass('disabled', true);
            };
            /**
             * This method is called when dialog title needs to be updated.
             * Base class returns something like 'Edit xyz' for edit mode,
             * and 'New xyz' for new record mode.
             *
             * But our dialog is readonly, so we should change it to 'View xyz'
             */
            FicherosReadOnlyDialog.prototype.getEntityTitle = function () {
                if (!this.isEditMode()) {
                    // we shouldn't hit here, but anyway for demo...
                    return "Como has conseguido entrar aquí?. Avisa a Informática";
                }
                else {
                    // entitySingular is type of record this dialog edits. something like 'Supplier'.
                    // you could hardcode it, but this is for demonstration
                    var entityType = _super.prototype.getEntitySingular.call(this);
                    // get name field value of record this dialog edits
                    var name_3 = this.getEntityNameFieldValue() || "";
                    // you could use Q.format with a local text, but again demo...
                    return 'Vista ' + entityType + " (" + name_3 + ")";
                }
            };
            /**
             * This method is actually the one that calls getEntityTitle()
             * and updates the dialog title. We could do it here too...
             */
            FicherosReadOnlyDialog.prototype.updateTitle = function () {
                _super.prototype.updateTitle.call(this);
                // remove super.updateTitle() call above and uncomment 
                // below line if you'd like to use this version
                // 
                // this.dialogTitle = 'View Supplier (' + this.getEntityNameFieldValue() + ')';
            };
            FicherosReadOnlyDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], FicherosReadOnlyDialog);
            return FicherosReadOnlyDialog;
        }(Nuevo_Roezec.FicherosDialog));
        Nuevo_Roezec.FicherosReadOnlyDialog = FicherosReadOnlyDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../Ficheros/FicherosGrid.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FicherosReadOnlyGrid = /** @class */ (function (_super) {
            __extends(FicherosReadOnlyGrid, _super);
            function FicherosReadOnlyGrid(container) {
                return _super.call(this, container) || this;
            }
            FicherosReadOnlyGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.FicherosReadOnly'; };
            FicherosReadOnlyGrid.prototype.getDialogType = function () { return Nuevo_Roezec.FicherosReadOnlyDialog; };
            /**
             * Removing add button from grid using its css class
             */
            FicherosReadOnlyGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/Ficheros/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            FicherosReadOnlyGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], FicherosReadOnlyGrid);
            return FicherosReadOnlyGrid;
        }(Nuevo_Roezec.FicherosGrid));
        Nuevo_Roezec.FicherosReadOnlyGrid = FicherosReadOnlyGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasEnvioDialog = /** @class */ (function (_super) {
            __extends(FormasEnvioDialog, _super);
            function FormasEnvioDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.FormasEnvioForm(_this.idPrefix);
                return _this;
            }
            FormasEnvioDialog.prototype.getFormKey = function () { return Nuevo_Roezec.FormasEnvioForm.formKey; };
            FormasEnvioDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.FormasEnvioRow.idProperty; };
            FormasEnvioDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.FormasEnvioRow.localTextPrefix; };
            FormasEnvioDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.FormasEnvioRow.nameProperty; };
            FormasEnvioDialog.prototype.getService = function () { return Nuevo_Roezec.FormasEnvioService.baseUrl; };
            FormasEnvioDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.FormasEnvioRow.deletePermission; };
            FormasEnvioDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.FormasEnvioRow.insertPermission; };
            FormasEnvioDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.FormasEnvioRow.updatePermission; };
            FormasEnvioDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], FormasEnvioDialog);
            return FormasEnvioDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.FormasEnvioDialog = FormasEnvioDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasEnvioGrid = /** @class */ (function (_super) {
            __extends(FormasEnvioGrid, _super);
            function FormasEnvioGrid(container) {
                return _super.call(this, container) || this;
            }
            FormasEnvioGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.FormasEnvio'; };
            FormasEnvioGrid.prototype.getDialogType = function () { return Nuevo_Roezec.FormasEnvioDialog; };
            FormasEnvioGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.FormasEnvioRow.idProperty; };
            FormasEnvioGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.FormasEnvioRow.insertPermission; };
            FormasEnvioGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.FormasEnvioRow.localTextPrefix; };
            FormasEnvioGrid.prototype.getService = function () { return Nuevo_Roezec.FormasEnvioService.baseUrl; };
            FormasEnvioGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], FormasEnvioGrid);
            return FormasEnvioGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.FormasEnvioGrid = FormasEnvioGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasJuridicasDialog = /** @class */ (function (_super) {
            __extends(FormasJuridicasDialog, _super);
            function FormasJuridicasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.FormasJuridicasForm(_this.idPrefix);
                return _this;
            }
            FormasJuridicasDialog.prototype.getFormKey = function () { return Nuevo_Roezec.FormasJuridicasForm.formKey; };
            FormasJuridicasDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.FormasJuridicasRow.idProperty; };
            FormasJuridicasDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.FormasJuridicasRow.localTextPrefix; };
            FormasJuridicasDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.FormasJuridicasRow.nameProperty; };
            FormasJuridicasDialog.prototype.getService = function () { return Nuevo_Roezec.FormasJuridicasService.baseUrl; };
            FormasJuridicasDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.FormasJuridicasRow.deletePermission; };
            FormasJuridicasDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.FormasJuridicasRow.insertPermission; };
            FormasJuridicasDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.FormasJuridicasRow.updatePermission; };
            FormasJuridicasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], FormasJuridicasDialog);
            return FormasJuridicasDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.FormasJuridicasDialog = FormasJuridicasDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var FormasJuridicasGrid = /** @class */ (function (_super) {
            __extends(FormasJuridicasGrid, _super);
            function FormasJuridicasGrid(container) {
                return _super.call(this, container) || this;
            }
            FormasJuridicasGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.FormasJuridicas'; };
            FormasJuridicasGrid.prototype.getDialogType = function () { return Nuevo_Roezec.FormasJuridicasDialog; };
            FormasJuridicasGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.FormasJuridicasRow.idProperty; };
            FormasJuridicasGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.FormasJuridicasRow.insertPermission; };
            FormasJuridicasGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.FormasJuridicasRow.localTextPrefix; };
            FormasJuridicasGrid.prototype.getService = function () { return Nuevo_Roezec.FormasJuridicasService.baseUrl; };
            FormasJuridicasGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], FormasJuridicasGrid);
            return FormasJuridicasGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.FormasJuridicasGrid = FormasJuridicasGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var HistorialEmpresasDialog = /** @class */ (function (_super) {
            __extends(HistorialEmpresasDialog, _super);
            function HistorialEmpresasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.HistorialEmpresasForm(_this.idPrefix);
                return _this;
            }
            HistorialEmpresasDialog.prototype.getFormKey = function () { return Nuevo_Roezec.HistorialEmpresasForm.formKey; };
            HistorialEmpresasDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.HistorialEmpresasRow.idProperty; };
            HistorialEmpresasDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.HistorialEmpresasRow.localTextPrefix; };
            HistorialEmpresasDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.HistorialEmpresasRow.nameProperty; };
            HistorialEmpresasDialog.prototype.getService = function () { return Nuevo_Roezec.HistorialEmpresasService.baseUrl; };
            HistorialEmpresasDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.HistorialEmpresasRow.deletePermission; };
            HistorialEmpresasDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.HistorialEmpresasRow.insertPermission; };
            HistorialEmpresasDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.HistorialEmpresasRow.updatePermission; };
            HistorialEmpresasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], HistorialEmpresasDialog);
            return HistorialEmpresasDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.HistorialEmpresasDialog = HistorialEmpresasDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var HistorialEmpresasEditDialog = /** @class */ (function (_super) {
            __extends(HistorialEmpresasEditDialog, _super);
            function HistorialEmpresasEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.HistorialEmpresasForm(_this.idPrefix);
                return _this;
            }
            HistorialEmpresasEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.HistorialEmpresasForm.formKey; };
            HistorialEmpresasEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.HistorialEmpresasRow.nameProperty; };
            HistorialEmpresasEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.HistorialEmpresasRow.localTextPrefix; };
            HistorialEmpresasEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], HistorialEmpresasEditDialog);
            return HistorialEmpresasEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.HistorialEmpresasEditDialog = HistorialEmpresasEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var HistorialEmpresasEditor = /** @class */ (function (_super) {
            __extends(HistorialEmpresasEditor, _super);
            function HistorialEmpresasEditor(container) {
                return _super.call(this, container) || this;
            }
            HistorialEmpresasEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.HistorialEmpresas"; };
            HistorialEmpresasEditor.prototype.getDialogType = function () { return Nuevo_Roezec.HistorialEmpresasEditDialog; };
            HistorialEmpresasEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.HistorialEmpresasRow.localTextPrefix; };
            HistorialEmpresasEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Procedimiento";
            };
            HistorialEmpresasEditor = __decorate([
                Serenity.Decorators.registerClass()
            ], HistorialEmpresasEditor);
            return HistorialEmpresasEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.HistorialEmpresasEditor = HistorialEmpresasEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var HistorialEmpresasGrid = /** @class */ (function (_super) {
            __extends(HistorialEmpresasGrid, _super);
            function HistorialEmpresasGrid(container) {
                return _super.call(this, container) || this;
            }
            HistorialEmpresasGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.HistorialEmpresas'; };
            HistorialEmpresasGrid.prototype.getDialogType = function () { return Nuevo_Roezec.HistorialEmpresasDialog; };
            HistorialEmpresasGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.HistorialEmpresasRow.idProperty; };
            HistorialEmpresasGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.HistorialEmpresasRow.insertPermission; };
            HistorialEmpresasGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.HistorialEmpresasRow.localTextPrefix; };
            HistorialEmpresasGrid.prototype.getService = function () { return Nuevo_Roezec.HistorialEmpresasService.baseUrl; };
            HistorialEmpresasGrid = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.registerClass()
            ], HistorialEmpresasGrid);
            return HistorialEmpresasGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.HistorialEmpresasGrid = HistorialEmpresasGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../HistorialEmpresas/HistorialEmpresasDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var HistorialReadOnlyDialog = /** @class */ (function (_super) {
            __extends(HistorialReadOnlyDialog, _super);
            function HistorialReadOnlyDialog() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             * This is the method that gets list of tool
             * buttons to be created in a dialog.
             *
             * Here we'll remove save and close button, and
             * apply changes buttons.
             */
            HistorialReadOnlyDialog.prototype.getToolbarButtons = function () {
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "save-and-close-button"; }), 1);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "apply-changes-button"; }), 1);
                // We could also remove delete button here, but for demonstration 
                // purposes we'll hide it in another method (updateInterface)
                // buttons.splice(Q.indexOf(buttons, x => x.cssClass == "delete-button"), 1);
                return buttons;
            };
            /**
             * This method is a good place to update states of
             * interface elements. It is called after dialog
             * is initialized and an entity is loaded into dialog.
             * This is also called in new item mode.
             */
            HistorialReadOnlyDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                // finding all editor elements and setting their readonly attribute
                // note that this helper method only works with basic inputs, 
                // some editors require widget based set readonly overload (setReadOnly)
                Serenity.EditorUtils.setReadonly(this.element.find('.editor'), true);
                // remove required asterisk (*)
                this.element.find('sup').hide();
                // here is a way to locate a button by its css class
                // note that this method is not available in 
                // getToolbarButtons() because buttons are not 
                // created there yet!
                // 
                // this.toolbar.findButton('delete-button').hide();
                // entity dialog also has reference variables to
                // its default buttons, lets use them to hide delete button
                this.deleteButton.hide();
                // we could also hide save buttons just like delete button,
                // but they are null now as we removed them in getToolbarButtons()
                // if we didn't we could write like this:
                // 
                // this.applyChangesButton.hide();
                // this.saveAndCloseButton.hide();
                // instead of hiding, we could disable a button
                // 
                // this.deleteButton.toggleClass('disabled', true);
            };
            /**
             * This method is called when dialog title needs to be updated.
             * Base class returns something like 'Edit xyz' for edit mode,
             * and 'New xyz' for new record mode.
             *
             * But our dialog is readonly, so we should change it to 'View xyz'
             */
            HistorialReadOnlyDialog.prototype.getEntityTitle = function () {
                if (!this.isEditMode()) {
                    // we shouldn't hit here, but anyway for demo...
                    return "Como has conseguido entrar aquí?. Avisa a Informática";
                }
                else {
                    // entitySingular is type of record this dialog edits. something like 'Supplier'.
                    // you could hardcode it, but this is for demonstration
                    var entityType = _super.prototype.getEntitySingular.call(this);
                    // get name field value of record this dialog edits
                    var name_4 = this.getEntityNameFieldValue() || "";
                    // you could use Q.format with a local text, but again demo...
                    return 'Vista ' + entityType + " (" + name_4 + ")";
                }
            };
            /**
             * This method is actually the one that calls getEntityTitle()
             * and updates the dialog title. We could do it here too...
             */
            HistorialReadOnlyDialog.prototype.updateTitle = function () {
                _super.prototype.updateTitle.call(this);
                // remove super.updateTitle() call above and uncomment 
                // below line if you'd like to use this version
                // 
                // this.dialogTitle = 'View Supplier (' + this.getEntityNameFieldValue() + ')';
            };
            HistorialReadOnlyDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], HistorialReadOnlyDialog);
            return HistorialReadOnlyDialog;
        }(Nuevo_Roezec.HistorialEmpresasDialog));
        Nuevo_Roezec.HistorialReadOnlyDialog = HistorialReadOnlyDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../HistorialEmpresas/HistorialEmpresasGrid.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var HistorialReadOnlyGrid = /** @class */ (function (_super) {
            __extends(HistorialReadOnlyGrid, _super);
            function HistorialReadOnlyGrid(container) {
                return _super.call(this, container) || this;
            }
            HistorialReadOnlyGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.HistorialReadOnly'; };
            HistorialReadOnlyGrid.prototype.getDialogType = function () { return Nuevo_Roezec.HistorialReadOnlyDialog; };
            /**
             * Removing add button from grid using its css class
             */
            HistorialReadOnlyGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/HistorialEmpresas/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            /**
            * We override getColumns() to change format functions for some columns.
            * You could also write them as formatter classes, and use them at server side
            */
            HistorialReadOnlyGrid.prototype.getColumns = function () {
                var columns = _super.prototype.getColumns.call(this);
                Q.first(columns, function (x) { return x.field == "EmpresaRazon" /* EmpresaRazon */; }).format =
                    function (ctx) { return "<a href=\"javascript:;\" class=\"empresa-link\">" + Q.htmlEncode(ctx.value) + "</a>"; };
                return columns;
            };
            HistorialReadOnlyGrid.prototype.onClick = function (e, row, cell) {
                // let base grid handle clicks for its edit links
                _super.prototype.onClick.call(this, e, row, cell);
                // if base grid already handled, we shouldn"t handle it again
                if (e.isDefaultPrevented()) {
                    return;
                }
                // get reference to current item
                var item = this.itemAt(row);
                // get reference to clicked element
                var target = $(e.target);
                if (target.hasClass("empresa-link")) {
                    e.preventDefault();
                    new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                    //    let message = Q.format(
                    //        "<p>Has pulsado sobre la la empresa {0}.</p>" +
                    //        "<p>Si pulsas sobre Si, abrimos la empresa.</p>" +
                    //        "<p>Si pulsas NO, abrimos la alarma.</p>",
                    //        Q.htmlEncode(item.EmpresaRazon));
                    //    Q.confirm(message, () => {
                    //        new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                    //    },
                    //        {
                    //            htmlEncode: false,
                    //            onNo: () => {
                    //                new Nuevo_Roezec.AlarmasDialog().loadByIdAndOpenDialog(item.AlarmaId);
                    //            }
                    //        });
                }
            };
            HistorialReadOnlyGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], HistorialReadOnlyGrid);
            return HistorialReadOnlyGrid;
        }(Nuevo_Roezec.HistorialEmpresasGrid));
        Nuevo_Roezec.HistorialReadOnlyGrid = HistorialReadOnlyGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IdiomasDialog = /** @class */ (function (_super) {
            __extends(IdiomasDialog, _super);
            function IdiomasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.IdiomasForm(_this.idPrefix);
                return _this;
            }
            IdiomasDialog.prototype.getFormKey = function () { return Nuevo_Roezec.IdiomasForm.formKey; };
            IdiomasDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.IdiomasRow.idProperty; };
            IdiomasDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.IdiomasRow.localTextPrefix; };
            IdiomasDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.IdiomasRow.nameProperty; };
            IdiomasDialog.prototype.getService = function () { return Nuevo_Roezec.IdiomasService.baseUrl; };
            IdiomasDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.IdiomasRow.deletePermission; };
            IdiomasDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.IdiomasRow.insertPermission; };
            IdiomasDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.IdiomasRow.updatePermission; };
            IdiomasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], IdiomasDialog);
            return IdiomasDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.IdiomasDialog = IdiomasDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IdiomasGrid = /** @class */ (function (_super) {
            __extends(IdiomasGrid, _super);
            function IdiomasGrid(container) {
                return _super.call(this, container) || this;
            }
            IdiomasGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Idiomas'; };
            IdiomasGrid.prototype.getDialogType = function () { return Nuevo_Roezec.IdiomasDialog; };
            IdiomasGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.IdiomasRow.idProperty; };
            IdiomasGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.IdiomasRow.insertPermission; };
            IdiomasGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.IdiomasRow.localTextPrefix; };
            IdiomasGrid.prototype.getService = function () { return Nuevo_Roezec.IdiomasService.baseUrl; };
            IdiomasGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], IdiomasGrid);
            return IdiomasGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.IdiomasGrid = IdiomasGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IslasDialog = /** @class */ (function (_super) {
            __extends(IslasDialog, _super);
            function IslasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.IslasForm(_this.idPrefix);
                return _this;
            }
            IslasDialog.prototype.getFormKey = function () { return Nuevo_Roezec.IslasForm.formKey; };
            IslasDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.IslasRow.idProperty; };
            IslasDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.IslasRow.localTextPrefix; };
            IslasDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.IslasRow.nameProperty; };
            IslasDialog.prototype.getService = function () { return Nuevo_Roezec.IslasService.baseUrl; };
            IslasDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.IslasRow.deletePermission; };
            IslasDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.IslasRow.insertPermission; };
            IslasDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.IslasRow.updatePermission; };
            IslasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], IslasDialog);
            return IslasDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.IslasDialog = IslasDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var IslasGrid = /** @class */ (function (_super) {
            __extends(IslasGrid, _super);
            function IslasGrid(container) {
                return _super.call(this, container) || this;
            }
            IslasGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Islas'; };
            IslasGrid.prototype.getDialogType = function () { return Nuevo_Roezec.IslasDialog; };
            IslasGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.IslasRow.idProperty; };
            IslasGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.IslasRow.insertPermission; };
            IslasGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.IslasRow.localTextPrefix; };
            IslasGrid.prototype.getService = function () { return Nuevo_Roezec.IslasService.baseUrl; };
            IslasGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], IslasGrid);
            return IslasGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.IslasGrid = IslasGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosDialog = /** @class */ (function (_super) {
            __extends(MercadosDialog, _super);
            function MercadosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.MercadosForm(_this.idPrefix);
                return _this;
            }
            MercadosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.MercadosForm.formKey; };
            MercadosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.MercadosRow.idProperty; };
            MercadosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.MercadosRow.localTextPrefix; };
            MercadosDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.MercadosRow.nameProperty; };
            MercadosDialog.prototype.getService = function () { return Nuevo_Roezec.MercadosService.baseUrl; };
            MercadosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.MercadosRow.deletePermission; };
            MercadosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.MercadosRow.insertPermission; };
            MercadosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.MercadosRow.updatePermission; };
            MercadosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], MercadosDialog);
            return MercadosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.MercadosDialog = MercadosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosGrid = /** @class */ (function (_super) {
            __extends(MercadosGrid, _super);
            function MercadosGrid(container) {
                return _super.call(this, container) || this;
            }
            MercadosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Mercados'; };
            MercadosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.MercadosDialog; };
            MercadosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.MercadosRow.idProperty; };
            MercadosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.MercadosRow.insertPermission; };
            MercadosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.MercadosRow.localTextPrefix; };
            MercadosGrid.prototype.getService = function () { return Nuevo_Roezec.MercadosService.baseUrl; };
            MercadosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], MercadosGrid);
            return MercadosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.MercadosGrid = MercadosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../EmpresasMercados/EmpresasMercadosDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosReadOnlyDialog = /** @class */ (function (_super) {
            __extends(MercadosReadOnlyDialog, _super);
            function MercadosReadOnlyDialog() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             * This is the method that gets list of tool
             * buttons to be created in a dialog.
             *
             * Here we'll remove save and close button, and
             * apply changes buttons.
             */
            MercadosReadOnlyDialog.prototype.getToolbarButtons = function () {
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "save-and-close-button"; }), 1);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "apply-changes-button"; }), 1);
                // We could also remove delete button here, but for demonstration 
                // purposes we'll hide it in another method (updateInterface)
                // buttons.splice(Q.indexOf(buttons, x => x.cssClass == "delete-button"), 1);
                return buttons;
            };
            /**
             * This method is a good place to update states of
             * interface elements. It is called after dialog
             * is initialized and an entity is loaded into dialog.
             * This is also called in new item mode.
             */
            MercadosReadOnlyDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                // finding all editor elements and setting their readonly attribute
                // note that this helper method only works with basic inputs, 
                // some editors require widget based set readonly overload (setReadOnly)
                Serenity.EditorUtils.setReadonly(this.element.find('.editor'), true);
                // remove required asterisk (*)
                this.element.find('sup').hide();
                // here is a way to locate a button by its css class
                // note that this method is not available in 
                // getToolbarButtons() because buttons are not 
                // created there yet!
                // 
                // this.toolbar.findButton('delete-button').hide();
                // entity dialog also has reference variables to
                // its default buttons, lets use them to hide delete button
                this.deleteButton.hide();
                // we could also hide save buttons just like delete button,
                // but they are null now as we removed them in getToolbarButtons()
                // if we didn't we could write like this:
                // 
                // this.applyChangesButton.hide();
                // this.saveAndCloseButton.hide();
                // instead of hiding, we could disable a button
                // 
                // this.deleteButton.toggleClass('disabled', true);
            };
            /**
             * This method is called when dialog title needs to be updated.
             * Base class returns something like 'Edit xyz' for edit mode,
             * and 'New xyz' for new record mode.
             *
             * But our dialog is readonly, so we should change it to 'View xyz'
             */
            MercadosReadOnlyDialog.prototype.getEntityTitle = function () {
                if (!this.isEditMode()) {
                    // we shouldn't hit here, but anyway for demo...
                    return "Como has conseguido entrar aquí?. Avisa a Informática";
                }
                else {
                    // entitySingular is type of record this dialog edits. something like 'Supplier'.
                    // you could hardcode it, but this is for demonstration
                    var entityType = _super.prototype.getEntitySingular.call(this);
                    // get name field value of record this dialog edits
                    var name_5 = this.getEntityNameFieldValue() || "";
                    // you could use Q.format with a local text, but again demo...
                    return 'Vista ' + entityType + " (" + name_5 + ")";
                }
            };
            /**
             * This method is actually the one that calls getEntityTitle()
             * and updates the dialog title. We could do it here too...
             */
            MercadosReadOnlyDialog.prototype.updateTitle = function () {
                _super.prototype.updateTitle.call(this);
                // remove super.updateTitle() call above and uncomment 
                // below line if you'd like to use this version
                // 
                // this.dialogTitle = 'View Supplier (' + this.getEntityNameFieldValue() + ')';
            };
            MercadosReadOnlyDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], MercadosReadOnlyDialog);
            return MercadosReadOnlyDialog;
        }(Nuevo_Roezec.EmpresasMercadosDialog));
        Nuevo_Roezec.MercadosReadOnlyDialog = MercadosReadOnlyDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../EmpresasMercados/EmpresasMercadosGrid.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MercadosReadOnlyGrid = /** @class */ (function (_super) {
            __extends(MercadosReadOnlyGrid, _super);
            function MercadosReadOnlyGrid(container) {
                return _super.call(this, container) || this;
            }
            MercadosReadOnlyGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.MercadosReadOnly'; };
            MercadosReadOnlyGrid.prototype.getDialogType = function () { return Nuevo_Roezec.MercadosReadOnlyDialog; };
            /**
             * Removing add button from grid using its css class
             */
            MercadosReadOnlyGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/Mercados/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            MercadosReadOnlyGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], MercadosReadOnlyGrid);
            return MercadosReadOnlyGrid;
        }(Nuevo_Roezec.EmpresasMercadosGrid));
        Nuevo_Roezec.MercadosReadOnlyGrid = MercadosReadOnlyGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MetadatosDialog = /** @class */ (function (_super) {
            __extends(MetadatosDialog, _super);
            function MetadatosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.MetadatosForm(_this.idPrefix);
                return _this;
            }
            MetadatosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.MetadatosForm.formKey; };
            MetadatosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.MetadatosRow.idProperty; };
            MetadatosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.MetadatosRow.localTextPrefix; };
            MetadatosDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.MetadatosRow.nameProperty; };
            MetadatosDialog.prototype.getService = function () { return Nuevo_Roezec.MetadatosService.baseUrl; };
            MetadatosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.MetadatosRow.deletePermission; };
            MetadatosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.MetadatosRow.insertPermission; };
            MetadatosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.MetadatosRow.updatePermission; };
            MetadatosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], MetadatosDialog);
            return MetadatosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.MetadatosDialog = MetadatosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MetadatosEditDialog = /** @class */ (function (_super) {
            __extends(MetadatosEditDialog, _super);
            function MetadatosEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.MetadatosForm(_this.idPrefix);
                return _this;
            }
            MetadatosEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.MetadatosForm.formKey; };
            MetadatosEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.MetadatosRow.nameProperty; };
            MetadatosEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.MetadatosRow.localTextPrefix; };
            MetadatosEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], MetadatosEditDialog);
            return MetadatosEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.MetadatosEditDialog = MetadatosEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MetadatosEditor = /** @class */ (function (_super) {
            __extends(MetadatosEditor, _super);
            function MetadatosEditor(container) {
                return _super.call(this, container) || this;
            }
            MetadatosEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.Metadatos"; };
            MetadatosEditor.prototype.getDialogType = function () { return Nuevo_Roezec.MetadatosEditDialog; };
            MetadatosEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.MetadatosRow.localTextPrefix; };
            MetadatosEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Metadato";
            };
            MetadatosEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], MetadatosEditor);
            return MetadatosEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.MetadatosEditor = MetadatosEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var MetadatosGrid = /** @class */ (function (_super) {
            __extends(MetadatosGrid, _super);
            function MetadatosGrid(container) {
                return _super.call(this, container) || this;
            }
            MetadatosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Metadatos'; };
            MetadatosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.MetadatosDialog; };
            MetadatosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.MetadatosRow.idProperty; };
            MetadatosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.MetadatosRow.insertPermission; };
            MetadatosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.MetadatosRow.localTextPrefix; };
            MetadatosGrid.prototype.getService = function () { return Nuevo_Roezec.MetadatosService.baseUrl; };
            MetadatosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], MetadatosGrid);
            return MetadatosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.MetadatosGrid = MetadatosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var NacesDialog = /** @class */ (function (_super) {
            __extends(NacesDialog, _super);
            function NacesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.NacesForm(_this.idPrefix);
                return _this;
            }
            NacesDialog.prototype.getFormKey = function () { return Nuevo_Roezec.NacesForm.formKey; };
            NacesDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.NacesRow.idProperty; };
            NacesDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.NacesRow.localTextPrefix; };
            NacesDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.NacesRow.nameProperty; };
            NacesDialog.prototype.getService = function () { return Nuevo_Roezec.NacesService.baseUrl; };
            NacesDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.NacesRow.deletePermission; };
            NacesDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.NacesRow.insertPermission; };
            NacesDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.NacesRow.updatePermission; };
            NacesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], NacesDialog);
            return NacesDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.NacesDialog = NacesDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var NacesGrid = /** @class */ (function (_super) {
            __extends(NacesGrid, _super);
            function NacesGrid(container) {
                return _super.call(this, container) || this;
            }
            NacesGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Naces'; };
            NacesGrid.prototype.getDialogType = function () { return Nuevo_Roezec.NacesDialog; };
            NacesGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.NacesRow.idProperty; };
            NacesGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.NacesRow.insertPermission; };
            NacesGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.NacesRow.localTextPrefix; };
            NacesGrid.prototype.getService = function () { return Nuevo_Roezec.NacesService.baseUrl; };
            // Añadidos
            // Primero campo de ordenación por defecto
            // No olvidarse Cambiar el Row y el Id
            NacesGrid.prototype.getDefaultSortBy = function () {
                return ["Codigo" /* Codigo */];
            };
            // Botones Excel y Pdf
            NacesGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/Naces/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            NacesGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], NacesGrid);
            return NacesGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.NacesGrid = NacesGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../EmpresasNace/EmpresasNaceDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var NacesReadOnlyDialog = /** @class */ (function (_super) {
            __extends(NacesReadOnlyDialog, _super);
            function NacesReadOnlyDialog() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            /**
             * This is the method that gets list of tool
             * buttons to be created in a dialog.
             *
             * Here we'll remove save and close button, and
             * apply changes buttons.
             */
            NacesReadOnlyDialog.prototype.getToolbarButtons = function () {
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "save-and-close-button"; }), 1);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "apply-changes-button"; }), 1);
                // We could also remove delete button here, but for demonstration 
                // purposes we'll hide it in another method (updateInterface)
                // buttons.splice(Q.indexOf(buttons, x => x.cssClass == "delete-button"), 1);
                return buttons;
            };
            /**
             * This method is a good place to update states of
             * interface elements. It is called after dialog
             * is initialized and an entity is loaded into dialog.
             * This is also called in new item mode.
             */
            NacesReadOnlyDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                // finding all editor elements and setting their readonly attribute
                // note that this helper method only works with basic inputs, 
                // some editors require widget based set readonly overload (setReadOnly)
                Serenity.EditorUtils.setReadonly(this.element.find('.editor'), true);
                // remove required asterisk (*)
                this.element.find('sup').hide();
                // here is a way to locate a button by its css class
                // note that this method is not available in 
                // getToolbarButtons() because buttons are not 
                // created there yet!
                // 
                // this.toolbar.findButton('delete-button').hide();
                // entity dialog also has reference variables to
                // its default buttons, lets use them to hide delete button
                this.deleteButton.hide();
                // we could also hide save buttons just like delete button,
                // but they are null now as we removed them in getToolbarButtons()
                // if we didn't we could write like this:
                // 
                // this.applyChangesButton.hide();
                // this.saveAndCloseButton.hide();
                // instead of hiding, we could disable a button
                // 
                // this.deleteButton.toggleClass('disabled', true);
            };
            /**
             * This method is called when dialog title needs to be updated.
             * Base class returns something like 'Edit xyz' for edit mode,
             * and 'New xyz' for new record mode.
             *
             * But our dialog is readonly, so we should change it to 'View xyz'
             */
            NacesReadOnlyDialog.prototype.getEntityTitle = function () {
                if (!this.isEditMode()) {
                    // we shouldn't hit here, but anyway for demo...
                    return "Como has conseguido entrar aquí?. Avisa a Informática";
                }
                else {
                    // entitySingular is type of record this dialog edits. something like 'Supplier'.
                    // you could hardcode it, but this is for demonstration
                    var entityType = _super.prototype.getEntitySingular.call(this);
                    // get name field value of record this dialog edits
                    var name_6 = this.getEntityNameFieldValue() || "";
                    // you could use Q.format with a local text, but again demo...
                    return 'Vista ' + entityType + " (" + name_6 + ")";
                }
            };
            /**
             * This method is actually the one that calls getEntityTitle()
             * and updates the dialog title. We could do it here too...
             */
            NacesReadOnlyDialog.prototype.updateTitle = function () {
                _super.prototype.updateTitle.call(this);
                // remove super.updateTitle() call above and uncomment 
                // below line if you'd like to use this version
                // 
                // this.dialogTitle = 'View Supplier (' + this.getEntityNameFieldValue() + ')';
            };
            NacesReadOnlyDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], NacesReadOnlyDialog);
            return NacesReadOnlyDialog;
        }(Nuevo_Roezec.EmpresasNaceDialog));
        Nuevo_Roezec.NacesReadOnlyDialog = NacesReadOnlyDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../EmpresasNace/EmpresasNaceGrid.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var NacesReadOnlyGrid = /** @class */ (function (_super) {
            __extends(NacesReadOnlyGrid, _super);
            function NacesReadOnlyGrid(container) {
                return _super.call(this, container) || this;
            }
            NacesReadOnlyGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.NacesReadOnly'; };
            NacesReadOnlyGrid.prototype.getDialogType = function () { return Nuevo_Roezec.NacesReadOnlyDialog; };
            /**
             * Removing add button from grid using its css class
             */
            NacesReadOnlyGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.splice(Q.indexOf(buttons, function (x) { return x.cssClass == "add-button"; }), 1);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/EmpresasNace/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            /**
    * We override getColumns() to change format functions for some columns.
    * You could also write them as formatter classes, and use them at server side
    */
            NacesReadOnlyGrid.prototype.getColumns = function () {
                var columns = _super.prototype.getColumns.call(this);
                Q.first(columns, function (x) { return x.field == "EmpresaRazon" /* EmpresaRazon */; }).format =
                    function (ctx) { return "<a href=\"javascript:;\" class=\"empresa-link\">" + Q.htmlEncode(ctx.value) + "</a>"; };
                return columns;
            };
            NacesReadOnlyGrid.prototype.onClick = function (e, row, cell) {
                // let base grid handle clicks for its edit links
                _super.prototype.onClick.call(this, e, row, cell);
                // if base grid already handled, we shouldn"t handle it again
                if (e.isDefaultPrevented()) {
                    return;
                }
                // get reference to current item
                var item = this.itemAt(row);
                // get reference to clicked element
                var target = $(e.target);
                if (target.hasClass("empresa-link")) {
                    e.preventDefault();
                    new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                    //    let message = Q.format(
                    //        "<p>Has pulsado sobre la la empresa {0}.</p>" +
                    //        "<p>Si pulsas sobre Si, abrimos la empresa.</p>" +
                    //        "<p>Si pulsas NO, abrimos la alarma.</p>",
                    //        Q.htmlEncode(item.EmpresaRazon));
                    //    Q.confirm(message, () => {
                    //        new Nuevo_Roezec.EmpresasDialog().loadByIdAndOpenDialog(item.EmpresaId);
                    //    },
                    //        {
                    //            htmlEncode: false,
                    //            onNo: () => {
                    //                new Nuevo_Roezec.AlarmasDialog().loadByIdAndOpenDialog(item.AlarmaId);
                    //            }
                    //        });
                }
            };
            NacesReadOnlyGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], NacesReadOnlyGrid);
            return NacesReadOnlyGrid;
        }(Nuevo_Roezec.EmpresasNaceGrid));
        Nuevo_Roezec.NacesReadOnlyGrid = NacesReadOnlyGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PaisesDialog = /** @class */ (function (_super) {
            __extends(PaisesDialog, _super);
            function PaisesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.PaisesForm(_this.idPrefix);
                return _this;
            }
            PaisesDialog.prototype.getFormKey = function () { return Nuevo_Roezec.PaisesForm.formKey; };
            PaisesDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.PaisesRow.idProperty; };
            PaisesDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.PaisesRow.localTextPrefix; };
            PaisesDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.PaisesRow.nameProperty; };
            PaisesDialog.prototype.getService = function () { return Nuevo_Roezec.PaisesService.baseUrl; };
            PaisesDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.PaisesRow.deletePermission; };
            PaisesDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.PaisesRow.insertPermission; };
            PaisesDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.PaisesRow.updatePermission; };
            PaisesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], PaisesDialog);
            return PaisesDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.PaisesDialog = PaisesDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PaisesGrid = /** @class */ (function (_super) {
            __extends(PaisesGrid, _super);
            function PaisesGrid(container) {
                return _super.call(this, container) || this;
            }
            PaisesGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Paises'; };
            PaisesGrid.prototype.getDialogType = function () { return Nuevo_Roezec.PaisesDialog; };
            PaisesGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.PaisesRow.idProperty; };
            PaisesGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.PaisesRow.insertPermission; };
            PaisesGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.PaisesRow.localTextPrefix; };
            PaisesGrid.prototype.getService = function () { return Nuevo_Roezec.PaisesService.baseUrl; };
            PaisesGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], PaisesGrid);
            return PaisesGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.PaisesGrid = PaisesGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PlazosDialog = /** @class */ (function (_super) {
            __extends(PlazosDialog, _super);
            function PlazosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.PlazosForm(_this.idPrefix);
                return _this;
            }
            PlazosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.PlazosForm.formKey; };
            PlazosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.PlazosRow.idProperty; };
            PlazosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.PlazosRow.localTextPrefix; };
            PlazosDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.PlazosRow.nameProperty; };
            PlazosDialog.prototype.getService = function () { return Nuevo_Roezec.PlazosService.baseUrl; };
            PlazosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.PlazosRow.deletePermission; };
            PlazosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.PlazosRow.insertPermission; };
            PlazosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.PlazosRow.updatePermission; };
            PlazosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], PlazosDialog);
            return PlazosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.PlazosDialog = PlazosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var PlazosGrid = /** @class */ (function (_super) {
            __extends(PlazosGrid, _super);
            function PlazosGrid(container) {
                return _super.call(this, container) || this;
            }
            PlazosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Plazos'; };
            PlazosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.PlazosDialog; };
            PlazosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.PlazosRow.idProperty; };
            PlazosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.PlazosRow.insertPermission; };
            PlazosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.PlazosRow.localTextPrefix; };
            PlazosGrid.prototype.getService = function () { return Nuevo_Roezec.PlazosService.baseUrl; };
            PlazosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], PlazosGrid);
            return PlazosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.PlazosGrid = PlazosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedenciaCapitalDialog = /** @class */ (function (_super) {
            __extends(ProcedenciaCapitalDialog, _super);
            function ProcedenciaCapitalDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.ProcedenciaCapitalForm(_this.idPrefix);
                return _this;
            }
            ProcedenciaCapitalDialog.prototype.getFormKey = function () { return Nuevo_Roezec.ProcedenciaCapitalForm.formKey; };
            ProcedenciaCapitalDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.idProperty; };
            ProcedenciaCapitalDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.localTextPrefix; };
            ProcedenciaCapitalDialog.prototype.getService = function () { return Nuevo_Roezec.ProcedenciaCapitalService.baseUrl; };
            ProcedenciaCapitalDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.deletePermission; };
            ProcedenciaCapitalDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.insertPermission; };
            ProcedenciaCapitalDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.updatePermission; };
            ProcedenciaCapitalDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], ProcedenciaCapitalDialog);
            return ProcedenciaCapitalDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.ProcedenciaCapitalDialog = ProcedenciaCapitalDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorDialog.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedenciaCapitalEditDialog = /** @class */ (function (_super) {
            __extends(ProcedenciaCapitalEditDialog, _super);
            function ProcedenciaCapitalEditDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Nuevo_Roezec.ProcedenciaCapitalForm(_this.idPrefix);
                return _this;
            }
            ProcedenciaCapitalEditDialog.prototype.getFormKey = function () { return Nuevo_Roezec.ProcedenciaCapitalForm.formKey; };
            ProcedenciaCapitalEditDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.nameProperty; };
            ProcedenciaCapitalEditDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.localTextPrefix; };
            ProcedenciaCapitalEditDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], ProcedenciaCapitalEditDialog);
            return ProcedenciaCapitalEditDialog;
        }(ProyectosZec.Common.GridEditorDialog));
        Nuevo_Roezec.ProcedenciaCapitalEditDialog = ProcedenciaCapitalEditDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
/// <reference path="../../Common/Helpers/GridEditorBase.ts" />
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedenciaCapitalEditor = /** @class */ (function (_super) {
            __extends(ProcedenciaCapitalEditor, _super);
            function ProcedenciaCapitalEditor(container) {
                return _super.call(this, container) || this;
            }
            ProcedenciaCapitalEditor.prototype.getColumnsKey = function () { return "Nuevo_Roezec.ProcedenciaCapital"; };
            ProcedenciaCapitalEditor.prototype.getDialogType = function () { return Nuevo_Roezec.ProcedenciaCapitalEditDialog; };
            ProcedenciaCapitalEditor.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.localTextPrefix; };
            ProcedenciaCapitalEditor.prototype.getAddButtonCaption = function () {
                return "Añadir Capital";
            };
            ProcedenciaCapitalEditor = __decorate([
                Serenity.Decorators.registerEditor(),
                Serenity.Decorators.registerClass()
            ], ProcedenciaCapitalEditor);
            return ProcedenciaCapitalEditor;
        }(ProyectosZec.Common.GridEditorBase));
        Nuevo_Roezec.ProcedenciaCapitalEditor = ProcedenciaCapitalEditor;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedenciaCapitalGrid = /** @class */ (function (_super) {
            __extends(ProcedenciaCapitalGrid, _super);
            function ProcedenciaCapitalGrid(container) {
                return _super.call(this, container) || this;
            }
            ProcedenciaCapitalGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.ProcedenciaCapital'; };
            ProcedenciaCapitalGrid.prototype.getDialogType = function () { return Nuevo_Roezec.ProcedenciaCapitalDialog; };
            ProcedenciaCapitalGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.idProperty; };
            ProcedenciaCapitalGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.insertPermission; };
            ProcedenciaCapitalGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ProcedenciaCapitalRow.localTextPrefix; };
            ProcedenciaCapitalGrid.prototype.getService = function () { return Nuevo_Roezec.ProcedenciaCapitalService.baseUrl; };
            ProcedenciaCapitalGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], ProcedenciaCapitalGrid);
            return ProcedenciaCapitalGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.ProcedenciaCapitalGrid = ProcedenciaCapitalGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedimientosDialog = /** @class */ (function (_super) {
            __extends(ProcedimientosDialog, _super);
            function ProcedimientosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.ProcedimientosForm(_this.idPrefix);
                return _this;
            }
            ProcedimientosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.ProcedimientosForm.formKey; };
            ProcedimientosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.ProcedimientosRow.idProperty; };
            ProcedimientosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ProcedimientosRow.localTextPrefix; };
            ProcedimientosDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.ProcedimientosRow.nameProperty; };
            ProcedimientosDialog.prototype.getService = function () { return Nuevo_Roezec.ProcedimientosService.baseUrl; };
            ProcedimientosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.ProcedimientosRow.deletePermission; };
            ProcedimientosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.ProcedimientosRow.insertPermission; };
            ProcedimientosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.ProcedimientosRow.updatePermission; };
            ProcedimientosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], ProcedimientosDialog);
            return ProcedimientosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.ProcedimientosDialog = ProcedimientosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var ProcedimientosGrid = /** @class */ (function (_super) {
            __extends(ProcedimientosGrid, _super);
            function ProcedimientosGrid(container) {
                return _super.call(this, container) || this;
            }
            ProcedimientosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Procedimientos'; };
            ProcedimientosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.ProcedimientosDialog; };
            ProcedimientosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.ProcedimientosRow.idProperty; };
            ProcedimientosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.ProcedimientosRow.insertPermission; };
            ProcedimientosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.ProcedimientosRow.localTextPrefix; };
            ProcedimientosGrid.prototype.getService = function () { return Nuevo_Roezec.ProcedimientosService.baseUrl; };
            // Añadidos
            // Primero campo de ordenación por defecto
            // No olvidarse Cambiar el Row y el Id
            ProcedimientosGrid.prototype.getDefaultSortBy = function () {
                return ["Orden" /* Orden */];
            };
            // Botones Excel y Pdf
            ProcedimientosGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/Procedimientos/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            ProcedimientosGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], ProcedimientosGrid);
            return ProcedimientosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.ProcedimientosGrid = ProcedimientosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SectoresDialog = /** @class */ (function (_super) {
            __extends(SectoresDialog, _super);
            function SectoresDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.SectoresForm(_this.idPrefix);
                return _this;
            }
            SectoresDialog.prototype.getFormKey = function () { return Nuevo_Roezec.SectoresForm.formKey; };
            SectoresDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.SectoresRow.idProperty; };
            SectoresDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.SectoresRow.localTextPrefix; };
            SectoresDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.SectoresRow.nameProperty; };
            SectoresDialog.prototype.getService = function () { return Nuevo_Roezec.SectoresService.baseUrl; };
            SectoresDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.SectoresRow.deletePermission; };
            SectoresDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.SectoresRow.insertPermission; };
            SectoresDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.SectoresRow.updatePermission; };
            SectoresDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], SectoresDialog);
            return SectoresDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.SectoresDialog = SectoresDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SectoresGrid = /** @class */ (function (_super) {
            __extends(SectoresGrid, _super);
            function SectoresGrid(container) {
                return _super.call(this, container) || this;
            }
            SectoresGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Sectores'; };
            SectoresGrid.prototype.getDialogType = function () { return Nuevo_Roezec.SectoresDialog; };
            SectoresGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.SectoresRow.idProperty; };
            SectoresGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.SectoresRow.insertPermission; };
            SectoresGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.SectoresRow.localTextPrefix; };
            SectoresGrid.prototype.getService = function () { return Nuevo_Roezec.SectoresService.baseUrl; };
            SectoresGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], SectoresGrid);
            return SectoresGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.SectoresGrid = SectoresGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SentidosresolucionDialog = /** @class */ (function (_super) {
            __extends(SentidosresolucionDialog, _super);
            function SentidosresolucionDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.SentidosresolucionForm(_this.idPrefix);
                return _this;
            }
            SentidosresolucionDialog.prototype.getFormKey = function () { return Nuevo_Roezec.SentidosresolucionForm.formKey; };
            SentidosresolucionDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.SentidosresolucionRow.idProperty; };
            SentidosresolucionDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.SentidosresolucionRow.localTextPrefix; };
            SentidosresolucionDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.SentidosresolucionRow.nameProperty; };
            SentidosresolucionDialog.prototype.getService = function () { return Nuevo_Roezec.SentidosresolucionService.baseUrl; };
            SentidosresolucionDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.SentidosresolucionRow.deletePermission; };
            SentidosresolucionDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.SentidosresolucionRow.insertPermission; };
            SentidosresolucionDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.SentidosresolucionRow.updatePermission; };
            SentidosresolucionDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], SentidosresolucionDialog);
            return SentidosresolucionDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.SentidosresolucionDialog = SentidosresolucionDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SentidosresolucionGrid = /** @class */ (function (_super) {
            __extends(SentidosresolucionGrid, _super);
            function SentidosresolucionGrid(container) {
                return _super.call(this, container) || this;
            }
            SentidosresolucionGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Sentidosresolucion'; };
            SentidosresolucionGrid.prototype.getDialogType = function () { return Nuevo_Roezec.SentidosresolucionDialog; };
            SentidosresolucionGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.SentidosresolucionRow.idProperty; };
            SentidosresolucionGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.SentidosresolucionRow.insertPermission; };
            SentidosresolucionGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.SentidosresolucionRow.localTextPrefix; };
            SentidosresolucionGrid.prototype.getService = function () { return Nuevo_Roezec.SentidosresolucionService.baseUrl; };
            SentidosresolucionGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], SentidosresolucionGrid);
            return SentidosresolucionGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.SentidosresolucionGrid = SentidosresolucionGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SubsectoresDialog = /** @class */ (function (_super) {
            __extends(SubsectoresDialog, _super);
            function SubsectoresDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.SubsectoresForm(_this.idPrefix);
                return _this;
            }
            SubsectoresDialog.prototype.getFormKey = function () { return Nuevo_Roezec.SubsectoresForm.formKey; };
            SubsectoresDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.SubsectoresRow.idProperty; };
            SubsectoresDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.SubsectoresRow.localTextPrefix; };
            SubsectoresDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.SubsectoresRow.nameProperty; };
            SubsectoresDialog.prototype.getService = function () { return Nuevo_Roezec.SubsectoresService.baseUrl; };
            SubsectoresDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.SubsectoresRow.deletePermission; };
            SubsectoresDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.SubsectoresRow.insertPermission; };
            SubsectoresDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.SubsectoresRow.updatePermission; };
            SubsectoresDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], SubsectoresDialog);
            return SubsectoresDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.SubsectoresDialog = SubsectoresDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var SubsectoresGrid = /** @class */ (function (_super) {
            __extends(SubsectoresGrid, _super);
            function SubsectoresGrid(container) {
                return _super.call(this, container) || this;
            }
            SubsectoresGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Subsectores'; };
            SubsectoresGrid.prototype.getDialogType = function () { return Nuevo_Roezec.SubsectoresDialog; };
            SubsectoresGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.SubsectoresRow.idProperty; };
            SubsectoresGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.SubsectoresRow.insertPermission; };
            SubsectoresGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.SubsectoresRow.localTextPrefix; };
            SubsectoresGrid.prototype.getService = function () { return Nuevo_Roezec.SubsectoresService.baseUrl; };
            // Añadidos
            // Primero campo de ordenación por defecto
            // No olvidarse Cambiar el Row y el Id
            SubsectoresGrid.prototype.getDefaultSortBy = function () {
                return ["SectorId" /* SectorId */];
            };
            // Botones Excel y Pdf
            SubsectoresGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Nuevo_Roezec/Subsectores/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            SubsectoresGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], SubsectoresGrid);
            return SubsectoresGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.SubsectoresGrid = SubsectoresGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TecnicosDialog = /** @class */ (function (_super) {
            __extends(TecnicosDialog, _super);
            function TecnicosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.TecnicosForm(_this.idPrefix);
                return _this;
            }
            TecnicosDialog.prototype.getFormKey = function () { return Nuevo_Roezec.TecnicosForm.formKey; };
            TecnicosDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.TecnicosRow.idProperty; };
            TecnicosDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TecnicosRow.localTextPrefix; };
            TecnicosDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.TecnicosRow.nameProperty; };
            TecnicosDialog.prototype.getService = function () { return Nuevo_Roezec.TecnicosService.baseUrl; };
            TecnicosDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.TecnicosRow.deletePermission; };
            TecnicosDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.TecnicosRow.insertPermission; };
            TecnicosDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.TecnicosRow.updatePermission; };
            TecnicosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TecnicosDialog);
            return TecnicosDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.TecnicosDialog = TecnicosDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TecnicosGrid = /** @class */ (function (_super) {
            __extends(TecnicosGrid, _super);
            function TecnicosGrid(container) {
                return _super.call(this, container) || this;
            }
            TecnicosGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.Tecnicos'; };
            TecnicosGrid.prototype.getDialogType = function () { return Nuevo_Roezec.TecnicosDialog; };
            TecnicosGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.TecnicosRow.idProperty; };
            TecnicosGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.TecnicosRow.insertPermission; };
            TecnicosGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TecnicosRow.localTextPrefix; };
            TecnicosGrid.prototype.getService = function () { return Nuevo_Roezec.TecnicosService.baseUrl; };
            TecnicosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TecnicosGrid);
            return TecnicosGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.TecnicosGrid = TecnicosGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TipologiasCapitalDialog = /** @class */ (function (_super) {
            __extends(TipologiasCapitalDialog, _super);
            function TipologiasCapitalDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.TipologiasCapitalForm(_this.idPrefix);
                return _this;
            }
            TipologiasCapitalDialog.prototype.getFormKey = function () { return Nuevo_Roezec.TipologiasCapitalForm.formKey; };
            TipologiasCapitalDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.TipologiasCapitalRow.idProperty; };
            TipologiasCapitalDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TipologiasCapitalRow.localTextPrefix; };
            TipologiasCapitalDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.TipologiasCapitalRow.nameProperty; };
            TipologiasCapitalDialog.prototype.getService = function () { return Nuevo_Roezec.TipologiasCapitalService.baseUrl; };
            TipologiasCapitalDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.TipologiasCapitalRow.deletePermission; };
            TipologiasCapitalDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.TipologiasCapitalRow.insertPermission; };
            TipologiasCapitalDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.TipologiasCapitalRow.updatePermission; };
            TipologiasCapitalDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TipologiasCapitalDialog);
            return TipologiasCapitalDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.TipologiasCapitalDialog = TipologiasCapitalDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TipologiasCapitalGrid = /** @class */ (function (_super) {
            __extends(TipologiasCapitalGrid, _super);
            function TipologiasCapitalGrid(container) {
                return _super.call(this, container) || this;
            }
            TipologiasCapitalGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.TipologiasCapital'; };
            TipologiasCapitalGrid.prototype.getDialogType = function () { return Nuevo_Roezec.TipologiasCapitalDialog; };
            TipologiasCapitalGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.TipologiasCapitalRow.idProperty; };
            TipologiasCapitalGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.TipologiasCapitalRow.insertPermission; };
            TipologiasCapitalGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TipologiasCapitalRow.localTextPrefix; };
            TipologiasCapitalGrid.prototype.getService = function () { return Nuevo_Roezec.TipologiasCapitalService.baseUrl; };
            TipologiasCapitalGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TipologiasCapitalGrid);
            return TipologiasCapitalGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.TipologiasCapitalGrid = TipologiasCapitalGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposAlarmaDialog = /** @class */ (function (_super) {
            __extends(TiposAlarmaDialog, _super);
            function TiposAlarmaDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.TiposAlarmaForm(_this.idPrefix);
                return _this;
            }
            TiposAlarmaDialog.prototype.getFormKey = function () { return Nuevo_Roezec.TiposAlarmaForm.formKey; };
            TiposAlarmaDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposAlarmaRow.idProperty; };
            TiposAlarmaDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposAlarmaRow.localTextPrefix; };
            TiposAlarmaDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.TiposAlarmaRow.nameProperty; };
            TiposAlarmaDialog.prototype.getService = function () { return Nuevo_Roezec.TiposAlarmaService.baseUrl; };
            TiposAlarmaDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.TiposAlarmaRow.deletePermission; };
            TiposAlarmaDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposAlarmaRow.insertPermission; };
            TiposAlarmaDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.TiposAlarmaRow.updatePermission; };
            TiposAlarmaDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposAlarmaDialog);
            return TiposAlarmaDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.TiposAlarmaDialog = TiposAlarmaDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposAlarmaGrid = /** @class */ (function (_super) {
            __extends(TiposAlarmaGrid, _super);
            function TiposAlarmaGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposAlarmaGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.TiposAlarma'; };
            TiposAlarmaGrid.prototype.getDialogType = function () { return Nuevo_Roezec.TiposAlarmaDialog; };
            TiposAlarmaGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposAlarmaRow.idProperty; };
            TiposAlarmaGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposAlarmaRow.insertPermission; };
            TiposAlarmaGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposAlarmaRow.localTextPrefix; };
            TiposAlarmaGrid.prototype.getService = function () { return Nuevo_Roezec.TiposAlarmaService.baseUrl; };
            TiposAlarmaGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposAlarmaGrid);
            return TiposAlarmaGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.TiposAlarmaGrid = TiposAlarmaGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposContactoDialog = /** @class */ (function (_super) {
            __extends(TiposContactoDialog, _super);
            function TiposContactoDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.TiposContactoForm(_this.idPrefix);
                return _this;
            }
            TiposContactoDialog.prototype.getFormKey = function () { return Nuevo_Roezec.TiposContactoForm.formKey; };
            TiposContactoDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposContactoRow.idProperty; };
            TiposContactoDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposContactoRow.localTextPrefix; };
            TiposContactoDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.TiposContactoRow.nameProperty; };
            TiposContactoDialog.prototype.getService = function () { return Nuevo_Roezec.TiposContactoService.baseUrl; };
            TiposContactoDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.TiposContactoRow.deletePermission; };
            TiposContactoDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposContactoRow.insertPermission; };
            TiposContactoDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.TiposContactoRow.updatePermission; };
            TiposContactoDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposContactoDialog);
            return TiposContactoDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.TiposContactoDialog = TiposContactoDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposContactoGrid = /** @class */ (function (_super) {
            __extends(TiposContactoGrid, _super);
            function TiposContactoGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposContactoGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.TiposContacto'; };
            TiposContactoGrid.prototype.getDialogType = function () { return Nuevo_Roezec.TiposContactoDialog; };
            TiposContactoGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposContactoRow.idProperty; };
            TiposContactoGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposContactoRow.insertPermission; };
            TiposContactoGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposContactoRow.localTextPrefix; };
            TiposContactoGrid.prototype.getService = function () { return Nuevo_Roezec.TiposContactoService.baseUrl; };
            TiposContactoGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposContactoGrid);
            return TiposContactoGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.TiposContactoGrid = TiposContactoGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDireccionesDialog = /** @class */ (function (_super) {
            __extends(TiposDireccionesDialog, _super);
            function TiposDireccionesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.TiposDireccionesForm(_this.idPrefix);
                return _this;
            }
            TiposDireccionesDialog.prototype.getFormKey = function () { return Nuevo_Roezec.TiposDireccionesForm.formKey; };
            TiposDireccionesDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposDireccionesRow.idProperty; };
            TiposDireccionesDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposDireccionesRow.localTextPrefix; };
            TiposDireccionesDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.TiposDireccionesRow.nameProperty; };
            TiposDireccionesDialog.prototype.getService = function () { return Nuevo_Roezec.TiposDireccionesService.baseUrl; };
            TiposDireccionesDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.TiposDireccionesRow.deletePermission; };
            TiposDireccionesDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposDireccionesRow.insertPermission; };
            TiposDireccionesDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.TiposDireccionesRow.updatePermission; };
            TiposDireccionesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposDireccionesDialog);
            return TiposDireccionesDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.TiposDireccionesDialog = TiposDireccionesDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDireccionesGrid = /** @class */ (function (_super) {
            __extends(TiposDireccionesGrid, _super);
            function TiposDireccionesGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposDireccionesGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.TiposDirecciones'; };
            TiposDireccionesGrid.prototype.getDialogType = function () { return Nuevo_Roezec.TiposDireccionesDialog; };
            TiposDireccionesGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposDireccionesRow.idProperty; };
            TiposDireccionesGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposDireccionesRow.insertPermission; };
            TiposDireccionesGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposDireccionesRow.localTextPrefix; };
            TiposDireccionesGrid.prototype.getService = function () { return Nuevo_Roezec.TiposDireccionesService.baseUrl; };
            TiposDireccionesGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposDireccionesGrid);
            return TiposDireccionesGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.TiposDireccionesGrid = TiposDireccionesGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDocumentoDialog = /** @class */ (function (_super) {
            __extends(TiposDocumentoDialog, _super);
            function TiposDocumentoDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.TiposDocumentoForm(_this.idPrefix);
                return _this;
            }
            TiposDocumentoDialog.prototype.getFormKey = function () { return Nuevo_Roezec.TiposDocumentoForm.formKey; };
            TiposDocumentoDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposDocumentoRow.idProperty; };
            TiposDocumentoDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposDocumentoRow.localTextPrefix; };
            TiposDocumentoDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.TiposDocumentoRow.nameProperty; };
            TiposDocumentoDialog.prototype.getService = function () { return Nuevo_Roezec.TiposDocumentoService.baseUrl; };
            TiposDocumentoDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.TiposDocumentoRow.deletePermission; };
            TiposDocumentoDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposDocumentoRow.insertPermission; };
            TiposDocumentoDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.TiposDocumentoRow.updatePermission; };
            TiposDocumentoDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposDocumentoDialog);
            return TiposDocumentoDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.TiposDocumentoDialog = TiposDocumentoDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposDocumentoGrid = /** @class */ (function (_super) {
            __extends(TiposDocumentoGrid, _super);
            function TiposDocumentoGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposDocumentoGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.TiposDocumento'; };
            TiposDocumentoGrid.prototype.getDialogType = function () { return Nuevo_Roezec.TiposDocumentoDialog; };
            TiposDocumentoGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposDocumentoRow.idProperty; };
            TiposDocumentoGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposDocumentoRow.insertPermission; };
            TiposDocumentoGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposDocumentoRow.localTextPrefix; };
            TiposDocumentoGrid.prototype.getService = function () { return Nuevo_Roezec.TiposDocumentoService.baseUrl; };
            TiposDocumentoGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposDocumentoGrid);
            return TiposDocumentoGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.TiposDocumentoGrid = TiposDocumentoGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposEnvioDialog = /** @class */ (function (_super) {
            __extends(TiposEnvioDialog, _super);
            function TiposEnvioDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.TiposEnvioForm(_this.idPrefix);
                return _this;
            }
            TiposEnvioDialog.prototype.getFormKey = function () { return Nuevo_Roezec.TiposEnvioForm.formKey; };
            TiposEnvioDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposEnvioRow.idProperty; };
            TiposEnvioDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposEnvioRow.localTextPrefix; };
            TiposEnvioDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.TiposEnvioRow.nameProperty; };
            TiposEnvioDialog.prototype.getService = function () { return Nuevo_Roezec.TiposEnvioService.baseUrl; };
            TiposEnvioDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.TiposEnvioRow.deletePermission; };
            TiposEnvioDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposEnvioRow.insertPermission; };
            TiposEnvioDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.TiposEnvioRow.updatePermission; };
            TiposEnvioDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposEnvioDialog);
            return TiposEnvioDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.TiposEnvioDialog = TiposEnvioDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposEnvioGrid = /** @class */ (function (_super) {
            __extends(TiposEnvioGrid, _super);
            function TiposEnvioGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposEnvioGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.TiposEnvio'; };
            TiposEnvioGrid.prototype.getDialogType = function () { return Nuevo_Roezec.TiposEnvioDialog; };
            TiposEnvioGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposEnvioRow.idProperty; };
            TiposEnvioGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposEnvioRow.insertPermission; };
            TiposEnvioGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposEnvioRow.localTextPrefix; };
            TiposEnvioGrid.prototype.getService = function () { return Nuevo_Roezec.TiposEnvioService.baseUrl; };
            TiposEnvioGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposEnvioGrid);
            return TiposEnvioGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.TiposEnvioGrid = TiposEnvioGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposGarantiaTasasDialog = /** @class */ (function (_super) {
            __extends(TiposGarantiaTasasDialog, _super);
            function TiposGarantiaTasasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.TiposGarantiaTasasForm(_this.idPrefix);
                return _this;
            }
            TiposGarantiaTasasDialog.prototype.getFormKey = function () { return Nuevo_Roezec.TiposGarantiaTasasForm.formKey; };
            TiposGarantiaTasasDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposGarantiaTasasRow.idProperty; };
            TiposGarantiaTasasDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposGarantiaTasasRow.localTextPrefix; };
            TiposGarantiaTasasDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.TiposGarantiaTasasRow.nameProperty; };
            TiposGarantiaTasasDialog.prototype.getService = function () { return Nuevo_Roezec.TiposGarantiaTasasService.baseUrl; };
            TiposGarantiaTasasDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.TiposGarantiaTasasRow.deletePermission; };
            TiposGarantiaTasasDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposGarantiaTasasRow.insertPermission; };
            TiposGarantiaTasasDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.TiposGarantiaTasasRow.updatePermission; };
            TiposGarantiaTasasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposGarantiaTasasDialog);
            return TiposGarantiaTasasDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.TiposGarantiaTasasDialog = TiposGarantiaTasasDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposGarantiaTasasGrid = /** @class */ (function (_super) {
            __extends(TiposGarantiaTasasGrid, _super);
            function TiposGarantiaTasasGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposGarantiaTasasGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.TiposGarantiaTasas'; };
            TiposGarantiaTasasGrid.prototype.getDialogType = function () { return Nuevo_Roezec.TiposGarantiaTasasDialog; };
            TiposGarantiaTasasGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposGarantiaTasasRow.idProperty; };
            TiposGarantiaTasasGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposGarantiaTasasRow.insertPermission; };
            TiposGarantiaTasasGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposGarantiaTasasRow.localTextPrefix; };
            TiposGarantiaTasasGrid.prototype.getService = function () { return Nuevo_Roezec.TiposGarantiaTasasService.baseUrl; };
            TiposGarantiaTasasGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposGarantiaTasasGrid);
            return TiposGarantiaTasasGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.TiposGarantiaTasasGrid = TiposGarantiaTasasGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposPersonaDialog = /** @class */ (function (_super) {
            __extends(TiposPersonaDialog, _super);
            function TiposPersonaDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.TiposPersonaForm(_this.idPrefix);
                return _this;
            }
            TiposPersonaDialog.prototype.getFormKey = function () { return Nuevo_Roezec.TiposPersonaForm.formKey; };
            TiposPersonaDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposPersonaRow.idProperty; };
            TiposPersonaDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposPersonaRow.localTextPrefix; };
            TiposPersonaDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.TiposPersonaRow.nameProperty; };
            TiposPersonaDialog.prototype.getService = function () { return Nuevo_Roezec.TiposPersonaService.baseUrl; };
            TiposPersonaDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.TiposPersonaRow.deletePermission; };
            TiposPersonaDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposPersonaRow.insertPermission; };
            TiposPersonaDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.TiposPersonaRow.updatePermission; };
            TiposPersonaDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposPersonaDialog);
            return TiposPersonaDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.TiposPersonaDialog = TiposPersonaDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var TiposPersonaGrid = /** @class */ (function (_super) {
            __extends(TiposPersonaGrid, _super);
            function TiposPersonaGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposPersonaGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.TiposPersona'; };
            TiposPersonaGrid.prototype.getDialogType = function () { return Nuevo_Roezec.TiposPersonaDialog; };
            TiposPersonaGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.TiposPersonaRow.idProperty; };
            TiposPersonaGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.TiposPersonaRow.insertPermission; };
            TiposPersonaGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.TiposPersonaRow.localTextPrefix; };
            TiposPersonaGrid.prototype.getService = function () { return Nuevo_Roezec.TiposPersonaService.baseUrl; };
            TiposPersonaGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposPersonaGrid);
            return TiposPersonaGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.TiposPersonaGrid = TiposPersonaGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var VersionesNaceDialog = /** @class */ (function (_super) {
            __extends(VersionesNaceDialog, _super);
            function VersionesNaceDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Nuevo_Roezec.VersionesNaceForm(_this.idPrefix);
                return _this;
            }
            VersionesNaceDialog.prototype.getFormKey = function () { return Nuevo_Roezec.VersionesNaceForm.formKey; };
            VersionesNaceDialog.prototype.getIdProperty = function () { return Nuevo_Roezec.VersionesNaceRow.idProperty; };
            VersionesNaceDialog.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.VersionesNaceRow.localTextPrefix; };
            VersionesNaceDialog.prototype.getNameProperty = function () { return Nuevo_Roezec.VersionesNaceRow.nameProperty; };
            VersionesNaceDialog.prototype.getService = function () { return Nuevo_Roezec.VersionesNaceService.baseUrl; };
            VersionesNaceDialog.prototype.getDeletePermission = function () { return Nuevo_Roezec.VersionesNaceRow.deletePermission; };
            VersionesNaceDialog.prototype.getInsertPermission = function () { return Nuevo_Roezec.VersionesNaceRow.insertPermission; };
            VersionesNaceDialog.prototype.getUpdatePermission = function () { return Nuevo_Roezec.VersionesNaceRow.updatePermission; };
            VersionesNaceDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], VersionesNaceDialog);
            return VersionesNaceDialog;
        }(Serenity.EntityDialog));
        Nuevo_Roezec.VersionesNaceDialog = VersionesNaceDialog;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Nuevo_Roezec;
    (function (Nuevo_Roezec) {
        var VersionesNaceGrid = /** @class */ (function (_super) {
            __extends(VersionesNaceGrid, _super);
            function VersionesNaceGrid(container) {
                return _super.call(this, container) || this;
            }
            VersionesNaceGrid.prototype.getColumnsKey = function () { return 'Nuevo_Roezec.VersionesNace'; };
            VersionesNaceGrid.prototype.getDialogType = function () { return Nuevo_Roezec.VersionesNaceDialog; };
            VersionesNaceGrid.prototype.getIdProperty = function () { return Nuevo_Roezec.VersionesNaceRow.idProperty; };
            VersionesNaceGrid.prototype.getInsertPermission = function () { return Nuevo_Roezec.VersionesNaceRow.insertPermission; };
            VersionesNaceGrid.prototype.getLocalTextPrefix = function () { return Nuevo_Roezec.VersionesNaceRow.localTextPrefix; };
            VersionesNaceGrid.prototype.getService = function () { return Nuevo_Roezec.VersionesNaceService.baseUrl; };
            VersionesNaceGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], VersionesNaceGrid);
            return VersionesNaceGrid;
        }(Serenity.EntityGrid));
        Nuevo_Roezec.VersionesNaceGrid = VersionesNaceGrid;
    })(Nuevo_Roezec = ProyectosZec.Nuevo_Roezec || (ProyectosZec.Nuevo_Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var RegistroDialog = /** @class */ (function (_super) {
            __extends(RegistroDialog, _super);
            function RegistroDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Registro.RegistroForm(_this.idPrefix);
                return _this;
            }
            RegistroDialog.prototype.getFormKey = function () { return Registro.RegistroForm.formKey; };
            RegistroDialog.prototype.getIdProperty = function () { return Registro.RegistroRow.idProperty; };
            RegistroDialog.prototype.getLocalTextPrefix = function () { return Registro.RegistroRow.localTextPrefix; };
            RegistroDialog.prototype.getNameProperty = function () { return Registro.RegistroRow.nameProperty; };
            RegistroDialog.prototype.getService = function () { return Registro.RegistroService.baseUrl; };
            RegistroDialog.prototype.getDeletePermission = function () { return Registro.RegistroRow.deletePermission; };
            RegistroDialog.prototype.getInsertPermission = function () { return Registro.RegistroRow.insertPermission; };
            RegistroDialog.prototype.getUpdatePermission = function () { return Registro.RegistroRow.updatePermission; };
            RegistroDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RegistroDialog);
            return RegistroDialog;
        }(Serenity.EntityDialog));
        Registro.RegistroDialog = RegistroDialog;
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var RegistroGrid = /** @class */ (function (_super) {
            __extends(RegistroGrid, _super);
            function RegistroGrid(container) {
                return _super.call(this, container) || this;
            }
            RegistroGrid.prototype.getColumnsKey = function () { return 'Registro.Registro'; };
            RegistroGrid.prototype.getDialogType = function () { return Registro.RegistroDialog; };
            RegistroGrid.prototype.getIdProperty = function () { return Registro.RegistroRow.idProperty; };
            RegistroGrid.prototype.getInsertPermission = function () { return Registro.RegistroRow.insertPermission; };
            RegistroGrid.prototype.getLocalTextPrefix = function () { return Registro.RegistroRow.localTextPrefix; };
            RegistroGrid.prototype.getService = function () { return Registro.RegistroService.baseUrl; };
            RegistroGrid.prototype.getDefaultSortBy = function () {
                return [Registro.RegistroRow.Fields.FechaRegistro]; // Este es el campo de ordenación por defecto
            };
            RegistroGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Registro/Registro/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            RegistroGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], RegistroGrid);
            return RegistroGrid;
        }(Serenity.EntityGrid));
        Registro.RegistroGrid = RegistroGrid;
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var TiposregistroDialog = /** @class */ (function (_super) {
            __extends(TiposregistroDialog, _super);
            function TiposregistroDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Registro.TiposregistroForm(_this.idPrefix);
                return _this;
            }
            TiposregistroDialog.prototype.getFormKey = function () { return Registro.TiposregistroForm.formKey; };
            TiposregistroDialog.prototype.getIdProperty = function () { return Registro.TiposregistroRow.idProperty; };
            TiposregistroDialog.prototype.getLocalTextPrefix = function () { return Registro.TiposregistroRow.localTextPrefix; };
            TiposregistroDialog.prototype.getNameProperty = function () { return Registro.TiposregistroRow.nameProperty; };
            TiposregistroDialog.prototype.getService = function () { return Registro.TiposregistroService.baseUrl; };
            TiposregistroDialog.prototype.getDeletePermission = function () { return Registro.TiposregistroRow.deletePermission; };
            TiposregistroDialog.prototype.getInsertPermission = function () { return Registro.TiposregistroRow.insertPermission; };
            TiposregistroDialog.prototype.getUpdatePermission = function () { return Registro.TiposregistroRow.updatePermission; };
            TiposregistroDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposregistroDialog);
            return TiposregistroDialog;
        }(Serenity.EntityDialog));
        Registro.TiposregistroDialog = TiposregistroDialog;
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Registro;
    (function (Registro) {
        var TiposregistroGrid = /** @class */ (function (_super) {
            __extends(TiposregistroGrid, _super);
            function TiposregistroGrid(container) {
                return _super.call(this, container) || this;
            }
            TiposregistroGrid.prototype.getColumnsKey = function () { return 'Registro.Tiposregistro'; };
            TiposregistroGrid.prototype.getDialogType = function () { return Registro.TiposregistroDialog; };
            TiposregistroGrid.prototype.getIdProperty = function () { return Registro.TiposregistroRow.idProperty; };
            TiposregistroGrid.prototype.getInsertPermission = function () { return Registro.TiposregistroRow.insertPermission; };
            TiposregistroGrid.prototype.getLocalTextPrefix = function () { return Registro.TiposregistroRow.localTextPrefix; };
            TiposregistroGrid.prototype.getService = function () { return Registro.TiposregistroService.baseUrl; };
            TiposregistroGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TiposregistroGrid);
            return TiposregistroGrid;
        }(Serenity.EntityGrid));
        Registro.TiposregistroGrid = TiposregistroGrid;
    })(Registro = ProyectosZec.Registro || (ProyectosZec.Registro = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var ActividadesDialog = /** @class */ (function (_super) {
            __extends(ActividadesDialog, _super);
            function ActividadesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Roezec.ActividadesForm(_this.idPrefix);
                return _this;
            }
            ActividadesDialog.prototype.getFormKey = function () { return Roezec.ActividadesForm.formKey; };
            ActividadesDialog.prototype.getIdProperty = function () { return Roezec.ActividadesRow.idProperty; };
            ActividadesDialog.prototype.getLocalTextPrefix = function () { return Roezec.ActividadesRow.localTextPrefix; };
            ActividadesDialog.prototype.getNameProperty = function () { return Roezec.ActividadesRow.nameProperty; };
            ActividadesDialog.prototype.getService = function () { return Roezec.ActividadesService.baseUrl; };
            ActividadesDialog.prototype.getDeletePermission = function () { return Roezec.ActividadesRow.deletePermission; };
            ActividadesDialog.prototype.getInsertPermission = function () { return Roezec.ActividadesRow.insertPermission; };
            ActividadesDialog.prototype.getUpdatePermission = function () { return Roezec.ActividadesRow.updatePermission; };
            ActividadesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], ActividadesDialog);
            return ActividadesDialog;
        }(Serenity.EntityDialog));
        Roezec.ActividadesDialog = ActividadesDialog;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var ActividadesGrid = /** @class */ (function (_super) {
            __extends(ActividadesGrid, _super);
            function ActividadesGrid(container) {
                return _super.call(this, container) || this;
            }
            ActividadesGrid.prototype.getColumnsKey = function () { return 'Roezec.Actividades'; };
            ActividadesGrid.prototype.getDialogType = function () { return Roezec.ActividadesDialog; };
            ActividadesGrid.prototype.getIdProperty = function () { return Roezec.ActividadesRow.idProperty; };
            ActividadesGrid.prototype.getInsertPermission = function () { return Roezec.ActividadesRow.insertPermission; };
            ActividadesGrid.prototype.getLocalTextPrefix = function () { return Roezec.ActividadesRow.localTextPrefix; };
            ActividadesGrid.prototype.getService = function () { return Roezec.ActividadesService.baseUrl; };
            // Agrupar y sumar 
            ActividadesGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                grid.setSelectionModel(new Slick.RowSelectionModel());
                return grid;
            };
            // Mostramos Footer con los totales
            ActividadesGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                // Fila Seleccionable
                opt.enableTextSelectionOnCells = true;
                opt.selectedCellCssClass = "slick-row-selected";
                opt.enableCellNavigation = true;
                return opt;
            };
            // Botones Excel y Pdf
            ActividadesGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Roezec/Actividades/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.push({
                    title: 'Agrupar por Año',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Año: ' + x.value + ' (' + x.count + ' Nace/Empresas)'; },
                            getter: "AnyoExpediente" /* AnyoExpediente */
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Nace',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Nace: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: "Actividad" /* Actividad */
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Empresa',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Empresa: ' + x.value + ' (' + x.count + ' Naces)'; },
                            getter: "Actividad" /* Actividad */
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Año y Nace',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Año: ' + x.value + ' (' + x.count + ' Nace/Empresas)'; },
                            getter: "AnyoExpediente" /* AnyoExpediente */
                        }, {
                            formatter: function (x) { return 'Nace: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: "Actividad" /* Actividad */
                        }]); }
                });
                buttons.push({
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: function () { return _this.view.setGrouping([]); }
                });
                return buttons;
                // Fin añadidos
            };
            ActividadesGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], ActividadesGrid);
            return ActividadesGrid;
        }(Serenity.EntityGrid));
        Roezec.ActividadesGrid = ActividadesGrid;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var EmpleosSSDialog = /** @class */ (function (_super) {
            __extends(EmpleosSSDialog, _super);
            function EmpleosSSDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Roezec.EmpleosSSForm(_this.idPrefix);
                return _this;
            }
            EmpleosSSDialog.prototype.getFormKey = function () { return Roezec.EmpleosSSForm.formKey; };
            EmpleosSSDialog.prototype.getIdProperty = function () { return Roezec.EmpleosSSRow.idProperty; };
            EmpleosSSDialog.prototype.getLocalTextPrefix = function () { return Roezec.EmpleosSSRow.localTextPrefix; };
            EmpleosSSDialog.prototype.getService = function () { return Roezec.EmpleosSSService.baseUrl; };
            EmpleosSSDialog.prototype.getDeletePermission = function () { return Roezec.EmpleosSSRow.deletePermission; };
            EmpleosSSDialog.prototype.getInsertPermission = function () { return Roezec.EmpleosSSRow.insertPermission; };
            EmpleosSSDialog.prototype.getUpdatePermission = function () { return Roezec.EmpleosSSRow.updatePermission; };
            EmpleosSSDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], EmpleosSSDialog);
            return EmpleosSSDialog;
        }(Serenity.EntityDialog));
        Roezec.EmpleosSSDialog = EmpleosSSDialog;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var EmpleosSSGrid = /** @class */ (function (_super) {
            __extends(EmpleosSSGrid, _super);
            function EmpleosSSGrid(container) {
                return _super.call(this, container) || this;
            }
            EmpleosSSGrid.prototype.getColumnsKey = function () { return 'Roezec.EmpleosSS'; };
            EmpleosSSGrid.prototype.getDialogType = function () { return Roezec.EmpleosSSDialog; };
            EmpleosSSGrid.prototype.getIdProperty = function () { return Roezec.EmpleosSSRow.idProperty; };
            EmpleosSSGrid.prototype.getInsertPermission = function () { return Roezec.EmpleosSSRow.insertPermission; };
            EmpleosSSGrid.prototype.getLocalTextPrefix = function () { return Roezec.EmpleosSSRow.localTextPrefix; };
            EmpleosSSGrid.prototype.getService = function () { return Roezec.EmpleosSSService.baseUrl; };
            // Agrupar y sumar 
            EmpleosSSGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                // sumamos Objetivo Empleo y de Inversión
                this.view.setSummaryOptions({
                    aggregators: [
                        new Slick.Aggregators.Sum(Roezec.EmpleosSSRow.Fields.Empleos)
                    ]
                });
                return grid;
            };
            // Mostramos Footer con los totales
            EmpleosSSGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                opt.showFooterRow = true;
                return opt;
            };
            EmpleosSSGrid.prototype.usePager = function () {
                return false;
            };
            // Botones Excel y Pdf
            EmpleosSSGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Roezec/EmpleosSS/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.push({
                    title: 'Agrupar por Año Exped.',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Año: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: Roezec.EmpleosSSRow.Fields.AnyoExpediente
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Año',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Año: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: Roezec.EmpleosSSRow.Fields.Anyo
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Isla',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Isla: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: Roezec.EmpleosSSRow.Fields.Isla
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Año e Isla',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Año: ' + x.value + ' (' + x.count + ' Empresas/año)'; },
                            getter: Roezec.EmpleosSSRow.Fields.Anyo
                        }, {
                            formatter: function (x) { return 'Isla: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: Roezec.EmpleosSSRow.Fields.Isla
                        }]); }
                });
                buttons.push({
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: function () { return _this.view.setGrouping([]); }
                });
                return buttons;
                // Fin añadidos
            };
            EmpleosSSGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], EmpleosSSGrid);
            return EmpleosSSGrid;
        }(Serenity.EntityGrid));
        Roezec.EmpleosSSGrid = EmpleosSSGrid;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var InscritasDialog = /** @class */ (function (_super) {
            __extends(InscritasDialog, _super);
            function InscritasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Roezec.InscritasForm(_this.idPrefix);
                return _this;
            }
            InscritasDialog.prototype.getFormKey = function () { return Roezec.InscritasForm.formKey; };
            InscritasDialog.prototype.getIdProperty = function () { return Roezec.InscritasRow.idProperty; };
            InscritasDialog.prototype.getLocalTextPrefix = function () { return Roezec.InscritasRow.localTextPrefix; };
            InscritasDialog.prototype.getNameProperty = function () { return Roezec.InscritasRow.nameProperty; };
            InscritasDialog.prototype.getService = function () { return Roezec.InscritasService.baseUrl; };
            InscritasDialog.prototype.getDeletePermission = function () { return Roezec.InscritasRow.deletePermission; };
            InscritasDialog.prototype.getInsertPermission = function () { return Roezec.InscritasRow.insertPermission; };
            InscritasDialog.prototype.getUpdatePermission = function () { return Roezec.InscritasRow.updatePermission; };
            InscritasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], InscritasDialog);
            return InscritasDialog;
        }(Serenity.EntityDialog));
        Roezec.InscritasDialog = InscritasDialog;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var InscritasGrid = /** @class */ (function (_super) {
            __extends(InscritasGrid, _super);
            function InscritasGrid(container) {
                return _super.call(this, container) || this;
            }
            InscritasGrid.prototype.getColumnsKey = function () { return 'Roezec.Inscritas'; };
            InscritasGrid.prototype.getDialogType = function () { return Roezec.InscritasDialog; };
            InscritasGrid.prototype.getIdProperty = function () { return Roezec.InscritasRow.idProperty; };
            InscritasGrid.prototype.getInsertPermission = function () { return Roezec.InscritasRow.insertPermission; };
            InscritasGrid.prototype.getLocalTextPrefix = function () { return Roezec.InscritasRow.localTextPrefix; };
            InscritasGrid.prototype.getService = function () { return Roezec.InscritasService.baseUrl; };
            // Botones Excel y Pdf
            InscritasGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Roezec/RoezecEmpresas/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
            };
            InscritasGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], InscritasGrid);
            return InscritasGrid;
        }(Serenity.EntityGrid));
        Roezec.InscritasGrid = InscritasGrid;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var NacesDialog = /** @class */ (function (_super) {
            __extends(NacesDialog, _super);
            function NacesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Roezec.NacesForm(_this.idPrefix);
                return _this;
            }
            NacesDialog.prototype.getFormKey = function () { return Roezec.NacesForm.formKey; };
            NacesDialog.prototype.getIdProperty = function () { return Roezec.NacesRow.idProperty; };
            NacesDialog.prototype.getLocalTextPrefix = function () { return Roezec.NacesRow.localTextPrefix; };
            NacesDialog.prototype.getNameProperty = function () { return Roezec.NacesRow.nameProperty; };
            NacesDialog.prototype.getService = function () { return Roezec.NacesService.baseUrl; };
            NacesDialog.prototype.getDeletePermission = function () { return Roezec.NacesRow.deletePermission; };
            NacesDialog.prototype.getInsertPermission = function () { return Roezec.NacesRow.insertPermission; };
            NacesDialog.prototype.getUpdatePermission = function () { return Roezec.NacesRow.updatePermission; };
            NacesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], NacesDialog);
            return NacesDialog;
        }(Serenity.EntityDialog));
        Roezec.NacesDialog = NacesDialog;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var NacesGrid = /** @class */ (function (_super) {
            __extends(NacesGrid, _super);
            function NacesGrid(container) {
                return _super.call(this, container) || this;
            }
            NacesGrid.prototype.getColumnsKey = function () { return 'Roezec.Naces'; };
            NacesGrid.prototype.getDialogType = function () { return Roezec.NacesDialog; };
            NacesGrid.prototype.getIdProperty = function () { return Roezec.NacesRow.idProperty; };
            NacesGrid.prototype.getInsertPermission = function () { return Roezec.NacesRow.insertPermission; };
            NacesGrid.prototype.getLocalTextPrefix = function () { return Roezec.NacesRow.localTextPrefix; };
            NacesGrid.prototype.getService = function () { return Roezec.NacesService.baseUrl; };
            // Botones Excel y Pdf
            NacesGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Roezec/Naces/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            NacesGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], NacesGrid);
            return NacesGrid;
        }(Serenity.EntityGrid));
        Roezec.NacesGrid = NacesGrid;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RepresentantesDialog = /** @class */ (function (_super) {
            __extends(RepresentantesDialog, _super);
            function RepresentantesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Roezec.RepresentantesForm(_this.idPrefix);
                return _this;
            }
            RepresentantesDialog.prototype.getFormKey = function () { return Roezec.RepresentantesForm.formKey; };
            RepresentantesDialog.prototype.getIdProperty = function () { return Roezec.RepresentantesRow.idProperty; };
            RepresentantesDialog.prototype.getLocalTextPrefix = function () { return Roezec.RepresentantesRow.localTextPrefix; };
            RepresentantesDialog.prototype.getNameProperty = function () { return Roezec.RepresentantesRow.nameProperty; };
            RepresentantesDialog.prototype.getService = function () { return Roezec.RepresentantesService.baseUrl; };
            RepresentantesDialog.prototype.getDeletePermission = function () { return Roezec.RepresentantesRow.deletePermission; };
            RepresentantesDialog.prototype.getInsertPermission = function () { return Roezec.RepresentantesRow.insertPermission; };
            RepresentantesDialog.prototype.getUpdatePermission = function () { return Roezec.RepresentantesRow.updatePermission; };
            RepresentantesDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RepresentantesDialog);
            return RepresentantesDialog;
        }(Serenity.EntityDialog));
        Roezec.RepresentantesDialog = RepresentantesDialog;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RepresentantesGrid = /** @class */ (function (_super) {
            __extends(RepresentantesGrid, _super);
            function RepresentantesGrid(container) {
                return _super.call(this, container) || this;
            }
            RepresentantesGrid.prototype.getColumnsKey = function () { return 'Roezec.Representantes'; };
            RepresentantesGrid.prototype.getDialogType = function () { return Roezec.RepresentantesDialog; };
            RepresentantesGrid.prototype.getIdProperty = function () { return Roezec.RepresentantesRow.idProperty; };
            RepresentantesGrid.prototype.getInsertPermission = function () { return Roezec.RepresentantesRow.insertPermission; };
            RepresentantesGrid.prototype.getLocalTextPrefix = function () { return Roezec.RepresentantesRow.localTextPrefix; };
            RepresentantesGrid.prototype.getService = function () { return Roezec.RepresentantesService.baseUrl; };
            // Botones Excel y Pdf
            RepresentantesGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Roezec/Representantes/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            RepresentantesGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], RepresentantesGrid);
            return RepresentantesGrid;
        }(Serenity.EntityGrid));
        Roezec.RepresentantesGrid = RepresentantesGrid;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasDialog = /** @class */ (function (_super) {
            __extends(RoezecEmpresasDialog, _super);
            function RoezecEmpresasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Roezec.RoezecEmpresasForm(_this.idPrefix);
                return _this;
            }
            RoezecEmpresasDialog.prototype.getFormKey = function () { return Roezec.RoezecEmpresasForm.formKey; };
            RoezecEmpresasDialog.prototype.getIdProperty = function () { return Roezec.RoezecEmpresasRow.idProperty; };
            RoezecEmpresasDialog.prototype.getLocalTextPrefix = function () { return Roezec.RoezecEmpresasRow.localTextPrefix; };
            RoezecEmpresasDialog.prototype.getNameProperty = function () { return Roezec.RoezecEmpresasRow.nameProperty; };
            RoezecEmpresasDialog.prototype.getService = function () { return Roezec.RoezecEmpresasService.baseUrl; };
            RoezecEmpresasDialog.prototype.getDeletePermission = function () { return Roezec.RoezecEmpresasRow.deletePermission; };
            RoezecEmpresasDialog.prototype.getInsertPermission = function () { return Roezec.RoezecEmpresasRow.insertPermission; };
            RoezecEmpresasDialog.prototype.getUpdatePermission = function () { return Roezec.RoezecEmpresasRow.updatePermission; };
            RoezecEmpresasDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RoezecEmpresasDialog);
            return RoezecEmpresasDialog;
        }(Serenity.EntityDialog));
        Roezec.RoezecEmpresasDialog = RoezecEmpresasDialog;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasGrid = /** @class */ (function (_super) {
            __extends(RoezecEmpresasGrid, _super);
            function RoezecEmpresasGrid(container) {
                return _super.call(this, container) || this;
            }
            RoezecEmpresasGrid.prototype.getColumnsKey = function () { return 'Roezec.RoezecEmpresas'; };
            RoezecEmpresasGrid.prototype.getDialogType = function () { return Roezec.RoezecEmpresasDialog; };
            RoezecEmpresasGrid.prototype.getIdProperty = function () { return Roezec.RoezecEmpresasRow.idProperty; };
            RoezecEmpresasGrid.prototype.getInsertPermission = function () { return Roezec.RoezecEmpresasRow.insertPermission; };
            RoezecEmpresasGrid.prototype.getLocalTextPrefix = function () { return Roezec.RoezecEmpresasRow.localTextPrefix; };
            RoezecEmpresasGrid.prototype.getService = function () { return Roezec.RoezecEmpresasService.baseUrl; };
            // Agrupar y sumar 
            RoezecEmpresasGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                // sumamos Objetivo Empleo y de Inversión
                this.view.setSummaryOptions({
                    aggregators: [
                        new Slick.Aggregators.Sum("ObjetivoEmpleo" /* ObjetivoEmpleo */),
                        new Slick.Aggregators.Sum("ObjetivoInversion" /* ObjetivoInversion */)
                    ]
                });
                return grid;
            };
            // Mostramos Footer con los totales
            RoezecEmpresasGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                opt.showFooterRow = true;
                return opt;
            };
            RoezecEmpresasGrid.prototype.usePager = function () {
                return true;
            };
            // Botones Excel y Pdf
            RoezecEmpresasGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Roezec/RoezecEmpresas/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.push({
                    title: 'Agrupar por Año',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Año: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: "AnyoExpediente" /* AnyoExpediente */
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Año y Técnico',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Año: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: "AnyoExpediente" /* AnyoExpediente */
                        }, {
                            formatter: function (x) { return 'Técnico: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: "Tecnico" /* Tecnico */
                        }]); }
                });
                buttons.push({
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: function () { return _this.view.setGrouping([]); }
                });
                return buttons;
                // Fin añadidos
            };
            RoezecEmpresasGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], RoezecEmpresasGrid);
            return RoezecEmpresasGrid;
        }(Serenity.EntityGrid));
        Roezec.RoezecEmpresasGrid = RoezecEmpresasGrid;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasSSDialog = /** @class */ (function (_super) {
            __extends(RoezecEmpresasSSDialog, _super);
            function RoezecEmpresasSSDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Roezec.RoezecEmpresasSSForm(_this.idPrefix);
                return _this;
            }
            RoezecEmpresasSSDialog.prototype.getFormKey = function () { return Roezec.RoezecEmpresasSSForm.formKey; };
            RoezecEmpresasSSDialog.prototype.getIdProperty = function () { return Roezec.RoezecEmpresasSSRow.idProperty; };
            RoezecEmpresasSSDialog.prototype.getLocalTextPrefix = function () { return Roezec.RoezecEmpresasSSRow.localTextPrefix; };
            RoezecEmpresasSSDialog.prototype.getNameProperty = function () { return Roezec.RoezecEmpresasSSRow.nameProperty; };
            RoezecEmpresasSSDialog.prototype.getService = function () { return Roezec.RoezecEmpresasSSService.baseUrl; };
            RoezecEmpresasSSDialog.prototype.getDeletePermission = function () { return Roezec.RoezecEmpresasSSRow.deletePermission; };
            RoezecEmpresasSSDialog.prototype.getInsertPermission = function () { return Roezec.RoezecEmpresasSSRow.insertPermission; };
            RoezecEmpresasSSDialog.prototype.getUpdatePermission = function () { return Roezec.RoezecEmpresasSSRow.updatePermission; };
            RoezecEmpresasSSDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RoezecEmpresasSSDialog);
            return RoezecEmpresasSSDialog;
        }(Serenity.EntityDialog));
        Roezec.RoezecEmpresasSSDialog = RoezecEmpresasSSDialog;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEmpresasSSGrid = /** @class */ (function (_super) {
            __extends(RoezecEmpresasSSGrid, _super);
            function RoezecEmpresasSSGrid(container) {
                return _super.call(this, container) || this;
            }
            RoezecEmpresasSSGrid.prototype.getColumnsKey = function () { return 'Roezec.RoezecEmpresasSS'; };
            RoezecEmpresasSSGrid.prototype.getDialogType = function () { return Roezec.RoezecEmpresasSSDialog; };
            RoezecEmpresasSSGrid.prototype.getIdProperty = function () { return Roezec.RoezecEmpresasSSRow.idProperty; };
            RoezecEmpresasSSGrid.prototype.getInsertPermission = function () { return Roezec.RoezecEmpresasSSRow.insertPermission; };
            RoezecEmpresasSSGrid.prototype.getLocalTextPrefix = function () { return Roezec.RoezecEmpresasSSRow.localTextPrefix; };
            RoezecEmpresasSSGrid.prototype.getService = function () { return Roezec.RoezecEmpresasSSService.baseUrl; };
            // Agrupar y sumar 
            RoezecEmpresasSSGrid.prototype.createSlickGrid = function () {
                var grid = _super.prototype.createSlickGrid.call(this);
                // need to register this plugin for grouping or you'll have errors
                grid.registerPlugin(new Slick.Data.GroupItemMetadataProvider());
                // sumamos Objetivo Empleo y de Inversión
                this.view.setSummaryOptions({
                    aggregators: [
                        new Slick.Aggregators.Sum(Roezec.RoezecEmpresasSSRow.Fields.ObjetivoEmpleo),
                        new Slick.Aggregators.Sum(Roezec.RoezecEmpresasSSRow.Fields.ObjetivoInversion),
                        new Slick.Aggregators.Sum(Roezec.RoezecEmpresasSSRow.Fields.Empleos2021),
                        new Slick.Aggregators.Sum(Roezec.RoezecEmpresasSSRow.Fields.Empleos2020),
                        new Slick.Aggregators.Sum(Roezec.RoezecEmpresasSSRow.Fields.Empleos2019),
                        new Slick.Aggregators.Sum(Roezec.RoezecEmpresasSSRow.Fields.Empleos2018),
                        new Slick.Aggregators.Sum(Roezec.RoezecEmpresasSSRow.Fields.Empleos2017)
                    ]
                });
                return grid;
            };
            // Mostramos Footer con los totales
            RoezecEmpresasSSGrid.prototype.getSlickOptions = function () {
                var opt = _super.prototype.getSlickOptions.call(this);
                opt.showFooterRow = true;
                return opt;
            };
            RoezecEmpresasSSGrid.prototype.usePager = function () {
                return false;
            };
            // Botones Excel y Pdf
            RoezecEmpresasSSGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Roezec/RoezecEmpresasSS/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                buttons.push({
                    title: 'Agrupar por Año',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Año: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: Roezec.RoezecEmpresasSSRow.Fields.AnyoExpediente
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Nace',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Nace: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: Roezec.RoezecEmpresasSSRow.Fields.NacePrincipal
                        }]); }
                });
                buttons.push({
                    title: 'Agrupar por Año y Técnico',
                    cssClass: 'expand-all-button',
                    onClick: function () { return _this.view.setGrouping([{
                            formatter: function (x) { return 'Año: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: Roezec.RoezecEmpresasSSRow.Fields.AnyoExpediente
                        }, {
                            formatter: function (x) { return 'Técnico: ' + x.value + ' (' + x.count + ' Empresas)'; },
                            getter: Roezec.RoezecEmpresasSSRow.Fields.Tecnico
                        }]); }
                });
                buttons.push({
                    title: 'Desagrupar',
                    cssClass: 'collapse-all-button',
                    onClick: function () { return _this.view.setGrouping([]); }
                });
                return buttons;
                // Fin añadidos
            };
            RoezecEmpresasSSGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], RoezecEmpresasSSGrid);
            return RoezecEmpresasSSGrid;
        }(Serenity.EntityGrid));
        Roezec.RoezecEmpresasSSGrid = RoezecEmpresasSSGrid;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEstadosDialog = /** @class */ (function (_super) {
            __extends(RoezecEstadosDialog, _super);
            function RoezecEstadosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Roezec.RoezecEstadosForm(_this.idPrefix);
                return _this;
            }
            RoezecEstadosDialog.prototype.getFormKey = function () { return Roezec.RoezecEstadosForm.formKey; };
            RoezecEstadosDialog.prototype.getIdProperty = function () { return Roezec.RoezecEstadosRow.idProperty; };
            RoezecEstadosDialog.prototype.getLocalTextPrefix = function () { return Roezec.RoezecEstadosRow.localTextPrefix; };
            RoezecEstadosDialog.prototype.getNameProperty = function () { return Roezec.RoezecEstadosRow.nameProperty; };
            RoezecEstadosDialog.prototype.getService = function () { return Roezec.RoezecEstadosService.baseUrl; };
            RoezecEstadosDialog.prototype.getDeletePermission = function () { return Roezec.RoezecEstadosRow.deletePermission; };
            RoezecEstadosDialog.prototype.getInsertPermission = function () { return Roezec.RoezecEstadosRow.insertPermission; };
            RoezecEstadosDialog.prototype.getUpdatePermission = function () { return Roezec.RoezecEstadosRow.updatePermission; };
            RoezecEstadosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RoezecEstadosDialog);
            return RoezecEstadosDialog;
        }(Serenity.EntityDialog));
        Roezec.RoezecEstadosDialog = RoezecEstadosDialog;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var RoezecEstadosGrid = /** @class */ (function (_super) {
            __extends(RoezecEstadosGrid, _super);
            function RoezecEstadosGrid(container) {
                return _super.call(this, container) || this;
            }
            RoezecEstadosGrid.prototype.getColumnsKey = function () { return 'Roezec.RoezecEstados'; };
            RoezecEstadosGrid.prototype.getDialogType = function () { return Roezec.RoezecEstadosDialog; };
            RoezecEstadosGrid.prototype.getIdProperty = function () { return Roezec.RoezecEstadosRow.idProperty; };
            RoezecEstadosGrid.prototype.getInsertPermission = function () { return Roezec.RoezecEstadosRow.insertPermission; };
            RoezecEstadosGrid.prototype.getLocalTextPrefix = function () { return Roezec.RoezecEstadosRow.localTextPrefix; };
            RoezecEstadosGrid.prototype.getService = function () { return Roezec.RoezecEstadosService.baseUrl; };
            RoezecEstadosGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], RoezecEstadosGrid);
            return RoezecEstadosGrid;
        }(Serenity.EntityGrid));
        Roezec.RoezecEstadosGrid = RoezecEstadosGrid;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var SociosDialog = /** @class */ (function (_super) {
            __extends(SociosDialog, _super);
            function SociosDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Roezec.SociosForm(_this.idPrefix);
                return _this;
            }
            SociosDialog.prototype.getFormKey = function () { return Roezec.SociosForm.formKey; };
            SociosDialog.prototype.getIdProperty = function () { return Roezec.SociosRow.idProperty; };
            SociosDialog.prototype.getLocalTextPrefix = function () { return Roezec.SociosRow.localTextPrefix; };
            SociosDialog.prototype.getNameProperty = function () { return Roezec.SociosRow.nameProperty; };
            SociosDialog.prototype.getService = function () { return Roezec.SociosService.baseUrl; };
            SociosDialog.prototype.getDeletePermission = function () { return Roezec.SociosRow.deletePermission; };
            SociosDialog.prototype.getInsertPermission = function () { return Roezec.SociosRow.insertPermission; };
            SociosDialog.prototype.getUpdatePermission = function () { return Roezec.SociosRow.updatePermission; };
            SociosDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], SociosDialog);
            return SociosDialog;
        }(Serenity.EntityDialog));
        Roezec.SociosDialog = SociosDialog;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
var ProyectosZec;
(function (ProyectosZec) {
    var Roezec;
    (function (Roezec) {
        var SociosGrid = /** @class */ (function (_super) {
            __extends(SociosGrid, _super);
            function SociosGrid(container) {
                return _super.call(this, container) || this;
            }
            SociosGrid.prototype.getColumnsKey = function () { return 'Roezec.Socios'; };
            SociosGrid.prototype.getDialogType = function () { return Roezec.SociosDialog; };
            SociosGrid.prototype.getIdProperty = function () { return Roezec.SociosRow.idProperty; };
            SociosGrid.prototype.getInsertPermission = function () { return Roezec.SociosRow.insertPermission; };
            SociosGrid.prototype.getLocalTextPrefix = function () { return Roezec.SociosRow.localTextPrefix; };
            SociosGrid.prototype.getService = function () { return Roezec.SociosService.baseUrl; };
            // Botones Excel y Pdf
            SociosGrid.prototype.getButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getButtons.call(this);
                buttons.push(ProyectosZec.Common.ExcelExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); },
                    service: 'Roezec/Socios/ListExcel',
                    separator: true
                }));
                buttons.push(ProyectosZec.Common.PdfExportHelper.createToolButton({
                    grid: this,
                    onViewSubmit: function () { return _this.onViewSubmit(); }
                }));
                return buttons;
                // Fin añadidos
            };
            SociosGrid = __decorate([
                Serenity.Decorators.registerClass()
                // Añadido para los filtros multiples
                ,
                Serenity.Decorators.filterable()
                // Fin Añadido
            ], SociosGrid);
            return SociosGrid;
        }(Serenity.EntityGrid));
        Roezec.SociosGrid = SociosGrid;
    })(Roezec = ProyectosZec.Roezec || (ProyectosZec.Roezec = {}));
})(ProyectosZec || (ProyectosZec = {}));
//# sourceMappingURL=ProyectosZec.Web.js.map