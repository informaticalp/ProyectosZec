﻿/************************************************************************************************************
 * Creación tabla de EmpleosEmpresas
 *
 * AUTOR : Javier Núñez
 * FECHA : Marzo 2022
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20220315140000)]
    public class RoezecDB_20220308_140000_Empleos_Empresas : Migration
    {
        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {
            /* Empresas_empleos*/
            Create.Table("empresas_empleos").InSchema("Roezec")
                .WithColumn("EmpresasEmpleosId").AsInt32().Identity().PrimaryKey()
                .WithColumn("EmpresaId").AsInt32().NotNullable()
                .ForeignKey("EmpresasEmpleos_ibfk_1","Empresas","EmpresaId")
                .WithColumn("Any").AsInt32().NotNullable()
                .WithColumn("Empleos").AsDecimal(8,2).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            Create.Index("Empresas_empleos_Idx1").OnTable("empresas_empleos").InSchema("Roezec")
                .OnColumn("EmpresaId")
                .Ascending()
                .OnColumn("Any")
                .Ascending()
                .WithOptions()
                .Unique();
        }


        public override void Down()
        {
        }
    }
}