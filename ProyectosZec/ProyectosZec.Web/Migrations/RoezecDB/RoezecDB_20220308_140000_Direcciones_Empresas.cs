﻿/************************************************************************************************************
 * Creación tabla de Direcciones 
 *
 * AUTOR : Javier Núñez
 * FECHA : Octubre 2021
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20220308140000)]
    public class RoezecDB_20220308_140000_DireccionesEmpresas : Migration
    {
        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {
            /* EmpresasDirecciones*/
            Create.Table("empresas_direcciones").InSchema("Roezec")
                .WithColumn("EmpresasDireccionesId").AsInt32().Identity().PrimaryKey()
                .WithColumn("EmpresaId").AsInt32().NotNullable()
                .ForeignKey("EmpresasDirecciones_ibfk_1","Empresas","EmpresaId")
                .WithColumn("Direccion").AsString(150).NotNullable()
                .WithColumn("Poblacion").AsString(100).NotNullable()
                .WithColumn("CP").AsString(5).Nullable()
                .WithColumn("PaisId").AsInt32().NotNullable()
                .WithColumn("IslaId").AsInt32().Nullable()
                .ForeignKey("DireccionesIslas_ibfk_2", "Islas", "IslaId")
                .WithColumn("Desde").AsDate().Nullable()
                .WithColumn("Hasta").AsDate().Nullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();          
        }

        public override void Down()
        {
        }
    }
}