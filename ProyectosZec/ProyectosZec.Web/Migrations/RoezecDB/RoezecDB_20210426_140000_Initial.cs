﻿/************************************************************************************************************
 * Creación Incial de las tablas auxiliares y la tabla de Proyectos y CRM
 *
 * Tablas:
 *      - Contactos
 *      - Continentes
 *      - Paises
 *      - tipos de acciones promocionales
 *      - acciones promocionales
 *      - Tecnicos
 *      - Islas
 *      - PrescriptorInversor
 *      - Sectores
 *      - Subsectores
 *      - Naces
 *      - Origenes_Proyecto
 *      - Capital
 *      
 * 
 * AUTOR : Javier Núñez
 * FECHA : ABRIL 2021
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20210426140000)]
    public class RoezecDB_20210426_140000_Initial : Migration
    {

        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {

            //IfDatabase("MySql")
            //    //Si es Mysql hay que hacer un drop de la BD porque si no, da un errorjavascript 
            //    .Execute.Sql(GetScript("Nuevo_Roezec.Web.Migrations.RoezecDB.DropDatabaseRoezecDBScript_MySql.sql"));
            

            // Asegurarse que la base de datos no existe y hacer un drop antes
            Create.Schema("Roezec");

            /* Idiomas */
            Create.Table("Idiomas").InSchema("Roezec")
                .WithColumn("IdiomaId").AsInt32().PrimaryKey().Identity().NotNullable()
                .WithColumn("Idioma").AsString(5)
                .WithColumn("NombreIdioma").AsString(30);
            /* Contactos */            

            Create.Table("Contactos").InSchema("Roezec")
                .WithColumn("ContactoId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Nombre").AsString(255).NotNullable()
                .WithColumn("Apellidos").AsString(45).NotNullable()
                .WithColumn("NIF").AsString(20).Nullable()
                .WithColumn("Telefono_fijo").AsString(20).Nullable()
                .WithColumn("Movil").AsString(20).Nullable()
                .WithColumn("IdiomaId").AsInt32().Nullable()
                .ForeignKey("Idiomas","IdiomaId")
                .WithColumn("Email").AsString(75).Nullable();

            /* Tabla Continentes */
            Create.Table("Continentes").InSchema("Roezec")
                .WithColumn("ContinenteId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Continente").AsString(30).NotNullable();

            /* Tabla Paises */

            Create.Table("Paises").InSchema("Roezec")
                .WithColumn("PaisId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Pais").AsString(50)
                .WithColumn("Capital").AsString(50)
                .WithColumn("ContinenteId").AsInt32().NotNullable()
                .ForeignKey("Continentes", "ContinenteId");


            /* Tabla Tecnicos. */

            Create.Table("Tecnicos").InSchema("Roezec")
                .WithColumn("TecnicoId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("NombreTecnico").AsString(30).NotNullable()
                .WithColumn("Tecnico").AsString(4).NotNullable().Unique();

            /* Tabla Islas */

            Create.Table("Islas").InSchema("Roezec")
            .WithColumn("IslaId").AsInt32().Identity().PrimaryKey().NotNullable()
            .WithColumn("NombreIsla").AsString(20).NotNullable()
            .WithColumn("Isla").AsString(3).NotNullable();


            /* Tabla PrescriptorInversor */

            Create.Table("PrescriptorInversor").InSchema("Roezec")
                .WithColumn("PrescriptorInversorId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("PrescriptorInversor").AsString(20).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();


            ///* Tabla Eventos de Calendario 
            //    Basado el Calendario https://www.cssscript.com/demo/feature-rich-event-calendar/
            //La definición de los eventos está en:
            //https://github.com/williamtroup/Calendar.js/blob/main/EVENT.md */

            //Create.Table("Eventos").InSchema("Roezec")
            //    .WithColumn("EventId").AsInt32().Identity().PrimaryKey().NotNullable()
            //    .WithColumn("Title").AsString(100).NotNullable() /* Titulo */
            //    .WithColumn("From").AsDateTime().NotNullable() /* Desde */
            //    .WithColumn("To").AsDateTime().NotNullable()   /* Hasta */
            //    .WithColumn("IsAllDay").AsBoolean().Nullable() /* True si es todo el dia de 00:00 a 23:59 */
            //    .WithColumn("Location").AsString(255).Nullable() /* Lugar */
            //    .WithColumn("Description").AsString(255).Nullable() /* The in-depth description of the event. */
            //    .WithColumn("Color").AsString(50).Nullable()  /* The color that should be used for the event (overrides all others). */
            //    .WithColumn("ColorText").AsString(50).Nullable() /* The color that should be used for the event text (overrides all others). */
            //    .WithColumn("ColorBorder").AsString(50).Nullable() /* The color that should be used for the event border (overrides all others). */
            //    .WithColumn("RepeatEery").AsInt32().WithDefaultValue(0) /* States how often the event should repeat (0 = Never, 1 = Every Day, 2 = Every Week, 3 = Every Month, 4 = Every Year). */
            //    .WithColumn("TecnicoId").AsInt32().Nullable()               
            //    .ForeignKey("Tecnicos","TecnicoId")
            //    .WithColumn("OrganizerName").AsString(70).Nullable()
            //    .WithColumn("Created").AsDateTime().Nullable()
            //    .WithColumn("ContactoId").AsInt32().Nullable()
            //    .ForeignKey("Contactos","ContactoId"); 

            /* Tabla Sectores */

            Create.Table("Sectores").InSchema("Roezec")
                .WithColumn("SectorId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Sector").AsString(50).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            /* Tabla Subsectores */
            Create.Table("Subsectores").InSchema("Roezec")
                .WithColumn("SubsectorId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("SectorId").AsInt32().NotNullable()
                .ForeignKey("Sectores", "SectorId")
                .WithColumn("Subsector").AsString(50).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            /* Tabla Capital */

            Create.Table("Capital").InSchema("Roezec")
                .WithColumn("CapitalId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Capital").AsString(20).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            /* Tabla Versiones Nace */
            Create.Table("Versiones_Nace").InSchema("Roezec")
                .WithColumn("VersionId").AsInt32().NotNullable().Identity().PrimaryKey()
                .WithColumn("Version").AsString(20).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            /* Tabla de Naces o Actividades */

            Create.Table("Naces").InSchema("Roezec")
                .WithColumn("NaceId").AsInt32().NotNullable().Identity().PrimaryKey()
                .WithColumn("VersionId").AsInt32().NotNullable()
                .ForeignKey("FK_versiones_Nace","Versiones_Nace","VersionId")
                .WithColumn("Codigo").AsString(5).Nullable()
                .WithColumn("Descripcion").AsString(150).NotNullable()
                .WithColumn("SubsectorId").AsInt32().Nullable()
                .ForeignKey("Subsectores", "SubsectorId")
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            Create.Index("CodigoNace").OnTable("Naces").OnColumn("Codigo");

 
            /**** Insertamos los registros de las tablas comunes ***/

            Insert.IntoTable("Idiomas").Row(new
            {
                Idioma = "es",
                NombreIdioma = "Español"
            });

            Insert.IntoTable("Idiomas").Row(new
            {
                Idioma = "en",
                NombreIdioma = "Inglés"
            });

            Insert.IntoTable("Idiomas").Row(new
            {
                Idioma = "de",
                NombreIdioma = "Alemán"
            });

            Insert.IntoTable("Idiomas").Row(new
            {
                Idioma = "it",
                NombreIdioma = "Italiano"
            });

            Insert.IntoTable("Idiomas").Row(new
            {
                Idioma = "fr",
                NombreIdioma = "Frances"
            });

            Insert.IntoTable("Idiomas").Row(new
            {
                Idioma = "pt",
                NombreIdioma = "Portugués"
            });

            Insert.IntoTable("Idiomas").Row(new
            {
                Idioma = "ru",
                NombreIdioma = "Ruso"
            });

            Insert.IntoTable("Idiomas").Row(new
            {
                Idioma = "zh",
                NombreIdioma = "Chino"
            });


            Insert.IntoTable("Continentes").Row(new
            {
                Continente = "Europa"
            });

            Insert.IntoTable("Continentes").Row(new
            {
                Continente = "América"
            });

            Insert.IntoTable("Continentes").Row(new
            {
                Continente = "Asia"
            });

            Insert.IntoTable("Continentes").Row(new
            {
                Continente = "Oceanía"
            });

            Insert.IntoTable("Continentes").Row(new
            {
                Continente = "Africa"
            });

            Insert.IntoTable("Tecnicos").Row(new
            {
                Tecnico = "CRM",
                NombreTecnico = "Carmen Reguero Marrero"
            });

            Insert.IntoTable("Tecnicos").Row(new
            {
                Tecnico = "DDM",
                NombreTecnico = "Delia Domínguez Montenegro"
            });

            Insert.IntoTable("Tecnicos").Row(new
            {
                Tecnico = "MDL",
                NombreTecnico = "'Monica Doreste Leander'"
            });

            Insert.IntoTable("Tecnicos").Row(new
            {
                Tecnico = "ESM",
                NombreTecnico = "Eva Sainero Martín"
            });

            Insert.IntoTable("Tecnicos").Row(new
            {
                Tecnico = "ISH",
                NombreTecnico = "Isabel Castro Hernández"
            });

            Insert.IntoTable("Tecnicos").Row(new
            {
                Tecnico = "SJJ",
                NombreTecnico = "Sabita Jagtani Jagtani"
            });

            Insert.IntoTable("Tecnicos").Row(new
            {
                Tecnico = "OMP",
                NombreTecnico = "Oga Martín Pascual"
            });


            Insert.IntoTable("Tecnicos").Row(new
            {
                Tecnico = "OTRO",
                NombreTecnico = "Otro"
            });

            Insert.IntoTable("PrescriptorInversor").Row(new
            {
                PrescriptorInversor = "PRESCRIPTOR"
            });

            Insert.IntoTable("PrescriptorInversor").Row(new
            {
                PrescriptorInversor = "INVESOR"
            });

            Insert.IntoTable("Islas").Row(new
            {
                Isla = "GC",
                NombreIsla = "GRAN CANARIA"
            });

            Insert.IntoTable("Islas").Row(new
            {
                Isla = "TF",
                NombreIsla = "TENERIFE"
            });

            Insert.IntoTable("Islas").Row(new
            {
                Isla = "FTV",
                NombreIsla = "FUERTEVENTURA"
            });

            Insert.IntoTable("Islas").Row(new
            {
                Isla = "LZ",
                NombreIsla = "LANZAROTE"
            });

            Insert.IntoTable("Islas").Row(new
            {
                Isla = "LP",
                NombreIsla = "LA PALMA"
            });

            Insert.IntoTable("Islas").Row(new
            {
                Isla = "GM",
                NombreIsla = "LA GOMERA"
            });

            Insert.IntoTable("Islas").Row(new
            {
                Isla = "EH",
                NombreIsla = "EL HIERRO"
            });

            Insert.IntoTable("Capital").Row(new
            {
                Capital = "EXTERIOR"
            });

            Insert.IntoTable("Capital").Row(new
            {
                Capital = "MIXTO"
            });

            Insert.IntoTable("Capital").Row(new
            {
                Capital = "LOCAL"
            });

            Insert.IntoTable("Sectores").Row(new
            {
                Sector = "INDUSTRIA"
            });

            Insert.IntoTable("Sectores").Row(new
            {
                Sector = "SERVICIOS"
            });

            Insert.IntoTable("Sectores").Row(new
            {
                Sector = "COMERCIO"
            });


            Insert.IntoTable("Versiones_Nace").Row(new
            {
                Version = "Rev. 1"
            });

            Insert.IntoTable("Versiones_Nace").Row(new
            {
                Version = "Rev. 1.1"
            });

            Insert.IntoTable("Versiones_Nace").Row(new
            {
                Version = "Rev. 2"
            });

            IfDatabase("MySql")
                //Carga de datos de paises
                .Execute.Script("ProyectosZec.Migrations.RoezecDB.RoezecDB_Script_Mysql.sql");

            //IfDatabase("MySql")
            //    //Carga de datos de paises
            //    .Execute.Script("ProyectosZec.Migrations.RoezecDB.RoezecDB_paises_DataScript_Mysql");

            //IfDatabase("MySql")
            //     //Carga de datos de Naces 
            //     .Execute.EmbeddedScript("ProyectosZec.Migrations.RoezecDB.RoezecDB_Nace_dataScript_Mysql.sql");
            
        }
        public override void Down()
        {
        }
    }
}