﻿/************************************************************************************************************
 * Creación tabla de Alarmas Roezec
 *
 * AUTOR : Javier Núñez
 * FECHA : Octubre 2021
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20211105140000)]
    public class RoezecDB_20211105_140000_AlarmasProcedimiento : Migration
    {
        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {
            /* AlarmasProcedimientos*/
            Create.Table("AlarmasProcedimientos").InSchema("Roezec")
                .WithColumn("AlarmaProcedimientoId").AsInt32().Identity().PrimaryKey()
                .WithColumn("ProcedimientoId").AsInt32().NotNullable()
                .ForeignKey("alarmasprocedimientos_ibfk_1","Procedimientos","ProcedimientoId")
                .WithColumn("TipoAlarmaId").AsInt32().NotNullable()
                .WithColumn("SentidoResolucionId").AsInt32().NotNullable()
                .ForeignKey("alarmasprocedimientos_ibfk_2", "sentidosresolucion", "SentidoResolucionId")
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();
            
            Alter.Table("Tipos_alarma")
                .AddColumn("AsuntoEmail").AsString(100).Nullable()
                .AddColumn("TextoMensaje").AsString(900).Nullable();
        }


        public override void Down()
        {
        }
    }
}