﻿/************************************************************************************************************
 * Creación tabla de Mercados y Empresas_Mercados
 *
 * AUTOR : Javier Núñez
 * FECHA : Septiempbre 2021
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20210913140000)]
    public class RoezecDB_20210913_140000_Mercados : Migration
    {
        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {
            /* Mercados */

            Create.Table("Mercados").InSchema("Roezec")
                .WithColumn("MercadoId").AsInt32().PrimaryKey().Identity().NotNullable()
                .WithColumn("Mercado").AsString(50)
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();


            /* Empresas_Mercados */
            Create.Table("Empresas_Mercados").InSchema("Roezec")
                .WithColumn("EmpresaMercadoId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("EmpresaId").AsInt32().NotNullable()
                .ForeignKey("FK_EmpresaMercado_Empresa", "Empresas","EmpresaId")
                .WithColumn("MercadoId").AsInt32().NotNullable()
                .ForeignKey("FK_EmpresaMercado_Mercado","Mercados","MercadoId")
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            Create.Index("Empresas_Mercados_Idx1").OnTable("Empresas_Mercados").InSchema("Roezec")
                .OnColumn("EmpresaId")
                .Ascending()
                .OnColumn("MercadoId")
                .Ascending()
                .WithOptions()
                .Unique();


        }

        public override void Down()
        {
        }
    }
}