﻿/************************************************************************************************************
 * Creación tabla de Alarmas Roezec
 *
 * AUTOR : Javier Núñez
 * FECHA : Octubre 2021
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20211020140000)]
    public class RoezecDB_20211020_140000_Alarmas : Migration
    {
        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {
            /* Plazos */
            Create.Table("Plazos").InSchema("Roezec")
                .WithColumn("PlazoId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Plazo").AsString(10).NotNullable();

            /* Tipos Alarma */
            Create.Table("Tipos_Alarma").InSchema("Roezec")
                .WithColumn("TipoAlarmaId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Texto").AsString(100).NotNullable()
                .WithColumn("Dias").AsInt32().NotNullable()
                .WithColumn("Plazo").AsInt32().NotNullable()
                .WithColumn("DiasAviso").AsInt32().NotNullable()
                .WithColumn("Email").AsString(200).Nullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            /* Alarmas */
            Create.Table("Alarmas").InSchema("Roezec")
                .WithColumn("AlarmaId").AsInt32().PrimaryKey().Identity().NotNullable()
                .WithColumn("TipoAlarmaId").AsInt32().NotNullable()
                .ForeignKey("Tipos_Alarma", "TipoAlarmaId")
                .WithColumn("EmpresaId").AsInt32().Nullable()
                .ForeignKey("Empresas", "EmpresaId")
                .WithColumn("Activa").AsBoolean().NotNullable().WithDefaultValue(1)
                .WithColumn("FechaCreacion").AsDateTime().NotNullable()
                .WithColumn("FechaCaducidad").AsDate().NotNullable()
                .WithColumn("FechaAviso").AsDate().NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();



            /* Cambio en Historial Empresas */
            Alter.Table("historial_empresas")
                .AddColumn("AlarmaActivada").AsBoolean().Nullable()
                .AddColumn("EstadoActivado").AsBoolean().Nullable();


            Insert.IntoTable("Plazos").Row(new
            {
                Plazo = "Días"
            });

            Insert.IntoTable("Plazos").Row(new
            {
                Plazo = "Meses"
            });

            Insert.IntoTable("Plazos").Row(new
            {
                Plazo = "Años"
            });
        }


        public override void Down()
        {
        }
    }
}