﻿/************************************************************************************************************
 * Creación tabla de Empresas Zec
 *
 * AUTOR : Javier Núñez
 * FECHA : ABRIL 2021
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20210830140000)]
    public class RoezecDB_20210830_140000_Envios : Migration
    {
        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {
            /* Estados de los Envíos */

            Create.Table("Estados_Envio").InSchema("Roezec")
                .WithColumn("EstadoEnvioId").AsInt32().PrimaryKey().Identity().NotNullable()
                .WithColumn("Estado").AsString(50)
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            /* Tipo de envio (Inicio/Resolucion)*/
            Create.Table("Tipos_Envio").InSchema("Roezec")
                .WithColumn("TipoEnvioId").AsInt32().Identity().PrimaryKey().Nullable()
                .WithColumn("Tipo").AsString(20).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            Create.Table("Formas_Envio").InSchema("Roezec")
                .WithColumn("FormaEnvioId").AsInt32().Identity().PrimaryKey().Nullable()
                .WithColumn("Forma").AsString(20).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            /* Envíos */
            Create.Table("Envios").InSchema("Roezec")
                .WithColumn("EnvioId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("HistorialId").AsInt32().NotNullable()
                .ForeignKey("FK_Envio_Historial","Historial_Empresas","HistorialId")
                .WithColumn("TipoEnvioId").AsInt32().NotNullable()
                .ForeignKey("FK_Envio_TipoEnvio","Tipos_Envio","TipoEnvioId")
                .WithColumn("FormaEnvioId").AsInt32().NotNullable()
                .ForeignKey("Formas_Envio", "FormaEnvioId")
                .WithColumn("EstadoEnvioId").AsInt32().NotNullable()
                .ForeignKey("Estados_Envio", "EstadoEnvioId")
                .WithColumn("FechaEnvio").AsDate().NotNullable()
                .WithColumn("ContactoEnvioId").AsInt32().NotNullable()
                .ForeignKey("FK_Envio_ContactoEnvio","Contactos","ContactoId")
                .WithColumn("FechaRecepcion").AsDate().Nullable()
                .WithColumn("ContactoAcuseId").AsInt32().Nullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            Insert.IntoTable("Formas_Envio")
                .Row(new
                {
                    Forma = "Correos"
                });


            Insert.IntoTable("Formas_Envio")
                .Row(new
                {
                    Forma = "Notifica"
                });

            Insert.IntoTable("Formas_Envio")
                .Row(new
                {
                    Forma = "BOE"
                });



            Insert.IntoTable("Tipos_Envio")
                .Row(new
                {
                    Tipo = "Inicio"
                });


            Insert.IntoTable("Tipos_Envio")
                .Row(new
                {
                    Tipo = "Resolucion"
                });


            Insert.IntoTable("Estados_Envio")
                .Row(new
                {
                    Estado = "Enviado"
                });

            Insert.IntoTable("Estados_Envio")
                .Row(new
                {
                    Estado = "Recibido"
                });

            Insert.IntoTable("Estados_Envio")
                .Row(new
                {
                    Estado = "Devuelto"
                });

            Insert.IntoTable("Estados_Envio")
                .Row(new
                {
                    Estado = "Rechazado"
                });

        }

        public override void Down()
        {
        }
    }
}