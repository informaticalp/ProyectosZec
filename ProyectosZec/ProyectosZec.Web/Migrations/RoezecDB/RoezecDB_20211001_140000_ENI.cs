﻿/************************************************************************************************************
 * Creación tabla de Documentos ENI
 *
 * AUTOR : Javier Núñez
 * FECHA : Octubre 2021
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20211001140000)]
    public class RoezecDB_20211001_140000_ENI : Migration
    {
        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {
            /* Tipos Documento ENI */
            Create.Table("Tipos_Documento").InSchema("Roezec")
                .WithColumn("TipoDocumentoId").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Tipo").AsString(50).NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();
            /* Ficheros ENI */
            Create.Table("Ficheros").InSchema("Roezec")
                .WithColumn("FicheroId").AsInt32().PrimaryKey().Identity().NotNullable()
                .WithColumn("HistorialId").AsInt32().NotNullable()
                .ForeignKey("FK_Hist_historialId", "historial_empresas", "HistorialId")
                .WithColumn("Fichero").AsString(1000).Nullable()
                .WithColumn("NombreNatural").AsString(255).Nullable()
                .WithColumn("Organo").AsString(50).Nullable()
                .WithColumn("FechaCaptura").AsDate().NotNullable()
                .WithColumn("TipoDocumentoId").AsInt32().NotNullable()
                .ForeignKey("Tipos_documento", "TipoDocumentoId").NotNullable()
                .WithColumn("CSV").AsString(200).Nullable()
                .WithColumn("RegulacionCSV").AsString(200).Nullable()
                .WithColumn("Observaciones").AsString(500).Nullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();
            
            /* Metadatos de cada fichero */

            Create.Table("Metadatos").InSchema("Roezec")
                .WithColumn("MetadatoId").AsInt32().PrimaryKey().Identity().NotNullable()
                .WithColumn("FicheroId").AsInt32()
                .ForeignKey("Ficheros","FicheroId")
                .WithColumn("Nombre").AsString(100).NotNullable()
                .WithColumn("Valor").AsString(100).NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Resolución"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Acuerdo"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Notificación"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Alegación"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Comunicación"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Certificado"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Declaración"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Informe"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Solicitud"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Denuncia"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Reursos"
            });
            Insert.IntoTable("Tipos_Documento").Row(new
            {
                Tipo = "Acta"
            });
        }


        public override void Down()
        {
        }
    }
}