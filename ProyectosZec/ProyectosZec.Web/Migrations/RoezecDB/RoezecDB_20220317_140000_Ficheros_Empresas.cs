﻿/************************************************************************************************************
 * Creación tabla de FicherosEmpresas
 *
 * AUTOR : Javier Núñez
 * FECHA : Marzo 2022
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20220317140000)]
    public class RoezecDB_20220317_140000_Ficheros_Empresas : Migration
    {
        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {
            /* Empresas_empleos*/
            Create.Table("empresas_ficheros").InSchema("Roezec")
                .WithColumn("EmpresasFicheroId").AsInt32().Identity().PrimaryKey()
                .WithColumn("EmpresaId").AsInt32().NotNullable()
                .ForeignKey("EmpresasFicheros_ibfk_1","Empresas","EmpresaId")
                .WithColumn("Fichero").AsString(1000).NotNullable()
                .WithColumn("Nombre").AsString(250).NotNullable()
                .WithColumn("Observaciones").AsString(1000).Nullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();

            Create.Table("tipos_direcciones").InSchema("Roezec")
                .WithColumn("TipoDireccionId").AsInt32().Identity().PrimaryKey()
                .WithColumn("TipoDireccion").AsString(50).NotNullable();

            Create.Table("tipos_persona").InSchema("Roezec")
                .WithColumn("TipoPersonaId").AsInt32().Identity()
                .WithColumn("TipoPersona").AsString().NotNullable();

            Alter.Table("Contactos").InSchema("Roezec")
                .AddColumn("TipoPersonaId").AsInt32().NotNullable()
                .ForeignKey("personas_tipo_ibfk", "tipos_persona", "TipoPersonaId");

            Alter.Table("empresas_direcciones").InSchema("Roezec")
                .AddColumn("TipoDireccionId").AsInt32().NotNullable();

            Alter.Table("historial_empresas").InSchema("Roezec")
                .AddColumn("FechaFirma").AsDate().Nullable();

            Alter.Table("Procedimientos").InSchema("Roezec")
                .AddColumn("Duracion").AsInt32().Nullable()
                .AddColumn("PlazoId").AsInt32().Nullable()
                .AddColumn("EstadoRetornoId").AsInt32().Nullable()
                .AddColumn("ProcedimientoRetornoId").AsInt32().Nullable();

        }


        public override void Down()
        {
        }
    }
}