﻿/************************************************************************************************************
 * Creación tabla de Alarmas Roezec
 *
 * AUTOR : Javier Núñez
 * FECHA : Octubre 2021
 * *********************************************************************************************************/



using FluentMigrator;
using System.IO;
using System;

namespace ProyectosZec.Migrations.RoezecDB
{
    [Migration(20211030140000)]
    public class RoezecDB_20211030_140000_Empresas_Nombres : Migration
    {
        private string GetScript(string name)
        {
            using (var sr = new StreamReader(this.GetType().Assembly.GetManifestResourceStream(name)))
                return sr.ReadToEnd();
        }
        public override void Up()
        {
            /* Empresas_Nombres*/
            Create.Table("Empresas_Nombres").InSchema("Roezec")
                .WithColumn("NombreId").AsInt32().Identity().PrimaryKey()
                .WithColumn("EmpresaId").AsInt32().NotNullable()
                .WithColumn("Nombre").AsString(255).NotNullable()
                .WithColumn("Desde").AsDate().Nullable()
                .WithColumn("Hasta").AsDate().Nullable()
                .WithColumn("Cif").AsString(20).Nullable()
                .WithColumn("UserId").AsInt32().NotNullable()
                .WithColumn("FechaModificacion").AsDateTime().NotNullable();     
        }

        public override void Down()
        {
        }
    }
}