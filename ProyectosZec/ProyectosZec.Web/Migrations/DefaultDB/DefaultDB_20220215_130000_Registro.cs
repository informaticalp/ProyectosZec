﻿using FluentMigrator;

namespace ProyectosZec.Migrations.DefaultDB
{
    [Migration(20220215130000)]
    public class DefaultDB_20220215_130000_Registro : AutoReversingMigration
    {
        public override void Up()
        {
            /* Tabla Servicios */
            Create.Table("TiposRegistro").InSchema("Default")
                .WithColumn("TipoRegistroId").AsInt32().Identity().PrimaryKey()
                .WithColumn("TipoRegistro").AsString(20).NotNullable();
            /*  Tabla severidades  */
            Create.Table("Registro").InSchema("Default")
                .WithColumn("RegistroId").AsInt32().Identity().PrimaryKey()
                .WithColumn("NumeroRegistro").AsString(255).NotNullable()
                .WithColumn("FechaRegistro").AsDate().NotNullable()
                .WithColumn("TipoRegistroId").AsInt32().NotNullable()
                .ForeignKey("FK_TipoRegistro", "TiposRegistro", "TipoRegistroId")
                .WithColumn("Titulo").AsString(100).Nullable()
                .WithColumn("Persona").AsString(100).Nullable()
                .WithColumn("Ficheros").AsString(1000).Nullable()
                .WithColumn("Observaciones").AsString(1000).Nullable()
                .WithColumn("UserId").AsInt32().Nullable()
                .WithColumn("FechaModificacion").AsDateTime().Nullable();
            Create.Index("NumeroRegistro_IDX")
                .OnTable("Registro")
                .InSchema("Default")
                .OnColumn("NumeroRegistro")
                .Unique();
        }
    }
}