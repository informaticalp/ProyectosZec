﻿using FluentMigrator;

namespace ProyectosZec.Migrations.DefaultDB
{
    [Migration(20220131130000)]
    public class DefaultDB_20220131_130000_ENS : AutoReversingMigration
    {
        public override void Up()
        {
            /* Tabla Servicios */
            Create.Table("Servicios").InSchema("Default")
                .WithColumn("ServicioId").AsInt32().Identity().PrimaryKey()
                .WithColumn("Servicio").AsString(30).NotNullable();
            /*  Tabla severidades  */
            Create.Table("Severidades").InSchema("Default")
                .WithColumn("SeveridadId").AsInt32().Identity().PrimaryKey()
                .WithColumn("Severidad").AsString(30).NotNullable();
            /* Tabla Incidencias */
            Create.Table("Incidencias").InSchema("Default")
                .WithColumn("IncidenciaId").AsInt32().Identity().PrimaryKey()
                .WithColumn("ServicioId").AsInt32().NotNullable()
                .ForeignKey("FK_Incidencias_ServicioID", "Servicios", "ServicioId")
                .WithColumn("SeveridadId").AsInt32().NotNullable()
                .ForeignKey("FK_Incidencias_SeveridadID", "Severidades", "SeveridadId")
                .WithColumn("Apertura").AsDateTime().NotNullable()
                .WithColumn("Cierre").AsDateTime().Nullable()
                .WithColumn("Observaciones").AsString(1500).NotNullable()
                .WithColumn("ObservacionesCierre").AsString(1500).Nullable();
            /* Tabla cambios */
            Create.Table("Cambios").InSchema("Default")
                .WithColumn("CombioId").AsInt32().Identity().PrimaryKey()
                .WithColumn("ServicioId").AsInt32().NotNullable()
                .ForeignKey("FK_Cambios_ServicioID", "Servicios", "ServicioId")
                .WithColumn("Observaciones").AsString(1500).NotNullable()
                .WithColumn("Fecha").AsDateTime().NotNullable();
            /* Tipos de servicio para contraseñas */
            Create.Table("TiposServicio").InSchema("Default")
                .WithColumn("TipoId").AsInt32().Identity().PrimaryKey()
                .WithColumn("TipoServicio").AsString(100).NotNullable();
            /* Contraseñas */
            Create.Table("Passwords").InSchema("Default")
                .WithColumn("PasswordId").AsInt32().Identity().PrimaryKey()
                .WithColumn("Aplicacion").AsString(100).NotNullable()
                .WithColumn("Acceso").AsString(80).Nullable()
                .WithColumn("Usuario").AsString(100).Nullable()
                .WithColumn("Password").AsString(30).Nullable()
                .WithColumn("Url").AsString(255).Nullable()
                .WithColumn("TipoId").AsInt32().Nullable()
                .ForeignKey("TiposServicio", "TipoId")
                .WithColumn("Observaciones").AsString(255).Nullable();
        }
    }
}