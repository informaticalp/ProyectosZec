﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ProyectosZec.Nuevo_Roezec
{
    public partial class EmpresasFicherosEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "ProyectosZec.Nuevo_Roezec.EmpresasFicherosEditor";

        public EmpresasFicherosEditorAttribute()
            : base(Key)
        {
        }
    }
}

