﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ProyectosZec.Nuevo_Roezec
{
    public partial class EmpresasNombresEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "ProyectosZec.Nuevo_Roezec.EmpresasNombresEditor";

        public EmpresasNombresEditorAttribute()
            : base(Key)
        {
        }
    }
}

