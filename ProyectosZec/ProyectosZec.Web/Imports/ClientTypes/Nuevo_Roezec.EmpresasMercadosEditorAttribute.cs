﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ProyectosZec.Nuevo_Roezec
{
    public partial class EmpresasMercadosEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "ProyectosZec.Nuevo_Roezec.EmpresasMercadosEditor";

        public EmpresasMercadosEditorAttribute()
            : base(Key)
        {
        }
    }
}

