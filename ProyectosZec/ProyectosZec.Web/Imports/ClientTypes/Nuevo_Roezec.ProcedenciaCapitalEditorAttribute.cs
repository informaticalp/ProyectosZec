﻿using Serenity;
using Serenity.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace ProyectosZec.Nuevo_Roezec
{
    public partial class ProcedenciaCapitalEditorAttribute : CustomEditorAttribute
    {
        public const string Key = "ProyectosZec.Nuevo_Roezec.ProcedenciaCapitalEditor";

        public ProcedenciaCapitalEditorAttribute()
            : base(Key)
        {
        }
    }
}

