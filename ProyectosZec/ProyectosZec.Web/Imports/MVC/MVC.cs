﻿







using System;

namespace MVC
{
    public static class Views
    {

        public static class Administration
        {

            public static class Language
            {

                public const string LanguageIndex = "~/Modules/Administration/Language/LanguageIndex.cshtml";
            }


            public static class Role
            {

                public const string RoleIndex = "~/Modules/Administration/Role/RoleIndex.cshtml";
            }


            public static class Translation
            {

                public const string TranslationIndex = "~/Modules/Administration/Translation/TranslationIndex.cshtml";
            }


            public static class User
            {

                public const string UserIndex = "~/Modules/Administration/User/UserIndex.cshtml";
            }

        }


        public static class Common
        {

            public static class Dashboard
            {

                public const string DashboardIndex = "~/Modules/Common/Dashboard/DashboardIndex.cshtml";
            }


            public static class Reporting
            {

                public const string ReportPage = "~/Modules/Common/Reporting/ReportPage.cshtml";
            }

        }


        public static class CuadroMandos
        {

            public static class Capital
            {

                public const string CapitalIndex = "~/Modules/CuadroMandos/Capital/CapitalIndex.cshtml";
            }


            public static class Estados
            {

                public const string EstadosIndex = "~/Modules/CuadroMandos/Estados/EstadosIndex.cshtml";
            }


            public static class Islas
            {

                public const string IslasIndex = "~/Modules/CuadroMandos/Islas/IslasIndex.cshtml";
            }


            public static class Prescriptorinversor
            {

                public const string PrescriptorinversorIndex = "~/Modules/CuadroMandos/Prescriptorinversor/PrescriptorinversorIndex.cshtml";
            }


            public static class Presentadas
            {

                public const string PresentadasIndex = "~/Modules/CuadroMandos/Presentadas/PresentadasIndex.cshtml";
            }


            public static class Proyectos
            {

                public const string ProyectosIndex = "~/Modules/CuadroMandos/Proyectos/ProyectosIndex.cshtml";
            }


            public static class Sectores
            {

                public const string SectoresIndex = "~/Modules/CuadroMandos/Sectores/SectoresIndex.cshtml";
            }


            public static class Subsectores
            {

                public const string SubsectoresIndex = "~/Modules/CuadroMandos/Subsectores/SubsectoresIndex.cshtml";
            }


            public static class Tecnicos
            {

                public const string TecnicosIndex = "~/Modules/CuadroMandos/Tecnicos/TecnicosIndex.cshtml";
            }

        }


        public static class ENS
        {

            public static class Cambios
            {

                public const string CambiosIndex = "~/Modules/ENS/Cambios/CambiosIndex.cshtml";
            }


            public static class Incidencias
            {

                public const string IncidenciasIndex = "~/Modules/ENS/Incidencias/IncidenciasIndex.cshtml";
            }


            public static class Passwords
            {

                public const string PasswordsIndex = "~/Modules/ENS/Passwords/PasswordsIndex.cshtml";
            }


            public static class Servicios
            {

                public const string ServiciosIndex = "~/Modules/ENS/Servicios/ServiciosIndex.cshtml";
            }


            public static class Severidades
            {

                public const string SeveridadesIndex = "~/Modules/ENS/Severidades/SeveridadesIndex.cshtml";
            }


            public static class Tiposservicio
            {

                public const string TiposservicioIndex = "~/Modules/ENS/Tiposservicio/TiposservicioIndex.cshtml";
            }
        }


        public static class Errors
        {

            public const string AccessDenied = "~/Views/Errors/AccessDenied.cshtml";

            public const string ValidationError = "~/Views/Errors/ValidationError.cshtml";
        }


        public static class Inmovilizado
        {

            public static class Inmovilizados
            {

                public const string InmovilizadosIndex = "~/Modules/Inmovilizado/Inmovilizados/InmovilizadosIndex.cshtml";
            }


            public static class Proveedores
            {

                public const string ProveedoresIndex = "~/Modules/Inmovilizado/Proveedores/ProveedoresIndex.cshtml";
            }


            public static class Subtiposinmovilizado
            {

                public const string SubtiposinmovilizadoIndex = "~/Modules/Inmovilizado/Subtiposinmovilizado/SubtiposinmovilizadoIndex.cshtml";
            }


            public static class Tiposinmovilizado
            {

                public const string TiposinmovilizadoIndex = "~/Modules/Inmovilizado/Tiposinmovilizado/TiposinmovilizadoIndex.cshtml";
            }

        }


        public static class Intranet
        {

            public static class Departamentos
            {

                public const string DepartamentosIndex = "~/Modules/Intranet/Departamentos/DepartamentosIndex.cshtml";
            }


            public static class Sedes
            {

                public const string SedesIndex = "~/Modules/Intranet/Sedes/SedesIndex.cshtml";
            }


            public static class Telefonos
            {

                public const string TelefonosIndex = "~/Modules/Intranet/Telefonos/TelefonosIndex.cshtml";
            }

        }


        public static class Kairos
        {

            public static class AusenciasProgramadas
            {

                public const string AusenciasProgramadasIndex = "~/Modules/Kairos/AusenciasProgramadas/AusenciasProgramadasIndex.cshtml";
            }


            public static class Departamentos
            {

                public const string DepartamentosIndex = "~/Modules/Kairos/Departamentos/DepartamentosIndex.cshtml";
            }


            public static class Diario
            {

                public const string DiarioIndex = "~/Modules/Kairos/Diario/DiarioIndex.cshtml";
            }


            public static class EstadosExtras
            {

                public const string EstadosExtrasIndex = "~/Modules/Kairos/EstadosExtras/EstadosExtrasIndex.cshtml";
            }


            public static class Extras
            {

                public const string ExtrasIndex = "~/Modules/Kairos/Extras/ExtrasIndex.cshtml";
            }


            public static class Fichajes
            {

                public const string FichajesIndex = "~/Modules/Kairos/Fichajes/FichajesIndex.cshtml";
            }


            public static class HorasExtraConsumidas
            {

                public const string HorasExtraConsumidasIndex = "~/Modules/Kairos/HorasExtraConsumidas/HorasExtraConsumidasIndex.cshtml";
            }


            public static class HorasExtraConsumidasReadOnly
            {

                public const string HorasExtraConsumidasReadOnlyIndex = "~/Modules/Kairos/HorasExtraConsumidasReadOnly/HorasExtraConsumidasReadOnlyIndex.cshtml";
            }


            public static class Hoy
            {

                public const string HoyIndex = "~/Modules/Kairos/Hoy/HoyIndex.cshtml";
            }


            public static class KrsAusenciasProgramadasTipos
            {

                public const string KrsAusenciasProgramadasTiposIndex = "~/Modules/Kairos/KrsAusenciasProgramadasTipos/KrsAusenciasProgramadasTiposIndex.cshtml";
            }


            public static class KrsEmpleados
            {

                public const string KrsEmpleadosIndex = "~/Modules/Kairos/KrsEmpleados/KrsEmpleadosIndex.cshtml";
            }


            public static class TiposFichaje
            {

                public const string TiposFichajeIndex = "~/Modules/Kairos/TiposFichaje/TiposFichajeIndex.cshtml";
            }

        }


        public static class Membership
        {

            public static class Account
            {

                public const string AccountLogin = "~/Modules/Membership/Account/AccountLogin.cshtml";

                public const string AccountLogin_AdminLTE = "~/Modules/Membership/Account/AccountLogin.AdminLTE.cshtml";

                public static class ChangePassword
                {

                    public const string AccountChangePassword = "~/Modules/Membership/Account/ChangePassword/AccountChangePassword.cshtml";
                }


                public static class ForgotPassword
                {

                    public const string AccountForgotPassword = "~/Modules/Membership/Account/ForgotPassword/AccountForgotPassword.cshtml";

                    public const string AccountForgotPassword_AdminLTE = "~/Modules/Membership/Account/ForgotPassword/AccountForgotPassword.AdminLTE.cshtml";
                }


                public static class ResetPassword
                {

                    public const string AccountResetPassword = "~/Modules/Membership/Account/ResetPassword/AccountResetPassword.cshtml";

                    public const string AccountResetPassword_AdminLTE = "~/Modules/Membership/Account/ResetPassword/AccountResetPassword.AdminLTE.cshtml";

                    public const string AccountResetPasswordEmail = "~/Modules/Membership/Account/ResetPassword/AccountResetPasswordEmail.cshtml";
                }


                public static class SignUp
                {

                    public const string AccountActivateEmail = "~/Modules/Membership/Account/SignUp/AccountActivateEmail.cshtml";

                    public const string AccountSignUp = "~/Modules/Membership/Account/SignUp/AccountSignUp.cshtml";

                    public const string AccountSignUp_AdminLTE = "~/Modules/Membership/Account/SignUp/AccountSignUp.AdminLTE.cshtml";
                }
            }

        }


        public static class Nuevo_Roezec
        {

            public static class Alarmas
            {

                public const string AlarmasIndex = "~/Modules/Nuevo_Roezec/Alarmas/AlarmasIndex.cshtml";
            }


            public static class Alarmasprocedimientos
            {

                public const string AlarmasProcedimientosIndex = "~/Modules/Nuevo_Roezec/Alarmasprocedimientos/AlarmasProcedimientosIndex.cshtml";
            }


            public static class Capital
            {

                public const string CapitalIndex = "~/Modules/Nuevo_Roezec/Capital/CapitalIndex.cshtml";
            }


            public static class Contactos
            {

                public const string ContactosIndex = "~/Modules/Nuevo_Roezec/Contactos/ContactosIndex.cshtml";
            }


            public static class Continentes
            {

                public const string ContinentesIndex = "~/Modules/Nuevo_Roezec/Continentes/ContinentesIndex.cshtml";
            }


            public static class Empresas
            {

                public const string EmpresasIndex = "~/Modules/Nuevo_Roezec/Empresas/EmpresasIndex.cshtml";
            }


            public static class EmpresasContactos
            {

                public const string EmpresasContactosIndex = "~/Modules/Nuevo_Roezec/EmpresasContactos/EmpresasContactosIndex.cshtml";
            }


            public static class EmpresasContactosReadOnly
            {

                public const string EmpresasContactosReadOnlyIndex = "~/Modules/Nuevo_Roezec/EmpresasContactosReadOnly/EmpresasContactosReadOnlyIndex.cshtml";
            }


            public static class EmpresasDirecciones
            {

                public const string EmpresasDireccionesIndex = "~/Modules/Nuevo_Roezec/EmpresasDirecciones/EmpresasDireccionesIndex.cshtml";
            }


            public static class EmpresasEmpleos
            {

                public const string EmpresasEmpleosIndex = "~/Modules/Nuevo_Roezec/EmpresasEmpleos/EmpresasEmpleosIndex.cshtml";
            }


            public static class EmpresasFicheros
            {

                public const string EmpresasFicherosIndex = "~/Modules/Nuevo_Roezec/EmpresasFicheros/EmpresasFicherosIndex.cshtml";
            }


            public static class EmpresasMercados
            {

                public const string EmpresasMercadosIndex = "~/Modules/Nuevo_Roezec/EmpresasMercados/EmpresasMercadosIndex.cshtml";
            }


            public static class EmpresasNace
            {

                public const string EmpresasNaceIndex = "~/Modules/Nuevo_Roezec/EmpresasNace/EmpresasNaceIndex.cshtml";
            }


            public static class EmpresasNombres
            {

                public const string EmpresasNombresIndex = "~/Modules/Nuevo_Roezec/EmpresasNombres/EmpresasNombresIndex.cshtml";
            }


            public static class Envios
            {

                public const string EnviosIndex = "~/Modules/Nuevo_Roezec/Envios/EnviosIndex.cshtml";
            }


            public static class EnviosProcedimiento
            {

                public const string EnviosProcedimientoIndex = "~/Modules/Nuevo_Roezec/EnviosProcedimiento/EnviosProcedimientoIndex.cshtml";
            }


            public static class EstadosEmpresa
            {

                public const string EstadosEmpresaIndex = "~/Modules/Nuevo_Roezec/EstadosEmpresa/EstadosEmpresaIndex.cshtml";
            }


            public static class EstadosEnvio
            {

                public const string EstadosEnvioIndex = "~/Modules/Nuevo_Roezec/EstadosEnvio/EstadosEnvioIndex.cshtml";
            }


            public static class Ficheros
            {

                public const string FicherosIndex = "~/Modules/Nuevo_Roezec/Ficheros/FicherosIndex.cshtml";
            }


            public static class FicherosReadOnly
            {

                public const string FicherosReadOnlyIndex = "~/Modules/Nuevo_Roezec/FicherosReadOnly/FicherosReadOnlyIndex.cshtml";
            }


            public static class FormasEnvio
            {

                public const string FormasEnvioIndex = "~/Modules/Nuevo_Roezec/FormasEnvio/FormasEnvioIndex.cshtml";
            }


            public static class FormasJuridicas
            {

                public const string FormasJuridicasIndex = "~/Modules/Nuevo_Roezec/FormasJuridicas/FormasJuridicasIndex.cshtml";
            }


            public static class HistorialEmpresas
            {

                public const string HistorialEmpresasIndex = "~/Modules/Nuevo_Roezec/HistorialEmpresas/HistorialEmpresasIndex.cshtml";
            }


            public static class HistorialReadOnly
            {

                public const string HistorialReadOnlyIndex = "~/Modules/Nuevo_Roezec/HistorialReadOnly/HistorialReadOnlyIndex.cshtml";
            }


            public static class Idiomas
            {

                public const string IdiomasIndex = "~/Modules/Nuevo_Roezec/Idiomas/IdiomasIndex.cshtml";
            }


            public static class Islas
            {

                public const string IslasIndex = "~/Modules/Nuevo_Roezec/Islas/IslasIndex.cshtml";
            }


            public static class Mercados
            {

                public const string MercadosIndex = "~/Modules/Nuevo_Roezec/Mercados/MercadosIndex.cshtml";
            }


            public static class MercadosReadOnly
            {

                public const string MercadosReadOnlyIndex = "~/Modules/Nuevo_Roezec/MercadosReadOnly/MercadosReadOnlyIndex.cshtml";
            }


            public static class Metadatos
            {

                public const string MetadatosIndex = "~/Modules/Nuevo_Roezec/Metadatos/MetadatosIndex.cshtml";
            }


            public static class Naces
            {

                public const string NacesIndex = "~/Modules/Nuevo_Roezec/Naces/NacesIndex.cshtml";
            }


            public static class NacesReadOnly
            {

                public const string NacesReadOnlyIndex = "~/Modules/Nuevo_Roezec/NacesReadOnly/NacesReadOnlyIndex.cshtml";
            }


            public static class Paises
            {

                public const string PaisesIndex = "~/Modules/Nuevo_Roezec/Paises/PaisesIndex.cshtml";
            }


            public static class Plazos
            {

                public const string PlazosIndex = "~/Modules/Nuevo_Roezec/Plazos/PlazosIndex.cshtml";
            }


            public static class ProcedenciaCapital
            {

                public const string ProcedenciaCapitalIndex = "~/Modules/Nuevo_Roezec/ProcedenciaCapital/ProcedenciaCapitalIndex.cshtml";
            }


            public static class Procedimientos
            {

                public const string ProcedimientosIndex = "~/Modules/Nuevo_Roezec/Procedimientos/ProcedimientosIndex.cshtml";
            }


            public static class Sectores
            {

                public const string SectoresIndex = "~/Modules/Nuevo_Roezec/Sectores/SectoresIndex.cshtml";
            }


            public static class Sentidosresolucion
            {

                public const string SentidosresolucionIndex = "~/Modules/Nuevo_Roezec/Sentidosresolucion/SentidosresolucionIndex.cshtml";
            }


            public static class Subsectores
            {

                public const string SubsectoresIndex = "~/Modules/Nuevo_Roezec/Subsectores/SubsectoresIndex.cshtml";
            }


            public static class Tecnicos
            {

                public const string TecnicosIndex = "~/Modules/Nuevo_Roezec/Tecnicos/TecnicosIndex.cshtml";
            }


            public static class TipologiasCapital
            {

                public const string TipologiasCapitalIndex = "~/Modules/Nuevo_Roezec/TipologiasCapital/TipologiasCapitalIndex.cshtml";
            }


            public static class TiposAlarma
            {

                public const string TiposAlarmaIndex = "~/Modules/Nuevo_Roezec/TiposAlarma/TiposAlarmaIndex.cshtml";
            }


            public static class TiposContacto
            {

                public const string TiposContactoIndex = "~/Modules/Nuevo_Roezec/TiposContacto/TiposContactoIndex.cshtml";
            }


            public static class TiposDirecciones
            {

                public const string TiposDireccionesIndex = "~/Modules/Nuevo_Roezec/TiposDirecciones/TiposDireccionesIndex.cshtml";
            }


            public static class TiposDocumento
            {

                public const string TiposDocumentoIndex = "~/Modules/Nuevo_Roezec/TiposDocumento/TiposDocumentoIndex.cshtml";
            }


            public static class TiposEnvio
            {

                public const string TiposEnvioIndex = "~/Modules/Nuevo_Roezec/TiposEnvio/TiposEnvioIndex.cshtml";
            }


            public static class TiposGarantiaTasas
            {

                public const string TiposGarantiaTasasIndex = "~/Modules/Nuevo_Roezec/TiposGarantiaTasas/TiposGarantiaTasasIndex.cshtml";
            }


            public static class TiposPersona
            {

                public const string TiposPersonaIndex = "~/Modules/Nuevo_Roezec/TiposPersona/TiposPersonaIndex.cshtml";
            }


            public static class VersionesNace
            {

                public const string VersionesNaceIndex = "~/Modules/Nuevo_Roezec/VersionesNace/VersionesNaceIndex.cshtml";
            }

        }


        public static class Registro
        {

            public static class Registro_
            {

                public const string RegistroIndex = "~/Modules/Registro/Registro/RegistroIndex.cshtml";
            }


            public static class Tiposregistro
            {

                public const string TiposregistroIndex = "~/Modules/Registro/Tiposregistro/TiposregistroIndex.cshtml";
            }

        }


        public static class Roezec
        {

            public static class Actividades
            {

                public const string ActividadesIndex = "~/Modules/Roezec/Actividades/ActividadesIndex.cshtml";
            }


            public static class EmpleosSS
            {

                public const string EmpleosSSIndex = "~/Modules/Roezec/EmpleosSS/EmpleosSSIndex.cshtml";
            }


            public static class Inscritas
            {

                public const string InscritasIndex = "~/Modules/Roezec/Inscritas/InscritasIndex.cshtml";
            }


            public static class Naces
            {

                public const string NacesIndex = "~/Modules/Roezec/Naces/NacesIndex.cshtml";
            }


            public static class Representantes
            {

                public const string RepresentantesIndex = "~/Modules/Roezec/Representantes/RepresentantesIndex.cshtml";
            }


            public static class RoezecEmpresas
            {

                public const string RoezecEmpresasIndex = "~/Modules/Roezec/RoezecEmpresas/RoezecEmpresasIndex.cshtml";
            }


            public static class RoezecEmpresasSS
            {

                public const string RoezecEmpresasSSIndex = "~/Modules/Roezec/RoezecEmpresasSS/RoezecEmpresasSSIndex.cshtml";
            }


            public static class RoezecEstados
            {

                public const string RoezecEstadosIndex = "~/Modules/Roezec/RoezecEstados/RoezecEstadosIndex.cshtml";
            }


            public static class Socios
            {

                public const string SociosIndex = "~/Modules/Roezec/Socios/SociosIndex.cshtml";
            }
        }


        public static class Shared
        {

            public const string _Layout = "~/Views/Shared/_Layout.cshtml";

            public const string _LayoutHead = "~/Views/Shared/_LayoutHead.cshtml";

            public const string _LayoutNoNavigation = "~/Views/Shared/_LayoutNoNavigation.cshtml";

            public const string _LayoutSlim = "~/Views/Shared/_LayoutSlim.cshtml";

            public const string _LayoutSlimHead = "~/Views/Shared/_LayoutSlimHead.cshtml";

            public const string Error = "~/Views/Shared/Error.cshtml";

            public const string LeftNavigation = "~/Views/Shared/LeftNavigation.cshtml";
        }

    }
}

