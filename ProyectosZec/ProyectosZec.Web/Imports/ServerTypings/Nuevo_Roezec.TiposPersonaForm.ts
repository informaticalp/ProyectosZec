﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class TiposPersonaForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.TiposPersona';
    }

    export interface TiposPersonaForm {
        TipoPersona: Serenity.StringEditor;
    }

    [,
        ['TipoPersona', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(TiposPersonaForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}