﻿
namespace ProyectosZec.Registro {
    export class RegistroForm extends Serenity.PrefixedContext {
        static formKey = 'Registro.Registro';
    }

    export interface RegistroForm {
        NumeroRegistro: Serenity.StringEditor;
        FechaRegistro: Serenity.DateEditor;
        TipoRegistroId: Serenity.IntegerEditor;
        Titulo: Serenity.StringEditor;
        Ficheros: Serenity.StringEditor;
        Observaciones: Serenity.StringEditor;
    }

    [,
        ['NumeroRegistro', () => Serenity.StringEditor],
        ['FechaRegistro', () => Serenity.DateEditor],
        ['TipoRegistroId', () => Serenity.IntegerEditor],
        ['Titulo', () => Serenity.StringEditor],
        ['Ficheros', () => Serenity.StringEditor],
        ['Observaciones', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(RegistroForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}