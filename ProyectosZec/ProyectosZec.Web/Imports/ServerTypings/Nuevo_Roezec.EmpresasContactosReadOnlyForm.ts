﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class EmpresasContactosReadOnlyForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EmpresasContactosReadOnly';
    }

    export interface EmpresasContactosReadOnlyForm {
        EmpresaId: Serenity.IntegerEditor;
        ContactoId: Serenity.IntegerEditor;
        TipoContactoId: Serenity.IntegerEditor;
        FechaAlta: Serenity.DateEditor;
        FechaBaja: Serenity.DateEditor;
    }

    [,
        ['EmpresaId', () => Serenity.IntegerEditor],
        ['ContactoId', () => Serenity.IntegerEditor],
        ['TipoContactoId', () => Serenity.IntegerEditor],
        ['FechaAlta', () => Serenity.DateEditor],
        ['FechaBaja', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(EmpresasContactosReadOnlyForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}