﻿
namespace ProyectosZec.ENS {
    export class SeveridadesForm extends Serenity.PrefixedContext {
        static formKey = 'ENS.Severidades';
    }

    export interface SeveridadesForm {
        Severidad: Serenity.StringEditor;
    }

    [,
        ['Severidad', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(SeveridadesForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}