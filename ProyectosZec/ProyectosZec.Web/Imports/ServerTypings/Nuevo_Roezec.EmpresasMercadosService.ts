﻿namespace ProyectosZec.Nuevo_Roezec {
    export namespace EmpresasMercadosService {
        export const baseUrl = 'Nuevo_Roezec/EmpresasMercados';

        export declare function Create(request: Serenity.SaveRequest<EmpresasMercadosRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<EmpresasMercadosRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EmpresasMercadosRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<EmpresasMercadosRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Nuevo_Roezec/EmpresasMercados/Create",
            Update = "Nuevo_Roezec/EmpresasMercados/Update",
            Delete = "Nuevo_Roezec/EmpresasMercados/Delete",
            Retrieve = "Nuevo_Roezec/EmpresasMercados/Retrieve",
            List = "Nuevo_Roezec/EmpresasMercados/List"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List'
        ].forEach(x => {
            (<any>EmpresasMercadosService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

