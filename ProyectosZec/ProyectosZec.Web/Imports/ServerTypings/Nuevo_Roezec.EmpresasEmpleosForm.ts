﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class EmpresasEmpleosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EmpresasEmpleos';
    }

    export interface EmpresasEmpleosForm {
        EmpresaId: Serenity.IntegerEditor;
        Any: Serenity.IntegerEditor;
        Empleos: Serenity.IntegerEditor;
        UserId: Serenity.IntegerEditor;
        FechaModificacion: Serenity.DateEditor;
    }

    [,
        ['EmpresaId', () => Serenity.IntegerEditor],
        ['Any', () => Serenity.IntegerEditor],
        ['Empleos', () => Serenity.IntegerEditor],
        ['UserId', () => Serenity.IntegerEditor],
        ['FechaModificacion', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(EmpresasEmpleosForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}