﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface VersionesNaceRow {
        VersionId?: number;
        Version?: string;
        Fecha?: string;
    }

    export namespace VersionesNaceRow {
        export const idProperty = 'VersionId';
        export const nameProperty = 'Version';
        export const localTextPrefix = 'Nuevo_Roezec.VersionesNace';
        export const lookupKey = 'Nuevo_Roezec.VersionesNace';

        export function getLookup(): Q.Lookup<VersionesNaceRow> {
            return Q.getLookup<VersionesNaceRow>('Nuevo_Roezec.VersionesNace');
        }
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export declare const enum Fields {
            VersionId = "VersionId",
            Version = "Version",
            Fecha = "Fecha"
        }
    }
}

