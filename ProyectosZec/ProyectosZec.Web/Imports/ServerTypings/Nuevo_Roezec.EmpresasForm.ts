﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EmpresasForm {
        Razon: Serenity.StringEditor;
        FormaJuridicaId: Serenity.LookupEditor;
        TecnicoId: Serenity.LookupEditor;
        Cif: Serenity.StringEditor;
        Expediente: Serenity.StringEditor;
        MotivoExencion: Serenity.StringEditor;
        TipologiaCapitalId: Serenity.LookupEditor;
        TipoGarantiaTasaId: Serenity.LookupEditor;
        EstadoEmpresaId: Serenity.LookupEditor;
        FechaCambioEstado: Serenity.DateEditor;
        Direccion: Serenity.StringEditor;
        Poblacion: Serenity.StringEditor;
        Cp: Serenity.StringEditor;
        IslaId: Serenity.LookupEditor;
        TelefonoFijo: Serenity.StringEditor;
        Movil: Serenity.StringEditor;
        Email: Serenity.EmailEditor;
        EmpleoTraspasado: Serenity.IntegerEditor;
        Empleo6Meses: Serenity.IntegerEditor;
        EmpleoPromedio: Serenity.IntegerEditor;
        EmpleoPromedio2Anos: Serenity.IntegerEditor;
        InversionTraspasada: Serenity.DecimalEditor;
        Inversion2Anos: Serenity.DecimalEditor;
        NumTasaLiquidacion: Serenity.StringEditor;
        NacesList: EmpresasNaceEditor;
        HistorialList: HistorialEmpresasEditor;
        ContactosList: EmpresasContactosEditor;
        CapitalList: ProcedenciaCapitalEditor;
        MercadosList: EmpresasMercadosEditor;
    }

    export class EmpresasForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Empresas';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EmpresasForm.init)  {
                EmpresasForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.LookupEditor;
                var w2 = s.DateEditor;
                var w3 = s.EmailEditor;
                var w4 = s.IntegerEditor;
                var w5 = s.DecimalEditor;
                var w6 = EmpresasNaceEditor;
                var w7 = HistorialEmpresasEditor;
                var w8 = EmpresasContactosEditor;
                var w9 = ProcedenciaCapitalEditor;
                var w10 = EmpresasMercadosEditor;

                Q.initFormType(EmpresasForm, [
                    'Razon', w0,
                    'FormaJuridicaId', w1,
                    'TecnicoId', w1,
                    'Cif', w0,
                    'Expediente', w0,
                    'MotivoExencion', w0,
                    'TipologiaCapitalId', w1,
                    'TipoGarantiaTasaId', w1,
                    'EstadoEmpresaId', w1,
                    'FechaCambioEstado', w2,
                    'Direccion', w0,
                    'Poblacion', w0,
                    'Cp', w0,
                    'IslaId', w1,
                    'TelefonoFijo', w0,
                    'Movil', w0,
                    'Email', w3,
                    'EmpleoTraspasado', w4,
                    'Empleo6Meses', w4,
                    'EmpleoPromedio', w4,
                    'EmpleoPromedio2Anos', w4,
                    'InversionTraspasada', w5,
                    'Inversion2Anos', w5,
                    'NumTasaLiquidacion', w0,
                    'NacesList', w6,
                    'HistorialList', w7,
                    'ContactosList', w8,
                    'CapitalList', w9,
                    'MercadosList', w10
                ]);
            }
        }
    }
}

