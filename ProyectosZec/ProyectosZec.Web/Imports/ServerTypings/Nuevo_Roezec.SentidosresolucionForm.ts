﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class SentidosresolucionForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Sentidosresolucion';
    }

    export interface SentidosresolucionForm {
        Sentido: Serenity.StringEditor;
    }

    [,
        ['Sentido', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(SentidosresolucionForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}