﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EmpresasNaceRow {
        EmpresaNaceId?: number;
        EmpresaId?: number;
        NaceId?: number;
        EmpresaRazon?: string;
        NaceLarga?: string;
        EmpresaFormaJuridicaId?: number;
        EmpresaNExpediente?: string;
        EmpresaTecnicoId?: number;
        EmpresaCif?: string;
        EmpresaDireccion?: string;
        EmpresaPoblacion?: string;
        EmpresaIslaId?: number;
        EmpresaTelefonoFijo?: string;
        EmpresaMovil?: string;
        EmpresaEmail?: string;
        EmpresaProyectoId?: number;
        EmpresaExpediente?: string;
        EmpresaMotivoExencion?: string;
        EmpresaTipologiaCapitalId?: number;
        EmpresaTipoGarantiaTasaId?: number;
        EmpresaEmpleoTraspasado?: number;
        EmpresaEmpleo6Meses?: number;
        EmpresaEmpleoPromedio?: number;
        EmpresaEmpleoPromedio2Anos?: number;
        EmpresaInversionTraspasada?: number;
        EmpresaInversion2Anos?: number;
        EmpresaEstadoEmpresaId?: number;
        TecnicoNombreTecnico?: string;
        IslaNombreIsla?: string;
        TipologiaCapitalCapital?: string;
        TipoGarantiaTasaGarantiaTasa?: string;
        EstadoEmpresaEstado?: string;
        EmpresaNumTasaLiquidacion?: string;
        NaceCodigo?: string;
        NaceDescripcion?: string;
        NaceSubsectorId?: number;
		NacePrincipal?: number;
        VersionId?: number;
        Version?: string;
        Subsector?: string;
        SectorId?: number;
        Sector?: string;
    }

    export namespace EmpresasNaceRow {
        export const idProperty = 'EmpresaNaceId';
        export const nameProperty = 'NaceDescripcion';
        export const localTextPrefix = 'Nuevo_Roezec.EmpresasNace';
        export const deletePermission = 'Roezec:Modify';
        export const insertPermission = 'Roezec:Modify';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export declare const enum Fields {
            EmpresaNaceId = "EmpresaNaceId",
            EmpresaId = "EmpresaId",
            NaceId = "NaceId",
            EmpresaRazon = "EmpresaRazon",
            NaceLarga = "NaceLarga",
            EmpresaFormaJuridicaId = "EmpresaFormaJuridicaId",
            EmpresaNExpediente = "EmpresaNExpediente",
            EmpresaTecnicoId = "EmpresaTecnicoId",
            EmpresaCif = "EmpresaCif",
            EmpresaDireccion = "EmpresaDireccion",
            EmpresaPoblacion = "EmpresaPoblacion",
            EmpresaIslaId = "EmpresaIslaId",
            EmpresaTelefonoFijo = "EmpresaTelefonoFijo",
            EmpresaMovil = "EmpresaMovil",
            EmpresaEmail = "EmpresaEmail",
            EmpresaProyectoId = "EmpresaProyectoId",
            EmpresaExpediente = "EmpresaExpediente",
            EmpresaMotivoExencion = "EmpresaMotivoExencion",
            EmpresaTipologiaCapitalId = "EmpresaTipologiaCapitalId",
            EmpresaTipoGarantiaTasaId = "EmpresaTipoGarantiaTasaId",
            EmpresaEmpleoTraspasado = "EmpresaEmpleoTraspasado",
            EmpresaEmpleo6Meses = "EmpresaEmpleo6Meses",
            EmpresaEmpleoPromedio = "EmpresaEmpleoPromedio",
            EmpresaEmpleoPromedio2Anos = "EmpresaEmpleoPromedio2Anos",
            EmpresaInversionTraspasada = "EmpresaInversionTraspasada",
            EmpresaInversion2Anos = "EmpresaInversion2Anos",
            EmpresaEstadoEmpresaId = "EmpresaEstadoEmpresaId",
            TecnicoNombreTecnico = "TecnicoNombreTecnico",
            IslaNombreIsla = "IslaNombreIsla",
            TipologiaCapitalCapital = "TipologiaCapitalCapital",
            TipoGarantiaTasaGarantiaTasa = "TipoGarantiaTasaGarantiaTasa",
            EstadoEmpresaEstado = "EstadoEmpresaEstado",
            EmpresaNumTasaLiquidacion = "EmpresaNumTasaLiquidacion",
            NaceCodigo = "NaceCodigo",
            NaceDescripcion = "NaceDescripcion",
            NaceSubsectorId = "NaceSubsectorId",
			NacePrincipal = "NacePrincipal",
            VersionId = "VersionId",
            Version = "Version",
            Subsector = "Subsector",
            SectorId = "SectorId",
            Sector = "Sector"
        }
    }
}

