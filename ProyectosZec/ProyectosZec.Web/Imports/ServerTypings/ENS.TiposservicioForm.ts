﻿
namespace ProyectosZec.ENS {
    export class TiposservicioForm extends Serenity.PrefixedContext {
        static formKey = 'ENS.Tiposservicio';
    }

    export interface TiposservicioForm {
        TipoServicio: Serenity.StringEditor;
    }

    [,
        ['TipoServicio', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(TiposservicioForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}