﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EmpresasNaceForm {
        NaceId: Serenity.LookupEditor;
    }

    export class EmpresasNaceForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EmpresasNace';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EmpresasNaceForm.init)  {
                EmpresasNaceForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;

                Q.initFormType(EmpresasNaceForm, [
                    'NaceId', w0
                ]);
            }
        }
    }
}

