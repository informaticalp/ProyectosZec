﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface EmpresasDireccionesRow {
        EmpresasDireccionesId?: number;
        EmpresaId?: number;
        Direccion?: string;
        Poblacion?: string;
        Cp?: string;
        IslaId?: number;
        Desde?: string;
        Hasta?: string;
        UserId?: number;
		UserName?: string;
		DisplayName?: string;
        FechaModificacion?: string;
        EmpresaRazon?: string;
        EmpresaDescripcion?: string;
        EmpresaFormaJuridicaId?: number;
        EmpresaTecnicoId?: number;
        EmpresaCif?: string;
        EmpresaDireccion?: string;
        EmpresaPoblacion?: string;
        EmpresaCp?: number;
        EmpresaIslaId?: number;
        EmpresaTelefonoFijo?: string;
        EmpresaMovil?: string;
        EmpresaCapitalSocial?: number;
        EmpresaEmail?: string;
        EmpresaProyectoId?: number;
        EmpresaNumeroRoezec?: number;
        EmpresaExpediente?: string;
        EmpresaMotivoExencion?: string;
        EmpresaTipologiaCapitalId?: number;
        EmpresaTipoGarantiaTasaId?: number;
        EmpresaEmpleoTraspasado?: number;
        EmpresaEmpleo6Meses?: number;
        EmpresaEmpleoPromedio?: number;
        EmpresaEmpleoPromedio2Anos?: number;
        EmpresaInversionTraspasada?: number;
        EmpresaInversion2Anos?: number;
        EmpresaEstadoEmpresaId?: number;
        EmpresaFechaAlta?: string;
        EmpresaFechaCambioEstado?: string;
        EmpresaNumTasaLiquidacion?: string;
        EmpresaUserId?: number;
        EmpresaFechaModificacion?: string;
        IslaNombreIsla?: string;
        IslaCodigo?: string;
        Isla?: string;
        IslaUserId?: number;
        IslaFechaModificacion?: string;
    }

    export namespace EmpresasDireccionesRow {
        export const idProperty = 'EmpresasDireccionesId';
        export const nameProperty = 'Direccion';
        export const localTextPrefix = 'Nuevo_Roezec.EmpresasDirecciones';
        export const deletePermission = 'Roezec:Delete';
        export const insertPermission = 'Roezec:Modify';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export namespace Fields {
            export declare const EmpresasDireccionesId;
            export declare const EmpresaId;
            export declare const Direccion;
            export declare const Poblacion;
            export declare const Cp;
            export declare const IslaId;
            export declare const Desde;
            export declare const Hasta;
            export declare const UserId;
			export declare const UserName;
			export declare const DisplayName;
            export declare const FechaModificacion;
            export declare const EmpresaRazon;
            export declare const EmpresaDescripcion;
            export declare const EmpresaFormaJuridicaId;
            export declare const EmpresaTecnicoId;
            export declare const EmpresaCif;
            export declare const EmpresaDireccion;
            export declare const EmpresaPoblacion;
            export declare const EmpresaCp;
            export declare const EmpresaIslaId;
            export declare const EmpresaTelefonoFijo;
            export declare const EmpresaMovil;
            export declare const EmpresaCapitalSocial;
            export declare const EmpresaEmail;
            export declare const EmpresaProyectoId;
            export declare const EmpresaNumeroRoezec;
            export declare const EmpresaExpediente;
            export declare const EmpresaMotivoExencion;
            export declare const EmpresaTipologiaCapitalId;
            export declare const EmpresaTipoGarantiaTasaId;
            export declare const EmpresaEmpleoTraspasado;
            export declare const EmpresaEmpleo6Meses;
            export declare const EmpresaEmpleoPromedio;
            export declare const EmpresaEmpleoPromedio2Anos;
            export declare const EmpresaInversionTraspasada;
            export declare const EmpresaInversion2Anos;
            export declare const EmpresaEstadoEmpresaId;
            export declare const EmpresaFechaAlta;
            export declare const EmpresaFechaCambioEstado;
            export declare const EmpresaNumTasaLiquidacion;
            export declare const EmpresaUserId;
            export declare const EmpresaFechaModificacion;
            export declare const IslaNombreIsla;
            export declare const IslaCodigo;
            export declare const Isla;
            export declare const IslaUserId;
            export declare const IslaFechaModificacion;
        }

        [
            'EmpresasDireccionesId',
            'EmpresaId',
            'Direccion',
            'Poblacion',
            'Cp',
            'IslaId',
            'Desde',
            'Hasta',
            'UserId',
			'UserName',
			'DisplayName',
            'FechaModificacion',
            'EmpresaRazon',
            'EmpresaDescripcion',
            'EmpresaFormaJuridicaId',
            'EmpresaTecnicoId',
            'EmpresaCif',
            'EmpresaDireccion',
            'EmpresaPoblacion',
            'EmpresaCp',
            'EmpresaIslaId',
            'EmpresaTelefonoFijo',
            'EmpresaMovil',
            'EmpresaCapitalSocial',
            'EmpresaEmail',
            'EmpresaProyectoId',
            'EmpresaNumeroRoezec',
            'EmpresaExpediente',
            'EmpresaMotivoExencion',
            'EmpresaTipologiaCapitalId',
            'EmpresaTipoGarantiaTasaId',
            'EmpresaEmpleoTraspasado',
            'EmpresaEmpleo6Meses',
            'EmpresaEmpleoPromedio',
            'EmpresaEmpleoPromedio2Anos',
            'EmpresaInversionTraspasada',
            'EmpresaInversion2Anos',
            'EmpresaEstadoEmpresaId',
            'EmpresaFechaAlta',
            'EmpresaFechaCambioEstado',
            'EmpresaNumTasaLiquidacion',
            'EmpresaUserId',
            'EmpresaFechaModificacion',
            'IslaNombreIsla',
            'IslaCodigo',
            'Isla',
            'IslaUserId',
            'IslaFechaModificacion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}