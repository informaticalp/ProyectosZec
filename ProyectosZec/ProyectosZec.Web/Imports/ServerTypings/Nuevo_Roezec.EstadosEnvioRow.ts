﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EstadosEnvioRow {
        EstadoEnvioId?: number;
        Estado?: string;
    }

    export namespace EstadosEnvioRow {
        export const idProperty = 'EstadoEnvioId';
        export const nameProperty = 'Estado';
        export const localTextPrefix = 'Nuevo_Roezec.EstadosEnvio';
        export const lookupKey = 'Nuevo_Roezec.EstadosEnvio';

        export function getLookup(): Q.Lookup<EstadosEnvioRow> {
            return Q.getLookup<EstadosEnvioRow>('Nuevo_Roezec.EstadosEnvio');
        }
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export declare const enum Fields {
            EstadoEnvioId = "EstadoEnvioId",
            Estado = "Estado"
        }
    }
}

