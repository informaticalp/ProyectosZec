﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface SentidosresolucionRow {
        SentidoResolucionId?: number;
        Sentido?: string;
    }

    export namespace SentidosresolucionRow {
        export const idProperty = 'SentidoResolucionId';
        export const nameProperty = 'Sentido';
        export const localTextPrefix = 'Nuevo_Roezec.Sentidosresolucion';
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export namespace Fields {
            export declare const SentidoResolucionId;
            export declare const Sentido;
        }

        [
            'SentidoResolucionId',
            'Sentido'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}