﻿namespace ProyectosZec.Nuevo_Roezec {
    export namespace EstadosEnvioService {
        export const baseUrl = 'Nuevo_Roezec/EstadosEnvio';

        export declare function Create(request: Serenity.SaveRequest<EstadosEnvioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<EstadosEnvioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EstadosEnvioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<EstadosEnvioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Nuevo_Roezec/EstadosEnvio/Create",
            Update = "Nuevo_Roezec/EstadosEnvio/Update",
            Delete = "Nuevo_Roezec/EstadosEnvio/Delete",
            Retrieve = "Nuevo_Roezec/EstadosEnvio/Retrieve",
            List = "Nuevo_Roezec/EstadosEnvio/List"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List'
        ].forEach(x => {
            (<any>EstadosEnvioService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

