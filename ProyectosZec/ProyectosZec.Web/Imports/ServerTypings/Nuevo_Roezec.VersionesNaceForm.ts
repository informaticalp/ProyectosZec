﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface VersionesNaceForm {
        Version: Serenity.StringEditor;
        Fecha: Serenity.DateEditor;
    }

    export class VersionesNaceForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.VersionesNace';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!VersionesNaceForm.init)  {
                VersionesNaceForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.DateEditor;

                Q.initFormType(VersionesNaceForm, [
                    'Version', w0,
                    'Fecha', w1
                ]);
            }
        }
    }
}

