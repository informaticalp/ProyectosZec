﻿
namespace ProyectosZec.Roezec {
    export class InscritasForm extends Serenity.PrefixedContext {
        static formKey = 'Roezec.Inscritas';
    }

    export interface InscritasForm {
        IdEmpresa: Serenity.IntegerEditor;
        NumeroAsiento: Serenity.IntegerEditor;
        NumeroLiquidacionTasa: Serenity.StringEditor;
        FechaSolicitud: Serenity.DateEditor;
        FechaResolucion: Serenity.DateEditor;
        FechaNotificacion: Serenity.DateEditor;
        Observaciones: Serenity.StringEditor;
        FechaAlta: Serenity.DateEditor;
        UsrAlta: Serenity.StringEditor;
    }

    [,
        ['IdEmpresa', () => Serenity.IntegerEditor],
        ['NumeroAsiento', () => Serenity.IntegerEditor],
        ['NumeroLiquidacionTasa', () => Serenity.StringEditor],
        ['FechaSolicitud', () => Serenity.DateEditor],
        ['FechaResolucion', () => Serenity.DateEditor],
        ['FechaNotificacion', () => Serenity.DateEditor],
        ['Observaciones', () => Serenity.StringEditor],
        ['FechaAlta', () => Serenity.DateEditor],
        ['UsrAlta', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(InscritasForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}