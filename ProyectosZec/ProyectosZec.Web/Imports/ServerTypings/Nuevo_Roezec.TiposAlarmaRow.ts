﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface TiposAlarmaRow {
        TipoAlarmaId?: number;
        Texto?: string;
        DiasAviso?: number;
        Email?: string;
    }

    export namespace TiposAlarmaRow {
        export const idProperty = 'TipoAlarmaId';
        export const nameProperty = 'Texto';
        export const localTextPrefix = 'Nuevo_Roezec.TiposAlarma';
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export namespace Fields {
            export declare const TipoAlarmaId;
            export declare const Texto;
            export declare const DiasAviso;
            export declare const Email;
        }

        [
            'TipoAlarmaId',
            'Texto',
            'DiasAviso',
            'Email'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}