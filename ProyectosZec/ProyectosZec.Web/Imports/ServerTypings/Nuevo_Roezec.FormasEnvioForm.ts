﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface FormasEnvioForm {
        Forma: Serenity.StringEditor;
    }

    export class FormasEnvioForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.FormasEnvio';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!FormasEnvioForm.init)  {
                FormasEnvioForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;

                Q.initFormType(FormasEnvioForm, [
                    'Forma', w0
                ]);
            }
        }
    }
}

