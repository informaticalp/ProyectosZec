﻿
namespace ProyectosZec.Registro {
    export interface RegistroRow {
        RegistroId?: number;
        NumeroRegistro?: string;
        FechaRegistro?: string;
        TipoRegistroId?: number;
        Titulo?: string;
        Ficheros?: string;
        Observaciones?: string;
        TipoRegistro?: string;
		UserId?: number;
		FechaModificacion?: string;
    }

    export namespace RegistroRow {
        export const idProperty = 'RegistroId';
        export const nameProperty = 'NumeroRegistro';
        export const localTextPrefix = 'Registro.Registro';
        export const deletePermission = 'Registro:Modify';
        export const insertPermission = 'Registro:Modify';
        export const readPermission = 'Registro:Read';
        export const updatePermission = 'Registro:Modify';

        export namespace Fields {
            export declare const RegistroId;
            export declare const NumeroRegistro;
            export declare const FechaRegistro;
            export declare const TipoRegistroId;
            export declare const Titulo;
            export declare const Ficheros;
            export declare const Observaciones;
            export declare const TipoRegistro;
			export declare const UserId;
			export declare const FechaModificacion;
        }

        [
            'RegistroId',
            'NumeroRegistro',
            'FechaRegistro',
            'TipoRegistroId',
            'Titulo',
            'Ficheros',
            'Observaciones',
            'TipoRegistro',
			'UserId',
			'FechaModificacion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}