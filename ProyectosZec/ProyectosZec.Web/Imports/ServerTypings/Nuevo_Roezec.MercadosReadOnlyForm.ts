﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class MercadosReadOnlyForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.MercadosReadOnly';
    }

    export interface MercadosReadOnlyForm {
        EmpresaId: Serenity.IntegerEditor;
        MercadoId: Serenity.IntegerEditor;
    }

    [,
        ['EmpresaId', () => Serenity.IntegerEditor],
        ['MercadoId', () => Serenity.IntegerEditor]
    ].forEach(x => Object.defineProperty(MercadosReadOnlyForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}