﻿
namespace ProyectosZec.ENS {
    export class ServiciosForm extends Serenity.PrefixedContext {
        static formKey = 'ENS.Servicios';
    }

    export interface ServiciosForm {
        Servicio: Serenity.StringEditor;
    }

    [,
        ['Servicio', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(ServiciosForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}