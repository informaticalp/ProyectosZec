﻿
namespace ProyectosZec.ENS {
    export interface ServiciosRow {
        ServicioId?: number;
        Servicio?: string;
    }

    export namespace ServiciosRow {
        export const idProperty = 'ServicioId';
        export const nameProperty = 'Servicio';
        export const localTextPrefix = 'ENS.Servicios';
        export const deletePermission = 'Ens:General';
        export const insertPermission = 'Ens:General';
        export const readPermission = 'Ens:General';
        export const updatePermission = 'Ens:General';

        export namespace Fields {
            export declare const ServicioId;
            export declare const Servicio;
        }

        [
            'ServicioId',
            'Servicio'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}