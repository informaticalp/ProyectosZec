﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface MercadosRow {
        MercadoId?: number;
        Mercado?: string;
    }

    export namespace MercadosRow {
        export const idProperty = 'MercadoId';
        export const nameProperty = 'Mercado';
        export const localTextPrefix = 'Nuevo_Roezec.Mercados';
        export const lookupKey = 'Nuevo_Roezec.Mercados';

        export function getLookup(): Q.Lookup<MercadosRow> {
            return Q.getLookup<MercadosRow>('Nuevo_Roezec.Mercados');
        }
        export const deletePermission = 'Roezec:Delete';
        export const insertPermission = 'Roezec:Modify';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export declare const enum Fields {
            MercadoId = "MercadoId",
            Mercado = "Mercado"
        }
    }
}

