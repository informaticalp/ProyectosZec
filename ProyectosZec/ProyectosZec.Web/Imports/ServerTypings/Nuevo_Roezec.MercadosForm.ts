﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface MercadosForm {
        Mercado: Serenity.StringEditor;
    }

    export class MercadosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Mercados';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!MercadosForm.init)  {
                MercadosForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;

                Q.initFormType(MercadosForm, [
                    'Mercado', w0
                ]);
            }
        }
    }
}

