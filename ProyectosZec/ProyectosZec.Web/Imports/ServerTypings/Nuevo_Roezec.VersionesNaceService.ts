﻿namespace ProyectosZec.Nuevo_Roezec {
    export namespace VersionesNaceService {
        export const baseUrl = 'Nuevo_Roezec/VersionesNace';

        export declare function Create(request: Serenity.SaveRequest<VersionesNaceRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<VersionesNaceRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<VersionesNaceRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<VersionesNaceRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Nuevo_Roezec/VersionesNace/Create",
            Update = "Nuevo_Roezec/VersionesNace/Update",
            Delete = "Nuevo_Roezec/VersionesNace/Delete",
            Retrieve = "Nuevo_Roezec/VersionesNace/Retrieve",
            List = "Nuevo_Roezec/VersionesNace/List"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List'
        ].forEach(x => {
            (<any>VersionesNaceService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

