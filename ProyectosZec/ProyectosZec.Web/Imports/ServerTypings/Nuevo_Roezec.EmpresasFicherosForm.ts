﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class EmpresasFicherosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EmpresasFicheros';
    }

    export interface EmpresasFicherosForm {
        EmpresaId: Serenity.IntegerEditor;
        Fichero: Serenity.StringEditor;
        Nombre: Serenity.StringEditor;
        Observaciones: Serenity.StringEditor;
        UserId: Serenity.IntegerEditor;
        FechaModificacion: Serenity.DateEditor;
    }

    [,
        ['EmpresaId', () => Serenity.IntegerEditor],
        ['Fichero', () => Serenity.StringEditor],
        ['Nombre', () => Serenity.StringEditor],
        ['Observaciones', () => Serenity.StringEditor],
        ['UserId', () => Serenity.IntegerEditor],
        ['FechaModificacion', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(EmpresasFicherosForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}