﻿
namespace ProyectosZec.ENS {
    export class PasswordsForm extends Serenity.PrefixedContext {
        static formKey = 'ENS.Passwords';
    }

    export interface PasswordsForm {
        Aplicacion: Serenity.StringEditor;
        Acceso: Serenity.StringEditor;
        Usuario: Serenity.StringEditor;
        Password: Serenity.StringEditor;
        Url: Serenity.StringEditor;
        TipoId: Serenity.IntegerEditor;
    }

    [,
        ['Aplicacion', () => Serenity.StringEditor],
        ['Acceso', () => Serenity.StringEditor],
        ['Usuario', () => Serenity.StringEditor],
        ['Password', () => Serenity.StringEditor],
        ['Url', () => Serenity.StringEditor],
        ['TipoId', () => Serenity.IntegerEditor]
    ].forEach(x => Object.defineProperty(PasswordsForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}