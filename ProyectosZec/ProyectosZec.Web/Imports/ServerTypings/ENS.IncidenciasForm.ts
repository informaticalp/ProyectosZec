﻿
namespace ProyectosZec.ENS {
    export class IncidenciasForm extends Serenity.PrefixedContext {
        static formKey = 'ENS.Incidencias';
    }

    export interface IncidenciasForm {
        ServicioId: Serenity.IntegerEditor;
        SeveridadId: Serenity.IntegerEditor;
        Apertura: Serenity.DateEditor;
        Cierre: Serenity.DateEditor;
        Observaciones: Serenity.StringEditor;
    }

    [,
        ['ServicioId', () => Serenity.IntegerEditor],
        ['SeveridadId', () => Serenity.IntegerEditor],
        ['Apertura', () => Serenity.DateEditor],
        ['Cierre', () => Serenity.DateEditor],
        ['Observaciones', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(IncidenciasForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}