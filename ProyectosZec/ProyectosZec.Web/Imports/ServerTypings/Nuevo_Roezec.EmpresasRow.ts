﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EmpresasRow {
        EmpresaId?: number;
        Razon?: string;
        FormaJuridicaId?: number;
        TecnicoId?: number;
        Cif?: string;
        Direccion?: string;
        Poblacion?: string;
        Cp?: string;
        IslaId?: number;
        TelefonoFijo?: string;
        Movil?: string;
        Email?: string;
        ProyectoId?: number;
        Expediente?: string;
        MotivoExencion?: string;
        TipologiaCapitalId?: number;
        TipoGarantiaTasaId?: number;
        EmpleoTraspasado?: number;
        Empleo6Meses?: number;
        EmpleoPromedio?: number;
        EmpleoPromedio2Anos?: number;
        InversionTraspasada?: number;
        Inversion2Anos?: number;
        EstadoEmpresaId?: number;
        FechaCambioEstado?: string;
        NumTasaLiquidacion?: string;
        FormaJuridicaJuridica?: string;
        TecnicoNombreTecnico?: string;
        Tecnico?: string;
        IslaNombreIsla?: string;
        Isla?: string;
        TipologiaCapitalCapital?: string;
        TipoGarantiaTasaGarantiaTasa?: string;
        EstadoEmpresaEstado?: string;
        ContactosList?: EmpresasContactosRow[];
        NacesList?: EmpresasNaceRow[];
        HistorialList?: HistorialEmpresasRow[];
        CapitalList?: ProcedenciaCapitalRow[];
        MercadosList?: EmpresasMercadosRow[];
		EmpleosList?: EmpresasEmpleosRow[];
		FicherosList?: EmpresasFicherosRow[];
    }

    export namespace EmpresasRow {
        export const idProperty = 'EmpresaId';
        export const nameProperty = 'Razon';
        export const localTextPrefix = 'Nuevo_Roezec.Empresas';
        export const lookupKey = 'Nuevo_Roezec.Empresas';

        export function getLookup(): Q.Lookup<EmpresasRow> {
            return Q.getLookup<EmpresasRow>('Nuevo_Roezec.Empresas');
        }
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Insert';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export declare const enum Fields {
            EmpresaId = "EmpresaId",
            Razon = "Razon",
            FormaJuridicaId = "FormaJuridicaId",
            TecnicoId = "TecnicoId",
            Cif = "Cif",
            Direccion = "Direccion",
            Poblacion = "Poblacion",
            Cp = "Cp",
            IslaId = "IslaId",
            TelefonoFijo = "TelefonoFijo",
            Movil = "Movil",
            Email = "Email",
            ProyectoId = "ProyectoId",
            Expediente = "Expediente",
            MotivoExencion = "MotivoExencion",
            TipologiaCapitalId = "TipologiaCapitalId",
            TipoGarantiaTasaId = "TipoGarantiaTasaId",
            EmpleoTraspasado = "EmpleoTraspasado",
            Empleo6Meses = "Empleo6Meses",
            EmpleoPromedio = "EmpleoPromedio",
            EmpleoPromedio2Anos = "EmpleoPromedio2Anos",
            InversionTraspasada = "InversionTraspasada",
            Inversion2Anos = "Inversion2Anos",
            EstadoEmpresaId = "EstadoEmpresaId",
            FechaCambioEstado = "FechaCambioEstado",
            NumTasaLiquidacion = "NumTasaLiquidacion",
            FormaJuridicaJuridica = "FormaJuridicaJuridica",
            TecnicoNombreTecnico = "TecnicoNombreTecnico",
            Tecnico = "Tecnico",
            IslaNombreIsla = "IslaNombreIsla",
            Isla = "Isla",
            TipologiaCapitalCapital = "TipologiaCapitalCapital",
            TipoGarantiaTasaGarantiaTasa = "TipoGarantiaTasaGarantiaTasa",
            EstadoEmpresaEstado = "EstadoEmpresaEstado",
            ContactosList = "ContactosList",
            NacesList = "NacesList",
            HistorialList = "HistorialList",
            CapitalList = "CapitalList",
			EmpleosList = "EmpleosList",
            MercadosList = "MercadosList",
			FicherosList = "FicherosList"
        }
    }
}

