﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class PlazosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Plazos';
    }

    export interface PlazosForm {
        Plazo: Serenity.StringEditor;
    }

    [,
        ['Plazo', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(PlazosForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}