﻿
namespace ProyectosZec.Kairos {
    export interface HoyRow {
        Id?: number;
        Nombre?: string;
        IdDepartamento?: number;
        SedId?: number;
        Sede?: string;
        Estado?: string;
    }

    export namespace HoyRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Nombre';
        export const localTextPrefix = 'Kairos.Hoy';
        export const deletePermission = 'Kairos:Delete';
        export const insertPermission = 'Kairos:Insert';
        export const readPermission = 'Kairos:Hoy';
        export const updatePermission = 'Kairos:Modify';

        export namespace Fields {
            export declare const Id;
            export declare const Nombre;
            export declare const IdDepartamento;
            export declare const SedeId;
            export declare const Sede;
            export declare const Estado;
        }

        [
            'Id',
            'Nombre',
            'IdDepartamento',
            'SedId',
            'Sede',
            'Estado'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}