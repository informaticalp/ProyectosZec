﻿
namespace ProyectosZec.ENS {
    export interface SeveridadesRow {
        SeveridadId?: number;
        Severidad?: string;
    }

    export namespace SeveridadesRow {
        export const idProperty = 'SeveridadId';
        export const nameProperty = 'Severidad';
        export const localTextPrefix = 'ENS.Severidades';
        export const deletePermission = 'Ens:General';
        export const insertPermission = 'Ens:General';
        export const readPermission = 'Ens:General';
        export const updatePermission = 'Ens:General';

        export namespace Fields {
            export declare const SeveridadId;
            export declare const Severidad;
        }

        [
            'SeveridadId',
            'Severidad'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}