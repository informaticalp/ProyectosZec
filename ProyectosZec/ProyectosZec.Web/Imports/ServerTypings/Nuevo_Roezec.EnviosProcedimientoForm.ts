﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EnviosProcedimientoForm {
        HistorialId: Serenity.IntegerEditor;
        TipoEnvioId: Serenity.IntegerEditor;
        FechaEnvio: Serenity.DateEditor;
        ContactoEnvioId: Serenity.IntegerEditor;
        FechaRecepcion: Serenity.DateEditor;
        ContactoAcuseId: Serenity.IntegerEditor;
    }

    export class EnviosProcedimientoForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EnviosProcedimiento';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EnviosProcedimientoForm.init)  {
                EnviosProcedimientoForm.init = true;

                var s = Serenity;
                var w0 = s.IntegerEditor;
                var w1 = s.DateEditor;

                Q.initFormType(EnviosProcedimientoForm, [
                    'HistorialId', w0,
                    'TipoEnvioId', w0,
                    'FechaEnvio', w1,
                    'ContactoEnvioId', w0,
                    'FechaRecepcion', w1,
                    'ContactoAcuseId', w0
                ]);
            }
        }
    }
}

