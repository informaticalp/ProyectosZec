﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface EmpresasFicherosRow {
        EmpresasFicheroId?: number;
        EmpresaId?: number;
        Fichero?: string;
        Nombre?: string;
        Observaciones?: string;
        UserId?: number;
        FechaModificacion?: string;
        EmpresaRazon?: string;
        EmpresaDescripcion?: string;
        EmpresaFormaJuridicaId?: number;
        EmpresaTecnicoId?: number;
        EmpresaCif?: string;
        EmpresaDireccion?: string;
        EmpresaPoblacion?: string;
        EmpresaCp?: number;
        EmpresaIslaId?: number;
        EmpresaTelefonoFijo?: string;
        EmpresaMovil?: string;
        EmpresaCapitalSocial?: number;
        EmpresaEmail?: string;
        EmpresaProyectoId?: number;
        EmpresaNumeroRoezec?: number;
        EmpresaExpediente?: string;
        EmpresaMotivoExencion?: string;
        EmpresaTipologiaCapitalId?: number;
        EmpresaTipoGarantiaTasaId?: number;
        EmpresaEmpleoTraspasado?: number;
        EmpresaEmpleo6Meses?: number;
        EmpresaEmpleoPromedio?: number;
        EmpresaEmpleoPromedio2Anos?: number;
        EmpresaInversionTraspasada?: number;
        EmpresaInversion2Anos?: number;
        EmpresaEstadoEmpresaId?: number;
        EmpresaFechaAlta?: string;
        EmpresaFechaCambioEstado?: string;
        EmpresaNumTasaLiquidacion?: string;
        EmpresaUserId?: number;
        EmpresaFechaModificacion?: string;
    }

    export namespace EmpresasFicherosRow {
        export const idProperty = 'EmpresasFicheroId';
        export const nameProperty = 'Nombre';
        export const localTextPrefix = 'Nuevo_Roezec.EmpresasFicheros';
        export const deletePermission = 'Roezec:Modify';
        export const insertPermission = 'Roezec:Insert';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export namespace Fields {
            export declare const EmpresasFicheroId;
            export declare const EmpresaId;
            export declare const Fichero;
            export declare const Nombre;
            export declare const Observaciones;
            export declare const UserId;
            export declare const FechaModificacion;
            export declare const EmpresaRazon;
            export declare const EmpresaDescripcion;
            export declare const EmpresaFormaJuridicaId;
            export declare const EmpresaTecnicoId;
            export declare const EmpresaCif;
            export declare const EmpresaDireccion;
            export declare const EmpresaPoblacion;
            export declare const EmpresaCp;
            export declare const EmpresaIslaId;
            export declare const EmpresaTelefonoFijo;
            export declare const EmpresaMovil;
            export declare const EmpresaCapitalSocial;
            export declare const EmpresaEmail;
            export declare const EmpresaProyectoId;
            export declare const EmpresaNumeroRoezec;
            export declare const EmpresaExpediente;
            export declare const EmpresaMotivoExencion;
            export declare const EmpresaTipologiaCapitalId;
            export declare const EmpresaTipoGarantiaTasaId;
            export declare const EmpresaEmpleoTraspasado;
            export declare const EmpresaEmpleo6Meses;
            export declare const EmpresaEmpleoPromedio;
            export declare const EmpresaEmpleoPromedio2Anos;
            export declare const EmpresaInversionTraspasada;
            export declare const EmpresaInversion2Anos;
            export declare const EmpresaEstadoEmpresaId;
            export declare const EmpresaFechaAlta;
            export declare const EmpresaFechaCambioEstado;
            export declare const EmpresaNumTasaLiquidacion;
            export declare const EmpresaUserId;
            export declare const EmpresaFechaModificacion;
        }

        [
            'EmpresasFicheroId',
            'EmpresaId',
            'Fichero',
            'Nombre',
            'Observaciones',
            'UserId',
            'FechaModificacion',
            'EmpresaRazon',
            'EmpresaDescripcion',
            'EmpresaFormaJuridicaId',
            'EmpresaTecnicoId',
            'EmpresaCif',
            'EmpresaDireccion',
            'EmpresaPoblacion',
            'EmpresaCp',
            'EmpresaIslaId',
            'EmpresaTelefonoFijo',
            'EmpresaMovil',
            'EmpresaCapitalSocial',
            'EmpresaEmail',
            'EmpresaProyectoId',
            'EmpresaNumeroRoezec',
            'EmpresaExpediente',
            'EmpresaMotivoExencion',
            'EmpresaTipologiaCapitalId',
            'EmpresaTipoGarantiaTasaId',
            'EmpresaEmpleoTraspasado',
            'EmpresaEmpleo6Meses',
            'EmpresaEmpleoPromedio',
            'EmpresaEmpleoPromedio2Anos',
            'EmpresaInversionTraspasada',
            'EmpresaInversion2Anos',
            'EmpresaEstadoEmpresaId',
            'EmpresaFechaAlta',
            'EmpresaFechaCambioEstado',
            'EmpresaNumTasaLiquidacion',
            'EmpresaUserId',
            'EmpresaFechaModificacion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}