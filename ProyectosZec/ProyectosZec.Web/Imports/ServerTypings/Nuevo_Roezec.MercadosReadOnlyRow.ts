﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface MercadosReadOnlyRow {
        EmpresaMercadoId?: number;
        EmpresaId?: number;
        MercadoId?: number;
        EmpresaRazon?: string;
        EmpresaFormaJuridicaId?: number;
        EmpresaTecnicoId?: number;
        EmpresaCif?: string;
        EmpresaDireccion?: string;
        EmpresaPoblacion?: string;
        EmpresaCp?: number;
        EmpresaIslaId?: number;
        EmpresaTelefonoFijo?: string;
        EmpresaMovil?: string;
        EmpresaEmail?: string;
        EmpresaProyectoId?: number;
        EmpresaExpediente?: string;
        EmpresaMotivoExencion?: string;
        EmpresaTipologiaCapitalId?: number;
        EmpresaTipoGarantiaTasaId?: number;
        EmpresaEmpleoTraspasado?: number;
        EmpresaEmpleo6Meses?: number;
        EmpresaEmpleoPromedio?: number;
        EmpresaEmpleoPromedio2Anos?: number;
        EmpresaInversionTraspasada?: number;
        EmpresaInversion2Anos?: number;
        EmpresaEstadoEmpresaId?: number;
        EmpresaFechaCambioEstado?: string;
        EmpresaNumTasaLiquidacion?: string;
        Mercado?: string;
    }

    export namespace MercadosReadOnlyRow {
        export const idProperty = 'EmpresaMercadoId';
        export const localTextPrefix = 'Nuevo_Roezec.MercadosReadOnly';
        export const deletePermission = 'Roezec::General';
        export const insertPermission = 'Roezec::General';
        export const readPermission = 'Roezec::General';
        export const updatePermission = 'Roezec::General';

        export namespace Fields {
            export declare const EmpresaMercadoId;
            export declare const EmpresaId;
            export declare const MercadoId;
            export declare const EmpresaRazon;
            export declare const EmpresaFormaJuridicaId;
            export declare const EmpresaTecnicoId;
            export declare const EmpresaCif;
            export declare const EmpresaDireccion;
            export declare const EmpresaPoblacion;
            export declare const EmpresaCp;
            export declare const EmpresaIslaId;
            export declare const EmpresaTelefonoFijo;
            export declare const EmpresaMovil;
            export declare const EmpresaEmail;
            export declare const EmpresaProyectoId;
            export declare const EmpresaExpediente;
            export declare const EmpresaMotivoExencion;
            export declare const EmpresaTipologiaCapitalId;
            export declare const EmpresaTipoGarantiaTasaId;
            export declare const EmpresaEmpleoTraspasado;
            export declare const EmpresaEmpleo6Meses;
            export declare const EmpresaEmpleoPromedio;
            export declare const EmpresaEmpleoPromedio2Anos;
            export declare const EmpresaInversionTraspasada;
            export declare const EmpresaInversion2Anos;
            export declare const EmpresaEstadoEmpresaId;
            export declare const EmpresaFechaCambioEstado;
            export declare const EmpresaNumTasaLiquidacion;
            export declare const Mercado;
        }

        [
            'EmpresaMercadoId',
            'EmpresaId',
            'MercadoId',
            'EmpresaRazon',
            'EmpresaFormaJuridicaId',
            'EmpresaTecnicoId',
            'EmpresaCif',
            'EmpresaDireccion',
            'EmpresaPoblacion',
            'EmpresaCp',
            'EmpresaIslaId',
            'EmpresaTelefonoFijo',
            'EmpresaMovil',
            'EmpresaEmail',
            'EmpresaProyectoId',
            'EmpresaExpediente',
            'EmpresaMotivoExencion',
            'EmpresaTipologiaCapitalId',
            'EmpresaTipoGarantiaTasaId',
            'EmpresaEmpleoTraspasado',
            'EmpresaEmpleo6Meses',
            'EmpresaEmpleoPromedio',
            'EmpresaEmpleoPromedio2Anos',
            'EmpresaInversionTraspasada',
            'EmpresaInversion2Anos',
            'EmpresaEstadoEmpresaId',
            'EmpresaFechaCambioEstado',
            'EmpresaNumTasaLiquidacion',
            'Mercado'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}