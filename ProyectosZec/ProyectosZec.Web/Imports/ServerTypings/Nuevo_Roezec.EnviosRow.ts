﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EnviosRow {
        EnvioId?: number;
        HistorialId?: number;
        TipoEnvioId?: number;
        FormaEnvioId?: number;
        EstadoEnvioId?: number;
        FechaEnvio?: string;
        ContactoEnvioId?: number;
        FechaRecepcion?: string;
        ContactoAcuseId?: number;
        TipoEnvioTipo?: string;
        ContactoEnvioNombre?: string;
        ContactoEnvioApellidos?: string;
        ContactoEnvioNif?: string;
        ContactoEnvioTelefonoFijo?: string;
        ContactoEnvioMovil?: string;
        ContactoEnvioIdiomaId?: number;
        ContactoEnvioEmail?: string;
        ContactoEnvioFullName?: string;
        ContactoAcuseFullName?: string;
        FormaEnvioForma?: string;
        EstadoEnvioEstado?: string;
    }

    export namespace EnviosRow {
        export const idProperty = 'EnvioId';
        export const nameProperty = 'ContactoEnvioFullName';
        export const localTextPrefix = 'Nuevo_Roezec.Envios';
        export const deletePermission = 'Roezec:Delete';
        export const insertPermission = 'Roezec:Insert';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export declare const enum Fields {
            EnvioId = "EnvioId",
            HistorialId = "HistorialId",
            TipoEnvioId = "TipoEnvioId",
            FormaEnvioId = "FormaEnvioId",
            EstadoEnvioId = "EstadoEnvioId",
            FechaEnvio = "FechaEnvio",
            ContactoEnvioId = "ContactoEnvioId",
            FechaRecepcion = "FechaRecepcion",
            ContactoAcuseId = "ContactoAcuseId",
            TipoEnvioTipo = "TipoEnvioTipo",
            ContactoEnvioNombre = "ContactoEnvioNombre",
            ContactoEnvioApellidos = "ContactoEnvioApellidos",
            ContactoEnvioNif = "ContactoEnvioNif",
            ContactoEnvioTelefonoFijo = "ContactoEnvioTelefonoFijo",
            ContactoEnvioMovil = "ContactoEnvioMovil",
            ContactoEnvioIdiomaId = "ContactoEnvioIdiomaId",
            ContactoEnvioEmail = "ContactoEnvioEmail",
            ContactoEnvioFullName = "ContactoEnvioFullName",
            ContactoAcuseFullName = "ContactoAcuseFullName",
            FormaEnvioForma = "FormaEnvioForma",
            EstadoEnvioEstado = "EstadoEnvioEstado"
        }
    }
}

