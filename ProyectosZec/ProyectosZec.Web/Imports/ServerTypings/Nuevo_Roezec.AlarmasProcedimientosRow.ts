﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface AlarmasProcedimientosRow {
        AlarmaProcedimientoId?: number;
        ProcedimientoId?: number;
		TipoAlarmaId?: number;
        SentidoResolucionId?: number;
        UserId?: number;
		UserName: string;
		DisplayName: string;
        FechaModificacion?: string;
        Procedimiento?: string;
		TipoAlarma: string;
        ProcedimientoEstadoEmpresaId?: number;
        ProcedimientoEstadoEmpresaIdDesfavorable?: number;
        ProcedimientoUserId?: number;
        ProcedimientoFechaModificacion?: string;
        ProcedimientoAlarmaFavorableId?: number;
        ProcedimientoAlarmaDesfavorableId?: number;
        SentidoResolucion?: string;
    }

    export namespace AlarmasProcedimientosRow {
        export const idProperty = 'AlarmaProcedimientoId';
		export const nameProperty = 'TipoAlarma';
        export const localTextPrefix = 'Nuevo_Roezec.AlarmasProcedimientos';
        export const deletePermission = 'Roezec:Delete';
        export const insertPermission = 'Roezec:Insert';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Update';

        export namespace Fields {
            export declare const AlarmaProcedimientoId;
            export declare const ProcedimientoId;
			export declare const TipoAlarmaId;
            export declare const SentidoResolucionId;
            export declare const UserId;
			export declare const UserName;
			export declare const DisplayName;
            export declare const FechaModificacion;
            export declare const Procedimiento;
			export declare const TipoAlarma;
            export declare const ProcedimientoEstadoEmpresaId;
            export declare const ProcedimientoEstadoEmpresaIdDesfavorable;
            export declare const ProcedimientoUserId;
            export declare const ProcedimientoFechaModificacion;
            export declare const ProcedimientoAlarmaFavorableId;
            export declare const ProcedimientoAlarmaDesfavorableId;
            export declare const SentidoResolucion;
        }

        [
            'AlarmaProcedimientoId',
            'ProcedimientoId',
            'SentidoResolucionId',
            'UserId',
			'UserName',
			'DisplayName',
            'FechaModificacion',
            'Procedimiento',
			'TipoAlarma',
            'ProcedimientoEstadoEmpresaId',
            'ProcedimientoEstadoEmpresaIdDesfavorable',
            'ProcedimientoUserId',
            'ProcedimientoFechaModificacion',
            'ProcedimientoAlarmaFavorableId',
            'ProcedimientoAlarmaDesfavorableId',
            'SentidoResolucion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}