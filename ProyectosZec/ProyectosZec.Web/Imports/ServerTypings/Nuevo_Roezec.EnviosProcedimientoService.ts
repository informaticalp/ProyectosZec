﻿namespace ProyectosZec.Nuevo_Roezec {
    export namespace EnviosProcedimientoService {
        export const baseUrl = 'Nuevo_Roezec/EnviosProcedimiento';

        export declare function Create(request: Serenity.SaveRequest<EnviosProcedimientoRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<EnviosProcedimientoRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<EnviosProcedimientoRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<EnviosProcedimientoRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Nuevo_Roezec/EnviosProcedimiento/Create",
            Update = "Nuevo_Roezec/EnviosProcedimiento/Update",
            Delete = "Nuevo_Roezec/EnviosProcedimiento/Delete",
            Retrieve = "Nuevo_Roezec/EnviosProcedimiento/Retrieve",
            List = "Nuevo_Roezec/EnviosProcedimiento/List"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List'
        ].forEach(x => {
            (<any>EnviosProcedimientoService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

