﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class TiposDireccionesForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.TiposDirecciones';
    }

    export interface TiposDireccionesForm {
        TipoDireccion: Serenity.StringEditor;
    }

    [,
        ['TipoDireccion', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(TiposDireccionesForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}