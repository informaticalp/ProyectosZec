﻿namespace ProyectosZec.Nuevo_Roezec {
    export namespace ProcedenciaCapitalService {
        export const baseUrl = 'Nuevo_Roezec/ProcedenciaCapital';

        export declare function Create(request: Serenity.SaveRequest<ProcedenciaCapitalRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<ProcedenciaCapitalRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<ProcedenciaCapitalRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<ProcedenciaCapitalRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Nuevo_Roezec/ProcedenciaCapital/Create",
            Update = "Nuevo_Roezec/ProcedenciaCapital/Update",
            Delete = "Nuevo_Roezec/ProcedenciaCapital/Delete",
            Retrieve = "Nuevo_Roezec/ProcedenciaCapital/Retrieve",
            List = "Nuevo_Roezec/ProcedenciaCapital/List"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List'
        ].forEach(x => {
            (<any>ProcedenciaCapitalService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

