﻿
namespace ProyectosZec.Roezec {
    export class EmpleosSSForm extends Serenity.PrefixedContext {
        static formKey = 'Roezec.EmpleosSS';
    }

    export interface EmpleosSSForm {
        Fecha: Serenity.DateEditor;
        Empleos: Serenity.DecimalEditor;
    }

    [,
        ['Fecha', () => Serenity.DateEditor],
        ['Empleos', () => Serenity.DecimalEditor]
    ].forEach(x => Object.defineProperty(EmpleosSSForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}