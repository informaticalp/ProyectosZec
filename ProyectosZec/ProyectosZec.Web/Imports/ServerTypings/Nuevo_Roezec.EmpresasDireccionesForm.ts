﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class EmpresasDireccionesForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EmpresasDirecciones';
    }

    export interface EmpresasDireccionesForm {
        EmpresaId: Serenity.IntegerEditor;
        Direccion: Serenity.StringEditor;
        Poblacion: Serenity.StringEditor;
        Cp: Serenity.StringEditor;
        IslaId: Serenity.IntegerEditor;
        Desde: Serenity.DateEditor;
        Hasta: Serenity.DateEditor;
        UserId: Serenity.IntegerEditor;
        FechaModificacion: Serenity.DateEditor;
    }

    [,
        ['EmpresaId', () => Serenity.IntegerEditor],
        ['Direccion', () => Serenity.StringEditor],
        ['Poblacion', () => Serenity.StringEditor],
        ['Cp', () => Serenity.StringEditor],
        ['IslaId', () => Serenity.IntegerEditor],
        ['Desde', () => Serenity.DateEditor],
        ['Hasta', () => Serenity.DateEditor],
        ['UserId', () => Serenity.IntegerEditor],
        ['FechaModificacion', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(EmpresasDireccionesForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}