﻿
namespace ProyectosZec.Roezec {
    export class RoezecEmpresasSSForm extends Serenity.PrefixedContext {
        static formKey = 'Roezec.RoezecEmpresasSS';
    }

    export interface RoezecEmpresasSSForm {
        DenominacionSocial: Serenity.StringEditor;
        Cif: Serenity.StringEditor;
        Direccion: Serenity.StringEditor;
        Cp: Serenity.StringEditor;
        Poblacion: Serenity.StringEditor;
        Provincia: Serenity.StringEditor;
        Isla: Serenity.StringEditor;
        NotasMarginales: Serenity.StringEditor;
        AnyoExpediente: Serenity.IntegerEditor;
        NumExpediente: Serenity.IntegerEditor;
        Agencia: Serenity.IntegerEditor;
        Tecnico: Serenity.StringEditor;
        FormaJuridica: Serenity.StringEditor;
        Superficie: Serenity.DecimalEditor;
        ExentaAreaAcotada: Serenity.StringEditor;
        MotivosExencion: Serenity.StringEditor;
        ObjetivoEmpleo: Serenity.DecimalEditor;
        ObjetivoInversion: Serenity.DecimalEditor;
        ObservacionesEmpleo: Serenity.StringEditor;
        ObservacionesInversion: Serenity.StringEditor;
        PreEmpleo: Serenity.IntegerEditor;
        PreInversion: Serenity.IntegerEditor;
        TrasEmpleo: Serenity.IntegerEditor;
        TrasInversion: Serenity.IntegerEditor;
        FechaAlta: Serenity.DateEditor;
        FechaModificacion: Serenity.DateEditor;
        FechaBaja: Serenity.DateEditor;
        Situacion: Serenity.StringEditor;
        UsrAlta: Serenity.StringEditor;
        UsrModificacion: Serenity.StringEditor;
        UsrBaja: Serenity.StringEditor;
        Trading: Serenity.IntegerEditor;
        Empleos2021: Serenity.DecimalEditor;
        Empleos2020: Serenity.DecimalEditor;
        Empleos2019: Serenity.DecimalEditor;
        Empleos2018: Serenity.DecimalEditor;
        Empleos2017: Serenity.DecimalEditor;
    }

    [,
        ['DenominacionSocial', () => Serenity.StringEditor],
        ['Cif', () => Serenity.StringEditor],
        ['Direccion', () => Serenity.StringEditor],
        ['Cp', () => Serenity.StringEditor],
        ['Poblacion', () => Serenity.StringEditor],
        ['Provincia', () => Serenity.StringEditor],
        ['Isla', () => Serenity.StringEditor],
        ['NotasMarginales', () => Serenity.StringEditor],
        ['AnyoExpediente', () => Serenity.IntegerEditor],
        ['NumExpediente', () => Serenity.IntegerEditor],
        ['Agencia', () => Serenity.IntegerEditor],
        ['Tecnico', () => Serenity.StringEditor],
        ['FormaJuridica', () => Serenity.StringEditor],
        ['Superficie', () => Serenity.DecimalEditor],
        ['ExentaAreaAcotada', () => Serenity.StringEditor],
        ['MotivosExencion', () => Serenity.StringEditor],
        ['ObjetivoEmpleo', () => Serenity.DecimalEditor],
        ['ObjetivoInversion', () => Serenity.DecimalEditor],
        ['ObservacionesEmpleo', () => Serenity.StringEditor],
        ['ObservacionesInversion', () => Serenity.StringEditor],
        ['PreEmpleo', () => Serenity.IntegerEditor],
        ['PreInversion', () => Serenity.IntegerEditor],
        ['TrasEmpleo', () => Serenity.IntegerEditor],
        ['TrasInversion', () => Serenity.IntegerEditor],
        ['FechaAlta', () => Serenity.DateEditor],
        ['FechaModificacion', () => Serenity.DateEditor],
        ['FechaBaja', () => Serenity.DateEditor],
        ['Situacion', () => Serenity.StringEditor],
        ['UsrAlta', () => Serenity.StringEditor],
        ['UsrModificacion', () => Serenity.StringEditor],
        ['UsrBaja', () => Serenity.StringEditor],
        ['Trading', () => Serenity.IntegerEditor],
        ['Empleos2021', () => Serenity.DecimalEditor],
        ['Empleos2020', () => Serenity.DecimalEditor],
        ['Empleos2019', () => Serenity.DecimalEditor],
        ['Empleos2018', () => Serenity.DecimalEditor],
        ['Empleos2017', () => Serenity.DecimalEditor]
    ].forEach(x => Object.defineProperty(RoezecEmpresasSSForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}