﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface ProcedimientosRow {
        ProcedimientoId?: number;
        Procedimiento?: string;
        Orden?: number;
        EstadoEmpresaId?: number;
		EstadoEmpresaIdDesfavorable?: number;
		EstadoEmpresaEstadoDesfavorable?: string;
        EstadoEmpresaEstado?: string;
		Instrucciones?: string;
		UserId?: number;
		UserName?: string;
		DisplayName?: string;
		Visible? : number;
		NameField?: string;
    }

    export namespace ProcedimientosRow {
        export const idProperty = 'ProcedimientoId';
        export const nameProperty = 'NameField';
        export const localTextPrefix = 'Nuevo_Roezec.Procedimientos';
        export const lookupKey = 'Nuevo_Roezec.Procedimientos';

        export function getLookup(): Q.Lookup<ProcedimientosRow> {
            return Q.getLookup<ProcedimientosRow>('Nuevo_Roezec.Procedimientos');
        }
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export declare const enum Fields {
            ProcedimientoId = "ProcedimientoId",
            Procedimiento = "Procedimiento",
			Orden = "Orden",
			Visible = "Visible",
			NameField = "NameField",
			DisplayName = "DisplayName",
			Instrucciones = "Instrucciones",
            EstadoEmpresaId = "EstadoEmpresaId",
            EstadoEmpresaEstado = "EstadoEmpresaEstado",
			EstadoEmpresaIdDesfavorable = "EstadoEmpresaIdDesfavorable",
			EstadoEmpresaEstadoDesfavorable = "EstadoEmpresaEstadoDesfavorable",
			UserId = "UserId",
			UserName = "UserName"
        }
    }
}

