﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface ProcedenciaCapitalForm {
        PaisId: Serenity.LookupEditor;
        Porcentaje: Serenity.DecimalEditor;
    }

    export class ProcedenciaCapitalForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.ProcedenciaCapital';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!ProcedenciaCapitalForm.init)  {
                ProcedenciaCapitalForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.DecimalEditor;

                Q.initFormType(ProcedenciaCapitalForm, [
                    'PaisId', w0,
                    'Porcentaje', w1
                ]);
            }
        }
    }
}

