﻿
namespace ProyectosZec.Registro {
    export class TiposregistroForm extends Serenity.PrefixedContext {
        static formKey = 'Registro.Tiposregistro';
    }

    export interface TiposregistroForm {
        TipoRegistro: Serenity.StringEditor;
    }

    [,
        ['TipoRegistro', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(TiposregistroForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}