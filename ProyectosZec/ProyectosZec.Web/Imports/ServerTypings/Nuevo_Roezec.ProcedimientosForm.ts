﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface ProcedimientosForm {
        Procedimiento: Serenity.StringEditor;
        Caducidad_dias: Serenity.IntegerEditor;
        EstadoEmpresaId: Serenity.LookupEditor;
    }

    export class ProcedimientosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Procedimientos';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!ProcedimientosForm.init)  {
                ProcedimientosForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;
                var w1 = s.IntegerEditor;
                var w2 = s.LookupEditor;

                Q.initFormType(ProcedimientosForm, [
                    'Procedimiento', w0,
                    'Caducidad_dias', w1,
                    'EstadoEmpresaId', w2
                ]);
            }
        }
    }
}

