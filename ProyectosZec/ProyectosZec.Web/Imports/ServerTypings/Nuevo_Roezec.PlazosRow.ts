﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface PlazosRow {
        PlazoId?: number;
        Plazo?: string;
    }

    export namespace PlazosRow {
        export const idProperty = 'PlazoId';
        export const nameProperty = 'Plazo';
        export const localTextPrefix = 'Nuevo_Roezec.Plazos';
        export const deletePermission = 'Roezec:Read';
        export const insertPermission = 'Roezec:Read';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Read';

        export namespace Fields {
            export declare const PlazoId;
            export declare const Plazo;
        }

        [
            'PlazoId',
            'Plazo'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}