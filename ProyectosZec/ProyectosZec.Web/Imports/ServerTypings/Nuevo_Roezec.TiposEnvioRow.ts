﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface TiposEnvioRow {
        TipoEnvioId?: number;
        Tipo?: string;
    }

    export namespace TiposEnvioRow {
        export const idProperty = 'TipoEnvioId';
        export const nameProperty = 'Tipo';
        export const localTextPrefix = 'Nuevo_Roezec.TiposEnvio';
        export const lookupKey = 'Nuevo_Roezec.TiposEnvio';

        export function getLookup(): Q.Lookup<TiposEnvioRow> {
            return Q.getLookup<TiposEnvioRow>('Nuevo_Roezec.TiposEnvio');
        }
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export declare const enum Fields {
            TipoEnvioId = "TipoEnvioId",
            Tipo = "Tipo"
        }
    }
}

