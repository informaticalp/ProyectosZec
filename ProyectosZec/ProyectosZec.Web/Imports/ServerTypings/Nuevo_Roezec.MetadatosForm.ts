﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class MetadatosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Metadatos';
    }

    export interface MetadatosForm {
        FicheroId: Serenity.IntegerEditor;
        Nombre: Serenity.StringEditor;
        Valor: Serenity.StringEditor;
        FechaModificacion: Serenity.DateEditor;
    }

    [,
        ['FicheroId', () => Serenity.IntegerEditor],
        ['Nombre', () => Serenity.StringEditor],
        ['Valor', () => Serenity.StringEditor],
        ['FechaModificacion', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(MetadatosForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}