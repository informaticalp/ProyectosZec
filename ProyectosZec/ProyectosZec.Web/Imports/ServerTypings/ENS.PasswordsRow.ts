﻿
namespace ProyectosZec.ENS {
    export interface PasswordsRow {
        PasswordId?: number;
        Aplicacion?: string;
        Acceso?: string;
        Usuario?: string;
        Password?: string;
        Url?: string;
        TipoId?: number;
        TipoTipoServicio?: string;
		Observaciones?: string;
    }

    export namespace PasswordsRow {
        export const idProperty = 'PasswordId';
        export const nameProperty = 'Aplicacion';
        export const localTextPrefix = 'ENS.Passwords';
        export const deletePermission = 'Ens:General';
        export const insertPermission = 'Ens:General';
        export const readPermission = 'Ens:General';
        export const updatePermission = 'Ens:General';

        export namespace Fields {
            export declare const PasswordId;
            export declare const Aplicacion;
            export declare const Acceso;
            export declare const Usuario;
            export declare const Password;
            export declare const Url;
            export declare const TipoId;
            export declare const TipoTipoServicio;
			export declare const Observaciones;
        }

        [
            'PasswordId',
            'Aplicacion',
            'Acceso',
            'Usuario',
            'Password',
            'Url',
            'TipoId',
            'TipoTipoServicio',
			'Observaciones'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}