﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface EmpresasNombresRow {
        NombreId?: number;
        EmpresaId?: number;
        Nombre?: string;
        UserId?: number;
        FechaModificacion?: string;
    }

    export namespace EmpresasNombresRow {
        export const idProperty = 'NombreId';
        export const nameProperty = 'Nombre';
        export const localTextPrefix = 'Nuevo_Roezec.EmpresasNombres';
		export const deletePermission = 'Roezec_Old:Delete';
        export const insertPermission = 'Roezec_Old:Insert';
        export const readPermission = 'Roezec_Old:Read';
        export const updatePermission = 'Roezec_Old:Modify';

        export namespace Fields {
            export declare const NombreId;
            export declare const EmpresaId;
            export declare const Nombre;
            export declare const UserId;
            export declare const FechaModificacion;
        }

        [
            'NombreId',
            'EmpresaId',
            'Nombre',
            'UserId',
            'FechaModificacion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}