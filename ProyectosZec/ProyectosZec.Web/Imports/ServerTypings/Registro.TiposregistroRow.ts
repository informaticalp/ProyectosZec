﻿
namespace ProyectosZec.Registro {
    export interface TiposregistroRow {
        TipoRegistroId?: number;
        TipoRegistro?: string;
    }

    export namespace TiposregistroRow {
        export const idProperty = 'TipoRegistroId';
        export const nameProperty = 'TipoRegistro';
        export const localTextPrefix = 'Registro.Tiposregistro';
        export const deletePermission = 'Registro:General';
        export const insertPermission = 'Registro:General';
        export const readPermission = 'Registro:General';
        export const updatePermission = 'Registro:General';

        export namespace Fields {
            export declare const TipoRegistroId;
            export declare const TipoRegistro;
        }

        [
            'TipoRegistroId',
            'TipoRegistro'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}