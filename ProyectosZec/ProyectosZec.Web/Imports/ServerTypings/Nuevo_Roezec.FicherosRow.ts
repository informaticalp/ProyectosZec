﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface FicherosRow {
        FicheroId?: number;
        HistorialId?: number;
        Fichero?: string;
        FechaModificacion?: string;
        HistorialEmpresaId?: number;
        HistorialProcedimientoId?: number;
        HistorialFechaInicio?: string;
        HistorialFechaResolucion?: string;
        HistorialSentidoResolucion?: number;
        HistorialFechaEfecto?: string;
        HistorialAcuseInicio?: string;
        HistorialPersonaAcuseIncioId?: number;
        HistorialAcuseResolucion?: string;
        HistorialPersonaAcuseResolucionId?: number;
        HistorialObservaciones?: string;
        HistorialFicheros?: string;
    }

    export namespace FicherosRow {
        export const idProperty = 'FicheroId';
        export const nameProperty = 'Fichero';
        export const localTextPrefix = 'Nuevo_Roezec.Ficheros';
        export const deletePermission = 'Roezec:Delete';
        export const insertPermission = 'Roezec:Insert';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export namespace Fields {
            export declare const FicheroId;
            export declare const HistorialId;
            export declare const Fichero;
            export declare const FechaModificacion;
            export declare const HistorialEmpresaId;
            export declare const HistorialProcedimientoId;
            export declare const HistorialFechaInicio;
            export declare const HistorialFechaResolucion;
            export declare const HistorialSentidoResolucion;
            export declare const HistorialFechaEfecto;
            export declare const HistorialAcuseInicio;
            export declare const HistorialPersonaAcuseIncioId;
            export declare const HistorialAcuseResolucion;
            export declare const HistorialPersonaAcuseResolucionId;
            export declare const HistorialObservaciones;
            export declare const HistorialFicheros;
        }

        [
            'FicheroId',
            'HistorialId',
            'Fichero',
            'FechaModificacion',
            'HistorialEmpresaId',
            'HistorialProcedimientoId',
            'HistorialFechaInicio',
            'HistorialFechaResolucion',
            'HistorialSentidoResolucion',
            'HistorialFechaEfecto',
            'HistorialAcuseInicio',
            'HistorialPersonaAcuseIncioId',
            'HistorialAcuseResolucion',
            'HistorialPersonaAcuseResolucionId',
            'HistorialObservaciones',
            'HistorialFicheros'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}