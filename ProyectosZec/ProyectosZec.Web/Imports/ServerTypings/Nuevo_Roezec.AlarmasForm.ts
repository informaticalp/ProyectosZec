﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class AlarmasForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Alarmas';
    }

    export interface AlarmasForm {
        TipoAlarmaId: Serenity.IntegerEditor;
        EmpresaId: Serenity.IntegerEditor;
        Activa: Serenity.IntegerEditor;
        FechaCreacion: Serenity.DateEditor;
        UserId: Serenity.IntegerEditor;
        FechaModificacion: Serenity.DateEditor;
    }

    [,
        ['TipoAlarmaId', () => Serenity.IntegerEditor],
        ['EmpresaId', () => Serenity.IntegerEditor],
        ['Activa', () => Serenity.IntegerEditor],
        ['FechaCreacion', () => Serenity.DateEditor],
        ['UserId', () => Serenity.IntegerEditor],
        ['FechaModificacion', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(AlarmasForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}