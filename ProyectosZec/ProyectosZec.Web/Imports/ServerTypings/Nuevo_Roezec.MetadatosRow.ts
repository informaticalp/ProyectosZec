﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface MetadatosRow {
        MetadatoId?: number;
        FicheroId?: number;
        Nombre?: string;
        Valor?: string;
        FechaModificacion?: string;
        FicheroHistorialId?: number;
        Fichero?: string;
        FicheroNombreNatural?: string;
        FicheroOrgano?: string;
        FicheroFechaCaptura?: string;
        FicheroTipoDocumentoId?: number;
        FicheroCsv?: string;
        FicheroRegulacionCsv?: string;
        FicheroObservaciones?: string;
        FicheroFechaModificacion?: string;
        FicheroUserId?: number;
    }

    export namespace MetadatosRow {
        export const idProperty = 'MetadatoId';
        export const nameProperty = 'Nombre';
        export const localTextPrefix = 'Nuevo_Roezec.Metadatos';
        export const deletePermission = 'Roezec:General';
        export const insertPermission = 'Roezec:General';
        export const readPermission = 'Roezec:General';
        export const updatePermission = 'Roezec:General';

        export namespace Fields {
            export declare const MetadatoId;
            export declare const FicheroId;
            export declare const Nombre;
            export declare const Valor;
            export declare const FechaModificacion;
            export declare const FicheroHistorialId;
            export declare const Fichero;
            export declare const FicheroNombreNatural;
            export declare const FicheroOrgano;
            export declare const FicheroFechaCaptura;
            export declare const FicheroTipoDocumentoId;
            export declare const FicheroCsv;
            export declare const FicheroRegulacionCsv;
            export declare const FicheroObservaciones;
            export declare const FicheroFechaModificacion;
            export declare const FicheroUserId;
        }

        [
            'MetadatoId',
            'FicheroId',
            'Nombre',
            'Valor',
            'FechaModificacion',
            'FicheroHistorialId',
            'Fichero',
            'FicheroNombreNatural',
            'FicheroOrgano',
            'FicheroFechaCaptura',
            'FicheroTipoDocumentoId',
            'FicheroCsv',
            'FicheroRegulacionCsv',
            'FicheroObservaciones',
            'FicheroFechaModificacion',
            'FicheroUserId'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}