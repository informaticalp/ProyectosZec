﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EnviosProcedimientoRow {
        EnvioId?: number;
        HistorialId?: number;
        TipoEnvioId?: number;
        FechaEnvio?: string;
        ContactoEnvioId?: number;
        FechaRecepcion?: string;
        ContactoAcuseId?: number;
        HistorialEmpresaId?: number;
        HistorialProcedimientoId?: number;
        HistorialFechaInicio?: string;
        HistorialFechaResolucion?: string;
        HistorialSentidoResolucion?: number;
        HistorialFechaEfecto?: string;
        HistorialAcuseInicio?: string;
        HistorialPersonaAcuseIncioId?: number;
        HistorialAcuseResolucion?: string;
        HistorialPersonaAcuseResolucionId?: number;
        HistorialObservaciones?: string;
        HistorialFicheros?: string;
        TipoEnvioTipo?: string;
        ContactoEnvioNombre?: string;
        ContactoEnvioApellidos?: string;
        ContactoEnvioNif?: string;
        ContactoEnvioTelefonoFijo?: string;
        ContactoEnvioMovil?: string;
        ContactoEnvioIdiomaId?: number;
        ContactoEnvioEmail?: string;
    }

    export namespace EnviosProcedimientoRow {
        export const idProperty = 'EnvioId';
        export const localTextPrefix = 'Nuevo_Roezec.EnviosProcedimiento';
        export const deletePermission = 'Roezec:Modify';
        export const insertPermission = 'Roezec:Modify';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export declare const enum Fields {
            EnvioId = "EnvioId",
            HistorialId = "HistorialId",
            TipoEnvioId = "TipoEnvioId",
            FechaEnvio = "FechaEnvio",
            ContactoEnvioId = "ContactoEnvioId",
            FechaRecepcion = "FechaRecepcion",
            ContactoAcuseId = "ContactoAcuseId",
            HistorialEmpresaId = "HistorialEmpresaId",
            HistorialProcedimientoId = "HistorialProcedimientoId",
            HistorialFechaInicio = "HistorialFechaInicio",
            HistorialFechaResolucion = "HistorialFechaResolucion",
            HistorialSentidoResolucion = "HistorialSentidoResolucion",
            HistorialFechaEfecto = "HistorialFechaEfecto",
            HistorialAcuseInicio = "HistorialAcuseInicio",
            HistorialPersonaAcuseIncioId = "HistorialPersonaAcuseIncioId",
            HistorialAcuseResolucion = "HistorialAcuseResolucion",
            HistorialPersonaAcuseResolucionId = "HistorialPersonaAcuseResolucionId",
            HistorialObservaciones = "HistorialObservaciones",
            HistorialFicheros = "HistorialFicheros",
            TipoEnvioTipo = "TipoEnvioTipo",
            ContactoEnvioNombre = "ContactoEnvioNombre",
            ContactoEnvioApellidos = "ContactoEnvioApellidos",
            ContactoEnvioNif = "ContactoEnvioNif",
            ContactoEnvioTelefonoFijo = "ContactoEnvioTelefonoFijo",
            ContactoEnvioMovil = "ContactoEnvioMovil",
            ContactoEnvioIdiomaId = "ContactoEnvioIdiomaId",
            ContactoEnvioEmail = "ContactoEnvioEmail"
        }
    }
}

