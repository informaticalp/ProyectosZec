﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface FormasEnvioRow {
        FormaEnvioId?: number;
        Forma?: string;
    }

    export namespace FormasEnvioRow {
        export const idProperty = 'FormaEnvioId';
        export const nameProperty = 'Forma';
        export const localTextPrefix = 'Nuevo_Roezec.FormasEnvio';
        export const lookupKey = 'Nuevo_Roezec.FormasEnvio';

        export function getLookup(): Q.Lookup<FormasEnvioRow> {
            return Q.getLookup<FormasEnvioRow>('Nuevo_Roezec.FormasEnvio');
        }
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export declare const enum Fields {
            FormaEnvioId = "FormaEnvioId",
            Forma = "Forma"
        }
    }
}

