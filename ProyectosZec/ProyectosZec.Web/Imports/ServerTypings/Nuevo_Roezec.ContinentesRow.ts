﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface ContinentesRow {
        ContinenteId?: number;
        Continente?: string;
    }

    export namespace ContinentesRow {
        export const idProperty = 'ContinenteId';
        export const nameProperty = 'Continente';
        export const localTextPrefix = 'Nuevo_Roezec.Continentes';
        export const lookupKey = 'Nuevo_Roezec.Continentes';

        export function getLookup(): Q.Lookup<ContinentesRow> {
            return Q.getLookup<ContinentesRow>('Nuevo_Roezec.Continentes');
        }
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export declare const enum Fields {
            ContinenteId = "ContinenteId",
            Continente = "Continente"
        }
    }
}

