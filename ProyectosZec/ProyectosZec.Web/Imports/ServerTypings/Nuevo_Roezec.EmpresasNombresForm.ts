﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class EmpresasNombresForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EmpresasNombres';
    }

    export interface EmpresasNombresForm {
        EmpresaId: Serenity.IntegerEditor;
        Nombre: Serenity.StringEditor;
        UserId: Serenity.IntegerEditor;
        FechaModificacion: Serenity.DateEditor;
    }

    [,
        ['EmpresaId', () => Serenity.IntegerEditor],
        ['Nombre', () => Serenity.StringEditor],
        ['UserId', () => Serenity.IntegerEditor],
        ['FechaModificacion', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(EmpresasNombresForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}