﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface AlarmasRow {
        AlarmaId?: number;
        TipoAlarmaId?: number;
        EmpresaId?: number;
        Activa?: number;
        FechaCreacion?: string;
		FechaCaducidad?: string;
		FechaAviso?: string;
        UserId?: number;
        FechaModificacion?: string;
        TipoAlarmaTexto?: string;
        TipoAlarmaDias?: number;
        TipoAlarmaDiasAviso?: number;
        TipoAlarmaEmail?: string;
        TipoAlarmaUserId?: number;
        TipoAlarmaFechaModificacion?: string;
        EmpresaRazon?: string;
        EmpresaFormaJuridicaId?: number;
        EmpresaTecnicoId?: number;
        EmpresaCif?: string;
        EmpresaDireccion?: string;
        EmpresaPoblacion?: string;
        EmpresaCp?: number;
        EmpresaIslaId?: number;
        EmpresaTelefonoFijo?: string;
        EmpresaMovil?: string;
        EmpresaCapitalSocial?: number;
        EmpresaEmail?: string;
        EmpresaProyectoId?: number;
        EmpresaNumeroRoezec?: number;
        EmpresaExpediente?: string;
        EmpresaMotivoExencion?: string;
        EmpresaTipologiaCapitalId?: number;
        EmpresaTipoGarantiaTasaId?: number;
        EmpresaEmpleoTraspasado?: number;
        EmpresaEmpleo6Meses?: number;
        EmpresaEmpleoPromedio?: number;
        EmpresaEmpleoPromedio2Anos?: number;
        EmpresaInversionTraspasada?: number;
        EmpresaInversion2Anos?: number;
        EmpresaEstadoEmpresaId?: number;
        EmpresaFechaCambioEstado?: string;
        EmpresaNumTasaLiquidacion?: string;
        EmpresaUserId?: number;
        EmpresaFechaModificacion?: string;
    }

    export namespace AlarmasRow {
        export const idProperty = 'AlarmaId';
        export const localTextPrefix = 'Nuevo_Roezec.Alarmas';
        export const deletePermission = 'Roezec:Read';
        export const insertPermission = 'Roezec:Read';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Read';

        export namespace Fields {
            export declare const AlarmaId;
            export declare const TipoAlarmaId;
            export declare const EmpresaId;
            export declare const Activa;
            export declare const FechaCreacion;
			export declare const FechaCducidad;
			export declare const FechaAviso;
            export declare const UserId;
            export declare const FechaModificacion;
            export declare const TipoAlarmaTexto;
            export declare const TipoAlarmaDias;
            export declare const TipoAlarmaDiasAviso;
            export declare const TipoAlarmaEmail;
            export declare const TipoAlarmaUserId;
            export declare const TipoAlarmaFechaModificacion;
            export declare const EmpresaRazon;
            export declare const EmpresaFormaJuridicaId;
            export declare const EmpresaTecnicoId;
            export declare const EmpresaCif;
            export declare const EmpresaDireccion;
            export declare const EmpresaPoblacion;
            export declare const EmpresaCp;
            export declare const EmpresaIslaId;
            export declare const EmpresaTelefonoFijo;
            export declare const EmpresaMovil;
            export declare const EmpresaCapitalSocial;
            export declare const EmpresaEmail;
            export declare const EmpresaProyectoId;
            export declare const EmpresaNumeroRoezec;
            export declare const EmpresaExpediente;
            export declare const EmpresaMotivoExencion;
            export declare const EmpresaTipologiaCapitalId;
            export declare const EmpresaTipoGarantiaTasaId;
            export declare const EmpresaEmpleoTraspasado;
            export declare const EmpresaEmpleo6Meses;
            export declare const EmpresaEmpleoPromedio;
            export declare const EmpresaEmpleoPromedio2Anos;
            export declare const EmpresaInversionTraspasada;
            export declare const EmpresaInversion2Anos;
            export declare const EmpresaEstadoEmpresaId;
            export declare const EmpresaFechaCambioEstado;
            export declare const EmpresaNumTasaLiquidacion;
            export declare const EmpresaUserId;
            export declare const EmpresaFechaModificacion;
        }

        [
            'AlarmaId',
            'TipoAlarmaId',
            'EmpresaId',
            'Activa',
            'FechaCreacion',
			'FechaAviso',
			'FechaCaducidad',
            'UserId',
            'FechaModificacion',
            'TipoAlarmaTexto',
            'TipoAlarmaDias',
            'TipoAlarmaDiasAviso',
            'TipoAlarmaEmail',
            'TipoAlarmaUserId',
            'TipoAlarmaFechaModificacion',
            'EmpresaRazon',
            'EmpresaFormaJuridicaId',
            'EmpresaTecnicoId',
            'EmpresaCif',
            'EmpresaDireccion',
            'EmpresaPoblacion',
            'EmpresaCp',
            'EmpresaIslaId',
            'EmpresaTelefonoFijo',
            'EmpresaMovil',
            'EmpresaCapitalSocial',
            'EmpresaEmail',
            'EmpresaProyectoId',
            'EmpresaNumeroRoezec',
            'EmpresaExpediente',
            'EmpresaMotivoExencion',
            'EmpresaTipologiaCapitalId',
            'EmpresaTipoGarantiaTasaId',
            'EmpresaEmpleoTraspasado',
            'EmpresaEmpleo6Meses',
            'EmpresaEmpleoPromedio',
            'EmpresaEmpleoPromedio2Anos',
            'EmpresaInversionTraspasada',
            'EmpresaInversion2Anos',
            'EmpresaEstadoEmpresaId',
            'EmpresaFechaCambioEstado',
            'EmpresaNumTasaLiquidacion',
            'EmpresaUserId',
            'EmpresaFechaModificacion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}