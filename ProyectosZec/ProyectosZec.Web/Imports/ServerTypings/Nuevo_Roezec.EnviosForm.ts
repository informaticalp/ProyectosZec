﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EnviosForm {
        TipoEnvioId: Serenity.LookupEditor;
        FormaEnvioId: Serenity.LookupEditor;
        EstadoEnvioId: Serenity.LookupEditor;
        FechaEnvio: Serenity.DateEditor;
        ContactoEnvioId: Serenity.LookupEditor;
        FechaRecepcion: Serenity.DateEditor;
        ContactoAcuseId: Serenity.LookupEditor;
    }

    export class EnviosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Envios';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EnviosForm.init)  {
                EnviosForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.DateEditor;

                Q.initFormType(EnviosForm, [
                    'TipoEnvioId', w0,
                    'FormaEnvioId', w0,
                    'EstadoEnvioId', w0,
                    'FechaEnvio', w1,
                    'ContactoEnvioId', w0,
                    'FechaRecepcion', w1,
                    'ContactoAcuseId', w0
                ]);
            }
        }
    }
}

