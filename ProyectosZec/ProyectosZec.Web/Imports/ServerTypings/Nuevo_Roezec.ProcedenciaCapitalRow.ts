﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface ProcedenciaCapitalRow {
        ProcedenciaId?: number;
        EmpresaId?: number;
        PaisId?: number;
        Porcentaje?: number;
        EmpresaRazon?: string;
        EmpresaFormaJuridicaId?: number;
        EmpresaTecnicoId?: number;
        EmpresaCif?: string;
        EmpresaDireccion?: string;
        EmpresaPoblacion?: string;
        EmpresaCp?: number;
        EmpresaIslaId?: number;
        EmpresaTelefonoFijo?: string;
        EmpresaMovil?: string;
        EmpresaEmail?: string;
        EmpresaProyectoId?: number;
        EmpresaExpediente?: string;
        EmpresaMotivoExencion?: string;
        EmpresaTipologiaCapitalId?: number;
        EmpresaTipoGarantiaTasaId?: number;
        EmpresaEmpleoTraspasado?: number;
        EmpresaEmpleo6Meses?: number;
        EmpresaEmpleoPromedio?: number;
        EmpresaEmpleoPromedio2Anos?: number;
        EmpresaInversionTraspasada?: number;
        EmpresaInversion2Anos?: number;
        EmpresaEstadoEmpresaId?: number;
        EmpresaFechaCambioEstado?: string;
        EmpresaNumTasaLiquidacion?: string;
        Pais?: string;
        PaisCapital?: string;
        PaisContinenteId?: number;
    }

    export namespace ProcedenciaCapitalRow {
        export const idProperty = 'ProcedenciaId';
        export const nameProperty = 'EmpresaRazon';
        export const localTextPrefix = 'Nuevo_Roezec.ProcedenciaCapital';
        export const deletePermission = 'Roezec:Delete';
        export const insertPermission = 'Roezec:Insert';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export declare const enum Fields {
            ProcedenciaId = "ProcedenciaId",
            EmpresaId = "EmpresaId",
            PaisId = "PaisId",
            Porcentaje = "Porcentaje",
            EmpresaRazon = "EmpresaRazon",
            EmpresaFormaJuridicaId = "EmpresaFormaJuridicaId",
            EmpresaTecnicoId = "EmpresaTecnicoId",
            EmpresaCif = "EmpresaCif",
            EmpresaDireccion = "EmpresaDireccion",
            EmpresaPoblacion = "EmpresaPoblacion",
            EmpresaCp = "EmpresaCp",
            EmpresaIslaId = "EmpresaIslaId",
            EmpresaTelefonoFijo = "EmpresaTelefonoFijo",
            EmpresaMovil = "EmpresaMovil",
            EmpresaEmail = "EmpresaEmail",
            EmpresaProyectoId = "EmpresaProyectoId",
            EmpresaExpediente = "EmpresaExpediente",
            EmpresaMotivoExencion = "EmpresaMotivoExencion",
            EmpresaTipologiaCapitalId = "EmpresaTipologiaCapitalId",
            EmpresaTipoGarantiaTasaId = "EmpresaTipoGarantiaTasaId",
            EmpresaEmpleoTraspasado = "EmpresaEmpleoTraspasado",
            EmpresaEmpleo6Meses = "EmpresaEmpleo6Meses",
            EmpresaEmpleoPromedio = "EmpresaEmpleoPromedio",
            EmpresaEmpleoPromedio2Anos = "EmpresaEmpleoPromedio2Anos",
            EmpresaInversionTraspasada = "EmpresaInversionTraspasada",
            EmpresaInversion2Anos = "EmpresaInversion2Anos",
            EmpresaEstadoEmpresaId = "EmpresaEstadoEmpresaId",
            EmpresaFechaCambioEstado = "EmpresaFechaCambioEstado",
            EmpresaNumTasaLiquidacion = "EmpresaNumTasaLiquidacion",
            Pais = "Pais",
            PaisCapital = "PaisCapital",
            PaisContinenteId = "PaisContinenteId"
        }
    }
}

