﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class TiposAlarmaForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.TiposAlarma';
    }

    export interface TiposAlarmaForm {
        Texto: Serenity.StringEditor;
        DiasAviso: Serenity.IntegerEditor;
        Email: Serenity.StringEditor;
    }

    [,
        ['Texto', () => Serenity.StringEditor],
        ['DiasAviso', () => Serenity.IntegerEditor],
        ['Email', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(TiposAlarmaForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}