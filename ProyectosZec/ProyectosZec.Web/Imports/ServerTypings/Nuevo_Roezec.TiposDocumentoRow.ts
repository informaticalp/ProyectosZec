﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface TiposDocumentoRow {
        TipoDocumentoId?: number;
        Tipo?: string;
    }

    export namespace TiposDocumentoRow {
        export const idProperty = 'TipoDocumentoId';
        export const nameProperty = 'Tipo';
        export const localTextPrefix = 'Nuevo_Roezec.TiposDocumento';
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export namespace Fields {
            export declare const TipoDocumentoId;
            export declare const Tipo;
        }

        [
            'TipoDocumentoId',
            'Tipo'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}