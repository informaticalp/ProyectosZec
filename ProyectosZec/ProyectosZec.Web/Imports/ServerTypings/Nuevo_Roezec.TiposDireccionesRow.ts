﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface TiposDireccionesRow {
        TipoDireccionId?: number;
        TipoDireccion?: string;
    }

    export namespace TiposDireccionesRow {
        export const idProperty = 'TipoDireccionId';
        export const nameProperty = 'TipoDireccion';
        export const localTextPrefix = 'Nuevo_Roezec.TiposDirecciones';
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export namespace Fields {
            export declare const TipoDireccionId;
            export declare const TipoDireccion;
        }

        [
            'TipoDireccionId',
            'TipoDireccion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}