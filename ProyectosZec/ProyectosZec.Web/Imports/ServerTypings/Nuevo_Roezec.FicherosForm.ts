﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class FicherosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Ficheros';
    }

    export interface FicherosForm {
        HistorialId: Serenity.IntegerEditor;
        Fichero: Serenity.StringEditor;
        FechaModificacion: Serenity.DateEditor;
    }

    [,
        ['HistorialId', () => Serenity.IntegerEditor],
        ['Fichero', () => Serenity.StringEditor],
        ['FechaModificacion', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(FicherosForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}