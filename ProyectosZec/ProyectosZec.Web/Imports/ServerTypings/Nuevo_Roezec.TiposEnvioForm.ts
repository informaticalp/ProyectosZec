﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface TiposEnvioForm {
        Tipo: Serenity.StringEditor;
    }

    export class TiposEnvioForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.TiposEnvio';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!TiposEnvioForm.init)  {
                TiposEnvioForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;

                Q.initFormType(TiposEnvioForm, [
                    'Tipo', w0
                ]);
            }
        }
    }
}

