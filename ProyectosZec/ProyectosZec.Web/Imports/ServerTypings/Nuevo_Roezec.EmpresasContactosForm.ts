﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EmpresasContactosForm {
        ContactoId: Serenity.LookupEditor;
        TipoContactoId: Serenity.LookupEditor;
        FechaAlta: Serenity.DateEditor;
        FechaBaja: Serenity.DateEditor;
    }

    export class EmpresasContactosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EmpresasContactos';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EmpresasContactosForm.init)  {
                EmpresasContactosForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.DateEditor;

                Q.initFormType(EmpresasContactosForm, [
                    'ContactoId', w0,
                    'TipoContactoId', w0,
                    'FechaAlta', w1,
                    'FechaBaja', w1
                ]);
            }
        }
    }
}

