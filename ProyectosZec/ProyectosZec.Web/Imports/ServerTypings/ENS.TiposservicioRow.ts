﻿
namespace ProyectosZec.ENS {
    export interface TiposservicioRow {
        TipoId?: number;
        TipoServicio?: string;
    }

    export namespace TiposservicioRow {
        export const idProperty = 'TipoId';
        export const nameProperty = 'TipoServicio';
        export const localTextPrefix = 'ENS.Tiposservicio';
        export const deletePermission = 'Ens:General';
        export const insertPermission = 'Ens:General';
        export const readPermission = 'Ens:General';
        export const updatePermission = 'Ens:General';

        export namespace Fields {
            export declare const TipoId;
            export declare const TipoServicio;
        }

        [
            'TipoId',
            'TipoServicio'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}