﻿namespace ProyectosZec.Kairos {
    export interface HorasExtraConsumidasForm {
        Tiempo: Serenity.DecimalEditor;
        Dia: Serenity.DateEditor;
    }

    export class HorasExtraConsumidasForm extends Serenity.PrefixedContext {
        static formKey = 'Kairos.HorasExtraConsumidas';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!HorasExtraConsumidasForm.init)  {
                HorasExtraConsumidasForm.init = true;

                var s = Serenity;
                var w0 = s.DecimalEditor;
                var w1 = s.DateEditor;

                Q.initFormType(HorasExtraConsumidasForm, [
                    'Tiempo', w0,
                    'Dia', w1
                ]);
            }
        }
    }
}

