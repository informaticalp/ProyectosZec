﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface NacesForm {
        VersionId: Serenity.LookupEditor;
        Codigo: Serenity.StringEditor;
        Descripcion: Serenity.StringEditor;
        SectorId: Serenity.LookupEditor;
        SubsectorId: Serenity.LookupEditor;
    }

    export class NacesForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.Naces';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!NacesForm.init)  {
                NacesForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.StringEditor;

                Q.initFormType(NacesForm, [
                    'VersionId', w0,
                    'Codigo', w1,
                    'Descripcion', w1,
                    'SectorId', w0,
                    'SubsectorId', w0
                ]);
            }
        }
    }
}

