﻿namespace ProyectosZec.Nuevo_Roezec {
    export namespace FormasEnvioService {
        export const baseUrl = 'Nuevo_Roezec/FormasEnvio';

        export declare function Create(request: Serenity.SaveRequest<FormasEnvioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<FormasEnvioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<FormasEnvioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<FormasEnvioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Nuevo_Roezec/FormasEnvio/Create",
            Update = "Nuevo_Roezec/FormasEnvio/Update",
            Delete = "Nuevo_Roezec/FormasEnvio/Delete",
            Retrieve = "Nuevo_Roezec/FormasEnvio/Retrieve",
            List = "Nuevo_Roezec/FormasEnvio/List"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List'
        ].forEach(x => {
            (<any>FormasEnvioService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

