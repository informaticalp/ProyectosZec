﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface HistorialEmpresasForm {
        ProcedimientoId: Serenity.LookupEditor;
        FechaInicio: Serenity.DateEditor;
        FechaResolucion: Serenity.DateEditor;
        SentidoResolucion: Serenity.BooleanEditor;
        FechaEfecto: Serenity.DateEditor;
        EnviosList: EnviosEditor;
        Observaciones: Serenity.TextAreaEditor;
        Ficheros: Serenity.MultipleImageUploadEditor;
    }

    export class HistorialEmpresasForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.HistorialEmpresas';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!HistorialEmpresasForm.init)  {
                HistorialEmpresasForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;
                var w1 = s.DateEditor;
                var w2 = s.BooleanEditor;
                var w3 = EnviosEditor;
                var w4 = s.TextAreaEditor;
                var w5 = s.MultipleImageUploadEditor;

                Q.initFormType(HistorialEmpresasForm, [
                    'ProcedimientoId', w0,
                    'FechaInicio', w1,
                    'FechaResolucion', w1,
                    'SentidoResolucion', w2,
                    'FechaEfecto', w1,
                    'EnviosList', w3,
                    'Observaciones', w4,
                    'Ficheros', w5
                ]);
            }
        }
    }
}

