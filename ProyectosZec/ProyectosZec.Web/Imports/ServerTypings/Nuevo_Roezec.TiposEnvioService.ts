﻿namespace ProyectosZec.Nuevo_Roezec {
    export namespace TiposEnvioService {
        export const baseUrl = 'Nuevo_Roezec/TiposEnvio';

        export declare function Create(request: Serenity.SaveRequest<TiposEnvioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Update(request: Serenity.SaveRequest<TiposEnvioRow>, onSuccess?: (response: Serenity.SaveResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Delete(request: Serenity.DeleteRequest, onSuccess?: (response: Serenity.DeleteResponse) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function Retrieve(request: Serenity.RetrieveRequest, onSuccess?: (response: Serenity.RetrieveResponse<TiposEnvioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;
        export declare function List(request: Serenity.ListRequest, onSuccess?: (response: Serenity.ListResponse<TiposEnvioRow>) => void, opt?: Q.ServiceOptions<any>): JQueryXHR;

        export declare const enum Methods {
            Create = "Nuevo_Roezec/TiposEnvio/Create",
            Update = "Nuevo_Roezec/TiposEnvio/Update",
            Delete = "Nuevo_Roezec/TiposEnvio/Delete",
            Retrieve = "Nuevo_Roezec/TiposEnvio/Retrieve",
            List = "Nuevo_Roezec/TiposEnvio/List"
        }

        [
            'Create', 
            'Update', 
            'Delete', 
            'Retrieve', 
            'List'
        ].forEach(x => {
            (<any>TiposEnvioService)[x] = function (r, s, o) {
                return Q.serviceRequest(baseUrl + '/' + x, r, s, o);
            };
        });
    }
}

