﻿
namespace ProyectosZec.Nuevo_Roezec {
    export interface TiposPersonaRow {
        TipoPersonaId?: number;
        TipoPersona?: string;
    }

    export namespace TiposPersonaRow {
        export const idProperty = 'TipoPersonaId';
        export const nameProperty = 'TipoPersona';
        export const localTextPrefix = 'Nuevo_Roezec.TiposPersona';
        export const deletePermission = 'Roezec:Admin';
        export const insertPermission = 'Roezec:Admin';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Admin';

        export namespace Fields {
            export declare const TipoPersonaId;
            export declare const TipoPersona;
        }

        [
            'TipoPersonaId',
            'TipoPersona'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}