﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class AlarmasProcedimientosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.AlarmasProcedimientos';
    }

    export interface AlarmasProcedimientosForm {
        ProcedimientoId: Serenity.IntegerEditor;
        SentidoResolucionId: Serenity.IntegerEditor;
        UserId: Serenity.IntegerEditor;
        FechaModificacion: Serenity.DateEditor;
    }

    [,
        ['ProcedimientoId', () => Serenity.IntegerEditor],
        ['SentidoResolucionId', () => Serenity.IntegerEditor],
        ['UserId', () => Serenity.IntegerEditor],
        ['FechaModificacion', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(AlarmasProcedimientosForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}