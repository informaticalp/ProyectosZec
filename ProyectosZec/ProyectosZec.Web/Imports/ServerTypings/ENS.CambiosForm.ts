﻿
namespace ProyectosZec.ENS {
    export class CambiosForm extends Serenity.PrefixedContext {
        static formKey = 'ENS.Cambios';
    }

    export interface CambiosForm {
        ServicioId: Serenity.IntegerEditor;
        Observaciones: Serenity.StringEditor;
        Fecha: Serenity.DateEditor;
    }

    [,
        ['ServicioId', () => Serenity.IntegerEditor],
        ['Observaciones', () => Serenity.StringEditor],
        ['Fecha', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(CambiosForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}