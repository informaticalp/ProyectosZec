﻿
namespace ProyectosZec.Nuevo_Roezec {
    export class TiposDocumentoForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.TiposDocumento';
    }

    export interface TiposDocumentoForm {
        Tipo: Serenity.StringEditor;
    }

    [,
        ['Tipo', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(TiposDocumentoForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}