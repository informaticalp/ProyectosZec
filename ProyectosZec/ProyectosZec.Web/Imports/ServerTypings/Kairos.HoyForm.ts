﻿
namespace ProyectosZec.Kairos {
    export class HoyForm extends Serenity.PrefixedContext {
        static formKey = 'Kairos.Hoy';
    }

    export interface HoyForm {
        CodigoCliente: Serenity.IntegerEditor;
        Nombre: Serenity.StringEditor;
        CodigoValidacion: Serenity.StringEditor;
        FechaBaja: Serenity.DateEditor;
        Pin: Serenity.IntegerEditor;
        Tecnico: Serenity.BooleanEditor;
        UsoHorario: Serenity.StringEditor;
        SacarFotoFichaje: Serenity.BooleanEditor;
        FechaActualizacion: Serenity.DateEditor;
        FechaBorrado: Serenity.DateEditor;
        NumeroTarjetaFichaje: Serenity.StringEditor;
        IdDepartamento: Serenity.IntegerEditor;
        IdEmpresa: Serenity.IntegerEditor;
        Email: Serenity.StringEditor;
        PermiteRecordatorio: Serenity.BooleanEditor;
        PermiteFichajeAutomatico: Serenity.BooleanEditor;
        IdEmpresaFichajeAutomatico: Serenity.IntegerEditor;
        ProgramaExternoIdEmpleado: Serenity.StringEditor;
        ProgramaExternoDescripcion: Serenity.StringEditor;
        IdHoraExtraCabecera: Serenity.IntegerEditor;
        ClaveAccesoWeb: Serenity.StringEditor;
        PermiteFichajeWeb: Serenity.BooleanEditor;
    }

    [,
        ['CodigoCliente', () => Serenity.IntegerEditor],
        ['Nombre', () => Serenity.StringEditor],
        ['CodigoValidacion', () => Serenity.StringEditor],
        ['FechaBaja', () => Serenity.DateEditor],
        ['Pin', () => Serenity.IntegerEditor],
        ['Tecnico', () => Serenity.BooleanEditor],
        ['UsoHorario', () => Serenity.StringEditor],
        ['SacarFotoFichaje', () => Serenity.BooleanEditor],
        ['FechaActualizacion', () => Serenity.DateEditor],
        ['FechaBorrado', () => Serenity.DateEditor],
        ['NumeroTarjetaFichaje', () => Serenity.StringEditor],
        ['IdDepartamento', () => Serenity.IntegerEditor],
        ['IdEmpresa', () => Serenity.IntegerEditor],
        ['Email', () => Serenity.StringEditor],
        ['PermiteRecordatorio', () => Serenity.BooleanEditor],
        ['PermiteFichajeAutomatico', () => Serenity.BooleanEditor],
        ['IdEmpresaFichajeAutomatico', () => Serenity.IntegerEditor],
        ['ProgramaExternoIdEmpleado', () => Serenity.StringEditor],
        ['ProgramaExternoDescripcion', () => Serenity.StringEditor],
        ['IdHoraExtraCabecera', () => Serenity.IntegerEditor],
        ['ClaveAccesoWeb', () => Serenity.StringEditor],
        ['PermiteFichajeWeb', () => Serenity.BooleanEditor]
    ].forEach(x => Object.defineProperty(HoyForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}