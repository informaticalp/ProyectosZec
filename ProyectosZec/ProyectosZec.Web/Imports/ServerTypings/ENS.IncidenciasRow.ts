﻿
namespace ProyectosZec.ENS {
    export interface IncidenciasRow {
        IncidenciaId?: number;
        ServicioId?: number;
        SeveridadId?: number;
        Apertura?: string;
        Cierre?: string;
        Observaciones?: string;
        Servicio?: string;
        SeveridadServicio?: string;
    }

    export namespace IncidenciasRow {
        export const idProperty = 'IncidenciaId';
        export const nameProperty = 'Observaciones';
        export const localTextPrefix = 'ENS.Incidencias';
        export const deletePermission = 'Ens:General';
        export const insertPermission = 'Ens:General';
        export const readPermission = 'Ens:General';
        export const updatePermission = 'Ens:General';

        export namespace Fields {
            export declare const IncidenciaId;
            export declare const ServicioId;
            export declare const SeveridadId;
            export declare const Apertura;
            export declare const Cierre;
            export declare const Observaciones;
            export declare const Servicio;
            export declare const SeveridadServicio;
        }

        [
            'IncidenciaId',
            'ServicioId',
            'SeveridadId',
            'Apertura',
            'Cierre',
            'Observaciones',
            'Servicio',
            'SeveridadServicio'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}