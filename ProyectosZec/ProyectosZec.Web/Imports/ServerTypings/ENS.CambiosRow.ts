﻿
namespace ProyectosZec.ENS {
    export interface CambiosRow {
        CombioId?: number;
        ServicioId?: number;
        Observaciones?: string;
        Fecha?: string;
        Servicio?: string;
    }

    export namespace CambiosRow {
        export const idProperty = 'CombioId';
        export const nameProperty = 'Observaciones';
        export const localTextPrefix = 'ENS.Cambios';
        export const deletePermission = 'Ens:General';
        export const insertPermission = 'Ens:General';
        export const readPermission = 'Ens:General';
        export const updatePermission = 'Ens:General';

        export namespace Fields {
            export declare const CombioId;
            export declare const ServicioId;
            export declare const Observaciones;
            export declare const Fecha;
            export declare const Servicio;
        }

        [
            'CombioId',
            'ServicioId',
            'Observaciones',
            'Fecha',
            'Servicio'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}