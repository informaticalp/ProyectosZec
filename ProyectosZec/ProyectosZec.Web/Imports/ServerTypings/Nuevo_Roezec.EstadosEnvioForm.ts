﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EstadosEnvioForm {
        Estado: Serenity.StringEditor;
    }

    export class EstadosEnvioForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EstadosEnvio';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EstadosEnvioForm.init)  {
                EstadosEnvioForm.init = true;

                var s = Serenity;
                var w0 = s.StringEditor;

                Q.initFormType(EstadosEnvioForm, [
                    'Estado', w0
                ]);
            }
        }
    }
}

