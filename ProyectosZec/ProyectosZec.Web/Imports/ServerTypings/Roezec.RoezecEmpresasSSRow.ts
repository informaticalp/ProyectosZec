﻿
namespace ProyectosZec.Roezec {
    export interface RoezecEmpresasSSRow {
        Id?: number;
        DenominacionSocial?: string;
        Cif?: string;
        Direccion?: string;
        Cp?: string;
        Poblacion?: string;
        Provincia?: string;
        Isla?: string;
        NotasMarginales?: string;
        AnyoExpediente?: number;
        NumExpediente?: number;
        Agencia?: number;
        Tecnico?: string;
        FormaJuridica?: string;
        Superficie?: number;
        ExentaAreaAcotada?: string;
        MotivosExencion?: string;
        ObjetivoEmpleo?: number;
        ObjetivoInversion?: number;
        ObservacionesEmpleo?: string;
        ObservacionesInversion?: string;
        PreEmpleo?: number;
        PreInversion?: number;
        TrasEmpleo?: number;
        TrasInversion?: number;
        FechaAlta?: string;
        FechaModificacion?: string;
        FechaBaja?: string;
        Situacion?: string;
        UsrAlta?: string;
        UsrModificacion?: string;
        UsrBaja?: string;
        Trading?: number;
        Empleos2021?: number;
        Empleos2020?: number;
        Empleos2019?: number;
        Empleos2018?: number;
        Empleos2017?: number;
		NaceId: number;
		NacePrincipal: string;
    }

    export namespace RoezecEmpresasSSRow {
        export const idProperty = 'Id';
        export const nameProperty = 'DenominacionSocial';
        export const localTextPrefix = 'Roezec.RoezecEmpresasSS';
        export const deletePermission = 'RoezecSS:General';
        export const insertPermission = 'RoezecSS:General';
        export const readPermission = 'RoezecSS:General';
        export const updatePermission = 'RoezecSS:General';

        export namespace Fields {
            export declare const Id;
            export declare const DenominacionSocial;
            export declare const Cif;
            export declare const Direccion;
            export declare const Cp;
            export declare const Poblacion;
            export declare const Provincia;
            export declare const Isla;
            export declare const NotasMarginales;
            export declare const AnyoExpediente;
            export declare const NumExpediente;
            export declare const Agencia;
            export declare const Tecnico;
            export declare const FormaJuridica;
            export declare const Superficie;
            export declare const ExentaAreaAcotada;
            export declare const MotivosExencion;
            export declare const ObjetivoEmpleo;
            export declare const ObjetivoInversion;
            export declare const ObservacionesEmpleo;
            export declare const ObservacionesInversion;
            export declare const PreEmpleo;
            export declare const PreInversion;
            export declare const TrasEmpleo;
            export declare const TrasInversion;
            export declare const FechaAlta;
            export declare const FechaModificacion;
            export declare const FechaBaja;
            export declare const Situacion;
            export declare const UsrAlta;
            export declare const UsrModificacion;
            export declare const UsrBaja;
            export declare const Trading;
            export declare const Empleos2021;
            export declare const Empleos2020;
            export declare const Empleos2019;
            export declare const Empleos2018;
            export declare const Empleos2017;
			export declare const NaceId;
            export declare const NacePrincipal;
        }

        [
            'Id',
            'DenominacionSocial',
            'Cif',
            'Direccion',
            'Cp',
            'Poblacion',
            'Provincia',
            'Isla',
            'NotasMarginales',
            'AnyoExpediente',
            'NumExpediente',
            'Agencia',
            'Tecnico',
            'FormaJuridica',
            'Superficie',
            'ExentaAreaAcotada',
            'MotivosExencion',
            'ObjetivoEmpleo',
            'ObjetivoInversion',
            'ObservacionesEmpleo',
            'ObservacionesInversion',
            'PreEmpleo',
            'PreInversion',
            'TrasEmpleo',
            'TrasInversion',
            'FechaAlta',
            'FechaModificacion',
            'FechaBaja',
            'Situacion',
            'UsrAlta',
            'UsrModificacion',
            'UsrBaja',
            'Trading',
            'Empleos2021',
            'Empleos2020',
            'Empleos2019',
            'Empleos2018',
            'Empleos2017',
			'NaceId',
			'NacePrincipal'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}