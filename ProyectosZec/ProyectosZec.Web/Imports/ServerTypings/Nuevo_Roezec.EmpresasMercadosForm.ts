﻿namespace ProyectosZec.Nuevo_Roezec {
    export interface EmpresasMercadosForm {
        MercadoId: Serenity.LookupEditor;
    }

    export class EmpresasMercadosForm extends Serenity.PrefixedContext {
        static formKey = 'Nuevo_Roezec.EmpresasMercados';
        private static init: boolean;

        constructor(prefix: string) {
            super(prefix);

            if (!EmpresasMercadosForm.init)  {
                EmpresasMercadosForm.init = true;

                var s = Serenity;
                var w0 = s.LookupEditor;

                Q.initFormType(EmpresasMercadosForm, [
                    'MercadoId', w0
                ]);
            }
        }
    }
}

