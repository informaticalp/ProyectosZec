﻿
namespace ProyectosZec.Roezec {
    export interface EmpleosSSRow {
        Id?: number;
        IdEmpresa?: number;
        Fecha?: string;
        Empleos?: number;
        Anyo?: number;
        Isla?: string;
        AnyoExpediente?: number;
    }

    export namespace EmpleosSSRow {
        export const idProperty = 'Id';
        export const localTextPrefix = 'Roezec.EmpleosSS';
        export const deletePermission = 'Roezec_Old_SS:Read';
        export const insertPermission = 'Roezec_Old_SS:Read';
        export const readPermission = 'Roezec_Old_SS:Read';
        export const updatePermission = 'Roezec_Old_SS:Read';

        export namespace Fields {
            export declare const Id;
            export declare const IdEmpresa;
            export declare const Fecha;
            export declare const Empleos;
            export declare const Anyo;
            export declare const Isla;
            export declare const AnyoExpediente;
        }

        [
            'Id',
            'IdEmpresa',
            'Fecha',
            'Empleos',
            'Anyo',
            'Isla',
            'AnyoExpediente'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}