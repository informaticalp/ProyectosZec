﻿
namespace ProyectosZec.Roezec {
    export interface InscritasRow {
        Id?: number;
        IdEmpresa?: number;
        NumeroAsiento?: number;
        NumeroLiquidacionTasa?: string;
        FechaSolicitud?: string;
        FechaResolucion?: string;
        FechaNotificacion?: string;
        Observaciones?: string;
        FechaAlta?: string;
        UsrAlta?: string;
        IdEmpresaDenominacionSocial?: string;
        IdEmpresaCif?: string;
        IdEmpresaDireccion?: string;
        IdEmpresaCp?: string;
        IdEmpresaPoblacion?: string;
        IdEmpresaProvincia?: string;
        IdEmpresaIsla?: string;
        IdEmpresaNotasMarginales?: string;
        IdEmpresaAnyoExpediente?: number;
        IdEmpresaNumExpediente?: number;
        IdEmpresaAgencia?: number;
        IdEmpresaTecnico?: string;
        IdEmpresaFormaJuridica?: string;
        IdEmpresaSuperficie?: number;
        IdEmpresaExentaAreaAcotada?: string;
        IdEmpresaMotivosExencion?: string;
        IdEmpresaObjetivoEmpleo?: number;
        IdEmpresaObjetivoInversion?: number;
        IdEmpresaObservacionesEmpleo?: string;
        IdEmpresaObservacionesInversion?: string;
        IdEmpresaPreEmpleo?: number;
        IdEmpresaPreInversion?: number;
        IdEmpresaTrasEmpleo?: number;
        IdEmpresaTrasInversion?: number;
        IdEmpresaFechaAlta?: string;
        IdEmpresaFechaModificacion?: string;
        IdEmpresaFechaBaja?: string;
        IdEmpresaSituacion?: string;
        IdEmpresaUsrAlta?: string;
        IdEmpresaUsrModificacion?: string;
        IdEmpresaUsrBaja?: string;
        IdEmpresaTrading?: number;
        IdEmpresaEmpleos2021?: number;
        IdEmpresaEmpleos2020?: number;
        IdEmpresaEmpleos2019?: number;
        IdEmpresaEmpleos2018?: number;
        IdEmpresaEmpleos2017?: number;
    }

    export namespace InscritasRow {
        export const idProperty = 'Id';
        export const nameProperty = 'NumeroLiquidacionTasa';
        export const localTextPrefix = 'Roezec.Inscritas';
        export const deletePermission = 'Roezec:Delete';
        export const insertPermission = 'Roezec:Insert';
        export const readPermission = 'Roezec:Read';
        export const updatePermission = 'Roezec:Modify';

        export namespace Fields {
            export declare const Id;
            export declare const IdEmpresa;
            export declare const NumeroAsiento;
            export declare const NumeroLiquidacionTasa;
            export declare const FechaSolicitud;
            export declare const FechaResolucion;
            export declare const FechaNotificacion;
            export declare const Observaciones;
            export declare const FechaAlta;
            export declare const UsrAlta;
            export declare const IdEmpresaDenominacionSocial;
            export declare const IdEmpresaCif;
            export declare const IdEmpresaDireccion;
            export declare const IdEmpresaCp;
            export declare const IdEmpresaPoblacion;
            export declare const IdEmpresaProvincia;
            export declare const IdEmpresaIsla;
            export declare const IdEmpresaNotasMarginales;
            export declare const IdEmpresaAnyoExpediente;
            export declare const IdEmpresaNumExpediente;
            export declare const IdEmpresaAgencia;
            export declare const IdEmpresaTecnico;
            export declare const IdEmpresaFormaJuridica;
            export declare const IdEmpresaSuperficie;
            export declare const IdEmpresaExentaAreaAcotada;
            export declare const IdEmpresaMotivosExencion;
            export declare const IdEmpresaObjetivoEmpleo;
            export declare const IdEmpresaObjetivoInversion;
            export declare const IdEmpresaObservacionesEmpleo;
            export declare const IdEmpresaObservacionesInversion;
            export declare const IdEmpresaPreEmpleo;
            export declare const IdEmpresaPreInversion;
            export declare const IdEmpresaTrasEmpleo;
            export declare const IdEmpresaTrasInversion;
            export declare const IdEmpresaFechaAlta;
            export declare const IdEmpresaFechaModificacion;
            export declare const IdEmpresaFechaBaja;
            export declare const IdEmpresaSituacion;
            export declare const IdEmpresaUsrAlta;
            export declare const IdEmpresaUsrModificacion;
            export declare const IdEmpresaUsrBaja;
            export declare const IdEmpresaTrading;
            export declare const IdEmpresaEmpleos2021;
            export declare const IdEmpresaEmpleos2020;
            export declare const IdEmpresaEmpleos2019;
            export declare const IdEmpresaEmpleos2018;
            export declare const IdEmpresaEmpleos2017;
        }

        [
            'Id',
            'IdEmpresa',
            'NumeroAsiento',
            'NumeroLiquidacionTasa',
            'FechaSolicitud',
            'FechaResolucion',
            'FechaNotificacion',
            'Observaciones',
            'FechaAlta',
            'UsrAlta',
            'IdEmpresaDenominacionSocial',
            'IdEmpresaCif',
            'IdEmpresaDireccion',
            'IdEmpresaCp',
            'IdEmpresaPoblacion',
            'IdEmpresaProvincia',
            'IdEmpresaIsla',
            'IdEmpresaNotasMarginales',
            'IdEmpresaAnyoExpediente',
            'IdEmpresaNumExpediente',
            'IdEmpresaAgencia',
            'IdEmpresaTecnico',
            'IdEmpresaFormaJuridica',
            'IdEmpresaSuperficie',
            'IdEmpresaExentaAreaAcotada',
            'IdEmpresaMotivosExencion',
            'IdEmpresaObjetivoEmpleo',
            'IdEmpresaObjetivoInversion',
            'IdEmpresaObservacionesEmpleo',
            'IdEmpresaObservacionesInversion',
            'IdEmpresaPreEmpleo',
            'IdEmpresaPreInversion',
            'IdEmpresaTrasEmpleo',
            'IdEmpresaTrasInversion',
            'IdEmpresaFechaAlta',
            'IdEmpresaFechaModificacion',
            'IdEmpresaFechaBaja',
            'IdEmpresaSituacion',
            'IdEmpresaUsrAlta',
            'IdEmpresaUsrModificacion',
            'IdEmpresaUsrBaja',
            'IdEmpresaTrading',
            'IdEmpresaEmpleos2021',
            'IdEmpresaEmpleos2020',
            'IdEmpresaEmpleos2019',
            'IdEmpresaEmpleos2018',
            'IdEmpresaEmpleos2017'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}